/*
 * PlugHR Admin User functionality
*/
/*
   0. Site Admin
   1. Site Users 
   2. Business Owner / HR Admin
   3. Payroll Admin
   4. Employees
*/

var ADMIN = require('./modules/admin-manager');
var CT = require('./modules/country-list');
var async 		= require('async');
var EM        = require('./modules/email-dispatcher');
var CLIENT = require('./modules/user-manager');

exports.showlogin = function(req, res) {
 	res.render('admin/login');
};

exports.login = function(req, res) {
  ADMIN.manualLogin(req.param('userid'), req.param('pwd'), function(e, o) {
    if (e)   {
      console.log(o.err+" -- "+ o.text);
      res.json(o);
    }	else	{
      req.plugHRapp.user = o;
      var rObject = new Object();
      rObject.err = false;
      if (o.usertype == 0)		{						// EQP SiteAdmin
	      rObject.redirect = 'a001';
	      res.json(rObject);
      } else if (o.usertype == 1)		{     // EQP SiteStaff
	      rObject.redirect = '101';
	      res.json(rObject);
      } else	{
       console.log("login failed");
       console.log(e);
       rObject.err = true;
       rObject.text = "Login failed";
	     res.json(rObject);
      }
    }
	});
};

exports.index = function(req, res) {
  res.render('admusr/101',{curruser : req.plugHRapp.user});
};

exports.show_upd_password = function(req, res) {
	res.render('admusr/103', {curruser : req.plugHRapp.user});
};

exports.upd_password = function(req, res) {
	ADMIN.updclientPassword(req.plugHRapp.user.userid , req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			res.json(o);
		}
	});
};

exports.show_client_password = function(req, res) {
	res.render('admusr/108', {curruser : req.plugHRapp.user});
};

exports.client_password = function(req, res) {
	CLIENT.updPwd(req.param('coid'), req.param('userid'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			res.json(o);
		}
	});
};

exports.logout = function(req, res) {
	req.plugHRapp.reset();
	var rObject = new Object();
	rObject.status = 200;
	rObject.redirect = 'admin';
	res.send(rObject);
};

exports.nosession = function(req, res) {
 	res.render('nosession');
};
// --------------------------------------------------------------------------
var preCompany = require('./modules/preCompany-manager');

exports.show105 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			preCompany.getFPpreCompany(req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admusr/105', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show105NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			preCompany.getNPpreCompany(req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
// --------------------------------------------------------------------------
// Clients lists
// --------------------------------------------------------------------------
var company = require('./modules/company-manager');

exports.show106 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			company.getallcompany(req.plugHRapp.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admusr/106', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0)});
		}
	);
};

// --------------------------------------------------------------------------
// Manage consultants
// --------------------------------------------------------------------------
var consul = require('./modules/consul-manager');

exports.show107 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			consul.getFPconsul(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admusr/107', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show107NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			consul.getNPconsul(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew107 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			consul.addnewconsul({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				emailid : req.param('email'),
				password : req.param('pwd'),
				cname : req.param('cname'),
				compname : req.param('comp'),
				addr1 : req.param('addr1'),
				addr2 : req.param('addr2'),
				city : req.param('city'),
				pincode : req.param('pcode'),
				gender : req.param('gender'),
				ccno : req.param('ccno'),
				ccdate : req.param('ccdate'),
				ccname : req.param('ccname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update107 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			consul.updateconsul({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				emailid : req.param('email'),
				password : req.param('pwd'),
				cname : req.param('cname'),
				compname : req.param('comp'),
				addr1 : req.param('addr1'),
				addr2 : req.param('addr2'),
				city : req.param('city'),
				pincode : req.param('pcode'),
				gender : req.param('gender'),
				ccno : req.param('ccno'),
				ccdate : req.param('ccdate'),
				ccname : req.param('ccname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete107 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			consul.deleteconsul({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				emailid : req.param('email'),
				password : req.param('pwd'),
				cname : req.param('cname'),
				compname : req.param('comp'),
				addr1 : req.param('addr1'),
				addr2 : req.param('addr2'),
				city : req.param('city'),
				pincode : req.param('pcode'),
				gender : req.param('gender'),
				ccno : req.param('ccno'),
				ccdate : req.param('ccdate'),
				ccname : req.param('ccname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

