/*
 * GET USERS listing.
*/
var COMPANY = require('./modules/company-manager');
var async = require('async');
var LIC = require('./modules/licenses-manager');
var devices 		= require('./modules/devices-manager');

//---------------------------------------------
// For File uploads 
//---------------------------------------------
var formidable = require('formidable')
var fs = require('fs');
var sys = require('sys');
var AWS = require('aws-sdk');

var mkdirp = require('mkdirp');
var format = require('util').format;
//Directory to upload file
var uploadPath="uploads/";
var userpictpath="uploads/userpics/";
//---------------------------------------------
/// Include ImageMagick
//---------------------------------------------
var im = require('imagemagick');
//---------------------------------------------

var accessKeyId =  process.env.AWS_ACCESS_KEY || "AKIAJGTOOVCWVATAEXFA";
var secretAccessKey = process.env.AWS_SECRET_KEY || "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";
var s3__bucket = "plughr-uploads";

exports.newCompany = function(req, res) {
	// employer join //
   
	COMPANY.newCompany({
		user 	      : req.param('email'),
		name        : req.param('username'),
	  password    : req.param('password'),
		company     : req.param('company'),
		mobile      : req.param('usermobile'),
	  usertype    : req.param('usertype')
	}, function(e,o)    {
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.regdevice = function(req, res) {
  if (!req.query.secode || !req.query.coid)
   	res.json('500');
  else    {
	  devices.regdevice(req.query.coid, req.query.secode, function(e, o)		 {
		  if (o.length > 0)  {
       	res.json(o);
		  }	else	{
       	res.json('500');
		  }
	  });
  }
};

exports.coverify = function(req, res) {
  if (!req.query.verid)
   	res.render('company/emailverify');
  else    {
	  COMPANY.verifyUser(req.query.verid, function(e, o)		 {
		  if (o.err != 0)  {
       	res.render('company/verifyfail');
		  }	else	{
       	res.render('company/verifyok');
		  }
	  });
  }
};

exports.verifycode = function(req, res) {
	COMPANY.verifyUser(req.param('verid'), function(e, o)		 {
		if (e)  {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			res.json(o);
		}
	});
};

exports.resendverid = function(req, res) {
	//console.log("------------------------------");
	//console.log(req.param('emailid'));
	//console.log("------------------------------");
	COMPANY.resendVerCode(req.param('emailid'), function(e, o)		{
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.company_owner = function(req, res) {
	res.render('owner/201', {curruser : req.sentriz.user});
};

exports.USER_partners = function(req, res) {
	res.render('USERS/301', {curruser : req.sentriz.user});
};

exports.USER_profile = function(req, res) {
	res.render('USERS/302', {curruser : req.sentriz.user});
};

exports.USER_courts = function(req, res) {
	res.render('USERS/303', {curruser : req.sentriz.user});
};

exports.new_courts = function(req, res) {
	res.render('USERS/305', {curruser : req.sentriz.user});
};

exports.show_upd_password = function(req, res) {
	res.render('USERS/304', {curruser : req.sentriz.user});
};

exports.upd_password = function(req, res) {
	USER.updatePassword(req.param('cuser'), req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};
// ---------------------------------------------------
// For Company Induction
// ---------------------------------------------------

var company = require('./modules/company-manager');

exports.show228 = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			company.getonecompany(req.sentriz.user.coid, req.sentriz.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/228', {curruser : req.sentriz.user, formdata : JSON.stringify(data0)});
		}
	);
};

exports.show328 = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			company.getonecompany(req.sentriz.user.coid, req.sentriz.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('hradmin/328', {curruser : req.sentriz.user, formdata : JSON.stringify(data0)});
		}
	);
};

exports.show428 = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			company.getonecompany(req.sentriz.user.coid, req.sentriz.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/428', {curruser : req.sentriz.user, formdata : JSON.stringify(data0)});
		}
	);
};

function oc(a){
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}

exports.showOnedoc = function(req, res) {
  //var tempFile = __dirname +'/uploads/' + req.param("docfile");
	var xcoid = req.sentriz.user.coid;
	var xuserid = req.sentriz.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/" + xuserid + "/ " + req.param("docfile");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};
// ---------------------------------------------------

exports.upload228doc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".pptx",".ppt"];
	      var maxSizeOfFile=10000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1024 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					var strKey = fields.ucoid + "/" + randm + file_extension;
				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
								s3.deleteObjects({
										Bucket: s3__bucket,
										Delete: {
												Objects: [
													 { Key: fields.ucoid + "/" + fields.currfile }
												]
										}
								}, function(err, data) {
									if (err)		{
										resp.fd = "File NOT Deleted";
									} else		{
										resp.fd = "File Deleted";
									}
									callback(err, data);
								});
							},
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Induction NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Induction Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          company.updateinduction({ coid: fields.ucoid,userid: fields.uuserid,docfile : randm + file_extension}, function(e1,o1)    {
									if (e1)
										resp.indcfname = randm + file_extension;
									else
										resp.indcfname = randm + file_extension;
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 5MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

// Company Licenses
exports.show104 = function(req, res) {
	res.render('admusr/104', {curruser : req.sentriz.user});
};

exports.showa004 = function(req, res) {
	res.render('admin/a004', {curruser : req.sentriz.user});
};

exports.searcha004 = function(req, res)   {
	LIC.getalllicense( req.param("coid"), function(e, o)			{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o);
			res.json(o);
		}
	});
};

exports.addnewlicense = function(req, res)   {
	LIC.addNewLicense({ coid:req.param("coid"), startdate:req.param("startdate"), enddate:req.param("enddate"), users:req.param("users"), notes:req.param("notes"), active:true},  function(e, o)			{
		if (e)    {
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o);
    	LIC.getalllicense( req.param("coid"), function(e2, liclist)			{
        if (!e2) {
  		    res.send(liclist);
        } else    {
          res.send(e2);
        }
      });
		}
  });
};

exports.updatelicense = function(req, res)   {
	LIC.updatelicense({ coid:req.param("coid"), startdate:req.param("startdate"), enddate:req.param("enddate"), users:req.param("users"), notes:req.param("notes"), active:true},  function(e, o)			{
		if (e)    {
			console.log(o.err + " -- " + o.text);
			res.json(o);
		}	else	{
    	LIC.getalllicense( req.param("coid"), function(e2, liclist)			{
        if (!e2) {
  		    res.send(liclist);
        } else    {
          res.send(e2);
        }
      });
		}
  });
};

exports.deletelicense = function(req, res)   {
	LIC.deletelicense({coid:req.param("coid"),dbid:req.param("dbid")}, function(e, o)			{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
	    res.json(o);
		}
  });
};

/*
exports.showlicenses = function(req, res)   {
	LIC.getalllicense( req.sentriz.user.uniid, function(e, liclist, tot)			{
  	res.render('uniadmins/uadmlicenses', {curruser : req.sentriz.user, licdata: JSON.stringify(liclist), totmesgs:tot});
	});
};


exports.licinactive = function(req, res)   {
	LIC.setinactive(req.param("_id"), function(e, liclist)			{
		if (e)		{
			console.log("University licenses not available");
			console.log(e);
			res.send(e, 400);
		}	else	{
			console.log("University licenses available");
    	LIC.getalllicense( req.param("uniid"), function(e2, liclist)			{
        if (!e) {
  		    res.send(liclist);
        } else    {
          res.send(e2);
        }
      });
		}
	});
};
*/


