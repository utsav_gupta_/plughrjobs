/*
 * EQUALPRO Homepage functionality
*/
/*
   0. Site Admin
   1. Site Users 
   2. Business employee
   3. HR Admin
   4. HR Users
*/

var async = require('async');
var users = require('./modules/user-manager');
var CT = require('./modules/country-list');
var COMPANY = require('./modules/company-manager');
var cvalues = require('./modules/cvalues-manager');
var jobs = require('./modules/jobs-manager');
var department = require('./modules/department-manager');
var kra = require('./modules/kra-manager');
var skills = require('./modules/skills-manager');
var candidskills = require('./modules/candidskills-manager');

//---------------------------------------------
// For File uploads 
//---------------------------------------------
var formidable = require('formidable')
var fs = require('fs');
var sys = require('sys');
var AWS = require('aws-sdk');

var mkdirp = require('mkdirp');
var format = require('util').format;
//Directory to upload file
var uploadPath="uploads/";
var userpictpath="uploads/userpics/";
//---------------------------------------------
/// Include ImageMagick
//---------------------------------------------
var im = require('imagemagick');
//---------------------------------------------

var accessKeyId =  process.env.AWS_ACCESS_KEY || "AKIAJGTOOVCWVATAEXFA";
var secretAccessKey = process.env.AWS_SECRET_KEY || "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";
var s3__bucket = "plughr-uploads";

var events = require('./modules/events-manager');
var blogs = require('./modules/blogs-manager');

exports.index = function(req, res) {
  res.render('candidate/501',{curruser : req.plughrjobs.user, cuserdata : JSON.stringify(req.plughrjobs.user), formdata: JSON.stringify(req.plughrjobs.user)});
};

// --------------------------------------------------------------------------
// Candidate self Password
// --------------------------------------------------------------------------
exports.show_upd_password = function(req, res) {
	res.render('candidate/503', {cologo:req.plughrjobs.user.clogo, curruser : req.plughrjobs.user});
};

exports.upd_password = function(req, res) {
	users.updatePassword(req.plughrjobs.user.user , req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// About You - part of user profile info
// --------------------------------------------------------------------------
exports.show502 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		/*function(callback) {
			department.getalldepartment(req.plughrjobs.user.user, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			kra.getallkra(req.plughrjobs.user.user, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},*/
		function(callback) {
			candidskills.getallskills(req.plughrjobs.user.user, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('candidate/502', {curruser : req.plughrjobs.user, skillsdata : JSON.stringify(data3), formdata : JSON.stringify(req.plughrjobs.user), totpages:tot});
		}
	);
};
exports.update502 = function(req, res) {
	var userObject = new Object();
	userObject.dbid = req.param('dbid');
	userObject.uname = req.param('uname');
	userObject.emailid = req.param('emailid');
	userObject.mobno = req.param('mobno');
	userObject.gender = req.param('gender');
	userObject.cal1 = req.param('cal1');
	userObject.cal2 = req.param('cal2');
	userObject.ccity = req.param('ccity');
	userObject.cpin = req.param('cpin');
	userObject.linkedin = req.param('linkedin');
	userObject.fb = req.param('fb');
	userObject.twit = req.param('twit');
	userObject.dob = req.param('dob');
	//userObject.dept = req.param('dept');
	userObject.industry = req.param('industry');
	userObject.skills = req.param('skills');
	userObject.about = req.param('about');
	userObject.sigach = req.param('sigach');
	userObject.indresp = req.param('indresp');
	userObject.adays = req.param('adays');
	
	/*
	userObject.astime1 = req.param('astime1');
	userObject.aetime1 = req.param('aetime1');
	userObject.astime2 = req.param('astime2');
	userObject.aetime2 = req.param('aetime2');
	userObject.astime3 = req.param('astime3');
	userObject.aetime3 = req.param('aetime3');
	userObject.astime4 = req.param('astime4');
	userObject.aetime4 = req.param('aetime4');
	userObject.astime5 = req.param('astime5');
	userObject.aetime5 = req.param('aetime5');
	userObject.astime6 = req.param('astime6');
	userObject.aetime6 = req.param('aetime6');
	userObject.astime7 = req.param('astime7');
	userObject.aetime7 = req.param('aetime7');
	*/

	userObject.e1org = req.param('e1org');
	//userObject.e1dept = req.param('e1dept');
	userObject.e1role = req.param('e1role');
	userObject.e1sdate = req.param('e1sdate');
	//userObject.e1days = req.param('e1days');
	//userObject.e1stime = req.param('e1stime');
	//userObject.e1etime = req.param('e1etime');
	userObject.e2org = req.param('e2org');
	//userObject.e2dept = req.param('e2dept');
	userObject.e2role = req.param('e2role');
	userObject.e2sdate = req.param('e2sdate');
	//userObject.e2days = req.param('e2days');
	//userObject.e2stime = req.param('e2stime');
	//userObject.e2etime = req.param('e2etime');
	userObject.p1org = req.param('p1org');
	//userObject.p1dept = req.param('p1dept');
	userObject.p1role = req.param('p1role');
	userObject.p1sdate = req.param('p1sdate');
	userObject.p1edate = req.param('p1edate');
	userObject.p2org = req.param('p2org');
	//userObject.p2dept = req.param('p2dept');
	userObject.p2role = req.param('p2role');
	userObject.p2sdate = req.param('p2sdate');
	userObject.p2edate = req.param('p2edate');
	userObject.ed1name = req.param('ed1name');
	userObject.ed1colg = req.param('ed1colg');
	userObject.ed1sdate = req.param('ed1sdate');
	userObject.ed1edate = req.param('ed1edate');
	userObject.ed2name = req.param('ed2name');
	userObject.ed2colg = req.param('ed2colg');
	userObject.ed2sdate = req.param('ed2sdate');
	userObject.ed2edate = req.param('ed2edate');
	userObject.ed3name = req.param('ed3name');
	userObject.ed3colg = req.param('ed3colg');
	userObject.ed3sdate = req.param('ed3sdate');
	userObject.ed3edate = req.param('ed3edate');

	users.updatecandiate(userObject, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Candidate job search page
// --------------------------------------------------------------------------
exports.jsearch = function(req, res) {
	var data0;
	async.parallel([
		function(callback) {
			jobs.searchjobs('','india','', function(e, o)		{
				if (e)    {
					data0 = null;
				}	else	{
					data0 = o;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
		  res.render('candidate/501jsearch', {curruser : JSON.stringify(req.plughrjobs.user), formdata : JSON.stringify(data0)});
		  //res.json(resp);
		}
	);
};

exports.searchjobs = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			jobs.searchjobs(req.param('stext'),req.param('jobcntry'), req.param('jobcity'), function(e, o)		{
				if (e)    {
					resp.err1 = true;
					resp.data = null;
				}	else	{
					resp.err1 = false;
					resp.data = o;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
		  res.json(resp);
		}
	);
};

exports.showjob = function(req, res) {
  if (!req.query.jobid)   {
   	res.render('candidate/501');
  } else    {
	  async.parallel([
		  function(callback) {
			  department.getalldepartment(null, function(e, o, t) {
				  if (o)
					  data0 = o;
				  else
					  data0 = null;
				  callback(e,o);
			  });
		  },
		  function(callback) {
			  kra.getallkra(null, function(e1, o1) {
				  if (o1)
					  data1 = o1;
				  else
					  data1 = null;
				  callback(e1,o1);
			  });
		  },
		  function(callback) {
			  skills.getallskills(null, function(e3, o3) {
				  if (o3)
					  data3 = o3;
				  else
					  data3 = null;
				  callback(e3,o3);
			  });
		  },
		  function(callback) {
    	  jobs.getjobbyid(req.query.jobid, function(e2, o2)		 {
				  if (o2)
					  data2 = o2;
				  else
					  data2 = null;
				  callback(e2,o2);
			  });
		  },
		  ], function(err, results) {
			  res.render('candidate/501jobdetails', {curruser : JSON.stringify(req.plughrjobs.user), deptdata : JSON.stringify(data0),kradata : JSON.stringify(data1),skillsdata : JSON.stringify(data3),formdata : JSON.stringify(data2)});
		  }
	  );
 	}
};

// --------------------------------------------------------------------------
// Top management self goals
// --------------------------------------------------------------------------
var usergoals = require('./modules/usergoals-manager');

exports.show404 = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/404', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1)});
		}
	);
};

exports.search404 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				pageno : req.param('pageno')
			}, function(e,o,tot)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		// For Rolewise goals from repository
		function(callback) {
			perfgoals.getperfgoalsbyrole({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				roleid : req.workhalf.user.role,
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.data2 = o2;
				} else		{
					resp.err = true;
					resp.data2 = null;
				}
				callback(e2,o2);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show404NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew404 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
				submitted : '0',
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update404 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete404 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.workhalf.user.userid,
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// User Education - part of user profile info
// --------------------------------------------------------------------------
var useredu = require('./modules/useredu-manager');

exports.show405 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			useredu.getFPuseredu(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/405', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show405NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			useredu.getNPuseredu(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew405 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.addnewuseredu({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update405 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.updateuseredu({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
				arrIndex : req.param('arrIndex'),
				old_udegree : req.param('old_dearned'),
				old_dlevel : req.param('old_dlevel'),
				old_dyear : req.param('old_dyear'),
				old_duni : req.param('old_duni'),
				old_dcollege : req.param('old_dcollege')
			}, function(e,o, tot)	{
				if (!o.err)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete405 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.deleteuseredu({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// User Work Experiences - part of user profile info
// --------------------------------------------------------------------------
var workexp = require('./modules/workexp-manager');

exports.show406 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			workexp.getFPworkexp(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/406', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};

exports.show406NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			workexp.getNPworkexp(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew406 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.addnewworkexp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.update406 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.updateworkexp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				arrIndex : req.param('arrIndex'),
				old_orgn : req.param('old_orgn'),
				old_title : req.param('old_title'),
				old_sdate : req.param('old_sdate'),
				old_edate : req.param('old_edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.delete406 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.deleteworkexp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Team Holiday calender
// --------------------------------------------------------------------------

exports.show407 = function(req, res) {
	users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/407', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('employee/407', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search407 = function(req, res) {
	var data0,  data1, data2, data3;
	var resp = new Object;

	async.parallel([
		//Get All Holiday List
		function(callback) {
			holidaylist.getallholidaylist(req.workhalf.user.coid, req.param('userid'), function(e,o)		{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		//Get All Locations List
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.param('userid'), function(e1,o1)		{
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get Holiday policy for location
		function(callback) {
			holidaypolicy.getlocholidaypolicy(req.workhalf.user.coid, req.param('userid'), req.param('location'), function(e2,o2)		{
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			userhday.getalluserhday(req.workhalf.user.coid, parseInt(req.param('userid')), function(e3,o3)		{
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			resp.hiddata = data0; 
			resp.onamedata = data1;
			resp.hpdata = data2;
			resp.formdata = data3;
			res.json(resp);
		}
	);
};
	
// --------------------------------------------------------------------------
// Company information
// --------------------------------------------------------------------------
var company = require('./modules/company-manager');

exports.show408 = function(req, res) {
	company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/408', {curruser : req.workhalf.user});
		else  {
			res.render('employee/408', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

// --------------------------------------------------------------------------
// Team skills rating
// --------------------------------------------------------------------------
var userskills = require('./modules/userskills-manager');

exports.show412 = function(req, res) {
	users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/412', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('employee/412', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search412 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.getskillsofuser(req.workhalf.user.coid, req.param('emp'), function(e1, o1) {
				if (o1)
					resp.data = o1;
				else
					resp.data = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.skilldata = o2;
				else
					resp.skilldata = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.skrdata = o3;
				else
					resp.skrdata = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			roles.getskillsofrole(req.workhalf.user.coid, req.param('role'), function(e4, o4) {
				if (o4)
					resp.roleskills = o4;
				else
					resp.roleskills = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew412 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.updateuserskills({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('emp')),
				dbid : req.param('dbid'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				skratings : req.param('skratings'),
				updby : req.workhalf.user.userid,
				active : true,
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.text = '';
					resp.data = o2;
				} else		{
					resp.err = true;
					resp.text = o2.text;
					resp.data = null;
				}
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.update412 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.updateuserskills({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('emp'),
				skillid : req.param('skill'),
				sklevel : req.param('rtng'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.delete412 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.deleteuserskills({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('emp'),
				skillid : req.param('skill'),
				sklevel : req.param('rtng'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Avail Holidays
// --------------------------------------------------------------------------
var holidaylist = require('./modules/holidaylist-manager');
var locations = require('./modules/locations-manager');
var holidaypolicy = require('./modules/holidaypolicy-manager');
var userhday = require('./modules/userhday-manager');

exports.show422 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Holiday List
		function(callback) {
			holidaylist.getallholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, function(e,o)		{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		//Get All Locations List
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e1,o1)		{
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get Holiday policy for location
		function(callback) {
			holidaypolicy.getlocholidaypolicy(req.workhalf.user.coid, req.workhalf.user.userid, req.workhalf.user.location, function(e2,o2)		{
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			userhday.getalluserhday(req.workhalf.user.coid, req.workhalf.user.userid, function(e3,o3)		{
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/422', {curruser : req.workhalf.user, hiddata:JSON.stringify(data0), onamedata:JSON.stringify(data1),  hpdata : JSON.stringify(data2),  formdata : JSON.stringify(data3)});
		}
	);
};
	

exports.addnew422 = function(req, res) {
	userhday.availhday({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		hpolicyid : req.param('policyid'),
		holidayid : req.param('holidayid'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};

exports.del422 = function(req, res) {
	userhday.cancelhday({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		hpolicyid : req.param('policyid'),
		holidayid : req.param('holidayid'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Upload Photo
// --------------------------------------------------------------------------
exports.show424 = function(req, res) {
	res.render('employee/424', {curruser : req.workhalf.user, formdata : JSON.stringify(req.workhalf.user)});
};

// --------------------------------------------------------------------------
// Upload Documents
// --------------------------------------------------------------------------
var userdocs = require('./modules/userdocs-manager');

exports.show425 = function(req, res) {
	var data0, data1, data2;
	var tot;

	async.parallel([
		function(callback) {
			userdocs.getFPuserdocs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			//console.log(data0);
			res.render('employee/425', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};

exports.show425NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			userdocs.getNPuserdocs(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.uploadnewdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".jpg",".tif",".png"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1034 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/" + fields.uuserid + "/" + files.docfile.name;

				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Document NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Document Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          userdocs.addnewuserdocs({ coid: fields.ucoid, userid: fields.uuserid, catg: fields.catg, docdesc : fields.desc, docfile : files.docfile.name}, function(e1,o1,tot)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.totpages = tot;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

function oc(a){
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}

exports.deleteUserDoc = function(req, res) {
	var s3 = new AWS.S3();
	var resp = new Object;

	async.series([
		function(callback) {
			userdocs.deleteuserdocs({coid: req.workhalf.user.coid, userid: req.workhalf.user.userid, dbid: req.param('dbid') }, function(e1,o1,tot)	{
				if (e1)		{
					resp.err = true;
					resp.text = "File NOT Deleted";
					resp.data = null;
				} else		{
					resp.err = false;
					resp.text = "File Deleted";
					resp.data = o1;
				}
				resp.totpages = tot;
				callback(e1,o1);
			});
		},
		function(callback) {
			s3.deleteObjects({
					Bucket: s3__bucket,
					Delete: {
							Objects: [
								 { Key: req.workhalf.user.coid + "/" + req.workhalf.user.userid  + "/" + req.param('filename')}
							]
					}
			}, function(err, data) {
				if (err)		{
					resp.fd = "File NOT Deleted";
				} else		{
					resp.fd = "File Deleted";
				}
				callback(err, data);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.showOnedoc = function(req, res) {
	var xcoid = req.workhalf.user.coid;
	var xuserid = req.workhalf.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/" + xuserid + "/ " + req.param("docfile");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};

// --------------------------------------------------------------------------
// Company Induction Exams
// --------------------------------------------------------------------------
var iquestions = require('./modules/iquestions-manager');

exports.show429 = function(req, res) {
	var data0,  data1, data2;
	var tot, Qs;
	async.parallel([
		function(callback) {
			iquestions.getalliquestions(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t, q) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				Qs = q;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/429', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot, totqs:Qs});
		}
	);
};

exports.save429 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.saveindc({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				indcans: req.param('ansData'),
				indcfb: req.param('feedback'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = true;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Employee Avail Leave
// --------------------------------------------------------------------------
var empleaves = require('./modules/empleaves-manager');
var leavetypes = require('./modules/leavetypes-manager');
var emplvbal = require('./modules/emplvbal-manager');

exports.show430 = function(req, res) {
	var data0, data1, data2, data3, data4, data5;
	var tot;
	async.parallel([
		function(callback) {
			empleaves.getFPempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			emplvbal.getemplvbal(req.workhalf.user.coid, req.workhalf.user.userid.toString(), function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		//Get All holidays List
		function(callback) {
			holidaylist.getallholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			res.render('employee/430', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,lnamedata : JSON.stringify(data1), codata:JSON.stringify(data2), lvdata:JSON.stringify(data3), emplvdata:JSON.stringify(data4), hdaydata:JSON.stringify(data5), totpages:tot});
		}
	);
};
exports.show430NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			empleaves.getNPempleaves(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew430 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.waterfall([
		function(callback) {
			empleaves.addnewempleaves({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				mgr : req.workhalf.user.mgr,
				lname : req.param('lname'),
				ldays : req.param('ldays'),
				hdays : req.param('hdays'),
				ltot : req.param('ltot'),
				lstatus: req.param('lstatus'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,resp);
				});
		},
		function(resp, callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.lvdata = o3;
				else
					resp.lvdata = null;
				callback(e3,resp);
			});
		},
		function(resp, callback) {
			users.getusersById(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				callback(e1,resp, o1[0].username);
			});
		},
		function(resp, username, callback) {
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.workhalf.user.mgr,
        mailid : '',
        mtype  : '1', 
        mesg   : username + " has submitted a new leave request. Please approve / reject at earliest.",
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update430 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.waterfall([
		function(callback) {
			empleaves.updateempleaves({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				mgr : req.workhalf.user.mgr,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				ltot : req.param('ltot'),
				ldays : req.param('ldays'),
				hdays : req.param('hdays'),
        optype : "u"
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,resp);
				});
		},
		function(resp,callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.lvdata = o3;
				else
					resp.lvdata = null;
				callback(e3,resp);
			});
		},
		function(resp, callback) {
			users.getusersById(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				callback(e1,resp, o1[0].username);
			});
		},
		function(resp, username, callback) {
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.workhalf.user.mgr,
        mailid : '',
        mtype  : '1', 
        mesg   : username + " has updated a leave request. Please approve / reject at earliest.",
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete430 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.waterfall([
		function(callback) {
			empleaves.deleteempleaves({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				mgr : req.workhalf.user.mgr,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				hdays : req.param('hdays'),
				ldays : req.param('ldays'),
				ltot : req.param('ltot'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,resp);
			});
		},
		function(resp,callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.lvdata = o3;
				else
					resp.lvdata = null;
				callback(e3,resp);
			});
		},
		function(resp, callback) {
			users.getusersById(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				callback(e1,resp, o1[0].username);
			});
		},
		function(resp, username, callback) {
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.workhalf.user.mgr,
        mailid : '',
        mtype  : '2', 
        mesg   : username + " has deleted a leave request.",
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		], function(err, results) {
				res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------
// Team leave request
// --------------------------------------------------------------------------
exports.show409 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			empleaves.getteamopenleaves(req.workhalf.user.coid, req.workhalf.user.userid.toString(), function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/409', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), lnamedata : JSON.stringify(data1), empdata : JSON.stringify(data2), totpages:tot});
		}
	);
};

exports.show409NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			empleaves.getNPempleaves(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update409 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.waterfall([
		function(callback) {
			empleaves.updateempleaves({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				ltot : req.param('ltot'),
				ldays : req.param('ldays'),
				hdays : req.param('hdays'),
				optype: req.param('optype'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,resp);
				});
		},
		function(resp, callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.lvdata = o3;
				else
					resp.lvdata = null;
				callback(e3,resp);
			});
		},
		function(resp, callback) {
			users.getusersById(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				callback(e1,resp, o1[0].username);
			});
		},
		function(resp, username, callback) {
      var actn = '';
      if (req.param('optype') == 'a')
        actn = "Approved";
      else
        actn = "Rejected";
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('luser'),
        mailid : '',
        mtype  : '2', 
        mesg   : username + " has "+ actn +" your leave request.",
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		], function(err, results) {
				res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------
// Team Leave history
// --------------------------------------------------------------------------
exports.show410 = function(req, res) {
	users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/410', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('employee/410', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search410 = function(req, res) {
	var data0,  data1, data2, data3;
	var resp = new Object;

	async.parallel([
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)		{
					resp.err = false;
					resp.data = o1;
				}		else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e1,o1);
			});
		},
		function(callback) {
			company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			empleaves.getempleaves(req.workhalf.user.coid, parseInt(req.param('userid')), function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			emplvbal.getemplvbal(req.workhalf.user.coid, req.param('userid'), function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			resp.codata = data2;
			resp.lvdata = data3;
			resp.emplvdata = data4;
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Accept Performance Goals
// --------------------------------------------------------------------------
var revperiod = require('./modules/revperiod-manager');
var perfgoals = require('./modules/perfgoals-manager');
var usergoals = require('./modules/usergoals-manager');

exports.show431 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
				res.render('employee/431', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1)});
		}
	);
};

exports.search431 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.getgoalsbyperiod_role({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				role : req.workhalf.user.role,
				rperiod : req.param('rpid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.save431 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				data: req.param('data'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				data: req.param('data'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance Progress
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show432 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/432', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3)});
		}
	);
};

exports.search432 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rpid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			incv.getincvbyperiod_user({
				coid : req.workhalf.user.coid, 
				userid : req.workhalf.user.userid,
				rperiod : req.param("rpid"),
			}, function(e1,o1)    {
				if (!o1.err)		{
					resp.incvfile = o1.incvfile;
				} else		{
					resp.incvfile = "";
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.task432 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updategoaltask({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				tid : req.param('tid'),
				tdesc : req.param('tdesc'),
				tdate : req.param('tdate'),
				tsts : req.param('tsts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.self432 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateselfrating({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit432 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.workhalf.user.mgr,
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has completed self-rating for " + rptitle.substr(0,rptitle.indexOf(' 00:00:00')),
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.submitgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.delete432 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				rperiod : req.param('rper'),
				role : req.param('role'),
				kra : req.param('kra'),
				meets : req.param('meets'),
				exceeds : req.param('exceed'),
				sigexceed : req.param('sigex'),
				weight : req.param('wt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.addnew432 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
				rperiod : req.param('rper'),
				role : req.param('role'),
				kra : req.param('kra'),
				meets : req.param('meets'),
				exceeds : req.param('exceed'),
				sigexceed : req.param('sigex'),
				weight : req.param('wt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Resignations
// --------------------------------------------------------------------------

var exits = require('./modules/exits-manager');

exports.show435 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			exits.getFPexits(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			 exitacts.getallexitacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/435', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), repldata : JSON.stringify(data1), eactdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show435NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			exits.getNPexits(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew435 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.addnewexits({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				mgr : req.workhalf.user.mgr,
				reason : req.param('reason'),
				ldate : req.param('ldate'),
				repluserid : req.param('repl'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (!e)		{
					resp.err = false;
					resp.data = o;
					resp.text = '';
				} else		{
					resp.err = o.err;
					resp.data = o.data;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update435 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.updateexits({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				reason : req.param('reason'),
				ldate : req.param('ldate'),
				repluserid : req.param('repl'),
				status : req.param('sts'),
				fback : req.param('fback'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete435 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.deleteexits({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				reason : req.param('reason'),
				ldate : req.param('ldate'),
				repluserid : req.param('repl'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Official data for user
// --------------------------------------------------------------------------

var department = require('./modules/department-manager');
var skills = require('./modules/skills-manager');

exports.show433 = function(req, res) {
	var data0, data1, data2, data3,  data4, data5, data6, data7, data8;
	var roleid = '';
	var kraids = [];

	async.parallel([
		function(callback) {
			async.series([
				function(callback) {
					users.getoneusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
						if (o)	{
							data0 = o;
							roleid = o.role;
						} else		{
							data0 = null;
							roleid = null;
						}
						callback(e,o);
					});
				},
				function(callback) {
					roles.getrolesbyid(req.workhalf.user.coid, roleid, function(e2, o2) {
						if (o2)		{
							data2 = o2;
						} else	{
							data2 = null;
						}
						callback(e2,o2);
					});
				},
				function(callback) {
					kra.getkraarray(req.workhalf.user.coid, data2[0].kras, function(e6, o6) {
						if (o6)
							data6 = o6;
						else
							data6 = null;
						callback(e6,o6);
					});
				},
				], function(err, results) {
					callback(null,null);
				}
			);
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		function(callback) {
			userskills.getskillsofuser(req.workhalf.user.coid, req.workhalf.user.userid, function(e8, o8) {
				if (o8)
					data8 = o8;
				else
					data8 = null;
				callback(e8,o8);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e7, o7) {
				if (o7)
					data7 = o7;
				else
					data7 = null;
				callback(e7,o7);
			});
		},
		], function(err, results) {
			res.render('employee/433', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2), locdata : JSON.stringify(data3), empdata : JSON.stringify(data4), skilldata : JSON.stringify(data5), kradata : JSON.stringify(data6), skrdata : JSON.stringify(data7), uskldata: JSON.stringify(data8)});
		}
	);
};

// --------------------------------------------------------------------------
// Manage Team member goals
// --------------------------------------------------------------------------
var usergoals = require('./modules/usergoals-manager');

exports.show434 = function(req, res) {
	var data0,  data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data0 = null;
				callback(e0,o0);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		// For manager's self goals
		function(callback) {
			usergoals.getgoalsbyuser({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
			}, function(e3,o3)	{
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/434', {curruser : req.workhalf.user, kradata : JSON.stringify(data0), rperioddata : JSON.stringify(data1), usrdata : JSON.stringify(data2),sgoaldata : JSON.stringify(data3) });
		}
	);
};

exports.search434 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('usr')),
				rperiod : req.param('rperiod'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		// For Rolewise goals from repository
		function(callback) {
			perfgoals.getperfgoalsbyrole({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				roleid : req.param('roleid'),
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.data2 = o2;
				} else		{
					resp.err = true;
					resp.data2 = null;
				}
				callback(e2,o2);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show434NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew434 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
				trgt_submited : '0',
				self_submited : '0',
				supr_submited : '0',
				skip_submited : '0',
			}, function(e3,o3, tot)	{
				if (o3)		{
					resp.err = false;
					resp.data = o3;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e3,o3);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.update434 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e3,o3, tot)	{
				if (o3)		{
					resp.err = false;
					resp.data = o3;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e3,o3);
			});
	  },
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.delete434 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit434 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
      var tstr = "here";
      var lstr = tstr.link("/432");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('userid'),
        mailid : '',
        mtype  : '2', 
        mesg   : "You have been assigned new targets for the "+rptitle.substr(0,rptitle.indexOf(' 00:00:00'))+ " by " + req.workhalf.user.username + ".  Look at your targets "+lstr,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.commitgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

var incv = require('./modules/incv-manager');

exports.getincvdoc = function (req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			incv.getincvbyperiod_user({coid : req.workhalf.user.coid, userid : req.param("user"), rperiod : req.param("rperiod")}, function(e1,o1)    {
				if (o1.err > 0)		{
					resp.err = true;
					resp.data = null;
					resp.text = o1.text;
				} else		{
					resp.err = false;
					resp.data = o1;
					resp.text = "";
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			//console.log(resp);
			res.json(resp);
		}
	);
}

// Upload incentives plan
exports.incvdocsul = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
	var resp = new Object;
	var data0,  data1, data2;
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      var filename = files.docfile.name;
      var extensionAllowed=[".docx",".doc",".pdf",".xls",".xlsx"];
      var maxSizeOfFile=2500;
      var i = filename.lastIndexOf('.');

		  var file_extension= (i < 0) ? '' : filename.substr(i);
		  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1024 ) < maxSizeOfFile))   {
				AWS.config.update({
					  accessKeyId: accessKeyId,
					  secretAccessKey: secretAccessKey
				});
		    //var randm = Math.random().toString(36).slice(-16);
				//var strKey = fields.ucoid + "/" + randm + file_extension;

				var incvfile = fields.urperiod + "_incv" + file_extension;
        var strKey = fields.ucoid + "/" + fields.uuserid + "/" + incvfile;
				//console.log(strKey);

			  var path = files.docfile.path;
			  fs.readFile(path, function(err, file_buffer)		{
		      var params = {
				      Bucket: s3__bucket,
				      Key: strKey,
		          Body: file_buffer,
							ACL: 'public-read'
		      };
					var s3 = new AWS.S3();
					async.series([
						function(callback) {
					    s3.putObject(params, function (e, o) {
								if (e)		{
									resp.err = 1;
									resp.text = "Incentive Plan NOT uploaded";
								} else		{
									resp.err = 0;
									resp.text = "Incentive Plan  uploaded";
								}
								callback(e,o);
							});
						},
						function(callback) {
							incv.updatedata({coid : fields.ucoid, userid : fields.uuserid, rperiod : fields.urperiod, incvfile : incvfile}, function(e1,o1,tot)    {
								if (o1)		{
									resp.err = false;
									resp.data = o1;
									resp.incvfile = incvfile;
								} else		{
									resp.err = true;
									resp.data = null;
									resp.incvfile = incvfile;
								}
								callback(e1,o1);
							});
						},
						], function(err, results) {
							res.json(resp);
						}
					);
				});
			} else  {
	      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
			}
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

// --------------------------------------------------------------------------
// Team Performance Progress
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');
var sklevel = require('./modules/sklevel-manager');
var userskills = require('./modules/userskills-manager');

exports.show438 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e6, o6) {
				if (o6)
					data6 = o6;
				else
					data6 = null;
				callback(e6,o6);
			});
		},
		], function(err, results) {
			res.render('employee/438', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), usrdata : JSON.stringify(data4), skilldata : JSON.stringify(data5), skrdata : JSON.stringify(data6)});
		}
	);
};
exports.search438 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.getskillsofuser(req.workhalf.user.coid, req.param('usr'), function(e1, o1) {
				if (o1)		{
					resp.skills = o1;
				} else		{
					resp.skills = null;
				}
				callback(e1,o1);
			});
		},
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.mgr438 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updatemgrrating({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit438 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
      var tstr = "here";
      var lstr = tstr.link("/432");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('userid'),
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has rated your performance for period  "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) + ". Please look up "+lstr,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.msubmitgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skip Reports Performance Progress
// --------------------------------------------------------------------------

exports.show439 = function(req, res) {
	var data0, data1, data2, data3, data4;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.render('employee/439', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), repdata : JSON.stringify(data4)});
		}
	);
};

exports.skips439 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.param('usr'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search439 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.mgr439 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateskiprating({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit439 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('userid'),
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has completed skip manager rating for " + rptitle.substr(0,rptitle.indexOf(' 00:00:00')),
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.ssubmitgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Self PIP
// --------------------------------------------------------------------------
var userpip = require('./modules/userpips-manager');

exports.show440 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			userpip.getFPpiptask(req.workhalf.user.coid, req.workhalf.user.userid.toString(), function(e,o)	{
				if (o)		{
					data0 = o;
				} else		{
					data0 = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.render('employee/440', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) });
		}
	);
};

exports.search440 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.getpipbyuser({
				coid : req.workhalf.user.coid,
				userid : req.param('usr'),
				rperiod : req.param('rperiod'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show440NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew440 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.addnewuserpips({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update440 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.updateuserpips({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete440 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.deleteuserpips({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.self440 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.updpipself({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				scomm : req.param('scomm'),
				old_obj : req.param('old_obj'),
				old_meets : req.param('old_meets'),
				old_sdate : req.param('old_sdate'),
				old_edate : req.param('old_edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			//console.log(resp);
			res.json(resp);
		}
	);
};


// --------------------------------------------------------------------------
// Search Employees
// --------------------------------------------------------------------------

exports.show426 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data1 = null;
				callback(e0,o0);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/426', {curruser : req.workhalf.user, formdata : null , skilldata : JSON.stringify(data0), depdata : JSON.stringify(data1), locdata : JSON.stringify(data2), roledata : JSON.stringify(data3)});
		}
	);
};

exports.show426user = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
  var uid = parseInt(req.query.userid);

	async.parallel([
		function(callback) {
		  users.getoneusers(req.query.coid, uid, function(e, o) {
				if (o)		{
					data1 = o;
					data1.coid = req.query.coid;
				} else
					data1 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.query.coid, uid, function(e1, o1) {
				if (o1)
					data2 = o1;
				else
					data2 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.query.coid, uid, function(e2, o2) {
				if (o2)
					data3 = o2;
				else
					data3 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.query.coid, uid, function(e3, o3) {
				if (o3)
					data4 = o3;
				else
					data4 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data5 = o4;
				else
					data5 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
	   	res.render('employee/426user', {formdata : JSON.stringify(data1), depdata : JSON.stringify(data2), roledata : JSON.stringify(data3), locdata : JSON.stringify(data4), skilldata : JSON.stringify(data5)});
		}
	);
};

exports.show426docs = function(req, res) {
	var data0, data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userdocs.getFPuserdocs(req.param('coid'), req.param('userid'), function(e, o, tot) {
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else	{
					resp.err = false;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search426 = function(req, res) {
	var data0,  data1, data2, data3;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.searchemp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				bgroup : req.param('bgrp'),
				pint : req.param('pint'),
				addr : req.param('addr'),
				univ : req.param('uni'),
				udegree : req.param('degree'),
				college : req.param('college'),
				orgn : req.param('emp'),
				skill : req.param('skill'),
			}, function(e,o, tot, searchObj)	{
				if (e)		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
		      resp.sobj = searchObj;
				} else		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
		      resp.sobj = searchObj;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.show426NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
		  users.searchNP(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), req.param('sobj'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

// --------------------------------------------------------------------------
// Goals unassigned Report
// --------------------------------------------------------------------------
exports.show427 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/427', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search427 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)	{
					resp.err = false;
					resp.data = o;
				} else	{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance tasks creation status
// --------------------------------------------------------------------------
exports.show431 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/431', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search431 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)	{
					resp.err = false;
					resp.data = o;
				} else	{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Overdue performance tasks - Team
// --------------------------------------------------------------------------
exports.show411 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/411', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search411 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)	{
					resp.err = false;
					resp.data = o;
				} else	{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Team member PIP
// --------------------------------------------------------------------------
var userpip = require('./modules/userpips-manager');

exports.show441 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/441', {curruser : req.workhalf.user, usrdata : JSON.stringify(data2) });
		}
	);
};

exports.search441 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			userpip.getFPpiptask(req.workhalf.user.coid,req.param('usr'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show441NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			userpip.getFPpiptask(req.workhalf.user.coid, req.param('usr'), req.param('newpage'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew441 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.addnewpiptask({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update441 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.updatepiptask({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				old_obj : req.param('old_obj'),
				old_meets : req.param('old_meets'),
				old_sdate : req.param('old_sdate'),
				old_edate : req.param('old_edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete441 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.deletepiptask({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				obj : req.param('obj'),
				meets : req.param('meets'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				//console.log(resp);
				res.json(resp);
		}
	);
};

exports.mgr441 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpip.updpipmgr({
				coid : req.workhalf.user.coid,
				mgr : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid: req.param('usr'),
				mcomm : req.param('mcomm'),
				old_obj : req.param('old_obj'),
				old_meets : req.param('old_meets'),
				old_sdate : req.param('old_sdate'),
				old_edate : req.param('old_edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			//console.log(resp);
			res.json(resp);
		}
	);
};


// --------------------------------------------------------------------------
// Approve / Reject Resignation
// --------------------------------------------------------------------------

var exits = require('./modules/exits-manager');
var exitacts = require('./modules/exitacts-manager');

exports.show442 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			exits.exitsbymgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			 exitacts.getallexitacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/442', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), repldata : JSON.stringify(data1), eactdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show442NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			exits.getNPexits(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update442 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.mgrupdates({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				mgr : req.param('mgr'),
				ldate : req.param('ldate'),
				repluserid : req.param('repl'),
				status : req.param('sts'),
				eact : req.param('eact'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Corporate Values
// --------------------------------------------------------------------------
var cvalues = require('./modules/cvalues-manager');

exports.show443 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			cvalues.getFPcvalues(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/443', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show443NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			cvalues.getNPcvalues(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew443 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.addnewcvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update443 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.updatecvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete443 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.deletecvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Policies
// --------------------------------------------------------------------------
var policies = require('./modules/policies-manager');

exports.show444 = function(req, res) {
	policies.getallpolicies(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/444', {curruser : req.workhalf.user, formdata : null });
		else  {
			res.render('employee/444', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.addnew444 = function(req, res) {
	policies.addnewpolicies({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		policyid : req.param('pid'),
		policytitle : req.param('ptitle'),
		fupdlink : req.param('fupd'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
exports.update444 = function(req, res) {
	policies.updatepolicies({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		policyid : req.param('pid'),
		policytitle : req.param('ptitle'),
		fupdlink : req.param('fupd'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
exports.delete444 = function(req, res) {
	policies.deletepolicies({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		policyid : req.param('pid'),
		policytitle : req.param('ptitle'),
		fupdlink : req.param('fupd'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
// --------------------------------------------------------------------------
// Company Committees
// --------------------------------------------------------------------------
var committees = require('./modules/committees-manager');

exports.show445 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			committees.getFPcommittees(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/445', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,membdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show445NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			committees.getNPcommittees(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew445 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.addnewcommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update445 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.updatecommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete445 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.deletecommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Active Resignations in SKIP team
// --------------------------------------------------------------------------

exports.show446 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/446', {curruser : req.workhalf.user, repdata : JSON.stringify(data1)});
		}
	);
};
exports.search446 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.exitsbymgr(req.workhalf.user.coid, req.param('rep'), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			 exitacts.getallexitacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance goals
// --------------------------------------------------------------------------

var perfgoals = require('./modules/perfgoals-manager');

exports.show447 = function(req, res) {
	var data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			users.getrolesbymgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/447', {curruser : req.workhalf.user, kradata : JSON.stringify(data1), roledata : JSON.stringify(data2), subroledata : JSON.stringify(data3), totpages:tot});
		}
	);
};

exports.search447 = function(req, res) {
	var data0,  data1, data2, data6;
	var resp = new Object;
	var tot;
	async.series([
		function(callback) {
			perfgoals.getperfgoalsbyrolekra({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid,roleid: req.param('role'),kraid: req.param('kra')}, function(e, o, t) {
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e2, o2) {
				if (o2)		{
					data2 = o2;
				} else	{
					data2 = null;
				}
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show447NP = function(req, res) {
	var data0,  data1, data2;
	async.series([
		function(callback) {
			perfgoals.getperfgoalsbyrole({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid,roleid: req.param('role')}, function(e, o, t) {
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e2, o2) {
				if (o2)		{
					data2 = o2;
				} else	{
					data2 = null;
				}
				callback(e2,o2);
			});
		},
		function(callback) {
			kra.getkraarray(req.workhalf.user.coid, data2[0].kras, function(e6, o6) {
				if (o6)
					resp.data1 = o6;
				else
					resp.data1 = null;
				callback(e6,o6);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew447 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.addnewperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update447 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.updateperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete447 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.deleteperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Departmental Inductions
// --------------------------------------------------------------------------
var induction = require('./modules/induction-manager');

exports.show448 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			induction.getallinduction(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/448', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), deptdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show448NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			cvalues.getNPcvalues(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

// --------------------------------------------------------------------------
// Team members
// --------------------------------------------------------------------------

exports.show436 = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/436', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3)});
		}
	);
};

// --------------------------------------------------------------------------
// SKIP team members
// --------------------------------------------------------------------------

exports.show437 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/437', {curruser : req.workhalf.user, repdata : JSON.stringify(data1)});
		}
	);
};
exports.search437 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.param("rep"), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};


// --------------------------------------------------------------------------
// Roles defined for each department
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show451 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/451', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search451 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.getrolesbydept(req.workhalf.user.coid,req.workhalf.user.userid, req.param('dept'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			resp.mhsdata = data2;
			resp.nhsdata = data2;
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// KRA defined for each role
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show452 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/452', {curruser : req.workhalf.user, roledata : JSON.stringify(data1)});
		}
	);
};

exports.search452 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;

	var roleid = '';
	var kraids = [];

	async.series([
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e0, o0) {
				if (o0)		{
					data0 = o0;
				} else	{
					data0 = null;
				}
				callback(e0,o0);
			});
		},
		function(callback) {
			kra.getkraarray(req.workhalf.user.coid, data0[0].kras, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			resp.data = data1;
			resp.dept = data2;
			res.json(resp);
		}
	);
};


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------

exports.show449 = function(req, res) {
	var indcObj = new Object;
  indcObj.coid = req.query.coid;
  indcObj.indc = req.query.indcid;
  indcObj.fname = req.query.fname;
  indcObj.title = req.query.title;

	res.render('employee/449', {curruser : req.workhalf.user, indcdata:JSON.stringify(indcObj)});
};

var deptindcq = require('./modules/deptindcq-manager');

exports.exam449 = function(req, res) {
	var data0,  data1, data2;
	var tot, Qs;
	async.parallel([
		function(callback) {
			deptindcq.getquesByIndc(req.query.coid, req.workhalf.user.userid, req.query.indc, function(e,o)	{
				if (o)		{
					data0 = o;
					Qs = o.length;
				}	else		{
					data0 = null;
					Qs = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/449exam', {curruser : req.workhalf.user, formdata : JSON.stringify(data0),indcid:req.query.indc, totqs:Qs});
		}
	);
};

exports.save449 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.savedeptindc({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				indc : req.param('indc'),
				indcans: req.param('ansData'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = true;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team Profile status Report
// --------------------------------------------------------------------------

exports.show453 = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/453', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3)});
		}
	);
};

// --------------------------------------------------------------------------
// SKIP team Profile report
// --------------------------------------------------------------------------

exports.show454 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('employee/454', {curruser : req.workhalf.user, repdata : JSON.stringify(data1)});
		}
	);
};
exports.search454 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.param("rep"), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Induction Results Report
// --------------------------------------------------------------------------

var deptindcq = require('./modules/deptindcq-manager');

exports.show455 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			induction.getallinduction(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('employee/455', {curruser : req.workhalf.user, usrdata : JSON.stringify(data1) ,indcdata : JSON.stringify(data2), totpages:tot});
		}
	);
};

exports.search455 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersById(req.workhalf.user.coid, req.param("usr"), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Self Onboarding Status
// --------------------------------------------------------------------------

exports.show457 = function(req, res) {
	async.parallel([
		function(callback) {
			users.getoneusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)		{
					data0 = o;
				}	else		{
					data0 = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/457', {curruser : req.workhalf.user, formdata : JSON.stringify(data0)});
		}
	);
};

// --------------------------------------------------------------------------
// Team Onboarding Status
// --------------------------------------------------------------------------

exports.show458 = function(req, res) {
	users.getusersByMgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/458', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('employee/458', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search458 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getoneusers(req.workhalf.user.coid, parseInt(req.param('emp')), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------
var blogs = require('./modules/blogs-manager');

exports.show460 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			blogs.getallblogs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('employee/460', {curruser : req.workhalf.user, formdata : JSON.stringify(data0)});
		}
	);
};

exports.show460a = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			//console.log(req.query.blog);
			blogs.getoneblog(req.workhalf.user.coid, req.workhalf.user.userid, req.query.blog, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			//console.log(data0);
			res.render('employee/460a', {curruser : req.workhalf.user, formdata : JSON.stringify(data0)});
		}
	);
};

// --------------------------------------------------------------------------
// Notifications
// --------------------------------------------------------------------------
exports.show461 = function(req, res) {
	users.getnotifs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('employee/461', {curruser : req.workhalf.user, formdata : null });
		else  {
		  //if (o.length > 0)
		    //req.workhalf.user.notiftot = o.length;
		 //else
		    req.workhalf.user.notiftot = '';
			res.render('employee/461', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

exports.set461 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.updnotifs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				mailid : req.param('mailid'),
				mesg : req.param('mesg'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.tot = '';
				} else		{
					resp.err = true;
					resp.data = null;
					resp.tot = '';
				}
				callback(e,o);
				});
		  },
		function(callback) {
	    users.getnotiftot(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)		{
				  //console.log(o);
    		  req.workhalf.user.notiftot = o;
					resp.tot = o;
				}
				callback(e,o);
				});
		  },
	  ], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance Progress
// --------------------------------------------------------------------------
var apprsl = require('./modules/apprsl-manager');

exports.show462 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All appraisal period List
		function(callback) {
			apprsl.getactiveapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)
					data1 = o;
				else
					data1 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallemployees(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data2 = o1;
				else
					data2 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/462', {curruser : req.workhalf.user, aprsldata : JSON.stringify(data1),empdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3)});
		}
	);
};

exports.self462 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.searchselfdata({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.othr462 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.searchothrdata({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.assign462 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.updateothers({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
				peerid : req.param('peerid'),
				teamid : req.param('teamid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		function(callback) {
      var tstr = "here";
      var lstr = tstr.link("/462");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('teamid'),
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has selected you to be Peer appraiser. Do the Peer rating "+lstr,
        sflag  : false,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(callback) {
      var tstr = "here";
      var lstr = tstr.link("/462");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('peerid'),
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has selected you to be Team member appraiser. Do the Peer rating "+lstr,
        sflag  : false,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.srtng462 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updateself({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.prtng462 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updatepeer({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.trtng462 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updateteam({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team member appraisal by supervisor
// --------------------------------------------------------------------------

exports.show463 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All appraisal period List
		function(callback) {
			apprsl.getactiveapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/463', {curruser : req.workhalf.user, aprsldata : JSON.stringify(data1), rleveldata : JSON.stringify(data2), usrdata : JSON.stringify(data3)});
		}
	);
};

exports.supr463 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.searchsuprdata({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.srtng463 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updatessupr({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team member appraisal by SKIP supervisor
// --------------------------------------------------------------------------

exports.show464 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All appraisal period List
		function(callback) {
			apprsl.getactiveapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/464', {curruser : req.workhalf.user, aprsldata : JSON.stringify(data1), rleveldata : JSON.stringify(data2), repdata : JSON.stringify(data3)});
		}
	);
};

exports.skiprs464 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.param('drep'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.skip464 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.searchskipdata({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.srtng464 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updatesskip({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------
exports.show465 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data0 = null;
				callback(e0,o0);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('employee/465', {curruser : req.workhalf.user, kradata : JSON.stringify(data0), rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3)});
		}
	);
};

exports.search465 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rpid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------
exports.show466 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data0 = null;
				callback(e0,o0);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.render('employee/466', {curruser : req.workhalf.user, kradata : JSON.stringify(data0), rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), usrdata : JSON.stringify(data4)});
		}
	);
};

exports.search466 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
				userid : parseInt(req.param('usr')),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skip Reports Performance history
// --------------------------------------------------------------------------

exports.show467 = function(req, res) {
	var data0, data1, data2, data3, data4;

	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data0 = null;
				callback(e0,o0);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.render('employee/467', {curruser : req.workhalf.user, kradata : JSON.stringify(data0), rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), repdata : JSON.stringify(data4)});
		}
	);
};

exports.skips467 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getteammembers(req.workhalf.user.coid, req.param('usr'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search467 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


