/* -----------------------------------------------
usertype 1 = site admin
usertype 2 = Site users
usertype 3 = Company admins
usertype 4 = Company users
usertype 5 = Candidates
------------------------------------------------- */

var USER = require('./modules/user-manager');
var CT = require('./modules/country-list');
var LIC = require('./modules/licenses-manager');
var jobs = require('./modules/jobs-manager');
var department = require('./modules/department-manager');
var kra = require('./modules/kra-manager');
var skills = require('./modules/skills-manager');

exports.index = function(req, res) {
	if (!req.plughrjobs.user)  {
   	res.render('homepage');
   	//res.render('login');
  } else    {
		if (req.plughrjobs.user.usertype == 1)		{
			res.redirect('101');
		} else if (req.plughrjobs.user.usertype == 2)		{
			res.redirect('201');
		} else if (req.plughrjobs.user.usertype == 3)		{
			res.redirect('301');
		} else if (req.plughrjobs.user.usertype == 4)		{
			res.redirect('401');
		} else if (req.plughrjobs.user.usertype == 5)		{
			res.redirect('501');
		} else	{
			res.redirect('homepage');
		}
	}
};

exports.newUser = function(req, res) {
	// candidates join //
   
	USER.newUser({
		user 	      : req.param('email'),
		name        : req.param('username'),
	  password    : req.param('password'),
		mobile      : req.param('usermobile'),
	  usertype    : req.param('usertype')
	}, function(e,o)    {
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.login = function(req, res) {
  USER.manualLogin(req.param('userid'), req.param('pwd'), function(e, o) {
	  var rObject = new Object();
    if (o.err)   {
	    //console.log(o.err+" -- "+ o.text);
	    res.json(o);
    }	else	{
      req.plughrjobs.user = o;
      rObject.err = false;
      if (o.usertype == 1)		{						// Workhalf SiteAdmin
	      rObject.redirect = '101';
	      res.json(rObject);
      } else if (o.usertype == 2)		{     // Workhalf Site User
	      rObject.redirect = '201';
	      res.json(rObject);
      } else if (o.usertype == 3)		{     // Employer Admin
	      rObject.redirect = '301';
		    res.json(rObject);
      } else if (o.usertype == 4)		{     // Employer User
	      rObject.redirect = '401';
		    res.json(rObject);
      } else if (o.usertype == 5)		{     // Candidates
	      rObject.redirect = '501';
		    res.json(rObject);
      } else	{
       rObject.err = true;
       rObject.text = "Login failed";
	     res.json(rObject);
      }
    }
	});
};

exports.resetpwd = function(req, res) {
	USER.resetpwd(req.param('emailid'), function(e, o)		{
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.logout = function(req, res) {
	req.plughrjobs.reset();
	var rObject = new Object();
	rObject.status = 200;
	rObject.redirect = '/';
	res.send(rObject);
};

exports.nosession = function(req, res) {
 	res.render('nosession');
};

exports.features = function(req, res) {
 	res.render('features');
};

exports.pricing = function(req, res) {
 	res.render('pricing');
};

exports.showverifycode = function(req, res) {
 	res.render('coverify');
};

exports.showresendverid = function(req, res) {
 	res.render('resendverid');
};

exports.csignup = function(req, res) {
 	res.render('csignup');
};

exports.esignup = function(req, res) {
 	res.render('esignup');
};

exports.signupok = function(req, res) {
 	res.render('company/signupok');
};

exports.forgotpwd = function(req, res) {
 	res.render('forgotpwd');
};

exports.showupdpwd = function(req, res) {
 	res.render('updpwd');
};

exports.updpwd = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			USER.golivepwd(req.param('coid'),req.param('userid'), req.param('npass'), function(e, o)		{
				if (e)    {
					resp.err1 = true;
					resp.text = o.text;
				}	else	{
					//console.log(o.err+" -- "+ o.text);
					resp.err1 = false;
					resp.text = o.text;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
		  res.json(resp);
		}
	);
};

exports.jsearch = function(req, res) {
	var data0;
	async.parallel([
		function(callback) {
			jobs.searchjobs('','india','', function(e, o)		{
				if (e)    {
					data0 = null;
				}	else	{
					data0 = o;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
		  res.render('jsearch', {curruser : JSON.stringify(req.plughrjobs.user), formdata : JSON.stringify(data0)});
		  //res.json(resp);
		}
	);
};

exports.searchjobs = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			jobs.searchjobs(req.param('stext'),req.param('jobcntry'), req.param('jobcity'), function(e, o)		{
				if (e)    {
					resp.err1 = true;
					resp.data = null;
				}	else	{
					resp.err1 = false;
					resp.data = o;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
		  res.json(resp);
		}
	);
};

exports.showjob = function(req, res) {
  if (!req.query.jobid)   {
   	res.render('homepage');
  } else    {
	  async.parallel([
		  /* function(callback) {
			  department.getalldepartment(null, function(e, o, t) {
				  if (o)
					  data0 = o;
				  else
					  data0 = null;
				  callback(e,o);
			  });
		  },
		  function(callback) {
			  kra.getallkra(null, function(e1, o1) {
				  if (o1)
					  data1 = o1;
				  else
					  data1 = null;
				  callback(e1,o1);
			  });
		  },
		  function(callback) {
			  skills.getallskills(null, function(e3, o3) {
				  if (o3)
					  data3 = o3;
				  else
					  data3 = null;
				  callback(e3,o3);
			  });
		  }, */
		  function(callback) {
    	  jobs.getjobbyid(req.query.jobid, function(e2, o2)		 {
				  if (o2)
					  data2 = o2;
				  else
					  data2 = null;
				  callback(e2,o2);
			  });
		  }
		  ], function(err, results) {
			  res.render('jobdetails', {curruser : JSON.stringify(req.plughrjobs.user), formdata : JSON.stringify(data2)});
		  }
	  );
 	}
};


