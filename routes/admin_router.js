/*
 * plugHRapp Homepage functionality
*/
/*
   0. Site Admin
   1. Site Users 
   2. Business Owner / HR Admin
   3. Payroll Admin
   4. Employees
*/

var ADMIN = require('./modules/admin-manager');
var CT = require('./modules/country-list');
var async 		= require('async');
var EM        = require('./modules/email-dispatcher');

exports.showlogin = function(req, res) {
 	res.render('admin/login');
};

exports.login = function(req, res) {
  ADMIN.manualLogin(req.param('userid'), req.param('pwd'), function(e, o) {
    if (e)   {
      console.log(o.err+" -- "+ o.text);
      res.json(o);
    }	else	{
      req.plugHRapp.user = o;
      var rObject = new Object();
      rObject.err = false;
      if (o.usertype == 0)		{						// EQP SiteAdmin
	      rObject.redirect = 'a001';
	      res.json(rObject);
      } else if (o.usertype == 1)		{     // EQP SiteStaff
	      rObject.redirect = '101';
	      res.json(rObject);
      } else	{
       console.log("login failed");
       console.log(e);
       rObject.err = true;
       rObject.text = "Login failed";
	     res.json(rObject);
      }
    }
	});
};

exports.index = function(req, res) {
  res.render('admin/a001',{curruser : req.plugHRapp.user});
};

exports.show_upd_password = function(req, res) {
	res.render('admin/a003', {curruser : req.plugHRapp.user});
};

exports.upd_password = function(req, res) {
	ADMIN.updatePassword(req.plugHRapp.user.userid , req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			res.json(o);
		}
	});
};

exports.logout = function(req, res) {
	req.plugHRapp.reset();
	var rObject = new Object();
	rObject.status = 200;
	rObject.redirect = 'admin';
	res.send(rObject);
};

exports.nosession = function(req, res) {
 	res.render('nosession');
};

// --------------------------------------------------------------------------
// Manage Site ADmins
// --------------------------------------------------------------------------
//var users 		= require('./modules/user-manager');

exports.showa002 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			ADMIN.getFPstadmins(req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admin/a002', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0)});
		}
	);
};
exports.showa002NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			ADMIN.getNPstadmins(req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnewa002 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			ADMIN.addnewstadmins({
				userid : req.plugHRapp.user.userid,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.updatea002 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ADMIN.updatestadmins({
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.deletea002 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			ADMIN.deletestadmins({
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
var preCompany = require('./modules/preCompany-manager');

exports.showa005 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			preCompany.getFPpreCompany(req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admin/a005', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.showa005NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			preCompany.getNPpreCompany(req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
// --------------------------------------------------------------------------
// Clients lists
// --------------------------------------------------------------------------
var company = require('./modules/company-manager');

exports.showa006 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			company.getallcompany(req.plugHRapp.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('admin/a006', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0)});
		}
	);
};


