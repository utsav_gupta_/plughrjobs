// Load the route handlers
var home = require('./home_router');
var company = require('./company_router');
var employer = require('./employer_router');
var owner = require('./employer_router');
var candidate = require('./candidate_router');
var admin = require('./admin_router');
var admusr = require('./admusr_router');

module.exports = function(app) {
	// Define the routes
  app.get('/', home.index);
  //app.get('/nosession', home.nosession);

  app.get('/jsearch',home.jsearch);
  app.post('/jsearch',home.searchjobs);
  app.get('/jobpage',home.showjob);
  app.get('/csignup',home.csignup);
  app.get('/esignup',home.esignup);
  app.get('/signupok',home.signupok);
  app.post('/userjoin',home.newUser); 
  app.post('/companyjoin',company.newCompany); 
	app.get('/emailverify', company.coverify);
	app.get('/verifycode', company.verifycode);
	app.get('/resendverid', company.resendverid);

  app.get('/login',home.login);
	app.get('/logout', home.logout);

	app.get('/301', IsLoggedIn, HasLicense, HasRights, employer.index);
	app.get('/302', IsLoggedIn, HasLicense, HasRights, employer.show_upd_pprofile);
	app.post('/302', IsLoggedIn, HasLicense, HasRights, employer.upd_pprofile);

	app.get('/303', IsLoggedIn, HasLicense, HasRights, employer.show_upd_cprofile);
	app.post('/303', IsLoggedIn, HasLicense, HasRights, employer.upd_cprofile);
  app.get('/complogo', IsLoggedIn, HasLicense, HasRights, employer.showclogo);
	app.post('/complogo', IsLoggedIn, HasLicense, HasRights, employer.uploadclogo);

	app.get('/304', IsLoggedIn, HasLicense, HasRights, employer.show_upd_password);
	app.post('/304', IsLoggedIn, HasLicense, HasRights, employer.upd_password);

	app.get('/305', IsLoggedIn, HasLicense, HasRights, employer.show305);
	app.get('/305newpage', IsLoggedIn, HasLicense, HasRights, employer.show305NP);
	app.post('/305add', IsLoggedIn, HasLicense, HasRights,  employer.addnew305);
	app.post('/305upd', IsLoggedIn, HasLicense, HasRights,  employer.update305);
	app.delete('/305', IsLoggedIn, HasLicense, HasRights,  employer.delete305);

	app.get('/501', IsLoggedIn, HasLicense, HasRights, candidate.index);
  app.get('/501jsearch', IsLoggedIn, HasLicense, HasRights, candidate.jsearch);
  app.post('/501jsearch', IsLoggedIn, HasLicense, HasRights, candidate.searchjobs);
  app.get('/501jobpage', IsLoggedIn, HasLicense, HasRights, candidate.showjob);

	app.get('/502', IsLoggedIn, HasLicense, HasRights, candidate.show502);
	app.post('/502', IsLoggedIn, HasLicense, HasRights, candidate.update502);

	app.get('/503', IsLoggedIn, HasLicense, HasRights, candidate.show_upd_password);
	app.post('/503', IsLoggedIn, HasLicense, HasRights, candidate.upd_password);

	// Page Not Found - THIS SHOULD ALWAYS BE LAST //
  //app.get('*', home.nopage);
	app.get('*', function(req, res) { res.render('page404', {curruser : "" })});
};

function IsLoggedIn(req, res, next) {
  if (req.plughrjobs.user) {
    next();
  } else {
    req.plughrjobs.error = 'Access denied!';
    res.redirect('/');
  }
}

function HasLicense(req, res, next) {
  if (req.plughrjobs.user) {
    next();
  } else {
    req.plughrjobs.error = 'Access denied!';
    res.redirect('/');
  }
}

function HasRights(req, res, next) {
  if (req.plughrjobs.user) {
    next();
  } else {
    req.plughrjobs.error = 'Access denied!';
    res.redirect('/');
  }
}

