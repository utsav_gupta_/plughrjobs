/*
 * workhalf Homepage functionality
*/

/*
   0. Site Admin
   1. Site Users 
   2. Business Owner
   3. HR Admin
   4. Employees
   5. Consultants
*/

var users 		= require('./modules/user-manager');
var CT 				= require('./modules/country-list');
var async 		= require('async');
var EM        = require('./modules/email-dispatcher');
var LIC 			= require('./modules/licenses-manager');
var department = require('./modules/department-manager');
var kra = require('./modules/kra-manager');
var jobs = require('./modules/jobs-manager');

//---------------------------------------------
// For File uploads 
//---------------------------------------------
var formidable = require('formidable')
var fs = require('fs');
var sys = require('sys');
var AWS = require('aws-sdk');

var mkdirp = require('mkdirp');
var format = require('util').format;

//Directory to upload file
var uploadPath="uploads/";
var userpictpath="uploads/userpics/";
//---------------------------------------------
/// Include ImageMagick
//---------------------------------------------
var im = require('imagemagick');
//---------------------------------------------

var accessKeyId =  process.env.AWS_ACCESS_KEY || "AKIAJGTOOVCWVATAEXFA";
var secretAccessKey = process.env.AWS_SECRET_KEY || "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";
var s3__bucket = "plughr-uploads";

var events = require('./modules/events-manager');

exports.index = function(req, res) {
  res.render('employer/301',{curruser : req.plughrjobs.user, formdata: JSON.stringify(req.plughrjobs.user)});
};

// --------------------------------------------------------------------------
// Admin user personal profile
// --------------------------------------------------------------------------
exports.show_upd_pprofile = function(req, res) {
	res.render('employer/302', {curruser : req.plughrjobs.user, formdata: JSON.stringify(req.plughrjobs.user)});
};

exports.upd_pprofile = function(req, res) {
	users.updatepprofile(req.plughrjobs.user.user, req.param('fname'), req.param('mobile'), req.param('desgn'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			req.plughrjobs.user = o.text;
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Company profile
// --------------------------------------------------------------------------
exports.show_upd_cprofile = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			company.getonecompany(req.plughrjobs.user.user, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
    	res.render('employer/303', {curruser : req.plughrjobs.user, formdata: JSON.stringify(data0)});
		}
	);
};

exports.showclogo = function(req, res) {
	var xcoid = req.plughrjobs.user.coid;
	var xuserid = req.plughrjobs.user.userid;
  var tempFile = '/uploads/'+ "plughrjobs" + "/" + req.param("filename");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};

exports.uploadclogo = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
	var resp = new Object;
	var data0,  data1, data2;
	mkdirp('./enttmp', function(err) {
		if (!err)		{
		  form.parse(req, function(err,fields, files) {
		    if (err)    {
		      res.json({err:1,text:"File upload failed - please retry"});
		    } else	{
				  if (!files.docfile)     {
						async.parallel([
							function(callback) {
	              company.updatecompany(
	                req.plughrjobs.user.user, 
							    fields.ucoid, 
	                fields.company, 
	                fields.industry, 
	                fields.addr,
	                fields.city,
	                fields.pincode,
	                fields.linkedin,
	                fields.about,
	                fields.values,
	                ''
                , function(e, o)		{
							  }, function(e1,o1)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.logofile = logofile;
										resp.totpages = 0;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.logofile = logofile;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
				  }  else    {
					  var filename = files.docfile.name;
						var extensionAllowed=[".jpg",".jpeg",".gif",".png"];
					  var maxSizeOfFile=2500;
					  var i = filename.lastIndexOf('.');
	
						var file_extension= (i < 0) ? '' : filename.substr(i);
						if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1024 ) < maxSizeOfFile))   {
							AWS.config.update({
									accessKeyId: accessKeyId,
									secretAccessKey: secretAccessKey
							});
							var randm = Math.random().toString(36).slice(-16);

							//var strKey = fields.ucoid + "/" + randm + file_extension;
							var logofile = fields.userid + file_extension;
					    var strKey = "plughrjobs/" + logofile;
							//console.log(strKey);

							var path = files.docfile.path;
							fs.readFile(path, function(err, file_buffer)		{
								var params = {
										Bucket: s3__bucket,
										Key: strKey,
									  Body: file_buffer,
										ACL: 'public-read'
								};
								var s3 = new AWS.S3();
								async.series([
									function(callback) {
										s3.putObject(params, function (e, o) {
											if (e)		{
												resp.err = 2;
												resp.text = "Logo NOT uploaded";
											} else		{
												resp.err = 0;
												resp.text = "Logo uploaded";
											}
											callback(e,o);
										});
									},
									function(callback) {
	                  company.updatecompany(
	                    req.plughrjobs.user.user, 
	                    fields.company, 
	                    fields.industry, 
	                    fields.addr,
	                    fields.city,
	                    fields.pincode,
	                    fields.linkedin,
	                    fields.about,
	                    fields.values,
	                    logofile
                    , function(e1, o1)		{
											if (o1)		{
												resp.err = false;
												resp.data = o1;
												resp.logofile = logofile;
												resp.totpages = 0;
											} else		{
												resp.err = true;
												resp.data = null;
												resp.logofile = logofile;
												resp.totpages = 0;
											}
											callback(e1,o1);
										});
									},
									], function(err, results) {
										res.json(resp);
									}
								);
							});
						} else  {
						  res.json({err:4,text:"Invalid file type OR size must be less than 2MB"});
						}
				  }
				}
		  });
		} else	{
		  //console.error('file upload error captured');
		  //console.error(e);
		  res.json({err:5,text:"File upload failed - please retry"});
		}
  });
};

exports.upd_cprofile = function(req, res) {
	company.updatecompany(req.plughrjobs.user.user, req.param('company'), req.param('industry'), req.param('addr'),req.param('city'),req.param('pincode'),req.param('linkedin'),req.param('about'),req.param('values'),req.param('clink'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Admin Password
// --------------------------------------------------------------------------
exports.show_upd_password = function(req, res) {
	res.render('employer/304', {cologo:req.plughrjobs.user.clogo, curruser : req.plughrjobs.user});
};

exports.upd_password = function(req, res) {
	users.updatePassword(req.plughrjobs.user.user , req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Manage job postings
// --------------------------------------------------------------------------
exports.show305 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		/* function(callback) {
			department.getalldepartment(req.plughrjobs.user.user, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			kra.getallkra(req.plughrjobs.user.user, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.plughrjobs.user.user, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		}, */
		function(callback) {
			jobs.getFPjobs(req.plughrjobs.user.user, function(e2, o2, t) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				tot = t;
				callback(e2,o2);
			});
		}
		], function(err, results) {
			res.render('employer/305', {curruser : req.plughrjobs.user, formdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show305NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			jobs.getNPjobs(req.plughrjobs.user.user, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew305 = function(req, res) {
	var data0,  data1, data2, tot;
	var resp = new Object;
	async.series([
		function(callback) {
			jobs.addnewjobs({
				companyadmin : req.plughrjobs.user.user,
				company : req.plughrjobs.user.company,
				title : req.param('title'),
				jdesc : req.param('jdesc'),
				jobcntry : req.param('jobcntry'),
				jobcity : req.param('jobcity'),
				locale : req.param('locale'),
				emailid : req.param('emailid'),
				workdays : req.param('workdays'),
				expr : req.param('expr'),
				skills : req.param('skills'),
				comp : req.param('comp'),
				curr : req.param('curr'),
				eqtymin : req.param('eqtymin'),
				eqtymax : req.param('eqtymax'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.text = 'Job posted successfully';
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.text = 'Error in posting job';
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.update305 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			jobs.updatejobs({
				dbid : req.param('dbid'),
				companyadmin : req.plughrjobs.user.user,
				company : req.plughrjobs.user.company,
				title : req.param('title'),
				jdesc : req.param('jdesc'),
				jobcntry : req.param('jobcntry'),
				jobcity : req.param('jobcity'),
				locale : req.param('locale'),
				emailid : req.param('emailid'),
				workdays : req.param('workdays'),
				expr : req.param('expr'),
				skills : req.param('skills'),
				comp : req.param('comp'),
				curr : req.param('curr'),
				eqtymin : req.param('eqtymin'),
				eqtymax : req.param('eqtymax'),

			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.text = 'Job updated successfully';
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.text = 'Error in updating job';
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete305 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			jobs.deletejobs({
				dbid : req.param('dbid'),
				companyadmin : req.plughrjobs.user.user,
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			users.getusercount(req.plughrjobs.user.coid, req.plughrjobs.user.userid, function(e1, o1) {
				if (o1)
					tot = o1;
				else
					tot = 0;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			if (tot < req.plughrjobs.user.licusers)	{
				resp.licv = true;
			}	else		{
				resp.licv = false;
			}
			resp.totusers = tot;
			resp.totlic = req.plughrjobs.user.licusers;
			res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// Manage Admins
// --------------------------------------------------------------------------
exports.show202 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getallhradmins(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getusercount(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					tot = o1;
				else
					tot = 0;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			if (tot < req.workhalf.user.licusers)
				var licvalid = true;
			else
				var licvalid = false;
			res.render('owner/202', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totusers:tot, totlic:req.workhalf.user.licusers, licv: licvalid});
		}
	);
};
exports.show202NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPhradmins(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew202 = function(req, res) {
	var data0,  data1, data2, tot;
	var resp = new Object;
	async.series([
		function(callback) {
			users.addnewhradmins({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
				});
		},
		function(callback) {
			users.getusercount(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					tot = o1;
				else
					tot = 0;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			if (tot < req.workhalf.user.licusers)	{
				resp.licv = true;
			}	else		{
				resp.licv = false;
			}
			resp.totusers = tot;
			resp.totlic = req.workhalf.user.licusers;
			res.json(resp);
		}
	);
};
exports.update202 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.updatehradmins({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete202 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.deletehradmins({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			users.getusercount(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					tot = o1;
				else
					tot = 0;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			if (tot < req.workhalf.user.licusers)	{
				resp.licv = true;
			}	else		{
				resp.licv = false;
			}
			resp.totusers = tot;
			resp.totlic = req.workhalf.user.licusers;
			res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company setup wizard
// --------------------------------------------------------------------------
exports.show224 = function(req, res) {
	res.render('owner/224', {curruser : req.workhalf.user, formdata : JSON.stringify(req.workhalf.user)});
};

exports.set224 = function(req, res) {
	var resp = new Object;
	if (req.workhalf.user.live)	{
		resp.err = true;
		resp.text = "Company already live...";
		//console.log("Company already live");
		res.send(resp);
	} else	{
		async.series([
			function(callback) {
				users.getallemployees(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
					if (e1)		{
						resp.err = true;
						emplist = null;
					}	else	{
						resp.err = false;
						resp.text = "Please create users before going live";
						emplist = o1;
					}
					//console.log(emplist.length);
					callback(e1,o1);
				});
			},
			function(callback) {
				if (!resp.err)			{
					users.updpwdflag(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
						if (e)
							resp.err = true;
						else
							resp.err = false;
						callback(e,o);
					});
				}
			},
			function(callback) {
				if (!resp.err)			{
					company.golive(req.workhalf.user.coid, function(e2,o2)    {
						if (o2)		{
							resp.err = false;
							req.workhalf.user.live = true;
						} else		{
							resp.err = true;
							req.workhalf.user.live = false;
						}
						callback(e2,o2);
					});
				}
			},
			function(callback) {
				if (!resp.err)			{
					async.forEach(emplist, function(user, callback) {
						if (user.useremailid && user.useremailid != "")		{
							EM.LiveEmployee({coid : req.workhalf.user.coid, userid : user.userid, username : user.username, useremailid : user.useremailid}, function(e3,o3)	{
								callback(e3,o3);
							});
						} else	{
							//console.log("no emailid for "+user.username);
							callback(false,false);
						}
					}, function(er1, res1) {
						//console.log("here");
						callback(er1,res1);
					});
				}
			},
			], function(err, results) {
				if (!resp.err)			{
					resp.text = "Company is live now....congratulations!";
					//console.log("Company is live now....congratulations!");
				}
				res.send(resp);
			}
		);
	}
};

exports.get224b1 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)		{
					if (o1.companyname && o1.aboutus && o1.hrdate && o1.clogo && o1.companyname != "" && o1.aboutus != "" && o1.hrdate != "" && o1.clogo != "" )	{
						resp.err1 = false;
						resp.sts1 = "Company data completed";
					}		else		{
						resp.err1 = true;
						resp.sts1 = "Company data not completed";
					}
					if (o1.lbmessage && o1.lbmessage != "" )	{
						resp.err2 = false;
						resp.sts2 = "Message to employees completed";
					}		else		{
						resp.err2 = true;
						resp.sts2 = "Message to employees not completed";
					}
					if (o1.indcfname && o1.indcfname != "" )	{
						resp.err7 = false;
						resp.sts7 = "Induction presentation uploaded";
					}		else		{
						resp.err7 = true;
						resp.sts7 = "Induction presentation not uploaded";
					}
				}	else  {
					resp.err1 = true;
					resp.sts1 = "Company info not available";
					resp.err2 = true;
					resp.sts2 = "Message to employees not completed";
					resp.err7 = true;
					resp.sts7 = "Induction presentation not uploaded";
				}
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)		{
					if (o3 > 0)	{
						resp.err3 = false;
						resp.sts3 = o3 + " office locations created";
					}		else		{
						resp.err3 = true;
						resp.sts3 = "Office locations NOT completed";
					}
				}	else  {
					resp.err3 = true;
					resp.sts3 = "Office location info not available";
				}
				callback(e3,o3);
			});
		},
		function(callback) {
			department.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)		{
					if (o4 > 0)	{
						resp.err4 = false;
						resp.sts4 = o4 + " departments created";
					}		else		{
						resp.err4 = true;
						resp.sts4 = "Departments NOT completed";
					}
				}	else  {
					resp.err4 = true;
					resp.sts4 = "Departments info not available";
				}
				callback(e4,o4);
			});
		},
		function(callback) {
			roles.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)		{
					if (o5 > 0)	{
						resp.err5 = false;
						resp.sts5 = o5 + " roles created";
					}		else		{
						resp.err5 = true;
						resp.sts5 = "Roles NOT completed";
					}
				}	else  {
					resp.err5 = true;
					resp.sts5 = "Roles info not available";
				}
				callback(e5,o5);
			});
		},
		function(callback) {
			cvalues.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e6, o6) {
				if (o6)		{
					if (o6 > 0)	{
						resp.err6 = false;
						resp.sts6 = o6 + " core values created";
					}		else		{
						resp.err6 = true;
						resp.sts6 = "Core values NOT completed";
					}
				}	else  {
					resp.err6 = true;
					resp.sts6 = "Core values info not available";
				}
				callback(e6,o6);
			});
		},
		function(callback) {
			iquestions.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e8, o8) {
				if (o8)		{
					if (o8 > 0)	{
						resp.err8 = false;
						resp.sts8 = o8 + " questions created";
					}		else		{
						resp.err8 = true;
						resp.sts8 = "Induction questions NOT completed";
					}
				}	else  {
					resp.err8 = true;
					resp.sts8 = "Induction questions info not available";
				}
				callback(e8,o8);
			});
		},
		function(callback) {
			holidaylist.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e9, o9) {
				if (o9)		{
					if (o9 > 0)	{
						resp.err9 = false;
						resp.sts9 = o9 + " Holidays created";
					}		else		{
						resp.err9 = true;
						resp.sts9 = "Holidays list NOT completed";
					}
				}	else  {
					resp.err9 = true;
					resp.sts9 = "holidays info not available";
				}
				callback(e9,o9);
			});
		},
		function(callback) {
			holidaypolicy.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e10, o10) {
				if (o10)		{
					if (o10 > 0)	{
						resp.err10 = false;
						resp.sts10 = o10 + " Holidays calender(s) created";
					}		else		{
						resp.err10 = true;
						resp.sts10 = "Holidays calender(s) NOT completed";
					}
				}	else  {
					resp.err10 = true;
					resp.sts10 = "holidays calenders not available";
				}
				callback(e10,o10);
			});
		},
		function(callback) {
			leavetypes.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e11, o11) {
				if (o11)		{
					if (o11 > 0)	{
						resp.err11 = false;
						resp.sts11 = o11 + " leave types created";
					}		else		{
						resp.err11 = true;
						resp.sts11 = "Leave types NOT completed";
					}
				}	else  {
					resp.err11 = true;
					resp.sts11 = "Leave types available";
				}
				callback(e11,o11);
			});
		},
		function(callback) {
			rlevel.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e12, o12) {
				if (o12)		{
					if (o12 > 0)	{
						resp.err12 = false;
						resp.sts12 = o12 + " performance ratings created";
					}		else		{
						resp.err12 = true;
						resp.sts12 = "Performance ratings not completed";
					}
				}	else  {
					resp.err12 = true;
					resp.sts12 = "Performance ratings not available";
				}
				callback(e12,o12);
			});
		},
		function(callback) {
			sklevel.getcount(req.workhalf.user.coid, req.workhalf.user.userid, function(e13, o13) {
				if (o13)		{
					if (o13 > 0)	{
						resp.err13 = false;
						resp.sts13 = o13 + " skill ratings created";
					}		else		{
						resp.err13 = true;
						resp.sts13 = "Skill ratings not completed";
					}
				}	else  {
					resp.err13 = true;
					resp.sts13 = "Skill ratings not available";
				}
				callback(e13,o13);
			});
		},
		function(callback) {
			users.getemployeescount(req.workhalf.user.coid, req.workhalf.user.userid, function(e14, o14) {
				if (o14)		{
					if (o14 > 0)	{
						resp.err14 = false;
						resp.sts14 = o14 + " user(s) created";
					}		else		{
						resp.err14 = true;
						resp.sts14 = "user(s) not completed";
					}
				}	else  {
					resp.err14 = true;
					resp.sts14 = "user(s) info not available";
				}
				callback(e14,o14);
			});
		},
		], function(err, results) {
			res.send(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Manpower Report
// --------------------------------------------------------------------------
exports.show225 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/225', {curruser : req.workhalf.user, locdata : JSON.stringify(data1), deptdata : JSON.stringify(data2)});
		}
	);
};
exports.search225 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByLoc({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, loc:req.param("loc"), etype:req.param("etype")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company information
// --------------------------------------------------------------------------
var company = require('./modules/company-manager');

exports.show208 = function(req, res) {
	company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/208', {curruser : req.workhalf.user});
		else  {
			res.render('owner/208', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company location information
// --------------------------------------------------------------------------
var locations = require('./modules/locations-manager');

exports.show209 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			locations.getFPlocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/209', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show209NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			locations.getNPlocations(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew209 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.addnewlocations({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				ptno : req.param('ptno'),
				se : req.param('se'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update209 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.updatelocations({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				ptno : req.param('ptno'),
				se : req.param('se'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete209 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.deletelocations({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				ptno : req.param('ptno'),
				se : req.param('se'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Department information
// --------------------------------------------------------------------------


exports.show210 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getFPdepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/210', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show210NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			department.getNPdepartment(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew210 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.addnewdepartment({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update210 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.updatedepartment({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete210 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.deletedepartment({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Leaderboard messages information
// --------------------------------------------------------------------------

//var company = require('./modules/company-manager');

exports.show211 = function(req, res) {
	company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/211', {curruser : req.workhalf.user});
		else  {
			res.render('owner/211', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.update211 = function(req, res) {
	company.updatelboard({
		coid : req.workhalf.user.coid,
		userid : req.workhalf.user.userid,
		lbmessage : req.param('mesg01'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// user skills rating
// --------------------------------------------------------------------------
var userskills = require('./modules/userskills-manager');

exports.show212 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/212', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/212', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search212 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.getskillsofuser(req.workhalf.user.coid, req.param('emp'), function(e1, o1) {
				if (o1)
					resp.data = o1;
				else
					resp.data = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.skilldata = o2;
				else
					resp.skilldata = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.skrdata = o3;
				else
					resp.skrdata = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.show212NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			userskills.getNPuserskills(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

// --------------------------------------------------------------------------
// Corporate Values
// --------------------------------------------------------------------------
var cvalues = require('./modules/cvalues-manager');

exports.show213 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			cvalues.getFPcvalues(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/213', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show213NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			cvalues.getNPcvalues(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew213 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.addnewcvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update213 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.updatecvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete213 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.deletecvalues({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Committees
// --------------------------------------------------------------------------
var committees = require('./modules/committees-manager');

exports.show214 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			committees.getFPcommittees(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/214', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,membdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show214NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			committees.getNPcommittees(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew214 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.addnewcommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update214 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.updatecommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete214 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.deletecommittees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Policies
// --------------------------------------------------------------------------
var policies = require('./modules/policies-manager');

exports.show216 = function(req, res) {
	policies.getallpolicies(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/216', {curruser : req.workhalf.user, formdata : null });
		else  {
			res.render('owner/216', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

exports.uploadpolcdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".jpg",".tif",".png"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1034 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/policy/" + files.docfile.name;

				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Document NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Document Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          policies.addnewpolicies({ coid: fields.ucoid, userid: fields.uuserid, policyid: fields.pid, policytitle : fields.ptitle, docfile : files.docfile.name}, function(e1,o1,tot)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.totpages = tot;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

function oc(a){
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}

exports.deletepolcDoc = function(req, res) {
	var s3 = new AWS.S3();
	var resp = new Object;

	async.series([
		function(callback) {
			policies.deletepolicies({coid: req.workhalf.user.coid, userid: req.workhalf.user.userid, dbid: req.param('dbid') }, function(e1,o1,tot)	{
				if (e1)		{
					resp.err = true;
					resp.text = "File NOT Deleted";
					resp.data = null;
				} else		{
					resp.err = false;
					resp.text = "File Deleted";
					resp.data = o1;
				}
				resp.totpages = tot;
				callback(e1,o1);
			});
		},
		function(callback) {
			s3.deleteObjects({
					Bucket: s3__bucket,
					Delete: {
							Objects: [
								 { Key: req.workhalf.user.coid + "/policy/" + req.param('filename')}
							]
					}
			}, function(err, data) {
				if (err)		{
					resp.fd = "File NOT Deleted";
				} else		{
					resp.fd = "File Deleted";
				}
				callback(err, data);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.showpolcdoc = function(req, res) {
	var xcoid = req.workhalf.user.coid;
	var xuserid = req.workhalf.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/policy/" + req.param("docfile");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Induction documents
// --------------------------------------------------------------------------

var induction = require('./modules/induction-manager');

exports.show215 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			induction.getFPinduction(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/215', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,deptdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show215NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			induction.getNPinduction(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.uploadIndcdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".ppt",".pptx"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1024 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/" + files.docfile.name;

				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Document NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Document Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          induction.addnewinduction({coid: fields.ucoid, userid:fields.uuserid, title : fields.title, itype : fields.itype, dept : fields.dept,	filename : files.docfile.name}, function(e1,o1,tot)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.totpages = tot;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

exports.deleteIndcdoc = function(req, res) {
	var s3 = new AWS.S3();
	var resp = new Object;

	async.series([
		function(callback) {
			induction.deleteinduction({coid: req.workhalf.user.coid, userid: req.workhalf.user.userid, dbid: req.param('dbid') }, function(e1,o1, tot)	{
				if (e1)		{
					resp.err = true;
					resp.text = "File NOT Deleted";
					resp.data = null;
				} else		{
					resp.err = false;
					resp.text = "File Deleted";
					resp.data = o1;
				}
				resp.totpages = tot;
				callback(e1,o1);
			});
		},
		function(callback) {
			s3.deleteObjects({
					Bucket: s3__bucket,
					Delete: {
							Objects: [
								 { Key: req.workhalf.user.coid + "/" + req.param('filename')}
							]
					}
			}, function(err, data) {
				if (err)		{
					resp.fd = "File NOT Deleted";
				} else		{
					resp.fd = "File Deleted";
				}
				callback(err, data);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.showIndcdoc = function(req, res) {
	var xcoid = req.workhalf.user.coid;
	var xuserid = req.workhalf.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/ " + req.param("filename");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};

exports.show215doc = function(req, res) 		{
  res.render('owner/215doc',{curruser : req.workhalf.user});
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Roles (by department)
// --------------------------------------------------------------------------
var roles = require('./modules/roles-manager');

exports.show217 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			roles.getFProles(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('owner/217', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), deptdata : JSON.stringify(data1), mhsdata : JSON.stringify(data2), kradata : JSON.stringify(data3), totpages:tot});
		}
	);
};
exports.show217NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			roles.getNProles(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew217 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	var roleid = '';
	var idlist = '';
	var kraids = '';

	async.series([
		function(callback) {
			if (req.param('newskill') != undefined)		{
				skills.addmanyskills({
					coid : req.workhalf.user.coid,
					userid : req.workhalf.user.userid,
					slist : req.param('newskill'),
				}, function(e, o, objs, ids )	{
					if (o)		{
						resp.err = false;
						resp.skills = objs;
						idlist = ids;
					} else		{
						idlist = null;
						resp.err = true;
						resp.skills = null;
					}
					callback(e,o);
				});
			} else	{
				callback(null,null);
			}
		},
		function(callback) {
			//console.log(roleid);
			if (req.param('newkra') != undefined)		{
				kra.addmanykras({
					coid : req.workhalf.user.coid,
					userid : req.workhalf.user.userid,
					//dept : req.param('dept'),
					//role : roleid,
					klist : req.param('newkra'),
				}, function(e,o,objs,ids)	{
					if (o)		{
						resp.kras = objs;
						kraids = ids;
						resp.err = false;
					} else		{
						resp.kras = null;
						kraids = null;
						resp.err = true;
					}
					callback(e,o);
				});
			} else	{
				callback(null,null);
			}
		},
		function(callback) {
			var sklist = [];
			if (req.param('skill') != undefined)
				sklist.push.apply(sklist,req.param('skill'));
			if (req.param('newskill') != undefined)		{
				if (idlist != undefined)
					sklist.push.apply(sklist,idlist);
			}
			//console.log(sklist);

			var kralist = [];
			if (req.param('kra') != undefined)
				kralist.push.apply(kralist,req.param('kra'));
			if (req.param('newkra') != undefined)		{
				if (kraids != undefined)
					kralist.push.apply(kralist,kraids);
			}
			//console.log(kralist);

			roles.addnewroles({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : sklist,
				kras : kralist,
			}, function(e,o, tot, rid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					roleid = rid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					roleid = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update217 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	var roleid = '';
	var idlist = '';
	var kraids = '';

	async.series([
		function(callback) {
			if (req.param('newskill') != undefined)		{
				skills.addmanyskills({
					coid : req.workhalf.user.coid,
					userid : req.workhalf.user.userid,
					slist : req.param('newskill'),
				}, function(e, o, objs, ids )	{
					if (o)		{
						resp.err = false;
						resp.skills = objs;
						idlist = ids;
					} else		{
						idlist = null;
						resp.err = true;
						resp.skills = null;
					}
					callback(e,o);
				});
			} else	{
				callback(null,null);
			}
		},
		function(callback) {
			//console.log(roleid);
			if (req.param('newkra') != undefined)		{
				kra.addmanykras({
					coid : req.workhalf.user.coid,
					userid : req.workhalf.user.userid,
					//dept : req.param('dept'),
					//role : roleid,
					klist : req.param('newkra'),
				}, function(e,o,objs,ids)	{
					if (o)		{
						resp.kras = objs;
						kraids = ids;
						resp.err = false;
					} else		{
						resp.kras = null;
						kraids = null;
						resp.err = true;
					}
					callback(e,o);
				});
			} else	{
				callback(null,null);
			}
		},
		function(callback) {
			var sklist = [];
			if (req.param('skill') != undefined)
				sklist.push.apply(sklist,req.param('skill'));
			if (req.param('newskill') != undefined)		{
				if (idlist != undefined)
					sklist.push.apply(sklist,idlist);
			}
			//console.log(sklist);

			var kralist = [];
			if (req.param('kra') != undefined)
				kralist.push.apply(kralist,req.param('kra'));
			if (req.param('newkra') != undefined)	{
				if (kraids != undefined)
					kralist.push.apply(kralist,kraids);
			}
			//console.log(kralist);

			roles.updateroles({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : sklist,
				kras : kralist,
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
}

exports.delete217 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.deleteroles({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : req.param('mhs'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Key REsult Areas (by department and role)
// --------------------------------------------------------------------------

exports.show218 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			kra.getFPkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/218', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,deptdata : JSON.stringify(data1) ,roledata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show218NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			kra.getNPkra(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew218 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.addnewkra({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				//dept : req.param('dept'),
				//role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update218 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.updatekra({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				//dept : req.param('dept'),
				//role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete218 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.deletekra({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				//dept : req.param('dept'),
				//role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skill Requirements by Dept + role
// --------------------------------------------------------------------------
var skills = require('./modules/skills-manager');

exports.show219 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			skills.getFPskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/219', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show219NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			skills.getNPskills(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew219 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.addnewskills({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update219 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.updateskills({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete219 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.deleteskills({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Employees Information
// --------------------------------------------------------------------------
exports.show220 = function(req, res) {
	var data0, data1, data2, data3, data4, data5, data6, data7;
	var tot,totu;
	async.parallel([
		function(callback) {
			users.getFPemployees(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		function(callback) {
			onbdacts.getallonbdacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e6, o6) {
				if (o6)
					data6 = o6;
				else
					data6 = null;
				callback(e6,o6);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e7, o7) {
				if (o7)
					data7 = o7;
				else
					data7 = null;
				callback(e7,o7);
			});
		},
		function(callback) {
			users.getusercount(req.workhalf.user.coid, req.workhalf.user.userid, function(e8, o8) {
				if (o8)
					totu = o8;
				else
					totu = 0;
				callback(e8,o8);
			});
		},
		], function(err, results) {
			if (totu < req.workhalf.user.licusers)
				var licvalid = true;
			else
				var licvalid = false;
			res.render('owner/220', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3), skilldata : JSON.stringify(data4), empdata : JSON.stringify(data5), onbdata : JSON.stringify(data6), skrdata : JSON.stringify(data7), totpages:tot, totusers:totu, totlic:req.workhalf.user.licusers, licv: licvalid});
		}
	);
};
exports.users220 = function(req, res) {
	var resp = new Object;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) 	{
				if (o5)
					resp.empdata = o5;
				else
					resp.empdata = null;
				callback(e5,o5);
			});
		},
		function(callback) {
			users.getusercount(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					resp.totusers = o4;
				else
					resp.totusers = 0;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			if (resp.totusers < req.workhalf.user.licusers)	{
				resp.licv = true;
			}	else		{
				resp.licv = false;
			}
			resp.totlic = req.workhalf.user.licusers;
			res.json(resp);
		}
	);
};
exports.show220NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPemployees(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew220 = function(req, res) {
	var data0,  data1, data2, data5, totu;
	var resp = new Object;
	async.series([
		function(callback) {
			users.addnewemployees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				live: req.workhalf.user.live,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				gender : req.param('gender'),
				dob : req.param('dob'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noasc : req.param('noasc'),
				wkns : req.param('wkns'),
				stng : req.param('stng'),
				onbdacts : req.param('onbdacts'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
			});
		},
		function(callback) {
			userskills.addnewuserskills({
				coid : req.workhalf.user.coid,
				userid : resp.userid,
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				skratings : req.param('skratings'),
				mgr : req.param('mgr'),
				updby : req.workhalf.user.userid,
				active: true
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o1.text;
				}
				callback(e2,o2);
			});
		},
		function(callback) {
			orgstru.addneworgstru({
				coid : req.workhalf.user.coid,
				userid : resp.userid,
				sdate : req.param('jdate'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				purpose : '1',				// 1= New User  2 = Transfer  3 = Reorg   4 = Promotion   5 = Demotion
				updby : req.workhalf.user.userid,
				upddate: new Date(),
			}, function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o1.text;
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.update220 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.updateemployees({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				live: req.workhalf.user.live,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				gender : req.param('gender'),
				dob : req.param('dob'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noasc : req.param('noasc'),
				wkns : req.param('wkns'),
				stng : req.param('stng'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.delete220 = function(req, res) {
	var data0,  data1, data2,totu;
	var resp = new Object;
	async.series([
		function(callback) {
			users.deleteemployees({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noasc : req.param('noasc'),
				wkns : req.param('wkns'),
				stng : req.param('stng'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			userskills.deleteuskillsbyuser({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
			}, function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o1.text;
				}
				callback(e1,o1);
				});
		},
		function(callback) {
			orgstru.deleteorgstru({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o2.text;
				}
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Holidays List
// --------------------------------------------------------------------------

var holidaylist = require('./modules/holidaylist-manager');

exports.show222 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			holidaylist.getFPholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/222', {curruser : req.workhalf.user, codata:JSON.stringify(data1), formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show222NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			holidaylist.getNPholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew222 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.addnewholidaylist({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update222 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.updateholidaylist({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete222 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.deleteholidaylist({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Holidays Policy
// --------------------------------------------------------------------------
var holidaypolicy = require('./modules/holidaypolicy-manager');

exports.show223 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			holidaypolicy.getFPholidaypolicy(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			holidaylist.getallholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/223', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,appofficedata : JSON.stringify(data1) ,selholidaysdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show223NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			holidaypolicy.getNPholidaypolicy(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			holidaylist.getallholidaylist(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew223 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.addnewholidaypolicy({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update223 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.updateholidaypolicy({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete223 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.deleteholidaypolicy({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Search Employees
// --------------------------------------------------------------------------

exports.show226 = function(req, res) {
	var data0,  data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data1 = null;
				callback(e0,o0);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('owner/226', {curruser : req.workhalf.user, skilldata : JSON.stringify(data0), depdata : JSON.stringify(data1), locdata : JSON.stringify(data2), roledata : JSON.stringify(data3), totpages:tot});
		}
	);
};
exports.show226user = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
  var uid = parseInt(req.query.userid);

	async.parallel([
		function(callback) {
		  users.getoneusers(req.query.coid, uid, function(e, o) {
				if (o)		{
					data1 = o;
					data1.coid = req.query.coid;
				} else
					data1 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.query.coid, uid, function(e1, o1) {
				if (o1)
					data2 = o1;
				else
					data2 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.query.coid, uid, function(e2, o2) {
				if (o2)
					data3 = o2;
				else
					data3 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.query.coid, uid, function(e3, o3) {
				if (o3)
					data4 = o3;
				else
					data4 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data5 = o4;
				else
					data5 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
	   	res.render('owner/226user', {formdata : JSON.stringify(data1), depdata : JSON.stringify(data2), roledata : JSON.stringify(data3), locdata : JSON.stringify(data4), skilldata : JSON.stringify(data5)});
		}
	);
};

exports.show226docs = function(req, res) {
	var data0, data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userdocs.getFPuserdocs(req.param('coid'), req.param('userid'), function(e, o, tot) {
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else	{
					resp.err = false;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search226 = function(req, res) {
	var data, data1, data2, data3;
	var resp = new Object;
	async.series([
		function(callback) {
			users.searchemp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				mgr : req.param('mgr'),
				role : req.param('role'),
				loc : req.param('loc'),
				dep : req.param('dep'),
				bgroup : req.param('bgrp'),
				pint : req.param('pint'),
				addr : req.param('addr'),
				univ : req.param('uni'),
				udegree : req.param('degree'),
				college : req.param('college'),
				orgn : req.param('emp'),
				skill : req.param('skill'),
			}, function(e,o, tot, searchObj)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
		      resp.sobj = searchObj;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
		      resp.sobj = searchObj;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.show226NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
		  users.searchNP(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), req.param('sobj'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// Company Induction Questions
// --------------------------------------------------------------------------
var iquestions = require('./modules/iquestions-manager');

exports.show229 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			iquestions.getFPiquestions(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/229', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show229NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			iquestions.getNPiquestions(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew229 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.addnewiquestions({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update229 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.updateiquestions({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete229 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.deleteiquestions({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Leave Types
// --------------------------------------------------------------------------

var leavetypes = require('./modules/leavetypes-manager');

exports.show230 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			leavetypes.getFPleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/230', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show230NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			leavetypes.getNPleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew230 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.addnewleavetypes({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				lname : req.param('lname'),
				lper : req.param('lper'),
				ldays : req.param('ldays'),
				lcriteria : req.param('lctr'),
				etype : req.param('etype'),
				puser : req.param('puser'),
        maxl : req.param('maxl'),
        negl : req.param('negl'),
				alimit : req.param('alimit'),
        maxtr : req.param('maxtr'),
        preh : req.param('preh'),
        inth : req.param('inth'),
        such : req.param('such'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update230 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.updateleavetypes({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				lper : req.param('lper'),
				ldays : req.param('ldays'),
				lcriteria : req.param('lctr'),
				etype : req.param('etype'),
				puser : req.param('puser'),
        maxl : req.param('maxl'),
        negl : req.param('negl'),
				alimit : req.param('alimit'),
        maxtr : req.param('maxtr'),
        preh : req.param('preh'),
        inth : req.param('inth'),
        such : req.param('such'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete230 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.deleteleavetypes({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				lcriteria : req.param('lctr'),
				lper : req.param('lper'),
				ldays : req.param('ldays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Organization Structure
// --------------------------------------------------------------------------

var orgstru = require('./modules/orgstru-manager');

exports.show221 = function(req, res) {
	var data0, data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			res.render('owner/221', {curruser : req.workhalf.user, mgrdata : JSON.stringify(data0), depdata : JSON.stringify(data1), locdata : JSON.stringify(data2), roledata : JSON.stringify(data3), skilldata : JSON.stringify(data4), skrdata : JSON.stringify(data5), totpages:tot});
		}
	);
};
exports.search221 = function(req, res) {
	var data0,  data1, data3;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.searchusers({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				uid : req.param('uid'),
				uname : req.param('uname'),
				mgr : req.param('mgr'),
				dep : req.param('dep'),
				loc : req.param('loc'),
			}, function(e,o, tot, searchObj)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.text = o.text;
					resp.totpages = tot;
		      resp.sobj = searchObj;
				} else		{
					resp.err = true;
					resp.text = o.text;
					resp.data = null;
					resp.totpages = 0;
		      resp.sobj = searchObj;
				}
				callback(e,o);
				});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			resp.departments = data1;
			resp.locations = data3;
			//console.log(resp.text);
			res.json(resp);
		}
	);
};
exports.show221NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
		  users.searchNP(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), req.param('sobj'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.save221 = function(req, res) {
	var data0, data1, data2;
	var resp = new Object;

	async.parallel([
		function(callback) {
			orgstru.updateorgstru({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				sdate : req.param('sdate'),
				updby : req.workhalf.user.userid,
				upddate: new Date(),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = false;
					resp.text = o.text;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.updemporgstru({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				salary : req.param('salary'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = false;
					resp.text = o.text;
				}
				callback(e,o);
			});
		},
		function(callback) {
			userskills.update2userskills({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('userid')),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				skratings : req.param('skratings'),
				updby : req.workhalf.user.userid,
				active : true,
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.text = '';
					resp.data = o2;
				} else		{
					resp.err = true;
					resp.text = o2.text;
					resp.data = null;
				}
				callback(e2,o2);
			});
		},
		function(callback) {
			orgstru.addneworgstru({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				sdate : req.param('sdate'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				salary : req.param('salary'),
				comments : req.param('comments'),
				purpose : req.param('ctype'),				// 1= New User  2 = Transfer  3 = Reorg   4 = Promotion   5 = Demotion
				updby : req.workhalf.user.userid,
				upddate: new Date(),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Review period list
// --------------------------------------------------------------------------

var revperiod = require('./modules/revperiod-manager');

exports.show231 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getFPrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/231', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show231NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			revperiod.getNPrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew231 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.addnewrevperiod({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rptitle : req.param('title'),
				//sdate : req.param('sdate'),
				//edate : req.param('edate'),
				status : req.param('sts'),
				frozen : req.param('frozen'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update231 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.updaterevperiod({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rptitle : req.param('title'),
				//sdate : req.param('sdate'),
				//edate : req.param('edate'),
				status : req.param('sts'),
				frozen : req.param('frozen'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete231 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.deleterevperiod({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rptitle : req.param('title'),
				//sdate : req.param('sdate'),
				//edate : req.param('edate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Rating Levels
// --------------------------------------------------------------------------

var rlevel = require('./modules/rlevel-manager');

exports.show235 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			rlevel.getFPrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/235', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show235NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			rlevel.getNPrlevel(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew235 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.addnewrlevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update235 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.updaterlevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete235 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.deleterlevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skills Rating Levels
// --------------------------------------------------------------------------

var sklevel = require('./modules/sklevel-manager');

exports.show206 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			sklevel.getFPsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/206', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show206NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			sklevel.getNPsklevel(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew206 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			sklevel.addnewsklevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				sklevel : req.param('sklevel'),
				skdesc : req.param('skdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update206 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			sklevel.updatesklevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				sklevel : req.param('sklevel'),
				skdesc : req.param('skdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete206 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			sklevel.deletesklevel({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				sklevel : req.param('sklevel'),
				skdesc : req.param('skdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Google Organisation Chart
// --------------------------------------------------------------------------

exports.show236 = function(req, res) 		{
	res.render('owner/236', {curruser : req.workhalf.user});
};
// --------------------------------------------------------------------------
// Manage Probations
// --------------------------------------------------------------------------

exports.show237 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getFPprobusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/237', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show237NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPprobusers(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update237 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;

	async.parallel([
		function(callback) {
			users.updateprob({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				probdate : req.param('probdate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Initiate employee exits
// --------------------------------------------------------------------------
var exits = require('./modules/exits-manager');

exports.show238 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/238', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/238', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.init238 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.addnewexits({
				coid : req.workhalf.user.coid,
				userid : req.param('emp'),
				mgr : req.param('mgr'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (!e)		{
					resp.err = false;
					resp.data = o;
					resp.text = '';
				} else		{
					resp.err = o.err;
					resp.data = o.data;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		function(callback) {
			users.setExit({
				coid : req.workhalf.user.coid,
				userid : req.param('emp'),
			}, function(e,o, tot)	{
				if (!e)		{
					resp.text = '';
				} else		{
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Team member goals
// --------------------------------------------------------------------------
var usergoals = require('./modules/usergoals-manager');

exports.show239 = function(req, res) {
	var data0,  data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/239', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1) ,usrdata : JSON.stringify(data2) });
		}
	);
};

exports.search239 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('usr')),
				rperiod : req.param('rperiod'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		// For Rolewise goals from repository
		function(callback) {
			perfgoals.getperfgoalsbyrole({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				roleid : req.param('roleid'),
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.data2 = o2;
				} else		{
					resp.err = true;
					resp.data2 = null;
				}
				callback(e2,o2);
				});
		},
		// For manager's self goals
		function(callback) {
			usergoals.getgoalsbyuser({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('mgrid')),
			}, function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.data1 = o1;
				} else		{
					resp.err = true;
					resp.data1 = null;
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show239NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew239 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
				submitted : '0',
			}, function(e3,o3, tot)	{
				if (o3)		{
					resp.err = false;
					resp.data = o3;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e3,o3);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.update239 = function(req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e3,o3, tot)	{
				if (o3)		{
					resp.err = false;
					resp.data = o3;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e3,o3);
			});
	  },
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.delete239 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				ugoaltitle : req.param('ugoaltitle'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit239 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
      var tstr = "here";
      var lstr = tstr.link("/432");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('userid'),
        mailid : '',
        mtype  : '2', 
        mesg   : "You have been assigned new targets for the "+rptitle.substr(0,rptitle.indexOf(' 00:00:00'))+ " by " + req.workhalf.user.username + ".  Look at your targets "+lstr,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.commitgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

var incv = require('./modules/incv-manager');

exports.getincvdoc = function (req, res) {
	var resp = new Object;
	async.parallel([
		function(callback) {
			incv.getincvbyperiod_user({coid : req.workhalf.user.coid, userid : req.param("user"), rperiod : req.param("rperiod")}, function(e1,o1)    {
				if (o1.err > 0)		{
					resp.err = true;
					resp.data = null;
					resp.text = o1.text;
				} else		{
					resp.err = false;
					resp.data = o1;
					resp.text = "";
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			//console.log(resp);
			res.json(resp);
		}
	);
}

// Upload incentives plan
exports.incvdocsul = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
	var resp = new Object;
	var data0,  data1, data2;
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      var filename = files.docfile.name;
      var extensionAllowed=[".docx",".doc",".pdf",".xls",".xlsx"];
      var maxSizeOfFile=2500;
      var i = filename.lastIndexOf('.');

		  var file_extension= (i < 0) ? '' : filename.substr(i);
		  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1024 ) < maxSizeOfFile))   {
				AWS.config.update({
					  accessKeyId: accessKeyId,
					  secretAccessKey: secretAccessKey
				});
		    //var randm = Math.random().toString(36).slice(-16);
				//var strKey = fields.ucoid + "/" + randm + file_extension;

				var incvfile = fields.urperiod + "_incv" + file_extension;
        var strKey = fields.ucoid + "/" + fields.uuserid + "/" + incvfile;
				//console.log(strKey);

			  var path = files.docfile.path;
			  fs.readFile(path, function(err, file_buffer)		{
		      var params = {
				      Bucket: s3__bucket,
				      Key: strKey,
		          Body: file_buffer,
							ACL: 'public-read'
		      };
					var s3 = new AWS.S3();
					async.series([
						function(callback) {
					    s3.putObject(params, function (e, o) {
								if (e)		{
									resp.err = 1;
									resp.text = "Incentive Plan NOT uploaded";
								} else		{
									resp.err = 0;
									resp.text = "Incentive Plan  uploaded";
								}
								callback(e,o);
							});
						},
						function(callback) {
							incv.updatedata({coid : fields.ucoid, userid : fields.uuserid, rperiod : fields.urperiod, incvfile : incvfile}, function(e1,o1,tot)    {
								if (o1)		{
									resp.err = false;
									resp.data = o1;
									resp.incvfile = incvfile;
								} else		{
									resp.err = true;
									resp.data = null;
									resp.incvfile = incvfile;
								}
								callback(e1,o1);
							});
						},
						], function(err, results) {
							res.json(resp);
						}
					);
				});
			} else  {
	      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
			}
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

// --------------------------------------------------------------------------
// Manage PIP - Freeze
// --------------------------------------------------------------------------
var userpips = require('./modules/userpips-manager');

exports.show240 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			userpips.getFPuserpips(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/240', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,usrdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show240NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			userpips.getNPuserpips(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew240 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.addnewuserpips({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update240 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.updateuserpips({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete240 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.deleteuserpips({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Exit Actions List
// --------------------------------------------------------------------------

var exitacts = require('./modules/exitacts-manager');

exports.show242 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			exitacts.getFPexitacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/242', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show242NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			exitacts.getNPexitacts(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew242 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.addnewexitacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update242 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.updateexitacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete242 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.deleteexitacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team Performance Progress
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');
var sklevel = require('./modules/sklevel-manager');
var userskills = require('./modules/userskills-manager');

exports.show243 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		function(callback) {
			sklevel.getallsklevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e6, o6) {
				if (o6)
					data6 = o6;
				else
					data6 = null;
				callback(e6,o6);
			});
		},
		], function(err, results) {
			res.render('owner/243', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), usrdata : JSON.stringify(data4), skilldata : JSON.stringify(data5), skrdata : JSON.stringify(data6)});
		}
	);
};

exports.search243 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userskills.getskillsofuser(req.workhalf.user.coid, req.param('usr'), function(e1, o1) {
				if (o1)		{
					resp.skills = o1;
				} else		{
					resp.skills = null;
				}
				callback(e1,o1);
			});
		},
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.mgr243 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updatemgrrating({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.submit243 = function(req, res) 		{
	var resp = new Object;
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(req.workhalf.user.coid, req.param('rpid'), function(e2, rpdata) {
				callback(e2, rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
      var tstr = "here";
      var lstr = tstr.link("/432");
			users.addnotifs({
				coid   : req.workhalf.user.coid,
				userid : req.param('userid'),
        mailid : '',
        mtype  : '2', 
        mesg   : req.workhalf.user.username + " has rated your performance for period  "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) + ". Please look up "+lstr,
        rflag  : false
			}, function(e4,o4)	{
				callback(e4,o4);
			});
	  },
		function(o4, callback) {
			usergoals.msubmitgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Approve / Reject Resignation
// --------------------------------------------------------------------------

var exits = require('./modules/exits-manager');
var exitacts = require('./modules/exitacts-manager');

exports.show241 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			exits.getapprexits(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			 exitacts.getallexitacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/241', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), repldata : JSON.stringify(data1), eactdata : JSON.stringify(data2), totpages:tot});
		}
	);
};

exports.show241NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			exits.getNPexits(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update241 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exits.finalise({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			users.finalise(req.workhalf.user.coid, req.param('ruser'), function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.text = o1.text;
				} else		{
					resp.err = true;
					resp.text = o1.text;
				}
				callback(e1,o1);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Income Tax brackets
// --------------------------------------------------------------------------
/*
var itslabs = require('./modules/itslabs-manager');

exports.show243 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			itslabs.getFPitslabs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/243', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show243NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			itslabs.getNPitslabs(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew243 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.addnewitslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update243 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.updateitslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete243 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.deleteitslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
*/
// --------------------------------------------------------------------------
// Professional Tax brackets
// --------------------------------------------------------------------------

var ptslabs = require('./modules/ptslabs-manager');

exports.show244 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			ptslabs.getFPptslabs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/244', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show244NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			ptslabs.getNPptslabs(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew244 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.addnewptslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update244 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.updateptslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete244 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.deleteptslabs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Salary Components
// --------------------------------------------------------------------------

var salcomp = require('./modules/salcomp-manager');

exports.show245 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salcomp.getFPsalcomp(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/245', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show245NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			salcomp.getNPsalcomp(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew245 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.addnewsalcomp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				isbasic : req.param('isb'),
				desc : req.param('desc'),
				sctype : req.param('type'),
				calctype : req.param('calc'),
				calcval : req.param('cval'),
				ymax : req.param('ymax'),
				catg : req.param('catg'),
				dummy : req.param('dummy'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update245 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.updatesalcomp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				isbasic : req.param('isb'),
				desc : req.param('desc'),
				sctype : req.param('type'),
				calctype : req.param('calc'),
				calcval : req.param('cval'),
				ymax : req.param('ymax'),
				catg : req.param('catg'),
				dummy : req.param('dummy'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete245 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.deletesalcomp({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				isbasic : req.param('isb'),
				desc : req.param('desc'),
				sctype : req.param('type'),
				calctype : req.param('calc'),
				calcval : req.param('cval'),
				ymax : req.param('ymax'),
				catg : req.param('catg'),
				dummy : req.param('dummy'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Monthly Salaries
// --------------------------------------------------------------------------

var salaries = require('./modules/salaries-manager');

exports.show246 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salaries.getFPsalaries(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			salstru.getallsalstru(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data1 = o;
				else
					data1 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/246', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), strudata : JSON.stringify(data1), totpages:tot});
		}
	);
};

exports.search246 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.getSalaryByUser({
				coid : req.workhalf.user.coid,
				userid : req.param('usr'),
				month : req.param('mon'),
				year : req.param('year'),
				stru : req.param('stru'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew246 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.addnewsalaries({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update246 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.updatesalaries({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete246 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.deletesalaries({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Salary Structures
// --------------------------------------------------------------------------
var salstru = require('./modules/salstru-manager');

exports.show247 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salstru.getFPsalstru(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			salcomp.getallsalcomp(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/247', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,scdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show247NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			salstru.getNPsalstru(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			salcomp.getallsalcomp(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew247 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.addnewsalstru({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update247 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.updatesalstru({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete247 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.deletesalstru({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Roles defined for each department
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show251 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/251', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search251 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.getrolesbydept(req.workhalf.user.coid,req.workhalf.user.userid, req.param('dept'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		function(callback) {
			skills.getallskills(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			resp.mhsdata = data2;
			resp.nhsdata = data2;
			res.json(resp);
		}
	);
};

exports.mgr251 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.updatemgr({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// KRA defined for each role
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show252 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/252', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), roledata : JSON.stringify(data2)});
		}
	);
};

exports.search252 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;

	var roleid = '';
	var kraids = [];

	async.series([
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e0, o0) {
				if (o0)		{
					data0 = o0;
				} else	{
					data0 = null;
				}
				callback(e0,o0);
			});
		},
		function(callback) {
			kra.getkraarray(req.workhalf.user.coid, data0[0].kras, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			resp.data = data1;
			resp.dept = data2;
			res.json(resp);
		}
	);
};

exports.mgr252 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.updatemgr({
				coid : req.workhalf.user.coid,
				dbid : req.param('dbid'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Department Induction Questions
// --------------------------------------------------------------------------
var deptindcq = require('./modules/deptindcq-manager');

exports.show253 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			induction.getallinduction(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/253', {curruser : req.workhalf.user, indcdata : JSON.stringify(data0)});
		}
	);
};

exports.search253 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.getquesByIndc(req.workhalf.user.coid, req.workhalf.user.userid, req.param('indc'),function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew253 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.addnewdeptindcq({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update253 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.updatedeptindcq({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete253 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.deletedeptindcq({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team profile
// --------------------------------------------------------------------------
exports.show254 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/254', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search254 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("rep")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Induction Results Report
// --------------------------------------------------------------------------

var deptindcq = require('./modules/deptindcq-manager');

exports.show255 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			induction.getallinduction(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/255', {curruser : req.workhalf.user, usrdata : JSON.stringify(data1) ,indcdata : JSON.stringify(data2), totpages:tot});
		}
	);
};

exports.search255 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersById(req.workhalf.user.coid, req.param("usr"), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Upload documents to user profile
// --------------------------------------------------------------------------
var userdocs = require('./modules/userdocs-manager');

exports.show256 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/256', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/256', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search256 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userdocs.getalluserdocs(req.workhalf.user.coid, req.param('emp'), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.uploadcompdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".jpg",".tif",".png"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1034 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/" + fields.uuserid + "/" + files.docfile.name;

				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Document NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Document Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          userdocs.addnewuserdocs({ coid: fields.ucoid, userid: fields.uuserid, catg: fields.catg, docdesc : fields.desc, docfile : files.docfile.name}, function(e1,o1,tot)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.totpages = tot;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

function oc(a){
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}

exports.deletecompDoc = function(req, res) {
	var s3 = new AWS.S3();
	var resp = new Object;

	async.series([
		function(callback) {
			userdocs.deleteuserdocs({coid: req.workhalf.user.coid, userid: req.param('userid'), dbid: req.param('dbid') }, function(e1,o1,tot)	{
				if (e1)		{
					resp.err = true;
					resp.text = "File NOT Deleted";
					resp.data = null;
				} else		{
					resp.err = false;
					resp.text = "File Deleted";
					resp.data = o1;
				}
				resp.totpages = tot;
				callback(e1,o1);
			});
		},
		function(callback) {
			s3.deleteObjects({
					Bucket: s3__bucket,
					Delete: {
							Objects: [
								 { Key: req.workhalf.user.coid + "/" + req.workhalf.user.userid  + "/" + req.param('filename')}
							]
					}
			}, function(err, data) {
				if (err)		{
					resp.fd = "File NOT Deleted";
				} else		{
					resp.fd = "File Deleted";
				}
				callback(err, data);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.showcompdoc = function(req, res) {
	var xcoid = req.workhalf.user.coid;
	var xuserid = req.workhalf.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/" + xuserid + "/ " + req.param("docfile");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Onboarding actions list
// --------------------------------------------------------------------------

var onbdacts = require('./modules/onbdacts-manager');

exports.show257 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			onbdacts.getFPonbdacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/257', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show257NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			onbdacts.getNPonbdacts(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew257 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			onbdacts.addnewonbdacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update257 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			onbdacts.updateonbdacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete257 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			onbdacts.deleteonbdacts({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Track user Onboarding 
// --------------------------------------------------------------------------

var ondblist = require('./modules/useronbd-manager');

exports.show258 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/258', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/258', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search258 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getoneusers(req.workhalf.user.coid, parseInt(req.param('emp')), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			onbdacts.getallonbdacts(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)		{
					resp.onbdacts = o1;
				}	else		{
					resp.onbdacts = null;
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew258 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
/*	async.parallel([
		function(callback) {
			ondblist.adduseronbd({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				onbdacts : req.param('onbdacts'),
			}, function(e,o)	{
				if (!e)		{
					resp.err = false;
					resp.data = o;
					resp.text = "";
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
*/
	var onbdacts = req.param('onbdacts');
	//console.log(onbdacts);
	async.forEachSeries(onbdacts, function(act, callback) {
		ondblist.adduseronbd({coid : req.workhalf.user.coid,userid : req.param('userid'),onbdacts : act}, function(e1,o1)	{
			data0 = o1;
			callback(e1,o1);
		});
	}, function(er1, res1) {
		//console.log(data0);
		res.json(data0);
	});
};

exports.update258 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ondblist.updateuseronbd({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				status : '1',
			}, function(e,o)	{
				if (!e)		{
					resp.err = false;
					resp.data = o;
					resp.text = "";
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// View Licenses
// --------------------------------------------------------------------------

exports.show207 = function(req, res)   {
	LIC.getalllicense( req.workhalf.user.coid, function(e, liclist, tot)			{
  	res.render('owner/207', {curruser : req.workhalf.user, licdata: JSON.stringify(liclist), totmesgs:tot});
	});
};

exports.show207NP = function(req, res) {
  LIC.getnewlicensePage(req.workhalf.user.coid, req.param("newpage"), function(e, o) {
		if (e)	{
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			res.send(o);
		}
  });
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Team profile
// --------------------------------------------------------------------------
exports.show254 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/254', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search254 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("rep")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Goals unassigned Report
// --------------------------------------------------------------------------
exports.show227 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/227', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search227 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("dept")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance tasks creation status
// --------------------------------------------------------------------------
exports.show232 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/232', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search232 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("dept")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Overdue performance tasks
// --------------------------------------------------------------------------
exports.show233 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/233', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search233 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("dept")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Opening leave balances
// --------------------------------------------------------------------------

var emplvbal = require('./modules/emplvbal-manager');

exports.show234 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/234', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), userdata : JSON.stringify(data2), ltypedata : JSON.stringify(data1), totpages:tot});
		}
	);
};

exports.show234NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			emplvbal.getNPemplvbal(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.search234 = function(req, res) {
	var data0,  data1, data2, data6;
	var resp = new Object;
	var tot;
	async.series([
		function(callback) {
			emplvbal.getemplvbal(req.workhalf.user.coid, req.param('userid'), function(e, o, t) {
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew234 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			emplvbal.addnewemplvbal({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				ltype : req.param('ltype'),
				pbal : req.param('pbal'),
				totav : req.param('used'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update234 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			emplvbal.updateemplvbal({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				ltype : req.param('ltype'),
				pbal : req.param('pbal'),
				totav : req.param('used'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete234 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			emplvbal.deleteemplvbal({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				ltype : req.param('ltype'),
				pbal : req.param('pbal'),
				totav : req.param('used'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Events
// --------------------------------------------------------------------------
var events = require('./modules/events-manager');

exports.show259 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			events.getFPevents(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/259', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show259NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			events.getNPevents(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew259 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			events.addnewevents({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				date : req.param('date'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update259 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			events.updateevents({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				date : req.param('date'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete259 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			events.deleteevents({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				date : req.param('date'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Company Blogs
// --------------------------------------------------------------------------
var blogs = require('./modules/blogs-manager');

exports.show260 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			blogs.getFPblogs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/260', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};

exports.show260a = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			//console.log(req.query.blog);
			blogs.getoneblog(req.workhalf.user.coid, req.workhalf.user.userid, req.query.blog, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			//console.log(data0);
			res.render('owner/260a', {curruser : req.workhalf.user, formdata : JSON.stringify(data0)});
		}
	);
};

exports.show260NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			blogs.getNPblogs(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew260 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			blogs.addnewblogs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				author : req.param('author'),
				stext : req.param('stext'),
				ftext : req.param('ftext'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.update260 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			blogs.updateblogs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				author : req.param('author'),
				stext : req.param('stext'),
				ftext : req.param('ftext'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete260 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			blogs.deleteblogs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				author : req.param('author'),
				stext : req.param('stext'),
				ftext : req.param('ftext'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Publish Company Blogs
// --------------------------------------------------------------------------
exports.show261 = function(req, res) {
	var data0,  data1, data2;
	async.series([
		function(callback) {
			blogs.getselectedblogs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			blogs.getallblogs(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/261', {curruser : req.workhalf.user, formdata : JSON.stringify(data0) ,blogsdata : JSON.stringify(data1)});
		}
	);
};
exports.update261 = function(req, res) {
	var data0,  data1, data2;
	async.series([
		function(callback) {
			blogs.setselected({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				blog : req.param('old1'),
				value : false,
			}, function(e,o)	{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
				});
		},
		function(callback) {
			blogs.setselected({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				blog : req.param('old2'),
				value : false,
			}, function(e,o)	{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
				});
		},
		function(callback) {
			blogs.setselected({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				blog : req.param('blog1'),
				value : true,
			}, function(e,o)	{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
				});
		},
		function(callback) {
			blogs.setselected({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				blog : req.param('blog2'),
				value : true,
			}, function(e,o)	{
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(data0);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------
exports.newTrainee = function(req, res) {
	var place = req.param('place');
	switch (place)		{
		case '0':
			var pdesc = "At our office";
			break;
		case '1':
			var pdesc = "Outside office,we will arrange the place";
			break;
		case '2':
			var pdesc = "Outside office,trainer needs to arrange the place";
			break;
	}
	EM.newTrainee({
		cuser     	 : req.workhalf.user,
		training     : req.param('training'),
		trainees     : req.param('trainees'),
    place        : pdesc,
    date         : req.param('date'),
		expect       : req.param('expect'),
	}, function(e,o)    {
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.newContract = function(req, res) {
	EM.newContract({
		cuser     	 : req.workhalf.user,
		role         : req.param('role'),
		location     : req.param('location'),
    tenure       : req.param('tenure'),
    fees         : req.param('fees'),
		totpersons   : req.param('totpersons'),
	}, function(e,o)    {
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

exports.newJob = function(req, res) {
	var jobtype = req.param('jobtype');
	switch (jobtype)		{
		case '0':
			var jtype = "Full time on rolls";
			break;
		case '1':
			var jtype = "Flexible on rolls";
			break;
		case '2':
			var jtype = "Full time contractual";
			break;
		case '3':
			var jtype = "Flexible contractual";
			break;
	}

	var jobfee = req.param('jobfee');
	switch (jobfee)		{
		case '0':
			var searchfee = "8.33% of annual CTC";
			break;
		case '1':
			var searchfee = "upto 12.5% of annual CTC";
			break;
		case '2':
			var searchfee = "more than 12.5% of annual CTC";
			break;
	}

	EM.newJob({
		cuser     	 : req.workhalf.user,
		title        : req.param('title'),
		jobdesc      : req.param('jobdesc'),
    jobloc       : req.param('jobloc'),
    jobtype      : jtype,
		comp         : req.param('comp'),
    stock        : req.param('stock'),		
    offer        : req.param('offer'),
    leave        : req.param('leave'),
    profile      : req.param('profile'),
    jobpost      : req.param('jobpost'),
    agent        : req.param('agent'),
    jobfee       : searchfee,
	}, function(e,o)    {
		if (e){
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};


// --------------------------------------------------------------------------
// Performance goals
// --------------------------------------------------------------------------

var perfgoals = require('./modules/perfgoals-manager');

exports.show262 = function(req, res) {
	var data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			kra.getallkra(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			users.getrolesbymgr(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('owner/262', {curruser : req.workhalf.user, kradata : JSON.stringify(data1), roledata : JSON.stringify(data2), subroledata : JSON.stringify(data3), totpages:tot});
		}
	);
};

exports.search262 = function(req, res) {
	var data0,  data1, data2, data6;
	var resp = new Object;
	var tot;
	async.series([
		function(callback) {
			perfgoals.getperfgoalsbyrolekra({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid,roleid: req.param('role'),kraid: req.param('kra')}, function(e, o, t) {
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e2, o2) {
				if (o2)		{
					data2 = o2;
				} else	{
					data2 = null;
				}
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show262NP = function(req, res) {
	var data0,  data1, data2;
	async.series([
		function(callback) {
			perfgoals.getperfgoalsbyrole({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid,roleid: req.param('role')}, function(e, o, t) {
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data0 = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		function(callback) {
			roles.getrolesbyid(req.workhalf.user.coid, req.param('role'), function(e2, o2) {
				if (o2)		{
					data2 = o2;
				} else	{
					data2 = null;
				}
				callback(e2,o2);
			});
		},
		function(callback) {
			kra.getkraarray(req.workhalf.user.coid, data2[0].kras, function(e6, o6) {
				if (o6)
					resp.data1 = o6;
				else
					resp.data1 = null;
				callback(e6,o6);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew262 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.addnewperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update262 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.updateperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete262 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			perfgoals.deleteperfgoals({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				role : req.param('role'),
				kra : req.param('kra'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Settings
// --------------------------------------------------------------------------
//var company = require('./modules/company-manager');

exports.show263 = function(req, res) {
	company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/263', {curruser : req.workhalf.user});
		else  {
			res.render('owner/263', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

exports.update263 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			company.updatesettings({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
        fysdate : req.param('fysdate'),
        fyedate : req.param('fyedate'),
        wwstart : req.param('wwstart'),
        weno : req.param('weno'),
				hrdate : req.param('hrdate'),
				hr2date : req.param('hr2date'),
				maxl : req.param('maxl'),
				negl : req.param('negl'),
				preh : req.param('preh'),
				inth : req.param('inth'),
				such : req.param('such'),
				maxcf : req.param('maxcf'),
				maxtr : req.param('maxtr'),

				prdate : req.param('prdate'),
				psdate : req.param('psdate'),
				rperiod : req.param('rperiod'),
				prwho : req.param('prwho'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team Leave history
// --------------------------------------------------------------------------
var empleaves = require('./modules/empleaves-manager');
var leavetypes = require('./modules/leavetypes-manager');
var emplvbal = require('./modules/emplvbal-manager');

exports.show264 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/264', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/264', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search264 = function(req, res) {
	var data0,  data1, data2, data3;
	var resp = new Object;

	async.parallel([
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)		{
					resp.err = false;
					resp.data = o1;
				}		else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e1,o1);
			});
		},
		function(callback) {
			company.getonecompany(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			empleaves.getempleaves(req.workhalf.user.coid, parseInt(req.param('userid')), function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			emplvbal.getemplvbal(req.workhalf.user.coid, req.param('userid'), function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			resp.codata = data2;
			resp.lvdata = data3;
			resp.emplvdata = data4;
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Notifications
// --------------------------------------------------------------------------
exports.show265 = function(req, res) {
	users.getnotifs(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/265', {curruser : req.workhalf.user, formdata : null });
		else  {
		  //if (o.length > 0)
		    //req.workhalf.user.notiftot = o.length;
		  //else
		    req.workhalf.user.notiftot = '';
			res.render('owner/265', {curruser : req.workhalf.user, formdata : JSON.stringify(o)});
		}
	});
};

exports.set265 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.updnotifs({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				mailid : req.param('mailid'),
				mesg : req.param('mesg'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.tot = '';
				} else		{
					resp.err = true;
					resp.data = null;
					resp.tot = '';
				}
				callback(e,o);
				});
		  },
	  function(callback) {
	    users.getnotiftot(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
				if (o)		{
          if (o != 0)   {
      		  req.workhalf.user.notiftot = o;
					  resp.tot = o;
					} else  {
      		  req.workhalf.user.notiftot = '';
					  resp.tot = '';
					}
				}
				callback(e,o);
				});
		  },
	  ], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// user skills rating
// --------------------------------------------------------------------------
var userskills = require('./modules/userskills-manager');

exports.show266 = function(req, res) {
	users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o) {
		if (e)
			res.render('owner/266', {curruser : req.workhalf.user, empdata : null });
		else  {
			res.render('owner/266', {curruser : req.workhalf.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search266 = function(req, res) {
	var resp = new Object;
	var tot;
  var empid = parseInt(req.param('emp'));
	async.parallel([
		function(callback) {
			empleaves.getallempleaves(req.workhalf.user.coid, empid, function(e, o) {
				if (o)    {
				  resp.err = false;
					resp.data = o;
				} else  {
				  resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			leavetypes.getallleavetypes(req.workhalf.user.coid, empid, function(e1, o1) {
				if (o1)
					resp.lnamedata = o1;
				else
					resp.lnamedata = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.delete266 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			empleaves.delempleaves({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				mgr : req.workhalf.user.mgr,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				hdays : req.param('hdays'),
				ldays : req.param('ldays'),
				ltot : req.param('ltot'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Team member goals
// --------------------------------------------------------------------------
var usergoals = require('./modules/usergoals-manager');

exports.show267 = function(req, res) {
	var data0,  data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/267', {curruser : req.workhalf.user, rperioddata : JSON.stringify(data1) ,usrdata : JSON.stringify(data2) });
		}
	);
};

exports.search267 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('usr')),
				rperiod : req.param('rperiod'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		// For Rolewise goals from repository
		function(callback) {
			perfgoals.getperfgoalsbyrole({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				rperiod : req.param('rperiod'),
				roleid : req.param('roleid'),
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.data2 = o2;
				} else		{
					resp.err = true;
					resp.data2 = null;
				}
				callback(e2,o2);
				});
		},
		// For manager's self goals
		function(callback) {
			usergoals.getgoalsbyuser({
				coid : req.workhalf.user.coid,
				userid : parseInt(req.param('mgrid')),
			}, function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.data1 = o1;
				} else		{
					resp.err = true;
					resp.data1 = null;
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show267NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.workhalf.user.coid, req.workhalf.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.reset267 = function(req, res) 		{
	var resp = new Object;
	async.series([
		function(callback) {
			usergoals.resetgoals({
				coid : req.workhalf.user.coid,
				userid : req.param('userid'),
				rperiod : req.param('rpid'),
				reset_type : req.param('reset_type'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Manage Performance appraisal
// --------------------------------------------------------------------------
var apprsl = require('./modules/apprsl-manager');

exports.show268 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			apprsl.getFPapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('owner/268', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), rpiddata : JSON.stringify(data1), deptdata : JSON.stringify(data2), locdata : JSON.stringify(data3), totpages:tot});
		}
	);
};

exports.show268NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			apprsl.getNPapprsl(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew268 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.addnewapprsl({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprtitle : req.param('aprtitle'),
				rvteam : req.param('rvteam'),
        loc : req.param('loc'),
        dept : req.param('dept'),
        role : req.param('role'),
        etype : req.param('etype'),
        jdate : req.param('jdate'),
        sdate : req.param('sdate'),
        edate : req.param('edate'),
				rvoptn : req.param('rvoptn'),
				rpwt : req.param('rpwt'),
				rperiod : req.param('rpid'),
				bvrt : req.param('bvrt'),
				bvwt : req.param('bvwt'),
				bvtxt : req.param('bvtxt'),
				cvrt : req.param('cvrt'),
				cvwt : req.param('cvwt'),
				trng : req.param('trng'),
				recom : req.param('recom'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.update268 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updateapprsl({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				aprtitle : req.param('aprtitle'),
				rvteam : req.param('rvteam'),
        loc : req.param('loc'),
        dept : req.param('dept'),
        role : req.param('role'),
        etype : req.param('etype'),
        jdate : req.param('jdate'),
        sdate : req.param('sdate'),
        edate : req.param('edate'),
				rvoptn : req.param('rvoptn'),
				rpwt : req.param('rpwt'),
				rperiod : req.param('rpid'),
				bvrt : req.param('bvrt'),
				bvwt : req.param('bvwt'),
				bvtxt : req.param('bvtxt'),
				cvrt : req.param('cvrt'),
				cvwt : req.param('cvwt'),
				trng : req.param('trng'),
				recom : req.param('recom'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.delete268 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.deleteapprsl({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.mark268 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.closeapprsl({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Review Cycle
// --------------------------------------------------------------------------
var revperiod = require('./modules/revperiod-manager');

exports.show269 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getFPrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/269', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show269NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			revperiod.getNPrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update269 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.updaterevperiod({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				status : req.param('sts'),
				frating : req.param('frat'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Review status Report
// --------------------------------------------------------------------------
exports.show270 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/270', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), rperioddata : JSON.stringify(data2)});
		}
	);
};

exports.search270 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.workhalf.user.coid, userid:req.workhalf.user.userid, depid:req.param("dept")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod({
				coid : req.workhalf.user.coid,
				rperiod : req.param('rpid'),
			}, function(e4,o4)	{
				if (o4)		{
					resp.data4 = o4;
				} else		{
					resp.data4 = null;
				}
				callback(e4,o4);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Business Numbers
// --------------------------------------------------------------------------
var biznum = require('./modules/biznum-manager');

exports.show271 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			biznum.getFPbiznum(req.workhalf.user.coid, req.workhalf.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('owner/271', {curruser : req.workhalf.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show271NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			biznum.getNPbiznum(req.workhalf.user.coid, req.workhalf.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew271 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			biznum.addnewbiznum({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				title : req.param('title'),
				value : req.param('value'),
				vfrom : req.param('vfrom'),
				vto : req.param('vto'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update271 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			biznum.updatebiznum({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				value : req.param('value'),
				vfrom : req.param('vfrom'),
				vto : req.param('vto'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete271 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			biznum.deletebiznum({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				value : req.param('value'),
				vfrom : req.param('vfrom'),
				vto : req.param('vto'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Appraisal by HR
// --------------------------------------------------------------------------

exports.show272 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All appraisal period List
		function(callback) {
			apprsl.getactiveapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			users.getallemployees(req.workhalf.user.coid, req.workhalf.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('owner/272', {curruser : req.workhalf.user, aprsldata : JSON.stringify(data1), rleveldata : JSON.stringify(data2), usrdata : JSON.stringify(data3)});
		}
	);
};

exports.hr272 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			apprsl.searchhrdata({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.hrrtng272 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			apprsl.updateshr({
				coid : req.workhalf.user.coid,
				mgrid : req.param('mgrid'),
				userid : req.param('userid'),
				aprslid : req.param('aprslid'),
				dbid : req.param('dbid'),
				rating : req.param('rating'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Appraisal status Report
// --------------------------------------------------------------------------
exports.show273 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.workhalf.user.coid, req.workhalf.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		//Get All appraisal period List
		function(callback) {
			apprsl.getactiveapprsl(req.workhalf.user.coid, req.workhalf.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('owner/273', {curruser : req.workhalf.user, deptdata : JSON.stringify(data1), aprsldata : JSON.stringify(data2)});
		}
	);
};

exports.search273 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({
			  coid:req.workhalf.user.coid, 
			  userid:req.workhalf.user.userid, 
			  depid:req.param("deptid")
		  }, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.users = o;
				} else		{
					resp.err = true;
					resp.users = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			apprsl.searchdeptdata({
				coid : req.workhalf.user.coid,
				userid : req.workhalf.user.userid,
				aprslid : req.param('aprslid'),
				deptid : req.param('deptid'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


