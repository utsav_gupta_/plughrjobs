/*
 * EQUALPRO Homepage functionality
*/
/*
   0. Site Admin
   1. Site Users 
   2. Business Owner / HR Admin
   3. Payroll Admin
   4. Employees
   5. Consultants
*/

var users = require('./modules/user-manager');
var CT = require('./modules/country-list');
var async = require('async');

//---------------------------------------------
// For File uploads 
//---------------------------------------------
var formidable = require('formidable')
var fs = require('fs');
var sys = require('sys');
var AWS = require('aws-sdk');

var mkdirp = require('mkdirp');
var format = require('util').format;
//Directory to upload file
var uploadPath="uploads/";
var userpictpath="uploads/userpics/";
//---------------------------------------------
/// Include ImageMagick
//---------------------------------------------
var im = require('imagemagick');
//---------------------------------------------

var accessKeyId =  process.env.AWS_ACCESS_KEY || "AKIAJGTOOVCWVATAEXFA";
var secretAccessKey = process.env.AWS_SECRET_KEY || "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";
var s3__bucket = "plughr-uploads";

exports.index = function(req, res) {
  res.render('payadm/301',{curruser : req.plugHRapp.user});
};

exports.show_upd_password = function(req, res) {
	res.render('payadm/303', {curruser : req.plugHRapp.user});
};

exports.upd_password = function(req, res) {
	users.updatePassword(req.plugHRapp.user.coid, req.plugHRapp.user.userid , req.param('opass'), req.param('npass'), function(e, o)		{
		if (e)    {
			console.log(o.err+" -- "+ o.text);
			res.json(o);
		}	else	{
			//console.log(o.err+" -- "+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// User Work Experiences - part of user profile info
// --------------------------------------------------------------------------
var workexp = require('./modules/workexp-manager');

exports.show306 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			workexp.getFPworkexp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/306', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show306NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			workexp.getNPworkexp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew306 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.addnewworkexp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update306 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.updateworkexp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				arrIndex : req.param('arrIndex'),
				old_orgn : req.param('old_orgn'),
				old_title : req.param('old_title'),
				old_sdate : req.param('old_sdate'),
				old_edate : req.param('old_edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete306 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			workexp.deleteworkexp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				orgn : req.param('org'),
				title : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// User Education - part of user profile info
// --------------------------------------------------------------------------

var useredu = require('./modules/useredu-manager');

exports.show305 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			useredu.getFPuseredu(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/305', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show305NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			useredu.getNPuseredu(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew305 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.addnewuseredu({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update305 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.updateuseredu({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
				arrIndex : req.param('arrIndex'),
				old_udegree : req.param('old_dearned'),
				old_dlevel : req.param('old_dlevel'),
				old_dyear : req.param('old_dyear'),
				old_duni : req.param('old_duni'),
				old_dcollege : req.param('old_dcollege')
			}, function(e,o, tot)	{
				if (!o.err)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete305 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			useredu.deleteuseredu({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				udegree : req.param('dearned'),
				edulevel : req.param('dlevel'),
				cyear : req.param('dyear'),
				univ : req.param('duni'),
				college : req.param('dcollege'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// About You - part of user profile info
// --------------------------------------------------------------------------
exports.show302 = function(req, res) {
	users.getoneusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/302', {curruser : req.plugHRapp.user});
		else  {
			res.render('payadm/302', {curruser : req.plugHRapp.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.update302 = function(req, res) {
  var coid = req.plugHRapp.user.coid;
	var userObject = new Object();
	userObject.coid = req.plugHRapp.user.coid;
	userObject.userid = req.plugHRapp.user.userid;
	userObject.uname = req.param('uname');
	userObject.emailid = req.param('emailid');
	userObject.mobile = req.param('mobno');
	userObject.gender = req.param('gender');
	userObject.dob = req.param('dob');
	userObject.bgrp = req.param('bgrp');
	userObject.emername = req.param('emername');
	userObject.emerrel = req.param('emerrel');
	userObject.emermob = req.param('emermob');
	userObject.pint = req.param('pint');
	userObject.pal1 = req.param('pal1');
	userObject.pal2 = req.param('pal2');
	userObject.pcity = req.param('pcity');
	userObject.ppin = req.param('ppin');
	userObject.cal1 = req.param('cal1');
	userObject.cal2 = req.param('cal2');
	userObject.ccity = req.param('ccity');
	userObject.cpin = req.param('cpin');
	userObject.acard = req.param('acard');
	userObject.panno = req.param('panno');
	userObject.ppno = req.param('ppno');
	userObject.ppidate = req.param('ppidate');
	userObject.ppedate = req.param('ppedate');

	users.updateprofile(userObject, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Upload Photo
// --------------------------------------------------------------------------
exports.show324 = function(req, res) {
	res.render('payadm/324', {curruser : req.plugHRapp.user, formdata : JSON.stringify(req.plugHRapp.user)});
};
exports.getprofilepict = function (req, res) {
  var file = userpictpath + req.query.filename;
  fs.exists(file, function(exists) {
    if (exists) {
      //console.log(file);
      res.sendfile(file);
    } else {
      //console.log(userpictpath + 'not_available.gif');
      res.sendfile(userpictpath + 'not_available.gif');
    }
  });
}; 

exports.updprofilepict = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.pictfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.pictfile.name;
	      var extensionAllowed=[".jpg",".jpeg",".gif",".png"];
	      var maxSizeOfFile = 3000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.pictfile.size /1034 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/" + fields.uuserid + "/" + fields.ucoid +"-" + fields.uuserid;

				  var path = files.pictfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Photo NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Photo Saved";
									}
									callback(e,o);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company information
// --------------------------------------------------------------------------
var company = require('./modules/company-manager');

exports.show308 = function(req, res) {
	company.getonecompany(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/308', {curruser : req.plugHRapp.user});
		else  {
			res.render('payadm/308', {curruser : req.plugHRapp.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.update308 = function(req, res) {
	company.updatecompany({
		coid : req.param('coid'),
		userid : req.param('userid'),
		cname : req.param('cname'),
		cwebsite : req.param('cwebsite'),
		clogo : req.param('clogo'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company location information
// --------------------------------------------------------------------------
var locations = require('./modules/locations-manager');

exports.show309 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			locations.getFPlocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/309', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show309NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			locations.getNPlocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew309 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.addnewlocations({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update309 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.updatelocations({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete309 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			locations.deletelocations({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				oname : req.param('oname'),
				aline1 : req.param('aline1'),
				aline2 : req.param('aline2'),
				city : req.param('city'),
				pincode : req.param('pincode'),
				phone1 : req.param('phone1'),
				phone2 : req.param('phone2'),
				faxno : req.param('faxno'),
				cperson : req.param('cperson'),
				chq : req.param('chq'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Department information
// --------------------------------------------------------------------------

var department = require('./modules/department-manager');

exports.show310 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getFPdepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/310', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show310NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			department.getNPdepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew310 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.addnewdepartment({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update310 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.updatedepartment({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete310 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			department.deletedepartment({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				depname : req.param('depname'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Leaderboard messages information
// --------------------------------------------------------------------------

//var company = require('./modules/company-manager');

exports.show311 = function(req, res) {
	company.getonecompany(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/311', {curruser : req.plugHRapp.user});
		else  {
			res.render('payadm/311', {curruser : req.plugHRapp.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.update311 = function(req, res) {
	company.updatelboard({
		coid : req.plugHRapp.user.coid,
		userid : req.plugHRapp.user.userid,
		lbmessage : req.param('mesg01'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Review cycle information
// --------------------------------------------------------------------------

//var revcycle = require('./modules/revcycle-manager');

exports.show312 = function(req, res) {
	company.getonecompany(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/312', {curruser : req.plugHRapp.user});
		else  {
			res.render('payadm/312', {curruser : req.plugHRapp.user, formdata : JSON.stringify(o)});
		}
	});
};
exports.update312 = function(req, res) {
	company.updaterevcycle({
		coid : req.plugHRapp.user.coid,
		userid : req.plugHRapp.user.userid,
		frevdate : req.param('frevdate'),
		revafter : req.param('revafter'),
	}, function(e,o)	{
		if (e)    {
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}	else	{
			console.log(o.err+' -- '+ o.text);
			res.json(o);
		}
	});
};

// --------------------------------------------------------------------------
// Corporate Values
// --------------------------------------------------------------------------
var cvalues = require('./modules/cvalues-manager');

exports.show313 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			cvalues.getFPcvalues(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/313', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show313NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			cvalues.getNPcvalues(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew313 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.addnewcvalues({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update313 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.updatecvalues({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete313 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			cvalues.deletecvalues({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				vtitle : req.param('title'),
				vdesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Committees
// --------------------------------------------------------------------------
var committees = require('./modules/committees-manager');

exports.show314 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			committees.getFPcommittees(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('payadm/314', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,membdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show314NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			committees.getNPcommittees(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew314 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.addnewcommittees({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update314 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.updatecommittees({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete314 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			committees.deletecommittees({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				comname : req.param('cname'),
				purpose : req.param('purp'),
				cmembers : req.param('memb'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Policies
// --------------------------------------------------------------------------
var policies = require('./modules/policies-manager');

exports.show316 = function(req, res) {
	policies.getallpolicies(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/316', {curruser : req.plugHRapp.user, formdata : null });
		else  {
			res.render('payadm/316', {curruser : req.plugHRapp.user, formdata : JSON.stringify(o)});
		}
	});
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Induction documents
// --------------------------------------------------------------------------

var induction = require('./modules/induction-manager');

exports.show315 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			induction.getFPinduction(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('payadm/315', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,deptdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show315NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			induction.getNPinduction(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.uploadIndcdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".ppt",".pptx"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1034 ) < maxSizeOfFile))   {
					AWS.config.update({
						  accessKeyId: accessKeyId,
						  secretAccessKey: secretAccessKey
					});
			    var randm = Math.random().toString(36).slice(-16);

					//var strKey = fields.ucoid + "/" + randm + file_extension;
	        var strKey = fields.ucoid + "/" + files.docfile.name;

				  var path = files.docfile.path;
				  fs.readFile(path, function(err, file_buffer)		{
			      var params = {
					      Bucket: s3__bucket,
					      Key: strKey,
			          Body: file_buffer,
								ACL: 'public-read'
			      };
						var s3 = new AWS.S3();
						var resp = new Object;

						var data0,  data1, data2;
						async.series([
							function(callback) {
						    s3.putObject(params, function (e, o) {
									if (e)		{
										resp.err = 1;
										resp.text = "Document NOT Saved";
									} else		{
										resp.err = 0;
										resp.text = "Document Saved";
									}
									callback(e,o);
								});
							},
							function(callback) {
			          induction.addnewinduction({coid: fields.ucoid, userid:fields.uuserid, title : fields.title, itype : fields.itype, dept : fields.dept,	filename : files.docfile.name}, function(e1,o1,tot)    {
									if (o1)		{
										resp.err = false;
										resp.data = o1;
										resp.totpages = tot;
									} else		{
										resp.err = true;
										resp.data = null;
										resp.totpages = 0;
									}
									callback(e1,o1);
								});
							},
							], function(err, results) {
								res.json(resp);
							}
						);
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 2MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};

// Previous code
/*
exports.uploadIndcdoc = function (req, res) { 
  var form = new formidable.IncomingForm();
  var msg="";
  try {
    form.parse(req, function(err,fields, files) {
      if (err)    {
        console.log(err);
        res.json({err:1,text:"File upload failed - please retry"});
      }
      if (!files.docfile)     {
        res.json({err:2,text:"Please select a file to upload"});
      }  else    {
	      var filename = files.docfile.name;
	      var extensionAllowed=[".docx",".doc",".pdf",".ppt",".pptx"];
	      var maxSizeOfFile=5000;
	      var i = filename.lastIndexOf('.');
	
			  var file_extension= (i < 0) ? '' : filename.substr(i);
			  if((file_extension.toLowerCase() in oc(extensionAllowed))&&((files.docfile.size /1034 )< maxSizeOfFile))   {
		      var tmp_path = files.docfile.path;
		      var target_dir = uploadPath + "/" + fields.ucoid + "/";
		      var target_path = target_dir + files.docfile.name;
					mkdirp(target_dir, function(err) { 
						// path was created unless there was error
						if (!err)		{
							fs.rename(tmp_path, target_path, function(err) {
								if (err)
					        res.json({err:3,text:"File upload failed - please retry"});
					      else  {
							    fs.unlink(tmp_path, function(err) {
					          induction.addnewinduction({coid: fields.ucoid, userid:fields.uuserid, title : fields.title, itype : fields.itype, dept : fields.dept,	filename : files.docfile.name}, function(e,o,tot)    {
											var resp = new Object;
											if (o)		{
												resp.err = false;
												resp.data = o;
												resp.totpages = tot;
											} else		{
												resp.err = true;
												resp.data = null;
												resp.totpages = 0;
											}
											res.json(resp);
					          });
					        });
					      }
						  });
						} else	{
			        res.json({err:6,text:"Directory error - Unable to upload document"});
						}
					});
				} else  {
		      res.json({err:5,text:"Invalid file type OR size must be less than 5MB"});
				}
      }
    });
  }
  catch (e) {
    console.error('file upload error captured');
    console.error(e);
    res.json({err:7,text:"File upload failed - please retry"});
  }
};
*/

exports.deleteIndcdoc = function(req, res) {
	var s3 = new AWS.S3();
	var resp = new Object;

	async.series([
		function(callback) {
			induction.deleteinduction({coid: req.plugHRapp.user.coid, userid: req.plugHRapp.user.userid, dbid: req.param('dbid') }, function(e1,o1, tot)	{
				if (e1)		{
					resp.err = true;
					resp.text = "File NOT Deleted";
					resp.data = null;
				} else		{
					resp.err = false;
					resp.text = "File Deleted";
					resp.data = o1;
				}
				resp.totpages = tot;
				callback(e1,o1);
			});
		},
		function(callback) {
			s3.deleteObjects({
					Bucket: s3__bucket,
					Delete: {
							Objects: [
								 { Key: req.plugHRapp.user.coid + "/" + req.param('filename')}
							]
					}
			}, function(err, data) {
				if (err)		{
					resp.fd = "File NOT Deleted";
				} else		{
					resp.fd = "File Deleted";
				}
				callback(err, data);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
/*
	induction.deleteinduction({coid: req.plugHRapp.user.coid, userid: req.plugHRapp.user.userid, dbid: req.param('dbid') }, function(e,o, tot)	{
		if (e)		{
			console.log("Document not deleted");
      res.json({err:1,text:"Document not deleted"});
		}	else	{
			console.log("Document deleted");
      var doc_file = uploadPath + req.plugHRapp.user.coid + '/' + req.param('filename');
      fs.unlink(doc_file, function(err) {
        if (err) 
          console.log("Document file "+doc_file+" not deleted");
		      res.json({err:1,text:"Document deleted"});
      });
      res.json({err:0,data:o, totpages: tot});
		}
	});
*/
};

exports.showIndcdoc = function(req, res) {
  //var tempFile = __dirname +'/uploads/' + req.param("docfile");
	var xcoid = req.plugHRapp.user.coid;
	var xuserid = req.plugHRapp.user.userid;
  var tempFile = '/uploads/'+ xcoid + "/ " + req.param("filename");
  res.sendfile(tempFile, function(err) {
    if (err)
      console.log(err);
    else 
      console.log('file sent');
  });
};

exports.show315doc = function(req, res) 		{
  res.render('payadm/315doc',{curruser : req.plugHRapp.user});
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Roles (by department)
// --------------------------------------------------------------------------
var roles = require('./modules/roles-manager');

exports.show317 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			roles.getFProles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/317', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,deptdata : JSON.stringify(data1) ,mhsdata : JSON.stringify(data2) ,nhsdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show317NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			roles.getNProles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew317 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.addnewroles({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : req.param('mhs'),
				optskills : req.param('nhs'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update317 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.updateroles({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : req.param('mhs'),
				optskills : req.param('nhs'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete317 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.deleteroles({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				roletitle : req.param('title'),
				roletype : req.param('rtype'),
				depid : req.param('dept'),
				mustskills : req.param('mhs'),
				optskills : req.param('nhs'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Key REsult Areas (by department and role)
// --------------------------------------------------------------------------

var kra = require('./modules/kra-manager');

exports.show318 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			kra.getFPkra(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/318', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,deptdata : JSON.stringify(data1) ,roledata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show318NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			kra.getNPkra(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew318 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.addnewkra({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dept : req.param('dept'),
				role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update318 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.updatekra({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				dept : req.param('dept'),
				role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete318 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			kra.deletekra({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				dept : req.param('dept'),
				role : req.param('role'),
				kratitle : req.param('title'),
				kradesc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skill Requirements by Dept + role
// --------------------------------------------------------------------------
var skills = require('./modules/skills-manager');

exports.show319 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			skills.getFPskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/319', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show319NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			skills.getNPskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew319 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.addnewskills({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update319 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.updateskills({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete319 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			skills.deleteskills({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				skillname : req.param('sname'),
				skilldesc : req.param('sdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Employees Information
// --------------------------------------------------------------------------
exports.show320 = function(req, res) {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getFPemployees(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			//res.render('payadm/330', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3) ,noexpdata : JSON.stringify(data4) ,vprofdata : JSON.stringify(data4) ,profdata : JSON.stringify(data4) ,sexpdata : JSON.stringify(data4), exprtdata : JSON.stringify(data4), empdata : JSON.stringify(data5), totpages:tot});
			res.render('payadm/320', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3) ,skilldata : JSON.stringify(data4), empdata : JSON.stringify(data5), totpages:tot});
		}
	);
};
exports.users320 = function(req, res) {
	var data5;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e5, o5) 	{
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			res.json({empdata : data5});
		}
	);
};
exports.show320NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPemployees(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew320 = function(req, res) {
	var data0,  data1, data2, data5;
	var resp = new Object;
	async.series([
		function(callback) {
			users.addnewemployees({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
			});
		},
		function(callback) {
			orgstru.addneworgstru({
				coid : req.plugHRapp.user.coid,
				userid : resp.userid,
				sdate : req.param('jdate'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				purpose : '1',				// 1= New User  2 = Transfer  3 = Reorg   4 = Promotion   5 = Demotion
				updby : req.plugHRapp.user.userid,
				upddate: new Date(),
			}, function(e1,o1, tot)	{
				if (o1)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o1.text;
				}
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.update320 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.updateemployees({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete320 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.deleteemployees({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			orgstru.deleteorgstru({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Holidays List
// --------------------------------------------------------------------------

var holidaylist = require('./modules/holidaylist-manager');

exports.show322 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			holidaylist.getFPholidaylist(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/322', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show322NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			holidaylist.getNPholidaylist(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew322 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.addnewholidaylist({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update322 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.updateholidaylist({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete322 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaylist.deleteholidaylist({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				holidayname : req.param('hname'),
				holidaytype : req.param('htype'),
				holidaydate : req.param('date'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Holidays Policy
// --------------------------------------------------------------------------
var holidaypolicy = require('./modules/holidaypolicy-manager');

exports.show323 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			holidaypolicy.getFPholidaypolicy(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			holidaylist.getallholidaylist(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/323', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,appofficedata : JSON.stringify(data1) ,selholidaysdata : JSON.stringify(data2), totpages:tot});
		}
	);
};
exports.show323NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			holidaypolicy.getNPholidaypolicy(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			holidaylist.getallholidaylist(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew323 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.addnewholidaypolicy({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update323 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.updateholidaypolicy({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.delete323 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			holidaypolicy.deleteholidaypolicy({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				htitle : req.param('htitle'),
				validfrom : req.param('validfrom'),
				validto : req.param('validto'),
				appoffice : req.param('appoffice'),
				maxopholidays : req.param('maxopholidays'),
				selholiday : req.param('selholidays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Search Employees
// --------------------------------------------------------------------------

exports.show326 = function(req, res) {
	var data0,  data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data1 = null;
				callback(e0,o0);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('payadm/326', {curruser : req.plugHRapp.user, skilldata : JSON.stringify(data0), depdata : JSON.stringify(data1), locdata : JSON.stringify(data2), roledata : JSON.stringify(data3), totpages:tot});
		}
	);
};
exports.show326user = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
  var uid = parseInt(req.query.userid);

	async.parallel([
		function(callback) {
		  users.getoneusers(req.query.coid, uid, function(e, o) {
				if (o)		{
					data1 = o;
					data1.coid = req.query.coid;
				} else
					data1 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.query.coid, uid, function(e1, o1) {
				if (o1)
					data2 = o1;
				else
					data2 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.query.coid, uid, function(e2, o2) {
				if (o2)
					data3 = o2;
				else
					data3 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.query.coid, uid, function(e3, o3) {
				if (o3)
					data4 = o3;
				else
					data4 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data5 = o4;
				else
					data5 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
	   	res.render('payadm/326user', {formdata : JSON.stringify(data1), depdata : JSON.stringify(data2), roledata : JSON.stringify(data3), locdata : JSON.stringify(data4), skilldata : JSON.stringify(data5)});
		}
	);
};

exports.show326docs = function(req, res) {
	var data0, data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userdocs.getFPuserdocs(req.param('coid'), req.param('userid'), function(e, o, tot) {
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else	{
					resp.err = false;
					resp.data = null;
					resp.text = o.text;
					resp.totpages = 0;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search326 = function(req, res) {
	var data, data1, data2, data3;
	var resp = new Object;
	async.series([
		function(callback) {
			users.searchemp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				mgr : req.param('mgr'),
				role : req.param('role'),
				loc : req.param('loc'),
				dep : req.param('dep'),
				bgroup : req.param('bgrp'),
				pint : req.param('pint'),
				addr : req.param('addr'),
				univ : req.param('uni'),
				udegree : req.param('degree'),
				college : req.param('college'),
				orgn : req.param('emp'),
				skill : req.param('skill'),
			}, function(e,o, tot, searchObj)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
		      resp.sobj = searchObj;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
		      resp.sobj = searchObj;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};
exports.show326NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
		  users.searchNP(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), req.param('sobj'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Manage payadms  - part of owner functionality
// --------------------------------------------------------------------------

exports.show304 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getFPpayadms(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/304', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0)});
		}
	);
};
exports.show304NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPpayadms(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew304 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.addnewpayadms({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update304 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.updatepayadms({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete304 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.deletepayadms({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Manage Consultants
// --------------------------------------------------------------------------

exports.show327 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getFPconsult(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			//res.render('payadm/327', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3) ,noexpdata : JSON.stringify(data4) ,vprofdata : JSON.stringify(data4) ,profdata : JSON.stringify(data4) ,sexpdata : JSON.stringify(data4), exprtdata : JSON.stringify(data4), empdata : JSON.stringify(data5), totpages:tot});
			res.render('payadm/327', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2) ,locdata : JSON.stringify(data3) ,skilldata : JSON.stringify(data4), empdata : JSON.stringify(data5), totpages:tot});
		}
	);
};
exports.show327NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPconsult(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew327 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.addnewconsult({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot, userid)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
					resp.userid = userid;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
					resp.userid = userid;
				}
				callback(e,o);
				});
		},
		function(callback) {
			orgstru.addneworgstru({
				coid : req.plugHRapp.user.coid,
				userid : resp.userid,
				sdate : req.param('jdate'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				purpose : '1',				// 1= New User  2 = Transfer  3 = Reorg   4 = Promotion   5 = Demotion
				updby : req.plugHRapp.user.userid,
				upddate: new Date(),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update327 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.updateconsult({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.delete327 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.series([
		function(callback) {
			users.deleteconsult({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				dbid : req.param('dbid'),
				username : req.param('uname'),
				useremailid : req.param('email'),
				password : req.param('pass'),
				mgr : req.param('mgr'),
				dept : req.param('dep'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				location : req.param('loc'),
				desg : req.param('desg'),
				doj : req.param('jdate'),
				probdate : req.param('probdate'),
				status : req.param('empsts'),
				salary : req.param('sal'),
				emptype : req.param('etype'),
				noexp : req.param('noexp'),
				vprof : req.param('vprof'),
				prof : req.param('prof'),
				sexp : req.param('sexp'),
				exprt : req.param('exprt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		function(callback) {
			orgstru.deleteorgstru({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = '';
				} else		{
					resp.err = true;
					resp.text = o.text;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Company Induction Questions
// --------------------------------------------------------------------------
var iquestions = require('./modules/iquestions-manager');

exports.show329 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			iquestions.getFPiquestions(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/329', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show329NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			iquestions.getNPiquestions(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew329 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.addnewiquestions({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update329 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.updateiquestions({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete329 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			iquestions.deleteiquestions({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Company Leave Types
// --------------------------------------------------------------------------

var leavetypes = require('./modules/leavetypes-manager');

exports.show330 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			leavetypes.getFPleavetypes(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/330', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show330NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			leavetypes.getNPleavetypes(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew330 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.addnewleavetypes({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				lname : req.param('lname'),
				lcriteria : req.param('lctr'),
				ldays : req.param('ldays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update330 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.updateleavetypes({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				lcriteria : req.param('lctr'),
				ldays : req.param('ldays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete330 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			leavetypes.deleteleavetypes({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				lname : req.param('lname'),
				lcriteria : req.param('lctr'),
				ldays : req.param('ldays'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Organization Structure
// --------------------------------------------------------------------------

var orgstru = require('./modules/orgstru-manager');

exports.show321 = function(req, res) {
	var data0, data1, data2, data3;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.render('payadm/321', {curruser : req.plugHRapp.user, mgrdata : JSON.stringify(data0), depdata : JSON.stringify(data1), locdata : JSON.stringify(data2), roledata : JSON.stringify(data3), totpages:tot});
		}
	);
};
exports.search321 = function(req, res) {
	var data0,  data1, data3;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.searchusers({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				uid : req.param('uid'),
				uname : req.param('uname'),
				mgr : req.param('mgr'),
				dep : req.param('dep'),
				loc : req.param('loc'),
			}, function(e,o, tot, searchObj)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.text = o.text;
					resp.totpages = tot;
		      resp.sobj = searchObj;
				} else		{
					resp.err = true;
					resp.text = o.text;
					resp.data = null;
					resp.totpages = 0;
		      resp.sobj = searchObj;
				}
				callback(e,o);
				});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			resp.departments = data1;
			resp.locations = data3;
			//console.log(resp.text);
			res.json(resp);
		}
	);
};
exports.show321NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
		  users.searchNP(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), req.param('sobj'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.save321 = function(req, res) {
	var data0, data1, data2;
	var resp = new Object;

	async.parallel([
		function(callback) {
			orgstru.updateorgstru({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				sdate : req.param('sdate'),
				updby : req.plugHRapp.user.userid,
				upddate: new Date(),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = false;
					resp.text = o.text;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.updemporgstru({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				salary : req.param('salary'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.text = o.text;
				} else		{
					resp.err = false;
					resp.text = o.text;
				}
				callback(e,o);
			});
		},
		function(callback) {
			orgstru.addneworgstru({
				coid : req.plugHRapp.user.coid,
				userid : req.param('userid'),
				sdate : req.param('sdate'),
				dept : req.param('dep'),
				location : req.param('loc'),
				role : req.param('role'),
				rtype : parseInt(req.param('rtype')),
				mgr : req.param('mgr'),
				emptype : req.param('etype'),
				salary : req.param('salary'),
				comments : req.param('comments'),
				purpose : req.param('ctype'),				// 1= New User  2 = Transfer  3 = Reorg   4 = Promotion   5 = Demotion
				updby : req.plugHRapp.user.userid,
				upddate: new Date(),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Manage Review Cycle
// --------------------------------------------------------------------------

var revperiod = require('./modules/revperiod-manager');

exports.show331 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getFPrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/331', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show331NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			revperiod.getNPrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew331 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.addnewrevperiod({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				rptitle : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update331 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.updaterevperiod({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rptitle : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete331 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			revperiod.deleterevperiod({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rptitle : req.param('title'),
				sdate : req.param('sdate'),
				edate : req.param('edate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Team member goals
// --------------------------------------------------------------------------
var usergoals = require('./modules/usergoals-manager');

exports.show334 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getteammembers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/334', {curruser : req.plugHRapp.user, rperioddata : JSON.stringify(data1) ,usrdata : JSON.stringify(data2) });
		}
	);
};

exports.search334 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		// For Team member's goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.plugHRapp.user.coid,
				userid : req.param('usr'),
				rperiod : req.param('rperiod'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
				}
				callback(e,o);
				});
		},
		// For Rolewise goals from repository
		function(callback) {
			perfgoals.getperfgoalsbyrole({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				rperiod : req.param('rperiod'),
				roleid : req.param('roleid'),
			}, function(e2,o2)	{
				if (o2)		{
					resp.err = false;
					resp.data2 = o2;
				} else		{
					resp.err = true;
					resp.data2 = null;
				}
				callback(e2,o2);
				});
		},
		// For manager's self goals
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				rperiod : req.param('rperiod'),
			}, function(e1,o1)	{
				if (o1)		{
					resp.err = false;
					resp.data1 = o1;
				} else		{
					resp.err = true;
					resp.data1 = null;
				}
				callback(e1,o1);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.show334NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew334 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.addnewusergoals({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				weight : req.param('wt'),
				meets : req.param('meets'),
				submitted : '0',
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update334 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateusergoals({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete334 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.deleteusergoals({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
				mgoalid : req.param('sgoal'),
				ugoalid : req.param('ugoalid'),
				weight : req.param('wt'),
				meets : req.param('meets'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Official data for user
// --------------------------------------------------------------------------

exports.show333 = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getoneusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getrolesbydept(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.plugHRapp.user.dept, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e5, o5) {
				if (o5)
					data5 = o5;
				else
					data5 = null;
				callback(e5,o5);
			});
		},
		], function(err, results) {
			res.render('payadm/333', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), depdata : JSON.stringify(data1), roledata : JSON.stringify(data2), locdata : JSON.stringify(data3), empdata : JSON.stringify(data4), skilldata : JSON.stringify(data5)});
		}
	);
};

// --------------------------------------------------------------------------
// Company Rating Levels
// --------------------------------------------------------------------------

var rlevel = require('./modules/rlevel-manager');

exports.show335 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			rlevel.getFPrlevel(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/335', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show335NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			rlevel.getNPrlevel(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew335 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.addnewrlevel({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update335 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.updaterlevel({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete335 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			rlevel.deleterlevel({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				rlevel : req.param('rlevel'),
				rdesc : req.param('rdesc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Google Organisation Chart
// --------------------------------------------------------------------------

exports.show336 = function(req, res) 		{
	res.render('payadm/336', {curruser : req.plugHRapp.user});
};
// --------------------------------------------------------------------------
// Manage Probations
// --------------------------------------------------------------------------

exports.show337 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getFPprobusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/337', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show337NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			users.getNPprobusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.update337 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;

	async.parallel([
		function(callback) {
			users.updateprob({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				probdate : req.param('probdate'),
				status : req.param('sts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team Performance Progress
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show338 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.render('payadm/338', {curruser : req.plugHRapp.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), usrdata : JSON.stringify(data4)});
		}
	);
};
exports.search338 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.plugHRapp.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.mgr338 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updatemgrrating({
				coid : req.plugHRapp.user.coid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Skip Reports Performance Progress
// --------------------------------------------------------------------------

exports.show339 = function(req, res) {
	var data0, data1, data2, data3, data4;

	async.parallel([
		//Get All Review period List
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			rlevel.getallrlevel(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			users.getteammembers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.render('payadm/339', {curruser : req.plugHRapp.user, rperioddata : JSON.stringify(data1), goalsdata : JSON.stringify(data2), rleveldata : JSON.stringify(data3), repdata : JSON.stringify(data4)});
		}
	);
};

exports.skips339 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getteammembers(req.plugHRapp.user.coid, req.param('usr'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.search339 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.getgoalsbyperiod_user({
				coid : req.plugHRapp.user.coid,
				rperiod : req.param('rperiod'),
				userid : req.param('usr'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.mgr339 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			usergoals.updateskiprating({
				coid : req.plugHRapp.user.coid,
				dbid : req.param('dbid'),
				rating : req.param('rating'),
				userid : req.param('userid'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage PIP - Freeze
// --------------------------------------------------------------------------
var userpips = require('./modules/userpips-manager');

exports.show340 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			userpips.getFPuserpips(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('payadm/340', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,usrdata : JSON.stringify(data1), totpages:tot});
		}
	);
};
exports.show340NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			userpips.getNPuserpips(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew340 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.addnewuserpips({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update340 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.updateuserpips({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete340 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userpips.deleteuserpips({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				userid : req.param('usr'),
				reason : req.param('rsn'),
				sdate : req.param('sdt'),
				edate : req.param('edt'),
				status : req.param('sts'),
				comments : req.param('cmts'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Performance Appraisal
// --------------------------------------------------------------------------

var apprsl = require('./modules/apprsl-manager');

exports.show341 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e0, o0) {
				if (o0)
					data0 = o0;
				else
					data0 = null;
				callback(e0,o0);
			});
		},
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/341', {curruser : req.plugHRapp.user, rperioddata : JSON.stringify(data0), depdata : JSON.stringify(data1), usrdata : JSON.stringify(data2) });
		}
	);
};

exports.recalc341 = function(req, res) 	{
	var data0,  data1, data2;
	var dusers;
	var resp = new Object;
	var srat = 0.0, mrat = 0.0, krat = 0.0;
	resp.data0 = [];
	async.series([
		// For Team member's goals
		function(callback) {
			users.getusersByDept({ coid : req.plugHRapp.user.coid, depid : req.param('dep')}, function(e,o)	{
				if (o)		{
					resp.err = false;
					//resp.data0 = o;
					dusers = o;
				} else		{
					resp.err = true;
					//resp.data0 = null;
					dusers = null;
				}
				callback(e,o);
				});
		},
		function(callback) {
		  async.forEachSeries(dusers, function(dusr, callback) {
				usergoals.getgoalsbyperiod_user({coid : req.plugHRapp.user.coid, rperiod: req.param('rperiod'), userid : dusr.userid}, function(e1,o1)	{
					if (o1)		{
						resp.err = false;
						srat=0.0;
						mrat=0.0;
						krat=0.0;
						for (var i in o1)			{
							var goal = o1[i];
							srat += goal.selfrating *(goal.weight / 100);
							mrat += goal.mgrrating *(goal.weight / 100);
							krat += goal.skiprating *(goal.weight / 100);
						}
						dusr.srat = srat.toString();
						dusr.mrat = mrat;
						dusr.krat = krat;
						resp.data0.push(dusr);
						callback();
					} else		{
						resp.err = true;
						callback();
					}
				});
			}, function(err, results) {
				callback();
			});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};

exports.search341 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	resp.data0 = [];
	async.series([
		function(callback) {
			apprsl.getByDept_rp({coid:req.plugHRapp.user.coid, depid:req.param('dep'), rperiod:req.param('rperiod')}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data0 = o;
					dusers = o;
				} else		{
					resp.err = true;
					resp.data0 = null;
					dusers = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};

exports.show341NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			usergoals.getNPusergoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			revperiod.getallrevperiod(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			usergoals.getallusergoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					data3 = o3;
				else
					data3 = null;
				callback(e3,o3);
			});
		},
		function(callback) {
			perfgoals.getallperfgoals(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e4, o4) {
				if (o4)
					data4 = o4;
				else
					data4 = null;
				callback(e4,o4);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.save341 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;

	var dusers = req.param('data');
	
	async.series([
		function(callback1) {
			apprsl.delapprsl_dept_rp({coid : req.plugHRapp.user.coid,rperiod : req.param('rperiod'),dept : req.param('dept')}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.text = "Deleted";
					callback1();
				} else		{
					resp.err = true;
					resp.text = "Not Deleted";
					callback1();
				}
				callback1();
			});
		},
		function(callback) {
		  async.forEachSeries(dusers, function(dusr, callback) {
				apprsl.addnewapprsl({coid:req.plugHRapp.user.coid, userid:dusr.userid, rperiod:dusr.rperiod, dept:dusr.dept,	mgr:dusr.mgr,	srat:dusr.srat, mrat:dusr.mrat,	krat:dusr.krat, frat:dusr.frat}, function(e1,o1)	{
					if (o1)		{
						resp.err = false;
						resp.text = "Appraisal Added";
						callback();
					} else		{
						resp.err = true;
						resp.text = "Appraisal not added";
						callback();
					}
				});
			}, function(err, results) {
				callback();
			});
		},
		], function(err, results) {
				res.json(resp);
		}
	);
};


// --------------------------------------------------------------------------
// Exit Actions List
// --------------------------------------------------------------------------

var exitacts = require('./modules/exitacts-manager');

exports.show342 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			exitacts.getFPexitacts(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/342', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show342NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			exitacts.getNPexitacts(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew342 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.addnewexitacts({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update342 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.updateexitacts({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete342 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			exitacts.deleteexitacts({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Income Tax brackets
// --------------------------------------------------------------------------

var itslabs = require('./modules/itslabs-manager');

exports.show343 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			itslabs.getFPitslabs(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/343', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show343NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			itslabs.getNPitslabs(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew343 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.addnewitslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update343 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.updateitslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete343 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			itslabs.deleteitslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				taxrate : req.param('trate'),
				min : req.param('min'),
				max : req.param('max'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Professional Tax brackets
// --------------------------------------------------------------------------

var ptslabs = require('./modules/ptslabs-manager');

exports.show344 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			ptslabs.getFPptslabs(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/344', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show344NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			ptslabs.getNPptslabs(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew344 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.addnewptslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update344 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.updateptslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete344 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			ptslabs.deleteptslabs({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				min : req.param('min'),
				max : req.param('max'),
				ptamt : req.param('pt'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Salary Components
// --------------------------------------------------------------------------

var salcomp = require('./modules/salcomp-manager');

exports.show345 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salcomp.getFPsalcomp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/345', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), totpages:tot});
		}
	);
};
exports.show345NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			salcomp.getNPsalcomp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};
exports.addnew345 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.addnewsalcomp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				title : req.param('title'),
				desc : req.param('desc'),
				type : req.param('type'),
				isbasic : req.param('isb'),
				ctcpercnt : req.param('ctcp'),
				basicpercnt : req.param('basp'),
				yearmax : req.param('ymax'),
				catg : req.param('catg'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update345 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.updatesalcomp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				type : req.param('type'),
				isbasic : req.param('isb'),
				ctcpercnt : req.param('ctcp'),
				basicpercnt : req.param('basp'),
				yearmax : req.param('ymax'),
				catg : req.param('catg'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete345 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salcomp.deletesalcomp({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				title : req.param('title'),
				desc : req.param('desc'),
				type : req.param('type'),
				isbasic : req.param('isb'),
				ctcpercnt : req.param('ctcp'),
				basicpercnt : req.param('basp'),
				yearmax : req.param('ymax'),
				catg : req.param('catg'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Monthly Salaries
// --------------------------------------------------------------------------

var salaries = require('./modules/salaries-manager');

exports.show346 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salaries.getFPsalaries(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			salstru.getallsalstru(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data1 = o;
				else
					data1 = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/346', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0), strudata : JSON.stringify(data1), totpages:tot});
		}
	);
};

exports.search346 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.getSalaryByUser({
				coid : req.plugHRapp.user.coid,
				userid : req.param('usr'),
				month : req.param('mon'),
				year : req.param('year'),
				stru : req.param('stru'),
			}, function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew346 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.addnewsalaries({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update346 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.updatesalaries({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete346 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salaries.deletesalaries({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				month : req.param('mon'),
				year : req.param('year'),
				userid : req.param('usr'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Manage Salary Structures
// --------------------------------------------------------------------------
var salstru = require('./modules/salstru-manager');

exports.show347 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			salstru.getFPsalstru(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		function(callback) {
			salcomp.getallsalcomp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('payadm/347', {curruser : req.plugHRapp.user, formdata : JSON.stringify(data0) ,scdata : JSON.stringify(data1), totpages:tot});
		}
	);
};

exports.show347NP = function(req, res) {
	var data0,  data1, data2;
	async.parallel([
		function(callback) {
			salstru.getNPsalstru(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('newpage'), function(e, o) {
				if (o)
					data0 = o;
				else
					data0 = null;
				callback(e,o);
			});
		},
		function(callback) {
			salcomp.getallsalcomp(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.send(data0);
		}
	);
};

exports.addnew347 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.addnewsalstru({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.update347 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.updatesalstru({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

exports.delete347 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			salstru.deletesalstru({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				sstitle : req.param('title'),
				ssdesc : req.param('desc'),
				ssearnings : req.param('inc'),
				ssdeductions : req.param('ded'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Roles defined for each department
// --------------------------------------------------------------------------

var usergoals = require('./modules/usergoals-manager');
var kra = require('./modules/kra-manager');
var roles = require('./modules/roles-manager');
var rlevel = require('./modules/rlevel-manager');

exports.show351 = function(req, res) {
	var data0,  data1, data2;

	async.parallel([
		//Get All Review period List
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('payadm/351', {curruser : req.plugHRapp.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search351 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			roles.getrolesbydept(req.plugHRapp.user.coid,req.plugHRapp.user.userid, req.param('dept'), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		function(callback) {
			skills.getallskills(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			resp.mhsdata = data2;
			resp.nhsdata = data2;
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Department Induction Questions
// --------------------------------------------------------------------------
var deptindcq = require('./modules/deptindcq-manager');

exports.show353 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			induction.getallinduction(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o, t) {
				if (o)
					data0 = o;
				else
					data0 = null;
				tot = t;
				callback(e,o);
			});
		},
		], function(err, results) {
			res.render('payadm/353', {curruser : req.plugHRapp.user, indcdata : JSON.stringify(data0)});
		}
	);
};

exports.search353 = function(req, res) 	{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.getquesByIndc(req.plugHRapp.user.coid, req.plugHRapp.user.userid, req.param('indc'),function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
				});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

exports.addnew353 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.addnewdeptindcq({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.update353 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.updatedeptindcq({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};
exports.delete353 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			deptindcq.deletedeptindcq({
				coid : req.plugHRapp.user.coid,
				userid : req.plugHRapp.user.userid,
				dbid : req.param('dbid'),
				indc : req.param('indc'),
				qtext : req.param('qtext'),
				a1text : req.param('a1text'),
				a2text : req.param('a2text'),
				a3text : req.param('a3text'),
				a4text : req.param('a4text'),
				answer : req.param('ans'),
			}, function(e,o, tot)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
					resp.totpages = tot;
				} else		{
					resp.err = true;
					resp.data = null;
					resp.totpages = 0;
				}
				callback(e,o);
				});
		},
			], function(err, results) {
				res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Team profile
// --------------------------------------------------------------------------
exports.show354 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			department.getalldepartment(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		], function(err, results) {
			res.render('owner/254', {curruser : req.plugHRapp.user, deptdata : JSON.stringify(data1)});
		}
	);
};
exports.search354 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersByDept({coid:req.plugHRapp.user.coid, userid:req.plugHRapp.user.userid, depid:req.param("rep")}, function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				} else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					resp.data1 = o1;
				else
					resp.data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			roles.getallroles(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					resp.data2 = o2;
				else
					resp.data2 = null;
				callback(e2,o2);
			});
		},
		function(callback) {
			locations.getalllocations(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e3, o3) {
				if (o3)
					resp.data3 = o3;
				else
					resp.data3 = null;
				callback(e3,o3);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Induction Results Report
// --------------------------------------------------------------------------

var deptindcq = require('./modules/deptindcq-manager');

exports.show355 = function(req, res) {
	var data0,  data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e1, o1) {
				if (o1)
					data1 = o1;
				else
					data1 = null;
				callback(e1,o1);
			});
		},
		function(callback) {
			induction.getallinduction(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e2, o2) {
				if (o2)
					data2 = o2;
				else
					data2 = null;
				callback(e2,o2);
			});
		},
		], function(err, results) {
			res.render('payadm/355', {curruser : req.plugHRapp.user, usrdata : JSON.stringify(data1) ,indcdata : JSON.stringify(data2), totpages:tot});
		}
	);
};

exports.search355 = function(req, res) 		{
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			users.getusersById(req.plugHRapp.user.coid, req.param("usr"), function(e,o)	{
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// Upload documents to user profile
// --------------------------------------------------------------------------
var userdocs = require('./modules/userdocs-manager');

exports.show356 = function(req, res) {
	users.getallusers(req.plugHRapp.user.coid, req.plugHRapp.user.userid, function(e, o) {
		if (e)
			res.render('payadm/356', {curruser : req.plugHRapp.user, empdata : null });
		else  {
			res.render('payadm/356', {curruser : req.plugHRapp.user, empdata : JSON.stringify(o)});
		}
	});
};

exports.search356 = function(req, res) {
	var data0,  data1, data2;
	var resp = new Object;
	async.parallel([
		function(callback) {
			userdocs.getalluserdocs(req.plugHRapp.user.coid, req.param('emp'), function(e, o) {
				if (o)		{
					resp.err = false;
					resp.data = o;
				}	else		{
					resp.err = true;
					resp.data = null;
				}
				callback(e,o);
			});
		},
		], function(err, results) {
			res.json(resp);
		}
	);
};

// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------



// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// -----
// --------------------------------------------------------------------------


