if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPusergoals = function(coid, userid, callback)   {
	var usergoals  = dbConn.collection(coid+'_usergoals');

	usergoals.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			usergoals.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'usergoals not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading usergoals'});
	});
};

exports.getNPusergoals = function(coid, userid, pageno, callback)   {
	var usergoals  = dbConn.collection(coid+'_usergoals');

	usergoals.find({}).sort({_id:1}).skip((pageno-1) * NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'usergoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallusergoals = function(coid, userid, callback)   {
	var usergoals  = dbConn.collection(coid+'_usergoals');

	usergoals.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'usergoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getgoalsbyuser = function(searchData, callback)   {
	var usergoals  = dbConn.collection(searchData.coid+'_usergoals');

	usergoals.find({userid:(searchData.userid).toString()}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'usergoals not found'});
		else
			callback(false,o);
	});
}

exports.getgoalsbyperiod = function(searchData, callback)   {
	var usergoals  = dbConn.collection(searchData.coid+'_usergoals');

	usergoals.find({ rperiod:searchData.rperiod }).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'usergoals not found'});
		else
			callback(false,o);
	});
}

exports.getgoalsbyperiod_user = function(searchData, callback)   {
	var usergoals  = dbConn.collection(searchData.coid+'_usergoals');
	//console.log(searchData);

	usergoals.find({rperiod:searchData.rperiod, userid:(searchData.userid).toString()}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			usergoals.find({rperiod:searchData.rperiod, userid:(searchData.userid).toString()}).sort({_id:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'usergoals not found'});
				else  {
					callback(false,o, tot);
				}
			});
		} else
			callback(true,{err:2,text:'Error in reading usergoals'});
	});
}

exports.addnewusergoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	newData.date = new Date();
	newData.userid = newData.userid;
	newData.mgoalid = newData.mgoalid;
	newData.ugoalid = newData.ugoalid;
	newData.weight = newData.weight;
	newData.meets = newData.meets;

	newData.selfrating = 0;

	newData.task1desc = '';
	newData.task1date = '';
	newData.task1status = 0;		// incomplete

	newData.task2desc = '';
	newData.task2date = '';
	newData.task2status = 0;		// incomplete

	newData.task3desc = '';
	newData.task3date = '';
	newData.task3status = 0;		// incomplete

	newData.task4desc = '';
	newData.task4date = '';
	newData.task4status = 0;		// incomplete

	newData.task5desc = '';
	newData.task5date = '';
	newData.task5status = 0;		// incomplete

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	//delete newData.userid;

	usergoals.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'usergoals not added'});
		else   {
			//getFPusergoals(scoid,suserid, function(e2,o2, totpages)     {
			getgoalsbyperiod_user({coid:scoid,userid:suserid,rperiod:newData.rperiod}, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table usergoals data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.updategoaltask = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	usergoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			if (newData.tid == '1')			{
				o.task1desc = newData.tdesc;
				o.task1date = newData.tdate;
				o.task1status = newData.tsts;
			}
			if (newData.tid == '2')			{
				o.task2desc = newData.tdesc;
				o.task2date = newData.tdate;
				o.task2status = newData.tsts;
			}
			if (newData.tid == '3')			{
				o.task3desc = newData.tdesc;
				o.task3date = newData.tdate;
				o.task3status = newData.tsts;
			}
			if (newData.tid == '4')			{
				o.task4desc = newData.tdesc;
				o.task4date = newData.tdate;
				o.task4status = newData.tsts;
			}
			if (newData.tid == '5')			{
				o.task5desc = newData.tdesc;
				o.task5date = newData.tdate;
				o.task5status = newData.tsts;
			}
			usergoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'usergoals not updated'});
				else   {
					getFPusergoals(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table usergoals data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in usergoals'});
		}
	});
}

exports.updateselfrating = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	usergoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.selfrating = newData.rating;
			usergoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Self Rating not updated'});
				else 
					callback(true,{err:2,text:'Self rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in usergoals'});
		}
	});
}

exports.updatemgrrating = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	usergoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.mgrrating = newData.rating;
			usergoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Manager Rating not updated'});
				else 
					callback(true,{err:2,text:'Manager rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in usergoals'});
		}
	});
}

exports.updateskiprating = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	usergoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.skiprating = newData.rating;
			usergoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Skip Manager Rating not updated'});
				else 
					callback(true,{err:2,text:'Skip Manager rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in usergoals'});
		}
	});
}

exports.commitgoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
	usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{trgt_submited:"1"}}, {multi: true},  function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Performance progress not submitted'});
		else 
			callback(true,{err:0,text:'Performance progress submitted'});
	});
}

exports.submitgoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
	usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{self_submited:"1"}}, {multi: true},  function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Performance progress not submitted'});
		else 
			callback(true,{err:0,text:'Performance progress submitted'});
	});
}
exports.msubmitgoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
	usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{supr_submited:"1"}}, {multi: true},  function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Performance progress not submitted'});
		else 
			callback(true,{err:0,text:'Performance progress submitted'});
	});
}
exports.ssubmitgoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
	usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{skip_submited:"1"}}, {multi: true},  function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Performance progress not submitted'});
		else 
			callback(true,{err:0,text:'Performance progress submitted'});
	});
}
exports.resetgoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
  if(newData.reset_type == "1")   {
	  usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{trgt_submited:"0"}}, {multi: true},  function(e, o)   {
		  if (e)
			  callback(true,{err:1,text:'Performance progress not submitted'});
		  else 
			  callback(true,{err:0,text:'Performance progress submitted'});
	  });
	}
  if(newData.reset_type == "2")   {
	  usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{self_submited:"0"}}, {multi: true},  function(e, o)   {
		  if (e)
			  callback(true,{err:1,text:'Performance progress not submitted'});
		  else 
			  callback(true,{err:0,text:'Performance progress submitted'});
	  });
	}
  if(newData.reset_type == "3")   {
	  usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{supr_submited:"0"}}, {multi: true},  function(e, o)   {
		  if (e)
			  callback(true,{err:1,text:'Performance progress not submitted'});
		  else 
			  callback(true,{err:0,text:'Performance progress submitted'});
	  });
	}
  if(newData.reset_type == "4")   {
	  usergoals.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{skip_submited:"0"}}, {multi: true},  function(e, o)   {
		  if (e)
			  callback(true,{err:1,text:'Performance progress not submitted'});
		  else 
			  callback(true,{err:0,text:'Performance progress submitted'});
	  });
	}
}

exports.updateusergoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');

	usergoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.rperiod = newData.rperiod;
			o.userid = newData.userid;
			o.mgoalid = newData.mgoalid;
			o.ugoalid = newData.ugoalid;
			o.weight = newData.weight;
			o.meets = newData.meets;
			delete o.coid;
			usergoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'usergoals not updated'});
				else   {
					//getFPusergoals(newData.coid,newData.userid, function(e2,o2, totpages)     {
					getgoalsbyperiod_user({coid:newData.coid,userid:newData.userid, rperiod:newData.rperiod}, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table usergoals data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in usergoals'});
		}
	});
}
exports.deleteusergoals = function(newData, callback)   {
	var usergoals  = dbConn.collection(newData.coid+'_usergoals');
	//console.log(newData);
	usergoals.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'usergoals data not deleted'});
		else   {
			//getFPusergoals(newData.coid,newData.userid, function(e2,o2, totpages)     {
			getgoalsbyperiod_user({coid:newData.coid,userid:newData.userid, rperiod:newData.rperiod}, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table usergoals data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPusergoals = function(coid, userid, callback)   {
	var usergoals  = dbConn.collection(coid+'_usergoals');

	usergoals.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			usergoals.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'usergoals not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading usergoals'});
	});
}

var getgoalsbyperiod_user = function(searchData, callback)   {
	var usergoals  = dbConn.collection(searchData.coid+'_usergoals');

	usergoals.find({rperiod:searchData.rperiod, userid:(searchData.userid).toString()}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			usergoals.find({rperiod:searchData.rperiod, userid:(searchData.userid).toString()}).sort({_id:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'usergoals not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading usergoals'});
	});

}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

