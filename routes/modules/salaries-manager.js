if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPsalaries = function(coid, userid, callback)   {
	var salaries  = dbConn.collection(coid+'_salaries');

	salaries.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salaries.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salaries not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salaries'});
	});
};
exports.getNPsalaries = function(coid, userid, pageno, callback)   {
	var salaries  = dbConn.collection(coid+'_salaries');

	salaries.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salaries not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallsalaries = function(coid, userid, callback)   {
	var salaries  = dbConn.collection(coid+'_salaries');

	salaries.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salaries not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getSalaryByUser = function(searchData, callback)   {
	var salaries  = dbConn.collection(searchData.coid+'_salaries');

	salaries.find({month:searchData.mon, year:searchData.year, userid:searchData.userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Salary data not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewsalaries = function(newData, callback)   {
	var salaries  = dbConn.collection(newData.coid+'_salaries');

	newData.date = new Date();
	newData.year = newData.year;
	newData.userid = newData.userid;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	salaries.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'salaries not added'});
		else   {
			getFPsalaries(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table salaries data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatesalaries = function(newData, callback)   {
	var salaries  = dbConn.collection(newData.coid+'_salaries');

	salaries.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.month = newData.month;
			o.year = newData.year;
			o.userid = newData.userid;
			delete o.coid;
			salaries.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'salaries not updated'});
				else   {
					getFPsalaries(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table salaries data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in salaries'});
		}
	});
}
exports.deletesalaries = function(newData, callback)   {
	var salaries  = dbConn.collection(newData.coid+'_salaries');

	salaries.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'salaries data not deleted'});
		else   {
			getFPsalaries(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table salaries data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPsalaries = function(coid, userid, callback)   {
	var salaries  = dbConn.collection(coid+'_salaries');

	salaries.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salaries.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salaries not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salaries'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

