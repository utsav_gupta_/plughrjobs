if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPemplvbal = function(coid, userid, callback)   {
	var emplvbal  = dbConn.collection(coid+'_emplvbal');

	emplvbal.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			emplvbal.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'emplvbal not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading emplvbal'});
	});
};
exports.getNPemplvbal = function(coid, userid, pageno, callback)   {
	var emplvbal  = dbConn.collection(coid+'_emplvbal');

	emplvbal.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'emplvbal not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallemplvbal = function(coid, userid, callback)   {
	var emplvbal  = dbConn.collection(coid+'_emplvbal');

	emplvbal.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'emplvbal not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getemplvbal = function(coid, userid, callback)   {
	var emplvbal  = dbConn.collection(coid+'_emplvbal');

	emplvbal.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'emplvbal not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewemplvbal = function(newData, callback)   {
	var emplvbal  = dbConn.collection(newData.coid+'_emplvbal');

	newData.date = new Date();
	newData.pbal = newData.pbal;
	newData.totav = newData.totav;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;

	emplvbal.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'emplvbal not added'});
		else   {
			getemplvbal(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table emplvbal data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateemplvbal = function(newData, callback)   {
	var emplvbal  = dbConn.collection(newData.coid+'_emplvbal');

	emplvbal.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.ltype = newData.ltype;
			o.pbal = newData.pbal;
			o.totav = newData.totav;
			delete o.coid;
			emplvbal.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'emplvbal not updated'});
				else   {
					getemplvbal(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table emplvbal data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in emplvbal'});
		}
	});
}
exports.deleteemplvbal = function(newData, callback)   {
	var emplvbal  = dbConn.collection(newData.coid+'_emplvbal');

	emplvbal.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'emplvbal data not deleted'});
		else   {
			getemplvbal(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table emplvbal data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getemplvbal = function(coid, userid, callback)   {
	var emplvbal  = dbConn.collection(coid+'_emplvbal');

	emplvbal.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'emplvbal not found'});
		else    {
			callback(false,o);
		}
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

