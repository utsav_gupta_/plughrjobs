if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.adduseronbd = function(newData, callback)   {
	var useronbd  = dbConn.collection(newData.coid+'_users');

	//console.log(newData);
	useronbd.findOne({userid:parseInt(newData.userid)}, function(e, o)   {
		if (o)			{
			//console.log(o.onbdacts);
			if (o.onbdacts == null)			{
				delete o.onbdacts;
				useronbd.save(o, {safe: true}, function (e1,o1)      {
					if (e1)
						callback(true,{err:1,text:'Onboarding status not updated'});
					else			{
						useronbd.update({userid:parseInt(newData.userid)}, { $push : {"onbdacts":newData.onbdacts}}, {safe: true}, function (e1,o1)      {
							if (e1)
								callback(true,{err:1,text:'User Onboarding status not added'});
							else   {
								getalluseronbd(newData.coid,newData.userid, function(e2,o2, totpages)     {
								if (e2)
									callback(true,{err:2,text:'User Onboarding status data added'});
								else
									callback(false,o2, totpages);
								});
							}
						});
					}
				});
			} else		{
				useronbd.update({userid:parseInt(newData.userid)}, { $push : {"onbdacts":newData.onbdacts}}, {safe: true}, function (e1,o1)      {
					if (e1)
						callback(true,{err:1,text:'User Onboarding status not added'});
					else   {
						getalluseronbd(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'User Onboarding status data added'});
						else
							callback(false,o2, totpages);
						});
					}
				});
			}
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}

exports.updateuseronbd = function(newData, callback)   {
	var useronbd  = dbConn.collection(newData.coid+'_users');
	
	useronbd.findOne({userid:parseInt(newData.userid)}, function(e, o)   {
		if (o)   {
			useronbd.update({userid:parseInt(newData.userid), "onbdacts.title":newData.title, "onbdacts.desc":newData.desc}, { $set : {"onbdacts.$.status":newData.status}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Onboarding status not updated'});
				else   {
					getalluseronbd(newData.coid,newData.userid, function(e2,o2)     {
						if (e2)
							callback(true,{err:2,text:'Onboarding status not updated'});
						else
							callback(false,o2);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:'Employee not found'});
		}
	});
}

var getalluseronbd = function(coid, userid, callback)   {
	var useronbd  = dbConn.collection(coid+'_users');

	useronbd.findOne({userid:parseInt(userid) }, function(e, o)   {
		if (e)
			callback(true,{err:1,text:'useronbd not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

