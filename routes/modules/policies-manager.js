exports.getallpolicies = function(coid, userid, callback)   {
	var policies  = dbConn.collection(coid+'_policies');

	policies.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'policies not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewpolicies = function(newData, callback)   {
	var policies  = dbConn.collection(newData.coid+'_policies');

	policies.findOne({policyid: newData.policyid}, function(e, o)   {
		if (o)
			callback(true,{err:1,text:'policies data already exists'});
		else    {
			newData.date = new Date();
			newData.policytitle = newData.policytitle;
			var scoid = newData.coid;
			delete newData.coid;
			var suserid = newData.userid;
			delete newData.userid;

			policies.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'policies not added'});
				else   {
					GetAllpolicies(scoid,suserid, function(e2,o2)     {
					if (e2)
						callback(true,{err:2,text:'Table policies data added'});
					else
						callback(false,o2);
					});
				}
			});
		}
	});
}
exports.updatepolicies = function(newData, callback)   {
	var policies  = dbConn.collection(newData.coid+'_policies');

	policies.findOne({policyid: newData.policyid}, function(e, o)   {
		if (o)   {
			o.policyid = newData.policyid;
			o.policytitle = newData.policytitle;
			delete o.coid;
			policies.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'policies not updated'});
				else   {
					GetAllpolicies(newData.coid,newData.userid, function(e2,o2)     {
						if (e2)
							callback(true,{err:2,text:'Table policies data updated'});
						else
							callback(false,o2);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in policies'});
		}
	});
}
exports.deletepolicies = function(newData, callback)   {
	var policies  = dbConn.collection(newData.coid+'_policies');

	policies.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'policies data not deleted'});
		else   {
			GetAllpolicies(newData.coid,newData.userid, function(e2,o2)     {
				if (e2)
					callback(true,{err:2,text:'Table policies data deleted'});
				else
					callback(false,o2);
			});
		}
	});
}
var GetAllpolicies = function(coid, userid, callback)   {
	var policies  = dbConn.collection(coid+'_policies');

	var query = {};
	policies.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Error in reading policies data'});
		else
			callback(false,o);
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

