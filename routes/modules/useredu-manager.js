if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuseredu = function(coid, userid, callback)   {
	var useredu  = dbConn.collection(coid+'_users');

	useredu.find({userid:userid}, {edudata:1}).toArray(function(e1, o1)   {
		if (!e1)      {
			if (o1[0].edudata)		{
				var tot = o1[0].edudata.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			useredu.find({userid:userid}, {edudata:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {edudata:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'useredu not found'});
				else	{
					console.log(tot);
					callback(false,o, tot);
				}
			});
		} else
			callback(true,{err:2,text:'Error in reading useredu'});
	});
};
exports.getNPuseredu = function(coid, userid, pageno, callback)   {
	var useredu  = dbConn.collection(coid+'_users');

	useredu.find({userid:userid}, {edudata:{ $slice: [(pageno-1)*NUMBER_OF_ITEMS_PER_PAGE, NUMBER_OF_ITEMS_PER_PAGE]}}, {edudata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'useredu not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalluseredu = function(coid, userid, callback)   {
	var useredu  = dbConn.collection(coid+'_users');

	useredu.find({userid:userid}, {edudata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'useredu not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewuseredu = function(newData, callback)   {
	var useredu  = dbConn.collection(newData.coid+'_users');

	useredu.find({userid:newData.userid}, {edudata:1}).toArray(function(e, o)   {
		if (o)			{
		  var edObj = new Object;
			edObj.udegree = newData.udegree;
			edObj.edulevel = newData.edulevel;
			edObj.cyear = newData.cyear;
			edObj.univ = newData.univ;
			edObj.college = newData.college;
		  edObj.date = new Date();

			useredu.update({userid:newData.userid}, { $push : {"edudata":edObj}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'useredu not added'});
				else   {
					getFPuseredu(newData.coid,newData.userid, function(e2,o2, totpages)     {
					if (e2)
						callback(true,{err:2,text:'Table useredu data added'});
					else
						callback(false,o2, totpages);
					});
				}
			});
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}
exports.updateuseredu = function(newData, callback)   {
	var useredu  = dbConn.collection(newData.coid+'_users');

	useredu.findOne({_id:getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			useredu.update({_id:getObjectId(newData.dbid), "edudata.udegree":newData.old_udegree, "edudata.edulevel":newData.old_dlevel, "edudata.cyear":newData.old_dyear, "edudata.univ":newData.old_duni, "edudata.college":newData.old_dcollege}, { $set : {"edudata.$.udegree":newData.udegree, "edudata.$.edulevel":newData.edulevel, "edudata.$.cyear":newData.cyear, "edudata.$.univ":newData.univ, "edudata.$.college":newData.college }}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'useredu not updated'});
				else   {
					getFPuseredu(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table useredu data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in useredu'});
		}
	});
}
exports.deleteuseredu = function(newData, callback)   {
	var useredu  = dbConn.collection(newData.coid+'_users');

	//console.log(newData);

	useredu.findOne({_id:getObjectId(newData.dbid)}, {edudata:1}, function(e, o)   {
		//console.log(o);
		if (o)   {
			useredu.update({_id:getObjectId(newData.dbid)}, { $pull : {"edudata": {udegree : newData.udegree, edulevel : newData.edulevel, cyear : newData.cyear, univ : newData.univ, college : newData.college }}}, {safe: true}, function (e1,o1)      {
				if (e1) 
					callback(true,{err:1,text:'useredu data not deleted'});
				else   {
					getFPuseredu(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table useredu data deleted'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in useredu'});
		}
	});
}
var getFPuseredu = function(coid, userid, callback)   {
	var useredu  = dbConn.collection(coid+'_users');

	useredu.find({userid:userid}, {edudata:1}).toArray(function(e1, o1)   {
		if (!e1)      {
			if (o1[0].edudata)		{
				var tot = o1[0].edudata.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			useredu.find({userid:userid}, {edudata:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {edudata:1}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'useredu not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading useredu'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

