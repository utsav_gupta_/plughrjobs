if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPdeptindcq = function(coid, userid, callback)   {
	var deptindcq  = dbConn.collection(coid+'_deptindcq');

	deptindcq.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);

			deptindcq.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'deptindcq not found'});
				else
					callback(false,o, tot, o1);
			});
		} else
			callback(true,{err:2,text:'Error in reading deptindcq'});
	});
};
exports.getNPdeptindcq = function(coid, userid, pageno, callback)   {
	var deptindcq  = dbConn.collection(coid+'_deptindcq');

	deptindcq.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'deptindcq not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalldeptindcq = function(coid, userid, callback)   {
	var deptindcq  = dbConn.collection(coid+'_deptindcq');

	deptindcq.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);

			deptindcq.find({}).sort({_id:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'deptindcq not found'});
				else
					callback(false,o, tot, o1);
			});
		} else
			callback(true,{err:2,text:'Error in reading deptindcq'});
	});
}

exports.getquesByIndc = function(coid, userid, indcid, callback)   {
	var deptindcq  = dbConn.collection(coid+'_deptindcq');

	deptindcq.find({userid:userid, indc:indcid}).sort({_id:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No questions found'});
		else
			callback(false,o);
	});
}

exports.addnewdeptindcq = function(newData, callback)   {
	var deptindcq  = dbConn.collection(newData.coid+'_deptindcq');

	newData.date = new Date();
	newData.indc = newData.indc;
	newData.a1text = newData.a1text;
	newData.a2text = newData.a2text;
	newData.a3text = newData.a3text;
	newData.a4text = newData.a4text;
	newData.answer = newData.answer;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	deptindcq.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'deptindcq not added'});
		else   {
			getFPdeptindcq(scoid,suserid, newData.indc, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table deptindcq data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatedeptindcq = function(newData, callback)   {
	var deptindcq  = dbConn.collection(newData.coid+'_deptindcq');

	deptindcq.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.indc = newData.indc;
			o.qtext = newData.qtext;
			o.a1text = newData.a1text;
			o.a2text = newData.a2text;
			o.a3text = newData.a3text;
			o.a4text = newData.a4text;
			o.answer = newData.answer;
			delete o.coid;
			deptindcq.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'deptindcq not updated'});
				else   {
					getFPdeptindcq(newData.coid,newData.userid, newData.indc, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table deptindcq data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in deptindcq'});
		}
	});
}
exports.deletedeptindcq = function(newData, callback)   {
	var deptindcq  = dbConn.collection(newData.coid+'_deptindcq');

	deptindcq.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'deptindcq data not deleted'});
		else   {
			getFPdeptindcq(newData.coid,newData.userid, newData.indc, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table deptindcq data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPdeptindcq = function(coid, userid, indcid, callback)   {
	var deptindcq  = dbConn.collection(coid+'_deptindcq');

	deptindcq.find({indc:indcid}).sort({_id:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No questions found'});
		else
			callback(false,o);
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

