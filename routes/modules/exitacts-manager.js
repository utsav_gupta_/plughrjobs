if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPexitacts = function(coid, userid, callback)   {
	var exitacts  = dbConn.collection(coid+'_exitacts');

	exitacts.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			exitacts.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'exitacts not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading exitacts'});
	});
};
exports.getNPexitacts = function(coid, userid, pageno, callback)   {
	var exitacts  = dbConn.collection(coid+'_exitacts');

	exitacts.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exitacts not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallexitacts = function(coid, userid, callback)   {
	var exitacts  = dbConn.collection(coid+'_exitacts');

	exitacts.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exitacts not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewexitacts = function(newData, callback)   {
	var exitacts  = dbConn.collection(newData.coid+'_exitacts');

	newData.date = new Date();
	newData.desc = newData.desc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	exitacts.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'exitacts not added'});
		else   {
			getFPexitacts(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table exitacts data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateexitacts = function(newData, callback)   {
	var exitacts  = dbConn.collection(newData.coid+'_exitacts');

	exitacts.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.desc = newData.desc;
			delete o.coid;
			exitacts.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'exitacts not updated'});
				else   {
					getFPexitacts(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table exitacts data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in exitacts'});
		}
	});
}
exports.deleteexitacts = function(newData, callback)   {
	var exitacts  = dbConn.collection(newData.coid+'_exitacts');

	exitacts.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'exitacts data not deleted'});
		else   {
			getFPexitacts(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table exitacts data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPexitacts = function(coid, userid, callback)   {
	var exitacts  = dbConn.collection(coid+'_exitacts');

	exitacts.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			exitacts.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'exitacts not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading exitacts'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

