if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 20;

exports.getFPholidaylist = function(coid, userid, callback)   {
	var holidaylist  = dbConn.collection(coid+'_holidaylist');

	holidaylist.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			holidaylist.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'holidaylist not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading holidaylist'});
	});
};
exports.getNPholidaylist = function(coid, userid, pageno, callback)   {
	var holidaylist  = dbConn.collection(coid+'_holidaylist');

	holidaylist.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'holidaylist not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallholidaylist = function(coid, userid, callback)   {
	var holidaylist  = dbConn.collection(coid+'_holidaylist');

	holidaylist.find({}).sort({_id:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'holidaylist not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getcount = function(coid, userid, callback)   {
	var holidaylist  = dbConn.collection(coid+'_holidaylist');

	holidaylist.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Holiday list not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.addnewholidaylist = function(newData, callback)   {
	var holidaylist  = dbConn.collection(newData.coid+'_holidaylist');

	newData.date = new Date();
	newData.holidaytype = newData.holidaytype;
	newData.holidaydate = newData.holidaydate;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	holidaylist.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'holidaylist not added'});
		else   {
			getFPholidaylist(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table holidaylist data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateholidaylist = function(newData, callback)   {
	var holidaylist  = dbConn.collection(newData.coid+'_holidaylist');

	holidaylist.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.holidayname = newData.holidayname;
			o.holidaytype = newData.holidaytype;
			o.holidaydate = newData.holidaydate;
			delete o.coid;
			holidaylist.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'holidaylist not updated'});
				else   {
					getFPholidaylist(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table holidaylist data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in holidaylist'});
		}
	});
}
exports.deleteholidaylist = function(newData, callback)   {
	var holidaylist  = dbConn.collection(newData.coid+'_holidaylist');

	holidaylist.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'holidaylist data not deleted'});
		else   {
			getFPholidaylist(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table holidaylist data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPholidaylist = function(coid, userid, callback)   {
	var holidaylist  = dbConn.collection(coid+'_holidaylist');

	holidaylist.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			holidaylist.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'holidaylist not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading holidaylist'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

