var crypto 		= require('crypto');
var EM        = require('./email-dispatcher');
var async 		= require('async');

if (process.env.OPENSHIFT_MONGODB_DB_URL)
  var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
  var NUMBER_OF_ITEMS_PER_PAGE = 3;

var PAGE_NUMBER = 0;

exports.manualLogin = function(usrid, pass, callback)  {
  var uid = parseInt(usrid);
  
  var users = dbConn.collection('phr_admins');
	users.findOne({userid:uid}, function(e, o) {
		if (o == null)	{
			callback(true,{err:1,text:'Admin not found'});
		}	else{
			validatePassword(pass, o.password, function(err, res) {
				if (!err)	{
	  			callback(false,o);
				}	else	{
	  			callback(true,{err:2,text:'Incorrect Password'});
				}
			});
		}
	});
}

exports.updatePassword = function(userid,oldpass, newpass, callback)    {
  var users = dbConn.collection("phr_admins");
  users.findOne({userid:userid}, function(e, o)    {
    if (!o)
      callback(true,{err:1,text:'Unknown User'});
	  else	{
      //console.log(userid);
      //console.log(oldpass);
      //console.log(o);
		  validatePassword(oldpass, o.password, function(err, res) {
            if (err)
       		   callback(true,{err:2,text:'Incorrect Current Password'});
		     else    {
			     saltAndHash(newpass, function(hash)		{
			       o.password = hash;
				     users.save(o, {safe: true}, function (e1,o1)   {
                     if (e1)
	                    callback(true,{err:3,text:'Password not updated'});
                     else
	                    callback(false,{err:0,text:'Password updated'});
                  });
			     });
		     }
		  });
	  }
  });
}

function validatePassword(plainPass, hashedPass, callback)    {
  //console.log(plainPass);
  //console.log(hashedPass);
  //console.log("------------");
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
  //console.log(validHash);
  //console.log("------------");
	if (hashedPass === validHash)   {
    //console.log("same");
    callback(false, 1);
  } else  {
    //console.log("Not  same");
    callback(true, 1);
	  //callback(null, hashedPass === validHash);
  }
}

exports.resetpwd = function(semail, callback)     {
	users.findOne({user:semail}, function(e, o) {
		if (o)      {
      //console.log("known user");
	    var spwd = Math.random().toString(36).slice(-8);
			saltAndHash(spwd, function(hash)    {
		    o.password = hash;
		    users.save(o, {safe: true}, function(e1, o1) {
	        if (e1)	{
            console.log("Unable to reset password");
      			callback(true,{err:1,text:'Unable to reset password'});
          } else  {
            //console.log("Password reset in db");
            EM.resetpwd(o,spwd, function (err, op)   {
              if (op)     {
                //console.log("new password emailed");
          			callback(false,{err:0,text:'Please check your email for new password'});
              } else  {
                //console.log("email not sent");
          			callback(true,{err:2,text:'Unable to reset password'});
              }
            });
          }
        });
      });
		} else    {
      //console.log("Unknown user");
		  callback(true,{err:1,text:'Unknown user'});
    }
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

/* private encryption & validation methods */
var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

// ------------------------------------------------------------
// Generated for Site Admins
// ------------------------------------------------------------
exports.getFPstadmins = function(userid, callback)   {
	var users  = dbConn.collection("phr_admins");

	users.find({usertype:'1'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'1'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'Admins not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading Admins'});
	});
};
exports.getNPstadmins = function(coid, userid, pageno, callback)   {
	var users  = dbConn.collection(phr_admins);

	users.find({usertype:'1'}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Admins not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallstadmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(phr_admins);

	users.find({usertype:'1'}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Admins not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewstadmins = function(newData, callback)   {
  var compuserIDs = dbConn.collection('phr_adminids');
	var users  = dbConn.collection("phr_admins");

  compuserIDs.findOne({}, function(e, usr) {
    if (usr)      {
			newData.date = new Date();
			newData.useremailid = newData.useremailid;
	    newData.userid = usr.userid;
			saltAndHash(newData.password, function(hash)    {
				newData.password = hash;
			});
	    newData.usertype = '1';
			var suserid = newData.userid;
			users.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Admin not added'});
				else   {
          compuserIDs.remove({userid:usr.userid},1, function(e3, res) {
						getFPstadmins(suserid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Admin user data NOT added'});
						else
							callback(false,o2, totpages, usr.userid);
						});
					});
				}
			});
    } else
      callback(true,{err:3,text:'unable to get Admin ids'});
	});
}
exports.updatestadmins = function(newData, callback)   {
	var users  = dbConn.collection("phr_admins");

	users.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.username = newData.username;
			o.useremailid = newData.useremailid;
			if (newData.password != '')	{
				saltAndHash(newData.password, function(hash)    {
					o.password = hash;
				});
			}
			o.usertype = '1';
			delete o.coid;
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Admin user not updated'});
				else   {
					getFPstadmins(newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Admin user NOT updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in Admin'});
		}
	});
}
exports.deletestadmins = function(newData, callback)   {
	var users  = dbConn.collection("phr_admins");

	users.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'Admin data not deleted'});
		else   {
			getFPstadmins(newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Admin data NOT deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPstadmins = function(userid, callback)   {
	var users  = dbConn.collection("phr_admins");

	users.find({usertype:'1'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'1'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'Admins not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading Admins'});
	});
}

// ------------------------------------------------------------


