var crypto 		= require('crypto')
var EM        = require('./email-dispatcher');
var async 		= require('async');

if (process.env.OPENSHIFT_MONGODB_DB_URL)
  var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
  var NUMBER_OF_ITEMS_PER_PAGE = 30;

var PAGE_NUMBER = 0;

//var userIDs = dbConn.collection('eqp_userids');

exports.newUser = function(newData, callback)   {
  var user = dbConn.collection('users');
  var preuser = dbConn.collection('preuser');

	user.findOne({user:newData.user}, function(e, o) {
		if (o)      {
      //console.log("existing email");
			callback(true,{err:1,text:'Email already registered'});
		}	else	{
	    preuser.findOne({user:newData.user}, function(e1, o1) {
		    if (o1) {
          //console.log("existing user awaiting verification");
			    callback(true,{err:2,text:"This email is pending verification. Please check your inbox"});
        } else  {
					var oldpass = newData.password;
    			saltAndHash(oldpass, function(hash)    {
				    newData.pass = hash;
				    newData.pwd = oldpass;
			      // append date stamp when record was created //
			      newData.date = new Date();;
            newData.verid = Math.random().toString(36).slice(-16);
		        preuser.insert(newData, {safe: true}, function (e2,o2)   {
              if (o2)     {
                EM.newCandRequest(newData, function (err, op)   {
                  if (op)     {
                    //console.log("success");
              			callback(false,{err:0,text:'Please check your email for further information'});
                  } else  {
                    //console.log("email not sent");
              			callback(false,{err:3,text:'Unable to send verification email'});
                  }
                });
              } else  {
                //console.log("fail");
                //console.log(e2);
          			callback(false,{err:4,text:'Join request unsuccessful'});
              }
            });
          });
			  }
      });
		}
	});
}

exports.manualLogin = function(uid, pass, callback)  {
  var users = dbConn.collection('users');
  var company = dbConn.collection('company');
	var resp = new Object;
	async.waterfall([
		function(callback) {
	    users.findOne({user:uid}, function(e1, o1) {
		    if (o1)	{
			    validatePassword(pass, o1.pass, function(e2, o2) {
			      if (e2) {
      				callback(e2, {err:true, text:"Invalid Password"});
				    }	else {
      				callback(e2, o1);
				    }
			    });
		    }	else{
  				callback(e1, {err:true, text:"Unknown user"});
		    }
	    });
		},
		function(user, callback1) {
	    resp = user;
      if (!user.err)   {
		    if (user.company)     {
	        company.findOne({user:user.user}, function(e3, o3)   {
			      if (!e3) 
			        resp.company = o3;
			      else
			        resp.company = null;
				    callback1(false,resp);
			    });
		    } else  {
			    callback1(true,resp);
		    }
	    } else  {
  	    callback1(true,resp);
	    }
	  },
		], function(err, results) {
		  if (results.err)  {
		    callback(1, results);
		  } else  {
		    callback(0, resp);
	    }
		}
	);
}

exports.golivepwd = function(coid, userid, newpass, callback)    {
  var users = dbConn.collection(coid+'_users');
  users.findOne({userid:parseInt(userid)}, function(e, o)    {
    if (!o)
      callback(true,{err:1,text:'Unknown User'});
	  else	{
			saltAndHash(newpass, function(hash)		{
     		o.password = hash;
     		o.pwdflag = false;
	     	users.save(o, {safe: true}, function (e1,o1)   {
        	if (e1)
          	callback(true,{err:3,text:'Password not updated'});
         	else
          	callback(false,{err:0,text:'Password updated'});
      	});
     	});
	  }
  });
}

exports.updatePassword = function(userid,oldpass, newpass, callback)    {
  var users = dbConn.collection('users');
  users.findOne({user:userid}, function(e, o)    {
    if (!o)
      callback(true,{err:1,text:'Unknown User'});
	  else	{
      //console.log(userid);
      //console.log(oldpass);
      //console.log(o);
		  validatePassword(oldpass, o.pass, function(err, res) {
          if (err)
            callback(true,{err:2,text:'Incorrect Current Password'});
  		    else    {
  			    saltAndHash(newpass, function(hash)		{
			        o.pass = hash;
				      users.save(o, {safe: true}, function (e1,o1)   {
                if (e1)
                  callback(true,{err:3,text:'Password not updated'});
                else
                  callback(false,{err:0,text:'Password updated'});
                });
			      });
	        }
		  });
	  }
  });
}

exports.updPwd = function(coid, userid, newpass, callback)    {
  var users = dbConn.collection(coid+'_users');
  users.findOne({userid:parseInt(userid)}, function(e, o)    {
    if (!o)
      callback(true,{err:1,text:'Unknown User'});
    else	{
      saltAndHash(newpass, function(hash)		{
      o.password = hash;
      users.save(o, {safe: true}, function (e1,o1)   {
        if (e1)
          callback(true,{err:3,text:'Password not updated'});
        else
          callback(false,{err:0,text:'Password updated'});
        });
      });
    }
  });
}

exports.resetpwd = function(semail, callback)     {
	users.findOne({user:semail}, function(e, o) {
		if (o)      {
      //console.log("known user");
	    var spwd = Math.random().toString(36).slice(-8);
			saltAndHash(spwd, function(hash)    {
		    o.password = hash;
		    users.save(o, {safe: true}, function(e1, o1) {
	        if (e1)	{
            console.log("Unable to reset password");
      			callback(true,{err:1,text:'Unable to reset password'});
          } else  {
            //console.log("Password reset in db");
            EM.resetpwd(o,spwd, function (err, op)   {
              if (op)     {
                //console.log("new password emailed");
          			callback(false,{err:0,text:'Please check your email for new password'});
              } else  {
                //console.log("email not sent");
          			callback(true,{err:2,text:'Unable to reset password'});
              }
            });
          }
        });
      });
		} else    {
      //console.log("Unknown user");
		  callback(true,{err:1,text:'Unknown user'});
    }
	});
}

exports.getusercount = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	//var cdate = new Date();
	//console.log(cdate);

	users.find({status: {$ne:'3'}}).count(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else		{
			callback(false,o);
		}
	});
};

exports.getallusers = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "4"},{"usertype": "5"}]};
  query["$and"].push(qry1);

	users.find(query).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getusersByDept = function(searchData, callback)   {
	var users  = dbConn.collection(searchData.coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];
	if (searchData.depidc != '')
    query["$and"].push({'dept':searchData.depid});

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "4"},{"usertype": "5"}]};
  query["$and"].push(qry1);

	users.find(query).sort({usertype:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getusersByLoc = function(searchData, callback)   {
	var users  = dbConn.collection(searchData.coid+'_users');

	//console.log(searchData);
  var query = {};
  var qry1 = {};

  query["$and"]=[];
  query["$and"].push({"usertype": "4"});
	if (searchData.loc != '' && searchData.loc != '0' )
    query["$and"].push({'location':searchData.loc});
	if (searchData.etype != '' && searchData.etype != '0' )
    query["$and"].push({'emptype':searchData.etype});
	users.find(query).sort({dept:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getusersByMgr = function(coid, mgrid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];
	if (mgrid != '')
    query["$and"].push({'mgr':mgrid.toString()});

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "4"}]};
  query["$and"].push(qry1);

	users.find(query).sort({usertype:1}).toArray(function(e, o)   {
		if (e)	{
			callback(true,{err:1,text:'users not found'});
		}   else    {
			callback(false,o);
		}
	});
}

exports.getrolesbymgr = function(coid, mgrid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];
	if (mgrid != '')
    query["$and"].push({'mgr':mgrid.toString()});

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "4"},{"usertype": "5"}]};
  query["$and"].push(qry1);

	users.find(query,{role:1,_id:0}).toArray(function(e, o)   {
		if (e)	{
			callback(true,{err:1,text:'Roles not found'});
		}   else    {
			callback(false,o);
		}
	});
}

exports.getusersById = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	users.find({userid:parseInt(userid)}).toArray(function(e, o)   {
		if (e)	{
			callback(true,{err:1,text:'users not found'});
		}   else    {
			callback(false,o);
		}
	});
}

// ------------------------------------------------------------
// Generated for About you
// ------------------------------------------------------------
exports.getoneusers = function(userid, callback)   {
	var users  = dbConn.collection('users');
  console.log(userid);
	users.findOne({userid:userid }, function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.updatepprofile = function(uid,fname,mobile, desgn, callback)   {
	var users  = dbConn.collection('users');
	users.findOne({user:uid}, function(e, o)   {
		if (o)    {
			o.name = fname;
			o.mobile = mobile;
			o.desgn = desgn;

			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not updated'});
				else
					callback(false,{err:0,text:o});
			});
		} else
  		callback(true,{err:2,text:'Unknown User'});
	});
}

exports.updatecandiate = function(newData, callback)   {
	var users  = dbConn.collection('users');
	users.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.uname = newData.uname;
			o.emailid = newData.emailid;
			o.mobno = newData.mobno;
			o.gender = newData.gender;
			o.cal1 = newData.cal1;
			o.cal2 = newData.cal2;
			o.ccity = newData.ccity;
			o.cpin = newData.cpin;
			o.linkedin = newData.linkedin;
      o.fb = newData.fb;
      o.twit = newData.twit;

      var tmp = newData.dob.split("-");
      o.dob = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));

			//o.dept = newData.dept;
			o.industry = newData.industry;
			o.skills = newData.skills;
			o.about = newData.about;
      o.sigach = newData.sigach;
      o.indresp = newData.indresp;
      o.adays = newData.adays;
      
      /*
      o.astime1 = newData.astime1;
      o.aetime1 = newData.aetime1;
      o.astime2 = newData.astime2;
      o.aetime2 = newData.aetime2;
      o.astime3 = newData.astime3;
      o.aetime3 = newData.aetime3;
      o.astime4 = newData.astime4;
      o.aetime4 = newData.aetime4;
      o.astime5 = newData.astime5;
      o.aetime5 = newData.aetime5;
      o.astime6 = newData.astime6;
      o.aetime6 = newData.aetime6;
      o.astime7 = newData.astime7;
      o.aetime7 = newData.aetime7;
      */

			o.e1org = newData.e1org;
			//o.e1dept = newData.e1dept;
			o.e1role = newData.e1role;

      if(newData.e1sdate != '') {
        var tmp = newData.e1sdate.split("-");
        o.e1sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }
      else
        o.e1sdate = newData.e1sdate;

      o.e1days = newData.e1days;
			//o.e1stime = newData.e1stime;
			//o.e1etime = newData.e1etime;
			o.e2org = newData.e2org;
			//o.e2dept = newData.e2dept;
			o.e2role = newData.e2role;

      if(newData.e2sdate != '') {
        var tmp = newData.e2sdate.split("-");
        o.e2sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }
      else
        o.e2sdate = newData.e2sdate;

			o.e2days = newData.e2days;
			//o.e2stime = newData.e2stime;
			//o.e2etime = newData.e2etime;
			o.p1org = newData.p1org;
			//o.p1dept = newData.p1dept;
			o.p1role = newData.p1role;

      if(newData.p1sdate != '') {
        var tmp = newData.p1sdate.split("-");
        o.p1sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }  
      else
        o.p1sdate = newData.p1sdate;

      if(newData.p1edate != '') {
        var tmp = newData.p1edate.split("-");
        o.p1edate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }  
      else
        o.p1edate = newData.p1edate;

			o.p2org = newData.p2org;
			//o.p2dept = newData.p2dept;
			o.p2role = newData.p2role;

      if(newData.p2sdate != '') {
        var tmp = newData.p2sdate.split("-");
        o.p2sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }
      else
        o.p2sdate = newData.p2sdate;

      if(newData.p2edate != '') {  
        var tmp = newData.p2edate.split("-");
        o.p2edate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      }
      else
        o.p2edate = newData.p2edate;

			o.ed1name = newData.ed1name;
			o.ed1colg = newData.ed1colg;

      var tmp = newData.ed1sdate.split("-");
      o.ed1sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      var tmp = newData.ed1edate.split("-");
      o.ed1edate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));

			o.ed2name = newData.ed2name;
			o.ed2colg = newData.ed2colg;

      var tmp = newData.ed2sdate.split("-");
      o.ed2sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      var tmp = newData.ed2edate.split("-");
      o.ed2edate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));

      o.ed3name = newData.ed3name;
      o.ed3colg = newData.ed3colg;

      var tmp = newData.ed3sdate.split("-");
      o.ed3sdate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));
      var tmp = newData.ed3edate.split("-");
      o.ed3edate = new Date(Date.UTC(tmp[2], tmp[1] - 1, tmp[0]));

			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not updated'});
				else    {
				  //console.log(o);
					callback(false,{err:0,text:o});
				}
			});
		} else  {
			callback(true,{err:2,text:' Invalid user'});
		}
	});
}

// ------------------------------------------------------------

// --------------------------------------------------------------
// 221 - Search for all users 
// --------------------------------------------------------------
exports.searchusers = function(searchData, callback)	{
  var query = {};
  var qry1 = {};
	var dlen = 0;

	//console.log(searchData);

  query["$and"]=[];
	if (searchData.loc != '')
    query["$and"].push({'location':searchData.loc});
	if (searchData.dep != '' )
    query["$and"].push({'dept':searchData.dep});
	if (searchData.mgr != '')
    query["$and"].push({'mgr':searchData.mgr});
	if (searchData.uname != '')
    query["$and"].push({'username':searchData.uname});
	if (searchData.uid != '')
    query["$and"].push({'userid':parseInt(searchData.uid)});

  qry1["$or"]=[];
  qry1 = { $or: [ {"usertype": "3"},{"usertype": "4"},{"usertype": "5"}]};
  query["$and"].push(qry1);

	//console.log(JSON.stringify(query));

	var users  = dbConn.collection(searchData.coid+'_users');
  users.find(query).count(function(e1,o)    {
    if (e1)		{
      console.log("Error in reading users data"+e1);
		  callback(true,{err:1,text:'Error in reading users data'});
    }		else		{
      if (o == 0)		{
	      //console.log("total records = " + o);
  		  callback(true,{err:2,text:'No users found for the criteria'});
      }  else    {
        var tot = o / NUMBER_OF_ITEMS_PER_PAGE;
        var tot = Math.ceil(tot);
      	users.find(query).limit(NUMBER_OF_ITEMS_PER_PAGE).sort( { _id:-1 } ).toArray(function(e2, res) {
		      if (e2) {
      		  callback(true,{err:3,text:'Error in reading students data'});
          } else  {
            callback(false,res,tot,searchData);
          }
      	});
      }
    }
	});
};

exports.searchNP = function(coid, userid, newpage, searchData, callback)	{
  var query = {};
  var qry1 = {};
  var qry2 = {};
  var qry3 = {};
  var qry4 = {};
	var dlen = 0;

	//console.log(searchData);

  query["$and"]=[];
  if (searchData.univ)
    query["$and"].push({"edudata.univ":searchData.univ});
  if (searchData.udegree)
    query["$and"].push({"edudata.udegree":searchData.udegree});
  if (searchData.college)
    query["$and"].push({"edudata.college":searchData.college});
  if (searchData.orgn)
    query["$and"].push({"expdata.orgn":searchData.college});


  qry1["$or"]=[];
  if (searchData.addr) {
	  qry1 = { $or: [ {"paddr1": searchData.addr},{"paddr2": searchData.addr},{"caddr1": searchData.addr},{"caddr2": searchData.addr} ]};
    query["$and"].push(qry1);
  }

  qry2["$or"]=[];
  if (searchData.bgroup) {
  	dlen = searchData.bgroup.length;
		for(var i=0; i < dlen; i++)	{
		  qry2["$or"].push({"bgroup": searchData.bgroup[i]});
	  }
		query["$and"].push(qry2);
  }

  qry3["$or"]=[];
  if (searchData.pint) {
  	dlen = searchData.pint.length;
		for(var i=0;i < dlen; i++)		{
		  qry3["$or"].push({"pint": searchData.pint[i].toString()});
	  }
		query["$and"].push(qry3);
  }

  qry4["$or"]=[];
  if (searchData.skill) {
		var zlen = searchData.skill.length;
		for( var i=0; i < zlen; i++)	{
			qry4 = { $or: [ {"noexp": searchData.skill[i]}, {"prof": searchData.skill[i]}, {"exprt": searchData.skill[i]} ]};
		}
		query["$and"].push(qry4);
  }
	//console.log(query);

	var users  = dbConn.collection(searchData.coid+'_users');
	users.find(query).sort( { _id:-1 }).skip((newpage-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, res) {
    if (e)
		  callback(true,{err:1,text:'Error in reading users data'});
    else  {
      //console.log(res);
      callback(false,res);
    }
	});
};

// --------------------------------------------------------------
// 226 - Search for employees
// --------------------------------------------------------------
exports.searchemp = function(searchData, callback)	{
  var query = {};
  var qry1 = {};
  var qry2 = {};
  var qry3 = {};
  var qry4 = {};
	var dlen = 0;

	//console.log(searchData);

  query["$and"]=[];
  if (searchData.univ)
    query["$and"].push({"edudata.univ":searchData.univ});
  if (searchData.udegree)
    query["$and"].push({"edudata.udegree":searchData.udegree});
  if (searchData.college)
    query["$and"].push({"edudata.college":searchData.college});
  if (searchData.orgn)
    query["$and"].push({"expdata.orgn":searchData.college});
	if (searchData.loc)
    query["$and"].push({"location":searchData.loc});
	if (searchData.dep)
    query["$and"].push({"dept":searchData.dep});
	if (searchData.mgr)
    query["$and"].push({"mgr":searchData.mgr});
	if (searchData.role)
    query["$and"].push({"role":searchData.role});


  qry1["$or"]=[];
  if (searchData.addr) {
	  qry1 = { $or: [ {"paddr1": searchData.addr},{"paddr2": searchData.addr},{"pcity": searchData.addr},{"caddr1": searchData.addr},{"caddr2": searchData.addr},{"ccity": searchData.addr} ]};
    query["$and"].push(qry1);
  }

  qry2["$or"]=[];
  if (searchData.bgroup) {
  	dlen = searchData.bgroup.length;
		for(var i=0; i < dlen; i++)	{
		  qry2["$or"].push({"bgroup": searchData.bgroup[i]});
	  }
		query["$and"].push(qry2);
  }

  qry3["$or"]=[];
  if (searchData.pint) {
  	dlen = searchData.pint.length;
		for(var i=0;i < dlen; i++)		{
		  qry3["$or"].push({"pint": searchData.pint[i].toString()});
	  }
		query["$and"].push(qry3);
  }

  qry4["$or"]=[];
  if (searchData.skill) {
		var zlen = searchData.skill.length;
		for( var i=0; i < zlen; i++)	{
			qry4 = { $or: [ {"noexp": searchData.skill[i]}, {"prof": searchData.skill[i]}, {"exprt": searchData.skill[i]} ]};
		}
		query["$and"].push(qry4);
  }

	//console.log(query);

	var users  = dbConn.collection(searchData.coid+'_users');
  users.find(query).count(function(e1,o)    {
    if (e1)		{
      console.log("Error in reading users data"+e1);
		  callback(true,{err:1,text:'Error in reading users data'});
    }		else		{
      if (o == 0)		{
	      //console.log("total records = " + o);
  		  callback(true,{err:2,text:'No users found for the criteria'});
      }  else    {
	      //console.log("total records = " + o);
        var tot = o / NUMBER_OF_ITEMS_PER_PAGE;
        var tot = Math.ceil(tot);
      	users.find(query).limit(NUMBER_OF_ITEMS_PER_PAGE).sort( { _id:-1 } ).toArray(function(e2, res) {
		      if (e2) {
      		  //console.log('Error in reading students data');
      		  callback(true,{err:3,text:'Error in reading students data'});
          } else  {
      		  //console.log(res);
            callback(false,res,tot,searchData);
          }
      	});
      }
    }
	});
};

exports.searchNP = function(coid, userid, newpage, sData, callback)	{
  var query = {};
  var qry1 = {};
  var qry2 = {};
  var qry3 = {};
  var qry4 = {};
	var dlen = 0;

	//console.log(sData);
	var searchData = JSON.parse(sData);

  query["$and"]=[];
  if (searchData.univ)
    query["$and"].push({"edudata.univ":searchData.univ});
  if (searchData.udegree)
    query["$and"].push({"edudata.udegree":searchData.udegree});
  if (searchData.college)
    query["$and"].push({"edudata.college":searchData.college});
  if (searchData.orgn)
    query["$and"].push({"expdata.orgn":searchData.college});
	if (searchData.loc)
    query["$and"].push({"location":searchData.loc});
	if (searchData.dep)
    query["$and"].push({"dept":searchData.dep});
	if (searchData.mgr)
    query["$and"].push({"mgr":searchData.mgr});
	if (searchData.role)
    query["$and"].push({"role":searchData.role});


  qry1["$or"]=[];
  if (searchData.addr) {
	  qry1 = { $or: [ {"paddr1": searchData.addr},{"paddr2": searchData.addr},{"caddr1": searchData.addr},{"caddr2": searchData.addr} ]};
    query["$and"].push(qry1);
  }

  qry2["$or"]=[];
  if (searchData.bgroup) {
  	dlen = searchData.bgroup.length;
		for(var i=0; i < dlen; i++)	{
		  qry2["$or"].push({"bgroup": searchData.bgroup[i]});
	  }
		query["$and"].push(qry2);
  }

  qry3["$or"]=[];
  if (searchData.pint) {
  	dlen = searchData.pint.length;
		for(var i=0;i < dlen; i++)		{
		  qry3["$or"].push({"pint": searchData.pint[i].toString()});
	  }
		query["$and"].push(qry3);
  }

  qry4["$or"]=[];
  if (searchData.skill) {
		var zlen = searchData.skill.length;
		for( var i=0; i < zlen; i++)	{
			qry4 = { $or: [ {"noexp": searchData.skill[i]}, {"prof": searchData.skill[i]}, {"exprt": searchData.skill[i]} ]};
		}
		query["$and"].push(qry4);
  }
	//console.log(query);

	var users  = dbConn.collection(searchData.coid+'_users');
	users.find(query).sort( { _id:-1 }).skip((newpage-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, res) {
    if (e)
		  callback(true,{err:1,text:'Error in reading users data'});
    else  {
      //console.log(res);
      callback(false,res);
    }
	});
};

// ------------------------------------------------------------
// Generated for Employees
// ------------------------------------------------------------
exports.getFPemployees = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({usertype:'4'});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	users.find(query).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find(query).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
};
exports.getNPemployees = function(coid, userid, pageno, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({usertype:'4'});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	users.find(query).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallemployees = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({usertype:'4'});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	users.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getemployeescount = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({usertype:'4'});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	users.find(query).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Users not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.addnewemployees = function(newData, callback)   {
  var compuserIDs = dbConn.collection(newData.coid+'_userids');
	var users  = dbConn.collection(newData.coid+'_users');

  compuserIDs.findOne({}, function(e, usr) {
    if (usr)      {
			newData.date = new Date();
			newData.useremailid = newData.useremailid;
	    newData.userid = usr.userid;
	    var textpass = newData.password;
			saltAndHash(newData.password, function(hash)    {
				newData.password = hash;
			});
      var tmp = newData.dob.split("-");
      newData.dob = new Date(tmp[0], tmp[1] - 1, tmp[2]);
			newData.gender = newData.gender;
			newData.bgroup = newData.bgrp;
			newData.mgr = newData.mgr;
			newData.dept = newData.dept;
			newData.role = newData.role;
			newData.rtype = newData.rtype;
			newData.location = newData.location;
			newData.desg = newData.desg;
      var tmp = newData.doj.split("-");
      newData.doj = new Date(tmp[0], tmp[1] - 1, tmp[2]);
      var tmp = newData.probdate.split("-");
      newData.probdate = new Date(tmp[0], tmp[1] - 1, tmp[2]);
			newData.status = newData.status;
			newData.salary = newData.salary;
			newData.emptype = newData.emptype;
			newData.noasc = newData.noasc;
			newData.wkns = newData.wkns;
			newData.stng = newData.stng;
	    newData.usertype = '4';

			var scoid = newData.coid;
			delete newData.coid;
			var suserid = newData.userid;

			users.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not added'});
				else   {
					EM.NewEmployee({coid : scoid, userid : usr.userid, username : newData.username, useremailid : newData.useremailid, password : textpass,colive:newData.live}, function(e2,o2)	{
			      compuserIDs.remove({userid:usr.userid},1, function(e3, res) {
							getFPemployees(scoid,suserid, function(e4,o4, totpages)     {
								if (e4)
									callback(true,{err:2,text:'Table users data added'});
								else	{
									//console.log(usr.userid);
									callback(false,o4, totpages, usr.userid);
								}
							});
						});
					});
				}
			});
    } else
      callback(true,{err:3,text:'unable to get user ids'});
	});
}
exports.updemporgstru = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.findOne({userid: parseInt(newData.userid)}, function(e, o)   {
		if (o)   {
			o.mgr = newData.mgr;
			o.dept = newData.dept;
			o.role = newData.role;
			o.location = newData.location;
			o.emptype = newData.emptype;
			o.rtype = newData.rtype;
			o.salary = newData.salary;

			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)			{
					callback(true,{err:1,text:'users not updated'});
				} else	{
					callback(false,{err:0,text:'Table users data updated'});
				}
			});
		} else  {
			callback(true,{err:2,text: 'ID not found in users'});
		}
	});
}

exports.updpwdflag = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.update({usertype: '4'}, {$set: {"pwdflag": true}},{ multi: true }, function(e, o)   {
		if (e)   {
			callback(true,{err:1,text:'password flag not updated'});
		} else	{
			callback(false,{err:0,text:'user password flag updated'});
		}
	});
}

exports.resetpwdflag = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.findOne({userid: parseInt(userid)}, function(e, o)   {
		if (o)   {
			o.pwdflag = false;
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)			{
					callback(true,{err:1,text:'password flag not reset'});
				} else	{
					callback(false,{err:0,text:'password flag reset'});
				}
			});
		} else  {
			callback(true,{err:2,text: 'ID not found in users'});
		}
	});
}

exports.deleteemployees = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'users data not deleted'});
		else   {
			getFPemployees(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table users data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPemployees = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({usertype:'4'});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	users.find(query).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find(query).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}
// ---------------------------------------------------------------

// ------------------------------------------------------------
// Generated for HR Admins
// ------------------------------------------------------------
exports.getFPhradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'2', owner:false}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'2', owner:false}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
};
exports.getNPhradmins = function(coid, userid, pageno, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'2', owner:false}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallhradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'2', owner:false}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}
exports.addnewhradmins = function(newData, callback)   {
  var compuserIDs = dbConn.collection(newData.coid+'_userids');
	var users  = dbConn.collection(newData.coid+'_users');

  compuserIDs.findOne({}, function(e, usr) {
    if (usr)      {
			newData.date = new Date();
			newData.useremailid = newData.useremailid;
	    newData.userid = usr.userid;
			saltAndHash(newData.password, function(hash)    {
				newData.password = hash;
			});
	    newData.owner = false;
	    newData.usertype = '2';
			var scoid = newData.coid;
			delete newData.coid;
			var suserid = newData.userid;
			users.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not added'});
				else   {
          compuserIDs.remove({userid:usr.userid},1, function(e3, res) {
						getallhradmins(scoid,suserid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table users data added'});
						else
							callback(false,o2, totpages, usr.userid);
						});
					});
				}
			});
    } else
      callback(true,{err:3,text:'unable to get user ids'});
	});
}
exports.updatehradmins = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.username = newData.username;
			o.useremailid = newData.useremailid;
			if (newData.password != '')	{
				saltAndHash(newData.password, function(hash)    {
					o.password = hash;
				});
			}
	    o.owner = false;
			o.usertype = '2';
			delete o.coid;
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not updated'});
				else   {
					getallhradmins(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table users data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in users'});
		}
	});
}
exports.deletehradmins = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'users data not deleted'});
		else   {
			getallhradmins(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table users data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}

var getallhradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'2', owner:false}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

var getFPhradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'2', owner:false}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'2', owner:false}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
}

// ------------------------------------------------------------

// ------------------------------------------------------------
// Generated for Payroll Admins
// ------------------------------------------------------------
exports.getFPpradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'3'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'3'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
};
exports.getNPpradmins = function(coid, userid, pageno, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'3'}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallpradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'3'}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewpradmins = function(newData, callback)   {
  var compuserIDs = dbConn.collection(newData.coid+'_userids');
	var users  = dbConn.collection(newData.coid+'_users');

  compuserIDs.findOne({}, function(e, usr) {
    if (usr)      {
			newData.date = new Date();
			newData.useremailid = newData.useremailid;
	    newData.userid = usr.userid;
			saltAndHash(newData.password, function(hash)    {
				newData.password = hash;
			});
	    newData.usertype = '3';
			var scoid = newData.coid;
			delete newData.coid;
			var suserid = newData.userid;
			users.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not added'});
				else   {
          compuserIDs.remove({userid:usr.userid},1, function(e3, res) {
						getallpradmins(scoid,suserid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table users data added'});
						else
							callback(false,o2, totpages, usr.userid);
						});
					});
				}
			});
    } else
      callback(true,{err:3,text:'unable to get user ids'});
	});
}
exports.updatepradmins = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.username = newData.username;
			o.useremailid = newData.useremailid;
			if (newData.password != '')	{
				saltAndHash(newData.password, function(hash)    {
					o.password = hash;
				});
			}
			o.usertype = '3';
			delete o.coid;
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'users not updated'});
				else   {
					getallpradmins(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table users data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in users'});
		}
	});
}
exports.deletepradmins = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'users data not deleted'});
		else   {
			getallpradmins(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table users data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}

var getallpradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'3'}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

var getFPpradmins = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({usertype:'3'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({usertype:'3'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
}

// ------------------------------------------------------------

// ------------------------------------------------------------
// For managing Probation
// ------------------------------------------------------------
exports.getFPprobusers = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({status:'1'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({status:'1'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
};
exports.getNPprobusers = function(coid, userid, pageno, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({status:'1'}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.updateprob = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.probdate = newData.probdate;
			o.status = newData.status;

			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Probation status not updated'});
				else   {
					getFPprobusers(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Probation status updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in users'});
		}
	});
}

function getFPprobusers(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({status:'1'}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			users.find({status:'1'}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'users not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading users'});
	});
};

exports.getteammembers = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.find({mgr:userid.toString()}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'team members not found'});
		else
			callback(false,o);
	});
}

exports.getskipmembers = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	var dreports;
	var dusers = [];

	async.waterfall([
		function(callback) {
			users.find({mgr:userid.toString()}).toArray(function(e, o)   {
				if (dreports)
					dreports = o;
				else
					dreports = null;
				callback(e,o);
			});
		},
		], function(err, results) {
			var drlen = dreports.length;
			for (ctr=0; ctr<drlen;ctr++)
				dusers.push(dreports[ctr].userid);

			var sreports = getskipteam(coid, dusers);
			res.json({err:false, data:sreports});
		}
	);
}

function getskipteam(coid, dusrs)			{
	var users  = dbConn.collection(coid+'_users');

	var ulen = dusrs.length;
	if (ulen < 1)			{
		return null;
	} else	{
		async.forEach(dusrs, function(user, callback) {
			users.find( {mgr:user.toString()} , function(e1, skips)   {
				if (skips)	{
					//console.log(skips);
					sreports.push(skips);
				}
				callback(e1,skips);
			});
		}, function(err, results) {
			callback(false,sreports);
		});
		return sreports;
	}
}

exports.finalise = function(coid, userid, callback)   {
	var users  = dbConn.collection(coid+'_users');

	users.findOne({userid: parseInt(userid)}, function(e, o)   {
		if (o)   {
			o.status = '3';
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Exit NOT Finalised'});
				else
					callback(false,{err:0,text:'Exit Finalised'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in users'});
		}
	});
}

// --------------------------------------------------------------------------
// Save results of Company Induction
// --------------------------------------------------------------------------
exports.saveindc = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.find({userid:newData.userid}, {indcans:1}).toArray(function(e, o)   {
		if (o)			{
			//o.indcans = newData.indcans;
			//o.indcfb = newData.indcfb;
		  //o.indcdate = new Date();
			users.update({userid:newData.userid}, { $set : {"indcans":newData.indcans, indcfb: newData.indcfb, indcdate: new Date()}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Induction Answers not updated'});
				else
					callback(true,{err:2,text:'Induction Answers updated'});
			});
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}

// --------------------------------------------------------------------------
// Save results of Department Induction
// --------------------------------------------------------------------------
exports.savedeptindc = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.find({userid:newData.userid}).toArray(function(e, o)   {
		if (o)			{
			var updObj = { $set : {} };
			updObj.$set[newData.indc] = newData.indcans;
			users.update({userid:newData.userid}, updObj, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Induction Answers not updated'});
				else
					callback(true,{err:2,text:'Induction Answers updated'});
			});
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}

// --------------------------------------------------------------------------
// Set Exit status
// --------------------------------------------------------------------------
exports.setExit = function(newData, callback)   {
	var users  = dbConn.collection(newData.coid+'_users');

	users.findOne({userid:parseInt(newData.userid)}, function(e, o)   {
		if (o)			{
			o.exit = true;
			users.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Exit status not updated'});
				else
					callback(true,{err:0,text:'Exit status updated'});
			});
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}

// --------------------------------------------------------------------------
// Get user notifications
// --------------------------------------------------------------------------

exports.getnotiftot = function(coid, userid, callback)   {
	var notifs  = dbConn.collection(coid+'_notifs');

	notifs.find({userid: parseInt(userid), rflag:false, sflag:false}).count(function(e, tot)   {
		if (e)
			callback(true,0);
		else		{
			callback(false,tot);
		}
	});
};

function getnotiftot(coid, userid, callback)   {
	var notifs  = dbConn.collection(coid+'_notifs');

	notifs.find({userid: parseInt(userid), rflag:false, sflag:false}).count(function(e, tot)   {
		if (e)
			callback(true,0);
		else		{
			callback(false,tot);
		}
	});
};

exports.getnotifs = function(coid, userid, callback)   {
	var notifs  = dbConn.collection(coid+'_notifs');

	notifs.find({userid: userid, rflag:false}).sort({date:-1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No notifications'});
		else    {
      resetnotifs(coid, userid, function (e1,o1)    {
      });
		  //console.log(o);
			callback(false,o);
		}
	});
};

function getnotifs(coid, userid, callback)   {
	var notifs  = dbConn.collection(coid+'_notifs');

	notifs.find({userid: userid, rflag:false}).sort({date:-1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No notifications'});
		else    {
      resetnotifs(coid, userid, function (e1,o1)    {
      });
			callback(false,o);
		}
	});
};

exports.updnotifs = function(newData, callback)   {
	var notifs  = dbConn.collection(newData.coid+'_notifs');

	notifs.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.rflag = true;
			notifs.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Notification not updated'});
				else   {
					getnotifs(newData.coid,newData.userid, function(e2,o2)     {
						if (e2)
							callback(true,{err:2,text:'Notification not updated'});
						else
							callback(false,o2);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in department'});
		}
	});
};

exports.addnotifs = function(newData, callback)   {
	var notifs  = dbConn.collection(newData.coid+'_notifs');

	newData.date = new Date();
	newData.userid = parseInt(newData.userid);
	newData.sflag = false;
  delete newData.coid;
  //console.log(newData);
	notifs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'Notification not added'});
		else   {
			getnotifs(newData.coid,newData.userid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Notification data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

// Mark all notifications for the user as Seen
function resetnotifs(coid, userid, callback)   {
	var notifs  = dbConn.collection(coid+'_notifs');

	notifs.update({userid:userid}, {$set: {"sflag": true}},{ multi: true }, function(e, o)   {
		callback(false,o);
	});
};

function validatePassword(plainPass, hashedPass, callback)    {
  //console.log(plainPass);
  //console.log(hashedPass);
  //console.log("------------");
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
  //console.log(validHash);
  //console.log("------------");
	if (hashedPass === validHash)   {
    //console.log("same");
    callback(false, 1);
  } else  {
    //console.log("Not  same");
    callback(true, 1);
	  //callback(null, hashedPass === validHash);
  }
}

/* private encryption & validation methods */
var generateSalt = function()   {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}


