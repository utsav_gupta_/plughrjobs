if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPbiznum = function(coid, userid, callback)   {
	var biznum  = dbConn.collection(coid+'_biznum');

	biznum.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			biznum.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'biznum not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading biznum'});
	});
};
exports.getNPbiznum = function(coid, userid, pageno, callback)   {
	var biznum  = dbConn.collection(coid+'_biznum');

	biznum.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'biznum not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallbiznum = function(coid, userid, callback)   {
	var biznum  = dbConn.collection(coid+'_biznum');

	biznum.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'biznum not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewbiznum = function(newData, callback)   {
	var biznum  = dbConn.collection(newData.coid+'_biznum');

	newData.date = new Date();
	newData.value = newData.value;

	var dt = newData.vfrom.split('-');
	newData.vfrom = new Date(dt[0], dt[1]-1, dt[2]);

	var dt = newData.vto.split('-');
	newData.vto = new Date(dt[0], dt[1]-1, dt[2]);

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	biznum.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'biznum not added'});
		else   {
			getFPbiznum(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table biznum data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatebiznum = function(newData, callback)   {
	var biznum  = dbConn.collection(newData.coid+'_biznum');

	biznum.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.value = newData.value;

	    var dt = newData.vfrom.split('-');
			o.vfrom = new Date(dt[0], dt[1]-1, dt[2]);

	    var dt = newData.vto.split('-');
			o.vto = new Date(dt[0], dt[1]-1, dt[2]);

			delete o.coid;
			biznum.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'biznum not updated'});
				else   {
					getFPbiznum(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table biznum data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in biznum'});
		}
	});
}
exports.deletebiznum = function(newData, callback)   {
	var biznum  = dbConn.collection(newData.coid+'_biznum');

	biznum.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'biznum data not deleted'});
		else   {
			getFPbiznum(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table biznum data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPbiznum = function(coid, userid, callback)   {
	var biznum  = dbConn.collection(coid+'_biznum');

	biznum.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			biznum.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'biznum not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading biznum'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

