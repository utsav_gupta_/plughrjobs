if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPperfgoals = function(coid, userid, callback)   {
	var perfgoals  = dbConn.collection(coid+'_perfgoals');

	perfgoals.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			perfgoals.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'perfgoals not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading perfgoals'});
	});
};
exports.getNPperfgoals = function(coid, userid, dept, pageno, callback)   {
	var perfgoals  = dbConn.collection(coid+'_perfgoals');

	perfgoals.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallperfgoals = function(coid, userid, callback)   {
	var perfgoals  = dbConn.collection(coid+'_perfgoals');

	perfgoals.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getgoalsbyperiod_user = function(searchData, callback)   {
	var perfgoals  = dbConn.collection(searchData.coid+'_perfgoals');

	perfgoals.find({userid:searchData.userid , rperiod:searchData.rperiod}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getperfgoalsbyrole = function(searchData, callback)   {
	var perfgoals  = dbConn.collection(searchData.coid+'_perfgoals');

	perfgoals.find({role:searchData.roleid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getperfgoalsbyrolekra = function(searchData, callback)   {
	var perfgoals  = dbConn.collection(searchData.coid+'_perfgoals');

	perfgoals.find({role:searchData.roleid, kra:searchData.kraid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewperfgoals = function(newData, callback)   {
	var perfgoals  = dbConn.collection(newData.coid+'_perfgoals');

	newData.date = new Date();
	newData.desc = newData.desc;
	newData.role = newData.role;
	newData.kra = newData.kra;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	perfgoals.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'perfgoals not added'});
		else   {
			getperfgoalsbyrolekra(scoid,suserid,newData.role,newData.kra, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table perfgoals data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateperfgoals = function(newData, callback)   {
	var perfgoals  = dbConn.collection(newData.coid+'_perfgoals');

	perfgoals.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.desc = newData.desc;
			o.role = newData.role;
			o.kra = newData.kra;
			delete o.coid;
			perfgoals.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'perfgoals not updated'});
				else   {
					getperfgoalsbyrolekra(newData.coid,newData.userid,newData.role,newData.kra, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table perfgoals data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in perfgoals'});
		}
	});
}
exports.deleteperfgoals = function(newData, callback)   {
	var perfgoals  = dbConn.collection(newData.coid+'_perfgoals');

	perfgoals.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'perfgoals data not deleted'});
		else   {
			getperfgoalsbyrolekra(newData.coid, newData.userid, newData.role, newData.kra, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table perfgoals data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPperfgoals = function(coid, userid, callback)   {
	var perfgoals  = dbConn.collection(coid+'_perfgoals');

	perfgoals.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			perfgoals.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'perfgoals not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading perfgoals'});
	});
}
var getperfgoalsbyrolekra = function(coid, userid, roleid, kraid, callback)   {
	var perfgoals  = dbConn.collection(coid+'_perfgoals');

	perfgoals.find({role:roleid, kra:kraid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'perfgoals not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

