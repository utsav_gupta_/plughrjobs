if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPsalstru = function(coid, userid, callback)   {
	var salstru  = dbConn.collection(coid+'_salstru');

	salstru.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salstru.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salstru not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salstru'});
	});
};
exports.getNPsalstru = function(coid, userid, pageno, callback)   {
	var salstru  = dbConn.collection(coid+'_salstru');

	salstru.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salstru not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallsalstru = function(coid, userid, callback)   {
	var salstru  = dbConn.collection(coid+'_salstru');

	salstru.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salstru not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewsalstru = function(newData, callback)   {
	var salstru  = dbConn.collection(newData.coid+'_salstru');

	newData.date = new Date();
	newData.ssdesc = newData.ssdesc;
	newData.ssearnings = newData.ssearnings;
	newData.ssdeductions = newData.ssdeductions;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	salstru.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'salstru not added'});
		else   {
			getFPsalstru(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table salstru data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatesalstru = function(newData, callback)   {
	var salstru  = dbConn.collection(newData.coid+'_salstru');

	salstru.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.sstitle = newData.sstitle;
			o.ssdesc = newData.ssdesc;
			o.ssearnings = newData.ssearnings;
			o.ssdeductions = newData.ssdeductions;
			delete o.coid;
			salstru.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'salstru not updated'});
				else   {
					getFPsalstru(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table salstru data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in salstru'});
		}
	});
}
exports.deletesalstru = function(newData, callback)   {
	var salstru  = dbConn.collection(newData.coid+'_salstru');

	salstru.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'salstru data not deleted'});
		else   {
			getFPsalstru(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table salstru data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPsalstru = function(coid, userid, callback)   {
	var salstru  = dbConn.collection(coid+'_salstru');

	salstru.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salstru.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salstru not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salstru'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

