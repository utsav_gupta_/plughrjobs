if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPblogs = function(coid, userid, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			blogs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'blogs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading blogs'});
	});
};
exports.getNPblogs = function(coid, userid, pageno, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'blogs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getoneblog = function(coid, userid, blogid, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({_id: getObjectId(blogid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No blogs selected'});
		else    {
			callback(false,o);
		}
	});
}
exports.getselectedblogs = function(coid, userid, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({selected:true}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No blogs selected'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallblogs = function(coid, userid, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({}).sort({date:-1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'blogs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewblogs = function(newData, callback)   {
	var blogs  = dbConn.collection(newData.coid+'_blogs');

	newData.date = new Date();
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	blogs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'blogs not added'});
		else   {
			getFPblogs(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table blogs data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateblogs = function(newData, callback)   {
	var blogs  = dbConn.collection(newData.coid+'_blogs');

	blogs.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.date = new Date();
			o.author = newData.author;
			o.stext = newData.stext;
			o.ftext = newData.ftext;
			delete o.coid;
			blogs.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'blogs not updated'});
				else   {
					getFPblogs(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table blogs data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in blogs'});
		}
	});
}
exports.setselected = function(newData, callback)   {
	var blogs  = dbConn.collection(newData.coid+'_blogs');

	if (newData.blog != "")			{
		blogs.findOne({_id: getObjectId(newData.blog)}, function(e, o)   {
			if (o)   {
				o.selected = newData.value;
				delete o.coid;
				blogs.save(o, {safe: true}, function (e1,o1)      {
					if (e1)
						callback(true,{err:1,text:'blog1 not updated'});
					else
						callback(false,o1);
				});
			} else  {
				callback(true,{err:2,text:' ID not found in blogs'});
			}
		});
	} else	{
		callback(false,{err:0,text:'currently no blogs published'});
	}
}
exports.deleteblogs = function(newData, callback)   {
	var blogs  = dbConn.collection(newData.coid+'_blogs');

	blogs.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'blogs data not deleted'});
		else   {
			getFPblogs(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table blogs data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPblogs = function(coid, userid, callback)   {
	var blogs  = dbConn.collection(coid+'_blogs');

	blogs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			blogs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'blogs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading blogs'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

