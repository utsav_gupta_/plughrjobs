if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPcommittees = function(coid, userid, callback)   {
	var committees  = dbConn.collection(coid+'_committees');

	committees.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			committees.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'committees not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading committees'});
	});
};
exports.getNPcommittees = function(coid, userid, pageno, callback)   {
	var committees  = dbConn.collection(coid+'_committees');

	committees.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'committees not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallcommittees = function(coid, userid, callback)   {
	var committees  = dbConn.collection(coid+'_committees');

	committees.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'committees not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewcommittees = function(newData, callback)   {
	var committees  = dbConn.collection(newData.coid+'_committees');

	newData.date = new Date();
	newData.purpose = newData.purpose;
	newData.cmembers = newData.cmembers;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	committees.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'committees not added'});
		else   {
			getFPcommittees(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table committees data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatecommittees = function(newData, callback)   {
	var committees  = dbConn.collection(newData.coid+'_committees');

	committees.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.comname = newData.comname;
			o.purpose = newData.purpose;
			o.cmembers = newData.cmembers;
			delete o.coid;
			committees.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'committees not updated'});
				else   {
					getFPcommittees(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table committees data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in committees'});
		}
	});
}
exports.deletecommittees = function(newData, callback)   {
	var committees  = dbConn.collection(newData.coid+'_committees');

	committees.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'committees data not deleted'});
		else   {
			getFPcommittees(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table committees data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPcommittees = function(coid, userid, callback)   {
	var committees  = dbConn.collection(coid+'_committees');

	committees.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			committees.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'committees not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading committees'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

