/* ------------------------------------------
  RPeriod Frequency 1 = "Yearly"
  RPeriod Frequency 2 = "Half-yearly"
  RPeriod Frequency 3 = "Quarterly"
  RPeriod Frequency 4 = "Monthly"

  RPeriod status 1 = "Yet to Start"
  RPeriod status 2 = "Active"
  RPeriod status 3 = "Overdue"
  RPeriod status 4 = "Closed"

  message type 1 = "Action required"
  message type 2 = "Information"
------------------------------------------ */

var EM        = require('./email-dispatcher');
var revperiod = require('./revperiod-manager');
var apprsl = require('./apprsl-manager');
var async 		= require('async');
var cvalues = require('./cvalues-manager');
var roles = require('./roles-manager');
var kra = require('./kra-manager');
var users = require('./user-manager');
var usergoals = require('./usergoals-manager');

// --------------------------------------------------------------------------
// BG Process - Select only live companies from list of companies
// --------------------------------------------------------------------------
exports.SelectLiveCO = function(userid, callback)   {
	var company = dbConn.collection('company');
	var bg01  = dbConn.collection('bgtable01');

	bg01.remove({}, function (e2,o2)      {
    if (!e2)    {
	    company.find({}).sort({companyid:1}).toArray(function(e, o)   {
		    if (e)
		      // Notification for PlugHRapp Admin
			    callback(true,{err:1,text:'company database not found'});
		    else    {
			    var colen = o.length;
			    for(var x=0;x<colen; x++)		{
				    var coObj = o[x];
				    if (coObj.live && coObj.live == true)		{
            	bg01.insert(coObj, {safe: true}, function (e1,o1)      {
		            if (e1)
			            callback(true,{err:1,text:'bgtable01 not inserted'});
		            else
            			callback(false,{err:0,text:'companies processed'});
            	});
				    }
			    }
			    callback(false,{err:0,text:'companies processed'});
		    }
	    });
    } else  {
	    callback(true,{err:3,text:'old data not removed'});
    }
	});
}

// ------------------------------------------------------------------------------------------------------
// BG Process - Clear all Action required messages prior to current date from notification collections
// ------------------------------------------------------------------------------------------------------
exports.clearnotifs = function(dummy, callback)   {
	var bg01  = dbConn.collection('bgtable01');
  var cdate = new Date();
  //cdate.setHours(0,0,0,0);

	// Step #1 - delete all Action required messages before today
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
	  async.forEach(colist, function(coObj, callback) {
    	var notifs  = dbConn.collection(coObj.companyid+'_notifs');
    	var query = { mtype:1, date: { $lt: cdate}};
      //console.log(query);
		  notifs.remove(query, function(e,o)    {
			  callback(e,o);
		  });
	  }, function() {
		  return;
	  });
	});

	// Step #2 - delete all messages with ReadFlag set to true
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
	  async.forEach(colist, function(coObj, callback) {
    	var notifs  = dbConn.collection(coObj.companyid+'_notifs');
    	var query = { rflag:true};
		  notifs.remove(query, function(e,o)    {
			  callback(e,o);
		  });
	  }, function() {
		  return;
	  });
	});
}

// Create the first review period based on company settings
exports.Proc01 = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
  	    //console.log("-" + coObj.companyid + "-");
				if (!coObj.prdate || coObj.prdate == '' || !coObj.psdate || coObj.psdate == '' )	{
					//console.log("Company " + coObj.companyid + " has no review starting period defined");
					notifyadmins(coObj.companyid,1, "Please define a review starting period defined in company settings");
				} else	{
					first_rperiod(coObj);
				}
				callback(null,null);
			});
			callback(false,{err:0,text:'companies processed'});
		}
	});
}

// Create NEW review periods based on company settings
exports.Proc02 = function(userid, callback)   {
	//console.log("in proc 02 " + new Date());
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
				if (coObj.crsdate && coObj.credate)	{
					manage_rperiod(coObj);
				}
				callback();
			});
			callback(false,{err:0,text:'companies processed'});
		}
	});
}

// Check for users without targets
// Get list of users without target, for each open review period, for each company which is live
exports.Proc031 = function(userid, callback)   {
	var old  = dbConn.collection('bgtable02');
	old.remove({}, function (e1,o1)      {
    if (!e1)    {
	    var bg01  = dbConn.collection('bgtable01');
	    bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		    if (e)
			    callback(true,{err:1,text:'company not found'});
		    else    {
      	  async.forEachSeries(colist, function(coObj, callback) {
				    users_wo_targets(coObj);
				    callback();
			    });
			    callback(false,{err:0,text:'companies processed'});
		    }
	    });
    } else  {
      console.log('nolist not deleted');
			callback(true,{err:1,text:'nolist not delete'});
    }
  });
}

var users_wo_targets = function(coObj)		{
	if (!coObj)
		return;
	rpfound = false;
	revperiod.getallrevperiod(coObj.companyid, '', function(e1, rperiods) {
	  async.forEachSeries(rperiods, function(rpObj, callback) {
			if (rpObj.status == '2')		{
			  var rpid = rpObj._id.toString();
	      getallusers(coObj.companyid, function(e1,users)		{
		      async.forEachSeries(users, function(usrObj, callback) {
			      verifyusergoal(coObj.companyid.toString(), usrObj.userid.toString(), rpid, function (e2,o2)			{
				      if (o2.err > 0)		{
    			      //console.log(coObj.companyid + " -- " + usrObj.username + " reporting to "+ usrObj.mgr +" has no target defined for period - "+rpObj.rptitle);
              	var nolist  = dbConn.collection('bgtable02');
			          nolist.insert({ coid:coObj.companyid, revperiod:rpid, rptitle:rpObj.rptitle, userid : usrObj.userid, username : usrObj.username, mgr:usrObj.mgr }, function(e,o)    {
				          //callback();
			          });
			       	}
				      callback();
			      });
		      }, function() {
			      callback();
		      });
	      });
			}
			callback();
		});
	});
}

var verifyusergoal = function(coid, userid, rpid, callback)   {
	var ugoals = dbConn.collection(coid +'_usergoals');
	ugoals.find({userid:userid, rperiod:rpid, trgt_submited:'1'}).count(function(e, gcount)   {
		if (gcount > 0 )    {
			callback(false,{err:0,text:'Goals defined for user'});
		} else  {
      //console.log(userid + " -- " + rpid);
  		callback(true,{err:1,text:'No goals defined for user'});
		}
	});
}

// Check for users without targets
// Get list of managers and calculcate nolist from bgtable02
// This one informs the Managers
exports.Proc032 = function(userid, callback)   {
  var bg02  = dbConn.collection('bgtable02');
  bg02.find({}).sort({coid:1, revperiod:1, mgr:1}).toArray(function(e, nolist)   {
    if (e)  {
	    callback(true,{err:1,text:'nolist data not found'});
    } else    {
      //console.log(nolist);
      if (nolist && nolist.length > 0)   {
        var newlist = [];
        var coid = nolist[0].coid;
        var rpid = nolist[0].revperiod;
        var rptitle = nolist[0].rptitle;
        var mgrid = nolist[0].mgr;
        var nolen = nolist.length;
        for (var x=0; x < nolen; x++)   {
          var noObj = nolist[x];
		      //console.log(JSON.stringify(noObj));
		      if (noObj.coid == coid && noObj.revperiod == rpid && noObj.mgr == mgrid)  {
		        newlist.push(noObj.username);
		      } else  {
            //console.log(coid, rpid, rptitle, mgrid, newlist);
            var notifs  = dbConn.collection(coid+'_notifs');
            var cdate = new Date();
            //cdate.setHours(0,0,0,0);

            var tstr = "set targets";
            var lstr = tstr.link("/434");
            var mesg = newlist.length + " users ( "+newlist+" ) are without targets for the current performance period. Please "+ lstr +" at the earliest.";
            //console.log(mgrid + " -- " + mesg);
            //var mesg = "For review period "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +", Targets are not set for "+newlist.length+ " users( "+newlist+" ). Please set targets at earliest."
            notifs.insert({ userid: parseInt(mgrid), mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	            callback(e,o);
            });
            newlist = [];
		        newlist.push(noObj.username);
            coid = noObj.coid;
            rpid = noObj.revperiod;
            rptitle = noObj.rptitle;
            mgrid = noObj.mgr;
		      }
	      }
	      //console.log("new list = " + newlist);
	      if (newlist.length > 0)   {
          var notifs  = dbConn.collection(coid+'_notifs');
          var cdate = new Date();
          //cdate.setHours(0,0,0,0);

          var tstr = "set targets";
          var lstr = tstr.link("/434");
          var mesg = newlist.length + " users ( "+newlist+" ) are without targets for the current performance period. Please "+ lstr +" at the earliest.";
          //console.log(mgrid + " -- " + mesg);
          //var mesg = "For review period "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +", Targets are not set for "+newlist.length+ " users( "+newlist+" ). Please set targets at earliest."
          notifs.insert({ userid: parseInt(mgrid), mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
            callback(e,o);
          });
	      }
      }
    }
  });
}

// Check for users without targets
// Get list of Admins and calculcate nolist from bgtable02
// This one informs the HR Admins
exports.Proc033 = function(userid, callback)   {
  var bg02  = dbConn.collection('bgtable02');
  bg02.find({}).sort({coid:1, revperiod:1, mgr:1}).toArray(function(e, nolist)   {
    if (e)  {
	    callback(true,{err:1,text:'nolist data not found'});
    } else    {
      //console.log(nolist);
      if (nolist && nolist.length > 0)   {
        var newlist = [];
        var coid = nolist[0].coid;
        var rpid = nolist[0].revperiod;
        var rptitle = nolist[0].rptitle;
        var nolen = nolist.length;
        for (var x=0; x < nolen; x++)   {
          var noObj = nolist[x];
		      //console.log(JSON.stringify(noObj));
		      if (noObj.coid == coid && noObj.revperiod == rpid )  {
		        newlist.push(noObj.username);
		      } else  {
	          getalladmins(coid, function(e1,users)		{
		          var utot = users.length;
		          async.forEach(users, function(usrObj, callback) {
                var notifs  = dbConn.collection(coid+'_notifs');
                var cdate = new Date();
                var tstr = "here";
                var lstr = tstr.link("/227");
                var mesg = newlist.length + " team members are without targets for "+rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +". Please look up the list " + lstr;
                //console.log(coid+" --> "+usrObj.userid+" : "+ mesg);
                notifs.insert({ userid: usrObj.userid, mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	                callback(e,o);
                });
		          }, function() {
                newlist = [];
		            newlist.push(noObj.username);
                coid = noObj.coid;
                rpid = noObj.revperiod;
                rptitle = noObj.rptitle;
                mgrid = noObj.mgr;
		          });
	          });
		      }
	      }
	      if (newlist.length > 0)   {
          getalladmins(coid, function(e1,users)		{
	          var utot = users.length;
	          async.forEach(users, function(usrObj, callback) {
              var notifs  = dbConn.collection(coid+'_notifs');
              var cdate = new Date();
              var tstr = "here";
              var lstr = tstr.link("/227");
              var mesg = newlist.length + " team members are without targets for "+rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +". Please look up the list " + lstr;
              //console.log(coid+" --> "+usrObj.userid+" : "+ mesg);
              notifs.insert({ userid: usrObj.userid, mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
                callback(e,o);
              });
	          }, function() {
              newlist = [];
	            newlist.push(noObj.username);
              coid = noObj.coid;
              rpid = noObj.revperiod;
              rptitle = noObj.rptitle;
              mgrid = noObj.mgr;
	          });
          });
	      }
      }
    }
  });
}

// Rate Now Notif to the Employee on expiry of performance period
exports.Proc04 = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
				rate_now(coObj);
				callback();
			});
			callback(false,{err:0,text:'companies processed'});
		}
	});
}

var rate_now = function(coObj)		{
	if (!coObj)
		return;
	rpfound = false;
	revperiod.getallrevperiod(coObj.companyid, '', function(e1, rplist) {
    async.forEachSeries(rplist, function(rpObj, callback) {
			if (rpObj.status == '3')		{
				var coid = coObj.companyid;
				verify_selfrate(coid, rpObj._id.toString(), function(e,ttot)		{
          callback();
				});
			}
		}, function() {
			return;
		});
	});
}

var verify_selfrate = function(coid, rpid, callback1)		{
	// check all employees for goal setting
	getallusers(coid, function(e1,users)		{
		var ttot = 0;			//total users without tasks
		async.forEach(users, function(usrObj, callback) {
			verifyuserrating(coid.toString(), usrObj.userid.toString(), rpid, function (e2,o2)			{
				callback();
			});
		}, function() {
			callback1(0,ttot);
		});
	});
}

var verifyuserrating = function(coid, userid, rpid, callback)   {
	async.waterfall([
		function(callback) {
			revperiod.getrptitle(coid, rpid, function(e2, rpdata) {
				callback(e2,rpdata[0].rptitle);
			});
		},
		function(rptitle, callback) {
    	var ugoals = dbConn.collection(coid +'_usergoals');
    	ugoals.find({userid:userid, rperiod:rpid}).toArray(function(e3,glist)	{
				if (glist && glist.length > 0)		{
          async.forEachSeries(glist, function(gObj, callback) {
				    ttitle = gObj.ugoaltitle;
				    //console.log(gObj);
            // first check if supervisor has committed target
      			if (gObj.trgt_submited && gObj.trgt_submited == "1" )		{
        			if (!gObj.self_submited || gObj.self_submited == "0" || gObj.self_submited == "")		{
                console.log(coid+" --> "+ userid + "--> Please complete self-rating for target "+ ttitle +" of "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +" at earliest & submit for supervisor rating");
                var notifs  = dbConn.collection(coid+'_notifs');
                var cdate = new Date();
                //cdate.setHours(0,0,0,0);

                var tstr = "here";
                var lstr = tstr.link("/432");
                var mesg = "Your self rating for current performance period is due. Do the rating "+lstr;
                //var mesg = "Please complete self-rating for target "+ ttitle +" of "+ rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +" at earliest & submit for supervisor rating";
                notifs.insert({ userid: parseInt(userid), mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	                //callback(e,o);
                });
        			}
      			}
		      }, function() {
			      callback();
    			});
				}
				callback();
			});
	  },
		], function(err, results) {
			callback();
		}
	);
}

// Check for pending supervisor rating
// Get list of Admins and calculcate nolist from bgtable02
exports.Proc06 = function(userid, callback)   {
  var bg02  = dbConn.collection('bgtable02');
  bg02.find({}).sort({coid:1, revperiod:1, mgr:1}).toArray(function(e, nolist)   {
    if (e)  {
	    callback(true,{err:1,text:'nolist data not found'});
    } else    {
      //console.log(nolist);
      if (nolist && nolist.length > 0)   {
        var newlist = [];
        var coid = nolist[0].coid;
        var rpid = nolist[0].revperiod;
        var rptitle = nolist[0].rptitle;
        var nolen = nolist.length;
        for (var x=0; x < nolen; x++)   {
          var noObj = nolist[x];
		      //console.log(JSON.stringify(noObj));
		      if (noObj.coid == coid && noObj.revperiod == rpid )  {
		        newlist.push(noObj.username);
		      } else  {
	          getalladmins(coid, function(e1,users)		{
		          var utot = users.length;
		          async.forEach(users, function(usrObj, callback) {
                var notifs  = dbConn.collection(coid+'_notifs');
                var cdate = new Date();
                var tstr = "here";
                var lstr = tstr.link("/227");
                var mesg = newlist.length + " team members are without targets for "+rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +". Please look up the list " + lstr;
                //console.log(coid+" --> "+usrObj.userid+" : "+ mesg);
                notifs.insert({ userid: usrObj.userid, mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	                callback(e,o);
                });
		          }, function() {
                newlist = [];
		            newlist.push(noObj.username);
                coid = noObj.coid;
                rpid = noObj.revperiod;
                rptitle = noObj.rptitle;
                mgrid = noObj.mgr;
		          });
	          });
		      }
	      }
	      if (newlist.length > 0)   {
          getalladmins(coid, function(e1,users)		{
	          var utot = users.length;
	          async.forEach(users, function(usrObj, callback) {
              var notifs  = dbConn.collection(coid+'_notifs');
              var cdate = new Date();
              var tstr = "here";
              var lstr = tstr.link("/227");
              var mesg = newlist.length + " team members are without targets for "+rptitle.substr(0,rptitle.indexOf(' 00:00:00')) +". Please look up the list " + lstr;
              //console.log(coid+" --> "+usrObj.userid+" : "+ mesg);
              notifs.insert({ userid: usrObj.userid, mailid : '', mtype:1, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
                callback(e,o);
              });
	          }, function() {
              newlist = [];
	            newlist.push(noObj.username);
              coid = noObj.coid;
              rpid = noObj.revperiod;
              rptitle = noObj.rptitle;
              mgrid = noObj.mgr;
	          });
          });
	      }
      }
    }
  });
}

// Check for Appraisals starting today and create them
exports.Proc09a = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				tmp_appraisals(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update the skip manager for each user
exports.Proc09x = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_skipmgr(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update Behavioral competencies
exports.Proc09b = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_bv(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update Core values
exports.Proc09c = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_cv(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update KRAs
exports.Proc09d = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_kra(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update review data
exports.Proc09e = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_rvp(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update Appraisal status as Started
exports.Proc09f = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				upd_aprsl_sts(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Update Appraisal status as Started
exports.Proc09g = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj.companyid);
				aprsl_notifs(coObj);
				callback();
			});
			callback(false,{err:0,text:'Appraisals created'});
		}
	});
}

// Check for users with new notifications today and email them
exports.Proc10 = function(userid, callback)   {
	var bg01  = dbConn.collection('bgtable01');
	bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
        //console.log(coObj);
				email_alerts(coObj);
				callback();
			});
			callback(false,{err:0,text:'companies processed'});
		}
	});
}

var first_rperiod = function(coObj)		{
	if (!coObj)
		return;
	if (!coObj.psdate || coObj.psdate == '')
		return;
	if (!coObj.prdate || coObj.prdate == '')
		return;

	// get first review start and end dates
	var tmpdate = coObj.psdate.split("-");
	var frsdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
	var tmpdate = coObj.prdate.split("-");
	var fredate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
	//console.log(frsdate);
	//console.log(fredate);

	// Create first review period if not already available
	var rpfound = false;
	revperiod.getallrevperiod(coObj.companyid, '', function(e1, o1) {
		//console.log(o1);
		if (o1.length > 0)		{
			var rlen = o1.length;
			for(x=0;x<rlen;x++)		{
				var rpObj = o1[x];

				if (typeof(rpObj.sdate) == "string")	{
					var tpos = rpObj.sdate.indexOf('T');
					//console.log(tpos);
					if (tpos != -1)
						rpObj.sdate = rpObj.sdate.substring(0,tpos);
					var tmpdate = rpObj.sdate.split("-");
					//console.log(tmpdate);
					rpObj.sdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				}

				if (typeof(rpObj.edate) == "string")	{
					var tpos = rpObj.edate.indexOf('T');
					//console.log(tpos);
					if (tpos != -1)
						rpObj.edate = rpObj.edate.substring(0,tpos);
					var tmpdate = rpObj.edate.split("-");
					//console.log(tmpdate);
					rpObj.edate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				}
        //console.log(coObj.companyid);
				//console.log(rpObj);
				if (rpObj.sdate.getTime() == frsdate.getTime() && rpObj.edate.getTime() == fredate.getTime())		{  
					rpfound = true;
				}
			}
      var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			if (!rpfound)	{
        var dd = frsdate.getDate();
        var mm = frsdate.getMonth()+1; //January is 0!
        var mname = monthNames[mm-1];
        var yyyy = frsdate.getFullYear();
        if (dd < 10){
          dd = '0'+ dd;
        } 
        if(mm < 10){
            mm = '0'+mm;
        } 
        var t1date = dd+'-'+mname+'-'+yyyy;

        var dd = fredate.getDate();
        var mm = fredate.getMonth()+1; //January is 0!
        var mname = monthNames[mm-1];
        var yyyy = fredate.getFullYear();
        if (dd < 10){
          dd = '0'+ dd;
        } 
        if(mm < 10){
            mm = '0'+mm;
        } 
        var t2date = dd+'-'+mname+'-'+yyyy;

				revperiod.addnewrevperiod({
					coid : coObj.companyid,
					userid : '',
					rptitle : 'Review starting from '+ t1date +' to '+t2date,
					sdate : frsdate,
					edate : fredate,
					status : '2',
					frozen : 'false',
				}, function(e,o)	{
					//emailadmins(coObj.companyid, "A New review period starting " + frsdate + " has been created");
					//console.log("New review starting period has started-1");
  				var dt = frsdate.toString();

          var tstr = "set targets";
          var lstr = tstr.link("/434");
          var nstr = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr +" for your team."

          var tstr1 = "set self targets";
          var lstr1 = tstr1.link("/404");
          var nstr1 = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr1;
          if (coObj.prwho == '1')   {
  					notifymgrs(coObj.companyid,1, nstr);
  					notifytopm(coObj.companyid,1, nstr1);
  				} else
  					notifyadmins(coObj.companyid,1, nstr);
					return;
				});
			}
		} else		{
			//console.log('No rev periods');
      var dd = frsdate.getDate();
      var mm = frsdate.getMonth()+1; //January is 0!
      var mname = monthNames[mm-1];
      var yyyy = frsdate.getFullYear();
      if (dd < 10){
        dd = '0'+ dd;
      } 
      if(mm < 10){
          mm = '0'+mm;
      } 
      var t1date = dd+'-'+mname+'-'+yyyy;

      var dd = fredate.getDate();
      var mm = fredate.getMonth()+1; //January is 0!
      var mname = monthNames[mm-1];
      var yyyy = fredate.getFullYear();
      if (dd < 10){
        dd = '0'+ dd;
      } 
      if(mm < 10){
          mm = '0'+mm;
      } 
      var t2date = dd+'-'+mname+'-'+yyyy;

			revperiod.addnewrevperiod({
				coid : coObj.companyid,
				userid : '',
				rptitle : 'Review starting from '+ t1date +' to '+t2date,
				sdate : frsdate,
				edate : fredate,
				status : '2',
				frozen : 'false',
			}, function(e,o)	{
				//emailadmins(coObj.companyid, "A New review period starting " + frsdate + " has been created");
				//console.log("New review starting period has started-2");
				var dt = frsdate.toString();

        var tstr = "set targets";
        var lstr = tstr.link("/434");
        var nstr = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr +" for your team."

        var tstr1 = "set self targets";
        var lstr1 = tstr1.link("/404");
        var nstr1 = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr1;
        if (coObj.prwho == '1')   {
					notifymgrs(coObj.companyid,1, nstr);
					notifytopm(coObj.companyid,1, nstr1);
				} else
					notifyadmins(coObj.companyid,1, nstr);
				return;
			});
		}
	});
}

function IsleapYear(year)
{
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

var manage_rperiod = function(coObj)		{
	if (!coObj)
		return;

	// get review frequency in months
	var nom = 0;
	switch (coObj.rperiod)		{
		case '1':		// yearly
			nom = 12;
			break;
		case '2':		// Half-yearly
			nom = 6;
			break;
		case '3':		// Quarterly
			nom = 3;
			break;
		case '4':		// Monthly
			nom = 1;
			break;
	}

	rpfound = false;
	revperiod.getallrevperiod(coObj.companyid, '', function(e1, o1) {
		//console.log(o1);
		if (o1.length > 0)		{
			// get Todaay's date
			var cdate = new Date();
			cdate.setHours(0, 0, 0, 0);

      var daysofmon = [31,28,31,30,31,30,31,31,30,31,30,31];
      //console.log(cdate.getFullYear());
      if (IsleapYear(cdate.getFullYear()))
        daysofmon[1] = 29;
      //console.log(daysofmon);

			var rlen = o1.length;
			for(x=0;x<rlen;x++)		{
				var rpObj = o1[x];

				//console.log(rpObj);
				if (typeof(rpObj.sdate) == "string")	{
					var tpos = rpObj.sdate.indexOf('T');
					//console.log(tpos);
					if (tpos != -1)
						rpObj.sdate = rpObj.sdate.substring(0,tpos);
					var tmpdate = rpObj.sdate.split("-");
					//console.log(tmpdate);
					rpObj.sdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				}

				if (typeof(rpObj.edate) == "string")	{
					var tpos = rpObj.edate.indexOf('T');
					//console.log(tpos);
					if (tpos != -1)
						rpObj.edate = rpObj.edate.substring(0,tpos);
					var tmpdate = rpObj.edate.split("-");
					//console.log(tmpdate);
					rpObj.edate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				}
				//console.log(rpObj);

				if (rpObj.status == '2' && rpObj.edate.getTime() < cdate.getTime())		{ 
					// get new review period end date

					var newedate = new Date(rpObj.edate.getTime());
					newedate.addMonths(nom);
					newedate.setDate(daysofmon[newedate.getMonth()]);  //Set the day to last day of month

					// get new review period start date
					var newsdate = new Date(rpObj.edate.getTime());
					newsdate.addDays(1);

					newsdate.setHours(0, 0, 0, 0);
					newedate.setHours(0, 0, 0, 0);

					async.series([
						// check if next period is created & active - if not create a new rperiod based on frequency
						//update Current review period in company table
						function(callback) {
							updrperiod(coObj.companyid, newsdate, newedate, function(e4, o4) {
								callback(e4,o4);
							});
						},
						function(callback) {
							var rpfound = false;
							//console.log('rpfound = ' + rpfound);
							revperiod.getallrevperiod(coObj.companyid, '', function(e3, o3) {
								//console.log(o1);
								if (o3.length > 0)		{
									var rlen = o3.length;
									for(x=0;x<rlen;x++)		{
										var rp = o3[x];
										if (typeof(rp.sdate) == "string")	{
											var tpos = rp.sdate.indexOf('T');
											//console.log(tpos);
											if (tpos != -1)
												rp.sdate = rp.sdate.substring(0,tpos);
											var tmpdate = rp.sdate.split("-");
											//console.log(tmpdate);
											rp.sdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
										}

										if (typeof(rp.edate) == "string")	{
											var tpos = rp.edate.indexOf('T');
											//console.log(tpos);
											if (tpos != -1)
												rp.edate = rp.edate.substring(0,tpos);
											var tmpdate = rp.edate.split("-");
											//console.log(tmpdate);
											rp.edate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
										}
										//console.log(rp);

										var dbsdate = rp.sdate;
										var dbedate = rp.edate;
										if (dbsdate.getTime() == newsdate.getTime() && dbedate.getTime() == newedate.getTime() && rp.status == '2')		{
											rpfound = true;
										}
									}
									//console.log('rpfound = ' + rpfound);
                  var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
									if (!rpfound)	{
                    var dd = newsdate.getDate();
                    var mm = newsdate.getMonth()+1; //January is 0!
                    var mname = monthNames[mm-1];
                    var yyyy = newsdate.getFullYear();
                    if (dd < 10){
                      dd = '0'+ dd;
                    } 
                    if(mm < 10){
                        mm = '0'+mm;
                    } 
                    var t1date = dd+'-'+mname+'-'+yyyy;

                    var dd = newedate.getDate();
                    var mm = newedate.getMonth()+1; //January is 0!
                    var mname = monthNames[mm-1];
                    var yyyy = newedate.getFullYear();
                    if (dd < 10){
                      dd = '0'+ dd;
                    } 
                    if(mm < 10){
                        mm = '0'+mm;
                    } 
                    var t2date = dd+'-'+mname+'-'+yyyy;

										revperiod.addnewrevperiod({
											coid : coObj.companyid,
											userid : '',
											rptitle : 'Review starting from '+ t1date+' to '+t2date,
											sdate : newsdate,
											edate : newedate,
											status : '2',
											frozen : 'false',
										}, function(e,o)	{
              				var dt = newsdate.toString();

                      var tstr = "set targets";
                      var lstr = tstr.link("/434");
                      var nstr = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr +" for your team."

                      var tstr1 = "set self targets";
                      var lstr1 = tstr1.link("/404");
                      var nstr1 = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr1;
                      if (coObj.prwho == '1')   {
              					notifymgrs(coObj.companyid,1, nstr);
              					notifytopm(coObj.companyid,1, nstr1);
              				} else    {
              					notifyadmins(coObj.companyid,1, nstr);
            					}
											callback(e3,o3);;
										});
									}
								} else		{
									//console.log('No rev periods');
                  var dd = newsdate.getDate();
                  var mm = newsdate.getMonth()+1; //January is 0!
                  var mname = monthNames[mm-1];
                  var yyyy = newsdate.getFullYear();
                  if (dd < 10){
                    dd = '0'+ dd;
                  } 
                  if(mm < 10){
                      mm = '0'+mm;
                  } 
                  var t1date = dd+'-'+mname+'-'+yyyy;

                  var dd = newedate.getDate();
                  var mm = newedate.getMonth()+1; //January is 0!
                  var mname = monthNames[mm-1];
                  var yyyy = newedate.getFullYear();
                  if (dd < 10){
                    dd = '0'+ dd;
                  } 
                  if(mm < 10){
                      mm = '0'+mm;
                  } 
                  var t2date = dd+'-'+mname+'-'+yyyy;

									revperiod.addnewrevperiod({
										coid : coObj.companyid,
										userid : '',
										rptitle : 'Review starting from '+ t1date+' to '+t2date,
										sdate : newsdate,
										edate : newedate,
										status : '2',
										frozen : 'false',
									}, function(e,o)	{
            				var dt = newsdate.toString();
                    var tstr = "set targets";
                    var lstr = tstr.link("/434");
                    var nstr = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr +" for your team."

                    var tstr1 = "set self targets";
                    var lstr1 = tstr1.link("/404");
                    var nstr1 = "A New review period starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Please "+ lstr1;
                    if (coObj.prwho == '1')   {
            					notifymgrs(coObj.companyid,1, nstr);
            					notifytopm(coObj.companyid,1, nstr1);
            				} else  {
            					notifyadmins(coObj.companyid,1, nstr);
          					}
										callback(e3,o3);
									});
								}
							});
						},
						//update active status to pending or 3
						function(callback) {
							revperiod.updstatus({coid:coObj.companyid, dbid:rpObj._id, status:'3'}, function(e2, o2) {
								callback(e2,o2);
							});
						},
						], function() {
							return;
						}
					);
				}
			}
		}
	});
}

var email_alerts = function(coObj)		{
	if (!coObj)
		return;
  var tstr = "login";
  var lstr = tstr.link("http://www.plughrapp.com");
  getallusers(coObj.companyid, function(e1,users)		{
    async.forEachSeries(users, function(usrObj, callback) {
      verifyusernotifs(coObj.companyid.toString(), usrObj.userid, function (e2,o2)			{
	      if (o2 > 0)		{
		      //console.log(coObj.companyid + " -- " + usrObj.userid + " has total of "+o2+ " notifcations");
			    EM.notifs({
				    companyid : coObj.companyid,
				    mailid    : usrObj.useremailid,
				    mesg      : "You have "+o2+" new notifications in plugHRapp. Please "+ lstr + ", to check your notifications",
			    }, function(e,o)    {
				    callback(e,o);
			    });
       	}
	      callback();
      });
    }, function() {
      return;
    });
  });
}

var tmp_appraisals = function(coObj)		{
	if (!coObj)
		return;

	// get Todaay's date
	var cdate = new Date();
	cdate.setHours(0, 0, 0, 0);

	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
  aprbg01.remove({}, function(e1,o1)    {
    if (!e1)  {
	    apprsl.getnewapprsl(coObj.companyid, '', function(e1, aprlist) {
	      //console.log(aprlist);
	      async.forEach(aprlist, function(aprObj, callback) {
		      var aprid = aprObj._id.toString();
          var tmp = aprObj.sdate.split("-");
          var aprsdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
        	aprsdate.setHours(0, 0, 0, 0);
      	  var aprdb  = dbConn.collection(coObj.companyid+'_'+aprObj._id);
          aprdb.remove({}, function(e2,o2)    {
            if (!e2)   {
              //console.log("cdate = " + cdate + "  -- " + cdate.getTime());
              //console.log("aprsdate = " + aprsdate + "  -- " + aprsdate.getTime());
        			if (aprsdate.getTime() <= cdate.getTime())		{       // If Curr date is greater than or same as appraisal start date
                //console.log("appraisal date reached for "+aprObj.aprtitle);
                var rvlen = aprObj.rvteam.length;
                var searchObj = new Object;
                searchObj.coid = coObj.companyid;
                searchObj.locid = aprObj.loc;
                searchObj.deptid = aprObj.dept;
                searchObj.rtype = aprObj.role;
                searchObj.emptype = aprObj.etype;
                searchObj.jdate = aprObj.jdate;
                searchusers(searchObj, function(e1,users)		{
                  //console.log(users.length);
                  async.forEach(users, function(usrObj,  callback) {
                    //console.log(usrObj.userid);
                    var selfaprl = false;
                    var supraprl = false;
                    var skipaprl = false;
                    var hraprl = false;
                    var peeraprl = false;
                    var teamaprl = false;

                    for (var x1=0; x1 < rvlen; x1++)    {
        	            switch (aprObj.rvteam[x1])  {
        	              case '1':
        	                selfaprl = true;
        	                break;
        	              case '2':
        	                supraprl = true;
        	                break;
        	              case '3':
        	                skipaprl = true;
        	                break;
        	              case '4':
        	                hraprl = true;
        	                break;
        	              case '5':
        	                peeraprl = true;
        	                break;
        	              case '6':
        	                teamaprl = true;
        	                break;
        	            }
                    }
                    var tmp = aprObj.sdate.split("-");
                    var sdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
                    var tmp = aprObj.edate.split("-");
                    var edate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
                    aprbg01.insert({ 
                        aprslid:aprid, 
                        userid : usrObj.userid,
                        username : usrObj.username,
                        useremailid : usrObj.useremailid,
                        mgr:usrObj.mgr,
                        loc:usrObj.location,
                        dept:usrObj.dept,
                        doj:usrObj.doj,
                        rtype:usrObj.rtype,
                        role:usrObj.role,
                        emptype:usrObj.emptype,
		                    aprtitle : aprObj.aprtitle,
		                    rpwt : aprObj.rpwt,
		                    bvwt : aprObj.bvwt,
		                    cvwt : aprObj.cvwt,
                        sdate : sdate,
                        edate : edate,
                        selfaprl : selfaprl,
                        selfratg : '',
                        supraprl : supraprl,
                        suprratg : '',
                        skipaprl : skipaprl,
                        skipratg : '',
                        hraprl : hraprl,
                        hrratg : '',
                        peeraprl : peeraprl,
                        peerratg : '',
                        teamaprl : teamaprl,
                        teamratg : '',
		                    trng : aprObj.trng,
		                    recom : aprObj.recom,
		                    rvoptn : aprObj.rvoptn,
		                    rperiod : aprObj.rperiod,
		                    bvrt : aprObj.bvrt,
		                    bvtxt : aprObj.bvtxt,
		                    cvrt : aprObj.cvrt,
                      }, function(e3,o3)    {
            	          //callback();
                    });
                  }, function() {
                    callback();
                  });
                });
              //} else  {
                //console.log("appraisal date not yet reached for "+aprObj.aprtitle);
              }
            };
          });
        }, function() {
          //callback();
        });
      });
    }
  });
}

var upd_aprsl_bv = function(coObj)		{
	if (!coObj)
		return;

	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	aprbg01.find({}).toArray(function(e, aprlist)   {
    if (!e)   {
      async.forEach(aprlist, function(aprObj,  callback) {
        if (aprObj.bvrt == 'true')      {
          //var rvlen = aprObj.bvtxt.length;
          aprObj.bvtxt.forEach (function (bvtext)  {
        	  var aprdb  = dbConn.collection(coObj.companyid+'_'+aprObj.aprslid);
            //console.log(aprObj.userid + ' -- ' + bvtext);
            aprdb.insert({ 
              aprslid : aprObj.aprslid, 
              userid : aprObj.userid,
              username : aprObj.username,
              mgr:aprObj.mgr,
              loc:aprObj.loc,
              dept:aprObj.dept,
              doj:aprObj.doj,
              rtype:aprObj.rtype,
              emptype:aprObj.emptype,
              aprtitle : aprObj.aprtitle,
              atype : '3',
              atitle : bvtext,
              rperiod : '',
              rpwt : aprObj.rpwt,
              bvwt : aprObj.bvwt,
              cvwt : aprObj.cvwt,
              sdate : aprObj.sdate,
              edate : aprObj.edate,
              selfaprl : aprObj.selfaprl,
              selfratg : '',
              supraprl : aprObj.supraprl,
              suprratg : '',
              skipaprl : aprObj.skipaprl,
              skipratg : '',
              hraprl : aprObj.hraprl,
              hrratg : '',
              peeraprl : aprObj.peeraprl,
              peeruserid : '',
              peerratg : '',
              teamaprl : aprObj.teamaprl,
              teamuserid : '',
              teamratg : '',
              trng : aprObj.trng,
              recom : aprObj.recom,
            }, function(e1,o1)    {
              //callback();
            });
          });
        }
      }, function() {
        //callback();
      });
    }
  });
}

var upd_aprsl_skipmgr = function(coObj)		{
	if (!coObj)
		return;

	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	aprbg01.find({}).toArray(function(e, aprlist)   {
    if (!e)   {
      async.forEach(aprlist, function(aprObj,  callback) {
        users.getusersById(coObj.companyid, aprObj.mgr, function(e1, usrObj) {    //Get manager's data
          if (!e1)   {
            //console.log(usrObj[0].userid + " -- " + usrObj[0].mgr);
            aprbg01.findOne({_id: getObjectId(aprObj._id)}, function(e, o)   {    // get the record to update
	            if (o)   {
		            o.skipmgr = usrObj[0].mgr;
		            aprbg01.save(o, {safe: true}, function (e1,o1)      {
			            if (e1)
				            console.log('apr_bg01 not updated');
			            //else
				            //console.log('apr_bg01 updated');
		            });
	            }
            });
          }
        });
      }, function() {
        //callback();
      });
    }
  });
}

var upd_aprsl_cv = function(coObj)		{
	if (!coObj)
		return;

	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	aprbg01.find({}).toArray(function(e, aprlist)   {
    if (!e)   {
      async.forEach(aprlist, function(aprObj,  callback) {
        if (aprObj.cvrt == 'true')      {
	        cvalues.getallcvalues(coObj.companyid, '', function(e1, cvals) {
            if (!e1)   {
	            async.forEach(cvals, function(cvalObj, callback) {
      	        var aprdb  = dbConn.collection(coObj.companyid+'_'+aprObj.aprslid);
                //console.log(aprObj.userid + ' -- ' + cvalObj.vtitle);
                aprdb.insert({ 
                  aprslid : aprObj.aprslid, 
                  userid : aprObj.userid,
                  username : aprObj.username,
                  mgr:aprObj.mgr,
                  loc:aprObj.loc,
                  dept:aprObj.dept,
                  doj:aprObj.doj,
                  rtype:aprObj.rtype,
                  emptype:aprObj.emptype,
                  aprtitle : aprObj.aprtitle,
                  atype : '4',
                  atitle : cvalObj.vtitle,
                  rperiod : '',
                  rpwt : aprObj.rpwt,
                  bvwt : aprObj.bvwt,
                  cvwt : aprObj.cvwt,
                  sdate : aprObj.sdate,
                  edate : aprObj.edate,
                  selfaprl : aprObj.selfaprl,
                  selfratg : '',
                  supraprl : aprObj.supraprl,
                  suprratg : '',
                  skipaprl : aprObj.skipaprl,
                  skipratg : '',
                  hraprl : aprObj.hraprl,
                  hrratg : '',
                  peeraprl : aprObj.peeraprl,
                  peeruserid : '',
                  peerratg : '',
                  teamaprl : aprObj.teamaprl,
                  teamuserid : '',
                  teamratg : '',
                  trng : aprObj.trng,
                  recom : aprObj.recom,
                }, function(e1,o1)    {
                  //callback();
                });
              }, function() {
                callback();
              });
            }
          });
        }
      }, function() {
        //callback();
      });
    }
  });
}

var upd_aprsl_kra = function(coObj)		{
	if (!coObj)
		return;

	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	aprbg01.find({}).toArray(function(e, aprlist)   {
    if (!e)   {
      async.forEach(aprlist, function(aprObj,  callback) {
        if (aprObj.rvoptn == '1')      {
          //console.log(aprObj.role);
	        roles.getrolesbyid(coObj.companyid, aprObj.role, function(e1, roledata) {
            if (!e1)   {
              //console.log(roledata[0].kras);
        			kra.getkraarray(coObj.companyid, roledata[0].kras, function(e2, kralist) {
                if (!e2)   {
	                async.forEach(kralist, function(kraObj, callback) {
          	        var aprdb  = dbConn.collection(coObj.companyid+'_'+aprObj.aprslid);
                    //console.log(coObj.companyid + " -- " + aprObj.userid + " -- " + kraObj.kratitle);
                    aprdb.insert({ 
                      aprslid : aprObj.aprslid, 
                      userid : aprObj.userid,
                      username : aprObj.username,
                      mgr:aprObj.mgr,
                      loc:aprObj.loc,
                      dept:aprObj.dept,
                      doj:aprObj.doj,
                      rtype:aprObj.rtype,
                      emptype:aprObj.emptype,
                      aprtitle : aprObj.aprtitle,
                      atype : '2',
                      atitle : kraObj.kratitle,
                      rperiod : '',
                      rpwt : aprObj.rpwt,
                      bvwt : aprObj.bvwt,
                      cvwt : aprObj.cvwt,
                      sdate : aprObj.sdate,
                      edate : aprObj.edate,
                      selfaprl : aprObj.selfaprl,
                      selfratg : '',
                      supraprl : aprObj.supraprl,
                      suprratg : '',
                      skipaprl : aprObj.skipaprl,
                      skipratg : '',
                      hraprl : aprObj.hraprl,
                      hrratg : '',
                      peeraprl : aprObj.peeraprl,
                      peeruserid : '',
                      peerratg : '',
                      teamaprl : aprObj.teamaprl,
                      teamuserid : '',
                      teamratg : '',
                      trng : aprObj.trng,
                      recom : aprObj.recom,
                    }, function(e1,o1)    {
                      //callback();
                    });
                  }, function() {
                    callback();
                  });
                }
              });
            }
          });
        }
      }, function() {
        //callback();
      });
    }
  });
}

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
    return -1;
}

var upd_aprsl_rvp = function(coObj)		{
	if (!coObj)
		return;

	revperiod.getclosedrperiod(coObj.companyid, '', function(e, rpdata) {
		if (!e)		{
		  //console.log(coObj.companyid);
		  //console.log(rpdata);
	    var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	    aprbg01.find({}).toArray(function(e, aprlist)   {
        if (!e)   {
          async.forEach(aprlist, function(aprObj,  callback) {
            if (aprObj.rvoptn == '2')      {
              aprObj.rperiod.forEach (function (rpid)  {
          			var result = rpdata.getIndexBy("_id", rpid);
          			if (result != -1)   {
            			usergoals.getgoalsbyperiod_user({coid : coObj.companyid,userid : aprObj.userid, rperiod : rpid}, function(e,ugoals)	{
                    //console.log(aprObj.userid + " -- " + rpid + " -- " + ugoals.length);
                    if (!e)   {
                      async.forEach(ugoals, function(ugObj, callback) {
              	        var aprdb  = dbConn.collection(coObj.companyid+'_'+aprObj.aprslid);
                        //console.log(aprObj.userid + " -- " + rpid + " -- " + ugObj.ugoaltitle);
                        var frating = ugObj.selfrating;
                        if (rpdata[result].frating == '1')  {
                          frating = ugObj.selfrating;
                        } else if (rpdata[result].frating == '2')  {
                          frating = ugObj.mgrrating;
                        } else if (rpdata[result].frating == '3')  {
                          frating = ugObj.skiprating;
                        }
                        aprdb.insert({ 
                          aprslid : aprObj.aprslid, 
                          userid : aprObj.userid,
                          username : aprObj.username,
                          mgr:aprObj.mgr,
                          loc:aprObj.loc,
                          dept:aprObj.dept,
                          doj:aprObj.doj,
                          rtype:aprObj.rtype,
                          emptype:aprObj.emptype,
                          aprtitle : aprObj.aprtitle,
                          atype : '1',
                          atitle : ugObj.ugoaltitle,
                          rperiod : rpid,
                          rpwt : aprObj.rpwt,
                          bvwt : aprObj.bvwt,
                          cvwt : aprObj.cvwt,
                          sdate : aprObj.sdate,
                          edate : aprObj.edate,
                          selfaprl : aprObj.selfaprl,
                          selfratg : frating,
                          supraprl : aprObj.supraprl,
                          suprratg : frating,
                          skipaprl : aprObj.skipaprl,
                          skipratg : frating,
                          hraprl : aprObj.hraprl,
                          hrratg : '',
                          peeraprl : aprObj.peeraprl,
                          peeruserid : '',
                          peerratg : '',
                          teamaprl : aprObj.teamaprl,
                          teamuserid : '',
                          teamratg : '',
                          trng : aprObj.trng,
                          recom : aprObj.recom,
                        }, function(e1,o1)    {
                          //callback();
                        });
                      }, function() {
                        //callback();
                      });
                    }
                  });
                }
              });
            }
          }, function() {
            //callback();
          });
        }
      });
    }
  });
}

var upd_aprsl_sts = function(coObj)		{
	if (!coObj)
		return;

	// get Todaay's date
	var cdate = new Date();
	cdate.setHours(0, 0, 0, 0);

	apprsl.getnewapprsl(coObj.companyid, '', function(e1, aprlist) {
    async.forEach(aprlist, function(aprObj, callback) {
      var tmp = aprObj.sdate.split("-");
      var aprsdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);

			if (aprsdate.getTime() <= cdate.getTime())		{       // If Curr date is greater than or same as appraisal start date
      	apprsl.updaprslsts(coObj.companyid, aprObj._id, 2 , function(e, o) {
				  if (e1)
					  console.log(coObj.companyid + ' apprsl '+aprObj._id +' not updated');
					//else
					  //console.log(coObj.companyid + ' apprsl '+aprObj._id +' updated');
      	});
			}
    });
  });
}

var aprsl_notifs = function(coObj)		{
	if (!coObj)
		return;

	// get Todaay's date
	var cdate = new Date();
	cdate.setHours(0, 0, 0, 0);

  var notifs  = dbConn.collection(coObj.companyid+'_notifs');
	var aprbg01  = dbConn.collection(coObj.companyid+'_apr_bg01');
	aprbg01.find({}).toArray(function(e, aprlist)   {
    if (!e)   {
      async.forEach(aprlist, function(aprObj,  callback) {
				var dt = aprObj.sdate.toString();
        if (aprObj.selfaprl == true)  {
          var tstr = "here";
          var lstr = tstr.link("/462");
          var mesg = "A New performance appraisal starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Do the self rating "+lstr;
          notifs.insert({ userid : aprObj.userid, mailid : '', mtype:'1', date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	          //callback(e,o);
          });
        }
        if (aprObj.supraprl == true)  {
          var tstr = "rating";
          var lstr = tstr.link("/463");
          var mesg = "A New performance appraisal starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Do the "+ lstr +" for your team."
          notifs.insert({ userid : aprObj.mgr, mailid : '', mtype:'1', date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	          //callback(e,o);
          });
        }
        if (aprObj.skipaprl == true)  {
          var tstr = "rating";
          var lstr = tstr.link("/464");
          var mesg = "A New performance appraisal starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Do the "+ lstr +" for your skip team."
          notifs.insert({ userid : aprObj.skipmgr, mailid : '', mtype:'1', date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
	          //callback(e,o);
          });
        }
        if (aprObj.hraprl == true)  {
          var tstr = "here";
          var lstr = tstr.link("/272");
          var mesg = "A New performance appraisal starting " + dt.substr(0,dt.indexOf(' 00:00:00')) + " has been created. Do the HR rating "+ lstr;
          notifyadmins(coObj.companyid,1, mesg);
        }
      });
    }
  });
}

var searchusers = function(searchData, callback)	{
  var query = {};
  var qry1 = {};
  var qry2 = {};
  var qry3 = {};
  var qry4 = {};
  var qry5 = {};

  //console.log(searchData);

  query["$and"]=[];
	if (searchData.locid != '')   {
    var llen = searchData.locid.length;
    qry1["$or"]=[];
    var qarr = [];
    for (var x=0; x<llen; x++)  {
      qarr.push({ "location": searchData.locid[x] });
    }
    qry1 = { $or: qarr };
    query["$and"].push(qry1);
  }
	if (searchData.deptid != '')   {
    var dlen = searchData.deptid.length;
    qry2["$or"]=[];
    var qarr = [];
    for (var x=0; x<dlen; x++)  {
      qarr.push({ "dept": searchData.deptid[x] });
    }
    qry2 = { $or: qarr };
    query["$and"].push(qry2);
  }
	if (searchData.rtype != '')   {
    var rlen = searchData.rtype.length;
    qry3["$or"]=[];
    var qarr = [];
    for (var x=0; x<rlen; x++)  {
      qarr.push({ "rtype": parseInt(searchData.rtype[x]) });
    }
    qry3 = { $or: qarr };
    query["$and"].push(qry3);
  }
	if (searchData.emptype != '')   {
    var elen = searchData.emptype.length;
    qry4["$or"]=[];
    var qarr = [];
    for (var x=0; x<elen; x++)  {
      qarr.push({ "emptype": searchData.emptype[x] });
    }
    qry4 = { $or: qarr };
    query["$and"].push(qry4);
  }
	if (searchData.jdate != '')   {
      var tmp = searchData.jdate.split("-");
      var sdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
      query["$and"].push({'doj':{ $lte: sdate}});
    }

	//console.log(JSON.stringify(query));

	var users  = dbConn.collection(searchData.coid+'_users');
	users.find(query).toArray(function(e, usrlist)   {
		if (e)
		  callback(true,{err:q,text:'No users found for the criteria'});
		else    {
			callback(false,usrlist);
		}
	});
};

// -------------------------------------------
// Utility functions
// -------------------------------------------
var notifyadmins = function(coid, mtype, mesg)		{
	var notifs  = dbConn.collection(coid+'_notifs');

  var cdate = new Date();
  //cdate.setHours(0,0,0,0);

	getalladmins(coid, function(e1,users)		{
		var utot = users.length;
		async.forEach(users, function(usrObj, callback) {
			notifs.insert({ userid:usrObj.userid, mailid : usrObj.useremailid, mtype:mtype, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
				callback(e,o);
			});
		}, function() {
			return;
		});
	});
}

var notifytopm = function(coid, mtype, mesg)		{
	var notifs  = dbConn.collection(coid+'_notifs');

  var cdate = new Date();
  //cdate.setHours(0,0,0,0);

	getalltopm(coid, function(e1,users)		{
		var utot = users.length;
		async.forEach(users, function(usrObj, callback) {
			notifs.insert({ userid:usrObj.userid, mailid : usrObj.useremailid, mtype:mtype, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
				callback(e,o);
			});
		}, function() {
			return;
		});
	});
}

var notifymgrs = function(coid, mtype, mesg)		{
	var notifs  = dbConn.collection(coid+'_notifs');

  var cdate = new Date();
  //cdate.setHours(0,0,0,0);

	getallmgrs(coid, function(e1,users)		{
		var utot = users.length;
		async.forEach(users, function(usrObj, callback) {
			notifs.insert({ userid:usrObj.userid, mailid : usrObj.useremailid, mtype:mtype, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
				callback(e,o);
			});
		}, function() {
			return;
		});
	});
}

var notifyall = function(coid, mtype, mesg)		{
	var notifs  = dbConn.collection(coid+'_notifs');

  var cdate = new Date();
  //cdate.setHours(0,0,0,0);

	getall(coid, function(e1,users)		{
		var utot = users.length;
		async.forEach(users, function(usrObj, callback) {
			notifs.insert({ userid:usrObj.userid, mailid : usrObj.useremailid, mtype:mtype, date:cdate, mesg : mesg, rflag:false, sflag:false}, function(e,o)    {
				callback(e,o);
			});
		}, function() {
			return;
		});
	});
}

var verifyusernotifs = function(coid, userid, callback)   {
	var unotifs = dbConn.collection(coid +'_notifs');
  var cdate = new Date();
  //cdate.setHours(0,0,0,0);
  cdate.setDate(cdate.getDate() - 1);
  //console.log(userid);
  //console.log(typeof(userid));
  //console.log(cdate);

	unotifs.find({userid:userid, date : {"$gte": cdate}}).count(function(e, ncounts)   {
		if (ncounts)    {
			callback(false,ncounts);
		} else  {
  		callback(true,{err:1,text:'No notifs for user'});
		}
	});
}

Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () { 
    return Date.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

Date.prototype.addDays = function (value) {
    var n = this.getDate();
    this.setDate(n + value);
    return this;
};

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

var updrperiod = function(coid,sdate,edate, callback)   {
	var company = dbConn.collection('company');
	company.findOne({companyid:coid}, function(e, o)   {
		if (o)    {
			o.crsdate = edate.getDate() + '-' +  (edate.getMonth() + 1) + '-' + edate.getFullYear();
			o.credate = sdate.getDate() + '-' +  (sdate.getMonth() + 1) + '-' + sdate.getFullYear();
			company.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'review cycle not updated'});
				else
					callback(false,{err:0,text:'review cycle updated'});
			});
		} else  {
  		callback(true,{err:2,text:'company not found'});
		}
	});
}

getall = function(coid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};
  var qry2 = {};

  query["$or"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "2"}]};
  query["$or"].push(qry1);

  qry2["$or"]=[];
  qry2 = { $or: [{"usertype": "4"}]};
  query["$or"].push(qry2);

	users.find(query).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

getusersofmgr = function(coid, mgrid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	users.find({"usertype": "4", "mgr":mgrid.toString()}).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

getallusers = function(coid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "4"}]};
  query["$and"].push(qry1);

	users.find(query).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

getallmgrs = function(coid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	users.find({usertype:'4',rtype:{'$lt':4}}).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

getalltopm = function(coid, callback)   {
	var users  = dbConn.collection(coid+'_users');
	users.find({usertype:'4',rtype: 1}).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

getalladmins = function(coid, callback)   {
	var users  = dbConn.collection(coid+'_users');

  var query = {};
  var qry1 = {};

  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [{"usertype": "2"}]};
  query["$and"].push(qry1);

	users.find(query).sort({userid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'users not found'});
		else    {
			callback(false,o);
		}
	});
}

var emailadmins = function(coid, mesg)		{
	// get all hradmins of the company
	getalladmins(coid, function(e1,users)		{
		var utot = users.length;
		async.forEach(users, function(usrObj, callback) {
			EM.notifs({
				companyid : coid,
				mailid    : usrObj.useremailid,
				mesg      : mesg,
			}, function(e,o)    {
				callback(e,o);
			});
		}, function() {
			return;
		});
	});
}

// ------------------------------------------------------------------------------
// One time Job - delete later
// Bring usergoals table uptodate
// tsubmitted becomes trgt_submited
// submitted becomes self_submited
// msubmitted becomes supr_submited
// ssubmitted becomes skip_submited
// ------------------------------------------------------------------------------
/*
exports.Proc0a = function(userid, callback)   {
  var colist = [
      "10001_blogs",
      "10001_committees",
      "10001_cvalues",
      "10001_department",
      "10001_empleaves",
      "10001_exitacts",
      "10001_holidaylist",
      "10001_iquestions",
      "10001_kra",
      "10001_leavetypes",
      "10001_locations",
      "10001_onbdacts",
      "10001_orgstru",
      "10001_perfgoals",
      "10001_revperiod",
      "10001_rlevel",
      "10001_roles",
      "10001_skills",
      "10001_sklevel",
      "10001_usergoals",
      "10001_userids",
      "10001_users",
      "10001_userskills",
      "10002_department",
      "10002_kra",
      "10002_revperiod",
      "10002_rlevel",
      "10002_roles",
      "10002_skills",
      "10002_userids",
      "10002_users",
      "10003_cvalues",
      "10003_department",
      "10003_kra",
      "10003_locations",
      "10003_roles",
      "10003_skills",
      "10003_userids",
      "10003_users",
      "10007_cvalues",
      "10007_department",
      "10007_kra",
      "10007_locations",
      "10007_roles",
      "10007_skills",
      "10007_userids",
      "10007_users",
      "10009_department",
      "10009_kra",
      "10009_skills",
      "10009_userids",
      "10009_users",
      "10010_department",
      "10010_kra",
      "10010_locations",
      "10010_perfgoals",
      "10010_revperiod",
      "10010_rlevel",
      "10010_roles",
      "10010_skills",
      "10010_userids",
      "10010_users",
      "10011_cvalues",
      "10011_department",
      "10011_kra",
      "10011_locations",
      "10011_rlevel",
      "10011_roles",
      "10011_skills",
      "10011_sklevel",
      "10011_userids",
      "10011_users",
      "10012_department",
      "10012_kra",
      "10012_locations",
      "10012_skills",
      "10012_userids",
      "10012_users",
      "10013_department",
      "10013_kra",
      "10013_skills",
      "10013_userids",
      "10013_users",
  ];
  async.forEach(colist, function(colname, callback) {
    //console.log(colname);
  	var deldb  = dbConn.collection(colname);
    deldb.drop(function(e, o)   {
	    if (e) 
		    console.log(colname+' not deleted');
		  else
		    console.log(colname+' deleted');
	  });
    callback();
  });
}

exports.Proc0b = function(userid, callback)   {
  var colist = [
      "10001",
      "10002",
      "10003",
      "10007",
      "10009",
      "10010",
      "10011",
      "10012",
      "10013",
  ];
  async.forEach(colist, function(coid, callback) {
    //console.log(coid);
  	var deldb  = dbConn.collection('company');
  	deldb.remove({companyid: parseInt(coid)}, function(e, o)   {
	    if (e) 
		    console.log(coid+' not deleted');
		  else
		    console.log(coid+' deleted');
	  });
    callback();
  });
}
*/
/*
exports.Proc0a = function(userid, callback)   {
  var bg01  = dbConn.collection('bgtable01');
  bg01.find({}).sort({companyid:1}).toArray(function(e, colist)   {
    if (e)
	    callback(true,{err:1,text:'company not found'});
    else    {
  	  async.forEachSeries(colist, function(coObj, callback) {
		    reset_userdoj(coObj);
		    callback();
	    });
	    callback(false,{err:0,text:'companies processed'});
    }
  });
}

var reset_userdoj = function(coObj)		{
	if (!coObj)
		return;
	var users = dbConn.collection(coObj.companyid +'_users');
  var usrdata = [];

  users.find({usertype:'4'}).toArray(function(e, usrlist)   {
    if (e)
	    callback(true,{err:1,text:'users not found'});
    else    {
  	  async.forEachSeries(usrlist, function(usrObj, callback) {
  	    var newObj = new Object;
  	    newObj.coid = coObj.companyid;
  	    newObj.id = usrObj._id;
  	    newObj.userid = usrObj.userid;
  	    //newObj.doj = usrObj.doj;
  	    //newObj.dob = usrObj.dob;
  	    //newObj.probdate = usrObj.probdate;
  	    //newObj.ppedate = usrObj.ppedate;
  	    //newObj.ppidate = usrObj.ppidate;

        if (typeof(usrObj.doj) == "string")    {
          var tmp = usrObj.doj.split("-");
          newObj.doj = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  	    }
        if (typeof(usrObj.dob) == "string")    {
          var tmp = usrObj.dob.split("-");
          newObj.dob = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  	    }
        if (typeof(usrObj.probdate) == "string")    {
          var tmp = usrObj.probdate.split("-");
          newObj.probdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  	    }
        if (typeof(usrObj.ppedate) == "string")    {
          var tmp = usrObj.ppedate.split("-");
          newObj.ppedate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  	    }
        if (typeof(usrObj.ppidate) == "string")    {
          var tmp = usrObj.ppidate.split("-");
          newObj.ppidate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  	    }
  	    usrdata.push(newObj);
		    callback();
	    });
    }
    async.forEachSeries(usrdata, function(usrObj, callback) {
      users.update({_id: getObjectId(usrObj.id)}, {$set:{"doj":usrObj.doj,"dob":usrObj.dob,"probdate":usrObj.probdate,"ppedate":usrObj.ppedate,"ppidate":usrObj.ppidate}}, { upsert:false, multi:false }, function(e, o)   {
        if (e)
          console.log(coObj.companyid + " " + usrObj.userid + "--> not updated");
        else 
          console.log(coObj.companyid + " " + usrObj.userid + "--> updated");
      });
	    callback();
    });
  });
}

var reset_rperiods = function(coObj)		{
	if (!coObj)
		return;
	var rperiods = dbConn.collection(coObj.companyid +'_revperiod');
  var rpdata = [];

  rperiods.find({}).toArray(function(e, rplist)   {
    if (e)
	    callback(true,{err:1,text:'rperiod not found'});
    else    {
  	  async.forEachSeries(rplist, function(rpObj, callback) {
  	    var newObj = new Object;
  	    newObj.coid = coObj.companyid;
  	    newObj.id = rpObj._id;
  	    newObj.sdate = rpObj.sdate;
  	    newObj.edate = rpObj.edate;

        //console.log(typeof(rpObj.sdate));
        //console.log(typeof(rpObj.edate));

        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        if (typeof(rpObj.sdate) == "string")    {
          var dd = rpObj.sdate.substr(0,2);
          var mm = rpObj.sdate.substr(3,2);
          var mname = monthNames[mm];
          var yyyy = rpObj.sdate.substr(6,4);
          var sdate = dd+'-'+mname+'-'+yyyy;
          
          var dd = rpObj.edate.substr(0,2);
          var mm = rpObj.edate.substr(3,2);
          var mname = monthNames[mm];
          var yyyy = rpObj.edate.substr(6,4);
          var edate = dd+'-'+mname+'-'+yyyy;

          var tmp = rpObj.sdate.split("-");
          newObj.sdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);

          var tmp = rpObj.edate.split("-");
          newObj.edate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
        } else  {
          var dd = rpObj.sdate.getDate();
          var mm = rpObj.sdate.getMonth()+1; //January is 0!
          var mname = monthNames[mm-1];
          var yyyy = rpObj.sdate.getFullYear();
          if (dd < 10){
            dd = '0'+ dd;
          } 
          if(mm < 10){
              mm = '0'+mm;
          } 
          var sdate = dd+'-'+mname+'-'+yyyy;

          dd = rpObj.edate.getDate();
          mm = rpObj.edate.getMonth()+1; //January is 0!
          var mname = monthNames[mm-1];
          yyyy = rpObj.edate.getFullYear();
          if(dd < 10){
            dd = '0'+ dd;
          } 
          if(mm < 10){
              mm = '0'+mm;
          } 
          var edate = dd+'-'+mname+'-'+yyyy;
  	    }
  	    newObj.rptitle = "Review Period from " + sdate + " to " + edate;
  	    rpdata.push(newObj);
  	    //console.log(rpdata.length);
		    callback();
	    });
    }
    async.forEachSeries(rpdata, function(rpObj, callback) {
      rperiods.update({_id: getObjectId(rpObj.id)}, {$set:{"rptitle":rpObj.rptitle,"sdate":rpObj.sdate,"edate":rpObj.edate}}, { upsert:false, multi:false }, function(e, o)   {
        if (e)
          console.log(coObj.companyid + " " + rpObj.id + "--> not updated");
        else 
          console.log(coObj.companyid + " " + rpObj.id + "--> updated");
      });
      //console.log(rpObj);
      //console.log('---------------------------------------');
	    callback();
    });
  });
}

var reset_goals = function(coObj)		{
	if (!coObj)
		return;
	var ugoals = dbConn.collection(coObj.companyid +'_usergoals');
  ugoals.update({}, {$rename:{"tsubmitted":"trgt_submited","submitted":"self_submited","msubmitted":"supr_submited","ssubmitted":"skip_submited"}}, { upsert:false, multi:true }, function(e, o)   {
    if (e)
      console.log(coObj.companyid + "--> not updated");
    else 
      console.log(coObj.companyid + "--> updated");
  });
}
*/
// ------------------------------------------------------------------------------

