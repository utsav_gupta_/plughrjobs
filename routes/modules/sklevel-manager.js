if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPsklevel = function(coid, userid, callback)   {
	var sklevel  = dbConn.collection(coid+'_sklevel');

	sklevel.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			sklevel.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'Skill level not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading Skill level'});
	});
};
exports.getNPsklevel = function(coid, userid, pageno, callback)   {
	var sklevel  = dbConn.collection(coid+'_sklevel');

	sklevel.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Skill level not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallsklevel = function(coid, userid, callback)   {
	var sklevel  = dbConn.collection(coid+'_sklevel');

	sklevel.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Skill level not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getcount = function(coid, userid, callback)   {
	var sklevel  = dbConn.collection(coid+'_sklevel');

	sklevel.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Skills ratings not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.addnewsklevel = function(newData, callback)   {
	var sklevel  = dbConn.collection(newData.coid+'_sklevel');

	newData.date = new Date();
	newData.skdesc = newData.skdesc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	sklevel.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'sklevel not added'});
		else   {
			getFPsklevel(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Skill level data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatesklevel = function(newData, callback)   {
	var sklevel  = dbConn.collection(newData.coid+'_sklevel');

	sklevel.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.sklevel = newData.sklevel;
			o.skdesc = newData.skdesc;
			delete o.coid;
			sklevel.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'sklevel not updated'});
				else   {
					getFPsklevel(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Skill level data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in sklevel'});
		}
	});
}
exports.deletesklevel = function(newData, callback)   {
	var sklevel  = dbConn.collection(newData.coid+'_sklevel');

	sklevel.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'sklevel data not deleted'});
		else   {
			getFPsklevel(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Skill level data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPsklevel = function(coid, userid, callback)   {
	var sklevel  = dbConn.collection(coid+'_sklevel');

	sklevel.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			sklevel.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'Skill level not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading Skill level'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

