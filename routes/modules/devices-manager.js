var crypto 		= require('crypto')

if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPdevices = function(coid, userid, callback)   {
	var devices  = dbConn.collection(coid+'_devices');

	devices.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			devices.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'devices not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading devices'});
	});
};
exports.getNPdevices = function(coid, userid, pageno, callback)   {
	var devices  = dbConn.collection(coid+'_devices');

	devices.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'devices not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalldevices = function(coid, userid, callback)   {
	var devices  = dbConn.collection(coid+'_devices');

	devices.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'devices not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.regdevice = function(coid, scode, callback)   {
	var devices  = dbConn.collection(coid+'_devices');

	devices.find({secode:scode}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'devices not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewdevices = function(newData, callback)   {
	var devices  = dbConn.collection(newData.coid+'_devices');

	newData.date = new Date();
	newData.devname = newData.devname;
	newData.secode = newData.secode;
	newData.password = newData.password;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

  saltAndHash(newData.password, function(hash)		{
    newData.password = hash;
	  devices.insert(newData, {safe: true}, function (e1,o1)      {
		  if (e1)
			  callback(true,{err:1,text:'devices not added'});
		  else   {
			  getFPdevices(scoid,suserid, function(e2,o2, totpages)     {
			  if (e2)
				  callback(true,{err:2,text:'Table devices data added'});
			  else
				  callback(false,o2, totpages);
			  });
		  }
	  });
  });
}
exports.updatedevices = function(newData, callback)   {
	var devices  = dbConn.collection(newData.coid+'_devices');

	devices.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
      saltAndHash(newData.password, function(hash)		{
        o.password = hash;
			  o.devname = newData.devname;
			  o.secode = newData.secode;
			  delete o.coid;
			  devices.save(o, {safe: true}, function (e1,o1)      {
				  if (e1)
					  callback(true,{err:1,text:'devices not updated'});
				  else   {
					  getFPdevices(newData.coid,newData.userid, function(e2,o2, totpages)     {
						  if (e2)
							  callback(true,{err:2,text:'Table devices data updated'});
						  else
							  callback(false,o2, totpages);
					  });
				  }
			  });
		  });
		} else  {
			callback(true,{err:2,text:' ID not found in devices'});
		}
	});
}
exports.deletedevices = function(newData, callback)   {
	var devices  = dbConn.collection(newData.coid+'_devices');

	devices.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'devices data not deleted'});
		else   {
			getFPdevices(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table devices data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPdevices = function(coid, userid, callback)   {
	var devices  = dbConn.collection(coid+'_devices');

	devices.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			devices.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'devices not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading devices'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}
/* private encryption & validation methods */
var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

