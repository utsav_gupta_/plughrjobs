if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 5;

exports.getFProles = function(coid, userid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			roles.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'roles not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading roles'});
	});
};
exports.getNProles = function(coid, userid, pageno, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'roles not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallroles = function(coid, userid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'roles not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getskillsofrole = function(coid, roleid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({_id:getObjectId(roleid)},{_id:0,mustskills:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'roles not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getcount = function(coid, userid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Roles not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.getrolesbydept = function(coid, userid,dept, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({depid:dept}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'roles not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getrolesbyid = function(coid, roleid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({_id: getObjectId(roleid)}).toArray(function(e, o)   {
		if (!o)
			callback(true,{err:1,text:'roles not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewroles = function(newData, callback)   {
	var roles  = dbConn.collection(newData.coid+'_roles');

	newData.date = new Date();
	newData.roletype = newData.roletype;
	newData.depid = newData.depid;
	newData.mustskills = newData.mustskills;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;
	var roleid = '';

	roles.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'roles not added'});
		else   {
			roleid = o1[0]._id;
			//console.log("new roleid = " + roleid);
			getFProles(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table roles data added'});
			else
				callback(false,o2, totpages, roleid);
			});
		}
	});
}
exports.updateroles = function(newData, callback)   {
	var roles  = dbConn.collection(newData.coid+'_roles');

	roles.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.roletitle = newData.roletitle;
			o.roletype = newData.roletype;
			o.depid = newData.depid;
			o.mustskills = newData.mustskills;
			o.kras = newData.kras;
			delete o.coid;
			roles.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'roles not updated'});
				else   {
					getFProles(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table roles data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in roles'});
		}
	});
}
exports.deleteroles = function(newData, callback)   {
	var roles  = dbConn.collection(newData.coid+'_roles');

	roles.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'roles data not deleted'});
		else   {
			getFProles(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table roles data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFProles = function(coid, userid, callback)   {
	var roles  = dbConn.collection(coid+'_roles');

	roles.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			roles.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'roles not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading roles'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

