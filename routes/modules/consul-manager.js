if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPconsul = function(coid, userid, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			consul.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'consul not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading consul'});
	});
};
exports.getNPconsul = function(coid, userid, pageno, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'consul not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallconsul = function(coid, userid, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'consul not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewconsul = function(newData, callback)   {
	var consul  = dbConn.collection('consultants');

	newData.date = new Date();
	saltAndHash(newData.pass, function(hash)    {
		newData.password = hash;
	});
	newData.cname = newData.cname;
	newData.compname = newData.compname;
	newData.addr1 = newData.addr1;
	newData.addr2 = newData.addr2;
	newData.city = newData.city;
	newData.pincode = newData.pincode;
	newData.gender = newData.gender;
	newData.ccno = newData.ccno;
	newData.ccdate = newData.ccdate;
	newData.ccname = newData.ccname;
	newData.actvcode = randomString(10, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	consul.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'consul not added'});
		else   {
			getFPconsul(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table consul data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateconsul = function(newData, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.emailid = newData.emailid;
			saltAndHash(newData.pass, function(hash)    {
				o.password = hash;
			});
			o.cname = newData.cname;
			o.compname = newData.compname;
			o.addr1 = newData.addr1;
			o.addr2 = newData.addr2;
			o.city = newData.city;
			o.pincode = newData.pincode;
			o.gender = newData.gender;
			o.sdate = newData.sdate;
			o.ccno = newData.ccno;
			o.ccdate = newData.ccdate;
			o.ccname = newData.ccname;
			delete o.coid;
			consul.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'consul not updated'});
				else   {
					getFPconsul(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table consul data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in consul'});
		}
	});
}
exports.deleteconsul = function(newData, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'consul data not deleted'});
		else   {
			getFPconsul(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table consul data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPconsul = function(coid, userid, callback)   {
	var consul  = dbConn.collection('consultants');

	consul.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			consul.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'consul not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading consul'});
	});
}
var crypto 		= require('crypto')

var generateSalt = function()  {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}
var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}
var saltAndHash = function(pass, callback)  {
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}
var randomString = function (length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

