if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPworkexp = function(coid, userid, callback)   {
	var workexp  = dbConn.collection(coid+'_users');

	workexp.find({userid:userid}, {expdata:1}).toArray(function(e1, o1)   {
		if (!e1)      {
			if (o1[0].expdata)		{
				var tot = o1[0].expdata.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			workexp.find({userid:userid}, {expdata:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {expdata:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'workexp not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading workexp'});
	});
};
exports.getNPworkexp = function(coid, userid, pageno, callback)   {
	var workexp  = dbConn.collection(coid+'_users');

	workexp.find({userid:userid}, {expdata:{ $slice: [(pageno-1)*NUMBER_OF_ITEMS_PER_PAGE, NUMBER_OF_ITEMS_PER_PAGE]}}, {expdata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'workexp not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallworkexp = function(coid, userid, callback)   {
	var workexp  = dbConn.collection(coid+'_users');

	workexp.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'workexp not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewworkexp = function(newData, callback)   {
	var workexp  = dbConn.collection(newData.coid+'_users');

	workexp.find({userid:newData.userid}, {expdata:1}).toArray(function(e, o)   {
		if (o)			{
		  var edObj = new Object;
			edObj.orgn = newData.orgn;
			edObj.title = newData.title;
			edObj.sdate = newData.sdate;
			edObj.edate = newData.edate;
		  edObj.date = new Date();

			workexp.update({userid:newData.userid}, { $push : {"expdata":edObj}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'workexp not added'});
				else   {
					getFPworkexp(newData.coid,newData.userid, function(e2,o2, totpages)     {
					if (e2)
						callback(true,{err:2,text:'Table workexp data added'});
					else
						callback(false,o2, totpages);
					});
				}
			});
		}
	});
}
exports.updateworkexp = function(newData, callback)   {
	var workexp  = dbConn.collection(newData.coid+'_users');

	workexp.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			workexp.update({_id:getObjectId(newData.dbid), "expdata.orgn":newData.old_orgn, "expdata.title":newData.old_title, "expdata.sdate":newData.old_sdate, "expdata.edate":newData.old_edate}, { $set : {"expdata.$.orgn":newData.orgn, "expdata.$.title":newData.title, "expdata.$.sdate":newData.sdate, "expdata.$.edate":newData.edate }}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'workexp not updated'});
				else   {
					getFPworkexp(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table workexp data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in workexp'});
		}
	});
}
exports.deleteworkexp = function(newData, callback)   {
	var workexp  = dbConn.collection(newData.coid+'_users');

	workexp.findOne({_id:getObjectId(newData.dbid)}, {expdata:1}, function(e, o)   {
		//console.log(o);
		if (o)   {
			workexp.update({_id:getObjectId(newData.dbid)}, { $pull : {"expdata": {orgn : newData.orgn, title : newData.title, sdate : newData.sdate, edate : newData.edate }}}, {safe: true}, function (e1,o1)      {
				if (e1) 
					callback(true,{err:1,text:'workexp data not deleted'});
				else   {
					getFPworkexp(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table workexp data deleted'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in users'});
		}
	});
}
var getFPworkexp = function(coid, userid, callback)   {
	var workexp  = dbConn.collection(coid+'_users');


	workexp.find({userid:userid}, {expdata:1}).toArray(function(e1, o1)   {
		if (!e1)      {
			if (o1[0].expdata)		{
				var tot = o1[0].expdata.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			workexp.find({userid:userid}, {expdata:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {expdata:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'workexp not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading workexp'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

