var preCompany = dbConn.collection('preCompany');
var company = dbConn.collection('company');
var users = dbConn.collection('users');

exports.autoLogin = function(user, pass, callback)  {
	company.findOne({user:user}, function(e, o) {
		if (o){
			o.pass == pass ? callback(o) : callback(null);
		}	else{
			callback(null);
		}
	});
}

exports.manualLogin = function(user, pass, callback)  {
	users.findOne({user:user}, function(e, o) {
		if (o == null)	{
			callback(true,{err:1,text:'User not found'});
			//callback('User not found');
		}	else{
			validatePassword(pass, o.password, function(err, res) {
				if (!err){
    			callback(false,o);
					//callback(null, o);
				}	else{
    			callback(true,{err:2,text:'Incorrect Password'});
					//callback('Incorrect Password');
				}
			});
		}
	});
}

exports.updatePassword = function(user,oldpass, newpass, callback)    {
   users.findOne({user:user}, function(e, o)    {
	   if (e)
		  callback(true,{err:1,text:'Unknown User'});
		else	{
      console.log(user);
      console.log(oldpass);
			validatePassword(oldpass, o.password, function(err, res) {
            if (err)
       		   callback(true,{err:2,text:'Incorrect Current Password'});
			   else    {
				   saltAndHash(newpass, function(hash)		{
				     o.password = hash;
					   users.save(o, {safe: true}, function (e1,o1)   {
                     if (e1)
		                  callback(true,{err:3,text:'Password not updated'});
                     else
		                  callback(false,{err:0,text:'Password updated'});
                  });
				   });
			   }
			});
		}
	});
}

function validatePassword(plainPass, hashedPass, callback)    {
  //console.log(plainPass);
  //console.log(hashedPass);
  //console.log("------------");
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
  //console.log(validHash);
  //console.log("------------");
	if (hashedPass === validHash)   {
    //console.log("same");
    callback(false, 1);
  } else  {
    //console.log("Not  same");
    callback(true, 1);
	  //callback(null, hashedPass === validHash);
  }
}

exports.resetpwd = function(semail, callback)     {
	users.findOne({user:semail}, function(e, o) {
		if (o)      {
      //console.log("known user");
	    var spwd = Math.random().toString(36).slice(-8);
			saltAndHash(spwd, function(hash)    {
		    o.password = hash;
		    users.save(o, {safe: true}, function(e1, o1) {
	        if (e1)	{
            console.log("Unable to reset password");
      			callback(true,{err:1,text:'Unable to reset password'});
          } else  {
            //console.log("Password reset in db");
            EM.resetpwd(o,spwd, function (err, op)   {
              if (op)     {
                //console.log("new password emailed");
          			callback(false,{err:0,text:'Please check your email for new password'});
              } else  {
                //console.log("email not sent");
          			callback(true,{err:2,text:'Unable to reset password'});
              }
            });
          }
        });
      });
		} else    {
      //console.log("Unknown user");
		  callback(true,{err:1,text:'Unknown user'});
    }
	});
}

/* private encryption & validation methods */
var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

exports.getProfileByID = function(hradminid, callback) {
	hradmin.findOne({user:hradminid}, function(e, o) {
		if (o == null)	{
			callback(true,{err:1,text:'User not found'});
			//callback('User not found');
		}	else{
  		callback(false,o);
		}
	});
}

exports.updateprofile = function(newData, callback)			{
	users.findOne({user:newData.user}, function(e, o)	{
    if (o)    {
		  o.username = newData.username;
		  o.mobile = newData.mobile;

		  users.save(o, {safe: true}, function(e, o1) {
        if (e)  {
          //console.log('profile not updated');
		      callback(true,{err:1,text:'Profile not updated'});
        } else  {
          //console.log('profile updated');
          callback(false,o);
        }
      });
    } else  {
      callback(true,{err:2,text:'Unknown user'});
    }
	});
}

