if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getallskills = function(coid, userid, callback)   {
	var skills  = dbConn.collection('phr_skills');

	skills.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'PlugHR skills repository is empty!'});
		else    {
			callback(false,o);
		}
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

