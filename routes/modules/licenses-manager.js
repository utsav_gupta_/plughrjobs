var crypto 		= require('crypto')
var moment 		= require('moment');

var licenses = dbConn.collection('licenses');

if (process.env.OPENSHIFT_MONGODB_DB_URL)
  var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
  var NUMBER_OF_ITEMS_PER_PAGE = 3;

var PAGE_NUMBER = 0;

/*
// OLD CODE - COPIED ON 1/APR/2016 - DELETE LATER
exports.addNewLicense = function(licData, callback)		{
	licData.coid = parseInt(licData.coid);
	licData.date = new Date();
  licData.active = true;

	licenses.findOne({coid: licData.coid, active:true }, function(e, o) {
    if (o)	{
			o.active = false;
	    licenses.save(o, {safe: true}, function(e1,o1)        {
        if (!e1)	{
					licenses.insert(licData, {safe: true}, function(e,o)    {
				    if (e)
					    callback(true,{err:2,text:'License not added'});
				    else
					    callback(false,{err:0,text:'License added for Company'});
				  });
				}
			});
    } else    {
	    licenses.insert(licData, {safe: true}, function(e,o)    {
        if (e)
	        callback(true,{err:2,text:'License not added'});
        else
	        callback(false,{err:0,text:'License created for Company'});
      });
    }
  });
}
*/

exports.addNewLicense = function(licData, callback)		{
	licData.coid = parseInt(licData.coid);
	licData.date = new Date();
  licData.active = true;

  licenses.update({coid: licData.coid, active:true }, {$set:{"active":false}}, { upsert:false, multi:true }, function(e, o)   {
    licenses.insert(licData, {safe: true}, function(e,o)    {
      if (e)
        callback(true,{err:2,text:'License not added'});
      else
        callback(false,{err:0,text:'License created for Company'});
    });
  });
}

exports.addTrialLicense = function(coData, callback)
{
	var d = new Date();
	var curr_date = d.getDate();
	var cur1_date = curr_date-1;
	var curr_month = d.getMonth() + 1; //Months are zero based
	var curr_year = d.getFullYear();
	var cur1_year = d.getFullYear() + 1;

  var licData = new Object;
  licData.coid = coData.companyid;
  licData.coname = coData.companyname;
	licData.startdate = curr_month + "/" + curr_date + "/" + curr_year;
	licData.enddate = curr_month + "/" + cur1_date + "/" + cur1_year;

  //licData.startdate = new Date(month, day, year);
  //licData.enddate = new Date(month, day, year);
  //licData.enddate.setDate(licData.startdate.getDate() + 365);

  licData.users = 10;
  licData.notes = "Perpetual License";
	licData.date = new Date();
  licData.active = true;

	//console.log(licData);

	licenses.findOne({coid: parseInt(licData.coid) }, function(e, o) {
    if (o)
	    callback(true,{err:1,text:'License already exists'});
    else    {
	    licenses.insert(licData, {safe: true}, function(e,o)    {
        if (e)
	        callback(true,{err:2,text:'License not added'});
        else
	        callback(false,{err:0,text:'License created for uni'});
      });
    }
  });
}

exports.setinactive = function(id, callback)	{
	licenses.findOne({ _id: getObjectId(id) }, function(e, o) {
		if (e) 
			callback(e)
		else  {
      o.active = false;
	    licenses.save(o, {safe: true}, callback);
    }
	});
};

var getObjectId = function(id)
{
	return licenses.dbConn.bson_serializer.ObjectID.createFromHexString(id)
}

exports.deletelicense = function(licData, callback)   {
	licenses.remove({_id: getObjectId(licData.dbid)}, function(e1,o1)        {
    if (e1)
      callback(true,{err:1,text:'License not Deleted'});
    else	{
    	getalllicense (licData.coid, function(e,o)		{
    		if (o)
    			callback(false, o);
    		else
		      callback(false,{err:0,text:'License Deleted'});
		  });
		}
  });
}

exports.updatelicense = function(licdata, callback)	{
	licenses.findOne({ coid: parseInt(licdata.coid), active:true }, function(e, o) {
		if (o) {
      o.startdate = licdata.startdate;
      o.enddate = licdata.enddate;
      o.users = licdata.users;
      o.notes = licdata.notes;
      o.active = true;
    	o.date = new Date();
	    licenses.save(o, {safe: true}, function(e1,o1)        {
        if (e1)
	        callback(true,{err:1,text:'License not Updated'});
        else
	        callback(false,{err:0,text:'License Updated'});
      });
    } else
      callback(true,{err:2,text:'License not found'});
	});
};

exports.getalllicense = function(coid, callback)	{
	var query = { coid: parseInt(coid) };
	licenses.find(query).toArray(function(e2, res) {
    if (e2)
		  callback(true,{err:1,text:'License not found'});
    else
		  callback(false,res);
	});
};

exports.verifylicense = function(coid, callback)	{
  var lfound = false;
  licenses.find({"coid":parseInt(coid), active:true}).toArray(function(e,liclist)    {
  	//console.log(liclist);
    if (!liclist)  {
      //console.log('active licenses not found..');
		  callback(true,{err:1,text:'No licenses for Company'});
    } else    {
      var llen = liclist.length;
      var tday = new Date();
      tday = tday.setHours(0,0,0,0)

      for (var i=0;i<llen;i++)  {
        var lic = liclist[i];

        var edate = new Date(lic.enddate);
        var sdate = new Date(lic.startdate);

        if (tday <= edate && tday >= sdate)       {
          //console.log('license dates also valid..');
    		  callback(false,lic);
          lfound = true;
          break;
        }
      }
      if (!lfound)    {
        //console.log('licenses dates not valid..');
        if (lic)
   		  	callback(true,{err:2,text:'Licenses expired for Company on '+lic.enddate});
   		  else
   		  	callback(true,{err:2,text:'No Licenses found for the company'});
      }
    }
  });
};

exports.getnewlicensePage = function(uniid, newpage, callback)	{
	var query = { uniid: uniid };
	licenses.find(query).sort( { active:1, enddate:-1 }).skip((newpage-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, res) {
    if (e)
		  callback(true,{err:1,text:'Error in reading Licenses data'});
    else  {
      callback(false,res);
    }
	});
};
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

var getalllicense = function(coid, callback)	{
	var query = { coid: parseInt(coid) };
	licenses.find(query).toArray(function(e2, res) {
    if (e2)
		  callback(true,{err:1,text:'License not found'});
    else
		  callback(false,res);
	});
};

