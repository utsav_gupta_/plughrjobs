if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			apprsl.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'apprsl not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading apprsl'});
	});
};
exports.getNPapprsl = function(coid, userid, pageno, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getByDept_rp = function(searchData, callback)   {
	var apprsl  = dbConn.collection(searchData.coid+'_apprsl');

	apprsl.find({rperiod:searchData.rperiod, dept:searchData.depid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	newData.date = new Date();
	newData.rperiod = newData.rperiod;
	newData.dept = newData.dept;
	newData.mgr = newData.mgr;
	newData.srat = newData.srat;
	newData.mrat = newData.mrat;
	newData.krat = newData.krat;
	newData.frat = newData.frat;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;

	apprsl.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)		{
			callback(true,{err:1,text:'apprsl not added'});
		}  else   {
			callback(true,{err:2,text:'Table apprsl data added'});
		}
	});
}
exports.updateapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	apprsl.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.userid = newData.userid;
			o.rperiod = newData.rperiod;
			o.dept = newData.dept;
			o.mgr = newData.mgr;
			o.srat = newData.srat;
			o.mrat = newData.mrat;
			o.krat = newData.krat;
			o.frat = newData.frat;
			delete o.coid;
			apprsl.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else   {
					getFPapprsl(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table apprsl data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}
exports.deleteapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	apprsl.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'apprsl data not deleted'});
		else   {
			getFPapprsl(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table apprsl data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}

exports.delapprsl_dept_rp = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');
	//console.log(newData);

	apprsl.remove({dept:newData.dept, rperiod:newData.rperiod}, function(e, o)   {
		if (e) 	{
			//console.log('Appraisals not deleted');
			callback(true,{err:1,text:'Appraisals not deleted'});
		} else	{
			//console.log('Appraisals deleted');
			callback(true,{err:2,text:'Appraisals deleted'});
		}
	});
}

var getFPapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			apprsl.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'apprsl not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading apprsl'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

