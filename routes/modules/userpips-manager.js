if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userpips.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userpips not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userpips'});
	});
};
exports.getNPuserpips = function(coid, userid, pageno, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userpips not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalluserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userpips not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getpipbyuser = function(searchData, callback)   {
	var userpips  = dbConn.collection(searchData.coid+'_userpips');

	userpips.find({userid:(searchData.userid).toString()}).toArray(function(e, o)   {
		if (e)		{
			callback(true,{err:1,text:'PIP data not found'});
		}	else    {
			callback(false,o);
		}
	});
}

exports.addnewuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	newData.date = new Date();
	newData.reason = newData.reason;
	newData.sdate = newData.sdate;
	newData.edate = newData.edate;
	newData.status = newData.status;
	newData.comments = newData.comments;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	newData.userid = parseInt(newData.userid);

	userpips.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'userpips not added'});
		else   {
			getFPuserpips(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table userpips data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.userid = newData.userid;
			o.reason = newData.reason;
			o.sdate = newData.sdate;
			o.edate = newData.edate;
			o.status = newData.status;
			o.comments = newData.comments;
			delete o.coid;
			userpips.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'userpips not updated'});
				else   {
					getFPuserpips(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table userpips data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userpips'});
		}
	});
}
exports.deleteuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userpips data not deleted'});
		else   {
			getFPuserpips(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table userpips data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPuserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userpips.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userpips not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userpips'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

//-----------------------------------------------------------------------------------
// ---- User PIP tasks
//-----------------------------------------------------------------------------------

exports.getFPpiptask = function(coid, userid, callback)   {
	var piptask  = dbConn.collection(coid+'_userpips');

	piptask.find({userid:userid, status:'1'}, {piptask:1}).toArray(function(e1, o1)   {
		if (o1 && o1.length > 0)      {
			if (o1[0].piptask)		{
				var tot = o1[0].piptask.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			piptask.find({userid:userid, status:'1'}, {piptask:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {piptask:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'PIP Objectives not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading PIP Objectives'});
	});
};
exports.getNPpiptask = function(coid, userid, pageno, callback)   {
	var piptask  = dbConn.collection(coid+'_userpips');

	piptask.find({userid:userid, status:'1'}, {piptask:{ $slice: [(pageno-1)*NUMBER_OF_ITEMS_PER_PAGE, NUMBER_OF_ITEMS_PER_PAGE]}}, {piptask:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'PIP Objectives not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallpiptask = function(coid, userid, callback)   {
	var piptask  = dbConn.collection(coid+'_userpips');

	piptask.find({userid:userid, status:'1'}, {piptask:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'piptask not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewpiptask = function(newData, callback)   {
	var piptask  = dbConn.collection(newData.coid+'_userpips');

	//newData.userid = parseInt(newData.userid);

	piptask.find({userid:newData.userid, status:'1'}, {piptask:1}).toArray(function(e, o)   {
		if (o)			{
		  var edObj = new Object;
			edObj.obj = newData.obj;
			edObj.meets = newData.meets;
			edObj.sdate = newData.sdate;
			edObj.edate = newData.edate;
			edObj.scomm = "";
			edObj.mcomm = "";
		  edObj.date = new Date();

			piptask.update({userid:newData.userid, status:'1'}, { $push : {"piptask":edObj}}, {safe: true}, function (e1,o1)      {
				if (e1)	{
					callback(true,{err:1,text:'PIP Objective not added'});
				} else   {
					getFPpiptask(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'PIP Objective added'});
						else		{
							callback(false,o2, totpages);
						}
					});
				}
			});
		} else	{
			callback(true,{err:1,text:'User not found'});
		}
	});
}
exports.updatepiptask = function(newData, callback)   {
	var piptask  = dbConn.collection(newData.coid+'_userpips');

	piptask.findOne({_id:getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			piptask.update({_id:getObjectId(newData.dbid), "piptask.obj":newData.old_obj, "piptask.meets":newData.old_meets, "piptask.sdate":newData.old_sdate, "piptask.edate":newData.old_edate}, { $set : {"piptask.$.obj":newData.obj, "piptask.$.meets":newData.meets, "piptask.$.sdate":newData.sdate, "piptask.$.edate":newData.edate}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'PIP Objective not updated'});
				else   {
					getFPpiptask(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'PIP Objective updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' PIP Objective not found'});
		}
	});
}
exports.deletepiptask = function(newData, callback)   {
	var piptask  = dbConn.collection(newData.coid+'_userpips');

	//console.log(newData);

	piptask.findOne({_id:getObjectId(newData.dbid)}, {piptask:1}, function(e, o)   {
		//console.log(o);
		if (o)   {
			piptask.update({_id:getObjectId(newData.dbid)}, { $pull : {"piptask": {obj : newData.obj, meets : newData.meets, sdate : newData.sdate, edate : newData.edate}}}, {safe: true}, function (e1,o1)      {
				if (e1) 
					callback(true,{err:1,text:'PIP Objective not deleted'});
				else   {
					getFPpiptask(newData.coid,newData.userid.toString(), function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'PIP Objective deleted'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:'PIP objective not found'});
		}
	});
}
var getFPpiptask = function(coid, userid, callback)   {
	var piptask  = dbConn.collection(coid+'_userpips');

	piptask.find({userid:userid, status:'1'}, {piptask:1}).toArray(function(e1, o1)   {
		if (o1 && o1.length > 0)      {
			if (o1[0].piptask)		{
				var tot = o1[0].piptask.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			piptask.find({userid:userid, status:'1'}, {piptask:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {piptask:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'PIP Objectives not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading PIP Objectives'});
	});
}

exports.updpipmgr = function(newData, callback)   {
	var piptask  = dbConn.collection(newData.coid+'_userpips');

	piptask.findOne({_id:getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			piptask.update({_id:getObjectId(newData.dbid), "piptask.obj":newData.old_obj, "piptask.meets":newData.old_meets, "piptask.sdate":newData.old_sdate, "piptask.edate":newData.old_edate}, { $set : {"piptask.$.mcomm":newData.mcomm}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Manager comment not updated'});
				else   {
					getFPpiptask(newData.coid,newData.userid.toString(), function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Manager comment updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' PIP Objective not found'});
		}
	});
}

exports.updpipself = function(newData, callback)   {
	var piptask  = dbConn.collection(newData.coid+'_userpips');

	piptask.findOne({_id:getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			piptask.update({_id:getObjectId(newData.dbid), "piptask.obj":newData.old_obj, "piptask.meets":newData.old_meets, "piptask.sdate":newData.old_sdate, "piptask.edate":newData.old_edate}, { $set : {"piptask.$.scomm":newData.scomm}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Team member comment not updated'});
				else   {
					getFPpiptask(newData.coid,newData.userid.toString(), function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Team member comment updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' PIP Objective not found'});
		}
	});
}


