if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPholidaypolicy = function(coid, userid, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	holidaypolicy.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			holidaypolicy.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'holidaypolicy not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading holidaypolicy'});
	});
};
exports.getNPholidaypolicy = function(coid, userid, pageno, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	holidaypolicy.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'holidaypolicy not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallholidaypolicy = function(coid, userid, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	holidaypolicy.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'holidaypolicy not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getcount = function(coid, userid, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	holidaypolicy.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Holiday policy not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.getlocholidaypolicy = function(coid, userid, location, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	  dd='0'+dd
	} 
	if(mm<10) {
	  mm='0'+mm
	} 
	today = dd+'-'+mm+'-'+yyyy;
  var query = {};
  var qry1 = {};

  query["$and"]=[];
  query["$and"].push({"appoffice":location});
  //query["$and"].push({"validfrom": {$lte: today}});
  //query["$and"].push({"validto": {$gte: today}});
	//console.log(JSON.stringify(query));

	holidaypolicy.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'holidaypolicy not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewholidaypolicy = function(newData, callback)   {
	var holidaypolicy  = dbConn.collection(newData.coid+'_holidaypolicy');

	newData.date = new Date();
	newData.htitle = newData.htitle;
	newData.validfrom = newData.validfrom;
	newData.validto = newData.validto;
	newData.appoffice = newData.appoffice;
	newData.maxopholidays = newData.maxopholidays;
	newData.selholiday = newData.selholiday;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	holidaypolicy.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'holidaypolicy not added'});
		else   {
			getFPholidaypolicy(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table holidaypolicy data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateholidaypolicy = function(newData, callback)   {
	var holidaypolicy  = dbConn.collection(newData.coid+'_holidaypolicy');

	holidaypolicy.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.htitle = newData.htitle;
			o.validfrom = newData.validfrom;
			o.validto = newData.validto;
			o.appoffice = newData.appoffice;
			o.maxopholidays = newData.maxopholidays;
			o.selholiday = newData.selholiday;
			delete o.coid;
			holidaypolicy.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'holidaypolicy not updated'});
				else   {
					getFPholidaypolicy(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table holidaypolicy data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in holidaypolicy'});
		}
	});
}
exports.deleteholidaypolicy = function(newData, callback)   {
	var holidaypolicy  = dbConn.collection(newData.coid+'_holidaypolicy');

	holidaypolicy.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'holidaypolicy data not deleted'});
		else   {
			getFPholidaypolicy(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table holidaypolicy data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPholidaypolicy = function(coid, userid, callback)   {
	var holidaypolicy  = dbConn.collection(coid+'_holidaypolicy');

	holidaypolicy.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			holidaypolicy.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'holidaypolicy not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading holidaypolicy'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

