if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

async = require("async");

exports.getFPskills = function(userid, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			skills.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'skills not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading skills'});
	});
};
exports.getNPskills = function(userid, pageno, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'skills not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallskills = function(userid, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.find({}).sort({skilldesc:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'skills not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getskillsArray = function(skills, callback)   {
	var skills  = dbConn.collection('candid_skills');

  var query = {};
  query["$or"] = [];
	var slen = 0;
	if (skills)		{
		var slen = skills.length;
		for (var i=0; i<slen; i++)		{
		  query["$or"].push({_id:skills[i]});
		}
	}
	console.log(query);	
	
	skills.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'skills not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewskills = function(newData, callback)   {
	var skills  = dbConn.collection('candid_skills');

	newData.date = new Date();
	newData.skilldesc = newData.skilldesc;
	var suserid = newData.userid;
	delete newData.userid;

	skills.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'skills not added'});
		else   {
			getFPskills(suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table skills data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.addmanyskills = function(newData, callback)   {
	var skills  = dbConn.collection('candid_skills');
	var suserid = newData.userid;
	var items = newData.slist;
	var idlist = [];
	var objlist = [];

	async.each(items, 
	  // Call an asynchronous function, often a save() to DB
		function(item, callback)		{
			var skilldata = new Object;
			skilldata.date = new Date();
			skilldata.skillname = item;
			skilldata.skilldesc = "";
			skills.insert(skilldata, {safe: true}, function (e1,o1)      {
				objlist.push(o1[0]);
				idlist.push(o1[0]._id.toString());
				callback(e1,o1);
			});
		},
		// 3rd param is the function to call when everything's done
		function(err){
		  // All tasks are done now
			getallskills(suserid, function(e2,o2)     {
				if (e2)
					callback(true,{err:2,text:'Table skills data added'});
				else
					callback(false,o2, objlist, idlist);
			});
		}
	);
}

exports.updateskills = function(newData, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.skillname = newData.skillname;
			o.skilldesc = newData.skilldesc;
			skills.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'skills not updated'});
				else   {
					getFPskills(newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table skills data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in skills'});
		}
	});
}
exports.deleteskills = function(newData, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'skills data not deleted'});
		else   {
			getFPskills(newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table skills data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPskills = function(userid, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			skills.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'skills not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading skills'});
	});
}

var getallskills = function(userid, callback)   {
	var skills  = dbConn.collection('candid_skills');

	skills.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'skills not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

