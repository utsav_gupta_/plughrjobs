if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPonbdacts = function(coid, userid, callback)   {
	var onbdacts  = dbConn.collection(coid+'_onbdacts');

	onbdacts.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			onbdacts.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'onbdacts not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading onbdacts'});
	});
};
exports.getNPonbdacts = function(coid, userid, pageno, callback)   {
	var onbdacts  = dbConn.collection(coid+'_onbdacts');

	onbdacts.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'onbdacts not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallonbdacts = function(coid, userid, callback)   {
	var onbdacts  = dbConn.collection(coid+'_onbdacts');

	onbdacts.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'onbdacts not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewonbdacts = function(newData, callback)   {
	var onbdacts  = dbConn.collection(newData.coid+'_onbdacts');

	newData.date = new Date();
	newData.desc = newData.desc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	onbdacts.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'onbdacts not added'});
		else   {
			getFPonbdacts(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table onbdacts data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateonbdacts = function(newData, callback)   {
	var onbdacts  = dbConn.collection(newData.coid+'_onbdacts');

	onbdacts.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.desc = newData.desc;
			delete o.coid;
			onbdacts.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'onbdacts not updated'});
				else   {
					getFPonbdacts(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table onbdacts data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in onbdacts'});
		}
	});
}
exports.deleteonbdacts = function(newData, callback)   {
	var onbdacts  = dbConn.collection(newData.coid+'_onbdacts');

	onbdacts.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'onbdacts data not deleted'});
		else   {
			getFPonbdacts(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table onbdacts data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPonbdacts = function(coid, userid, callback)   {
	var onbdacts  = dbConn.collection(coid+'_onbdacts');

	onbdacts.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			onbdacts.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'onbdacts not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading onbdacts'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

