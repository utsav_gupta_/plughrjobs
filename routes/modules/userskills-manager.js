if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuserskills = function(coid, userid, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	userskills.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userskills.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userskills not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userskills'});
	});
};
exports.getNPuserskills = function(coid, userid, pageno, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	userskills.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userskills not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalluserskills = function(coid, userid, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	userskills.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userskills not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getskillsofuser = function(coid, userid, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	if (typeof(userid) == 'string')
		userid = parseInt(userid);
	//console.log(userid);
	//console.log(typeof(userid));

	userskills.find({userid: userid, active:true}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userskills not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewuserskills = function(newData, callback)   {
	var userskills  = dbConn.collection(newData.coid+'_userskills');

	newData.date = new Date();
	var scoid = newData.coid;
	var suserid = newData.userid;
	delete newData.coid;
	userskills.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'userskills not added'});
		else   {
			getskillsofuser(scoid,suserid, function(e2,o2)     {
			if (e2)
				callback(true,{err:2,text:'Table userskills data added'});
			else
				callback(false,o2);
			});
		}
	});
}
exports.updateuserskills = function(newData, callback)   {
	var userskills  = dbConn.collection(newData.coid+'_userskills');

	userskills.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.active = false;
			userskills.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'New ratings not updated'});
				else   {
					newData.date = new Date();
					var scoid = newData.coid;
					var suserid = newData.userid;
					delete newData.dbid;
					delete newData.coid;
					userskills.insert(newData, {safe: true}, function (e2,o2)      {
						if (o2)		{
							getskillsofuser(scoid,suserid, function(e3,o3)     {
								if (e3)
									callback(true,{err:2,text:'New ratings for employee updated'});
								else
									callback(false,o3);
							});
						} else	{
							callback(true,{err:3,text:'New Ratings data NOT updated'});
						}
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userskills'});
		}
	});
}
exports.update2userskills = function(newData, callback)   {
	var userskills  = dbConn.collection(newData.coid+'_userskills');

	userskills.findOne({userid: newData.userid, active:true}, function(e, o)   {
		if (o)   {
			o.active = false;
			userskills.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'New ratings not updated'});
				else   {
					newData.date = new Date();
					var scoid = newData.coid;
					var suserid = newData.userid;
					delete newData.coid;
					userskills.insert(newData, {safe: true}, function (e2,o2)      {
						if (o2)		{
							getskillsofuser(scoid,suserid, function(e3,o3)     {
								if (e3)
									callback(true,{err:2,text:'New ratings for employee updated'});
								else
									callback(false,o3);
							});
						} else	{
							callback(true,{err:3,text:'New Ratings data NOT updated'});
						}
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userskills'});
		}
	});
}

exports.deleteuserskills = function(newData, callback)   {
	var userskills  = dbConn.collection(newData.coid+'_userskills');

	userskills.remove({userid: newData.userid}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userskills data not deleted'});
		else   {
			getFPuserskills(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table userskills data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
exports.deleteuskillsbyuser = function(newData, callback)   {
	var userskills  = dbConn.collection(newData.coid+'_userskills');

	userskills.remove({userid: parseInt(newData.userid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userskills data not deleted'});
		else
			callback(false,o);
	});
}

var getskillsofuser = function(coid, userid, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	userskills.find({userid: userid, active:true}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userskills not found'});
		else    {
			callback(false,o);
		}
	});
}

var getFPuserskills = function(coid, userid, callback)   {
	var userskills  = dbConn.collection(coid+'_userskills');

	userskills.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userskills.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userskills not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userskills'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

