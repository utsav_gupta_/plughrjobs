if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPptslabs = function(coid, userid, callback)   {
	var ptslabs  = dbConn.collection(coid+'_ptslabs');

	ptslabs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			ptslabs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'ptslabs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading ptslabs'});
	});
};
exports.getNPptslabs = function(coid, userid, pageno, callback)   {
	var ptslabs  = dbConn.collection(coid+'_ptslabs');

	ptslabs.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'ptslabs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallptslabs = function(coid, userid, callback)   {
	var ptslabs  = dbConn.collection(coid+'_ptslabs');

	ptslabs.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'ptslabs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewptslabs = function(newData, callback)   {
	var ptslabs  = dbConn.collection(newData.coid+'_ptslabs');

	newData.date = new Date();
	newData.max = newData.max;
	newData.ptamt = newData.ptamt;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	ptslabs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'ptslabs not added'});
		else   {
			getFPptslabs(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table ptslabs data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateptslabs = function(newData, callback)   {
	var ptslabs  = dbConn.collection(newData.coid+'_ptslabs');

	ptslabs.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.min = newData.min;
			o.max = newData.max;
			o.ptamt = newData.ptamt;
			delete o.coid;
			ptslabs.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'ptslabs not updated'});
				else   {
					getFPptslabs(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table ptslabs data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in ptslabs'});
		}
	});
}
exports.deleteptslabs = function(newData, callback)   {
	var ptslabs  = dbConn.collection(newData.coid+'_ptslabs');

	ptslabs.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'ptslabs data not deleted'});
		else   {
			getFPptslabs(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table ptslabs data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPptslabs = function(coid, userid, callback)   {
	var ptslabs  = dbConn.collection(coid+'_ptslabs');

	ptslabs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			ptslabs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'ptslabs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading ptslabs'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

