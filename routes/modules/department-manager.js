if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPdepartment = function(userid, callback)   {
	var department  = dbConn.collection('department');

	department.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			department.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'department not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading department'});
	});
};
exports.getNPdepartment = function(userid, pageno, callback)   {
	var department  = dbConn.collection('department');

	department.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'department not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalldepartment = function(userid, callback)   {
	var department  = dbConn.collection('department');

	department.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'department not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getcount = function(userid, callback)   {
	var department  = dbConn.collection('department');

	department.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Departments not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.addnewdepartment = function(newData, callback)   {
	var department  = dbConn.collection('department');

	newData.date = new Date();
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	department.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'department not added'});
		else   {
			getFPdepartment(ssuserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table department data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatedepartment = function(newData, callback)   {
	var department  = dbConn.collection('department');

	department.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.depname = newData.depname;
			delete o.coid;
			department.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'department not updated'});
				else   {
					getFPdepartment(newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table department data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in department'});
		}
	});
}
exports.deletedepartment = function(newData, callback)   {
	var department  = dbConn.collection('department');

	department.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'department data not deleted'});
		else   {
			getFPdepartment(newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table department data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPdepartment = function(userid, callback)   {
	var department  = dbConn.collection('department');

	department.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			department.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'department not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading department'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

