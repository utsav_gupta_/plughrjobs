if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 20;

exports.getFPempleaves = function(coid, userid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  
	empleaves.find({userid:userid, date: {$gte:cdate}}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			empleaves.find({userid:userid, date: {$gte:cdate}}).sort({lstatus:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'empleaves not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading empleaves'});
	});
};

exports.getNPempleaves = function(coid, userid, pageno, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}
  
	empleaves.find({userid:userid, date: {$gte:cdate}}).sort({lstatus:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallempleaves = function(coid, userid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}

	empleaves.find({userid:userid, date: {$gte:cdate}}).sort({lstatus:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getempleaves = function(coid, userid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}
  
	empleaves.find({userid:userid, date: {$gte:cdate}, lstatus:{ $ne: 'rejected'} }).sort({lstatus:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getteamopenleaves = function(coid, mgrid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}
  
	empleaves.find({mgr:mgrid, date: {$gte:cdate}, lstatus:'applied'}).sort({lstatus:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewempleaves = function(newData, callback)   {
	var empleaves  = dbConn.collection(newData.coid+'_empleaves');

	newData.date = new Date();
	newData.ldays = newData.ldays;
	newData.hdays = newData.hdays;
	newData.ltot = newData.ltot;
	newData.mgr = newData.mgr;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	//delete newData.userid;

	empleaves.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'empleaves not added'});
		else   {
			getFPempleaves(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table empleaves data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateempleaves = function(newData, callback)   {
	var empleaves  = dbConn.collection(newData.coid+'_empleaves');

	empleaves.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
      var mgrid = '';
			if (newData.optype === "a")   {
				o.lstatus = "approved";
				mgrid = newData.userid;
			} else if (newData.optype === "u")    {
				o.lstatus = "applied";
				o.ldays = newData.ldays;
	      o.hdays = newData.hdays;
	      o.ltot = newData.ltot;
				mgrid = newData.mgr;
			} else  {
				o.lstatus = "rejected";
				mgrid = newData.userid;
      }
      //console.log(o);
			
			delete o.coid;
			empleaves.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'empleaves not updated'});
				else   {
					getteamopenleaves(newData.coid,mgrid.toString(), function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table empleaves data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in empleaves'});
		}
	});
}

exports.delempleaves = function(newData, callback)   {
	var empleaves  = dbConn.collection(newData.coid+'_empleaves');

	empleaves.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'empleaves data not deleted'});
		else   {
			getallempleaves(newData.coid,newData.userid, function(e2,o2)     {
				if (e2)
					callback(true,{err:2,text:'Table empleaves data deleted'});
				else    {
					callback(false,o2);
				}
			});
		}
	});
}

function getallempleaves(coid, userid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}

  var empid = parseInt(userid);
	empleaves.find({userid:empid, date: {$gte:cdate}}).sort({lstatus:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.deleteempleaves = function(newData, callback)   {
	var empleaves  = dbConn.collection(newData.coid+'_empleaves');

	empleaves.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'empleaves data not deleted'});
		else   {
			getFPempleaves(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table empleaves data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}

var getFPempleaves = function(coid, userid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}
  
	empleaves.find({userid:userid, date: {$gte:cdate}}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			empleaves.find({userid:userid, date: {$gte:cdate}}).sort({lstatus:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'empleaves not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading empleaves'});
	});
}

var getteamopenleaves = function(coid, mgrid, callback)   {
	var empleaves  = dbConn.collection(coid+'_empleaves');

  var cdate = new Date();
  var cyear = cdate.getFullYear();
  var cdate = new Date(cyear, '0', '1');
  //console.log(cdate);
  //, date: {$gte:cdate}
  
	empleaves.find({mgr:mgrid, date: {$gte:cdate}, lstatus:'applied'}).sort({lstatus:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'empleaves not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

