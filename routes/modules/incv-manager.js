var crypto 		= require('crypto')
var MongoDB 	= require('mongodb').Db;
var Server 		= require('mongodb').Server;
//var moment 	= require('moment');
var EM        = require('./email-dispatcher');
var async 		= require('async');

exports.getincvbyperiod_user = function(searchData, callback)   {
	var incv  = dbConn.collection(searchData.coid+'_incv');
	incv.findOne({rperiod:searchData.rperiod, userid:searchData.userid}, function(e, o)   {
		if (!o)		{
			callback(true,{err:1,text:'Incentive plan not found'});
		}	else    {
			callback(false,o);
		}
	});
}

exports.updatedata = function(newData, callback)   {
	var incv  = dbConn.collection(newData.coid+'_incv');
	incv.findOne({userid:parseInt(newData.userid), rperiod:newData.rperiod}, function(e, o)   {
		if (o)    {
			o.incvfile = newData.incvfile;
			incv.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Incentive plan not updated'});
				else
					callback(false,{err:0,text:'Incentive plan updated'});
			});
		} else  {
			newData.incvfile = newData.incvfile;
			incv.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Incentive plan not inserted'});
				else
					callback(false,{err:0,text:'Incentive plan inserted'});
			});
		}
	});
}

/* private encryption & validation methods */
var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

