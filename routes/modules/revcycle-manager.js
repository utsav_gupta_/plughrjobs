exports.getonerevcycle = function(coid, userid, callback)   {
	var revcycle  = dbConn.collection(coid+'_revcycle');

	revcycle.findOne({userid:userid }, function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revcycle not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.updaterevcycle = function(newData, callback)   {
	var revcycle  = dbConn.collection(newData.coid+'_revcycle');

	revcycle.findOne({userid:newData.userid}, function(e, o)   {
		if (o)    {
				o.frevdate = newData.frevdate;
				o.revafter = newData.revafter;
			delete o.coid;
			revcycle.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'revcycle not updated'});
				else
					callback(false,{err:0,text:'revcycle updated'});
			});
		} else  {
			newData.frevdate = newData.frevdate;
			newData.revafter = newData.revafter;
			delete newData.coid;
			revcycle.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'revcycle not inserted'});
				else
					callback(false,{err:0,text:'revcycle inserted'});
			});
		}
	});
}

