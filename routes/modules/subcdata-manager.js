if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPsubcdata = function(coid, userid, callback)   {
	var subcdata  = dbConn.collection(coid+'_salcomp');

	subcdata.find({_id:getObjectId(salcomp)}, {subcdata:1}).toArray(function(e1, o1)   {
		if (!e1)      {
			if (o1[0].subcdata)		{
				var tot = o1[0].subcdata.length / NUMBER_OF_ITEMS_PER_PAGE;
				var tot = Math.ceil(tot);
			} else	{
				var tot = 0;
			}
			subcdata.find({_id:getObjectId(salcomp)}, {subcdata:{ $slice: NUMBER_OF_ITEMS_PER_PAGE}}, {subcdata:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'Salary Sub-component not found'});
				else	{
					console.log(tot);
					callback(false,o, tot);
				}
			});
		} else
			callback(true,{err:2,text:'Error in reading subcdata'});
	});
};

exports.getNPsubcdata = function(coid, userid, pageno, callback)   {
	var subcdata  = dbConn.collection(coid+'_salcomp');

	subcdata.find({_id:getObjectId(salcomp)}, {subcdata:{ $slice: [(pageno-1)*NUMBER_OF_ITEMS_PER_PAGE, NUMBER_OF_ITEMS_PER_PAGE]}}, {subcdata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Salary Sub-component not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallsubcdata = function(coid, userid, callback)   {
	var subcdata  = dbConn.collection(coid+'_salcomp');

	subcdata.find({_id:getObjectId(salcomp)}, {subcdata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Salary Sub-component not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getsubc_salc = function(coid, userid, salcomp, callback)   {
	var subcdata  = dbConn.collection(coid+'_salcomp');

	subcdata.find({_id:getObjectId(salcomp)}, {subcdata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Salary Sub-component not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewsubcdata = function(newData, callback)   {
	var subcdata  = dbConn.collection(newData.coid+'_salcomp');

	subcdata.find({_id:getObjectId(newData.salcomp)}, {subcdata:1}).toArray(function(e, o)   {
		if (o)			{
		  var edObj = new Object;
			edObj.sctitle = newData.title;
			edObj.scdesc = newData.desc;
		  edObj.date = new Date();

			subcdata.update({_id:getObjectId(newData.salcomp)}, { $push : {"subcdata":edObj}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Salary Sub-component not added'});
				else   {
					getFPsubcdata(newData.coid,newData.userid,newData.salcomp, function(e2,o2, totpages)     {
					if (e2)
						callback(true,{err:2,text:'Salary Sub-component added'});
					else
						callback(false,o2, totpages);
					});
				}
			});
		} else	{
			callback(true,{err:1,text:'Salary component not found'});
		}
	});
}

exports.updatesubcdata = function(newData, callback)   {
	var subcdata  = dbConn.collection(newData.coid+'_salcomp');

	subcdata.find({_id:getObjectId(newData.salcomp)}, {subcdata:1}).toArray(function(e, o)   {
		if (o)   {
			subcdata.update({_id:getObjectId(newData.salcomp), "subcdata.sctitle":newData.old_title, "subcdata.scdesc":newData.old_desc}, { $set : {"subcdata.$.sctitle":newData.title, "subcdata.$.scdesc":newData.desc}}, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Salary Sub-component not updated'});
				else   {
					getFPsubcdata(newData.coid,newData.userid,newData.salcomp, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Salary Sub-component updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' Salary component not found'});
		}
	});
}

exports.deletesubcdata = function(newData, callback)   {
	var subcdata  = dbConn.collection(newData.coid+'_salcomp');

	//console.log(newData);

	subcdata.findOne({_id:getObjectId(newData.salcomp)}, {subcdata:1}, function(e, o)   {
		//console.log(o);
		if (o)   {
			subcdata.update({_id:getObjectId(newData.salcomp)}, { $pull : {"subcdata": {sctitle : newData.title, scdesc : newData.desc}}}, {safe: true}, function (e1,o1)      {
				if (e1) 
					callback(true,{err:1,text:'Salary Sub-component not deleted'});
				else   {
					getFPsubcdata(newData.coid,newData.userid,newData.salcomp, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Salary Sub-component deleted'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' Salary component not found'});
		}
	});
}

var getFPsubcdata = function(coid, userid, salcomp, callback)   {
	var subcdata  = dbConn.collection(coid+'_salcomp');

	subcdata.find({_id:getObjectId(salcomp)}, {subcdata:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Salary Sub-component not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}



