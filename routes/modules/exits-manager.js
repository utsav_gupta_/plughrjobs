if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPexits = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

	exits.find({userid:userid.toString()}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			exits.find({userid:userid.toString()}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'exits not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading exits'});
	});
};
exports.getNPexits = function(coid, userid, pageno, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

	exits.find({userid:userid.toString()}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else
			callback(false,o);
	});
}
exports.getallexits = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

  var query = {};
  var qry1 = {};
  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '1'}, {"status": '2'}, {"status": '3'}, {"status": '4'}]};
  query["$and"].push(qry1);

	exits.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getapprexits = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

  var query = {};
  var qry1 = {};
  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '2'}]};
  query["$and"].push(qry1);

	exits.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.exitsbyuser = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({userid:userid.toString()});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '0'}]};
  query["$and"].push(qry1);

	exits.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.exitsbymgr = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({mgr:userid.toString()});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '0'},{"status": '1'}, {"status": '2'}, {"status": '3'}, {"status": '4'}]};
  query["$and"].push(qry1);

	exits.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewexits = function(newData, callback)   {
	var exits  = dbConn.collection(newData.coid+'_exits');

	newData.date = new Date();
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;

  var query = {};
  var qry1 = {};
  query["$and"]=[];
  query["$and"].push({"userid":newData.userid});

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '0'}, {"status": '1'}, {"status": '2'}]};
  query["$and"].push(qry1);

	exits.findOne(query, function(e, o)   {
		if (o)   {
			callback(true,{err:3,text:'Already a resignation is pending'});
		} else	{
			exits.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'exits not added'});
				else   {
					getFPexits(scoid,suserid, function(e2,o2, totpages)     {
					if (e2)
						callback(true,{err:2,text:'Table exits data added'});
					else
						callback(false,o2, totpages);
					});
				}
			});
		}
	});
}
exports.updateexits = function(newData, callback)   {
	var exits  = dbConn.collection(newData.coid+'_exits');

	exits.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.reason = newData.reason;
			o.ldate = newData.ldate;
			o.repluserid = newData.repluserid;
			o.status = newData.status;
			o.fback = newData.fback;
			delete o.coid;
			exits.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'exits not updated'});
				else   {
					getFPexits(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table exits data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in exits'});
		}
	});
}
exports.mgrupdates = function(newData, callback)   {
	var exits  = dbConn.collection(newData.coid+'_exits');

	exits.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.ldate = newData.ldate;
			o.repluserid = newData.repluserid;
			o.status = newData.status;
			o.eact = newData.eact;
			exits.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'exits not updated'});
				else   {
					exitsbymgr(newData.coid,newData.mgr, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table exits data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in exits'});
		}
	});
}

exports.finalise = function(newData, callback)   {
	var exits  = dbConn.collection(newData.coid+'_exits');

	exits.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.status = '5';
			exits.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'exits not updated'});
				else   {
					getapprexits(newData.coid,newData.mgr, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table exits data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in exits'});
		}
	});
}

exports.deleteexits = function(newData, callback)   {
	var exits  = dbConn.collection(newData.coid+'_exits');

	exits.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'exits data not deleted'});
		else   {
			getFPexits(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table exits data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPexits = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

	exits.find({userid:userid.toString()}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			exits.find({userid:userid.toString()}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'exits not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading exits'});
	});
}

var exitsbymgr = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

	exits.find({mgr:userid.toString()}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

var getapprexits = function(coid, userid, callback)   {
	var exits  = dbConn.collection(coid+'_exits');

  var query = {};
  var qry1 = {};
  query["$and"]=[];

  qry1["$or"]=[];
  qry1 = { $or: [ {"status": '2'}]};
  query["$and"].push(qry1);

	exits.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'exits not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

