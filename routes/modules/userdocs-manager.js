if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuserdocs = function(coid, userid, callback)   {
	var userdocs  = dbConn.collection(coid+'_userdocs');

	userdocs.find({userid:String(userid)}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userdocs.find({userid:String(userid)}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userdocs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userdocs'});
	});
};
exports.getNPuserdocs = function(coid, userid, pageno, callback)   {
	var userdocs  = dbConn.collection(coid+'_userdocs');

	userdocs.find({userid:String(userid)}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userdocs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalluserdocs = function(coid, userid, callback)   {
	var userdocs  = dbConn.collection(coid+'_userdocs');

	userdocs.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userdocs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewuserdocs = function(newData, callback)   {
	var userdocs  = dbConn.collection(newData.coid+'_userdocs');

	newData.date = new Date();
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	//delete newData.userid;

	userdocs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'userdocs not added'});
		else   {
			getFPuserdocs(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table userdocs data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.deleteuserdocs = function(newData, callback)   {
	var userdocs  = dbConn.collection(newData.coid+'_userdocs');
	userdocs.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userdocs data not deleted'});
		else   {
			getFPuserdocs(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table userdocs data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPuserdocs = function(coid, userid, callback)   {
	var userdocs  = dbConn.collection(coid+'_userdocs');

	userdocs.find({userid:String(userid)}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userdocs.find({userid:String(userid)}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userdocs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userdocs'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

