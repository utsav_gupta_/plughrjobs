if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			apprsl.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'apprsl not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading apprsl'});
	});
};

exports.getNPapprsl = function(coid, userid, pageno, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getnewapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({status:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getactiveapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({status:2}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addnewapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	newData.date = new Date();
	newData.status = 1;

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	apprsl.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'apprsl not added'});
		else   {
			getFPapprsl(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table apprsl data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	apprsl.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.aprtitle = newData.aprtitle;
			o.rvteam = newData.rvteam;
			o.loc = newData.loc;
			o.dept = newData.dept;
			o.role = newData.role;
			o.etype = newData.etype;
			o.jdate = newData.jdate;
			o.sdate = newData.sdate;
			o.edate = newData.edate;
			o.rvoptn = newData.rvoptn;
			o.rpwt = newData.rpwt;
			o.rperiod = newData.rperiod;
			o.bvrt = newData.bvrt;
			o.bvwt = newData.bvwt;
			o.bvtxt = newData.bvtxt;
			o.cvrt = newData.cvrt;
			o.cvwt = newData.cvwt;
			o.trng = newData.trng;
			o.recom = newData.recom;
			delete o.coid;
			apprsl.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else   {
					getFPapprsl(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table apprsl data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}
exports.updaprslsts = function(coid, apid, sts, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.findOne({_id: getObjectId(apid)}, function(e, o)   {
		if (o)   {
			o.status = sts;
			apprsl.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.deleteapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');
	var aprdb  = dbConn.collection(newData.coid+'_'+newData.dbid);

	apprsl.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'apprsl data not deleted'});
		else   {
	    aprdb.drop(function(e, o)   {
		    if (e) 
			    callback(true,{err:1,text:'apprsl data not deleted'});
			  else    {
			    getFPapprsl(newData.coid,newData.userid, function(e2,o2, totpages)     {
				    if (e2)
					    callback(true,{err:2,text:'Table apprsl data deleted'});
				    else
					    callback(false,o2, totpages);
			    });
			  }
		  });
		}
	});
}

exports.closeapprsl = function(newData, callback)   {
	var apprsl  = dbConn.collection(newData.coid+'_apprsl');

	apprsl.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.status = 3;
			apprsl.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else   {
					getFPapprsl(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table apprsl data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

var getFPapprsl = function(coid, userid, callback)   {
	var apprsl  = dbConn.collection(coid+'_apprsl');

	apprsl.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			apprsl.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'apprsl not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading apprsl'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

// -----------------------------------------------------------------

exports.searchselfdata = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);

	aprdb.find({aprslid:searchData.aprslid, userid:parseInt(searchData.userid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.searchothrdata = function(searchData, callback)   {
  var query = {};
  var qry1 = {};

  query["$and"]=[];
	if (searchData.aprslid != '')
    query["$and"].push({'aprslid':searchData.aprslid});

  qry1["$or"]=[];
  qry1 = { $or: [{"peeruserid": searchData.userid.toString()},{"teamuserid": searchData.userid.toString()}]};
  query["$and"].push(qry1);

  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);
  //console.log(JSON.stringify(query));

	aprdb.find(query).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

exports.updateothers = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);

  aprdb.update({aprslid:searchData.aprslid, userid:parseInt(searchData.userid)}, {$set:{"peeruserid":searchData.peerid,"teamuserid":searchData.teamid}}, { upsert:false, multi:true }, function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Appraisal not updated'});
		else    {
			callback(false,o);
		}
	});
}

exports.updateself = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.selfratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.updatepeer = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.peerratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.updateteam = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.teamratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.searchsuprdata = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData);

	aprdb.find({aprslid:searchData.aprslid, mgr:searchData.mgrid, userid:parseInt(searchData.userid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

exports.updatessupr = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.suprratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.searchskipdata = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData);

	aprdb.find({aprslid:searchData.aprslid, mgr:searchData.mgrid, userid:parseInt(searchData.userid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

exports.updatesskip = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.skipratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.searchhrdata = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData);

	aprdb.find({aprslid:searchData.aprslid, userid:parseInt(searchData.userid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

exports.updateshr = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);

	aprdb.findOne({_id: getObjectId(searchData.dbid)}, function(e, o)   {
		if (o)   {
			o.hrratg = searchData.rating;
			aprdb.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'apprsl not updated'});
				else 
					callback(false,o1);
			});
		} else  {
			callback(true,{err:2,text:' ID not found in apprsl'});
		}
	});
}

exports.searchdeptdata = function(searchData, callback)   {
  var aprdb  = dbConn.collection(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData.coid + '_' + searchData.aprslid);
  //console.log(searchData);

	aprdb.find({dept:searchData.deptid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'apprsl not found'});
		else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

