if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPkra = function(userid, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			kra.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'kra not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading kra'});
	});
};
exports.getNPkra = function(userid, pageno, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallkra = function(userid, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getkraarray = function(kras, callback)   {
	var kra  = dbConn.collection('kra');

	//console.log(kras);
  var query = {};
  query["$or"] = [];
	var klen = 0;
	if (kras)		{
		var klen = kras.length;
		for (var i=0; i<klen; i++)		{
		  query["$or"].push({_id: getObjectId(kras[i])});
		}
	}
	//console.log(query);	

	kra.find(query).sort({kratitle:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}

/*
exports.getkrabyroles = function(userid, dept, role, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({dept:dept, role:role}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getkrabyrole = function(userid, role, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({role:role}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}
*/

exports.addnewkra = function(newData, callback)   {
	var kra  = dbConn.collection('kra');

	newData.date = new Date();
	//newData.role = newData.role;
	newData.kratitle = newData.kratitle;
	newData.kradesc = newData.kradesc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	kra.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'kra not added'});
		else   {
			getFPkra(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table kra data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.addmanykras = function(newData, callback)   {
	var kra  = dbConn.collection('kra');

	var skills  = dbConn.collection('skills');
	var scoid = newData.coid;
	var suserid = newData.userid;
	var items = newData.klist;
	var idlist = [];
	var objlist = [];

	async.each(items, 
	  // Call an asynchronous function, often a save() to DB
		function(item, callback)		{
			var kradata = new Object;
			//kradata.dept = newData.dept;
			//kradata.role = newData.role.toString();
			kradata.kratitle = item;
			kradata.kradesc = "";
			kradata.date = new Date();
			kra.insert(kradata, {safe: true}, function (e1,o1)      {
				objlist.push(o1[0]);
				idlist.push(o1[0]._id.toString());
				callback(e1,o1);
			});
		},
		// 3rd param is the function to call when everything's done
		function(err)		{
		  // All tasks are done now
			getallkra(scoid,suserid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'KRA data added'});
				else
					callback(false,o2, objlist, idlist);
			});
		}
	);
}

exports.updatekra = function(newData, callback)   {
	var kra  = dbConn.collection('kra');

	kra.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			//o.dept = newData.dept;
			//o.role = newData.role;
			o.kratitle = newData.kratitle;
			o.kradesc = newData.kradesc;
			delete o.coid;
			kra.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'kra not updated'});
				else   {
					getFPkra(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table kra data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in kra'});
		}
	});
}
exports.deletekra = function(newData, callback)   {
	var kra  = dbConn.collection('kra');
	kra.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'kra data not deleted'});
		else   {
			getFPkra(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table kra data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}

var getFPkra = function(userid, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			kra.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'kra not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading kra'});
	});
}

var getallkra = function(userid, callback)   {
	var kra  = dbConn.collection('kra');

	kra.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'kra not found'});
		else    {
			callback(false,o);
		}
	});
}

var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

