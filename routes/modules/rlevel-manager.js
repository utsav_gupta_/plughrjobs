if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPrlevel = function(coid, userid, callback)   {
	var rlevel  = dbConn.collection(coid+'_rlevel');

	rlevel.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			rlevel.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'rlevel not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading rlevel'});
	});
};
exports.getNPrlevel = function(coid, userid, pageno, callback)   {
	var rlevel  = dbConn.collection(coid+'_rlevel');

	rlevel.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'rlevel not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallrlevel = function(coid, userid, callback)   {
	var rlevel  = dbConn.collection(coid+'_rlevel');

	rlevel.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'rlevel not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getcount = function(coid, userid, callback)   {
	var rlevel  = dbConn.collection(coid+'_rlevel');

	rlevel.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Performance ratings not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.addnewrlevel = function(newData, callback)   {
	var rlevel  = dbConn.collection(newData.coid+'_rlevel');

	newData.date = new Date();
	newData.rdesc = newData.rdesc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	rlevel.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'rlevel not added'});
		else   {
			getFPrlevel(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table rlevel data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updaterlevel = function(newData, callback)   {
	var rlevel  = dbConn.collection(newData.coid+'_rlevel');

	rlevel.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.rlevel = newData.rlevel;
			o.rdesc = newData.rdesc;
			delete o.coid;
			rlevel.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'rlevel not updated'});
				else   {
					getFPrlevel(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table rlevel data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in rlevel'});
		}
	});
}
exports.deleterlevel = function(newData, callback)   {
	var rlevel  = dbConn.collection(newData.coid+'_rlevel');

	rlevel.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'rlevel data not deleted'});
		else   {
			getFPrlevel(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table rlevel data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPrlevel = function(coid, userid, callback)   {
	var rlevel  = dbConn.collection(coid+'_rlevel');

	rlevel.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			rlevel.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'rlevel not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading rlevel'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

