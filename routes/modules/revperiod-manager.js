if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPrevperiod = function(coid, userid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			revperiod.find({}).sort({_id:-1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'revperiod not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading revperiod'});
	});
};

exports.getNPrevperiod = function(coid, userid, pageno, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({}).sort({_id:-1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revperiod not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getrptitle = function(coid, rpid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({_id: getObjectId(rpid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revperiod not found'});
		else    {
			//console.log(o);
			callback(false,o);
		}
	});
}

exports.getrevperiodbyid = function(coid, rpid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({_id: getObjectId(rpid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revperiod not found'});
		else    {
			//console.log(o);
			callback(false,o);
		}
	});
}

exports.getallrevperiod = function(coid, userid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({}).sort({_id:-1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revperiod not found'});
		else    {
			//console.log(o);
			callback(false,o);
		}
	});
}

exports.getclosedrperiod = function(coid, userid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({status:'4'}).sort({_id:-1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'revperiod not found'});
		else    {
			//console.log(o);
			callback(false,o);
		}
	});
}

exports.addnewrevperiod = function(newData, callback)   {
	var revperiod  = dbConn.collection(newData.coid+'_revperiod');

	newData.date = new Date();
	//newData.sdate = newData.sdate;
	//newData.edate = newData.edate;
	newData.status = newData.status;
	newData.frozen = newData.frozen;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	revperiod.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'revperiod not added'});
		else   {
			getFPrevperiod(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table revperiod data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updaterevperiod = function(newData, callback)   {
	var revperiod  = dbConn.collection(newData.coid+'_revperiod');

	revperiod.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.status = newData.status;
			o.frating = newData.frating;
			revperiod.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'revperiod not updated'});
				else   {
					getFPrevperiod(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table revperiod data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in revperiod'});
		}
	});
}
exports.updstatus = function(newData, callback)   {
	var revperiod  = dbConn.collection(newData.coid+'_revperiod');

	revperiod.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.status = newData.status;
			revperiod.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'revperiod not updated'});
				else   {
					callback(true,{err:2,text:'Table revperiod data updated'});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in revperiod'});
		}
	});
}
exports.deleterevperiod = function(newData, callback)   {
	var revperiod  = dbConn.collection(newData.coid+'_revperiod');

	revperiod.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'revperiod data not deleted'});
		else   {
			getFPrevperiod(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table revperiod data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPrevperiod = function(coid, userid, callback)   {
	var revperiod  = dbConn.collection(coid+'_revperiod');

	revperiod.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			revperiod.find({}).sort({_id:-1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'revperiod not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading revperiod'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}


