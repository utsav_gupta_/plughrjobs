exports.getonelboard = function(coid, userid, callback)   {
	var lboard  = dbConn.collection(coid+'_lboard');

	lboard.findOne({userid:userid }, function(e, o)   {
		if (e)
			callback(true,{err:1,text:'lboard not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.updatelboard = function(newData, callback)   {
	var lboard  = dbConn.collection(newData.coid+'_lboard');

	lboard.findOne({userid:newData.userid}, function(e, o)   {
		if (o)    {
				o.msgid = newData.msgid;
				o.msgdes = newData.msgdes;
			delete o.coid;
			lboard.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'lboard not updated'});
				else
					callback(false,{err:0,text:'lboard updated'});
			});
		} else  {
			newData.msgid = newData.msgid;
			newData.msgdes = newData.msgdes;
			delete newData.coid;
			lboard.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'lboard not inserted'});
				else
					callback(false,{err:0,text:'lboard inserted'});
			});
		}
	});
}

