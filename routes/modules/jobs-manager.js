if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPjobs = function(userid, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({companyadmin:userid}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			jobs.find({companyadmin:userid}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'jobs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading jobs'});
	});
};

exports.getNPjobs = function(userid, pageno, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({companyadmin:userid}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'jobs not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.searchjobs = function(stext, jobcntry, jobcity, callback)   {
  var query = {};
  var qry1 = {};
	var dlen = 0;

	//console.log(stext);
	//console.log(jobcntry);
	//console.log(jobcity);

  query["$and"]=[];
  if (stext != '') {
	  var query1 = { $or: [ {"title" : {$regex : ".*"+ stext +".*", "$options" : "-i"}},{"jdesc" : {$regex : ".*"+ stext +".*", "$options" : "-i"}}]};

	  query["$and"].push(query1);
  }
  if (jobcntry != '')    {
    query["$and"].push({"jobcntry" : {$regex : ".*"+ jobcntry +".*", "$options" : "-i"}});
  }
  if (jobcity != '')    {
    query["$and"].push({"jobcity" : {$regex : ".*"+ jobcity +".*", "$options" : "-i"}});
  }
	//console.log(JSON.stringify(query));

	var jobs  = dbConn.collection('jobs');
  jobs.find(query).sort( { _id:-1 } ).toArray(function(e1,o1) {
    if (e1) {
      //console.log('no data');
	    callback(true,{err:1,text:'no jobs data'});
    } else  {
      //console.log(o1);
      //callback(false,res,tot,searchData);
      callback(false,o1);
    }
  });
}

exports.getcompanyjobs = function(userid, callback)   {
	var jobs  = dbConn.collection('jobs');
	jobs.find({companyadmin:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'jobs not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getjobbyid = function(jobid, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({_id: getObjectId(jobid)}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'jobs not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getalljobs = function(userid, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({companyadmin:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'jobs not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getcount = function(userid, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({companyadmin:userid}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'jobss not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.addnewjobs = function(newData, callback)   {
	var jobs  = dbConn.collection('jobs');

	newData.date = new Date();
	var suserid = newData.companyadmin;

	jobs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'jobs not added'});
		else   {
			getFPjobs(suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table jobs data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatejobs = function(newData, callback)   {
	var jobs  = dbConn.collection('jobs');

	var suserid = newData.companyadmin;
	jobs.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.company = newData.company;
			o.jdesc = newData.jdesc;
			o.jobcntry = newData.jobcntry;
			o.jobcity = newData.jobcity;
			o.locale = newData.locale;
			o.emailid = newData.emailid;
			o.workdays = newData.workdays;
			o.expr = newData.expr;
			o.skills = newData.skills;
			o.comp = newData.comp;
			o.curr = newData.curr;
			o.eqtymin = newData.eqtymin;
			o.eqtymax = newData.eqtymax;
			jobs.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'jobs not updated'});
				else   {
					getFPjobs(suserid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table jobs data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in jobs'});
		}
	});
}
exports.deletejobs = function(newData, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'jobs data not deleted'});
		else   {
			getFPjobs(newData.companyadmin, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table jobs data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPjobs = function(userid, callback)   {
	var jobs  = dbConn.collection('jobs');

	jobs.find({companyadmin:userid}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			jobs.find({companyadmin:userid}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'jobs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading jobs'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}


