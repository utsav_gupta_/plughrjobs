if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPsalcomp = function(coid, userid, callback)   {
	var salcomp  = dbConn.collection(coid+'_salcomp');

	salcomp.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salcomp.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salcomp not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salcomp'});
	});
};
exports.getNPsalcomp = function(coid, userid, pageno, callback)   {
	var salcomp  = dbConn.collection(coid+'_salcomp');

	salcomp.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salcomp not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallsalcomp = function(coid, userid, callback)   {
	var salcomp  = dbConn.collection(coid+'_salcomp');

	salcomp.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'salcomp not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewsalcomp = function(newData, callback)   {
	var salcomp  = dbConn.collection(newData.coid+'_salcomp');

	newData.date = new Date();
	newData.isbasic = newData.isbasic;
	newData.desc = newData.desc;
	newData.sctype = newData.sctype;
	newData.calctype = newData.calctype;
	newData.calcval = newData.calcval;
	newData.ymax = newData.ymax;
	newData.catg = newData.catg;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	salcomp.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'salcomp not added'});
		else   {
			getFPsalcomp(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table salcomp data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatesalcomp = function(newData, callback)   {
	var salcomp  = dbConn.collection(newData.coid+'_salcomp');

	salcomp.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.isbasic = newData.isbasic;
			o.desc = newData.desc;
			o.sctype = newData.sctype;
			o.calctype = newData.calctype;
			o.calcval = newData.calcval;
			o.ymax = newData.ymax;
			o.catg = newData.catg;
			delete o.coid;
			salcomp.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'salcomp not updated'});
				else   {
					getFPsalcomp(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table salcomp data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in salcomp'});
		}
	});
}
exports.deletesalcomp = function(newData, callback)   {
	var salcomp  = dbConn.collection(newData.coid+'_salcomp');

	salcomp.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'salcomp data not deleted'});
		else   {
			getFPsalcomp(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table salcomp data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPsalcomp = function(coid, userid, callback)   {
	var salcomp  = dbConn.collection(coid+'_salcomp');

	salcomp.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			salcomp.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'salcomp not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading salcomp'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

