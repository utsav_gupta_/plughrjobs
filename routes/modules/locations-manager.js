if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPlocations = function(coid, userid, callback)   {
	var locations  = dbConn.collection(coid+'_locations');

	locations.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			locations.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'locations not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading locations'});
	});
};
exports.getNPlocations = function(coid, userid, pageno, callback)   {
	var locations  = dbConn.collection(coid+'_locations');

	locations.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'locations not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalllocations = function(coid, userid, callback)   {
	var locations  = dbConn.collection(coid+'_locations');

	locations.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'locations not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getcount = function(coid, userid, callback)   {
	var locations  = dbConn.collection(coid+'_locations');

	locations.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'locations not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.addnewlocations = function(newData, callback)   {
	var locations  = dbConn.collection(newData.coid+'_locations');

	newData.date = new Date();
	newData.aline1 = newData.aline1;
	newData.aline2 = newData.aline2;
	newData.city = newData.city;
	newData.pincode = newData.pincode;
	newData.phone1 = newData.phone1;
	newData.phone2 = newData.phone2;
	newData.faxno = newData.faxno;
	newData.cperson = newData.cperson;
	newData.chq = newData.chq;
	newData.ptno = newData.ptno;
	newData.se = newData.se;

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	locations.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'locations not added'});
		else   {
			getFPlocations(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table locations data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatelocations = function(newData, callback)   {
	var locations  = dbConn.collection(newData.coid+'_locations');

	locations.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.oname = newData.oname;
			o.aline1 = newData.aline1;
			o.aline2 = newData.aline2;
			o.city = newData.city;
			o.pincode = newData.pincode;
			o.phone1 = newData.phone1;
			o.phone2 = newData.phone2;
			o.faxno = newData.faxno;
			o.cperson = newData.cperson;
			o.chq = newData.chq;
			o.ptno = newData.ptno;
			o.se = newData.se;

			delete o.coid;
			locations.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'locations not updated'});
				else   {
					getFPlocations(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table locations data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in locations'});
		}
	});
}
exports.deletelocations = function(newData, callback)   {
	var locations  = dbConn.collection(newData.coid+'_locations');

	locations.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'locations data not deleted'});
		else   {
			getFPlocations(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table locations data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPlocations = function(coid, userid, callback)   {
	var locations  = dbConn.collection(coid+'_locations');

	locations.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			locations.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'locations not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading locations'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

