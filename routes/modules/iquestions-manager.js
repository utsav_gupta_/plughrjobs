if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPiquestions = function(coid, userid, callback)   {
	var iquestions  = dbConn.collection(coid+'_iquestions');

	iquestions.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);

			iquestions.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'iquestions not found'});
				else
					callback(false,o, tot, o1);
			});
		} else
			callback(true,{err:2,text:'Error in reading iquestions'});
	});
};
exports.getNPiquestions = function(coid, userid, pageno, callback)   {
	var iquestions  = dbConn.collection(coid+'_iquestions');

	iquestions.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'iquestions not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getalliquestions = function(coid, userid, callback)   {
	var iquestions  = dbConn.collection(coid+'_iquestions');

	iquestions.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);

			iquestions.find({}).sort({_id:1}).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'iquestions not found'});
				else
					callback(false,o, tot, o1);
			});
		} else
			callback(true,{err:2,text:'Error in reading iquestions'});
	});
}
exports.getcount = function(coid, userid, callback)   {
	var iquestions  = dbConn.collection(coid+'_iquestions');

	iquestions.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Induction questions not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.addnewiquestions = function(newData, callback)   {
	var iquestions  = dbConn.collection(newData.coid+'_iquestions');

	newData.date = new Date();
	newData.a1text = newData.a1text;
	newData.a2text = newData.a2text;
	newData.a3text = newData.a3text;
	newData.a4text = newData.a4text;
	newData.answer = newData.answer;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	iquestions.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'iquestions not added'});
		else   {
			getFPiquestions(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table iquestions data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateiquestions = function(newData, callback)   {
	var iquestions  = dbConn.collection(newData.coid+'_iquestions');

	iquestions.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.qtext = newData.qtext;
			o.a1text = newData.a1text;
			o.a2text = newData.a2text;
			o.a3text = newData.a3text;
			o.a4text = newData.a4text;
			o.answer = newData.answer;
			delete o.coid;
			iquestions.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'iquestions not updated'});
				else   {
					getFPiquestions(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table iquestions data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in iquestions'});
		}
	});
}
exports.deleteiquestions = function(newData, callback)   {
	var iquestions  = dbConn.collection(newData.coid+'_iquestions');

	iquestions.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'iquestions data not deleted'});
		else   {
			getFPiquestions(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table iquestions data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPiquestions = function(coid, userid, callback)   {
	var iquestions  = dbConn.collection(coid+'_iquestions');

	iquestions.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			iquestions.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'iquestions not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading iquestions'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

