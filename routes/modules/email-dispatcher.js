var sendgrid  = require('sendgrid')("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");

var EM = {};
module.exports = EM;

EM.NewEmployee = function(userdata, callback)
{
  if (!userdata)   {
		console.log('Invalid data supplied to New Employee email module');
		callback(true,null);
  }
  if (userdata.useremailid != "" && userdata.colive == true)		{
		var email     = new sendgrid.Email();
		var htmltext  = "<h1>workhalf</h1>";
		    htmltext += "<hr>";
		    htmltext += "<p>Dear "+ userdata.username + ",";
		    htmltext += "</p>";
		    htmltext += "<br>";
		    htmltext += "<p>You have been given login access to workhalf by your employer</p>";
		    htmltext += "<hr>";
      	htmltext += "<p> Please click <a href='http://www.workhalf.com/'>here</a> access workhalf</p>";
		    htmltext += "<hr>";
		    htmltext += "<p>Please find below your login details</p>";
		    htmltext += "<br>";
		    htmltext += "<p> Your Company ID : ";
		    htmltext += userdata.coid;
		    htmltext += "<p> Your User ID : ";
		    htmltext += userdata.userid;
		    htmltext += "</p>";
		    htmltext += "<p> Your Password : ";
		    htmltext += userdata.password;
		    htmltext += "</p>";
		    htmltext += "<br>";
		    htmltext += "<p>Please login with this password and <b>change this password Immediately</b></p>";
		    htmltext += "<br>";
		    htmltext += "<p>Do more today!";
		    htmltext += "</p>";
		    htmltext += "<br>";
		    htmltext += "<p>Your's Sincerely</p>";
		    htmltext += "<br>";
		    htmltext += "<br>";
		    htmltext += "<p><b>workhalf Team</b></p>";

		email.addTo(userdata.useremailid);
		email.setFrom('no-reply@workhalf.com');
		email.setFromName('workhalf Notification');
		email.setSubject('workhalf - Employee Login access');
		email.setHtml(htmltext);
		sendgrid.send(email, function(err, result) {
			if (err) {
		    console.log('A SendGrid error occurred: ' + err);
		    callback(err, null);
		  }	else
		    callback(null,result);
		});
	} else	{
		console.log("user has no email id or Company is not yet live");
    callback(true, "no email id");
	}
}

EM.resetpwd = function(userdata, callback)
{
  if (!userdata)   {
      console.log('Invalid user data supplied to Password Reset email module');
      callback(true,null);
  }
  if (userdata.useremailid != "" && userdata.live == false)		{
		var email     = new sendgrid.Email();
		var htmltext  = "<h1>workhalf</h1>";
		    htmltext += "<hr>";
		    htmltext += "<h3>Your password has been reset by your employer.</h3>";
		    htmltext += "<hr>";
		    htmltext += "<h3>Please login with new password and change this new password immediately</h3>";
		    htmltext += "<br>";
		    htmltext += "<p> Your new password is : ";
		    htmltext += userdata.password;
		    htmltext += "</p>";
		    htmltext += "<hr>";
		    htmltext += "<p>Your's Sincerely</p>";
		    htmltext += "<br>";
		    htmltext += "<br>";
		    htmltext += "<p><b>workhalf Team</b></p>";

		email.addTo(userdata.useremailid);
		email.setFrom('no-reply@workhalf.com');
		email.setFromName('workhalf Notification');
		email.setSubject('workhalf user password reset request');
		email.setHtml(htmltext);
		sendgrid.send(email, function(err, result) {
			if (err) {
		    console.log('A SendGrid error occurred: ' + err);
		    callback(err, null);
		  }	else
		    callback(null,result);
		});
	} else	{
		console.log("user has no email id or company is not yet live");
    callback(true, "no email id");
	}
}

EM.LiveEmployee = function(userdata, callback)		{
  if (!userdata)   {
		console.log('Invalid data supplied to Live Employee email module');
		callback(true,null);
  }
	var email     = new sendgrid.Email();
	var htmltext  = "<h1>workhalf</h1>";
	    htmltext += "<hr>";
	    htmltext += "<p>Dear "+ userdata.username + ",";
	    htmltext += "</p>";
	    htmltext += "<br>";
	    htmltext += "<p>You have been given login access to workhalf by your employer</p>";
	    htmltext += "<hr>";
	    htmltext += "<p>Please find below your login details</p>";
	    htmltext += "<br>";
	    htmltext += "<p> Your Company ID : ";
	    htmltext += userdata.coid;
	    htmltext += "<p> Your User ID : ";
	    htmltext += userdata.userid;
	    htmltext += "</p>";
	    htmltext += "<p> Your Password : ";
	    htmltext += userdata.coid+"abc";
	    htmltext += "</p>";
	    htmltext += "<br>";
	    htmltext += "<p><b>When you login with this password, you will be promted to change password.</b></p>";
	    htmltext += "<p>After changing password, you have to login again with same companyid & userid provided above, but with new password</p>";
	    htmltext += "<br>";
	    htmltext += "<p>Do more today!";
	    htmltext += "</p>";
	    htmltext += "<br>";
	    htmltext += "<p>Your's Sincerely</p>";
	    htmltext += "<br>";
	    htmltext += "<br>";
	    htmltext += "<p><b>workhalf Team</b></p>";

	email.addTo(userdata.useremailid);
	email.setFrom('no-reply@workhalf.com');
	email.setFromName('workhalf Notification');
	email.setSubject('workhalf - Employee Login access');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
	    console.log('A SendGrid error occurred: ' + err);
	    callback(err, null);
	  }	else
	    callback(null,result);
	});
}

EM.newTrainee = function(codata, callback)			{
	//console.log(codata);
  if (!codata)   {
      console.log('Invalid data supplied to newTrainee request email module');
      callback(400);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1> New request for training</h1>";
      htmltext += "<hr>";
      htmltext += "<p>Dear "+ codata.cuser.username +",";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p> Thank you for signing for training requirement with plugHR, your human resource performance partner";
      htmltext += "</p> You have provided the following details";
      htmltext += "<br>";
      htmltext += "<p> Company : " + codata.cuser.coid;
      htmltext += "<p> training : " + codata.training;
      htmltext += "<p> trainees : " + codata.trainees;
      htmltext += "<p> place : " + codata.place;
      htmltext += "<p> date : " + codata.date;
      htmltext += "<p> expect : " + codata.expect;      
      htmltext += "</p>";
      htmltext += "<p> We will reach out to you very soon";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p>Do more today!";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>plugHR Team</b></p>";

	email.addTo(codata.emailid);
	//email.addTo('prashant@karera.co');
	email.setFrom('no-reply@plugHR.com');
	email.setFromName('plugHR Notification');
	email.setSubject('Training request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.newContract = function(codata, callback)			{
	//console.log(codata);
  if (!codata)   {
      console.log('Invalid data supplied to newContract request email module');
      callback(400);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1> New request for contract staffing</h1>";
      htmltext += "<hr>";
      htmltext += "<p>Dear "+ codata.cuser.username +",";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p> Thank you for signing for staffing requirement with plugHR, your human resource performance partner";
      htmltext += "</p> You have provided the following details";
      htmltext += "<br>";
      htmltext += "<p> Company : " + codata.cuser.coid;
      htmltext += "<p> role : " + codata.role;
      htmltext += "<p> location : " + codata.location;
      htmltext += "<p> tenure : " + codata.tenure;
      htmltext += "<p> fees : " + codata.fees;
      htmltext += "<p> totpersons : " + codata.totpersons;      
      htmltext += "</p>";
      htmltext += "<p> We will reach out to you very soon";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p>Do more today!";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>plugHR Team</b></p>";

	email.addTo(codata.emailid);
	//email.addTo('prashant@karera.co');
	email.setFrom('no-reply@plugHR.com');
	email.setFromName('plugHR Notification');
	email.setSubject('Staffing request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.newJob = function(codata, callback)			{
	//console.log(codata);
  if (!codata)   {
      console.log('Invalid data supplied to newJob request email module');
      callback(400);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1> New Job Posting request</h1>";
      htmltext += "<hr>";
      htmltext += "<p>Dear "+ codata.cuser.username +",";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p> Thank you for signing for recruitment requirement with plugHR, your human resource performance partner";
      htmltext += "</p> You have provided the following details";
      htmltext += "<br>";
      htmltext += "<p> Company : " + codata.cuser.coid;
      htmltext += "<p> title : " + codata.title;
      htmltext += "<p> jobdesc : " + codata.jobdesc;
      htmltext += "<p> jobloc : " + codata.jobloc;
      htmltext += "<p> jobtype : " + codata.jobtype;
      htmltext += "<p> comp : " + codata.comp;      
      htmltext += "<p> stock : " + codata.stock;      
      htmltext += "<p> offer : " + codata.offer;      
      htmltext += "<p> leave : " + codata.leave;                  
      htmltext += "<p> profile : " + codata.profile;      
      htmltext += "<p> jobpost : " + codata.jobpost;
      htmltext += "<p> agent : " + codata.agent;
      htmltext += "<p> jobfee : " + codata.jobfee;            
      htmltext += "</p>";
      htmltext += "<p> We will reach out to you very soon";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p>Do more today!";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>plugHR Team</b></p>";

	email.addTo(codata.emailid);
	//email.addTo('prashant@karera.co');
	email.setFrom('no-reply@plugHR.com');
	email.setFromName('plugHR Notification');
	email.setSubject('Recruitment request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.notifs = function(notifdata, callback)			{
	//console.log(notifdata);
	var email     = new sendgrid.Email();
  var htmltext  = "<h1> workhalf Notifications </h1>";
      htmltext += "<hr>";
      htmltext += "<p>"+notifdata.mesg;
      htmltext += "</p>";
      htmltext += "<br>";
      //htmltext += "<p> Company ID : "+notifdata.companyid+" </p>";
      //htmltext += "<p> Email to   : "+notifdata.mailid+" </p>";
      htmltext += "<br>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>plugHR Team</b></p>";

	email.addTo(notifdata.mailid);
	email.setFrom('no-reply@plugHR.com');
	email.setFromName('plugHR Notification');
	email.setSubject("Message from workhalf");
	//email.setSubject("Message from workhalf"+notifdata.companyid+" "+notifdata.mailid);
	email.setHtml(htmltext);

  //console.log(notifdata.companyid + " - " + notifdata.mailid + " - " + notifdata.mesg);

	sendgrid.send(email, function(err, result) {
		email = null;
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.newCandRequest = function(userdata, callback)    {
  if (!userdata)   {
      console.log('Invalid Candidate data supplied to newStuRequest email module');
      callback(true,null);
  }
  var htmltext  = "<h1> Welcome to Workhalf</h1>";
      htmltext += "<hr>";
      htmltext += "<h3>Your request to join Workhalf platform has been received.</h3>";
      htmltext += "<hr>";
      htmltext += "<h3>You have to verify your email id to begin using Workhalf</h3>";
      htmltext += "<br>";
      htmltext += "<p>Please click on this <a href='www.workhalf.com/emailverify?verid="+userdata.verid+"'>link</a> to verify and activate your email id </p>";
      htmltext += "<p> Alternatively you can also go to <a href='http://www.workhalf.com/emailverify'>Workhalf website</a> and manually verify your emailid</p>";
      htmltext += "<br>";
      htmltext += "<p> Your activation code = ";
      htmltext += userdata.verid;
      htmltext += "</p>";
      htmltext += "<hr>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>Team Workhalf</b></p>";

	var email = new sendgrid.Email();
	email.addTo(userdata.user);
	email.setFrom('no-reply@workhalf.com');
	email.setFromName('Workhalf Notification');
	email.setSubject('Workhalf - Candidate profile creation request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
	    console.log('A SendGrid error occurred: ' + err);
	    callback(err, null);
	  }	else
	    callback(null,result);
	});
}

EM.resendverid = function(userdata, callback)   {
  if (!userdata)   {
      console.log('Invalid user data supplied to resendverid email module');
      callback(true,null);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1>Workhalf</h1>";
      htmltext += "<hr>";
      htmltext += "<h3>We received your request to resend verification code.</h3>";
      htmltext += "<hr>";
      htmltext += "<h3>You have to verify your email id to begin using Workhalf</h3>";
      htmltext += "<br>";
      htmltext += "<p> Please click on this <a href='http://www.workhalf.com/emailverify?verid="+userdata.verid+"'>link.</a> to verify and activate your email id </p>";
      htmltext += "<p> Alternatively you can also go to <a href='http://www.workhalf.com/emailverify'>Workhalf website</a> and manually verify your emailid</p>";
      htmltext += "<br>";
      htmltext += "<p> Your activation code = ";
      htmltext += userdata.verid;
      htmltext += "</p>";
      htmltext += "<hr>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>Team Workhalf</b></p>";

	email.addTo(userdata.user);
	email.setFrom('no-reply@workhalf.com');
	email.setFromName('Workhalf Notification');
	email.setSubject('Workhalf resend verification mail');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.newCORequest = function(codata, oldpass, callback)			{
  if (!codata)   {
      console.log('Invalid company data supplied to newCoRequest email module');
      callback(400);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1> Verify your workhalf account creation</h1>";
      htmltext += "<hr>";
      htmltext += "<p>Dear "+ codata.name +",";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p> Thank you for signing up on workhalf. Please activate your account by clicking on the verification link provided in this email.";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p> Here is your verification <a href='http://www.workhalf.com/emailverify?verid="+codata.verid+"'>link.</a></p>";
      htmltext += "<p> Alternatively you can also go to <a href='http://www.workhalf.com/emailverify'>Workhalf website</a> and manually verify your emailid</p>";
      htmltext += "<br>";
      htmltext += "<p> Your activation code = ";
      htmltext += codata.verid;
      htmltext += "</p>";
      htmltext += "<hr>";
      htmltext += "<p>Your's Sincerely</p>";
      htmltext += "<br>";
      htmltext += "<br>";
      htmltext += "<p><b>Team Workhalf</b></p>";

	email.addTo(codata.user);
	email.setFrom('no-reply@workhalf.com');
	email.setFromName('workhalf Notification');
	email.setSubject('Workhalf New membership request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

EM.newCOActivate = function(newData, callback)    {
  if (!newData)   {
      console.log('Invalid company data supplied to newCoActivate email module');
      callback(400);
  }
	var email     = new sendgrid.Email();
  var htmltext  = "<h1>Your workhalf account activated</h1>";
      htmltext += "<hr>";
      htmltext += "<p>Dear "+ newData.name +",";"</p>";
      htmltext += "<br>";
      htmltext += "<p>Your account is activated and good to go. Use the following credentials to login, you can then complete company setup, Post Jobs and start recruiting candidates.";"</p>";
      htmltext += "<br>";
      htmltext += "<p><b>1. User Id:</b> "+ newData.user +"";
      htmltext += "<br>";
      htmltext += "<p><b>2. Password:</b> "+ newData.password +"";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p>We welcome you to the growing family of workhalf users. For any support need, write a mail to helpdesk@workhalf.com and we'd reach out to you at the earliest.</p>";
      htmltext += "<br>";
      htmltext += "<p>Do more today!";
      htmltext += "</p>";
      htmltext += "<br>";
      htmltext += "<p><b>workhalf Team</b></p>";

	email.addTo(newData.user);
	email.setFrom('no-reply@workhalf.com');
	email.setFromName('Workhalf Notification');
	email.setSubject('Workhalf New membership request');
	email.setHtml(htmltext);
	sendgrid.send(email, function(err, result) {
		if (err) {
      console.log('A SendGrid error occurred: ' + err);
      callback(err, null);
    }	else
      callback(null,result);
  });
}

