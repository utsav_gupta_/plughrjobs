if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPleavetypes = function(coid, userid, callback)   {
	var leavetypes  = dbConn.collection(coid+'_leavetypes');

	leavetypes.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			leavetypes.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'leavetypes not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading leavetypes'});
	});
};
exports.getNPleavetypes = function(coid, userid, pageno, callback)   {
	var leavetypes  = dbConn.collection(coid+'_leavetypes');

	leavetypes.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'leavetypes not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallleavetypes = function(coid, userid, callback)   {
	var leavetypes  = dbConn.collection(coid+'_leavetypes');

	leavetypes.find({}).sort({_id:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'leavetypes not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getcount = function(coid, userid, callback)   {
	var leavetypes  = dbConn.collection(coid+'_leavetypes');

	leavetypes.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Leave types not found'});
		else    {
			callback(false,o1);
		}
	});
}

exports.addnewleavetypes = function(newData, callback)   {
	var leavetypes  = dbConn.collection(newData.coid+'_leavetypes');

	newData.date = new Date();
	newData.lname = newData.lname;
	newData.lper = newData.lper;
	newData.ldays = newData.ldays;
	newData.lcriteria = newData.lcriteria;
	newData.etype = newData.etype;
	newData.puser = newData.puser;
	newData.maxl = newData.maxl;
	newData.negl = newData.negl;
	newData.alimit = newData.alimit;
	newData.maxtr = newData.maxtr;
	newData.preh = newData.preh;
	newData.inth = newData.inth;
	newData.such = newData.such;

	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	leavetypes.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'leavetypes not added'});
		else   {
			getFPleavetypes(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table leavetypes data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateleavetypes = function(newData, callback)   {
	var leavetypes  = dbConn.collection(newData.coid+'_leavetypes');

	leavetypes.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
	    o.lname = newData.lname;
	    o.lper = newData.lper;
	    o.ldays = newData.ldays;
	    o.lcriteria = newData.lcriteria;
	    o.etype = newData.etype;
	    o.puser = newData.puser;
	    o.maxl = newData.maxl;
	    o.negl = newData.negl;
	    o.alimit = newData.alimit;
	    o.maxtr = newData.maxtr;
	    o.preh = newData.preh;
	    o.inth = newData.inth;
	    o.such = newData.such;
			delete o.coid;
			leavetypes.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'leavetypes not updated'});
				else   {
					getFPleavetypes(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table leavetypes data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in leavetypes'});
		}
	});
}

exports.deleteleavetypes = function(newData, callback)   {
	var leavetypes  = dbConn.collection(newData.coid+'_leavetypes');

	leavetypes.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'leavetypes data not deleted'});
		else   {
			getFPleavetypes(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table leavetypes data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPleavetypes = function(coid, userid, callback)   {
	var leavetypes  = dbConn.collection(coid+'_leavetypes');

	leavetypes.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			leavetypes.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'leavetypes not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading leavetypes'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

