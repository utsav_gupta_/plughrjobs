if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({userid:userid}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userpips.find({userid:userid}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'PIP info not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'PIP info not found'});
	});
};

exports.getNPuserpips = function(coid, userid, pageno, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({userid:userid}).sort({_id:1}).skip((pageno-1) * NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userpips not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getalluserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.distinct("userid").toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No users currently on PIP'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallpipusers = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'userpips not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getpipbyuser = function(searchData, callback)   {
	var userpips  = dbConn.collection(searchData.coid+'_userpips');

	userpips.find({userid:(searchData.userid).toString()}).toArray(function(e, o)   {
		if (e)		{
			callback(true,{err:1,text:'PIP data not found'});
		}	else    {
			callback(false,o);
		}
	});
}

exports.addnewuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	newData.scomm = "";
	newData.mcomm = "";
	newData.status = 0;				// 0 = open  1=closed
	newData.date = new Date();
	
	var scoid = newData.coid;
	var suserid = newData.userid;
	delete newData.coid;
	delete newData.dbid;

	userpips.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'PIP data not added'});
		else   {
			getFPuserpips(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'PIP data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.updateuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.obj = newData.obj;
			o.meets = newData.meets;
			o.sdate = newData.sdate;
			o.edate = newData.edate;

			userpips.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'userpips not updated'});
				else   {
					getFPuserpips(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table userpips data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userpips'});
		}
	});
}

exports.updateselfcomm = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.scomm = newData.scomm;
			userpips.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Self Rating not updated'});
				else 
					callback(true,{err:2,text:'Self Rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userpips'});
		}
	});
}

exports.updatemgrcomm = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.mcomm = newData.mcomm;
			userpips.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Manager Rating not updated'});
				else 
					callback(true,{err:2,text:'Manager Rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userpips'});
		}
	});
}

exports.freezepip = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.status = '1';
			userpips.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'PIP status not updated'});
				else 
					callback(true,{err:2,text:'PIP status rating updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in userpips'});
		}
	});
}


exports.submitgoals = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');
	userpips.update( {userid:newData.userid.toString(), rperiod:newData.rperiod}, {$set:{submitted:"1"}}, {multi: true},  function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Performance progress not submitted'});
		else 
			callback(true,{err:0,text:'Performance progress submitted'});
	});
}

exports.deleteuserpips = function(newData, callback)   {
	var userpips  = dbConn.collection(newData.coid+'_userpips');

	userpips.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userpips data not deleted'});
		else   {
			getFPuserpips(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table userpips data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPuserpips = function(coid, userid, callback)   {
	var userpips  = dbConn.collection(coid+'_userpips');

	userpips.find({userid:userid}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userpips.find({userid:userid}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'userpips not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userpips'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

