if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPcvalues = function(coid, userid, callback)   {
	var cvalues  = dbConn.collection(coid+'_cvalues');

	cvalues.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			cvalues.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'cvalues not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading cvalues'});
	});
};
exports.getNPcvalues = function(coid, userid, pageno, callback)   {
	var cvalues  = dbConn.collection(coid+'_cvalues');

	cvalues.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'cvalues not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallcvalues = function(coid, userid, callback)   {
	var cvalues  = dbConn.collection(coid+'_cvalues');

	cvalues.find().toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'cvalues not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getcount = function(coid, userid, callback)   {
	var cvalues  = dbConn.collection(coid+'_cvalues');

	cvalues.find({}).count(function(e1, o1)   {
		if (e1)
			callback(true,{err:1,text:'Core values not found'});
		else    {
			callback(false,o1);
		}
	});
}
exports.addnewcvalues = function(newData, callback)   {
	var cvalues  = dbConn.collection(newData.coid+'_cvalues');

	newData.date = new Date();
	newData.vdesc = newData.vdesc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	cvalues.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'cvalues not added'});
		else   {
			getFPcvalues(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table cvalues data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.updatecvalues = function(newData, callback)   {
	var cvalues  = dbConn.collection(newData.coid+'_cvalues');

	cvalues.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.vtitle = newData.vtitle;
			o.vdesc = newData.vdesc;
			delete o.coid;
			cvalues.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'cvalues not updated'});
				else   {
					getFPcvalues(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table cvalues data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in cvalues'});
		}
	});
}
exports.deletecvalues = function(newData, callback)   {
	var cvalues  = dbConn.collection(newData.coid+'_cvalues');

	cvalues.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'cvalues data not deleted'});
		else   {
			getFPcvalues(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table cvalues data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPcvalues = function(coid, userid, callback)   {
	var cvalues  = dbConn.collection(coid+'_cvalues');

	cvalues.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			cvalues.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'cvalues not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading cvalues'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

