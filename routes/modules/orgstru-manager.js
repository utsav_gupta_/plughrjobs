if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPorgstru = function(coid, userid, callback)   {
	var orgstru  = dbConn.collection(coid+'_orgstru');

	orgstru.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			orgstru.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'orgstru not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading orgstru'});
	});
};
exports.getNPorgstru = function(coid, userid, pageno, callback)   {
	var orgstru  = dbConn.collection(coid+'_orgstru');

	orgstru.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'orgstru not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.getallorgstru = function(coid, userid, callback)   {
	var orgstru  = dbConn.collection(coid+'_orgstru');

	orgstru.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'Orgstru not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.addneworgstru = function(newData, callback)   {
	var orgstru  = dbConn.collection(newData.coid+'_orgstru');

	var suserid = newData.userid;
	newData.userid = parseInt(suserid);
	newData.active = '1';
	newData.edate = '';
	newData.date = new Date();

	var scoid = newData.coid;
	delete newData.coid;
	//delete newData.userid;

	orgstru.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'orgstru not added'});
		else
			callback(true,{err:0,text:'Table orgstru data added'});
	});
}

exports.updateorgstru = function(newData, callback)   {
	var orgstru  = dbConn.collection(newData.coid+'_orgstru');

	orgstru.findOne({userid: parseInt(newData.userid), active:'1'}, function(e, o)   {
		if (o)   {
			o.active = '0';
			o.edate = newData.sdate;
			o.updby = newData.updby;
			o.upddate = newData.upddate;
			orgstru.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'orgstru not updated'});
				else 
					callback(true,{err:2,text:'Table orgstru data updated'});
			});
		} else  {
			callback(true,{err:2,text:' ID not found in orgstru'});
		}
	});
}
exports.deleteorgstru = function(newData, callback)   {
	var orgstru  = dbConn.collection(newData.coid+'_orgstru');

	orgstru.remove({userid: parseInt(newData.userid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'Table orgstru data not deleted'});
		else
			callback(true,{err:2,text:'Table orgstru data deleted'});
	});
}
var getFPorgstru = function(coid, userid, callback)   {
	var orgstru  = dbConn.collection(coid+'_orgstru');

	orgstru.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			orgstru.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'orgstru not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading orgstru'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

