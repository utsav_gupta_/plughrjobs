if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPitslabs = function(coid, userid, callback)   {
	var itslabs  = dbConn.collection(coid+'_itslabs');

	itslabs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			itslabs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'itslabs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading itslabs'});
	});
};
exports.getNPitslabs = function(coid, userid, pageno, callback)   {
	var itslabs  = dbConn.collection(coid+'_itslabs');

	itslabs.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'itslabs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallitslabs = function(coid, userid, callback)   {
	var itslabs  = dbConn.collection(coid+'_itslabs');

	itslabs.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'itslabs not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewitslabs = function(newData, callback)   {
	var itslabs  = dbConn.collection(newData.coid+'_itslabs');

	newData.date = new Date();
	newData.min = newData.min;
	newData.max = newData.max;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	itslabs.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'itslabs not added'});
		else   {
			getFPitslabs(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table itslabs data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateitslabs = function(newData, callback)   {
	var itslabs  = dbConn.collection(newData.coid+'_itslabs');

	itslabs.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.taxrate = newData.taxrate;
			o.min = newData.min;
			o.max = newData.max;
			delete o.coid;
			itslabs.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'itslabs not updated'});
				else   {
					getFPitslabs(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table itslabs data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in itslabs'});
		}
	});
}
exports.deleteitslabs = function(newData, callback)   {
	var itslabs  = dbConn.collection(newData.coid+'_itslabs');

	itslabs.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'itslabs data not deleted'});
		else   {
			getFPitslabs(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table itslabs data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPitslabs = function(coid, userid, callback)   {
	var itslabs  = dbConn.collection(coid+'_itslabs');

	itslabs.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			itslabs.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'itslabs not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading itslabs'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

