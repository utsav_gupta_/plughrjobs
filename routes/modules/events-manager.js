if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPevents = function(coid, userid, callback)   {
	var events  = dbConn.collection(coid+'_events');

	events.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			events.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'events not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading events'});
	});
};
exports.getNPevents = function(coid, userid, pageno, callback)   {
	var events  = dbConn.collection(coid+'_events');

	events.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'events not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallevents = function(coid, userid, callback)   {
	var events  = dbConn.collection(coid+'_events');

	events.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'events not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewevents = function(newData, callback)   {
	var events  = dbConn.collection(newData.coid+'_events');

	newData.creationdate = new Date();
	newData.date = newData.date;
	newData.desc = newData.desc;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	events.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'events not added'});
		else   {
			getFPevents(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table events data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updateevents = function(newData, callback)   {
	var events  = dbConn.collection(newData.coid+'_events');

	events.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.title = newData.title;
			o.date = newData.date;
			o.desc = newData.desc;
			delete o.coid;
			events.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'events not updated'});
				else   {
					getFPevents(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table events data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in events'});
		}
	});
}
exports.deleteevents = function(newData, callback)   {
	var events  = dbConn.collection(newData.coid+'_events');

	events.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'events data not deleted'});
		else   {
			getFPevents(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table events data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPevents = function(coid, userid, callback)   {
	var events  = dbConn.collection(coid+'_events');

	events.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			events.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'events not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading events'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

