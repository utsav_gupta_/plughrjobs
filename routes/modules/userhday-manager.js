if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPuserhday = function(coid, userid, callback)   {
	var userhday  = dbConn.collection(coid+'_userhday');

	userhday.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			userhday.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'user hasn\'t availed any holidays yet'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading userhday'});
	});
};
exports.getNPuserhday = function(coid, userid, pageno, callback)   {
	var userhday = dbConn.collection(coid+'_userhday');

	userhday.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'No more holidays availed by this user'});
		else    {
			callback(false,o);
		}
	});
}

exports.getalluserhday = function(coid, userid, callback)   {
	var userhday  = dbConn.collection(coid+'_userhday');

	userhday.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:"user hasn\'t availed any holidays yet"});
		else    {
			callback(false,o);
		}
	});
}

var getalluserhday = function(coid, userid, callback)   {
	var userhday  = dbConn.collection(coid+'_userhday');

	userhday.find({userid:userid}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:"user hasn\'t availed any holidays yet"});
		else    {
			callback(false,o);
		}
	});
}

exports.availhday = function(newData, callback)   {
	var userhday  = dbConn.collection(newData.coid+'_userhday');

	newData.date = new Date();
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	//delete newData.userid;

	userhday.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'Unable to avail this holiday'});
		else   {
			getalluserhday(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Holiday availed'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}

exports.cancelhday = function(newData, callback)   {
	var userhday  = dbConn.collection(newData.coid+'_userhday');

	userhday.remove({userid: newData.userid, hpolicyid: newData.hpolicyid, holidayid: newData.holidayid}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'userhday data not deleted'});
		else   {
			getalluserhday(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table userhday data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

