var crypto 		= require('crypto')
var EM        = require('./email-dispatcher');
var async 		= require('async');
var LIC       = require('./licenses-manager');

var CompIDs = dbConn.collection('sz_compids');
var userIDs = dbConn.collection('sz_userids');
var deptIDs = dbConn.collection('sz_deptids');
var skillIDs = dbConn.collection('sz_skills');
var kraIDs = dbConn.collection('sz_kraids');

var preCompany = dbConn.collection('preCompany');
var preUser = dbConn.collection('preuser');
var company = dbConn.collection('company');
var user = dbConn.collection('users');

exports.newCompany = function(newData, callback)     {
	user.findOne({user:newData.user}, function(e, o) {
    if (o)      {
     	console.log("existing email");
			callback(true,{err:1,text:'Email already registered'});
		}	else	{
      preUser.findOne({user:newData.user}, function(e1, o1) {
        if (o1) {
          console.log("existing user awaiting verification");
          callback(true,{err:2,text:"Email already registered. Email verification is pending"});
        } else  {
					var oldpass = newData.password;
			   	saltAndHash(oldpass, function(hash)    {
				    newData.pass = hash;
				    newData.pwd = oldpass;
				    // append date stamp when record was created //
				    newData.date = new Date();
            newData.verid = Math.random().toString(36).slice(-16);
            //var access = 'Password : '+hash +' --- Verification Code : '+newData.verid;
            preUser.insert(newData, {safe: true}, function (e2,o2)   {
              if (o2)     {
                EM.newCORequest(newData, oldpass, function (err, op)   {
                   if (op)     {
                    //console.log("success");
               			callback(false,{err:0,text:'Account created and a verification email has been sent.'});
                   } else  {
                    //console.log("email not sent");
               			callback(false,{err:3,text:'Please check your email for Password'});
                   }
					      });
					    }
			      });
          });
	      }
      });
		}
	});
}

exports.verifycompany = function(sverid, callback)     {
  preCompany.findOne({verid:sverid}, function(e, o) {
  	if (o)      {
      var coData = new Object;
      var userData = new Object;
      var newData = new Object;

      //newData = o;
      //delete newData.verid;
			//newData.date = new Date();;

      // coData.companyid = Math.floor(Math.random()*90000) + 10000;

	    CompIDs.findOne({}, function(e, co) {
		    if (co)      {
          //console.log("company id is "+ co.compid);
          coData.companyid = co.compid;
          coData.companyname = o.company;
			    coData.creationdate = new Date();

          userData.userid = 1;
          userData.useremailid = o.user;
					if (o.usertype == "1")
						userData.owner = true;
					else
						userData.owner = false;
          userData.usertype = "2";
          userData.username = o.name;
          userData.password = o.password;
          userData.mobile = o.mobile;
			    coData.creationdate = new Date();

          newData.companyid = co.compid;
          newData.userid = 1;
          newData.password = o.pwd;
          newData.name = o.name;
          newData.user = o.user;

			    company.insert(coData, {safe: true}, function (e1,o1)   {
            if (o1)     {
              var users = dbConn.collection(co.compid+'_users');
			        users.insert(userData, {safe: true}, function (e2,o2)   {
                if (o2)     {
				          LIC.addTrialLicense(coData, function(e3,o3)    {
                    if (o3)  {
				              preCompany.remove({user:o.user},1, function(e3, res) {
				                CompIDs.remove({compid:co.compid},1, function(e3, res) {
				                  var compuserIDs = dbConn.collection(co.compid+'_userids');
				                  userIDs.find().each(function(er, c) { 
				                    if (c)  {
				                      compuserIDs.insert(c, function (er1,c1) {
				                      });
				                    }
				                  });
				                  var compdeptIDs = dbConn.collection(co.compid+'_department');
				                  deptIDs.find().each(function(er, c) { 
				                    if (c)  {
				                       c.date = new Date();
				                      compdeptIDs.insert(c, function (er1,c1) {
				                      });
				                    }
				                  });
				                  var compskillIDs = dbConn.collection(co.compid+'_skills');
				                  skillIDs.find().each(function(er, c) { 
				                    if (c)  {
				                       c.date = new Date();
				                      compskillIDs.insert(c, function (er1,c1) {
				                      });
				                    }
				                  });
				                  var compkraIDs = dbConn.collection(co.compid+'_kra');
				                  kraIDs.find().each(function(er, c) { 
				                    if (c)  {
				                       c.date = new Date();
				                      compkraIDs.insert(c, function (er1,c1) {
				                      });
				                    }
				                  });
				                  EM.newCOActivate(newData, function (err, op)   {
				                     if (op)     {
				                       console.log("success");
				                       callback(true,{err:0,text:'Verification successful. Sign in to update your profile'});
				                     } else  {
				                       console.log("email not sent");
				                 			 callback(false,{err:3,text:'Please check your email for Password'});
				                     }
				                  });
				                });
				              });
			              } else	{
				              callback(true,{err:1,text:'Unable to create licenses'});
			              }
                  });
                } else  {
		              callback(true,{err:1,text:'Verification unsuccessful(c1u0)'});
                }
              });
            } else  {
		          callback(true,{err:2,text:'Verification unsuccessful(c0u0)'});
            }
          });
        } else  {
		      callback(true,{err:3,text:'Unable to generate new company id'});
		    }
      });
    }	else	{
		  callback(true,{err:4,text:'Unknown Verification Code'});
		}
  });
}

exports.resendVerCode = function(semail, callback)     {
	preUser.findOne({user:semail}, function(e, o) {
		if (o)      {
      //console.log("user verification id available");
      EM.resendverid(o, function (err, op)   {
        if (op)     {
          ///console.log("success");
    			callback(false,{err:0,text:'Please check your email for verification code'});
        } else  {
          //console.log("email not sent");
    			callback(true,{err:1,text:'Unable to send verification code'});
        }
      });
		} else    {
      //console.log("user verification id not available");
		  callback(true,{err:2,text:'Unknown email id'});
    }
	});
}

exports.getonecompany = function(userid, callback)   {
  //console.log(userid);
	company.findOne({user:userid}, function(e, o)   {
		if (e)	{
			callback(true,{err:1,text:'company not found'});
		} else    {
		  //console.log(o);
			callback(false,o);
		}
	});
}

exports.getallcompany = function(userid, callback)   {
	company.find({}).sort({companyid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
			callback(false,o);
		}
	});
}

exports.updatecompany = function(userid,coname,ind,addr,city,pin,linkp,abt,values,clogo, callback)   {
	company.findOne({user:userid}, function(e, o)   {
		if (o)    {
			o.companyname = coname;
			o.industry = ind;
			o.addr = addr;
			o.city = city;
			o.pincode = pin;
			o.linkedin = linkp;
			o.about = abt;
			o.values = values;
			if (clogo != '')
				o.clogo = clogo;
		  company.save(o, {safe: true}, function (e1,o1)      {
			  if (e1)
				  callback(true,{err:1,text:'company not updated'});
			  else
				  callback(false,{err:0,text:'company updated'});
		  });
		} else  {
  		callback(true,{err:2,text:'Unknown company'});
		}
	});
}

exports.updatesettings = function(newData, callback)   {
	//var company  = dbConn.collection(newData.coid+'_company');

	company.findOne({companyid:parseInt(newData.coid)}, function(e, o)   {
		if (o)    {
        o.fysdate = newData.fysdate;
        o.fyedate = newData.fyedate;
        o.wwstart = newData.wwstart;
        o.weno = newData.weno;
				o.hrdate = newData.hrdate;
				o.hr2date = newData.hr2date;
				o.maxl = newData.maxl;
				o.negl = newData.negl;
				o.preh = newData.preh;
				o.inth = newData.inth;
				o.such = newData.such;
				o.maxcf = newData.maxcf;
				o.maxtr = newData.maxtr;

				o.psdate = newData.psdate;
				o.prdate = newData.prdate;
				o.rperiod = newData.rperiod;
				o.prwho = newData.prwho;

			  delete o.coid;
			  company.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'company not updated'});
				else
					callback(false,{err:0,text:'company updated'});
			});
		} else  {
      newData.fysdate = newData.fysdate;
      newData.fyedate = newData.fyedate;
      newData.wwstart = newData.wwstart;
      newData.weno = newData.weno;
			newData.hrdate = newData.hrdate;
			newData.hr2date = newData.hr2date;
			newData.maxl = newData.maxl;
			newData.negl = newData.negl;
			newData.preh = newData.preh;
			newData.inth = newData.inth;
			newData.such = newData.such;
			newData.maxcf = newData.maxcf;
			newData.maxtr = newData.maxtr;

			newData.psdate = newData.psdate;
			newData.prdate = newData.prdate;
			newData.rperiod = newData.rperiod;
			newData.prwho = newData.prwho;

			delete newData.coid;
			company.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'company not inserted'});
				else
					callback(false,{err:0,text:'company inserted'});
			});
		}
	});
}

exports.golive = function(coid, callback)   {
	company.findOne({companyid:parseInt(coid)}, function(e, o)   {
		if (o)    {
			o.live = true;
			company.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Company not live'});
				else
					callback(false,{err:0,text:'Company is now live'});
			});
		} else  {
  		callback(true,{err:2,text:'company not found'});
		}
	});
}

exports.updatelboard = function(newData, callback)   {
	//var company  = dbConn.collection(newData.coid+'_company');
  //console.log(newData);

	company.findOne({companyid:parseInt(newData.coid)}, function(e, o)   {
		if (o)    {
				o.lbmessage = newData.lbmessage;
			delete o.coid;
			company.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'Leader Board not updated'});
				else
					callback(false,{err:0,text:'Leader Board updated'});
			});
		} else  {
  		callback(true,{err:2,text:'company not found'});
		}
	});
}

exports.updateinduction = function(newData, callback)   {
	//var company  = dbConn.collection(newData.coid+'_company');
	company.findOne({companyid:parseInt(newData.coid)}, function(e, o)   {
		if (o)    {
			o.indcfname = newData.docfile;
			delete o.coid;
			company.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'company not updated'});
				else
					callback(false,{err:0,text:'company updated'});
			});
		} else  {
			newData.indcfname = newData.filename;
			delete newData.coid;
			company.insert(newData, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'company not inserted'});
				else
					callback(false,{err:0,text:'company inserted'});
			});
		}
	});
}

/* private encryption & validation methods */
var generateSalt = function()		{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)			{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

exports.checkInfo = function(userid, callback)   {
	company.find({}).sort({companyid:1}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'company not found'});
		else    {
			var colen = o.length;
			for(var x=0;x<colen; x++)		{
				var coObj = o[x];
				if (!coObj.hrdate || !coObj.hrdate2)
					console.log("Company " + coObj.companyid + " has no Leave accrual start month defined");
			}
			callback(false,{err:0,text:'companies processed'});
		}
	});
}

exports.verifyUser = function(sverid, callback)     {
  //console.log(sverid);
  var users = dbConn.collection('users');
  var preuser = dbConn.collection('preuser');
  var company = dbConn.collection('company');

	preuser.findOne({verid:sverid}, function(e, o) {
		if (o)      {
      var newData = new Object;
      newData = o;
      delete newData.verid;
			newData.date = new Date();;
      // create user
			users.insert(newData, {safe: true}, function (e2,o2)   {
        if (o2)     {
          // remove pre-user
          preuser.remove({user:o.user},1, function(e3, res) {
            //console.log("user "+o.user+" is a website user");
            if(!!newData.company && newData.company != '')    {
              company.insert({companyname:newData.company, user:newData.user, industry:newData.industry}, {safe: true}, function (e3,o3)   {
                if (o3)     {
            			callback(true,{err:0,text:'Verification successful. Sign in to post your jobs'});
          			}
              });
            } else  {
        			callback(true,{err:0,text:'Verification successful. Sign in to update your profile'});
      			}
          });
        } else  {
    			callback(true,{err:1,text:'Verification unsuccessful'});
        }
			});
		}	else	{
			callback(true,{err:2,text:'Unknown Verification Code'});
		}
	});
}

