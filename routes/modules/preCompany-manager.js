if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPpreCompany = function(userid, callback)   {
	var preCompany  = dbConn.collection('preCompany');

	preCompany.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			preCompany.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'preCompany not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading preCompany'});
	});
};
exports.getNPpreCompany = function(userid, pageno, callback)   {
	var preCompany  = dbConn.collection(coid+'_preCompany');

	preCompany.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'preCompany not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallpreCompany = function(coid, userid, callback)   {
	var preCompany  = dbConn.collection(coid+'_preCompany');

	preCompany.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'preCompany not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewpreCompany = function(newData, callback)   {
	var preCompany  = dbConn.collection(newData.coid+'_preCompany');

	newData.date = new Date();
	newData.email = newData.email;
	newData.usermobile = newData.usermobile;
	newData.company = newData.company;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	preCompany.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'preCompany not added'});
		else   {
			getFPpreCompany(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table preCompany data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.updatepreCompany = function(newData, callback)   {
	var preCompany  = dbConn.collection(newData.coid+'_preCompany');

	preCompany.findOne({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (o)   {
			o.username = newData.username;
			o.email = newData.email;
			o.usermobile = newData.usermobile;
			o.company = newData.company;
			delete o.coid;
			preCompany.save(o, {safe: true}, function (e1,o1)      {
				if (e1)
					callback(true,{err:1,text:'preCompany not updated'});
				else   {
					getFPpreCompany(newData.coid,newData.userid, function(e2,o2, totpages)     {
						if (e2)
							callback(true,{err:2,text:'Table preCompany data updated'});
						else
							callback(false,o2, totpages);
					});
				}
			});
		} else  {
			callback(true,{err:2,text:' ID not found in preCompany'});
		}
	});
}
exports.deletepreCompany = function(newData, callback)   {
	var preCompany  = dbConn.collection(newData.coid+'_preCompany');

	preCompany.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'preCompany data not deleted'});
		else   {
			getFPpreCompany(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table preCompany data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPpreCompany = function(coid, userid, callback)   {
	var preCompany  = dbConn.collection(coid+'_preCompany');

	preCompany.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			preCompany.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'preCompany not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading preCompany'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

