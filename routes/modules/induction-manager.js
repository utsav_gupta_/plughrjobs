if (process.env.OPENSHIFT_MONGODB_DB_URL)
	var NUMBER_OF_ITEMS_PER_PAGE = 20;
else
	var NUMBER_OF_ITEMS_PER_PAGE = 3;

exports.getFPinduction = function(coid, userid, callback)   {
	var induction  = dbConn.collection(coid+'_induction');

	induction.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			induction.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'induction not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading induction'});
	});
};
exports.getNPinduction = function(coid, userid, pageno, callback)   {
	var induction  = dbConn.collection(coid+'_induction');

	induction.find({}).sort({_id:1}).skip((pageno-1)*NUMBER_OF_ITEMS_PER_PAGE).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'induction not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.getallinduction = function(coid, userid, callback)   {
	var induction  = dbConn.collection(coid+'_induction');

	induction.find({}).toArray(function(e, o)   {
		if (e)
			callback(true,{err:1,text:'induction not found'});
		else    {
			callback(false,o);
		}
	});
}
exports.addnewinduction = function(newData, callback)   {
	var induction  = dbConn.collection(newData.coid+'_induction');

	newData.date = new Date();
	newData.itype = newData.itype;
	newData.dept = newData.dept;
	newData.filename = newData.filename;
	var scoid = newData.coid;
	delete newData.coid;
	var suserid = newData.userid;
	delete newData.userid;

	induction.insert(newData, {safe: true}, function (e1,o1)      {
		if (e1)
			callback(true,{err:1,text:'induction not added'});
		else   {
			getFPinduction(scoid,suserid, function(e2,o2, totpages)     {
			if (e2)
				callback(true,{err:2,text:'Table induction data added'});
			else
				callback(false,o2, totpages);
			});
		}
	});
}
exports.deleteinduction = function(newData, callback)   {
	var induction  = dbConn.collection(newData.coid+'_induction');

	induction.remove({_id: getObjectId(newData.dbid)}, function(e, o)   {
		if (e) 
			callback(true,{err:1,text:'induction data not deleted'});
		else   {
			getFPinduction(newData.coid,newData.userid, function(e2,o2, totpages)     {
				if (e2)
					callback(true,{err:2,text:'Table induction data deleted'});
				else
					callback(false,o2, totpages);
			});
		}
	});
}
var getFPinduction = function(coid, userid, callback)   {
	var induction  = dbConn.collection(coid+'_induction');

	induction.find({}).count(function(e1, o1)   {
		if (!e1)      {
			var tot = o1 / NUMBER_OF_ITEMS_PER_PAGE;
			var tot = Math.ceil(tot);
			induction.find({}).sort({_id:1}).limit(NUMBER_OF_ITEMS_PER_PAGE).toArray(function(e, o)   {
				if (e)
					callback(true,{err:1,text:'induction not found'});
				else
					callback(false,o, tot);
			});
		} else
			callback(true,{err:2,text:'Error in reading induction'});
	});
}
var getObjectId = function(dbid)    {
	var ObjectId = require('mongodb').ObjectID;
	var objID = new ObjectId(dbid);
	return  objID;
}

