var EM        = require('./modules/email-dispatcher');
var BG        = require('./modules/bgproc-manager');

exports.bgjob01 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.SelectLiveCO({}, function(e, o) {
				callback(e,o);
			});
		},
		function(callback) {
			BG.clearnotifs({}, function(e0, o0) {
				callback(e0,o0);
			});
		},
		function(callback) {
			BG.Proc01({}, function(e1, o1) {
				callback(e1,o1);
			});
		},
		function(callback) {
			BG.Proc02({}, function(e2, o2) {
				callback(e2,o2);
			});
		},
		function(callback) {
			BG.Proc031({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// Check for users without targets
// Check for users yet to submit self review ratings
exports.bgjob02 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.Proc032({}, function(e32, o32) {
				callback(e32,o32);
			});
		},
		function(callback) {
			BG.Proc033({}, function(e33, o33) {
				callback(e33,o33);
			});
		},
		], function() {
			return;
		}
	);
};

/*
// for pending supervisor ratings
exports.bgjob03 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.Proc06a({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// for pending skip supervisor ratings
exports.bgjob04 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.Proc06b({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// Rate now email for users on closing of review period
exports.bgjob05 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.Proc06c({}, function(e4, o4) {
				callback(e4,o4);
			});
		},
		], function() {
			return;
		}
	);
};

// for pending leave approvals & Incomplete user profiles
exports.bgjob06 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
	  // Pending leave approvals
		function(callback) {
			BG.Proc07a({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
    // Incomplete user profiles
		function(callback) {
			BG.Proc07b({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};
*/

// Process all Appraisals starting on the date - Phase 1
exports.bgjob09a = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
		  // Create user list by company
		  console.log("1 - creating user list by company " + new Date());
			BG.Proc09a({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// Process all Appraisals starting on the date - Phase 2
exports.bgjob09b = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
		  // Update Behavioral competencies
		  console.log("2 - Update Behavioral competencies " + new Date());
			BG.Proc09b({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		function(callback) {
		  // Update the skip manager for each user
		  console.log("1x - Update the skip manager for each user " + new Date());
			BG.Proc09x({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		function(callback) {
		  // Update Core values
		  console.log("3 - Update Core values " + new Date());
			BG.Proc09c({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// Process all Appraisals starting on the date - Phase 3
exports.bgjob09c = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
		  // Update KRAs
		  console.log("4 - Update KRAs " + new Date());
			BG.Proc09d({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		function(callback) {
		  // Update review data
		  console.log("5 - Update review data " + new Date());
			BG.Proc09e({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		function(callback) {
		  // Update Appraisal status as Started
		  console.log("6 - Update Appraisal status as Started " + new Date());
			BG.Proc09f({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// Process all Appraisals starting on the date - Phase 4
exports.bgjob09d = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
		  // Create notifications based on Final Table
		  console.log("7 - Create notifications based on Final Table " + new Date());
			BG.Proc09g({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// User emails about new notifications
exports.bgjob10 = function() {
	var data0, data1, data2;
	var tot;
	async.series([
		function(callback) {
			BG.Proc10({}, function(e3, o3) {
				callback(e3,o3);
			});
		},
		], function() {
			return;
		}
	);
};

// One time Job - delete later
// Bring usergoals table uptodate
// tsubmitted becomes trgt_submited
// submitted becomes self_submited
// msubmitted becomes supr_submited
// ssubmitted becomes skip_submited
/*
exports.bgjob0a = function() {
	var data0, data1, data2;
	var tot;
	async.parallel([
		function(callback) {
			BG.Proc0a({}, function(e1, o1) {
				callback(e1,o1);
			});
		},
		function(callback) {
			BG.Proc0b({}, function(e2, o2) {
				callback(e2,o2);
			});
		},
		], function() {
			return;
		}
	);
};
*/
