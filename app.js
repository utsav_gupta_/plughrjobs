var async = require('async');
var express = require('express');
var http = require('http');
var path = require('path');
var fs = require('fs');
var schedule = require('node-schedule');

var MongoClient = require('mongodb').MongoClient
const clientSessions = require("client-sessions");

if (process.env.MONGO_URL)   {
	dbPort 		= process.env.MONGODB_PORT;
	dbHost 		= process.env.MONGODB_HOST;
	dbUser    = process.env.MONGODB_USER;
	dbPass    = process.env.MONGODB_PASSWORD;
	dbName 		= 'sampledb';
	dbUrl 			= 'mongodb://admin:prashant123@172.30.54.209:27017/sampledb';
} else  {
	dbPort 		= 27017;
	dbHost 		= process.env.MONGODB_HOST;
	dbName 		= 'sampledb';
	dbUrl 			= 'mongodb://admin:prashant123@172.30.54.209:27017/sampledb';
}

// openshift keeps /temp directory in another volume..making file movement across volumes an error
process.env.TMPDIR = 'enttmp';
process.env.TZ = 'UTC';

var app = express();

var serverPort = 8080;
var serverIP = '127.0.0.1';

MongoClient.connect(dbUrl, function(err, db) {
	if (err)		{
		console.log('Cannot connect to database ' + dbUrl);
    process.exit(1);
	} else	{
		global.dbConn = db;				// leverage the connection pooling offered by mongoDB driver
		app.set('views', path.join(__dirname, 'views'));
		app.set('view engine', 'jade');

		app.use(express.compress());
		app.use(express.favicon());

		app.use(express.json());
		app.use(express.urlencoded());
		app.use(express.methodOverride());

		app.use(clientSessions({
			secret: 'zK09QI7QG2RVxMO6Syzh7nTX0Opc6bin8oIz8u5ulccRwcnjHl7i8i6Jx2WOySSH', // set this to a long random string!
			cookieName: 'plughrjobs', // cookie name dictates the key name added to the request object
			secret: 'r6QDMcG6RTwIWRVCbNI7FFZ5Wlomls1sFTuhiNmTGvoTvbhuC8OI5c1GWWowjs5X', // should be a large unguessable string
			duration: 1 * 30 * 60 * 1000 // how long the session will stay valid in ms (set to 30 mins)
			//activeDuration: 1000 * 60 * 60 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
		}));

		//  Setup termination handlers (for exit and a list of signals).
		//  Process on exit and signals.
		process.on('exit', function() { terminator(); });

		// Removed 'SIGPIPE' from the list - bugz 852598.
		['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
		 'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
		].forEach(function(element, index, array) {
				process.on(element, function() { terminator(element); });
		});

		var cacheexpiry = 60 * 60 * 24 * 7;			// Cache-expiry set to 1 week

		app.use(express.static(path.join(__dirname, 'public'), { maxAge: cacheexpiry }));
		app.use('/uploads', express.static(__dirname + '/uploads'));

		// development only
		if ('development' == app.get('env')) {
			app.use(express.errorHandler());
		}

		// Add router middleware explicitly
		app.use(app.router);
		// Pass the Express instance to the routes module
		var routes = require('./routes/router.js')(app);

		var enteemApp = http.createServer(app).listen(serverPort, serverIP, function(){
			console.log('PlugHRjobs server started on '+ Date(Date.now()) +' and listening on server '+ serverIP + ' and port ' + serverPort);
		});
	}
});

/**
 *  terminator === the termination handler
 *  Terminate server on receipt of the specified signal.
 *  @param {string} sig  Signal to terminate on.
 */
function terminator(sig)    {
  if (typeof sig === "string") {
    console.log('%s: Received %s - terminating workhalf app ...',Date(Date.now()), sig);
    process.exit(1);
  }
  console.log('%s: Node server stopped.', Date(Date.now()) );
};

