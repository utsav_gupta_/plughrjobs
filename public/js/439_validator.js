$(function() {
	$(document).ajaxStart(function(){
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
	$('table.perfgoals').footable();
	var uid = $('#cuser').text();
	$('#modal_tdate').datepicker({ dateFormat: 'dd-mm-yy'});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status < 3)
				$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function def_replist()    {
	var xObj = JSON.parse($('#repdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#rep-list');
		}
	}
}

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	$('#usr-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#usr-list');
		}
	}
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_rating');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
	$('#rperiod-list').prop('selectedIndex', 0);
	$('#usr-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	$('#accordion1').empty();
	$('#formdata').val('');

	$('#rperiod-list').prop('disabled', false);
	$('#rep-list').prop('disabled', false);
	$('#usr-list').prop('disabled', true);
	$('#search_btn').prop('disabled', true);
	$('#submit_btn').prop('disabled',true);
}

function getGoalName(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
			return(selx.title);
		}
	}
	return "";
}

function get_skips()		{
	var s_rperiod = $('#rperiod-list').val();
	var s_repobj = JSON.parse($('#rep-list').val());
	var s_usr = s_repobj.userid;
	var s_role = s_repobj.roleid;

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod= s_rperiod;
	formdataObj.usr= s_usr;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/439skips',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#usrdata').val(JSON.stringify(resp.data));
				def_usr();
				$('#rperiod-list').prop('disabled', true);
				$('#rep-list').prop('disabled', true);
				$('#usr-list').prop('disabled', false);
				$('#search_btn').prop('disabled', false);
				setSuccess($('#errtext1'),'Skip reportees selected');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function GetValidInputs()			{
	var s_rperiod = $('#rperiod-list').val();
	var s_usrobj = JSON.parse($('#usr-list').val());
	if (s_usrobj)   {
	  var s_usr = s_usrobj.userid;
	  var s_role = s_usrobj.roleid;

	  var formdataObj = new  Object();
	  formdataObj.coid = $('#ccoid').text();
	  formdataObj.userid = $('#cuserid').text();
	  formdataObj.rperiod= s_rperiod;
	  formdataObj.usr= s_usr;
    return formdataObj;
  } else  {
    return null;
  }
}

function get_goals()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/439search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				showgoals();
				$('#rperiod-list').prop('disabled', true);
				$('#usr-list').prop('disabled', true);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function showgoals()			{
	var rcount = 0;
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		$('#accordion1').empty();
		var glen = ugoals.length;
		var newDiv = "<div id='goalslist'>";
    if (glen < 1)   {
			setError($('#errtext1'),'No Targets defined');
		  $('#submit_btn').prop('disabled',true);
      return false;
    }
		var submitted = 1;

		for (var ctr = 0;ctr < glen; ctr++)		{
			var obj = ugoals[ctr];

			var or = "";
			if (!obj.selfrating)
				or = "Pending";
			else
				or = obj.selfrating;
			var mr = "";
			if (!obj.mgrrating)
				mr = "Pending";
			else
				mr = obj.mgrrating;
			var sr = "";
			if (!obj.skiprating)
				sr = "Pending";
			else
				sr = obj.skiprating;

			newDiv += "<div id='goal" + ctr + "'><h3><b><a href='#'>"+getGoalName(obj.ugoalid)+"</a></b></h3>";
			newDiv += "<div>";
			newDiv += "<p><span  class='text-success'><b>Expected Performance </b></span>"+obj.meets+"</p>";
			newDiv += "<p><span  class='text-success'><b>Weightage (%) </b></span>"+obj.weight;
			newDiv += "<span class='text-success col-sm-offset-1'><b>Self-rating </b></span>"+or;
			newDiv += "<span class='text-success col-sm-offset-1'><b>Manager rating </b></span>"+mr;
			newDiv += "<span class='text-success col-sm-offset-1'><b>Skip Manager rating </b></span>"+sr+"</p>";

			// For Task #1
			newDiv += "<p>";
			newDiv += "<span  class='text-primary'><b>Task #1 </b></span>";
			newDiv += "is ";
			if (obj.task1desc)			{
				newDiv += "<b>\" </b><i>" + obj.task1desc + "</i><b> \"</b>. ";
				if (obj.task1date)
					newDiv += "You have planned to complete it by " + obj.task1date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task1status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #2
			newDiv += "<p>";
			newDiv += "<span  class='text-primary'><b>Task #2 </b></span>";
			newDiv += "is ";
			if (obj.task2desc)			{
				newDiv += "<b>\" </b><i>" + obj.task2desc + "</i><b> \"</b>. ";
				if (obj.task2date)
					newDiv += "You have planned to complete it by " + obj.task2date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task2status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #3
			newDiv += "<p>";
			newDiv += "<span  class='text-primary'><b>Task #3 </b></span>";
			newDiv += "is ";
			if (obj.task3desc)			{
				newDiv += "<b>\" </b><i>" + obj.task3desc + "</i><b> \"</b>. ";
				if (obj.task3date)
					newDiv += "You have planned to complete it by " + obj.task3date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task3status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #4
			newDiv += "<p>";
			newDiv += "<span  class='text-primary'><b>Task #4 </b></span>";
			newDiv += "is ";
			if (obj.task4desc)			{
				newDiv += "<b>\" </b><i>" + obj.task4desc + "</i><b> \"</b>. ";
				if (obj.task4date)
					newDiv += "You have planned to complete it by " + obj.task4date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task4status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #5
			newDiv += "<p>";
			newDiv += "<span  class='text-primary'><b>Task #5 </b></span>";
			newDiv += "is ";
			if (obj.task5desc)			{
				newDiv += "<b>\" </b><i>" + obj.task5desc + "</i><b> \"</b>. ";
				if (obj.task5date)
					newDiv += "You have planned to complete it by " + obj.task5date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task5status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			if (obj.self_submited == '1')			{
			  if (obj.supr_submited == '1')			{
			    if (obj.skip_submited == '1')			{
				    skipsubmitted = 0;
		    		newDiv += "<span class = 'text-danger'><b>Performance progress is already submitted after skip manager review.</b></span>";
    			  $('#submit_btn').prop('disabled',true);
			    } else	{
    				newDiv += "<button class='btn-sm btn-info col-sm-2' id = 'upd_btn' data-btn ='" + ctr + "' type='button'>Skip Manager Rating</button>";
    			  $('#submit_btn').prop('disabled',false);
			    }
			  } else	{
				  submitted = 0;
				  newDiv += "<span class = 'text-danger'><b>Supervisor is yet to review and provide rating </b></span>";
  			  $('#submit_btn').prop('disabled',true);
			  }
			} else	{
				submitted = 0;
				newDiv += "<span class = 'text-danger'><b>Performance progress is not yet submitted for review & approval. </b></span>";
			  $('#submit_btn').prop('disabled',true);
			}
			newDiv += "<br><br><hr>";
			newDiv += "</div></div>";
		}
		newDiv += "</div>";
		$('#accordion1').append(newDiv);
	}
	$('#accordion1').css("visibility","visible");
	$('#accordion1').accordion("refresh");
	setSuccess($('#errtext1'),'Targets for selected review period listed below');
}

function showRating(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_indx1').val(indx);
		$('#modal_dbid1').val(obj._id);
		$('#modal_rting').val(obj.selfrating);

	  $('#RatingModal').modal();
  }
  return false;
}

function upd_rating()				{
	var s_dbid = $('#modal_dbid1').val();
	var s_rating = $('#modal_rating').val();
	var s_indx = $('#modal_indx1').val();

	var s_usrobj = JSON.parse($('#usr-list').val());
	var s_usr = s_usrobj.userid;
	var s_role = s_usrobj.roleid;

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = s_usr;
	ratingObj.dbid = s_dbid;
	ratingObj.rating = s_rating;

	//alert(JSON.stringify(ratingObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/439mgr',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].skiprating = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
				showgoals();

				setSuccess($('#errtext1'),'Manager rating updated');
				$('#RatingModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function verify_ratings()     {
	var ugoals = JSON.parse($('#formdata').val());
	if (ugoals)		{
		var glen = ugoals.length;
		var submitted = 0;
		for (var ctr = 0;ctr < glen; ctr++)		{
			var obj = ugoals[ctr];
      if (!obj.skiprating || obj.skiprating == '')
        return false;
    }
  }
}

function submit_progress()				{
	var mesg = "Your skip team member would be intimated of these targets, do you want to submit?";
	alertify.set({ labels: {
		ok     : "Submit",
		cancel : "Don't submit"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var s_usrobj = JSON.parse($('#usr-list').val());
			var s_usr = s_usrobj.userid;

			var ratingObj = new  Object();
			ratingObj.coid = $('#ccoid').text();
			ratingObj.userid = s_usr;
			ratingObj.rpid = $('#rperiod-list').val();

			//alert(JSON.stringify(ratingObj));

			$.ajax({
				type: 'POST',
				url: '/439submit',
				data: ratingObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (resp.err == 0)   {
						var ugoals = JSON.parse($('#formdata').val());
						for (var ctr=0; ctr < ugoals.length; ctr++)			{
							ugoals[ctr].skip_submited = '1';
						}
						$('#formdata').val(JSON.stringify(ugoals));
						showgoals();

						setSuccess($('#errtext1'),'Performance progress submitted successfully');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.text);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

$(document).ready(function()	{
	def_rperiod();
	def_replist();
	def_rlevel();
	$("#accordion1").accordion({ 
		header: "h3",          
		autoheight: true,
		active: false,
		alwaysOpen: false,
		fillspace: false,
		collapsible: true,
    highstyle : "auto"
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		return false;
	});
	$('#search_btn').click(function() 			{
		get_goals();
		$('#submit_btn').prop('disabled',false);
		return false;
	});
	$('#skips_btn').click(function() 			{
		get_skips();
		return false;
	});

	$('#accordion1').on('click','button', function (evt) {
		var indx = $(this).data('btn');
		var txt = $(this).text();

		switch (txt)		{
			case 'Skip Manager Rating':
				showRating(indx);
				break;
		}
   	return false;
	});
	$('#ratingupd_btn').click(function() 			{
		upd_rating();
		return false;
	});
	$('#submit_btn').click(function() 			{
    if (verify_ratings() == false)
      setError($('#errtext1'),"Please complete skip manager rating for all targets");
    else
  		submit_progress();
		return false;
	});
});

