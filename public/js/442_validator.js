/*
	// Status values
	1 = "Submitted"
	2 = "Accepted"
	3 = "Rejected"
	4 = "Withdrawn"
*/

$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#ldate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#eact-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth : true,
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#ldate').val('');
	$('#repl-list').prop('selectedIndex', 0);
	$('#eact-list').prop('selectedIndex', 0);
	$('#status-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function getEact(eactid)		{
	var xObj = JSON.parse($('#eactdata').val());
	var rval = '';
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var fnd = eactid.indexOf(obj._id);
			if (fnd != -1)
				rval += obj.title + ', ';
		}
	}
	return rval;
}


function getUser(usrid)		{
	var xObj = JSON.parse($('#repldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == usrid)
				return obj.username;
		}
	}
	return "";
}

function def_repl()    {
	var xObj = JSON.parse($('#repldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#repl-list');
		}
	}
}

function def_eacts()    {
	var xObj = JSON.parse($('#eactdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#eact-list');
		}
	}
}

function defMesgTable()    {
	$('table.exits').data('footable').reset();
	$('table.exits thead').append('<tr>');
	$('table.exits thead tr').append('<th>Employee</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Reason</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Last working day</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Take-over person</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Exit Activites</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.exits thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.exits thead tr').append('</tr>');
	$('table.exits').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#repldata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.exits').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.userid != null)
				newRow += getUser(obj.userid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.reason)     {
				case '1':
					newRow += 'Better Career Prospects';
					break;
				case '2':
					newRow += 'Better Role';
					break;
				case '3':
					newRow += 'Better Salary';
					break;
				case '4':
					newRow += 'Work-Life Balance';
					break;
				case '5':
					newRow += 'Dream Job Offer';
					break;
				case '6':
					newRow += 'Family circumstances';
					break;
				case '7':
					newRow += 'Personal Reasons';
					break;
				case '8':
					newRow += 'Getting Married';
					break;
				case '9':
					newRow += 'Higher Education';
					break;
				case '10':
					newRow += 'Health Reasons';
					break;
				case '11':
					newRow += 'Full-time position';
					break;
				case '12':
					newRow += 'Maternity related';
					break;
				case '13':
					newRow += 'Not returning from Maternity leave';
					break;
				case '14':
					newRow += 'Other Reasons';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.ldate != null)
				newRow += obj.ldate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.repluserid == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.eact != null)
				newRow += getEact(obj.eact);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '0':
					newRow += 'Initiated';
					break;
				case '1':
					newRow += 'Submitted';
					break;
				case '2':
					newRow += 'Approved';
					break;
				case '3':
					newRow += 'Rejected';
					break;
				case '4':
					newRow += 'Withdrawn';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.status == '1')
				newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/442newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.exits').data('footable');

			$('table.exits tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.exits tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.exits').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.exits').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#ldate').val(obj.ldate);
		$('#repl-list').val(obj.repluserid);
		$('#status-list').val(obj.status);
		$('#mgr').val(obj.mgr);
		$('#eact-list').select2('val', obj.eact);

		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function GetValidInputs()		{
	var s_ldate = $('#ldate').val();
	if (s_ldate == '')	{
		$('#ldate').css('border-color', 'red');
		setError($('#errtext1'),'Please input Expected Last working day');
		$('#ldate').focus();
		return false;
	} else    {
		$('#ldate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_repl = $('#repl-list').val();
	if (s_repl == null)	{
		$('#repl-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Who do you think is best person to take-over from you?');
		$('#repl-list').focus();
		return false;
	} else    {
		$('#repl-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sts = $('#status-list').val();
	if (s_sts == null)	{
		$('#status-list').css('border-color', 'red');
		setError($('#errtext1'),'Please approve or reject this exit request');
		$('#status-list').focus();
		return false;
	} else    {
		$('#status-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_eact = $('#eact-list').val();
	if (parseInt(s_sts) == 2)	{
		if (s_eact == null)	{
			$('#eact-list').css('border-color', 'red');
			setError($('#errtext1'),'Please select Exit Actions');
			$('#eact-list').focus();
			return false;
		} else    {
			$('#eact-list').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.mgr= $('#mgr').val();
	formdataObj.ldate= s_ldate;
	formdataObj.repl= s_repl;
	formdataObj.sts= s_sts;
	formdataObj.eact= s_eact;
	return formdataObj;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/442upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.exits').data('footable');

				$('table.exits tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.exits tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'exits data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_repl();
	def_eacts();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

