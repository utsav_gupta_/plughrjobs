$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.indcs').footable();
});

function defMesgTable()    {
	$('table.indcs').data('footable').reset();
	$('table.indcs thead').append('<tr>');
	$('table.indcs thead tr').append('<th>Title</th>');
	$('table.indcs thead tr').append('<th data-hide="phone,tablet">Induction Type</th>');
	$('table.indcs thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.indcs thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.indcs thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.indcs thead tr').append('</tr>');
	$('table.indcs').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#deptdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.indcs').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.itype)     {
				case '1':
					newRow += 'Mandatory';
					break;
				case '2':
					newRow += 'Optional';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.dept == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-eye-open" title="Edit"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}
$(function () {
	$('table.indcs').footable().on('click', '.row-edit', function(e) {
    e.preventDefault();
    var rtable = $('table.userdocs').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
	});
});
function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#formdata").val());
    var indcid = dataObj[rowIndex]._id;
    var docfile = dataObj[rowIndex].filename;
    var indctitle = dataObj[rowIndex].title;
		var xcoid = $('#ccoid').text();
		var xuserid = $('#cuserid').text();

    //var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + "/" + docfile;
    //window.open(docsrc);
		window.location.href = '/449?coid='+xcoid + '&indcid=' + indcid + '&fname=' + docfile + '&title=' + indctitle;

    //window.open('/uploads/'+ xcoid + "/" + docfile);
    return false;
  }
}
function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/448newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.indcs').data('footable');

			$('table.indcs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.indcs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
});

