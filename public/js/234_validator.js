$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode == 46)
		return true;
	if (charCode > 31 && ( charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#ltype-list').prop('selectedIndex', 0);
	$('#pbal').val('');
	$('#used').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

// ------------------------------------------------------------
function disableForm1()		{
	document.getElementById('user-list').disabled = true;
	document.getElementById('search_btn').disabled = true;
	document.getElementById('clear_btn').disabled = false;
}

function enableForm1()		{
	document.getElementById('user-list').disabled = false;
	document.getElementById('search_btn').disabled = false;
	document.getElementById('clear_btn').disabled = false;
}

function disableForm2()		{
	document.getElementById('ltype-list').disabled = true;
	document.getElementById('pbal').disabled = true;
	document.getElementById('used').disabled = true;

	document.getElementById('add_btn').disabled = true;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function enableForm2()		{
	$('#dbid').val('');
	$('#ltype-list').prop('selectedIndex', 0);
	$('#pbal').val('');
	$('#used').val('');
	clearError($('#errtext1'));

	document.getElementById('ltype-list').disabled = false;
	document.getElementById('pbal').disabled = false;
	document.getElementById('used').disabled = false;

	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}
function clearTable()		{
	var rtable = $('table.emplvbal').data('footable');

	$('table.emplvbal tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.emplvbal tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function def_user()    {
	var xObj = JSON.parse($('#userdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#user-list');
		}
	}
}
function def_ltype()    {
	var xObj = JSON.parse($('#ltypedata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.lname).appendTo('#ltype-list');
		}
	}
}
function defMesgTable()    {
	$('table.emplvbal').data('footable').reset();
	$('table.emplvbal thead').append('<tr>');
	$('table.emplvbal thead tr').append('<th>Leave type</th>');
	$('table.emplvbal thead tr').append('<th data-hide="phone,tablet">Previous Balance</th>');
	$('table.emplvbal thead tr').append('<th data-hide="phone,tablet">Availed</th>');
	$('table.emplvbal thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.emplvbal thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.emplvbal thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.emplvbal thead tr').append('</tr>');
	$('table.emplvbal').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#ltypedata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.emplvbal').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.ltype == selx._id)	{
					newRow += selx.lname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.pbal != null)
				newRow += obj.pbal;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.totav != null)
				newRow += obj.totav;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/234newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.emplvbal').data('footable');

			$('table.emplvbal tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.emplvbal tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.emplvbal').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.emplvbal').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#ltype-list').val(obj.ltype);
		$('#pbal').val(obj.pbal);
		$('#used').val(obj.totav);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Please reconfirm action";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());

			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex,10)+1;
			var obj = fdata[rno-1];
			dataObj.dbid= obj._id;
			dataObj.ltype= obj.ltype;
			dataObj.pbal= obj.pbal;
			dataObj.used= obj.totav;

			$.ajax({
				type: 'DELETE',
				url: '/234',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.emplvbal').data('footable');

						$('table.emplvbal tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.emplvbal tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'emplvbal data deleted');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}
function GetValidInputs()		{
	var s_user = $('#user-list').val();
	if (s_user == null)	{
		$('#user-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a user');
		$('#user-list').focus();
		return false;
	} else    {
		$('#user-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ltype = $('#ltype-list').val();
	if (s_ltype == null)	{
		$('#ltype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Leave type');
		$('#ltype-list').focus();
		return false;
	} else    {
		$('#ltype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pbal = $('#pbal').val();
	if (s_pbal == '')	{
		$('#pbal').css('border-color', 'red');
		setError($('#errtext1'),'Please input Previous Balance');
		$('#pbal').focus();
		return false;
	} else    {
		$('#pbal').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_used = $('#used').val();
	if (s_used == '')	{
		$('#used').css('border-color', 'red');
		setError($('#errtext1'),'Please input Availed');
		$('#used').focus();
		return false;
	} else    {
		$('#used').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = s_user;
	formdataObj.dbid= $('#dbid').val();
	formdataObj.ltype= s_ltype;
	formdataObj.pbal= s_pbal;
	formdataObj.used= s_used;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/234add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.emplvbal').data('footable');

				$('table.emplvbal tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.emplvbal tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'emplvbal data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/234upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.emplvbal').data('footable');

				$('table.emplvbal tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.emplvbal tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'emplvbal data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}


function GetSearchInputs()		{
	var s_user = $('#user-list').val();
	if (s_user == null)	{
		$('#user-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a user');
		$('#user-list').focus();
		return false;
	} else    {
		$('#user-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = s_user;
	return formdataObj;
}

function get_bals()		{
	var formdataObj = GetSearchInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/234search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));
				//$('#kradata').val(JSON.stringify(resp.data1));
				$('#totpages').val(JSON.stringify(resp.totpages));

				fillUserData();
				SetPagination();

				disableForm1();
				enableForm2();

				$("#add_btn").prop('disabled',false);
				setSuccess($('#errtext1'),'Leave Balances for selected user listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}
$(document).ready(function()	{
	def_user();
	def_ltype();
	defMesgTable();
	$('#search_btn').click(function() 		{
		get_bals();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		disableForm2();
		enableForm1();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});


