$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#date').datepicker({ dateFormat: 'dd-mm-yy'});
/*
	$('#date').glDatePicker({
    selectableDOW: [1, 4, 6]
	});
*/
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#hname').val('');
	$('#htype-list').prop('selectedIndex', 0);
	$('#date').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.holidaylist').data('footable').reset();
	$('table.holidaylist thead').append('<tr>');
	$('table.holidaylist thead tr').append('<th>Name</th>');
	$('table.holidaylist thead tr').append('<th data-hide="phone,tablet">Holiday Type</th>');
	$('table.holidaylist thead tr').append('<th data-hide="phone,tablet">Date</th>');
	$('table.holidaylist thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.holidaylist thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.holidaylist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.holidaylist thead tr').append('</tr>');
	$('table.holidaylist').footable();
}

var sortBy = (function () {

  //cached privated objects
  var _toString = Object.prototype.toString,
      //the default parser function
      _parser = function (x) { return x; },
      //gets the item to be sorted
      _getItem = function (x) {
        return this.parser((_toString.call(x) === "[object Object]" && x[this.prop]) || x);
      };

  // Creates a method for sorting the Array
  // @array: the Array of elements
  // @o.prop: property name (if it is an Array of objects)
  // @o.desc: determines whether the sort is descending
  // @o.parser: function to parse the items to expected type
  return function (array, o) {
    if (!(array instanceof Array) || !array.length)
      return [];
    if (_toString.call(o) !== "[object Object]")
      o = {};
    if (typeof o.parser !== "function")
      o.parser = _parser;
    //if @o.desc is false: set 1, else -1
    o.desc = [1, -1][+!!o.desc];
    return array.sort(function (a, b) {
      a = _getItem.call(o, a);
      b = _getItem.call(o, b);
      return ((a > b) - (b > a)) * o.desc;
    });
  };

}());

function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj)			{
		var mlen = mesgObj.length;
		for(var x=0;x<mlen;x++)	{
			var tmpdate = mesgObj[x].holidaydate.split("-");
			mesgObj[x].hdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
			mesgObj[x].hdate.setHours(0, 0, 0, 0);
		}
		//alert(JSON.stringify(mesgObj));
		sortBy(mesgObj, { prop: "hdate" });
		$('#formdata').val(JSON.stringify(mesgObj));
	}
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.holidaylist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.holidayname != null)
				newRow += obj.holidayname;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.holidaytype)     {
				case '1':
					newRow += 'Mandatory';
					break;
				case '2':
					newRow += 'Optional';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.holidaydate != null)
				newRow += obj.holidaydate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/222newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			if (mesgObj)			{
				var mlen = mesgObj.length;
				for(var x=0;x<mlen;x++)	{
					var tmpdate = mesgObj[x].holidaydate.split("-");
					mesgObj[x].hdate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
					mesgObj[x].setHours(0, 0, 0, 0);
				}
				alert(JSON.stringify(mesgObj));
				$('#formdata').val(JSON.stringify(mesgObj));
				// DELETE & RE-CREATE Table 
				var rtable = $('table.holidaylist').data('footable');

				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				fillUserData();
			}
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.holidaylist').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.holidaylist').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#hname').val(obj.holidayname);
		$('#htype-list').val(obj.holidaytype);
		$('#date').val(obj.holidaydate);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.hname= obj.holidayname;
			dataObj.htype= obj.holidaytype;
			dataObj.date= obj.holidaydate;

			$.ajax({
				type: 'DELETE',
				url: '/222',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.holidaylist').data('footable');

						$('table.holidaylist tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.holidaylist tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'holidaylist data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_hname = $('#hname').val();
	if (s_hname == '')	{
		$('#hname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Name');
		$('#hname').focus();
		return false;
	} else    {
		$('#hname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_htype = $('#htype-list').val();
	if (s_htype == null)	{
		$('#htype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Holiday Type');
		$('#htype-list').focus();
		return false;
	} else    {
		$('#htype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_date = $('#date').val();
	if (s_date == '')	{
		$('#date').css('border-color', 'red');
		setError($('#errtext1'),'Please input Date');
		$('#date').focus();
		return false;
	} else    {
		$('#date').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.hname= s_hname;
	formdataObj.htype= s_htype;
	formdataObj.date= s_date;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/222add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.holidaylist').data('footable');

				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'holidaylist data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/222upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.holidaylist').data('footable');

				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.holidaylist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'holidaylist data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

