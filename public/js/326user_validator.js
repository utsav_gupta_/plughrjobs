$(function () {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('table.userdocs').footable();
	$('#pint-list').select2({
		placeholder: 'Select all applicable personal interests',
		allowClear: true,
		width : 'style'
	});
	$('#noexp-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		width : 'style'
	});
	$('#vprof-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		width : 'style'
	});
	$('#prof-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		width : 'style'
	});
	$('#sexp-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		width : 'style'
	});
	$('#exprt-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		width : 'style'
	});
});
$(document).ready(function()	{
	def_noexp();
	def_vprof();
	def_prof();
	def_sexp();
	def_exprt();
	fillUserProfile();
	defDocsTable();
	$('#showdocs_btn').click(function() {
	  var docsObject = new Object();
	  docsObject.coid = $("#ucoid").val();
	  docsObject.userid = $("#uuserid").val();
	  $.ajax({
		  type: "GET",
		  url: '/326docs',
		  data: docsObject,
		  dataType: 'json',
        beforeSend:  function()   {
          startAjaxIcon();
        },
			  success: function(docdata)	{
          if (!docdata.err)     {
            $("#docsdata").val(JSON.stringify(docdata.data));
            var rtable = $('table.userdocs').data('footable');
            $('table.userdocs tbody tr').each(function() {
              rtable.removeRow($(this));
            });
            $('table.userdocs thead tr').each(function() {
              rtable.removeRow($(this));
            });
						defDocsTable();
            filldocsData();
            setSuccess($('#errtext1'),"User docs listed below");
          } else  {
            setError($('#errtext1'),docdata.text);
          }
          stopAjaxIcon();
			  },
			  error: function(err) { 
          setError($('#errtext1'),err.text);
          stopAjaxIcon();
			  }
	  });
	  return false;
  });
});
function fillUserProfile()		{
	var formdataObj = JSON.parse($('#formdata').val());
	if (formdataObj != null)    {
		$('#ucoid').val(formdataObj.coid);
		$('#uuserid').val(formdataObj.userid);

		$('#uName').text(formdataObj.username);
		$('#emailid').val(formdataObj.useremailid);
		$('#mobno').val(formdataObj.mobile);
		$('#gender-list').val(formdataObj.gender);
		$('#dob').val(formdataObj.dob);
		$('#bgrp-list').val(formdataObj.bgroup);
		$('#emername').val(formdataObj.emername);
		$('#emerrel').val(formdataObj.emerrel);
		$('#emermob').val(formdataObj.emermob);
		$('#pal1').val(formdataObj.paddr1);
		$('#pal2').val(formdataObj.paddr2);
		$('#pcity').val(formdataObj.pcity);
		$('#ppin').val(formdataObj.ppincode);
		$('#cal1').val(formdataObj.caddr1);
		$('#cal2').val(formdataObj.caddr2);
		$('#ccity').val(formdataObj.ccity);
		$('#cpin').val(formdataObj.cpincode);
		$('#acard').val(formdataObj.aadhaar);
		$('#panno').val(formdataObj.panno);
		$('#ppno').val(formdataObj.ppno);
		$('#ppidate').val(formdataObj.ppidate);
		$('#ppedate').val(formdataObj.ppedate);
		$('#pint-list').select2('val', formdataObj.pint);
		$('#dept').val(getDept(formdataObj.dept));
		$('#role').val(getRole(formdataObj.role));
		$('#loc').val(getLoc(formdataObj.location));
		$('#desg').val(formdataObj.desg);
		$('#jdate').val(formdataObj.doj);
		$('#etype-list').val(formdataObj.emptype);

		$('#noexp-list').select2('val', formdataObj.noexp);
		$('#vprof-list').select2('val', formdataObj.vprof);
		$('#prof-list').select2('val', formdataObj.prof);
		$('#sexp-list').select2('val', formdataObj.sexp);
		$('#exprt-list').select2('val', formdataObj.exprt);

		document.getElementById('emailid').disabled = true;
		document.getElementById('mobno').disabled = true;
		document.getElementById('gender-list').disabled = true;
		document.getElementById('pint-list').disabled = true;
		document.getElementById('dob').disabled = true;
		document.getElementById('bgrp-list').disabled = true;
		document.getElementById('emername').disabled = true;
		document.getElementById('emerrel').disabled = true;
		document.getElementById('emermob').disabled = true;
		document.getElementById('pal1').disabled = true;
		document.getElementById('pal2').disabled = true;
		document.getElementById('pcity').disabled = true;
		document.getElementById('ppin').disabled = true;
		document.getElementById('cal1').disabled = true;
		document.getElementById('cal2').disabled = true;
		document.getElementById('ccity').disabled = true;
		document.getElementById('cpin').disabled = true;
		document.getElementById('acard').disabled = true;
		document.getElementById('panno').disabled = true;
		document.getElementById('ppno').disabled = true;
		document.getElementById('ppidate').disabled = true;
		document.getElementById('ppedate').disabled = true;
		document.getElementById('dept').disabled = true;
		document.getElementById('role').disabled = true;
		document.getElementById('loc').disabled = true;
		document.getElementById('desg').disabled = true;
		document.getElementById('jdate').disabled = true;
		document.getElementById('etype-list').disabled = true;

		document.getElementById('exprt-list').disabled = true;
		document.getElementById('vprof-list').disabled = true;
		document.getElementById('prof-list').disabled = true;
		document.getElementById('sexp-list').disabled = true;
		document.getElementById('noexp-list').disabled = true;

    var randm = (new Date()).toString();
    var imgsrc = ('/userpictul?filename=' + formdataObj.coid + "-" + formdataObj.userid + '&rnd=' + randm);
    $("#userpict").attr('src',imgsrc);
	}
}
function defDocsTable()    {
	$('table.userdocs').data('footable').reset();
	$('table.userdocs thead').append('<tr>');
	$('table.userdocs thead tr').append('<th>Description</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">Category</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">Filename</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.userdocs thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.userdocs thead tr').append('</tr>');
	$('table.userdocs').footable();
}

function filldocsData()		{
	var mesgObj = JSON.parse($('#docsdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.userdocs').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.docdesc != null)
				newRow += obj.docdesc;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.catg)     {
				case '1':
					newRow += 'Educational';
					break;
				case '2':
					newRow += 'Work Experiences';
					break;
				case '3':
					newRow += 'Trainings & Certifications';
					break;
				case '4':
					newRow += 'Personal Documents';
					break;
				case '5':
					newRow += 'Others';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.docfile != null)
				newRow += obj.docfile;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span class="glyphicon glyphicon-eye-open" title="View Document"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td>';

			newRow += '</tr>';

			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}
$(function () {
  $('table.userdocs').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.userdocs').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
  });
});
function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#docsdata").val());
    var docfile = dataObj[rowIndex].docfile;
		var xcoid = $('#ucoid').val();
		var xuserid = $('#uuserid').val();

    var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + "/" + xuserid + "/" + docfile;
    window.open(docsrc);

    //window.open('/uploads/'+ xcoid + "/" + xuserid + "/" + docfile);
    return false;
  }
}
function getLoc(locid)		{
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == locid)
				return obj.oname;
		}
	}
	return "";
}
function getRole(roleid)		{
	var xObj = JSON.parse($('#roledata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == roleid)
				return obj.roletitle;
		}
	}
	return "";
}
function getDept(depid)		{
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == depid)
				return obj.depname;
		}
	}
	return "";
}
function getSkill(skillid)		{
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == skillid)
				return obj.skilltitle;
		}
	}
	return "";
}
function def_noexp()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#noexp-list');
		}
	}
}
function def_vprof()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#vprof-list');
		}
	}
}
function def_prof()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#prof-list');
		}
	}
}
function def_sexp()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#sexp-list');
		}
	}
}
function def_exprt()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#exprt-list');
		}
	}
}

