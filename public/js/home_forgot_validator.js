$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
	$('#reset_btn').click(function() {
		var semail = $("#emailid").val();
    if (semail == "")   {
      $('#emailid').css('border-color', 'red');
      setError($('#errtext2'),'Please input email id');
      return false;
    } else    {
      $('#emailid').css('border-color', 'default');
      clearError($('#errtext2'));
    }
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
      $('#emailid').css('border-color', 'red');
      setError($('#errtext2'),'Please input valid email id');
      return false;
    } else    {
      $('#emailid').css('border-color', 'default');
      clearError($('#errtext2'));
    }

		var verObject = new Object();
		verObject.emailid = semail;
		$.ajax({
      type: 'GET',
			url: '/resetpwd',
			data: verObject,
			dataType: 'json',
			success : function(robj) { 
        if (robj.err == 0)
  				setSuccess($('#errtext2'),robj.text);
        else
  				setError($('#errtext2'),robj.text);
			},
			error: function(eobj) {
				setError($('#errtext2'),eobj.text);
			}
		});
		return false;
	});

});

