$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.devices').footable();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function secode_gen()   {
  var scode = (Math.random()*1e64).toString(36).substr(0,24);;
	$('#secode').val(scode);
}

function clearForm()		{
	$('#dbid').val('');
	$('#devname').val('');
	$('#secode').val('');
	$('#pass1').val('');
	$('#pass2').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.devices').data('footable').reset();
	$('table.devices thead').append('<tr>');
	$('table.devices thead tr').append('<th>Device Name</th>');
	$('table.devices thead tr').append('<th data-hide="phone,tablet">Security Code</th>');
	$('table.devices thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.devices thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.devices thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.devices thead tr').append('</tr>');
	$('table.devices').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.devices').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.devname != null)
				newRow += obj.devname;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.secode != null)
				newRow += obj.secode;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/205newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.devices').data('footable');

			$('table.devices tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.devices tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(function () {
	$('table.devices').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.devices').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#devname').val(obj.devname);
		$('#secode').val(obj.secode);
		$('#pass1').val('');
		$('#pass2').val('');

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.devname= obj.devname;
			dataObj.secode= obj.secode;
			dataObj.pass= obj.password;

      //alert(JSON.stringify(dataObj));

			$.ajax({
				type: 'DELETE',
				url: '/205',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						$('#totusers').val(resp.totusers);
						$('#totlic').val(resp.totlic);
						$('#licv').val(resp.licv);

						// DELETE & RE-CREATE Table 
						var rtable = $('table.devices').data('footable');

						$('table.devices tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.devices tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'users data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs(mode)		{
	var s_devname = $('#devname').val();
	if (s_devname == '')	{
		$('#devname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Device Name');
		$('#devname').focus();
		return false;
	} else    {
		$('#devname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_secode = $('#secode').val();
	if (s_secode == '')	{
		$('#secode').css('border-color', 'red');
		setError($('#errtext1'),'Please Generate a security code');
		$('#secode').focus();
		return false;
	} else    {
		$('#secode').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pass1 = $('#pass1').val();
	if (mode == 'add')		{
		if (s_pass1 == '')	{
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	if (s_pass1 != '')	{
		if (s_pass1.length < 8)   {
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		var s_pass2 = $('#pass2').val();
		if (s_pass2 == '')	{
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass2.length < 8)   {
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass1 != s_pass2)	{
			$('#pass1').css('border-color', 'red');
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Passwords are not same');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.devname= s_devname;
	formdataObj.secode= s_secode;
	formdataObj.pass= s_pass1;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs('add');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/205add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.devices').data('footable');

				$('table.devices tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.devices tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);

				fillUserData();
				SetPagination();
				clearForm();
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs('edit');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/205upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.devices').data('footable');

				$('table.devices tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.devices tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'users data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	
	$('#gen_btn').click(function() 			{
		secode_gen();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;

		def_role();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

