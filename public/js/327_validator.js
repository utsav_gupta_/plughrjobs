$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#jdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#probdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#noexp-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
 		dropdownAutoWidth: true
	});
	$('#vprof-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
	$('#prof-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
	$('#sexp-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
	$('#exprt-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}
function clearForm()		{
	$('#dbid').val('');
	$('#uname').val('');
	$('#email').val('');
	$('#pass1').val('');
	$('#pass2').val('');
	$('#mgr-list').prop('selectedIndex', 0);
	$('#dep-list').prop('selectedIndex', 0);
	$('#role-list').prop('selectedIndex', 0);
	$('#loc-list').prop('selectedIndex', 0);
	$('#desg').val('');
	$('#jdate').val('');
	$('#probdate').val('');
	$('#sal').val('');
	$('#etype-list').prop('selectedIndex', 0);
	$('#noexp-list').select2('val', null);
	$('#vprof-list').select2('val', null);
	$('#prof-list').select2('val', null);
	$('#sexp-list').select2('val', null);
	$('#exprt-list').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_emp()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#mgr-list');
		}
	}
}

function def_dep()    {
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dep-list');
		}
	}
}

function def_role(roleid)    {
	var xObj = JSON.parse($('#roledata').val());
	var seldep = $('#dep-list').val();

	$('#role-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == seldep)		{
				var optObj = new Object;
				optObj.roleid = obj._id;
				optObj.depid = obj.depid;
				optObj.rtype = obj.roletype;
				if (roleid && roleid == optObj.roleid)
					$('<option selected>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
				else
					$('<option>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
			}
		}
	}
}

function def_loc()    {
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#loc-list');
		}
	}
}

function def_skills()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#noexp-list');
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#vprof-list');
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#prof-list');
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#exprt-list');
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#sexp-list');
		}
	}
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Designation</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Reporting to</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Salary (CTC)</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Employment Type</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}

function fillUserData()		{
	var sel4Obj = JSON.parse($('#depdata').val());
	var sel5Obj = JSON.parse($('#roledata').val());
	var sel6Obj = JSON.parse($('#locdata').val());
	var sel16Obj = JSON.parse($('#empdata').val());

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel4Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel4Obj[ctr];
				if (obj.dept == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel5Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel5Obj[ctr];
				if (obj.role == selx._id)	{
					newRow += selx.roletitle;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel6Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel6Obj[ctr];
				if (obj.location == selx._id)	{
					newRow += selx.oname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.desg != null)
				newRow += obj.desg;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel16Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel16Obj[ctr];
				if (obj.mgr == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.doj != null)
				newRow += obj.doj;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.salary != null)
				newRow += obj.salary;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.emptype)     {
				case '1':
					newRow += 'Full-time';
					break;
				case '2':
					newRow += 'Part-time';
					break;
				case '3':
					newRow += 'Freelancing';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'On Probation';
					break;
				case '2':
					newRow += 'Confirmed';
					break;
				case '3':
					newRow += 'On Notice Period';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/327newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.users').data('footable');

			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.users').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.users').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#uname').val(obj.username);
		$('#email').val(obj.useremailid);
		$('#pass1').val('');
		$('#pass2').val('');
		$('#mgr-list').val(obj.mgr);
		$('#dep-list').val(obj.dept);
		def_role(obj.role);
		//$('#role-list').val(obj.role);
		$('#loc-list').val(obj.location);
		$('#desg').val(obj.desg);
		$('#jdate').val(obj.doj);
		$('#probdate').val(obj.probdate);
		$('#sal').val(obj.salary);
		$('#etype-list').val(obj.emptype);
		$('#noexp-list').select2('val', obj.noexp);
		$('#vprof-list').select2('val', obj.vprof);
		$('#prof-list').select2('val', obj.prof);
		$('#sexp-list').select2('val', obj.sexp);
		$('#exprt-list').select2('val', obj.exprt);

		document.getElementById('mgr-list').disabled = true;
		document.getElementById('dep-list').disabled = true;
		document.getElementById('role-list').disabled = true;
		document.getElementById('loc-list').disabled = true;
		document.getElementById('probdate').disabled = true;
		document.getElementById('etype-list').disabled = true;

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.uname= obj.username;
			dataObj.email= obj.useremailid;
			dataObj.pass= obj.password;
			dataObj.mgr= obj.mgr;
			dataObj.dep= obj.dept;
			dataObj.role= obj.role;
			dataObj.loc= obj.location;
			dataObj.desg= obj.desg;
			dataObj.jdate= obj.doj;
			dataObj.probdate= obj.probdate;
			dataObj.sal= obj.salary;
			dataObj.etype= obj.emptype;
			dataObj.noexp= obj.noexp;
			dataObj.vprof= obj.vprof;
			dataObj.prof= obj.prof;
			dataObj.sexp= obj.sexp;
			dataObj.exprt= obj.exprt;

			$.ajax({
				type: 'DELETE',
				url: '/327',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.users').data('footable');

						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'users data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs(mode)		{
	var s_uname = $('#uname').val();
	if (s_uname == '')	{
		$('#uname').css('border-color', 'red');
		setError($('#errtext1'),'Please input User Name');
		$('#uname').focus();
		return false;
	} else    {
		$('#uname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_email = $('#email').val();
	if (s_email != '')	{
		var lastAtPos = s_email.lastIndexOf('@');
		var lastDotPos = s_email.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_email.indexOf('@@') == -1 && lastDotPos > 2 && (s_email.length - lastDotPos) > 2);
		if (!result)	{
			setError($('#errtext1'),'Please input valid email id');
			$('#email').focus();
			return false;
		} else    {
			$('#email').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	var s_pass1 = $('#pass1').val();
	if (mode == 'add')		{
		if (s_pass1 == '')	{
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	if (s_pass1 != '')	{
		if (s_pass1.length < 8)   {
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		var s_pass2 = $('#pass2').val();
		if (s_pass2 == '')	{
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass2.length < 8)   {
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass1 != s_pass2)	{
			$('#pass1').css('border-color', 'red');
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Passwords are not same');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	var s_dep = $('#dep-list').val();
	if (s_dep == null)	{
		$('#dep-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Department');
		$('#dep-list').focus();
		return false;
	} else    {
		$('#dep-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_role = $('#role-list').val();
	var s_role = $('#role-list').val();
	var s_rtype;
	if (s_role != null)	{
		var tmp = JSON.parse(s_role); 
		s_role = tmp.roleid;
		s_rtype = tmp.rtype;
	}
	if (s_role == null)	{
		$('#role-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Role');
		$('#role-list').focus();
		return false;
	} else    {
		$('#role-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_loc = $('#loc-list').val();
	if (s_loc == null)	{
		$('#loc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Location');
		$('#loc-list').focus();
		return false;
	} else    {
		$('#loc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_desg = $('#desg').val();
	if (s_desg == '')	{
		$('#desg').css('border-color', 'red');
		setError($('#errtext1'),'Please input Designation');
		$('#desg').focus();
		return false;
	} else    {
		$('#desg').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_jdate = $('#jdate').val();
	if (s_jdate == '')	{
		$('#jdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input Date Joined');
		$('#jdate').focus();
		return false;
	} else    {
		$('#jdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_probdate = $('#probdate').val();
	if (s_probdate == '')	{
		$('#probdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input probation end Date');
		$('#probdate').focus();
		return false;
	} else    {
		$('#probdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sal = $('#sal').val();
	if (s_sal == '')	{
		$('#sal').css('border-color', 'red');
		setError($('#errtext1'),'Please input Salary (CTC)');
		$('#sal').focus();
		return false;
	} else    {
		$('#sal').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_etype = $('#etype-list').val();
	if (s_etype == null)	{
		$('#etype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Employment Type');
		$('#etype-list').focus();
		return false;
	} else    {
		$('#etype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_mgr = $('#mgr-list').val();
	if (s_etype == null)	{
		$('#mgr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a reporting manager');
		$('#mgr-list').focus();
		return false;
	} else    {
		$('#mgr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_noexp = $('#noexp-list').val();
	var s_vprof = $('#vprof-list').val();
	var s_prof = $('#prof-list').val();
	var s_sexp = $('#sexp-list').val();
	var s_exprt = $('#exprt-list').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.uname= s_uname;
	formdataObj.email= s_email;
	formdataObj.pass= s_pass1;
	formdataObj.mgr= s_mgr;
	formdataObj.dep= s_dep;
	formdataObj.role= s_role;
	formdataObj.rtype= s_rtype;
	formdataObj.loc= s_loc;
	formdataObj.desg= s_desg;
	formdataObj.jdate= s_jdate;
	formdataObj.probdate= s_probdate;
	formdataObj.sal= s_sal;
	formdataObj.etype= s_etype;
	formdataObj.noexp= s_noexp;
	formdataObj.vprof= s_vprof;
	formdataObj.prof= s_prof;
	formdataObj.sexp= s_sexp;
	formdataObj.exprt= s_exprt;
	// status ==> 1 = On Probation 2 = Confirmed 3 = On notice period
	if (s_jdate == s_probdate)
		formdataObj.empsts = 2;
	else
		formdataObj.empsts = 1;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs('add');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/327add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);

				fillUserData();
				SetPagination();

				get_users();
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function get_users()		{
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();

	$.ajax({
		type: 'GET',
		url: '/220users',
		data: formdataObj,
		dataType: 'json',
		success: function(resp) {
			if (resp)   {
				$('#empdata').val(JSON.stringify(resp.empdata));
				def_emp();
				clearForm();
				setSuccess($('#errtext1'),'users data added');
			}
		},
		error: function(err) {
			setError($('#errtext1'), err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs('edit');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/327upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'users data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_dep();
	def_emp();
	def_role();
	def_loc();
	def_skills();

	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();

		document.getElementById('mgr-list').disabled = false;
		document.getElementById('dep-list').disabled = false;
		document.getElementById('role-list').disabled = false;
		document.getElementById('loc-list').disabled = false;
		document.getElementById('probdate').disabled = false;
		document.getElementById('etype-list').disabled = false;

		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;

		def_role();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

