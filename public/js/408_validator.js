$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		$('#cname').text(formdataObj.companyname);
		$('#cwebsite').text(formdataObj.cwebsite);
		$('#abtus').text(formdataObj.aboutus);

    var randm = (new Date()).toString();
    var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + $('#ccoid').text() + "/" + formdataObj.clogo  + "?" + new Date().getTime();
    $("#userpict").attr('src',imgsrc);
	}
}


$(document).ready(function()	{
	showformdata();
});

