$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  // Show current data
  var cuser = JSON.parse($("#formdata").val());
  $("#fname").val(cuser.name);
  $("#mobile").val(cuser.mobile);
  $("#desgn").val(cuser.desgn);
});
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}
$(document).ready(function()	{
   $('#updppr_btn').click(function()   {
      var sfname = $("#fname").val();
      if (sfname == "")	{
         $('#fname').css('border-color', 'red');
         setError($('#errtext1'),'Please input full name');
         return false;
      } else    {
         $('#fname').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      var smobile = $("#mobile").val();
      if (smobile == "")	{
         $('#mobile').css('border-color', 'red');
         setError($('#errtext1'),'Please input mobile number');
         return false;
      } else    {
         $('#mobile').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (smobile.length < 10)   {
         $('#mobile').css('border-color', 'red');
         setError($('#errtext1'),'Please input 10 digit mobile number');
         return false;
      } else    {
         $('#mobile').css('border-color', 'default');
         clearError($('#errtext1'));
      }

      var sdesgn = $("#desgn").val();
      if (sdesgn == "")	{
         $('#desgn').css('border-color', 'red');
         setError($('#errtext1'),'Please input your designation');
         return false;
      } else    {
         $('#desgn').css('border-color', 'default');
         clearError($('#errtext1'));
      }

      var joinObject = new Object();
      joinObject.fname = sfname;
      joinObject.mobile = smobile;
      joinObject.desgn = sdesgn;
      $.ajax({
         type: "POST",
         url: '/302',
         data: joinObject,
         dataType: 'json',
         beforeSend:  function()   {
            startAjaxIcon();
         },
         success: function(robj) {
            if (robj.err == 0)  {
               $("#formdata").val(JSON.stringify(robj.text));
               setSuccess($('#errtext1'),'Profile updated');
               var cuser = JSON.parse($("#formdata").val());
               $("#fname").val(cuser.name);
               $("#mobile").val(cuser.mobile);
               $("#desgn").val(cuser.desgn);
            } else
               setError($('#errtext1'),robj.text);
               stopAjaxIcon();
            },
           error: function(eobj) {
              setError($('#errtext1'),eobj.text);
              stopAjaxIcon();
           }
      });
      return false;
   });
});

