$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('table.onbdlist').footable();
});

function def_emp()    {
	$('#emp-list').empty();
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defMesgTable()    {
	$('table.onbdlist').data('footable').reset();
	$('table.onbdlist thead').append('<tr>');
	$('table.onbdlist thead tr').append('<th>Title</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.onbdlist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.onbdlist thead tr').append('</tr>');
	$('table.onbdlist').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		if (mesgObj.onbdacts)
			var glen = mesgObj.onbdacts.length;
		else
			var glen = 0;
		var rtable = $('table.onbdlist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj.onbdacts[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.desc != null)
				newRow += obj.desc;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
			  case '0':
    			newRow += "Incomplete";
    			break;
			  case '1':
    			newRow += "Completed";
    			break;
			}
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function get_list()		{
	var s_emp = $('#emp-list').val();
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp= s_emp;

	$.ajax({
		type: 'POST',
		url: '/458search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.onbdlist').data('footable');

				$('table.onbdlist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.onbdlist tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_emp();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_list();
		$('#emp-list').prop('disabled', true);
		return false;
	});
	$('#clear_btn').click(function() 			{
		// DELETE & RE-CREATE Table 
		var rtable = $('table.onbdlist').data('footable');
		$('table.onbdlist tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.onbdlist tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('#emp-list').prop('disabled', false);
		return false;
	});
});

