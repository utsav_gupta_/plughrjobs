$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.leavetypes').footable();
	var uid = $('#cuser').text();
});

function getPrevBal(ltype)			{
	var ld = 0.0;
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee
	if (lvbalObj != null)    {
		var glen = lvbalObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = lvbalObj[i];
			if (obj.ltype == ltype)	{
				ld += parseFloat(obj.pbal);
			}
		}
	}
	return ld;
}

function prev_availed(ltype)			{
	var ld = 0.0;
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee
	if (lvbalObj != null)    {
		var glen = lvbalObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = lvbalObj[i];
			if (obj.ltype == ltype)	{
				ld += parseFloat(obj.totav);
			}
		}
	}
	return ld;
}
function leave_availed(ltype)			{
	//alert(ltype);
	var mesgObj = JSON.parse($('#lvdata').val());
	var ld = 0.0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			if (obj.lname == ltype && obj.status != 'rejected')	{
				ld += parseFloat(obj.ltot);
			}
		}
	}
	return ld;
}

function def_emp()    {
	$('#emp-list').empty();
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var nObj = new Object;
			nObj.userid = obj.userid;
			nObj.location = obj.location;
			$('<option>').val(JSON.stringify(nObj)).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defMesgTable()    {
	$('table.leavetypes').data('footable').reset();
	$('table.leavetypes thead').append('<tr>');
	$('table.leavetypes thead tr').append('<th>Leave Name</th>');
	//$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Availing Criteria</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Accrual period</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Accrual days</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Previous balace </th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Accured leaves </th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Previous availed </th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Availed leaves </th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Current Balance </th>');
	$('table.leavetypes thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.leavetypes thead tr').append('</tr>');
	$('table.leavetypes').footable();
}

function fillUserData()		{
	var xObj = JSON.parse($('#codata').val());
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee
	var empObj = JSON.parse($('#empdata').val());

	var codate = new Date(xObj.hrdate.substring(2,6), xObj.hrdate.substring(0,1)-1, '1');

	var tmpdate = empObj[0].doj;
	var tmpdate = tmpdate.split("-");
	var jodate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);

	if (jodate > codate)	{
		var hrmon = parseInt(tmpdate[1]);
		var hrdate = tmpdate[1] + "-" + tmpdate[2];
	}	else		{
		var hrmon = parseInt(xObj.hrdate.substring(0,1));
		var hrdate = xObj.hrdate;
	}

	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var lper = ['year','quarter','month','week'];

	var date = new Date();
	var cmon = date.getMonth()+1;
	var cyr = date.getFullYear();
	var firstDay = new Date(cyr, hrmon-1, 1);
	if (hrmon > cmon)
		cyr++;

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.leavetypes').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.lname != null)
				newRow += obj.lname;
			else
				newRow += '**No Data**';
			newRow += '</td>';

/*			newRow += '<td>';
			if (obj.lcriteria != null)
				newRow += obj.lcriteria;
			else
				newRow += '**No Data**';
			newRow += '</td>';
*/
			newRow += '<td>';
			if (obj.lper != null)
				switch (obj.lper)			{
					case '1':
						newRow += "Annual";
						break;
					case '2':
						newRow += "Quarterly";
						break;
					case '3':
						newRow += "Monthly";
						break;
					case '4':
						newRow += "Weekly";
						break;
				}
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var pb = getPrevBal(obj._id);
			newRow += pb.toString();
			newRow += '</td>';

			var ld = 0.00;
			if (obj.lper == 1 )			{			// Annual
				ld = ld + parseInt(obj.ldays);
			}
			if (obj.lper == 2 )			{			// Quarterly
				if (hrmon > cmon)
					cmon += 8;
				else
					cmon -= 4;
				var qr = Math.floor(cmon / 3);
				ld = ld + Math.floor(obj.ldays * qr);
			}
			if (obj.lper == 3 )			{			// Monthly
				//alert(hrmon);
				//alert(cmon);
				cmon = cmon - hrmon;
				ld = ld + obj.ldays * cmon;
			}
			if (obj.lper == 4 )			{			// Weekly
				if (hrmon > cmon)
					cmon += 8;
				else
					cmon -= 4;
				//alert(firstDay);
				var wb = weeks_between(firstDay, date)+1;
				//alert(wb);
				ld = ld + Math.floor(obj.ldays * wb);
			}
			newRow += '<td>';
			newRow += ld.toString();
			newRow += '</td>';

			var pa = 0.00;
			pa = prev_availed(obj._id);
			newRow += '<td>';
			newRow += pa;
			newRow += '</td>';

			var la = 0.00;
			la = leave_availed(obj._id);
			newRow += '<td>';
			newRow += la;
			newRow += '</td>';

			var cb = 0.00;
			cb = pb + ld - pa - la;
			newRow += '<td>';
			newRow += '<b>'+cb+'</b>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function weeks_between(date1, date2) {
	// The number of milliseconds in one week
	var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
	// Convert both dates to milliseconds
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();
	// Calculate the difference in milliseconds
	var difference_ms = Math.abs(date1_ms - date2_ms);
	// Convert back to weeks and return hole weeks
	return Math.floor(difference_ms / ONE_WEEK);
}

function leave_availed(ltype)			{
	//alert(ltype);
	var mesgObj = JSON.parse($('#lvdata').val());
	var ld = 0.0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			if (obj.lname == ltype && obj.status != 'rejected')	{
				ld += parseFloat(obj.ltot);
			}
		}
	}
	return ld;
}

function get_list()		{
	var s_emp = JSON.parse($('#emp-list').val());
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = s_emp.userid;
	formdataObj.location = s_emp.location;

	$.ajax({
		type: 'POST',
		url: '/410search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#codata').val(JSON.stringify(resp.codata));
				$('#lvdata').val(JSON.stringify(resp.lvdata));
				$('#formdata').val(JSON.stringify(resp.data));
				$('#emplvdata').val(JSON.stringify(resp.emplvdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.leavetypes').data('footable');

				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_emp();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_list();
		return false;
	});
});

/*			if (obj._id == ltype)		{
				$('#lcrt').text(obj.lcriteria);
				$('#laccp').text("Starting " + hrdate +", " + obj.ldays + " days of leaves for every completed "+ lper[obj.lper-1]);
				$('#lavbl').text("");
				var ld = 0.00;
				//alert(ltype);
				ld = getPrevBal(ltype);
				//alert(ld);

				var la = 0.00;
				if (obj.lper == 1 )			{			// Annual
					ld = ld + parseInt(obj.ldays);
					$('#lavbl').text("Total allowed leave days = " + ld);
				}
				if (obj.lper == 2 )			{			// Quarterly
					if (hrmon > cmon)
						cmon += 8;
					else
						cmon -= 4;
					var qr = Math.floor(cmon / 3);
					ld = ld + Math.floor(obj.ldays * qr);
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				if (obj.lper == 3 )			{			// Monthly
					//alert(hrmon);
					//alert(cmon);
					cmon = cmon - hrmon;
					ld = ld + obj.ldays * cmon;
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				if (obj.lper == 4 )			{			// Weekly
					if (hrmon > cmon)
						cmon += 8;
					else
						cmon -= 4;
					//alert(firstDay);
					var wb = weeks_between(firstDay, date)+1;
					//alert(wb);
					ld = ld + Math.floor(obj.ldays * wb);
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				la = leave_availed(ltype);
				$('#ltakn').text("Leave availed till today = " + parseFloat(la));
				$('#lv01').val(parseFloat(ld));
				$('#lv02').val(parseFloat(la));
				$('#lv03').val(0);
			}
		}
	}

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.leavetypes').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var ld = 0.00;
			var la = 0.00;
			if (obj.lper == 1 )			{			// Annual
				ld = obj.ldays;
			}
			if (obj.lper == 2 )			{			// Quarterly
				if (hrmon > cmon)
					cmon += 8;
				else
					cmon -= 4;
				var qr = Math.floor(cmon / 3);
				ld = Math.floor(obj.ldays * qr);
			}
			if (obj.lper == 3 )			{			// Monthly
				if (hrmon > cmon)
					cmon += 8;
				else
					cmon -= 4;
				ld = obj.ldays * cmon;
			}
			if (obj.lper == 4 )			{			// Weekly
				if (hrmon > cmon)
					cmon += 8;
				else
					cmon -= 4;
				var wb = weeks_between(firstDay, date);
				ld = Math.floor(obj.ldays * wb);
			}
			la = leave_availed(obj._id);

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.lname != null)
				newRow += obj.lname;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.lcriteria != null)
				newRow += obj.lcriteria;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.lper != null)
				switch (obj.lper)			{
					case '1':
						newRow += "Annual";
						break;
					case '2':
						newRow += "Quarterly";
						break;
					case '3':
						newRow += "Monthly";
						break;
					case '4':
						newRow += "Weekly";
						break;
				}
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += ld;
			newRow += '</td>';

			newRow += '<td>';
			newRow += la;
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}
*/

