$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function showformdata()		{
	var sel02Obj = JSON.parse($('#skilldata').val());
	var sel03Obj = JSON.parse($('#empdata').val());
	var sel4Obj = JSON.parse($('#depdata').val());
	var sel5Obj = JSON.parse($('#roledata').val());
	var sel6Obj = JSON.parse($('#locdata').val());

	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		$('#uid').val(formdataObj.userid);
		$('#uname').val(formdataObj.username);

		var dblen = sel4Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel4Obj[ctr];
			if (formdataObj.dept == selx._id)	{
				$('#dep').val(selx.depname);
				break;
			}
		}
		var dblen = sel5Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel5Obj[ctr];
			if (formdataObj.role == selx._id)		{
				$('#role').val(selx.roletitle);
				break;
			}
		}
		var dblen = sel6Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel6Obj[ctr];
			if (formdataObj.location == selx._id)		{
				$('#loc').val(selx.oname);
				break;
			}
		}

		var dblen = sel03Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel03Obj[ctr];
			if (formdataObj.mgr == selx.userid)	{
				$('#mgr').val(selx.username);
				break;
			}
		}

		$('#desg').val(formdataObj.desg);

		switch (formdataObj.emptype)     {
			case '1':
				$('#etype').val('Full-time');
				break;
			case '2':
				$('#etype').val('Part-time');
				break;
			case '3':
				$('#etype').val('Freelancing');
				break;
		}
		switch (formdataObj.status)     {
			case '1':
				$('#sts').val('On Probation');
				break;
			case '2':
				$('#sts').val('Confirmed');
				break;
			case '3':
				$('#sts').val('On Notice Period');
				break;
		}
		$('#sdate').val(formdataObj.doj);
		$('#probdate').val(formdataObj.probdate);

		var skilltext = '';
		var datalen = formdataObj.exprt.length;
		if (datalen > 0)			{
			for (var i=0; i<datalen; i++)			{
				data = formdataObj.exprt[i]; 
				var dblen = sel02Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel02Obj[ctr];
					if (data == selx._id)	{
						skilltext += selx.skillname+', ';
					}
				}
			}
		}
		$('#exprt').val(skilltext);

		skilltext = '';
		var datalen = formdataObj.vprof.length;
		if (datalen > 0)			{
			for (var i=0; i<datalen; i++)			{
				data = formdataObj.vprof[i]; 
				var dblen = sel02Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel02Obj[ctr];
					if (data == selx._id)	{
						skilltext += selx.skillname+', ';
					}
				}
			}
		}
		$('#vprof').val(skilltext);

		skilltext = '';
		var datalen = formdataObj.prof.length;
		if (datalen > 0)			{
			for (var i=0; i<datalen; i++)			{
				data = formdataObj.prof[i]; 
				var dblen = sel02Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel02Obj[ctr];
					if (data == selx._id)	{
						skilltext += selx.skillname+', ';
					}
				}
			}
		}
		$('#prof').val(skilltext);

		skilltext = '';
		var datalen = formdataObj.sexp.length;
		if (datalen > 0)			{
			for (var i=0; i<datalen; i++)			{
				data = formdataObj.sexp[i]; 
				var dblen = sel02Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel02Obj[ctr];
					if (data == selx._id)	{
						skilltext += selx.skillname+', ';
					}
				}
			}
		}
		$('#sexp').val(skilltext);

		skilltext = '';
		var datalen = formdataObj.noexp.length;
		if (datalen > 0)			{
			for (var i=0; i<datalen; i++)			{
				data = formdataObj.noexp[i]; 
				var dblen = sel02Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel02Obj[ctr];
					if (data == selx._id)	{
						skilltext += selx.skillname+', ';
					}
				}
			}
		}
		$('#noexp').val(skilltext);
	}
}

$(document).ready(function()	{
	showformdata();
});

