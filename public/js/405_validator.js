$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#dearned').val('');
	$('#dlevel-list').prop('selectedIndex', 0);
	$('#dyear').val('');
	$('#duni').val('');
	$('#dcollege').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.useredu').data('footable').reset();
	$('table.useredu thead').append('<tr>');
	$('table.useredu thead tr').append('<th>Degree Earned</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">Edu Level</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">Completed Year</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">University</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">College</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.useredu thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.useredu thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.useredu thead tr').append('</tr>');
	$('table.useredu').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		if (mesgObj[0].edudata)
			var glen = mesgObj[0].edudata.length;
		else
			var glen = 0;
		var rtable = $('table.useredu').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[0].edudata[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.udegree != null)
				newRow += obj.udegree;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.edulevel)     {
			  case '1':
    			newRow += "High School";
    			break;
			  case '2':
    			newRow += "Higher Secondary School";
    			break;
			  case '3':
    			newRow += "Bachelors Degree";
    			break;
			  case '4':
    			newRow += "Masters Degree";
    			break;
			  case '5':
    			newRow += "UG Diploma";
    			break;
			  case '6':
    			newRow += "PG Diploma";
    			break;
			  case '7':
    			newRow += "MBA";
    			break;
			  case '8':
    			newRow += "Phd";
    			break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.cyear != null)
				newRow += obj.cyear;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.univ != null)
				newRow += obj.univ;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.college != null)
				newRow += obj.college;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/405newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.useredu').data('footable');

			$('table.useredu tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.useredu tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.useredu').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.useredu').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var mesgObj = JSON.parse($('#formdata').val());
		if (mesgObj.length != 0)    {
			var glen = mesgObj[0].edudata.length;

			var rno = parseInt(rowIndex);
			var obj = mesgObj[0].edudata[rno];
			$('#editrow').val(rno-1);

			$('#dbid').val(mesgObj[0]._id);
			$('#dearned').val(obj.udegree);
			$('#dlevel-list').prop('selectedIndex', (obj.edulevel-1));
			$('#dyear').val(obj.cyear);
			$('#duni').val(obj.univ);
			$('#dcollege').val(obj.college);

			document.getElementById('add_btn').disabled = true;
			document.getElementById('upd_btn').disabled = false;
			document.getElementById('cancel_btn').disabled = false;
		}
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[0].edudata[rno];
			dataObj.dbid= fdata[0]._id;
			dataObj.dearned= obj.udegree;
			dataObj.dlevel= obj.edulevel;
			dataObj.dyear= obj.cyear;
			dataObj.duni= obj.univ;
			dataObj.dcollege= obj.college;

			$.ajax({
				type: 'DELETE',
				url: '/405',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.useredu').data('footable');

						$('table.useredu tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.useredu tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'useredu data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_dearned = $('#dearned').val();
	if (s_dearned == '')	{
		$('#dearned').css('border-color', 'red');
		setError($('#errtext1'),'Please input Degree Earned');
		$('#dearned').focus();
		return false;
	} else    {
		$('#dearned').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dlevel = $('#dlevel-list').val();
	if (s_dlevel == null)	{
		$('#dlevel-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Edu Level');
		$('#dlevel-list').focus();
		return false;
	} else    {
		$('#dlevel-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dyear = $('#dyear').val();
	if (s_dyear == '')	{
		$('#dyear').css('border-color', 'red');
		setError($('#errtext1'),'Please input Completed Year');
		$('#dyear').focus();
		return false;
	} else    {
		$('#dyear').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_duni = $('#duni').val();
	if (s_duni == '')	{
		$('#duni').css('border-color', 'red');
		setError($('#errtext1'),'Please input University');
		$('#duni').focus();
		return false;
	} else    {
		$('#duni').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dcollege = $('#dcollege').val();
	if (s_dcollege == '')	{
		$('#dcollege').css('border-color', 'red');
		setError($('#errtext1'),'Please input College');
		$('#dcollege').focus();
		return false;
	} else    {
		$('#dcollege').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.dearned= s_dearned;
	formdataObj.dlevel= s_dlevel;
	formdataObj.dyear= s_dyear;
	formdataObj.duni= s_duni;
	formdataObj.dcollege= s_dcollege;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/405add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.useredu').data('footable');

				$('table.useredu tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.useredu tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'useredu data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
	formdataObj.arrIndex = $('#editrow').val();

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length == 0)
		return false;
	else		{
		var obj = mesgObj[0].edudata[$('#editrow').val()];
		formdataObj.old_dearned= obj.udegree;
		formdataObj.old_dlevel= obj.edulevel;
		formdataObj.old_dyear= obj.cyear;
		formdataObj.old_duni= obj.univ;
		formdataObj.old_dcollege= obj.college;
	}

	$.ajax({
		type: 'POST',
		url: '/405upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.useredu').data('footable');

				$('table.useredu tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.useredu tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'useredu data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

