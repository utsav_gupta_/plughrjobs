$(document).ready(function()	{
	$('#home1-signin').click(function() {
		$("#loginUserId").val('');
    $('#loginUserId').css('border-color', 'default');
		$("#loginPassword").val('');
    $('#loginPassword').css('border-color', 'default');
    clearError($('#errtext3'));
	  $('#loginModal').modal();
		return false;
	});
	$('#showresend_btn').click(function() {
    window.location = "/showcoverify";
	});
	$('#coverify').click(function() {
    window.location = "/showcoverify";
	});
  $('#verify_btn').click(function() {
    var sverid = $("#verID").val();
    if (sverid == "")   {
       $('#verID').css('border-color', 'red');
       setError($('#errtext1'),'Please input verification code');
       return false;
    } else    {
       $('#verID').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (sverid.length < 16 )   {
       $('#verID').css('border-color', 'red');
       setError($('#errtext1'),'Verification code should be 16 characters long');
       return false;
    } else    {
       $('#verID').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var verObject = new Object();
		verObject.verid = sverid;
		$.ajax({
         type: 'GET',
			url: '/verifycode',
			data: verObject,
			dataType: 'json',
			success : function(robj) { 
         if (robj.err == 0)
  				setSuccess($('#errtext1'),robj.text);
         else
  				setError($('#errtext1'),robj.text);
         },
			error: function(eobj) {
				setError($('#errtext1'),eobj.text);
			}
		});
		return false;
	});
	$('#resend_btn').click(function() {
		var semail = $("#emailid").val();
      if (semail == "")   {
         $('#emailid').css('border-color', 'red');
         setError($('#errtext2'),'Please input email id');
         return false;
      } else    {
         $('#emailid').css('border-color', 'default');
         clearError($('#errtext2'));
      }
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
         $('#emailid').css('border-color', 'red');
         setError($('#errtext2'),'Please input valid email id');
         return false;
      } else    {
         $('#emailid').css('border-color', 'default');
         clearError($('#errtext2'));
      }
		var verObject = new Object();
		verObject.emailid = semail;
		$.ajax({
      type: 'GET',
			url: '/resendverid',
			data: verObject,
			dataType: 'json',
			success : function(robj) { 
         if (robj.err == 0)
  				setSuccess($('#errtext2'),robj.text);
         else
  				setError($('#errtext2'),robj.text);
			},
			error: function(eobj) {
				setError($('#errtext2'),eobj.text);
			}
		});
		return false;
	});
	$('#signin_btn').click(function() {
    var suserid = $("#loginUserId").val();
    if (suserid == "")   {
      $('#loginUserId').css('border-color', 'red');
      setError($('#errtext3'),'Please input email id');
      return false;
    } else    {
      $('#loginUserId').css('border-color', 'default');
      clearError($('#errtext3'));
    }
		var lastAtPos = suserid.lastIndexOf('@');
		var lastDotPos = suserid.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && suserid.indexOf('@@') == -1 && lastDotPos > 2 && (suserid.length - lastDotPos) > 2);
		if (!result)	{
       $('#loginUserId').css('border-color', 'red');
       setError($('#errtext3'),'Please input valid email id');
       return false;
    } else    {
       $('#loginUserId').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var spwd = $("#loginPassword").val();
    if (!spwd)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Please input password');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }
    if (spwd.length < 8)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var loginObject = new Object();
		loginObject.userid = suserid;
		loginObject.pwd = spwd;

		//alert(JSON.stringify(loginObject));

		$.ajax({
		  url: '/login',
		  data: loginObject,
		  dataType: 'json',
      beforeSend:  function()   {
        startAjaxIcon();
      },
      success : function(robj) { 
        //alert(JSON.stringify(robj));
        if (robj.err == false)    {
          //alert('1');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 0)    {
          //alert('2');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 99)    {
          //alert('3');
	        window.location.href = robj.redirect+"?coid=" + robj.coid+"&userid="+ robj.userid;
        } else    {
          //alert('4');
		      setError($('#errtext3'),robj.text);
          stopAjaxIcon();
        }
          //alert('5');
			},
			error: function(eobj) {
				setError($('#errtext3'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});

