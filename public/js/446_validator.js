/*
	// Status values
	1 = "Submitted"
	2 = "Accepted"
	3 = "Rejected"
	4 = "Withdrawn"
*/

$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.exits').footable();
});

function def_replist()    {
	var xObj = JSON.parse($('#repdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#rep-list');
		}
	}
}

function getEact(eactid)		{
	var xObj = JSON.parse($('#eactdata').val());
	var rval = '';
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var fnd = eactid.indexOf(obj._id);
			if (fnd != -1)
				rval += obj.title + ', ';
		}
	}
	return rval;
}


function getUser(usrid)		{
	var xObj = JSON.parse($('#userdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == usrid)
				return obj.username;
		}
	}
	return "";
}

function defMesgTable()    {
	$('table.exits').data('footable').reset();
	$('table.exits thead').append('<tr>');
	$('table.exits thead tr').append('<th>Employee</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Reason</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Last working day</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Take-over person</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Exit Activites</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.exits thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.exits thead tr').append('</tr>');
	$('table.exits').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#userdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.exits').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.userid != null)
				newRow += getUser(obj.userid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.reason)     {
				case '1':
					newRow += 'Better Career Prospects';
					break;
				case '2':
					newRow += 'Better Role';
					break;
				case '3':
					newRow += 'Better Salary';
					break;
				case '4':
					newRow += 'Work-Life Balance';
					break;
				case '5':
					newRow += 'Dream Job Offer';
					break;
				case '6':
					newRow += 'Family circumstances';
					break;
				case '7':
					newRow += 'Personal Reasons';
					break;
				case '8':
					newRow += 'Getting Married';
					break;
				case '9':
					newRow += 'Higher Education';
					break;
				case '10':
					newRow += 'Health Reasons';
					break;
				case '11':
					newRow += 'Full-time position';
					break;
				case '12':
					newRow += 'Maternity related';
					break;
				case '13':
					newRow += 'Not returning from Maternity leave';
					break;
				case '14':
					newRow += 'Other Reasons';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.ldate != null)
				newRow += obj.ldate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.repluserid == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.eact != null)
				newRow += getEact(obj.eact);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'Submitted';
					break;
				case '2':
					newRow += 'Approved';
					break;
				case '3':
					newRow += 'Rejected';
					break;
				case '4':
					newRow += 'Withdrawn';
					break;
			}
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function GetValidInputs()		{
	var s_repl = $('#rep-list').val();
	if (s_repl == null)	{
		$('#repl-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a direct reportee');
		$('#repl-list').focus();
		return false;
	} else    {
		$('#repl-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rep = s_repl;
	return formdataObj;
}

function get_exits()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/446search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				if (resp.data.length > 0)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#userdata').val(JSON.stringify(resp.data1));
					$('#eactdata').val(JSON.stringify(resp.data2));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.exits').data('footable');
					$('table.exits tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.exits tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					fillUserData();
					setSuccess($('#errtext1'),'All Skip level Resignations listed below');
				} else	{
					// DELETE & RE-CREATE Table 
					var rtable = $('table.exits').data('footable');
					$('table.exits tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.exits tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					setError($('#errtext1'),"No active resignations");
				}
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_replist();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_exits();
		return false;
	});
});

