var currQuestion = 0;
var formdataObj;
var totQ = 0;
var userAns = [];
var ansData = new Array();

$(function() {
	$('table.anstable').footable();
	$(document).ajaxStart(function()   {
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
});
function clearForm()		{
	$('#qtext').text('');
	$('#htext').text('');
	$("#choices").empty();

	clearError($('#errtext1'));
	document.getElementById('prev_btn').visible = false;
	document.getElementById('next_btn').visible = false;
}
function showWelcome()		{
	var txtWelcome = "This induction exam consists of " + totQ + " questions. Some questions have only one answer and some have multiple answers.";

	$('#qtext').text(txtWelcome);
	$('#htext').text('Press Start when ready to take the examination');
	clearError($('#errtext1'));

	$('#next_btn').html('Start');
	$('#prev_btn').hide();
	currQuestion = 0;
}

function showQuestion()			{
	$('#qtext').text(formdataObj[currQuestion-1].qtext);
	var ctr = formdataObj[currQuestion-1].answer.length;
	if ( ctr > 1)		{
		$('#htext').text('Please select all applicable answers');
		$("#choices").empty();
		$("#choices").append("<h5><input type='checkbox' name='answers' id='1' value='1'>" + formdataObj[currQuestion-1].a1text + "</h5>");
		$("#choices").append("<h5><input type='checkbox' name='answers' id='2' value='2'>" + formdataObj[currQuestion-1].a2text + "</h5>");
		$("#choices").append("<h5><input type='checkbox' name='answers' id='3' value='3'>" + formdataObj[currQuestion-1].a3text + "</h5>");
		$("#choices").append("<h5><input type='checkbox' name='answers' id='4' value='4'>" + formdataObj[currQuestion-1].a4text + "</h5>");

		if (userAns[currQuestion-1] )	{
			userAns[currQuestion-1].forEach(function(i)			{
				document.getElementById(i).checked = true;
			});
		}
	} else	{
		$('#htext').text('Please select any one answer');
		$("#choices").empty();
		$("#choices").append("<h5><input type='radio' name='answers' id='1' value='1'>" + formdataObj[currQuestion-1].a1text + "</h5>");
		$("#choices").append("<h5><input type='radio' name='answers' id='2' value='2'>" + formdataObj[currQuestion-1].a2text + "</h5>");
		$("#choices").append("<h5><input type='radio' name='answers' id='3' value='3'>" + formdataObj[currQuestion-1].a3text + "</h5>");
		$("#choices").append("<h5><input type='radio' name='answers' id='4' value='4'>" + formdataObj[currQuestion-1].a4text + "</h5>");

		if (userAns[currQuestion-1] )	{
			document.getElementById(userAns[currQuestion-1]).checked = true;
		}
	}
	clearError($('#errtext1'));
}

function showPrevQuestion()		{
	currQuestion--;
	var ctr = formdataObj[currQuestion].answer.length;

	showQuestion();
	if (currQuestion > totQ)
		$('#next_btn').hide();
	else
		$('#next_btn').show();

	if (currQuestion > 1)
		$('#prev_btn').show();
	else
		$('#prev_btn').hide();

	if (currQuestion == totQ)
		$('#next_btn').html('End Exam');
	else
		$('#next_btn').html('Next');
}
function showNextQuestion()		{
	currQuestion++;

	showQuestion();

	if (currQuestion > totQ)
		$('#next_btn').hide();
	else
		$('#next_btn').show();

	if (currQuestion > 1)
		$('#prev_btn').show();
	else
		$('#prev_btn').hide();

	if (currQuestion == totQ)		{
		$('#next_btn').html('End Exam');
	} else
		$('#next_btn').html('Next');
}

$(document).ready(function()	{
	formdataObj = JSON.parse($('#formdata').val());
	totQ = $('#totqs').val();

	for (var i =0;i < totQ;i++)		{
		userAns.push('');
	}
	showWelcome();

	$('#prev_btn').click(function() 			{
		if (currQuestion > 0)	{
			var selAns = $("input[name=answers]:checked").val();
			if (!selAns)			{
			  setError($('#errtext1'),'Please select an Answer');
			} else	{
				var tmp = [];
				var ctr = formdataObj[currQuestion-1].answer.length;
				if ( ctr > 1)		{
					$("input[name=answers]:checked").each(function()			{
						tmp.push(this.value);
					});
					userAns[currQuestion-1] = tmp;
				} else		{
					var selAns = $("input[name=answers]:checked").val();
					userAns[currQuestion-1] = selAns;
				}
				showPrevQuestion();
		  }
		}	else
			showPrevQuestion();
	});
	$('#next_btn').click(function() 			{
		if ($('#next_btn').html() == 'Save Results')	{
			var s_fdbk = $('#feedback').val();
			if (s_fdbk == '')	{
				setError($('#errtext1'),'Please provide your feedback');
			} else	{
				save_results(s_fdbk);
			}
			return false;
		}
		if ($('#next_btn').html() == 'End Exam')	{
			var selAns = $("input[name=answers]:checked").val();
			if (!selAns)			{
				setError($('#errtext1'),'Please select an Answer');
			} else	{
				var tmp = [];
				var ctr = formdataObj[currQuestion-1].answer.length;
				if ( ctr > 1)		{
					$("input[name=answers]:checked").each(function()			{
						tmp.push(this.value);
					});
					userAns[currQuestion-1] = tmp;
				} else		{
					var selAns = $("input[name=answers]:checked").val();
					userAns[currQuestion-1] = selAns;
				}

				var mesg = "Are you sure you want to end this exam?";
				alertify.set({ labels: {
					ok     : "End Exam ",
					cancel : "Don't End"
					} 
				});
				alertify.set({ buttonFocus: "cancel" });
				alertify.set({ buttonReverse: true });
				alertify.confirm(mesg, function (e) {
					if (e) {
						FinaliseExam();
					}
				});
			}
		} else		{
			if (currQuestion > 0)	{
				var selAns = $("input[name=answers]:checked").val();
				if (!selAns)			{
					setError($('#errtext1'),'Please select an Answer');
				} else	{
					var tmp = [];
					var ctr = formdataObj[currQuestion-1].answer.length;
					if ( ctr > 1)		{
						$("input[name=answers]:checked").each(function()			{
							tmp.push(this.value);
						});
						userAns[currQuestion-1] = tmp;
					} else		{
						var selAns = $("input[name=answers]:checked").val();
						userAns[currQuestion-1] = selAns;
					}
					showNextQuestion();
				}
			}	else
				showNextQuestion();
		}
	});
});
function FinaliseExam()		{
	clearForm();
	var txtFinal = "Congratulations on completing this Induction Exam";

	$('#qtext').text(txtFinal);
	$('#htext').text('Please find below summary of your effort');
	$("#choices").empty();
	$("#choices").append("<h4><label for='feedback' class='col-lg-2 col-lg-offset-1 control-label text-default'>Comments / Feedback</label></h4>");
	$("#choices").append("<div class='col-lg-8'><textarea id='feedback' rows='4' name='feedback' placeholder='Please provide any feedback you have' class='form-control'></textarea></div>");
	clearError($('#errtext1'));

	defAnsTable();
	fillAnsData();

	$('#next_btn').html('Save Results');
	$('#prev_btn').hide();
	currQuestion = 0;
}
function defAnsTable()    {
	$('table.anstable').data('footable').reset();
	$('table.anstable thead').append('<tr>');
	$('table.anstable thead tr').append('<th>Question</th>');
	$('table.anstable thead tr').append('<th data-hide="phone,tablet">Correct Answer</th>');
	$('table.anstable thead tr').append('<th data-hide="phone,tablet">Your Answer</th>');
	$('table.anstable thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.anstable thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.anstable thead tr').append('</tr>');
	$('table.anstable').footable();
}

function fillAnsData()		{
	var qsObj = JSON.parse($('#formdata').val());

	if (qsObj.length != 0)    {
		var rtable = $('table.anstable').data('footable');
		var rcount = 0;

		for(var i=0; i < totQ; i++)				{
			var newRow = '<tr>';

			newRow += '<td>';
			if (qsObj[i].qtext != null)
				newRow += qsObj[i].qtext;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (qsObj[i].answer != null)			{
				if (qsObj[i].answer.length > 1)			{
					qsObj[i].answer.forEach(function(j)			{
						switch(j)		{
							case '1':
								newRow += qsObj[i].a1text;
								break;
							case '2':
								newRow += qsObj[i].a2text;
								break;
							case '3':
								newRow += qsObj[i].a3text;
								break;
							case '4':
								newRow += qsObj[i].a4text;
								break;
						}
						newRow += ', ';
					});
				} else	{
					switch(parseInt(qsObj[i].answer))		{
						case 1:
							newRow += qsObj[i].a1text;
							break;
						case 2:
							newRow += qsObj[i].a2text;
							break;
						case 3:
							newRow += qsObj[i].a3text;
							break;
						case 4:
							newRow += qsObj[i].a4text;
							break;
					}
				}
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';

			if (userAns[i] != null)			{
				if (userAns[i].length > 1)		{
					userAns[i].forEach(function(j)			{
						switch(parseInt(j))		{
							case 1:
								newRow += qsObj[i].a1text;
								break;
							case 2:
								newRow += qsObj[i].a2text;
								break;
							case 3:
								newRow += qsObj[i].a3text;
								break;
							case 4:
								newRow += qsObj[i].a4text;
								break;
						}
						newRow += ', ';
					});
				} else		{
					switch(parseInt(userAns[i]))		{
						case 1:
							newRow += qsObj[i].a1text;
							break;
						case 2:
							newRow += qsObj[i].a2text;
							break;
						case 3:
							newRow += qsObj[i].a3text;
							break;
						case 4:
							newRow += qsObj[i].a4text;
							break;
					}
				}
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';

			var inlist = true;
			var ansstatus = "";
			if (qsObj[i].answer != null || userAns[i] != null)			{
				if (userAns[i].length == qsObj[i].answer.length)		{
					if (userAns[i].length > 1)		{
						userAns[i].forEach(function(j)			{
							if ( qsObj[i].answer.indexOf(j) == -1 )	{
								inlist = false;
							}
						});
						if (inlist)		{
							newRow += '<a class="corr" href="#"><span style="font-size:16px;color:green" class="glyphicon glyphicon-ok" title="Edit"></span></a>';
							ansstatus = "Correct";
						} else		{
							newRow += '<a class="nocorr" href="#"><span  style="font-size:16px;color:red"" class="glyphicon glyphicon-remove" title="Edit"></span></a>';
							ansstatus = "Not Correct";
						}
					} else	{
						if (parseInt(userAns[i]) == parseInt(qsObj[i].answer))		{
							newRow += '<a class="corr" href="#"><span style="font-size:16px;color:green" class="glyphicon glyphicon-ok" title="Edit"></span></a>';
							ansstatus = "Correct";
						}	else	{
							newRow += '<a class="nocorr" href="#"><span  style="font-size:16px;color:red" class="glyphicon glyphicon-remove" title="Edit"></span></a>';
							ansstatus = "Not Correct";
						}
					}
				} else	{
						newRow += '<a class="nocorr" href="#"><span  style="font-size:16px;color:red" class="glyphicon glyphicon-remove" title="Edit"></span></a>';
						ansstatus = "Not Correct";
				}
			}
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += i+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
			ansData.push({qtext:qsObj[i].qtext, cans:qsObj[i].answer, uans:userAns[i], status: ansstatus});
		}
		rtable.redraw();
	}
}
function save_results(s_fdbk)			{
	var saveObj = new Object;
	saveObj.ansData = ansData;
	saveObj.feedback = s_fdbk;
	
	$.ajax({
		type: 'POST',
		url: '/429save',
		data: saveObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			setSuccess($('#errtext1'),'Answers Saved');
			$('#next_btn').hide();
			$('#prev_btn').hide();
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

