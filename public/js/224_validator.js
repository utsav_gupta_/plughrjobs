$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.wizard').footable();
});

function defWizardTable()    {
	$('table.wizard').data('footable').reset();
	$('table.wizard thead').append('<tr>');
	$('table.wizard thead tr').append('<th>Setup Step</th>');
	$('table.wizard thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.wizard thead tr').append('<th data-hide="phone,tablet">View Form</th>');
	$('table.wizard thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.wizard thead tr').append('</tr>');
	$('table.wizard').footable();
}
function fillWizardData()		{
	var modules = JSON.parse($('#formdata').val());
	var mlen = modules.length;
	var rtable = $('table.wizard').data('footable');
	$('table.wizard tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.wizard tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	var rcount = 0;
	for(var i=0; i<mlen; i++)   {
		var newRow = '<tr>';

		newRow += '<td>';
		newRow += modules[i].text;
		newRow += '</td>';

		if (modules[i].error)
			newRow += '<td class="text-danger">';
		else
			newRow += '<td class="text-success">';
		newRow += modules[i].status;
		newRow += '</td>';

		newRow += '<td>';
		newRow += '<a class="row-delete" href="/' + modules[i].link + '"  target="_blank"><span id = "d'+i+'" class="glyphicon glyphicon-share-alt" title="Goto form"></span></a>';
		newRow += '</td>';

		newRow += '<td id="rowIndex">';
		newRow += rcount+1;
		newRow += '</td></tr>';
		rtable.appendRow(newRow);
		rcount++;
	}
	rtable.redraw();
}

function getStatusData1()    {
	$.ajax ({
		type: 'GET',
		url: '/224s01',
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(stsObj)	{
			var modules = JSON.parse($('#formdata').val());
			if (stsObj != null)			{
				var slen = modules.length;
				for(var x=1; x<slen+1; x++)			{
					modules[x-1].error = stsObj["err"+x];
					modules[x-1].status = stsObj["sts"+x];
					//console.log(modules[x-1].text);
					//console.log(modules[x-1].status);
				}
			}
			$('#formdata').val(JSON.stringify(modules));
			fillWizardData();
			enableLaunch();
			stopAjaxIcon();
		}
	});
	return false;
}

function enableLaunch()	{
	var modules = JSON.parse($('#formdata').val());
	var slen = modules.length;
	var error = false;
	for(var x=1; x<slen+1; x++)			{
		if (modules[x-1].error)
			error = true;
	}
  if (!error)
  	$('#add_btn').hide();
  else
  	$('#add_btn').show();
}

function colaunch()	{
	var mesg = "Going live is irreversible process! Please confirm again";
	alertify.set({ labels: {
		ok     : "Go Live!",
		cancel : "Not now"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			setusers();
		};
	});
}

function setusers()    {
	$.ajax ({
		type: 'POST',
		url: '/224set',
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
			//setSuccess($('#errtext1'),"Going live now - phase 1");
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		},
		success: function(stsObj)	{
			if (!stsObj.err)
				setSuccess($('#errtext1'),stsObj.text);
			else
				setError($('#errtext1'),stsObj.text);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	var modules = [
			{text:'Basic data about company',status:'', type:0,link:'208'},
			{text:'Message to employees', type:0,link:'211'},
			{text:'Office locations', status:'', type:0,link:'209'},
			{text:'Departments', status:'', type:0,link:'210'},
			{text:'Roles', status:'', type:0,link:'217'},
			{text:'Core values', type:0,link:'213'},
			{text:'Corporate Induction presentation', type:0,link:'228'},
			{text:'Corporate Induction Exams', type:0,link:'229'},
			{text:'Holidays list', type:0,link:'222'},
			{text:'Annual Holiday calender', type:0,link:'223'},
			{text:'Leaves Setup', type:0,link:'230'},
			{text:'Performance ratings scale', type:0,link:'235'},
			{text:'Employee skills ratings scale', type:0,link:'206'},
			{text:'Employees', type:0,link:'220'},
			];
	$('#formdata').val(JSON.stringify(modules));

	defWizardTable();
	fillWizardData();
	getStatusData1();
	$('#add_btn').click(function() 			{
		colaunch();
		return false;
	});
});

