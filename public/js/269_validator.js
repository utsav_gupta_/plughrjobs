$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').text('');
	$('#sts-list').prop('selectedIndex', 0);
	$('#frat-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.revperiod').data('footable').reset();
	$('table.revperiod thead').append('<tr>');
	$('table.revperiod thead tr').append('<th>Title</th>');
	$('table.revperiod thead tr').append('<th data-hide="phone,tablet">Starting Date</th>');
	$('table.revperiod thead tr').append('<th data-hide="phone,tablet">Ending Date</th>');
	$('table.revperiod thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.revperiod thead tr').append('<th data-hide="phone,tablet">Final Rating</th>');
	$('table.revperiod thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.revperiod thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.revperiod thead tr').append('</tr>');
	$('table.revperiod').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.revperiod').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.rptitle != null)
				newRow += obj.rptitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			var tpos = obj.sdate.indexOf('T');
			newRow += '<td>';
			if (obj.sdate != null)
				newRow += obj.sdate.substring(0,tpos);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			var tpos = obj.edate.indexOf('T');
			newRow += '<td>';
			if (obj.edate != null)
				newRow += obj.edate.substring(0,tpos);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'Yet to Start';
					break;
				case '2':
					newRow += 'Active';
					break;
				case '3':
					newRow += 'Overdue';
					break;
				case '4':
					newRow += 'Closed';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if ( obj.frating )    {
			  switch (obj.frating)     {
				  case '1':
					  newRow += ' ';
					  break;
				  case '2':
					  newRow += 'Supervisor\'s rating';
					  break;
				  case '3':
					  newRow += 'Skip supervisor\'s rating';
					  break;
				  case '4':
					  newRow += 'Self rating';
					  break;
			  }
		  } else
				  newRow += 'N/A';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/269newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.revperiod').data('footable');

			$('table.revperiod tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.revperiod tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.revperiod').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);

		$('#title').text(obj.rptitle);
		$('#sts-list').val(obj.status);
		$('#frat-list').val(obj.frat);

		document.getElementById('sts-list').disabled = false;
		document.getElementById('frat-list').disabled = false;

		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function GetValidInputs()		{
	var s_sts = $('#sts-list').val();
	if (s_sts == null)	{
		$('#sts-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select Status');
		$('#sts-list').focus();
		return false;
	} else    {
		$('#sts-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_frat = $('#frat-list').val();
	if (s_frat == null)
    s_frat = '';

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.sts= s_sts;
	formdataObj.frat= s_frat;
	return formdataObj;
}


function data_upd()		{
	var mesg = "Do you REALLY want to Update Review period status?";
	alertify.set({ labels: {
		ok     : "Update",
		cancel : "Don't Update"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
      var formdataObj = GetValidInputs();
      if (!formdataObj)
	      return false;

      //alert(JSON.stringify(formdataObj));
      //return false;

      $.ajax({
	      type: 'POST',
	      url: '/269upd',
	      data: formdataObj,
	      dataType: 'json',
	      beforeSend:  function()   {
		      startAjaxIcon();
	      },
	      success: function(resp) {
		      if (!resp.err)   {
			      $('#formdata').val(JSON.stringify(resp.data));
			      $('#totpages').val(JSON.stringify(resp.totpages));

			      // DELETE & RE-CREATE Table 
			      var rtable = $('table.revperiod').data('footable');

			      $('table.revperiod tbody tr').each(function() {
				      rtable.removeRow($(this));
			      });
			      $('table.revperiod tbody tr').each(function() {
				      rtable.removeRow($(this));
			      });
			      $('#editrow').val(-1);
			      //defMesgTable();
			      fillUserData();
			      SetPagination();
			      clearForm();
			      setSuccess($('#errtext1'),'revperiod data updated');
		      } else  {
			      setError($('#errtext1'),o.text);
		      }
		      stopAjaxIcon();
	      },
	      error: function(err) {
		      setError($('#errtext1'),err.responseText);
		      stopAjaxIcon();
	      }
      });
      return false;
		}
	});
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('sts-list').disabled = true;
		document.getElementById('frat-list').disabled = true;

		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

