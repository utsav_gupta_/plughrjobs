$(function () {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selectfile_btn').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').val(fileName);
  });
	$('table.policies').footable();
	var uid = $('#cuser').text();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#pid').val('');
	$('#ptitle').val('');
	$('#selfile').val('');
	document.getElementById('add_btn').disabled = false;
}

function defMesgTable()    {
	$('table.policies').data('footable').reset();
	$('table.policies thead').append('<tr>');
	$('table.policies thead tr').append('<th>Policy ID</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">Policy Title</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">Filename</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.policies thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.policies thead tr').append('</tr>');
	$('table.policies').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.policies').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			newRow += obj.policyid;
			newRow += '</td>';
			newRow += '<td>';
			newRow += obj.policytitle;
			newRow += '</td>';
			newRow += '<td>';
			newRow += obj.docfile;
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span class="glyphicon glyphicon-eye-open" title="View Document"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(function () {
  $('table.policies').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.policies').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
  });
  $('table.policies').footable().on('click', '.row-delete', function(e) {
    e.preventDefault();
    var rtable = $('table.policies').data('footable');
    var row = $(this).parents('tr:first');
    delDoc(e, row[0]["rowIndex"]-1);
  });
});

function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#formdata").val());
    var docfile = dataObj[rowIndex].docfile;
		var xcoid = $('#ccoid').text();
		var xuserid = $('#cuserid').text();

    var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + '/policy/' + docfile;
    window.open(docsrc);

    return false;
  }
}

function delDoc(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var dataObj = JSON.parse($("#formdata").val());
			var docsObject = new Object();
			docsObject.coid = $('#ccoid').text();
			docsObject.userid = $('#cuserid').text();
			docsObject.pid = dataObj[rowIndex].catg;
			docsObject.ptitle = dataObj[rowIndex].docdesc;
			docsObject.filename = dataObj[rowIndex].docfile;
			docsObject.dbid= dataObj[rowIndex]._id;

			//alert(JSON.stringify(docsObject));
			//return false;

			$.ajax({
				type: 'DELETE',
				url: '/polcdocsul',
				data: docsObject,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(o) {
					if (!o.err)   {
						$('#formdata').val(JSON.stringify(o));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.policies').data('footable');

						$('table.policies tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.policies tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						clearForm();
						setSuccess($('#errtext1'),'policies data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()			{
	var s_pid = $('#pid').val();
	if (s_pid == '')	{
		$('#pid').css('border-color', 'red');
		setError($('#errtext1'),'Please input Policy ID');
		return false;
	} else    {
		$('#pid').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ptitle = $('#ptitle').val();
	if (s_ptitle == '')	{
		$('#ptitle').css('border-color', 'red');
		setError($('#errtext1'),'Please input Policy Title');
		return false;
	} else    {
		$('#ptitle').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_filename = $('#filename').val();
	if (s_filename == '')	{
		$('#filename').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select File');
		$('#filename').focus();
		return false;
	} else    {
		$('#filename').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var sfile = $("#selfile").val();
  if (sfile == "")	{
    $('#selfile').css('border-color', 'red');
    setError($('#errtext1'),'Please select a file to upload');
    return false;
  } else    {
    $('#selfile').css('border-color', 'default');
    clearError($('#errtext1'));
  }
	// Check filetype and filesize
	var filename = $("#selfile").val();
  var extensionAllowed=[".jpg",".jpeg",".tiff",".png",".doc",".docx",".pdf"];
  var i = filename.lastIndexOf('.');
  var file_extension= (i < 0) ? '' : filename.substr(i);
	var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

	if(rfound == -1)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),' Only MS Word, PDF documents and image formats (JPG or PNG or TIFF) are allowed');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}
	var fsize = document.getElementById('docfile').files[0].size;
	if ((fsize / 1024) > 5500)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),'Document file should be less than 5MB');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.pid= s_pid;
	formdataObj.ptitle= s_ptitle;
	formdataObj.filename= sfile;
  return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

  $('#submit_btn').click();
  return true;
}

$(document).ready(function()	{
  var options = {
    beforeSend: function()  {
      clearError($('#errtext1'));
      startAjaxIcon();
    },
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.policies').data('footable');

				$('table.policies tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.policies tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
				clearForm();
				setSuccess($('#errtext1'),'New Document uploaded');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
  	complete: function(response)    {
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#errtext1'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#adddocs-form").ajaxForm(options);

	defMesgTable();
	fillUserData();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
});

