$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#frevdate').datepicker({ dateFormat: 'dd-mm-yy'});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		$('#frevdate').val(formdataObj.frevdate);
		$('#revafter-list').prop('selectedIndex', (formdataObj.revafter-1));
	}
}

function GetValidInputs()			{
	var s_frevdate = $('#frevdate').val();
	if (s_frevdate == '')	{
		$('#frevdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input First Review Date');
		return false;
	} else    {
		$('#frevdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_revafter = $('#revafter-list').val();
	if (s_revafter == '')	{
		$('#revafter-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Review Thereafter');
		return false;
	} else    {
		$('#revafter-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.frevdate= s_frevdate;
	formdataObj.revafter= s_revafter;
  return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/312',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(o) { 
			if (!o.err)     {
				setSuccess($('#errtext1'),'data added');
				$('#formdata').val(JSON.stringify(o));
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) { 
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	showformdata();
	$('#upd_btn').click(function() 			{
		data_add();
	});
});

