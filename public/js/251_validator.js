$(function() {
	$(document).ajaxStart(function(){
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
	$('table.roles').footable();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dept-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('searchs_btn').disabled = false;
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}

function defMesgTable()    {
	$('table.roles').data('footable').reset();
	$('table.roles thead').append('<tr>');
	$('table.roles thead tr').append('<th>Role Title</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Type</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Skills</th>');
	$('table.roles thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.roles thead tr').append('</tr>');
	$('table.roles').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#deptdata').val());
	var sel4Obj = JSON.parse($('#mhsdata').val());
	var sel5Obj = JSON.parse($('#nhsdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.roles').data('footable');

		// DELETE & RE-CREATE Table 
		var rtable = $('table.roles').data('footable');

		$('table.roles tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.roles tbody tr').each(function() {
			rtable.removeRow($(this));
		});

		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.roletitle != null)
				newRow += obj.roletitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.roletype)     {
				case '1':
					newRow += 'Top Management';
					break;
				case '2':
					newRow += 'Function Head';
					break;
				case '3':
					newRow += 'Team Leader';
					break;
				case '4':
					newRow += 'Front line';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.depid == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mustskills)		{
				var olen = obj.mustskills.length;
				var dblen = sel5Obj.length;
				for (var ctr = 0; ctr < olen; ctr++)		{
					var selx = obj.mustskills[ctr];
					for (var temp = 0;temp < dblen; temp++)		{
						var inx = sel5Obj[temp];
						if (selx == inx._id)	{
							newRow += inx.skillname+ ', ';
							break;
						}
					}
					if (temp == dblen)
						newRow += '*' + selx + '*' + ', ';
				}
			}
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/251newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.roles').data('footable');

			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_dept();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_roles();
		return false;
	});
});

function GetValidInputs()			{
	var s_dept = $('#dept-list').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dept= s_dept;

  return formdataObj;
}

function get_roles()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/251search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#mhsdata').val(JSON.stringify(resp.mhsdata));
				$('#nhsdata').val(JSON.stringify(resp.nhsdata));
				fillUserData();
				setSuccess($('#errtext1'),'Roles for selected department listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}


