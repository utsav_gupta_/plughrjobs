$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());
	if (formdataObj.length != 0)    {
		$('#blog1-list').val(formdataObj[0]._id);
		$('#blog2-list').val(formdataObj[1]._id);
	}
}

function def_blog()    {
	var xObj = JSON.parse($('#blogsdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#blog1-list');
			$('<option>').val(obj._id).text(obj.title).appendTo('#blog2-list');
		}
	}
}
function GetValidInputs()		{
	var s_blog1 = $('#blog1-list').val();
	if (s_blog1 == null)	{
		$('#blog1-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Blog 1');
		$('#blog1-list').focus();
		return false;
	} else    {
		$('#blog1-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_blog2 = $('#blog2-list').val();
	if (s_blog2 == null)	{
		$('#blog2-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Blog 2');
		$('#blog2-list').focus();
		return false;
	} else    {
		$('#blog2-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if (s_blog1 == s_blog2)	{
		$('#blog1-list').css('border-color', 'red');
		$('#blog2-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select different Blogs');
		$('#blog1-list').focus();
		return false;
	} else    {
		$('#blog1-list').css('border-color', 'default');
		$('#blog2-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.blog1= s_blog1;
	formdataObj.blog2= s_blog2;
	var currObj = JSON.parse($('#formdata').val());
	if (currObj.length != 0)    {
		formdataObj.old1 = currObj[0]._id;
		formdataObj.old2 = currObj[1]._id;
	} else	{
		formdataObj.old1 = "";
		formdataObj.old2 = "";
	}
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/261',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(o) { 
			if (!o.err)     {
				setSuccess($('#errtext1'),'Selected blogs published');
				$('#formdata').val(JSON.stringify(o));
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) { 
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_blog();
	showformdata();
	$('#upd_btn').click(function() 			{
		data_add();
	});
});

