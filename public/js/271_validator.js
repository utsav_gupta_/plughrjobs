$(function() {
	$('#vfrom').datepicker({ dateFormat: 'yy-mm-dd'});
	$('#vto').datepicker({ dateFormat: 'yy-mm-dd'});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#value').val('');
	$('#vfrom').val('');
	$('#vto').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.biznum').data('footable').reset();
	$('table.biznum thead').append('<tr>');
	$('table.biznum thead tr').append('<th>Business number title</th>');
	$('table.biznum thead tr').append('<th data-hide="phone,tablet">Value</th>');
	$('table.biznum thead tr').append('<th data-hide="phone,tablet">Valid From</th>');
	$('table.biznum thead tr').append('<th data-hide="phone,tablet">Valid upto</th>');
	$('table.biznum thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.biznum thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.biznum thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.biznum thead tr').append('</tr>');
	$('table.biznum').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.biznum').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.value != null)
				newRow += obj.value;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			var tpos = obj.vfrom.indexOf('T');
			newRow += '<td>';
			if (obj.vfrom != null)
				newRow += obj.vfrom.substr(0,tpos);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			var tpos = obj.vto.indexOf('T');
			newRow += '<td>';
			if (obj.vto != null)
				newRow += obj.vto.substr(0,tpos);
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/271newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.biznum').data('footable');

			$('table.biznum tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.biznum tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.biznum').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.biznum').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#title').val(obj.title);
		$('#value').val(obj.value);

		var tpos = obj.vfrom.indexOf('T');
		$('#vfrom').val(obj.vfrom.substr(0,tpos));
		var tpos = obj.vto.indexOf('T');
		$('#vto').val(obj.vto.substr(0,tpos));

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Please reconfirm action";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
		  var fdata = JSON.parse($('#formdata').val());

		  var dataObj = new Object();
		  dataObj.coid = $('#ccoid').text();
		  dataObj.user = $('#cuser').text();
		  var rno = parseInt(rowIndex,10)+1;
		  var obj = fdata[rno-1];
		  dataObj.dbid= obj._id;
		  dataObj.title= obj.title;
		  dataObj.value= obj.value;
		  dataObj.vfrom= obj.vfrom;
		  dataObj.vto= obj.vto;

		  $.ajax({
			  type: 'DELETE',
			  url: '/271',
			  data: dataObj,
			  dataType: 'json',
			  beforeSend:  function()   {
				  startAjaxIcon();
			  },
			  success: function(resp) {
				  if (!resp.err)   {
					  $('#formdata').val(JSON.stringify(resp.data));
					  $('#totpages').val(JSON.stringify(resp.totpages));

					  // DELETE & RE-CREATE Table 
					  var rtable = $('table.biznum').data('footable');

					  $('table.biznum tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('table.biznum tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('#editrow').val(-1);
					  //defMesgTable();
					  fillUserData();
					  SetPagination();
					  clearForm();
					  setSuccess($('#errtext1'),'biznum data deleted');
				  } else  {
					  setError($('#errtext1'),o.text);
				  }
				  stopAjaxIcon();
			  },
			  error: function(err) {
				  setError($('#errtext1'),err.responseText);
				  stopAjaxIcon();
			  }
		  });
    }
		return false;
	});
}
function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Business number title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_value = $('#value').val();
	if (s_value == '')	{
		$('#value').css('border-color', 'red');
		setError($('#errtext1'),'Please input Value');
		$('#value').focus();
		return false;
	} else    {
		$('#value').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_vfrom = $('#vfrom').val();
	if (s_vfrom == '')	{
		$('#vfrom').css('border-color', 'red');
		setError($('#errtext1'),'Please input Valid From');
		$('#vfrom').focus();
		return false;
	} else    {
		$('#vfrom').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_vto = $('#vto').val();
	if (s_vto == '')	{
		$('#vto').css('border-color', 'red');
		setError($('#errtext1'),'Please input Valid upto');
		$('#vto').focus();
		return false;
	} else    {
		$('#vto').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.title= s_title;
	formdataObj.value= s_value;
  formdataObj.vfrom = s_vfrom;
  formdataObj.vto = s_vto;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/271add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.biznum').data('footable');

				$('table.biznum tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.biznum tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'biznum data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/271upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.biznum').data('footable');

				$('table.biznum tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.biznum tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'biznum data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

