$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.users').footable();
});

function def_aprslid()    {
	var xObj = JSON.parse($('#aprsldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.aprtitle).appendTo('#aprslid-list');
		}
	}
}

function def_deptlist()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Self-Rating</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Supervisor Rating</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Skip Supervisor Rating</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">HR Rating</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Peer Rating</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Team member Rating</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
    return -1;
}

function consolidate_data()		{
  var tabledata = [];
	var userObj = JSON.parse($('#userdata').val());
	var dataObj = JSON.parse($('#formdata').val());
	if (dataObj != null && userObj != null)    {
		var glen = userObj.length;
		for(var i=0; i<glen; i++)   {
			var usrobj = userObj[i];
			tabledata.push({userid:usrobj.userid,username:usrobj.username, self:true, supr:true, skip:true, hr:true, peer:true, team:true });
		}
    //console.log(tabledata);
		var glen = dataObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = dataObj[i];
			var result = tabledata.getIndexBy("userid", obj.userid);

      if (obj.selfaprl == false || obj.selfratg == '')  {
        tabledata[result].self = false;
      }
      if (obj.supraprl == false || obj.suprratg == '')  {
        tabledata[result].supr = false;
      }
      if (obj.skipaprl == false || obj.skipratg == '')  {
        tabledata[result].skip = false;
      }
      if (obj.hraprl == false || obj.hrratg == '')  {
        tabledata[result].hr = false;
      }
      if (obj.peeraprl == false || obj.peerratg == '')  {
        tabledata[result].peer = false;
      }
      if (obj.teamaprl == false || obj.teamratg == '')  {
        tabledata[result].team = false;
      }
		}
    //console.log(tabledata);
	}
	fillUserData(tabledata);
}

function fillUserData(mesgObj)		{
	//var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.self == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td>';
			if (obj.supr == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td>';
			if (obj.skip == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td>';
			if (obj.hr == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td>';
			if (obj.peer == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td>';
			if (obj.team == true)
				newRow += "<span class='glyphicon glyphicon-ok status-green'></span>";
			else
				newRow += "<span class='glyphicon glyphicon-remove status-red'></span>";
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function GetValidInputs()		{
	var s_dept = $('#dept-list').val();
	if (s_dept == null)	{
		$('#dept-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a department');
		$('#dept-list').focus();
		return false;
	} else    {
		$('#dept-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_aprslid = $('#aprslid-list').val();
	if (s_aprslid == null)	{
		$('#aprslid-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a department');
		$('#aprslid-list').focus();
		return false;
	} else    {
		$('#aprslid-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.deptid = s_dept;
	formdataObj.aprslid = s_aprslid;
	return formdataObj;
}

function get_team()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/273search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				if (resp.data.length > 0)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#userdata').val(JSON.stringify(resp.users));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
          consolidate_data();
					//fillUserData();
					setSuccess($('#errtext1'),'Appraisal status for selected department are as below');
				} else	{
					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					setError($('#errtext1'),"No users defined for department");
				}
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_deptlist();
	def_aprslid();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_team();
		return false;
	});
});

