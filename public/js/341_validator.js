$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.usergoals').footable();
});
function clearForm()		{
	//$('#dbid').val('');
	clearError($('#errtext1'));
}

function disableForm1()		{
	document.getElementById('rperiod-list').disabled = true;
	document.getElementById('dep-list').disabled = true;

	document.getElementById('modal_btn').disabled = false;
	document.getElementById('search_btn').disabled = true;
	document.getElementById('clear_btn').disabled = false;
}
function enableForm1()		{
	document.getElementById('rperiod-list').disabled = false;
	document.getElementById('dep-list').disabled = false;

	document.getElementById('modal_btn').disabled = true;
	document.getElementById('search_btn').disabled = false;
	document.getElementById('clear_btn').disabled = false;
}

function clearTable()		{
	var rtable = $('table.usergoals').data('footable');

	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status == 2)
				$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function def_dep()    {
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dep-list');
		}
	}
}

function getMgr(mgrid)		{
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == mgrid)
				return obj.username;
		}
	}
	return "";
}

function getMgrID(mgrname)		{
	if (mgrname == "")
		return "";
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.username.indexOf(mgrname) > -1)
				return obj.userid;
		}
	}
	return "";
}

function defMesgTable()    {
	$('table.usergoals').data('footable').reset();
	$('table.usergoals thead').append('<tr>');
	$('table.usergoals thead tr').append('<th>User Name</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Manager</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Self rating</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Manager rating</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Skip rating</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Final rating</th>');
	$('table.usergoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.usergoals thead tr').append('</tr>');
	$('table.usergoals').footable();
}


function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.usergoals').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			newRow += getMgr(obj.userid);
			newRow += '</td>';

			newRow += '<td>';
			newRow += getMgr(obj.mgr);
			newRow += '</td>';

			newRow += '<td>';
			newRow += obj.srat;
			newRow += '</td>';

			newRow += '<td>';
			newRow += obj.mrat;
			newRow += '</td>';

			newRow += '<td>';
			newRow += obj.krat;
			newRow += '</td>';

			newRow += '<td>';
			newRow += obj.frat;
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}

function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/341newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(resp)	{
			if (!resp.err)			{
				$('#formdata').val(JSON.stringify(resp.data0));
				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				fillUserData();
			}
			stopAjaxIcon();
		}
	});
	return false;
}

function get_empl()		{
	var s_rperiod = $('#rperiod-list').val();
	if (s_rperiod == null)	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a Review period');
		$('#rperiod-list').focus();
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dep = $('#dep-list').val();
	if (s_dep == null)	{
		$('#dep-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a Department');
		$('#dep-list').focus();
		return false;
	} else    {
		$('#dep-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod = s_rperiod;
	formdataObj.dep = s_dep;

	$.ajax({
		type: 'POST',
		url: '/341search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));

				fillUserData();
				SetPagination();
				disableForm1();
				setSuccess($('#errtext1'),'Appraisal data for team members of selected department are listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function recalc_appr()		{
	var s_rperiod = $('#rperiod-list').val();
	if (s_rperiod == null)	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a Review period');
		$('#rperiod-list').focus();
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dep = $('#dep-list').val();
	if (s_dep == null)	{
		$('#dep-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a Department');
		$('#dep-list').focus();
		return false;
	} else    {
		$('#dep-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod = s_rperiod;
	formdataObj.dep = s_dep;

	$.ajax({
		type: 'POST',
		url: '/341recalc',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});

				fillUserData();
				SetPagination();
				disableForm1();
				setSuccess($('#errtext1'),'Appraisal data for team members of selected department are listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function ShowModal()		{
  $('#RatingModal').modal();
  return false;
}

function save_app()		{
	var s_opt = $('input[name=opt]:checked').val();
	if (!s_opt)	{
		$('#opt').css('border-color', 'red');
		setError($('#errtext3'),'Please select a valid Option');
		$('#opt').focus();
		return false;
	} else    {
		$('#opt').css('border-color', 'default');
		clearError($('#errtext3'));
	}

	var ans = confirm("This action will overwrite existing appraisal data for the department. Proceed?")
	if (!ans)	{
		$('#RatingModal').modal('hide');
		return false;
	}

	var dataObj = JSON.parse($('#formdata').val());
	if (dataObj != null)    {
		var s_rperiod = $('#rperiod-list').val();
		var s_dep = $('#dep-list').val();

		var glen = dataObj.length;
		for(var i=0; i<glen; i++)   {
			dataObj[i].coid = $('#ccoid').text();
			dataObj[i].rperiod = s_rperiod;
			switch (s_opt)			{
				case '1':
					dataObj[i].frat = dataObj[i].mrat;
					break
				case '2':
					dataObj[i].frat = dataObj[i].krat;
					break
				case '3':
					var frt = (parseFloat(dataObj[i].srat) + parseFloat(dataObj[i].mrat) + parseFloat(dataObj[i].krat));
					var frt = (frt / 3).toFixed(1);
					dataObj[i].frat = frt;
					break
				case '4':
					var frt = (parseFloat(dataObj[i].mrat) + parseFloat(dataObj[i].krat));
					var frt = (frt / 2).toFixed(1);
					dataObj[i].frat = frt;
					break
			}
		}

		var saveObj = new Object;
		saveObj.coid = $('#ccoid').text();
		saveObj.rperiod = s_rperiod;
		saveObj.dept = s_dep;
		saveObj.data = dataObj;

		//alert(JSON.stringify(saveObj));
		//return false;
	
		$.ajax({
			type: 'POST',
			url: '/341save',
			data: saveObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
/*
					$('#formdata').val(JSON.stringify(resp.data0));
					// DELETE & RE-CREATE Table 
					var rtable = $('table.usergoals').data('footable');
					$('table.usergoals tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.usergoals tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					fillUserData();
					SetPagination();
					disableForm1();
*/
					setSuccess($('#errtext1'),'Appraisal data saved');
				} else  {
					setError($('#errtext1'),resp.text);
				}
				$('#RatingModal').modal('hide');
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext3'),err.text);
				stopAjaxIcon();
			}
		});
	}
	return false;
}

$(document).ready(function()	{
	def_rperiod();
	def_dep();
	defMesgTable();

	$('#search_btn').click(function() 		{
		get_empl();
		return false;
	});

	$('#modal_btn').click(function() 		{
		ShowModal();
		return false;
	});
	$('#save_btn').click(function() 		{
		save_app();
		return false;
	});
	$('#recalc_btn').click(function() 		{
		recalc_appr();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		enableForm1();
		return false;
	});
});

