$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.apprslst').footable();
});

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#usr-list');
		}
	}
}

function def_aprsl()    {
	var xObj = JSON.parse($('#aprsldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var aprObj = new Object;
			aprObj.id = obj._id;
			aprObj.rvteam = obj.rvteam;
      if  (obj.rvteam.indexOf('2') != -1 )
  			aprObj.supr = true;
  		else
  			aprObj.supr = false;
			$('<option>').val(JSON.stringify(aprObj)).text(obj.aprtitle).appendTo('#aprsl-list');
		}
	}
}

function aprsl_set()  {
	var txt = $('#aprsl-list').val();
  if (txt != null && txt != '')    {
	  var obj = JSON.parse(txt);
	  if (obj.supr)   {
	    $('#supr_btn').show();
		  clearError($('#errtext1'));
	  } else  {
	    $('#supr_btn').hide();
    	setSuccess($('#errtext1'),'Selected appraisal doesn\'t require supervisor rating');
	  }
  }
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_srat');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
	$('#aprsl-list').prop('selectedIndex', 0);
	$('#usr-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	var rtable = $('table.apprslst').data('footable');
	$('table.apprslst tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.apprslst tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('#formdata').val('');
}

function defMesgTable()    {
	$('table.apprslst').data('footable').reset();
	$('table.apprslst thead').append('<tr>');
	$('table.apprslst thead tr').append('<th>Employee Name</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Appraisal Type</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Appraisal Target</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Target Type</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Current Rating</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">New Rating</th>');
	$('table.apprslst thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.apprslst thead tr').append('</tr>');
	$('table.apprslst').footable();
}

function showgoals(searchtype)			{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.apprslst').data('footable');
		var rcount = 0;
  	var cusr = $('#cuserid').text();
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
      if (obj.atype != '1')     {
		    var newRow = '<tr>';

		    newRow += '<td>';
		    if (obj.username != null)
			    newRow += obj.username;
		    else
			    newRow += '**No Data**';
		    newRow += '</td>';

		    newRow += '<td>';
        switch (searchtype) {
          case '1':
    				newRow += 'Self Appraisal';
    				break;
          case '2':
    				newRow += 'Supervisor Appraisal';
    				break;
          case '3':
    				newRow += 'Skip Supervisor Appraisal';
    				break;
          case '4':
    				newRow += 'HR Appraisal';
    				break;
          case '5':
            if (obj.peeruserid == cusr)
      				newRow += 'Peer Appraisal';
      			else
      				newRow += 'Team member Appraisal';
    				break;
        }
		    newRow += '</td>';

		    newRow += '<td>';
		    if (obj.atitle != null)
			    newRow += obj.atitle;
		    else
			    newRow += '**No Data**';
		    newRow += '</td>';

		    newRow += '<td>';
        switch (obj.atype) {
          case '1':
    				newRow += 'Review';
    				break;
          case '2':
    				newRow += 'KRA';
    				break;
          case '3':
    				newRow += 'Behavioral competencies';
    				break;
          case '4':
    				newRow += 'Corporate values';
    				break;
        }
		    newRow += '</td>';

		    newRow += '<td>';
        switch (searchtype) {
          case '1':
    				newRow += obj.selfratg;
    				break;
          case '2':
    				newRow += obj.suprratg;
    				break;
          case '3':
    				newRow += obj.skipratg;
    				break;
          case '4':
    				newRow += obj.hrratg;
    				break;
          case '5':
            if (obj.peeruserid == cusr)
      				newRow += obj.peerratg;
      			else
      				newRow += obj.teamratg;
    				break;
        }
		    newRow += '</td>';

		    newRow += '<td>';
        switch (searchtype) {
          case '1':
    			  newRow += '<a class="row-edit" href="#"><span id = "s1'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
    				break;
          case '2':
    			  newRow += '<a class="row-edit" href="#"><span id = "s2'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
    				break;
          case '3':
    			  newRow += '<a class="row-edit" href="#"><span id = "s3'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
    				break;
          case '4':
    			  newRow += '<a class="row-edit" href="#"><span id = "s4'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
    				break;
          case '5':
            if (obj.peeruserid == cusr)
      			  newRow += '<a class="row-edit" href="#"><span id = "s5'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      			else
      			  newRow += '<a class="row-edit" href="#"><span id = "s6'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
    				break;
        }
		    newRow += '</td>';

		    newRow += '<td id="rowIndex">';
		    newRow += rcount+1;
		    newRow += '</td></tr>';
		    rtable.appendRow(newRow);
		    rcount++;
      }
		}
		rtable.redraw();
	}
}

$(function () {
	$('table.apprslst').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var stype = row.substring(0,2);
		//alert(stype);
		var rowindex = row.substring(2);
		//alert(rowindex);
		clearError($('#errtext1'));
		editdetails(e, stype, rowindex);
	});
});

function editdetails(evtObj, stype, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
	  if (dataObj)		{
		  $('#editrow').val(rowIndex);
		  var rno = parseInt(rowIndex);
		  var obj = dataObj[rno];

	    var s_rpid = $('#aprsl-list').val();
	    if (s_rpid != '' || s_rpid != null )	{
        var sObj = JSON.parse(s_rpid);
        var apid = sObj.id;

        switch (stype) {
          case 's2':
		        $('#modal_sapid').val(apid);
		        $('#modal_sindx').val(rno);
		        $('#modal_sdbid').val(obj._id);
    		    $('#modal_srat').val(obj.suprratg);
    	      $('#RatingModal').modal();
    				break;
        }
	    }
    }
    return false;
	}
}

function GetValidInputs()			{
	var s_rpid = $('#aprsl-list').val();
	if (s_rpid == '' || s_rpid == null )	{
		$('#aprsl-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an appraisal');
		return false;
	} else    {
		$('#aprsl-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_usr = $('#usr-list').val();
	if (s_usr == '' || s_usr == null )	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an user');
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
  var sObj = JSON.parse(s_rpid);
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.mgrid = $('#cuserid').text();
	formdataObj.userid = s_usr;
	formdataObj.aprslid= sObj.id;
  return formdataObj;
}

function get_supr()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/463supr',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				showgoals('2');
				setSuccess($('#errtext1'),'Selected appraisal listed below');
      	$('#aprsl-list').prop('disabled', true);
      	$('#usr-list').prop('disabled', true);
	      $('#supr_btn').prop('disabled', true);
      	$('#clear_btn').prop('disabled', false);
      	$('#prnt_btn').prop('disabled', false);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function upd_supr_rating()				{
	var s_dbid = $('#modal_sdbid').val();
	var s_indx = $('#modal_sindx').val();
	var s_rating = $('#modal_srat').val();
	var s_apid = $('#modal_sapid').val();

	if (s_rating == '' || s_rating == null )	{
		$('#modal_srat').css('border-color', 'red');
		setError($('#errtext3'),'Please select a rating');
		return false;
	} else    {
		$('#modal_srat').css('border-color', 'default');
		clearError($('#errtext3'));
	}

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = $('#cuserid').text();
	ratingObj.dbid = s_dbid;
	ratingObj.aprslid = s_apid;
	ratingObj.rating = s_rating;

	$.ajax({
		type: 'POST',
		url: '/463srtng',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].suprratg = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
	      var rtable = $('table.apprslst').data('footable');
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
				showgoals('2');

				setSuccess($('#errtext1'),'Supervisor rating updated');
				$('#RatingModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function prnt_aprsl()				{
	var dataObj = JSON.parse($('#formdata').val());
  if (dataObj)		{
	  //var rno = parseInt(rowIndex);
	  //var obj = dataObj[rno];

    var s_rpid = $('#aprsl-list').val();
    if (s_rpid != '' || s_rpid != null )	{
      var sObj = JSON.parse(s_rpid);
      var apid = sObj.id;

      //$('#modal_sapid').val(apid);
      //$('#modal_sindx').val(rno);
      //$('#modal_sdbid').val(obj._id);
	    //$('#modal_srat').val(obj.suprratg);
      $('#prntModal').modal();
    }
  }
}

$(document).ready(function()	{
	def_aprsl();
  aprsl_set();
	def_rlevel();
	def_usr();
  defMesgTable();
	$('#supr_btn').click(function() 			{
		get_supr();
		return false;
	});
	$('#sratngupd_btn').click(function() 			{
		upd_supr_rating();
		return false;
	});
	$('#clear_btn').click(function() 			{
  	$('#aprsl-list').prop('disabled', false);
  	$('#usr-list').prop('disabled', false);
  	$('#clear_btn').prop('disabled', true);
  	$('#prnt_btn').prop('disabled', true);
	  $('#supr_btn').prop('disabled', false);
		$('#aprsl-list').css('border-color', 'default');
		clearError($('#errtext1'));
		clearForm();
    aprsl_set();
		return false;
	});
	$('#prnt_btn').click(function() 			{
		prnt_aprsl();
		return false;
	});
});

