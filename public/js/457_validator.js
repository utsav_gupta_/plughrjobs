$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('table.onbdlist').footable();
});

function defMesgTable()    {
	$('table.onbdlist').data('footable').reset();
	$('table.onbdlist thead').append('<tr>');
	$('table.onbdlist thead tr').append('<th>Title</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.onbdlist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.onbdlist thead tr').append('</tr>');
	$('table.onbdlist').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		if (mesgObj.onbdacts)
			var glen = mesgObj.onbdacts.length;
		else
			var glen = 0;
		var rtable = $('table.onbdlist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj.onbdacts[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.desc != null)
				newRow += obj.desc;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
			  case '0':
    			newRow += "Incomplete";
    			break;
			  case '1':
    			newRow += "Completed";
    			break;
			}
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
});

