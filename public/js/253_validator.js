$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#ans').select2({
		placeholder: 'Select all applicable answers',
		allowClear: true,
		dropdownAutoWidth : true,
	});
	$('table.iquestions').footable();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_indc()    {
	$('#indc-list').empty();
	var xObj = JSON.parse($('#indcdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#indc-list');
		}
	}
}

function enableForm()		{
	$('#qtext').prop('disabled', false);
	$('#a1text').prop('disabled', false);
	$('#a2text').prop('disabled', false);
	$('#a3text').prop('disabled', false);
	$('#a4text').prop('disabled', false);
	$('#ans').prop('disabled', false);
	$('#indc-list').prop('disabled', true);
	$('#search_btn').prop('disabled', true);
}

function disableForm()		{
	$('#qtext').prop('disabled', true);
	$('#a1text').prop('disabled', true);
	$('#a2text').prop('disabled', true);
	$('#a3text').prop('disabled', true);
	$('#a4text').prop('disabled', true);
	$('#ans').prop('disabled', true);
	$('#indc-list').prop('disabled', false);
	$('#search_btn').prop('disabled', false);
}

function clearForm()		{
	$('#dbid').val('');
	$('#qtext').val('');
	$('#a1text').val('');
	$('#a2text').val('');
	$('#a3text').val('');
	$('#a4text').val('');
	$('#ans').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.iquestions').data('footable').reset();
	$('table.iquestions thead').append('<tr>');
	$('table.iquestions thead tr').append('<th>Question</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Answer 1</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Answer 2</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Answer 3</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Answer 4</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Correct Answer(s)</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.iquestions thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.iquestions thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.iquestions thead tr').append('</tr>');
	$('table.iquestions').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.iquestions').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.qtext != null)
				newRow += obj.qtext;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.a1text != null)
				newRow += obj.a1text;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.a2text != null)
				newRow += obj.a2text;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.a3text != null)
				newRow += obj.a3text;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.a4text != null)
				newRow += obj.a4text;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.answer != null)
				newRow += obj.answer;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/253newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.iquestions').data('footable');

			$('table.iquestions tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.iquestions tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.iquestions').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.iquestions').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#qtext').val(obj.qtext);
		$('#a1text').val(obj.a1text);
		$('#a2text').val(obj.a2text);
		$('#a3text').val(obj.a3text);
		$('#a4text').val(obj.a4text);
		$('#ans').select2('val', obj.answer);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.indc = obj.indc;
			dataObj.dbid= obj._id;
			dataObj.qtext= obj.qtext;
			dataObj.a1text= obj.a1text;
			dataObj.a2text= obj.a2text;
			dataObj.a3text= obj.a3text;
			dataObj.a4text= obj.a4text;
			dataObj.ans= obj.answer;

			$.ajax({
				type: 'DELETE',
				url: '/253',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.iquestions').data('footable');

						$('table.iquestions tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.iquestions tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'iquestions data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_qtext = $('#qtext').val();
	if (s_qtext == '')	{
		$('#qtext').css('border-color', 'red');
		setError($('#errtext1'),'Please input Question');
		$('#qtext').focus();
		return false;
	} else    {
		$('#qtext').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_a1text = $('#a1text').val();
	if (s_a1text == '')	{
		$('#a1text').css('border-color', 'red');
		setError($('#errtext1'),'Please input Answer 1');
		$('#a1text').focus();
		return false;
	} else    {
		$('#a1text').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_a2text = $('#a2text').val();
	if (s_a2text == '')	{
		$('#a2text').css('border-color', 'red');
		setError($('#errtext1'),'Please input Answer 2');
		$('#a2text').focus();
		return false;
	} else    {
		$('#a2text').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_a3text = $('#a3text').val();
	if (s_a3text == '')	{
		$('#a3text').css('border-color', 'red');
		setError($('#errtext1'),'Please input Answer 3');
		$('#a3text').focus();
		return false;
	} else    {
		$('#a3text').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_a4text = $('#a4text').val();
	if (s_a4text == '')	{
		$('#a4text').css('border-color', 'red');
		setError($('#errtext1'),'Please input Answer 4');
		$('#a4text').focus();
		return false;
	} else    {
		$('#a4text').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ans = $('#ans').val();
	if (s_ans == '')	{
		$('#ans').css('border-color', 'red');
		setError($('#errtext1'),'Please input Correct Answer');
		$('#ans').focus();
		return false;
	} else    {
		$('#ans').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var i_ans = parseInt(s_ans);
	if (i_ans < 1 || i_ans > 4)			{
		$('#ans').css('border-color', 'red');
		setError($('#errtext1'),'Please input number between 1 to 4');
		$('#ans').focus();
		return false;
	} else    {
		$('#ans').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_indc = $('#indc-list').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.indc= s_indc;
	formdataObj.qtext= s_qtext;
	formdataObj.a1text= s_a1text;
	formdataObj.a2text= s_a2text;
	formdataObj.a3text= s_a3text;
	formdataObj.a4text= s_a4text;
	formdataObj.ans= s_ans;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/253add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.iquestions').data('footable');

				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'iquestions data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/253upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.iquestions').data('footable');

				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'iquestions data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function search_indc()		{
	var s_indc = $('#indc-list').val();
	var sObj = new Object;
	sObj.indc = s_indc;

	$.ajax({
		type: 'GET',
		url: '/253search',
		data: sObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.iquestions').data('footable');
				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.iquestions tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
				SetPagination();
				clearForm();
				enableForm();
				setSuccess($('#errtext1'),'iquestions data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_indc();
	defMesgTable();
	$('#search_btn').click(function() 			{
		search_indc();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		disableForm();
		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		// DELETE Table 
		var rtable = $('table.iquestions').data('footable');
		$('table.iquestions tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.iquestions tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		return false;
	});
});

