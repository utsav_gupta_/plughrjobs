$(function () {
  $('table.license').footable();
});

function SetPagination() {
  var totpages = $("#totmesgs").val();
  if (totpages == 0)
    totpages =1; 
  var options = {
    currentPage: 1,
    totalPages: totpages,
    size:'small',
    bootstrapMajorVersion:3,
    itemTexts: function (type, page, current) {
      switch (type) {
        case "first":
            return "First";
        case "prev":
            return "Previous";
        case "next":
            return "Next";
        case "last":
            return "Last";
        case "page":
            return page;
      }
    },
    onPageClicked: function(e,originalEvent,type,page)    {
      var currPage = $(e.currentTarget).bootstrapPaginator("getPages").current;
      if (currPage != page)   {
        GetNextPage(page);
      }
    }
  }
  $('#licensepages').bootstrapPaginator(options);
}

function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: "GET",
		url: '/unilicnewpage',
		data: mesgObject,
		dataType: 'json',
    beforeSend:  function()   {
      startAjaxIcon();
    },
		success: function(mesgObj)	{
      $("#licdata").val(JSON.stringify(mesgObj));
      var rtable = $('table.license').data('footable');
      $('table.license tbody tr').each(function() {
        rtable.removeRow($(this));
      });
      $('table.license thead tr').each(function() {
        rtable.removeRow($(this));
      });
      defLicenseTable();
      fillLicenseData();
      stopAjaxIcon();
		},
		error: function(eobj) {
			setError($('#errtext1'),eobj.text);
      stopAjaxIcon();
		}
	});
  return false;
}

function defLicenseTable()    {
	$('table.license').data('footable').reset();
	$('table.license thead').append('<tr>');
	$('table.license thead tr').append('<th>Description</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">End Date</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">Users</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.license thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.license thead tr').append('</tr>');
  $('table.license').footable();
}
function fillLicenseData()		{
	var mesgObj = JSON.parse($("#licdata").val());
  var rtable = $('table.license').data('footable');

  var rcount = 0;
  
  var objlen = mesgObj.length;
  if (objlen > 0)		{
  	for (var i = 0; i < objlen; i++)		{
	  	var obj = mesgObj[i];

			var sD = new Date(obj.startdate);
			var eD = new Date(obj.enddate);
			sD = $.datepicker.formatDate('dd/mm/yy', sD);
			eD = $.datepicker.formatDate('dd/mm/yy', eD);

			var newRow = '<tr>';
			newRow += '<td>';
			newRow += obj.notes;
			newRow += '</td><td>';
			newRow += sD;
			newRow += '</td><td>';
			newRow += eD;
			newRow += '</td><td>';
			newRow += obj.users;
			newRow += '</td><td>';
			if (obj.active)
				newRow += "<span class='status-green'>Active License</span>";
			else
				newRow += "<span class='status-red'>Expired</span>";
			newRow += '</td><td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
	}
  rtable.redraw();
}

$(document).ready(function()	{

  defLicenseTable();
  fillLicenseData();
  SetPagination();
});

