$(document).ready(function()	{
	$('#home1-signin').click(function() {
		$("#modal_name").val('');
		$("#modal_mobile").val('');
		$("#modal_email").val('');
	  $('#startModal').modal();
		return false;
	});
	$('#home2-signin').click(function() {
		$("#modal_name").val('');
		$("#modal_mobile").val('');
		$("#modal_email").val('');
	  $('#startModal').modal();
		return false;
	});
	$('#save_btn').click(function() {
		var susername = $("#modal_name").val();
    if (susername == "")   {
       $('#modal_name').css('border-color', 'red');
       setError($('#errtext1'),"Pls input your name");
       return false;
    } else    {
       $('#modal_name').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var susermobile = $("#modal_mobile").val();
    if (susermobile == "")   {
       $('#modal_mobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input your mobile number');
       return false;
    } else    {
       $('#modal_mobile').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var slen = susermobile.length;
    if (slen < 10)   {
       $('#modal_mobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid mobile number');
       return false;
    } else    {
       $('#modal_mobile').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
	  var semail = $("#modal_email").val();
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
    if (semail == "")   {
       $('#modal_email').css('border-color', 'red');
       setError($('#errtext1'),'Please input email id');
       return false;
    } else    {
       $('#modal_email').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
       $('#modal_email').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid email id');
       return false;
    } else    {
       $('#modal_email').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
    if (susermobile.length < 10)   {
       $('#modal_mobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid mobile number');
       return false;
    } else    {
       $('#modal_mobile').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sprefer = $("#modal_prefer").val();
    if (!sprefer)   {
       $('#modal_prefer').css('border-color', 'red');
       setError($('#errtext1'),'Please select a plugHR service option');
       return false;
    } else    {
       $('#modal_prefer').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
/*
		var scoagree = $("#coagree").prop('checked');
    if (scoagree == false)   {
       $('#coagree').css('border-color', 'red');
       setError($('#errtext1'),'Please confirm your authority');
       return false;
    } else    {
       $('#coagree').css('border-color', 'default');
       clearError($('#errtext1s'));
    }
		var sstuagree = $("#useragree").prop('checked');
    if (sstuagree == false)   {
       $('#useragree').css('border-color', 'red');
       setError($('#errtext1'),'You have to agree to our policies to Join');
       return false;
    } else    {
       $('#useragree').css('border-color', 'default');
       clearError($('#errtext1s'));
    }
*/
		var joinObject = new Object();
		joinObject.username = susername;
		joinObject.email = semail;
		joinObject.usermobile = susermobile;
    joinObject.prefer = sprefer;

	  //alert(JSON.stringify(joinObject));
		//return false;

		$.ajax({
			type: "POST",
			url: '/companyjoin',
			data: joinObject,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success : function(robj) { 
				$('#startModal').modal('hide');
		    stopAjaxIcon();
			},
			error: function(eobj) {
				setError($('#errtext1'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
	$('#about1-signin').click(function() {
		var sfname = $("#fname").val();
    if (sfname == "")   {
       $('#fname').css('border-color', 'red');
       setError($('#errtext1'),"Pls input your first name");
       return false;
    } else    {
       $('#fname').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var slname = $("#lname").val();
    if (slname == "")   {
       $('#lname').css('border-color', 'red');
       setError($('#errtext1'),"Pls input your last name");
       return false;
    } else    {
       $('#lname').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sph1 = $("#ph1").val();
    if (sph1 == "")   {
       $('#ph1').css('border-color', 'red');
       setError($('#errtext1'),'Please input your country code');
       return false;
    } else    {
       $('#ph1').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sph2 = $("#ph2").val();
    if (sph2 == "")   {
       $('#ph2').css('border-color', 'red');
       setError($('#errtext1'),'Please input your phone number');
       return false;
    } else    {
       $('#ph2').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sph3 = $("#ph3").val();
    if (sph3 == "")   {
       $('#ph3').css('border-color', 'red');
       setError($('#errtext1'),'Please input your phone number');
       return false;
    } else    {
       $('#ph3').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sph4 = $("#ph4").val();
    if (sph4 == "")   {
       $('#ph4').css('border-color', 'red');
       setError($('#errtext1'),'Please input your phone number');
       return false;
    } else    {
       $('#ph4').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
	  var semail = $("#email").val();
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
    if (semail == "")   {
       $('#email').css('border-color', 'red');
       setError($('#errtext1'),'Please input email id');
       return false;
    } else    {
       $('#email').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
       $('#email').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid email id');
       return false;
    } else    {
       $('#email').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var srole = $("#roletype:checked").val();
    if (srole == "")   {
       $('#roletype').css('border-color', 'red');
       setError($('#errtext1'),"Pls select a role");
       return false;
    } else    {
       $('#roletype').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sfit = $("#fit").val();
    if (sfit == "")   {
       $('#fit').css('border-color', 'red');
       setError($('#errtext1'),"Pls explain your role fit");
       return false;
    } else    {
       $('#fit').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sprofile = $("#profile").val();
    if (sprofile == "")   {
       $('#profile').css('border-color', 'red');
       setError($('#errtext1'),"Pls paste your linkedin profile page link");
       return false;
    } else    {
       $('#profile').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var saddr1 = $("#addr1").val();
    if (saddr1 == "")   {
       $('#addr1').css('border-color', 'red');
       setError($('#errtext1'),"Pls mention your current residence");
       return false;
    } else    {
       $('#addr1').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var stweet = $("#tweet").val();
		var sblog = $("#blog").val();
		var setype = $("#etype-list").val();
		var saddr2 = $("#addr2").val();
		var scity = $("#city").val();
		var sstate = $("#state").val();			
		var szip = $("#zip").val();
		var scountry = $("#country").val();
					
						
		var joinObject = new Object();
		joinObject.fname = sfname;
		joinObject.lname = slname;
		joinObject.email = semail;
		joinObject.ph1 = sph1;
		joinObject.ph2 = sph2;
		joinObject.ph3 = sph3;
		joinObject.ph4 = sph4;
    joinObject.role = srole;
		joinObject.fit = sfit;
		joinObject.tweet = stweet;
		joinObject.profile = sprofile;
		joinObject.etype = setype;
		joinObject.addr1 = saddr1;
		joinObject.blog = sblog;		
		joinObject.addr2 = saddr2;	
		joinObject.city = scity;		
		joinObject.state = sstate;				
		joinObject.zip = szip;				
		joinObject.country = scountry;								

	//	alert(JSON.stringify(joinObject));
		//return false;

		$.ajax({
			type: "POST",
			url: '/applicantjoin',
			data: joinObject,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success : function(robj) { 
		    if (robj.err == 0)		{
				  setSuccess($('#errtext1'),robj.text);
		    } else
					setError($('#errtext1'),robj.text);
		    stopAjaxIcon();
			},
			error: function(eobj) {
				setError($('#errtext1'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});


