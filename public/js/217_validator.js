$(function() {
	$(document).ajaxStart(function(){
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
	$('#mhs-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth : true,
		tags: true,
    tokenSeparators: [","],
    createTag: function(newTag) {
        return {
            id: 'new:' + newTag.term,
            text: newTag.term + ' (new)'
        };
    }
	});
	$('#kra-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth : true,
		tags: true,
    tokenSeparators: [","],
    createTag: function(newTag) {
        return {
            id: 'new:' + newTag.term,
            text: newTag.term + ' (new)'
        };
    }
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#rtype-list').prop('selectedIndex', 0);
	$('#dept-list').prop('selectedIndex', 0);
	$('#mhs-list').select2('val', null);
	$('#kra-list').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}

function def_kra()    {
	$('#kra-list').empty();
	var xObj = JSON.parse($('#kradata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			//console.log(obj.dept);
			$('<option>').val(obj._id).text(obj.kratitle).appendTo('#kra-list');
		}
	}
}

function def_mhs()    {
	$('#mhs-list').empty();
	var xObj = JSON.parse($('#mhsdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#mhs-list');
		}
	}
}

function defMesgTable()    {
	$('table.roles').data('footable').reset();
	$('table.roles thead').append('<tr>');
	$('table.roles thead tr').append('<th>Role Title</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Type</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">KRAs</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Required Skills</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.roles thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.roles thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.roles thead tr').append('</tr>');
	$('table.roles').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#deptdata').val());
	var sel4Obj = JSON.parse($('#mhsdata').val());
	var sel5Obj = JSON.parse($('#kradata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.roles').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.roletitle != null)
				newRow += obj.roletitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.roletype)     {
				case '1':
					newRow += 'Top Management';
					break;
				case '2':
					newRow += 'Function Head';
					break;
				case '3':
					newRow += 'Team Leader';
					break;
				case '4':
					newRow += 'Front line';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.depid == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.kras)		{
				var olen = obj.kras.length;
				var dblen = sel5Obj.length;
				for (var ctr = 0; ctr < olen; ctr++)		{
					var selx = obj.kras[ctr];
					for (var temp = 0;temp < dblen; temp++)		{
						var inx = sel5Obj[temp];
						if (selx == inx._id)	{
							newRow += inx.kratitle+ ', ';
							break;
						}
					}
					if (temp == dblen)
						newRow += '*' + selx + '*' + ', ';
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mustskills)		{
				var olen = obj.mustskills.length;
				var dblen = sel4Obj.length;
				for (var ctr = 0; ctr < olen; ctr++)		{
					var selx = obj.mustskills[ctr];
					for (var temp = 0;temp < dblen; temp++)		{
						var inx = sel4Obj[temp];
						if (selx == inx._id)	{
							newRow += inx.skillname+ ', ';
							break;
						}
					}
					if (temp == dblen)
						newRow += '*' + selx + '*' + ', ';
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/217newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.roles').data('footable');

			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.roles').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		// Change #1
		var rowindex = row.substring(1);
		// -----
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.roles').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		// Change #2
		var rowindex = row.substring(1);
		// -----
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		// Change #3
		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];
		// -----

		$('#dbid').val(obj._id);
		$('#title').val(obj.roletitle);
		$('#rtype-list').val(obj.roletype);
		$('#dept-list').val(obj.depid);
		$('#mhs-list').select2('val', obj.mustskills);
		$('#kra-list').select2('val', obj.kras);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();

		// Change #4
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
		// -----

			dataObj.dbid= obj._id;
			dataObj.title= obj.roletitle;
			dataObj.rtype= obj.roletype;
			dataObj.dept= obj.depid;
			dataObj.mhs= obj.mustskills;

			$.ajax({
				type: 'DELETE',
				url: '/217',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.roles').data('footable');

						$('table.roles tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.roles tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'roles data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Role Title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_rtype = $('#rtype-list').val();
	if (s_rtype == null)	{
		$('#rtype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Type');
		$('#rtype-list').focus();
		return false;
	} else    {
		$('#rtype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dept = $('#dept-list').val();
	if (s_dept == null)	{
		$('#dept-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Department');
		$('#dept-list').focus();
		return false;
	} else    {
		$('#dept-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_kra = $('#kra-list').val();
	if (s_kra == null)	{
		$('#kra-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select or add KRAs');
		$('#kra-list').focus();
		return false;
	} else    {
		$('#kra-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_mhs = $('#mhs-list').val();
	if (s_mhs == null)	{
		$('#mhs-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select or add skills');
		$('#mhs-list').focus();
		return false;
	} else    {
		$('#mhs-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var klen = s_kra.length;
	var oldKra = [];
	var newKra = [];
	for (var ctr = 0; ctr < klen; ctr++)		{
		var ns = s_kra[ctr];
		//alert(ns.substr(0,4));
		if (ns.substr(0,4) == 'new:')
			newKra.push(ns.substr(4));
		else
			oldKra.push(ns);
	}
	//alert(oldKra);
	//alert(newKra);

	var slen = s_mhs.length;
	var oldSkill = [];
	var newSkill = [];
	for (ctr = 0; ctr < slen; ctr++)		{
		ns = s_mhs[ctr];
		//alert(ns.substr(0,4));
		if (ns.substr(0,4) == 'new:')
			newSkill.push(ns.substr(4));
		else
			oldSkill.push(ns);
	}
	//alert(oldSkill);
	//alert(newSkill);

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid = $('#dbid').val();
	formdataObj.title = s_title;
	formdataObj.rtype = s_rtype;
	formdataObj.dept = s_dept;

	formdataObj.kra = [];
	formdataObj.skill = [];
	formdataObj.kra.push.apply(formdataObj.kra, oldKra);
	formdataObj.skill.push.apply(formdataObj.skill, oldSkill);

	formdataObj.newkra = [];
	formdataObj.newskill = [];
	formdataObj.newkra.push.apply(formdataObj.newkra, newKra);
	formdataObj.newskill.push.apply(formdataObj.newskill, newSkill);

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/217add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				if (resp.skills != undefined)		{
					var skdata = JSON.parse($('#mhsdata').val());
					var slen = resp.skills.length;
					for (i=0;i<slen;i++)	{
						skdata.push(resp.skills[i]);
					}
					$('#mhsdata').val(JSON.stringify(skdata));
				}
				if (resp.kras != undefined)		{
					var krdata = JSON.parse($('#kradata').val());
					var slen = resp.kras.length;
					for (i=0;i<slen;i++)	{
						krdata.push(resp.kras[i]);
					}
					$('#kradata').val(JSON.stringify(krdata));
				}

				// DELETE & RE-CREATE Table 
				var rtable = $('table.roles').data('footable');

				$('table.roles tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.roles tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				def_kra();
				def_mhs();
				setSuccess($('#errtext1'),'roles data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/217upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				if (resp.skills != undefined)		{
					var skdata = JSON.parse($('#mhsdata').val());
					var slen = resp.skills.length;
					for (i=0;i<slen;i++)	{
						skdata.push(resp.skills[i]);
					}
					$('#mhsdata').val(JSON.stringify(skdata));
				}
				if (resp.kras != undefined)		{
					var krdata = JSON.parse($('#kradata').val());
					var slen = resp.kras.length;
					for (i=0;i<slen;i++)	{
						krdata.push(resp.kras[i]);
					}
					$('#kradata').val(JSON.stringify(krdata));
				}

				// DELETE & RE-CREATE Table 
				var rtable = $('table.roles').data('footable');

				$('table.roles tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.roles tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				def_kra();
				def_mhs();
				setSuccess($('#errtext1'),'roles data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_dept();
	def_mhs();
	def_kra();

	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

