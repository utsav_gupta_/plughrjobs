$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
	$(function fillUserProfile()		{
		var loggeruserObj = JSON.parse($("#formdata").val());

    var randm = (new Date()).toString();
    var imgsrc = ('/userpictul?filename=' + loggeruserObj.coid + "-" + loggeruserObj.userid + '&rnd=' + randm);

    //var imgsrc = '/userpictul?stuid='+loggeruserObj.user;
    $("#userpict").attr('src',imgsrc);
	});
  var options = {
    beforeSend: function()  {
      clearError($('#message'));
      startAjaxIcon();
    },
    success: function(response)     {
      if (response.err == 0)  
        setSuccess($('#message'),response.text);
      else
        setError($('#message'),response.text);

      //$('#addphoto-form').clearForm();
  		//$("#emailid").val($("#cuser").text());
      //stopAjaxIcon();
    },
  	complete: function(response)    {
      var randm = (new Date()).toString();
	    var imgsrc = ('/userpictul?filename=' +  $("#ccoid").text()  + "-" + $("#cuserid").text() + '&rnd=' + randm);
      $("#userpict").attr('src',imgsrc);
      $("#updphoto").text('select new photo');
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#message'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#addphoto-form").ajaxForm(options);

	$('#updphoto_btn').click(function() {
		var sfile = $("#updphoto").text();
	  if (sfile == "select new photo")	{
      $('#updphoto').css('font-color', 'red');
      setError($('#message'),'Please select a file to upload');
      return false;
    } else    {
      $('#updphoto').css('font-color', 'default');
      clearError($('#message'));
    }
    return true;
	});
});

