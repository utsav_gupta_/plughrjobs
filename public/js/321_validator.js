$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#modal_sdate').datepicker({ dateFormat: 'dd-mm-yy'});
  $('table.users').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#uid').val('');
	$('#uname').val('');
	$('#mgr').val('');
	$('#dep-list').prop('selectedIndex', -1);
	$('#loc-list').prop('selectedIndex', -1);
	clearError($('#errtext2'));
}
function clearTable()		{
	var rtable = $('table.users').data('footable');

	$('table.users tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.users tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function getMgr(mgrid)		{
	var xObj = JSON.parse($('#mgrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == mgrid)
				return obj.username;
		}
	}
	return "";
}

function getMgrID(mgrname)		{
	if (mgrname == "")
		return "";
	var xObj = JSON.parse($('#mgrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.username.indexOf(mgrname) > -1)
				return obj.userid;
		}
	}
	return "";
}

function getDept(depid)		{
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == depid)
				return obj.depname;
		}
	}
	return "";
}

function getRole(roleid)		{
	var xObj = JSON.parse($('#roledata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == roleid)
				return obj.roletitle;
		}
	}
	return "";
}

function getLoc(locid)		{
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == locid)
				return obj.oname;
		}
	}
	return "";
}
function def_dep()    {
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dep-list');
			$('<option>').val(obj._id).text(obj.depname).appendTo('#modal_dept-list');
		}
	}
}
function def_loc(locid)    {
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#loc-list');
			$('<option>').val(obj._id).text(obj.oname).appendTo('#modal_loc-list');
		}
	}
}
function def_role(roleid)    {
	var xObj = JSON.parse($('#roledata').val());
	var seldep = $('#modal_dept-list').val();

	$('#role-list').empty();
	$('#modal_role-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == seldep)		{
				var optObj = new Object;
				optObj.roleid = obj._id;
				optObj.depid = obj.depid;
				optObj.rtype = obj.roletype;
				if (roleid && roleid == optObj.roleid)		{
					$('<option selected>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
					$('<option selected>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#modal_role-list');
				} else	{
					$('<option>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
					$('<option>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#modal_role-list');
				}
			}
		}
	}
}
function def_mgr()    {
	var xObj = JSON.parse($('#mgrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#mgr-list');
			$('<option>').val(obj.userid).text(obj.username).appendTo('#modal_mgr-list');
		}
	}
}
function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User id</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Reporting To</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Work location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Employment Type</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Salary (CTC)</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Update</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}
function fillUserData()		{
	var sel4Obj = JSON.parse($('#depdata').val());
	var sel5Obj = JSON.parse($('#locdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mgr != null)
				newRow += getMgr(obj.mgr);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.dept != null)
				newRow += getDept(obj.dept);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.role != null)
				newRow += getRole(obj.role);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.location != null)
				newRow += getLoc(obj.location);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.emptype)     {
				case '1':
					newRow += 'Full-time Employee';
					break;
				case '2':
					newRow += 'Part-time Employee';
					break;
				case '3':
					newRow += 'Freelancing';
					break;
				case '4':
					newRow += 'Full-time Consultant';
					break;
				case '5':
					newRow += 'Part-time Consultant';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.salary != null)
				newRow += obj.salary;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-user" title="Update"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/321newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.users').data('footable');

			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(function () {
	$('table.users').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.users').data('footable');
    var row = $(this).parents('tr:first');
		clearError($('#errtext1'));
    viewDoc(e, row[0]["rowIndex"]-1);
	});
});

function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);
		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		var coid = $('#ccoid').text();
		var userid = obj.userid;

		$('#modal_uid').val(obj.userid);
		$('#modal_sdate').val('');
		$('#modal_comments').val('');

		$('#modal_dept-list').val(obj.dept);
		$('#modal_loc-list').val(obj.location);
		def_role(obj.role);
		//$('#modal_role-list').val(obj.role);
		$('#modal_mgr-list').val(obj.mgr);
		$('#modal_etype-list').val(obj.emptype);
		$('#modal_sal').val(obj.salary);

    $('#MesgModel').modal();
    return false;
  }
}

function GetValidInputs()		{
	var s_uid = $('#uid').val();
	var s_uname = $('#uname').val();
	var s_mgr = $('#mgr').val();
	var s_dep = $('#dep-list').val();
	var s_loc = $('#loc-list').val();

  if (!s_uid && !s_uname && !s_mgr && !s_dep && !s_loc)   {
      setError($('#errtext1'),'Please input at least one criteria');
      return false;
  } else  {
      clearError($('#errtext1'));
  }
	var searchObj = new  Object();
	searchObj.coid = $('#ccoid').text();
	searchObj.userid = $('#cuserid').text();
	searchObj.uid= s_uid;
	searchObj.uname= s_uname;
	searchObj.mgr= getMgrID(s_mgr);
	searchObj.dep= s_dep;
	searchObj.loc= s_loc;
	return searchObj;
}

function search_emp()		{
	var searchObj = GetValidInputs();
	if (!searchObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/321search',
		data: searchObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err && resp.totpages > 0)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));
        $("#listquery").val(resp.sobj);
				$('#depdata').val(JSON.stringify(resp.departments));
				$('#locdata').val(JSON.stringify(resp.locations));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Search Complete');
			} else  {
				clearForm();
				clearTable();
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			clearForm();
			clearTable();
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});

	return false;
}

function GetModalInputs(mode)		{
	var s_sdate = $('#modal_sdate').val();
	if (s_sdate == '')	{
		$('#modal_sdate').css('border-color', 'red');
		setError($('#errtext2'),'Please input Effective Date');
		$('#modal_sdate').focus();
		return false;
	} else    {
		$('#modal_sdate').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_dep = $('#modal_dept-list').val();
	if (s_dep == null)	{
		$('#modal_dept-list').css('border-color', 'red');
		setError($('#errtext2'),'Please input Department');
		$('#modal_dept-list').focus();
		return false;
	} else    {
		$('#modal_dept-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_loc = $('#modal_loc-list').val();
	if (s_loc == null)	{
		$('#modal_loc-list').css('border-color', 'red');
		setError($('#errtext2'),'Please input Location');
		$('#modal_loc-list').focus();
		return false;
	} else    {
		$('#modal_loc-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}

	var s_role = $('#modal_role-list').val();
	var s_rtype;

	if (s_role != null)	{
		var tmp = JSON.parse(s_role); 
		s_role = tmp.roleid;
		s_rtype = tmp.rtype;
	}
	if (s_role == null)	{
		$('#modal_role-list').css('border-color', 'red');
		setError($('#errtext2'),'Please input Role');
		$('#modal_role-list').focus();
		return false;
	} else    {
		$('#modal_role-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_mgr = $('#modal_mgr-list').val();
	if (s_mgr == null)	{
		$('#modal_mgr-list').css('border-color', 'red');
		setError($('#errtext2'),'Please select a reporting manager');
		$('#modal_mgr-list').focus();
		return false;
	} else    {
		$('#modal_mgr-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_etype = $('#modal_etype-list').val();
	if (s_etype == null)	{
		$('#modal_etype-list').css('border-color', 'red');
		setError($('#errtext2'),'Please input Employment Type');
		$('#modal_etype-list').focus();
		return false;
	} else    {
		$('#modal_etype-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}

	var s_sal = $('#modal_sal').val();
	if (s_sal == '')	{
		$('#modal_sal').css('border-color', 'red');
		setError($('#errtext2'),'Please input Salary (CTC)');
		$('#sal').focus();
		return false;
	} else    {
		$('#modal_sal').css('border-color', 'default');
		clearError($('#errtext2'));
	}

	var s_ctype = $('#modal_ctype-list').val();
	if (s_ctype == null)	{
		$('#modal_ctype-list').css('border-color', 'red');
		setError($('#errtext2'),'Please input Purpose of this change');
		$('#modal_ctype-list').focus();
		return false;
	} else    {
		$('#modal_ctype-list').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_comments = $('#modal_comments').val();
	if (s_comments == '')		{
		$('#modal_comments').css('border-color', 'red');
		setError($('#errtext2'),'Please input comments about this change');
		$('#modal_comments').focus();
		return false;
	} else    {
		$('#modal_comments').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var modaldataObj = new  Object();
	modaldataObj.coid = $('#ccoid').text();
	modaldataObj.userid = $('#modal_uid').val();
	modaldataObj.sdate = s_sdate;
	modaldataObj.mgr = s_mgr;
	modaldataObj.dep = s_dep;
	modaldataObj.role = s_role;
	modaldataObj.rtype = s_rtype;
	modaldataObj.loc = s_loc;
	modaldataObj.etype= s_etype;
	modaldataObj.salary = s_sal;
	modaldataObj.ctype= s_ctype;
	modaldataObj.comments = s_comments;
	return modaldataObj;
}

function save_orgchange()		{
	var modalObj = GetModalInputs();
	if (!modalObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/321save',
		data: modalObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				clearForm();
				clearTable();
				setSuccess($('#errtext1'),'Org update saved');
				$('#MesgModel').modal('hide');
			} else  {
				setError($('#errtext2'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext2'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

$(document).ready(function()	{
	def_dep();
	def_loc();
	def_role();
	def_mgr();
	defMesgTable();
	clearForm();
	$('#search_btn').click(function() 		{
		search_emp();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		return false;
	});
	$('#upd_btn').click(function() 			{
		save_orgchange();
		return false;
	});
});


