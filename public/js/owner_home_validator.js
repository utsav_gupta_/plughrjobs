$(document).ready(function()	{
	$('#modal_date').datepicker({ dateFormat: 'dd-mm-yy'});

	var userObj = JSON.parse($('#formdata').val());
	//alert(JSON.stringify(userObj));

	var cdate = new Date;
	//alert(cdate);
	var cyear = cdate.getFullYear();
	//alert(cyear);
	var pdate = new Date();
	pdate.setDate(pdate.getDate()-30);
	//alert(pdate);
	var bdate = new Date();

	var cday = cdate.getDay();
	switch (cday)		{
		case 0:
			cday = 'HAPPY SUNDAY';
			bdate.setDate(bdate.getDate()+6);
			break;
		case 1:
			cday = 'HAPPY MONDAY';
			bdate.setDate(bdate.getDate()+5);
			break;
		case 2:
			cday = 'HAPPY TUESDAY';
			bdate.setDate(bdate.getDate()+4);
			break;
		case 3:
			cday = 'HAPPY WEDNESDAY';
			bdate.setDate(bdate.getDate()+3);
			break;
		case 4:
			cday = 'HAPPY THURSDAY';
			bdate.setDate(bdate.getDate()+2);
			break;
		case 5:
			cday = 'HAPPY FRIDAY';
			bdate.setDate(bdate.getDate()+1);
			break;
		case 6:
			cday = 'HAPPY SATURDAY';
			bdate.setDate(bdate.getDate()+0);
			break;
	}
	$("#greet").text(cday);

	cdate.setHours(0, 0, 0, 0);
	bdate.setHours(0, 0, 0, 0);
	var totu = 0;     // Total users
	var newu = 0;     // new users
	var bdayu = 0;    // birthday users
	var adayu = 0;    // work anniversary users

	var usersObj = JSON.parse($('#usersdata').val());
	//alert(JSON.stringify(usersObj));
	var glen = usersObj.length;
	//alert(glen);
	if (glen > 0)			{
		for (var x=0;x<glen;x++)		{
			if (usersObj[x].status && usersObj[x].status != '3')
  			totu++;

			// Find users joined in past 30 days
			if (usersObj[x].doj && usersObj[x].doj != "")		{
				var tpos = usersObj[x].doj.indexOf("T");
				var tmpdate = usersObj[x].doj.substr(0,tpos);
				var tmpdate = tmpdate.split("-");
				var doj = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[2]);
				if (doj > pdate && doj <= cdate)	{
					//alert(doj);
					newu++;
				}
				var doa = new Date(cyear, tmpdate[1] - 1, tmpdate[2]);
				if (doa >= pdate && doa <= cdate)	{
					adayu++;
				}
			}

			// Find users having birth days this week
			if (usersObj[x].dob && usersObj[x].dob != "")		{
				var tpos = usersObj[x].dob.indexOf("T");
				var tmpdate = usersObj[x].dob.substr(0,tpos);
				var tmpdate = tmpdate.split("-");
				var dob = new Date(cyear, tmpdate[1] - 1, tmpdate[2]);
				if (dob >= cdate && dob <= bdate)	{
					//alert(doj);
					bdayu++;
				}
			}

		}
	}

	var hday = ", No holidays coming up this week";
	var hdayObj = JSON.parse($('#hlistdata').val());
	//alert(JSON.stringify(hdayObj));
	var hlen = hdayObj.length;
	//alert(hlen);
	if (hlen > 0)			{
		for (var x=0;x<hlen;x++)		{
			// Find upcoming holidays this week
			if (hdayObj[x].holidaydate && hdayObj[x].holidaydate != "")		{
				var tmpdate = hdayObj[x].holidaydate.split("-");
				var hld = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				if (hld >= cdate && hld <= bdate)	{
					//alert(hld);
					var hpos = hld.toString().indexOf('00:00:00');
					hday = ", holiday coming up on " + hld.toString().substring(0,hpos);
					break;
				}
			}
		}
	}

	var pday = 0;
	var pdayObj = JSON.parse($('#rperdata').val());
	//alert(JSON.stringify(pdayObj));
	var plen = pdayObj.length;
	//alert(hlen);
	if (plen > 0)			{
		for (var x=0;x<plen;x++)		{
			// Find upcoming holidays this week
			if (pdayObj[x].edate && pdayObj[x].edate != "")		{
				//alert(pdayObj[x].sdate);
				var td = pdayObj[x].edate.indexOf('T');
				if (td > 0)
					td = pdayObj[x].edate.substr(0,td);
				else
					td = pdayObj[x].edate;
				var tmpdate = td.split("-");
				//alert(tmpdate[2]);
				//alert(tmpdate[1]);
				//alert(tmpdate[0]);
				var prd = new Date(tmpdate[0], tmpdate[1] - 1, tmpdate[2]);
				//alert(prd);
				//alert(cdate);
				if (prd > cdate)			{
					var timeDiff = Math.abs(prd.getTime() - cdate.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					if (pday == 0 || pday > diffDays)	{
						pday = diffDays;
					}
				}
			}
		}
	}

	var eObj = new Object;
	var ectr = 1;
	var eventsObj = JSON.parse($('#eventsdata').val());
	//alert(JSON.stringify(eventsObj));
	var elen = eventsObj.length;
	//alert(elen);
	if (elen > 0)			{
		for (var x=0;x<elen;x++)		{
			// Find upcoming events this week
			if (eventsObj[x].date && eventsObj[x].date != "")		{
				var tmpdate = eventsObj[x].date.split("-");
				var prd = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);
				if (prd > cdate)			{
					if (ectr < 4)	{
						eObj['title'+ectr] = eventsObj[x].title;
						eObj['date'+ectr] = eventsObj[x].date;
						eObj['desc'+ectr] = eventsObj[x].desc;
						ectr++;
					}
				}
			}
		}
	}

	if (eObj.length != 0)		{
		$("#evtitle1").text(eObj.title1);
		$("#evdate1").text(eObj.date1);
		$("#evdesc1").text(eObj.desc1);
		$("#evtitle2").text(eObj.title2);
		$("#evdate2").text(eObj.date2);
		$("#evdesc2").text(eObj.desc2);
		$("#evtitle3").text(eObj.title3);
		$("#evdate3").text(eObj.date3);
		$("#evdesc3").text(eObj.desc3);
	}
	var sts = "You have " + totu +" team members today, " + newu +" joined in last 30 days, " + adayu + " members have work anniversaries this week, " + bdayu + " members have birthdays this week" + hday  +", Performance review is " + pday + " day(s) away";
	$("#status").text(sts);

	var blogsObj = JSON.parse($('#blogsdata').val());
	if (blogsObj != null)    {
		$("#blog1title").text(blogsObj[0].title);
		$("#blog1title").attr('href','/260a?blog='+blogsObj[0]._id);
		$("#blog1author").text(blogsObj[0].author);
		$("#blog1stext").text(blogsObj[0].stext);

		$("#blog2title").text(blogsObj[1].title);
		$("#blog2title").attr('href','/260a?blog='+blogsObj[1]._id);
		$("#blog2author").text(blogsObj[1].author);
		$("#blog2stext").text(blogsObj[1].stext);
	}

	$('#posttrain').click(function() {
		$("#modal_training").val('');
		$("#modal_trainees").val('');
		$("#modal_place").prop('selectedIndex', 0);
		$("#modal_date").val('');
		$("#modal_expect").val('');				
	  $('#trainingModal').modal();
		return false;
	});

	$('#training_btn').click(function() {
		var straining = $("#modal_training").val();
    if (straining == "")   {
       $('#modal_training').css('border-color', 'red');
       setError($('#errtext1'),"What do you want to train on?");
       return false;
    } else    {
       $('#modal_training').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var strainees = $("#modal_trainees").val();
    if (strainees == "")   {
       $('#modal_trainees').css('border-color', 'red');
       setError($('#errtext1'),"How many people are to be trained?");
       return false;
    } else    {
       $('#modal_trainees').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var splace = $("#modal_place").val();
    if (!splace)   {
       $('#modal_place').css('border-color', 'red');
       setError($('#errtext1'),'Please select a training location');
       return false;
    } else    {
       $('#modal_place').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sdate = $("#modal_date").val();
    if (sdate == "")   {
       $('#modal_date').css('border-color', 'red');
       setError($('#errtext1'),"When do you want to train?");
       return false;
    } else    {
       $('#modal_date').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var sexpect = $("#modal_expect").val();
    if (sexpect == "")   {
       $('#modal_expect').css('border-color', 'red');
       setError($('#errtext1'),"Describe your expectation from the training?");
       return false;
    } else    {
       $('#modal_expect').css('border-color', '#ccc');
       clearError($('#errtext1'));
    }
		var joinObject = new Object();
		joinObject.training = straining;
		joinObject.trainees = strainees;
		joinObject.date = sdate;
    joinObject.place = splace;
		joinObject.expect = sexpect;
		
	  //alert(JSON.stringify(joinObject));
		//return false;

		$.ajax({
			type: "POST",
			url: '/traineejoin',
			data: joinObject,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success : function(robj) { 
				$('#trainingModal').modal('hide');
		    stopAjaxIcon();
			},
			error: function(eobj) {
				setError($('#errtext1'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});

$('#defjob').click(function() {
	$("#modal_role").val('');
	$("#modal_location").val('');
	$("#modal_tenure").val('');
	$("#modal_fees").val('');		
	$("#modal_totpersons").val('');				
  $('#contractModal').modal();
	return false;
});

$('#contract_btn').click(function() {
	var srole = $("#modal_role").val();
  if (srole == "")   {
     $('#modal_role').css('border-color', 'red');
     setError($('#errtext2'),"Please mention a role title");
     return false;
  } else    {
     $('#modal_role').css('border-color', '#ccc');
     clearError($('#errtext2'));
  }
	var slocation = $("#modal_location").val();
  if (slocation == "")   {
     $('#modal_location').css('border-color', 'red');
     setError($('#errtext2'),"Please mention a location");
     return false;
  } else    {
     $('#modal_location').css('border-color', '#ccc');
     clearError($('#errtext2'));
  }
	var stenure = $("#modal_tenure").val();
  if (stenure == "")   {
     $('#modal_tenure').css('border-color', 'red');
     setError($('#errtext2'),'Please mention the duration of contract');
     return false;
  } else    {
     $('#modal_tenure').css('border-color', '#ccc');
     clearError($('#errtext2'));
  }
	var sfees = $("#modal_fees").val();
  if (sfees == "")   {
     $('#modal_fees').css('border-color', 'red');
     setError($('#errtext2'),"Please mention the fees per person");
     return false;
  } else    {
     $('#modal_fees').css('border-color', '#ccc');
     clearError($('#errtext2'));
  }
	var stotpersons = $("#modal_totpersons").val();
  if (stotpersons == "")   {
     $('#modal_totpersons').css('border-color', 'red');
     setError($('#errtext2'),"Please mention the total persons required");
     return false;
  } else    {
     $('#modal_totpersons').css('border-color', '#ccc');
     clearError($('#errtext2'));
  }
	var joinObject = new Object();
	joinObject.role = srole;
	joinObject.location = slocation;
	joinObject.tenure = stenure;
  joinObject.fees = sfees;
	joinObject.totpersons = stotpersons;
	
  //alert(JSON.stringify(joinObject));
	//return false;

	$.ajax({
		type: "POST",
		url: '/contractjoin',
		data: joinObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success : function(robj) { 
			$('#contractModal').modal('hide');
	    stopAjaxIcon();
		},
		error: function(eobj) {
			setError($('#errtext2'),eobj.text);
      stopAjaxIcon();
		}
	});
	return false;
});

$('#postjob').click(function() {
	$("#modal_title").val('');
	$("#modal_jobdesc").val('');
	$("#modal_jobloc").val('');
	$("#modal_jobtype").prop('selectedIndex', 0);
	$("#modal_comp").val('');				
	$("#modal_stock").val('');				
	$("#modal_offer1").prop('checked',false);
	$("#modal_offer2").prop('checked',false);
	$("#modal_offer3").prop('checked',false);
	$("#modal_offer4").prop('checked',false);
	$("#modal_offer5").prop('checked',false);
	$("#modal_offer6").prop('checked',false);
	$("#modal_leave").val('');						
	$("#modal_profile").val('');						
	$("#modal_jobpost1").prop('checked',false);
	$("#modal_jobpost2").prop('checked',false);
	$('input:radio[name=modal_agent1]').attr('checked',true)
	$('input:radio[name=modal_agent2]').attr('checked',false)
	//$("#modal_jobfee").prop('selectedIndex', 0);
  $('#jobModal').modal();
	return false;
});

$('#job_btn').click(function() {
	var stitle = $("#modal_title").val();
  if (stitle == "")   {
     $('#modal_title').css('border-color', 'red');
     setError($('#errtext3'),"Please mention the job title");
     return false;
  } else    {
     $('#modal_title').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sjobdesc = $("#modal_jobdesc").val();
  if (sjobdesc == "")   {
     $('#modal_jobdesc').css('border-color', 'red');
     setError($('#errtext3'),"Please mention a brief description");
     return false;
  } else    {
     $('#modal_jobdesc').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sjobloc = $("#modal_jobloc").val();
  if (sjobloc == "")   {
     $('#modal_jobloc').css('border-color', 'red');
     setError($('#errtext3'),"Please mention the job location");
     return false;
  } else    {
     $('#modal_jobloc').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sjobtype = $("#modal_jobtype").val();
  if (!sjobtype)   {
     $('#modal_jobtype').css('border-color', 'red');
     setError($('#errtext3'),"Please select a job type");
     return false;
  } else    {
     $('#modal_jobtype').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var scomp = $("#modal_comp").val();
  if (scomp == "")   {
     $('#modal_comp').css('border-color', 'red');
     setError($('#errtext3'),"Please mention annual compensation");
     return false;
  } else    {
     $('#modal_comp').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sstock = $("#modal_stock").val();
  if (sstock == "")   {
     $('#modal_stock').css('border-color', 'red');
     setError($('#errtext3'),"Please mention ESOPs");
     return false;
  } else    {
     $('#modal_stock').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var soffer = [];
	soffer[0] = $("#modal_offer1").prop('checked');
	soffer[1] = $("#modal_offer2").prop('checked');
	soffer[2] = $("#modal_offer3").prop('checked');
	soffer[3] = $("#modal_offer4").prop('checked');
	soffer[4] = $("#modal_offer5").prop('checked');
	soffer[5] = $("#modal_offer6").prop('checked');

	var sleave = $("#modal_leave").val();
  if (sleave == "")   {
     $('#modal_leave').css('border-color', 'red');
     setError($('#errtext3'),"Please mention the annual leave");
     return false;
  } else    {
     $('#modal_leave').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sprofile = $("#modal_profile").val();
  if (sprofile == "")   {
     $('#modal_profile').css('border-color', 'red');
     setError($('#errtext3'),"Please mention the candidate profile");
     return false;
  } else    {
     $('#modal_profile').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
	var sjobpost = [];
	sjobpost[0] = $("#modal_jobpost1").prop('checked');
	sjobpost[1] = $("#modal_jobpost2").prop('checked');

	var sagent = $("#modal_agent").prop('checked');
/*	var sjobfee = $("#modal_jobfee").val();
  if (!sjobfee)   {
     $('#modal_jobfee').css('border-color', 'red');
     setError($('#errtext3'),"Mention the job fee payable");
     return false;
  } else    {
     $('#modal_jobfee').css('border-color', '#ccc');
     clearError($('#errtext3'));
  }
*/

	var joinObject = new Object();
	joinObject.title = stitle;
	joinObject.jobdesc = sjobdesc;
	joinObject.jobloc = sjobloc;
  joinObject.jobtype = sjobtype;
	joinObject.comp = scomp;
	joinObject.stock = sstock;
	joinObject.offer = soffer;
	joinObject.leave = sleave;
	joinObject.profile = sprofile;
	joinObject.jobpost = sjobpost;
	joinObject.agent = sagent;
	//joinObject.jobfee = sjobfee;
															
  //alert(JSON.stringify(joinObject));
	//return false;

	$.ajax({
		type: "POST",
		url: '/jobpost',
		data: joinObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success : function(robj) { 
			$('#jobModal').modal('hide');
	    stopAjaxIcon();
		},
		error: function(eobj) {
			setError($('#errtext3'),eobj.text);
      stopAjaxIcon();
		}
	});
	return false;
});

function searchGo(keyword)    {
  var searchTags = [];
  searchTags["Add logo"] = 208;
  searchTags["About company"] = 208;
  searchTags["Activate exit"] = 238;
  searchTags["add blog"] = 260;
  searchTags["add committee"] = 214;
  searchTags["add employees"] = 220;
  searchTags["add kra"] = 218;
  searchTags["add people"] = 220;
  searchTags["add performance indicator"] = 202;
  searchTags["add policy"] = 216;
  searchTags["add role"] = 217;
  searchTags["Add self KPI"] = 202;
  searchTags["add skills"] = 219;
  searchTags["add team's KPI"] = 202;
  searchTags["Add team's performance indicator"] = 202;
  searchTags["Add user licences"] = 201;
  searchTags["add users"] = 220;
  searchTags["add values"] = 213;
  searchTags["address"] = 209;
  searchTags["announce events"] = 259;
  searchTags["Appraisal period"] = 231;
  searchTags["appraisal scale"] = 235;
  searchTags["assign holiday"] = 223;
  searchTags["blogs"] = 260;
  searchTags["carry forward"] = 234;
  searchTags["Check induction status"] = 255;
  searchTags["city"] = 209;
  searchTags["company"] = 208;
  searchTags["company info"] = 208;
  searchTags["Company location"] = 209;
  searchTags["company logo"] = 208;
  searchTags["company values"] = 213;
  searchTags["compensation components"] = 245;
  searchTags["corporate values"] = 213;
  searchTags["create committee"] = 214;
  searchTags["create improvement plan"] = 240;
  searchTags["delete user"] = 221;
  searchTags["department"] = 210;
  searchTags["department induction"] = 215;
  searchTags["documents"] = 256;
  searchTags["edit entries"] = 221;
  searchTags["edit role"] = 217;
  searchTags["edit user"] = 221;
  searchTags["employee documents"] = 256;
  searchTags["engagements"] = 259;
  searchTags["events"] = 259;
  searchTags["exit"] = 238;
  searchTags["exit checklist"] = 242;
  searchTags["Exits status"] = 242;
  searchTags["FAQ"] = 229;
  searchTags["find skills"] = 219;
  searchTags["goals unassigned"] = 227;
  searchTags["holiday"] = 222;
  searchTags["holiday calendar"] = 222;
  searchTags["improvement plan"] = 240;
  searchTags["incomplete profile"] = 254;
  searchTags["induction"] = 255;
  searchTags["induction checklist"] = 257;
  searchTags["induction presentation"] = 228;
  searchTags["induction status"] = 255;
  searchTags["KPI"] = 202;
  searchTags["KRA"] = 252;
  searchTags["kra list"] = 252;
  searchTags["KRA report"] = 252;
  searchTags["last working day"] = 242;
  searchTags["leader speaks"] = 211;
  searchTags["leaders message"] = 211;
  searchTags["leadership board"] = 211;
  searchTags["leave addition"] = 230;
  searchTags["leave balance "] = 234;
  searchTags["leave correction"] = 234;
  searchTags["leave management"] = 230;
  searchTags["leaves"] = 230;
  searchTags["list of holidays"] = 222;
  searchTags["Location"] = 209;
  searchTags["manage user"] = 221;
  searchTags["message for company"] = 211;
  searchTags["message for team"] = 211;
  searchTags["office"] = 209;
  searchTags["Office headquarter"] = 209;
  searchTags["onboarding"] = 257;
  searchTags["onboarding status"] = 257;
  searchTags["overdue tasks"] = 233;
  searchTags["payroll"] = 205;
  searchTags["pending tasks"] = 232;
  searchTags["pending team task"] = 232;
  searchTags["performance indicator"] = 202;
  searchTags["performance scale"] = 235;
  searchTags["PIP"] = 240;
  searchTags["Policy"] = 216;
  searchTags["post blog"] = 261;
  searchTags["Prelauch setup"] = 224;
  searchTags["presentation"] = 228;
  searchTags["probation changes"] = 237;
  searchTags["probation status"] = 237;
  searchTags["Q&A"] = 229;
  searchTags["questions"] = 229;
  searchTags["resignation"] = 238;
  searchTags["review period"] = 231;
  searchTags["Role report"] = 251;
  searchTags["Roles"] = 251;
  searchTags["salary"] = 245;
  searchTags["salary components"] = 245;
  searchTags["Self KPI"] = 202;
  searchTags["seperation"] = 238;
  searchTags["seperation checklist"] = 242;
  searchTags["Setting"] = 263;
  searchTags["Setup Payroll"] = 247;
  searchTags["Setup salaries"] = 246;
  searchTags["skill list"] = 219;
  searchTags["skill rating scale"] = 206;
  searchTags["skill scale"] = 206;
  searchTags["Skills"] = 212;
  searchTags["SOP"] = 216;
  searchTags["targets unassigned"] = 227;
  searchTags["task report"] = 233;
  searchTags["team"] = 254;
  searchTags["team skill"] = 212;
  searchTags["team task"] = 233;
  searchTags["team task report"] = 233;
  searchTags["team's skill set"] = 212;
  searchTags["test"] = 229;
  searchTags["upload policy"] = 216;
  searchTags["view employee"] = 226;
  searchTags["view skills"] = 212;
  searchTags["view user"] = 226;
  searchTags["website"] = 208;
  searchTags["without goals"] = 227;
  searchTags["without targets"] = 227;
  searchTags["write blog"] = 260;

  //alert(keyword);
  for (var key in searchTags) {
    if (key == keyword) {
      //alert("key " + key + " has value " + searchTags[key]);
      window.location = searchTags[key];
    }
  }
}

