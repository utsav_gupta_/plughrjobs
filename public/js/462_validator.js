$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.apprslst').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function aprsl_set()  {
	var txt = $('#aprsl-list').val();
	//alert(txt);
  if (txt != null && txt != '')    {
	  var obj = JSON.parse(txt);
	  if (obj.peer)   {
	    $('#assign_btn').show();
	    $('#othr_btn').show();
		  clearError($('#errtext1'));
	  } else  {
	    $('#assign_btn').hide();
	    $('#othr_btn').hide();
    	setSuccess($('#errtext1'),'Selected appraisal doesn\'t require ratings from peer or team member(s)');
	  }
  }
}

function def_aprsl()    {
	var xObj = JSON.parse($('#aprsldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var aprObj = new Object;
			aprObj.id = obj._id;
			aprObj.rvteam = obj.rvteam;
      if  (obj.rvteam.indexOf('4') != -1 || obj.rvteam.indexOf('5') != -1 )
  			aprObj.peer = true;
  		else
  			aprObj.peer = false;
			$('<option>').val(JSON.stringify(aprObj)).text(obj.aprtitle).appendTo('#aprsl-list');
		}
	}
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_srat');
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_prat');
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_trat');
		}
	}
}

function def_appraisers()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#modal_peer');
			$('<option>').val(obj.userid).text(obj.username).appendTo('#modal_team');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
	$('#aprsl-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	var rtable = $('table.apprslst').data('footable');
	$('table.apprslst tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.apprslst tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('#formdata').val('');
}

function getGoalName(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
			return(selx.title);
		}
	}
	return "";
}

function verify_ratings()     {
	var ugoals = JSON.parse($('#formdata').val());
	if (ugoals)		{
		var glen = ugoals.length;
		var submitted = 0;
		for (var ctr = 0;ctr < glen; ctr++)		{
			var obj = ugoals[ctr];
      if (!obj.selfrating)
        return false;
    }
  }
}

function defMesgTable()    {
	$('table.apprslst').data('footable').reset();
	$('table.apprslst thead').append('<tr>');
	$('table.apprslst thead tr').append('<th>Employee Name</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Appraisal Type</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Appraisal Target</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Target Type</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">Current Rating</th>');
	$('table.apprslst thead tr').append('<th data-hide="phone,tablet">New Rating</th>');
	$('table.apprslst thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.apprslst thead tr').append('</tr>');
	$('table.apprslst').footable();
}

function showgoals(searchtype)			{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.apprslst').data('footable');
		var rcount = 0;
  	var cusr = $('#cuserid').text();
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
      if (obj.atype != '1')     {
        if (searchtype == '5' && obj.atype == '2')     {
          //alert(JSON.stringify(obj));
        } else  {
			    var newRow = '<tr>';

			    newRow += '<td>';
			    if (obj.username != null)
				    newRow += obj.username;
			    else
				    newRow += '**No Data**';
			    newRow += '</td>';

			    newRow += '<td>';
          switch (searchtype) {
            case '1':
      				newRow += 'Self Appraisal';
      				break;
            case '2':
      				newRow += 'Supervisor Appraisal';
      				break;
            case '3':
      				newRow += 'Skip Supervisor Appraisal';
      				break;
            case '4':
      				newRow += 'HR Appraisal';
      				break;
            case '5':
              if (obj.peeruserid == cusr)
        				newRow += 'Peer Appraisal';
        			else
        				newRow += 'Team member Appraisal';
      				break;
          }
			    newRow += '</td>';

			    newRow += '<td>';
			    if (obj.atitle != null)
				    newRow += obj.atitle;
			    else
				    newRow += '**No Data**';
			    newRow += '</td>';

			    newRow += '<td>';
          switch (obj.atype) {
            case '1':
      				newRow += 'Review';
      				break;
            case '2':
      				newRow += 'KRA';
      				break;
            case '3':
      				newRow += 'Behavioral competencies';
      				break;
            case '4':
      				newRow += 'Corporate values';
      				break;
          }
			    newRow += '</td>';

			    newRow += '<td>';
          switch (searchtype) {
            case '1':
      				newRow += obj.selfratg;
      				break;
            case '2':
      				newRow += obj.suprratg;
      				break;
            case '3':
      				newRow += obj.skipratg;
      				break;
            case '4':
      				newRow += obj.hrratg;
      				break;
            case '5':
              if (obj.peeruserid == cusr)
        				newRow += obj.peerratg;
        			else
        				newRow += obj.teamratg;
      				break;
          }
			    newRow += '</td>';

			    newRow += '<td>';
          switch (searchtype) {
            case '1':
      			  newRow += '<a class="row-edit" href="#"><span id = "s1'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      				break;
            case '2':
      			  newRow += '<a class="row-edit" href="#"><span id = "s2'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      				break;
            case '3':
      			  newRow += '<a class="row-edit" href="#"><span id = "s3'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      				break;
            case '4':
      			  newRow += '<a class="row-edit" href="#"><span id = "s4'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      				break;
            case '5':
              if (obj.peeruserid == cusr)
        			  newRow += '<a class="row-edit" href="#"><span id = "s5'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
        			else
        			  newRow += '<a class="row-edit" href="#"><span id = "s6'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      				break;
          }
			    newRow += '</td>';

			    newRow += '<td id="rowIndex">';
			    newRow += rcount+1;
			    newRow += '</td></tr>';
			    rtable.appendRow(newRow);
			    rcount++;
        }
      }
		}
		rtable.redraw();
	}
}

$(function () {
	$('table.apprslst').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var stype = row.substring(0,2);
		//alert(stype);
		var rowindex = row.substring(2);
		//alert(rowindex);
		clearError($('#errtext1'));
		editdetails(e, stype, rowindex);
	});
});

function editdetails(evtObj, stype, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
	  if (dataObj)		{
		  $('#editrow').val(rowIndex);
		  var rno = parseInt(rowIndex);
		  var obj = dataObj[rno];

	    var s_rpid = $('#aprsl-list').val();
	    if (s_rpid != '' || s_rpid != null )	{
        var sObj = JSON.parse(s_rpid);
        var apid = sObj.id;

        switch (stype) {
          case 's1':
		        $('#modal_sapid').val(apid);
		        $('#modal_sindx').val(rno);
		        $('#modal_sdbid').val(obj._id);
    		    $('#modal_srat').val(obj.selfratg);
    	      $('#RatingModal').modal();
    				break;
          case 's2':
    				break;
          case 's3':
    				break;
          case 's4':
    				break;
          case 's5':
		        $('#modal_papid').val(apid);
		        $('#modal_pindx').val(rno);
		        $('#modal_pdbid').val(obj._id);
    		    $('#modal_prat').val(obj.peerratg);
    	      $('#PeerModal').modal();
    				break;
          case 's6':
		        $('#modal_tapid').val(apid);
		        $('#modal_tindx').val(rno);
		        $('#modal_tdbid').val(obj._id);
    		    $('#modal_trat').val(obj.teamratg);
    	      $('#TeamModal').modal();
    				break;
        }
	    }
    }
    return false;
	}
}

function GetValidInputs()			{
	var s_rpid = $('#aprsl-list').val();
	if (s_rpid == '' || s_rpid == null )	{
		$('#aprsl-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an appraisal');
		return false;
	} else    {
		$('#aprsl-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
  var sObj = JSON.parse(s_rpid);
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.aprslid= sObj.id;
  return formdataObj;
}

function get_self()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/462self',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				showgoals('1');
				setSuccess($('#errtext1'),'Selected appraisal listed below');
      	$('#aprsl-list').prop('disabled', true);
      	$('#clear_btn').prop('disabled', false);
	      $('#self_btn').prop('disabled', true);
	      $('#assign_btn').prop('disabled', true);
	      $('#othr_btn').prop('disabled', true);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function get_apprs()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  $('#modal_aprslid').val(formdataObj.aprslid);
  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/462self',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
        //alert(JSON.stringify(resp.data[0]));

				$('#formdata').val(JSON.stringify(resp.data));
      	$('#aprsl-list').prop('disabled', true);
      	$('#clear_btn').prop('disabled', false);
	      $('#self_btn').prop('disabled', true);
	      $('#assign_btn').prop('disabled', true);
	      $('#othr_btn').prop('disabled', true);

        $('#modal_peer').val(resp.data[0].peeruserid);
        $('#modal_team').val(resp.data[0].teamuserid);

        $('#AssignModal').modal();

			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function get_othr()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/462othr',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				showgoals('5');
				setSuccess($('#errtext1'),'Selected appraisal listed below');
      	$('#aprsl-list').prop('disabled', true);
      	$('#clear_btn').prop('disabled', false);
	      $('#self_btn').prop('disabled', true);
	      $('#assign_btn').prop('disabled', true);
	      $('#othr_btn').prop('disabled', true);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function assign_aprs()				{
	var s_apid = $('#modal_aprslid').val();
	var s_peer = $('#modal_peer').val();
	if (s_peer == '' || s_peer == null )	{
		$('#modal_peer').css('border-color', 'red');
		setError($('#errtext2'),'Please select a peer appraiser');
		return false;
	} else    {
		$('#modal_peer').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_team = $('#modal_team').val();
	if (s_team == '' || s_team == null )	{
		$('#modal_team').css('border-color', 'red');
		setError($('#errtext2'),'Please select a team appraiser');
		return false;
	} else    {
		$('#modal_team').css('border-color', 'default');
		clearError($('#errtext2'));
	}
  if (s_team == s_peer)   {
		$('#modal_team').css('border-color', 'red');
		$('#modal_peer').css('border-color', 'red');
		setError($('#errtext2'),'Peer and Team appraisers should be different');
		return false;
	} else    {
		$('#modal_peer').css('border-color', 'default');
		$('#modal_team').css('border-color', 'default');
		clearError($('#errtext2'));
	}
  
	var assignObj = new  Object();
	assignObj.coid = $('#ccoid').text();
	assignObj.userid = $('#cuserid').text();
	assignObj.aprslid = s_apid;
	assignObj.peerid = s_peer;
	assignObj.teamid = s_team;

	$.ajax({
		type: 'POST',
		url: '/462assign',
		data: assignObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				setSuccess($('#errtext1'),'Appraisers assigned');
				$('#AssignModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function upd_self_rating()				{
	var s_dbid = $('#modal_sdbid').val();
	var s_indx = $('#modal_sindx').val();
	var s_rating = $('#modal_srat').val();
	var s_apid = $('#modal_sapid').val();

	if (s_rating == '' || s_rating == null )	{
		$('#modal_srat').css('border-color', 'red');
		setError($('#errtext3'),'Please select a self rating');
		return false;
	} else    {
		$('#modal_srat').css('border-color', 'default');
		clearError($('#errtext3'));
	}

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = $('#cuserid').text();
	ratingObj.dbid = s_dbid;
	ratingObj.aprslid = s_apid;
	ratingObj.rating = s_rating;

	$.ajax({
		type: 'POST',
		url: '/462srtng',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].selfratg = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
	      var rtable = $('table.apprslst').data('footable');
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
				showgoals('1');

				setSuccess($('#errtext1'),'Self rating updated');
				$('#RatingModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function upd_peer_rating()				{
	var s_dbid = $('#modal_pdbid').val();
	var s_indx = $('#modal_pindx').val();
	var s_rating = $('#modal_prat').val();
	var s_apid = $('#modal_papid').val();

	if (s_rating == '' || s_rating == null )	{
		$('#modal_prat').css('border-color', 'red');
		setError($('#errtext4'),'Please select a self rating');
		return false;
	} else    {
		$('#modal_prat').css('border-color', 'default');
		clearError($('#errtext4'));
	}

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = $('#cuserid').text();
	ratingObj.dbid = s_dbid;
	ratingObj.aprslid = s_apid;
	ratingObj.rating = s_rating;

	$.ajax({
		type: 'POST',
		url: '/462prtng',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].peerratg = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
	      var rtable = $('table.apprslst').data('footable');
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
				showgoals('5');

				setSuccess($('#errtext1'),'Peer rating updated');
				$('#PeerModal').modal('hide');
			} else  {
				setError($('#errtext4'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext4'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function upd_team_rating()				{
	var s_dbid = $('#modal_tdbid').val();
	var s_indx = $('#modal_tindx').val();
	var s_rating = $('#modal_trat').val();
	var s_apid = $('#modal_tapid').val();

	if (s_rating == '' || s_rating == null )	{
		$('#modal_trat').css('border-color', 'red');
		setError($('#errtext5'),'Please select a self rating');
		return false;
	} else    {
		$('#modal_trat').css('border-color', 'default');
		clearError($('#errtext5'));
	}

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = $('#cuserid').text();
	ratingObj.dbid = s_dbid;
	ratingObj.aprslid = s_apid;
	ratingObj.rating = s_rating;

	$.ajax({
		type: 'POST',
		url: '/462trtng',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].teamratg = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
	      var rtable = $('table.apprslst').data('footable');
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
	      $('table.apprslst tbody tr').each(function() {
		      rtable.removeRow($(this));
	      });
				showgoals('5');

				setSuccess($('#errtext1'),'Team member rating updated');
				$('#TeamModal').modal('hide');
			} else  {
				setError($('#errtext5'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext5'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

$(document).ready(function()	{
	def_aprsl();
  aprsl_set();
	def_rlevel();
	def_appraisers();
  defMesgTable();
	$('#self_btn').click(function() 			{
		get_self();
		return false;
	});
	$('#assign_btn').click(function() 			{
		get_apprs();
		return false;
	});
	$('#othr_btn').click(function() 			{
		get_othr();
		return false;
	});
	$('#clear_btn').click(function() 			{
  	$('#aprsl-list').prop('disabled', false);
  	$('#clear_btn').prop('disabled', true);
	  $('#self_btn').prop('disabled', false);
	  $('#assign_btn').prop('disabled', false);
	  $('#othr_btn').prop('disabled', false);
		$('#aprsl-list').css('border-color', 'default');
		clearError($('#errtext1'));
		clearForm();
    aprsl_set();
		return false;
	});
	$('#sratngupd_btn').click(function() 			{
		upd_self_rating();
		return false;
	});
	$('#pratngupd_btn').click(function() 			{
		upd_peer_rating();
		return false;
	});
	$('#tratngupd_btn').click(function() 			{
		upd_team_rating();
		return false;
	});
	$('#assignupd_btn').click(function() 			{
		assign_aprs();
		return false;
	});
});

