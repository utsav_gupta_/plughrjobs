$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selectfile_btn').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').val(fileName);
  });
  $('table.induction').footable();
});
function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#itype-list').prop('selectedIndex', 0);
	$('#dept-list').prop('selectedIndex', 0);
	$('#selfile').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}
function defMesgTable()    {
	$('table.induction').data('footable').reset();
	$('table.induction thead').append('<tr>');
	$('table.induction thead tr').append('<th>Title</th>');
	$('table.induction thead tr').append('<th data-hide="phone,tablet">Induction Type</th>');
	$('table.induction thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.induction thead tr').append('<th data-hide="phone,tablet">File</th>');
	$('table.induction thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.induction thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.induction thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.induction thead tr').append('</tr>');
	$('table.induction').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#deptdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.induction').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.itype)     {
				case '1':
					newRow += 'Mandatory';
					break;
				case '2':
					newRow += 'Optional';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.dept == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.filename != null)
				newRow += obj.filename;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-eye-open" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/315newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.induction').data('footable');

			$('table.induction tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.induction tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.induction').footable().on('click', '.row-edit', function(e) {
    e.preventDefault();
    var rtable = $('table.userdocs').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
	});
	$('table.induction').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});
function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#formdata").val());
    var docfile = dataObj[rowIndex].filename;
		var xcoid = $('#ccoid').text();
		var xuserid = $('#cuserid').text();

    var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + "/" + docfile;
    window.open(docsrc);

    //window.open('/uploads/'+ xcoid + "/" + docfile);
    return false;
  }
}
function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.coid = $('#ccoid').text();
			dataObj.userid = $('#cuserid').text();
			dataObj.dbid= obj._id;
			dataObj.title= obj.title;
			dataObj.itype= obj.itype;
			dataObj.dept= obj.dept;
			dataObj.filename= obj.filename;

			$.ajax({
				type: 'DELETE',
				url: '/indcdocsul',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.induction').data('footable');

						$('table.induction tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.induction tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'induction data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_itype = $('#itype-list').val();
	if (s_itype == null)	{
		$('#itype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Induction Type');
		$('#itype-list').focus();
		return false;
	} else    {
		$('#itype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dept = $('#dept-list').val();
	if (s_dept == null)	{
		$('#dept-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Department');
		$('#dept-list').focus();
		return false;
	} else    {
		$('#dept-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var sfile = $("#selfile").val();
  if (sfile == "")	{
    $('#selfile').css('border-color', 'red');
    setError($('#errtext1'),'Please select a file to upload');
    return false;
  } else    {
    $('#selfile').css('border-color', 'default');
    clearError($('#errtext1'));
  }
	var s_filename = $('#filename').val();

	// Check filetype and filesize
	var filename = $("#selfile").val();
  var extensionAllowed=[".ppt",".pptx"];
  var i = filename.lastIndexOf('.');
  var file_extension= (i < 0) ? '' : filename.substr(i);
	var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

	if(rfound == -1)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),' Only Powerpoint documents (.ppt or .pptx) are allowed');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}
	var fsize = document.getElementById('docfile').files[0].size;
	if ((fsize / 1024) > 10500)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),'Document file should be less than 10MB');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.title= s_title;
	formdataObj.itype= s_itype;
	formdataObj.dept= s_dept;
	formdataObj.fname= s_filename;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
  $('#submit_btn').click();
  return true;

}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/315upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.induction').data('footable');

				$('table.induction tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.induction tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'induction data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
  var options = {
    beforeSend: function()  {
      clearError($('#errtext1'));
      startAjaxIcon();
    },
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.induction').data('footable');

				$('table.induction tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.induction tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'New Document uploaded');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
  	complete: function(response)    {
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#errtext1'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#adddocs-form").ajaxForm(options);

	def_dept();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
});

