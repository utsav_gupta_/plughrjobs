$(function () {
  $("#sdate" ).datepicker();
  $("#edate" ).datepicker();
  $('table.license').footable();
  $('#addlic_btn').show();
  $('#updlic_btn').hide();
});

function SetPagination() {
  var totpages = $("#totmesgs").val();
  if (totpages == 0)
    totpages =1; 
  var options = {
    currentPage: 1,
    totalPages: totpages,
    size:'small',
    bootstrapMajorVersion:3,
    itemTexts: function (type, page, current) {
      switch (type) {
        case "first":
            return "First";
        case "prev":
            return "Previous";
        case "next":
            return "Next";
        case "last":
            return "Last";
        case "page":
            return page;
      }
    },
    onPageClicked: function(e,originalEvent,type,page)    {
      var currPage = $(e.currentTarget).bootstrapPaginator("getPages").current;
      if (currPage != page)   {
        GetNextPage(page);
      }
    }
  }
  $('#licensepages').bootstrapPaginator(options);
}
function defLicenseTable()    {
	$('table.license').data('footable').reset();
	$('table.license thead').append('<tr>');
	$('table.license thead tr').append('<th>Start Date</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">End Date</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">Users</th>');
	$('table.license thead tr').append('<th data-hide="phone,tablet">Comments</th>');
	$('table.license thead tr').append('<th data-hide="phone">Active</th>');
	$('table.license thead tr').append('<th data-hide="phone">Update</th>');
	$('table.license thead tr').append('<th data-hide="phone">Delete</th>');
	$('table.license thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.license thead tr').append('</tr>');
  $('table.license').footable();
}
function fillLicenseData()		{
	var mesgObj = JSON.parse($("#licdata").val());
  var rtable = $('table.license').data('footable');

  var rcount = 0;
  
  var objlen = mesgObj.length;
  if (objlen > 0)		{
  	for (var i = 0; i < objlen; i++)		{
	  	var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			newRow += obj.startdate;
			newRow += '</td><td>';
			newRow += obj.enddate;
			newRow += '</td><td>';
			newRow += obj.users;
			newRow += '</td><td>';
			newRow += obj.notes;
			newRow += '</td><td>';
			newRow += obj.active;
			newRow += '</td><td>';
			if (obj.active)
				newRow += '<a class="row-update" href="#"><span id = "u'+i+'" class="glyphicon glyphicon-pencil" title="Update Type"></span></a>';
		
			newRow += '</td><td>';
			if (!obj.active)
				newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete License"></span></a>';

			newRow += '</td><td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
		  rtable.appendRow(newRow);
			rcount++;
		}
	}
  rtable.redraw();
}

$(function () {
  $('table.license').footable().on('click', '.row-update', function(e) {
    e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
    updatelicense(e, rowindex);
  });
  $('table.license').footable().on('click', '.row-delete', function(e) {
    e.preventDefault();
    var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
    deletelicense(e, rowindex);
  });
});

function updatelicense(evtObj, rowIndex) {
  if (evtObj && evtObj.target) {
  	var dataObj = JSON.parse($("#licdata").val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

    $("#coid").val(obj.coid);
    $("#sdate").val(obj.startdate);
    $("#edate").val(obj.enddate);
    $("#users").val(obj.users);
    $("#notes").val(obj.notes);

    $('#coid').attr("disabled", "disabled");
    $('#addlic_btn').hide();
    $('#updlic_btn').show();
    $('#licenseModal').modal();
  }
}

function deletelicense(evtObj, rowIndex) {
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var dataObj = JSON.parse($("#licdata").val());
			$('#editrow').val(rowIndex);

			var rno = parseInt(rowIndex);
			var obj = dataObj[rno];
		  var licObj = new Object;
		  licObj.coid = obj.coid;
		  licObj.dbid = obj._id;

			$.ajax ({
		    type      : 'DELETE',
				url       : '/a004',
				data      : licObj,
				dataType  : 'json',
				error: function(err) { 
					alert(err.text);
				},
				success: function(unidata)	{
          $("#licdata").val(JSON.stringify(unidata));

		      // DELETE & RE-CREATE Table
		      var rtable = $('table.license').data('footable');
		      $('table.license tbody tr').each(function() {
		          rtable.removeRow($(this));
		      });
		      $('table.license thead tr').each(function() {
		        rtable.removeRow($(this));
		      });
		      defLicenseTable();
          fillLicenseData();
          SetPagination();
		      alert("Company license deleted");
		      $('#licenseModal').modal('hide');
				}
			});
			return false;
		}
	});
}

$(document).ready(function()	{
  defLicenseTable();
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});

	$('#addnew_btn').click(function() {
		var sadmin = $("#comid").val();
		if (sadmin != "")
      $("#coid").val(sadmin);

    $('#coid').attr("disabled", "disabled");
    //$('#coid').removeAttr("disabled");
    $('#addlic_btn').show();
    $('#updlic_btn').hide();
    $('#licenseModal').modal();
  });

	$('#search_btn').click(function() {
		var coid = $("#comid").val();

		if (coid != "")		{
		  var searchObject = new Object();
		  searchObject.coid = coid;
		  $.ajax ({
		  	type: "POST",
			  url: '/a004search',
			  data: searchObject,
			  dataType: 'json',
			  error: function(err) { 
				  alert(err.text);
			  },
			  success: function(unidata)	{
          $("#licdata").val(JSON.stringify(unidata));

          // DELETE & RE-CREATE Table
          var rtable = $('table.license').data('footable');
          $('table.license tbody tr').each(function() {
              rtable.removeRow($(this));
          });
          $('table.license thead tr').each(function() {
            rtable.removeRow($(this));
          });

          defLicenseTable();
          fillLicenseData();
          SetPagination();
          $('#addnew_btn').prop('disabled' ,false);
			  }
		  });
    } else    {
			setError($('#errtext1'),'Please input valid Company id');
    }
	  return false;
	});

	$('#addlic_btn').click(function()     {
    var scoid = $("#coid").val();
    var sstart = $("#sdate").val();
    var send = $("#edate").val();
    var susers = $("#users").val();
    var snotes = $("#notes").val();

		var licObject = new Object;
    licObject.coid = scoid;
    licObject.startdate = sstart;
    licObject.enddate = send;
    licObject.users = susers;
    licObject.notes = snotes;
    licObject.active = true;

	  $.ajax ({
      type      : 'POST',
		  url       : '/a004add',
		  data      : licObject,
		  dataType  : 'json',
		  error: function(err) { 
			  alert(err.text);
		  },
		  success: function(ldata)	{
        if (!ldata.err)   {
          $("#licdata").val(JSON.stringify(ldata));

          // DELETE & RE-CREATE Table
          var rtable = $('table.license').data('footable');
          $('table.license tbody tr').each(function() {
              rtable.removeRow($(this));
          });
          $('table.license thead tr').each(function() {
            rtable.removeRow($(this));
          });

          defLicenseTable();
          fillLicenseData();
          SetPagination();

          alert("New license assigned to Company");
        } else
  			  alert(ldata.text);
        $('#licenseModal').modal('hide');
		  }
	  });
		return false;
	});

	$('#updlic_btn').click(function()     {
    var scoid = $("#coid").val();
    var sstart = $("#sdate").val();
    var send = $("#edate").val();
    var susers = $("#users").val();
    var snotes = $("#notes").val();

		var licObject = new Object;
    licObject.coid = scoid;
    licObject.startdate = sstart;
    licObject.enddate = send;
    licObject.users = susers;
    licObject.notes = snotes;
    licObject.active = true;

	  $.ajax ({
      type      : 'POST',
		  url       : '/a004upd',
		  data      : licObject,
		  dataType  : 'json',
		  error: function(err) { 
			  alert(err.text);
		  },
		  success: function(ldata)	{
        if (!ldata.err)   {
          $("#licdata").val(JSON.stringify(ldata));

          // DELETE & RE-CREATE Table
          var rtable = $('table.license').data('footable');
          $('table.license tbody tr').each(function() {
              rtable.removeRow($(this));
          });
          $('table.license thead tr').each(function() {
            rtable.removeRow($(this));
          });

          defLicenseTable();
          fillLicenseData();
          SetPagination();

          alert("Company license updated");
        } else
  			  alert(ldata.text);
        $('#licenseModal').modal('hide');
		  }
	  });
		return false;
	});
});

