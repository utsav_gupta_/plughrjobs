$(document).ready(function()	{
  if ($("#formdata").val())     {
    showResults(JSON.parse($("#formdata").val()));
    setSuccess($('#errtext1'),'');
  }
	var cdate = new Date;
	var cyear = cdate.getFullYear();
	var pdate = new Date();
	pdate.setDate(pdate.getDate()-30);
	var bdate = new Date();

	var cday = cdate.getDay();
	switch (cday)		{
		case 0:
			cday = 'HAPPY SUNDAY';
			bdate.setDate(bdate.getDate()+6);
			break;
		case 1:
			cday = 'HAPPY MONDAY';
			bdate.setDate(bdate.getDate()+5);
			break;
		case 2:
			cday = 'HAPPY TUESDAY';
			bdate.setDate(bdate.getDate()+4);
			break;
		case 3:
			cday = 'HAPPY WEDNESDAY';
			bdate.setDate(bdate.getDate()+3);
			break;
		case 4:
			cday = 'HAPPY THURSDAY';
			bdate.setDate(bdate.getDate()+2);
			break;
		case 5:
			cday = 'HAPPY FRIDAY';
			bdate.setDate(bdate.getDate()+1);
			break;
		case 6:
			cday = 'HAPPY SATURDAY';
			bdate.setDate(bdate.getDate()+0);
			break;
	}
	$("#greet").text(cday);

	$('#jsearch_btn').click(function() {
    window.location = "/501jsearch";
	});

  function showResults(sData)     {
    $("#results").empty();
    var wdays = ['Full time', 'Few days a week', 'Work from home'];
    var newDiv = "";
    var curncy = ["","USA Dollar","IND Rupees","UK Pounds","EUR","Canadian Dollar","Singapore Dollar","Australian Dollar","UAE Dirhams","Malaysia Ringgit","Thailand Baht"];
    var dlen = sData.length;
    for(i=0;i<dlen;i++)     {

      newDiv += "<div class='well' id='well'"+i+">";
      if (sData[i].company) {
        newDiv += "<h3>" + sData[i].title + "&nbsp;,&nbsp;&nbsp;" + sData[i].company.companyname + "&nbsp;,&nbsp;&nbsp;" + sData[i].jobcity + "</h3>";
      } else  {
        newDiv += "<h3>" + sData[i].title + "&nbsp;,&nbsp;&nbsp;" + 'n/a' + "&nbsp;,&nbsp;&nbsp;" + 'n/a' + "</h3>";
      }
      newDiv += "<p>" + wdays[sData[i].workdays-1] + "</p>";
      if (sData[i].eqtymin != '' && parseFloat(sData[i].eqtymin) != 0 && sData[i].eqtymax != '' && parseFloat(sData[i].eqtymax) != 0 )
        var sstr = " Annual " + sData[i].comp + " ( " + curncy[sData[i].curr] + " )" + " ,  Equity " + sData[i].eqtymin + "% to " + sData[i].eqtymax +"%";
      else
        var sstr = " Annual " + sData[i].comp + " ( " + curncy[sData[i].curr] + " )";
      $('#salry').text(sstr);

      newDiv += "<p>" + sstr;

      newDiv += "<a href='/501jobpage?jobid="+ sData[i]._id +"' target='_blank'>" + " more... " +  "</a></p>";
      newDiv += "</div>";
    }
    $("#results").append(newDiv);
  }
	$('#searchjobs_btn').click(function() {
    var s_stext = $("#stext").val();
	  var s_jobcntry = $('#jobcntry').val();
	  var s_jobcity = $('#jobcity').val();
    if (s_stext == '' && s_jobcntry == '' && s_jobcity == '' )	{
		   $('#label1').css('color', 'red');
       $('#label6').css('color', 'red');
 	     $('#label6').css('color', 'red');
       setError($('#errtext1'),'Please input at least one criteria to search');
       return false;
    } else    {
 		   $('#label1').css('color', 'inherit');
 		   $('#label6').css('color', 'inherit');
       $('#label6').css('color', 'inherit');
       clearError($('#errtext1'));
    }
    var searchObject = new Object();
    searchObject.stext = s_stext;
    searchObject.jobcntry = s_jobcntry;
    searchObject.jobcity = s_jobcity;

    $.ajax({
       type: "POST",
       url: '/jsearch',
       data: searchObject,
       dataType: 'json',
       beforeSend:  function()   {
          startAjaxIcon();
       },
       success: function(robj) {
          if (robj.err1 == false)  {
            if (robj.data.length > 0)   {
              showResults(robj.data);
              setSuccess($('#errtext1'),'Search complete');
            } else
              $("#results").empty();
              setError($('#errtext1'),"No matching jobs found");
            stopAjaxIcon();
          } else
            $("#results").empty();
            setError($('#errtext1'),"No matching jobs found");
            stopAjaxIcon();
          },
       error: function(eobj) {
          setError($('#errtext1'),eobj.text);
          stopAjaxIcon();
       }
    });
    return false;
	});
});

