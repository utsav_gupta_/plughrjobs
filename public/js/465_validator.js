$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.targetlist').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			if (xObj[i].status == '4')  {
			  var obj = new Object;
			  obj._id = xObj[i]._id;
			  obj.frating = xObj[i].frating;
  			$('<option>').val(JSON.stringify(obj)).text(xObj[i].rptitle).appendTo('#rperiod-list');
			}
		}
	}
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_rating');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
	$('#rperiod-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	var rtable = $('table.targetlist').data('footable');
	$('table.targetlist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.targetlist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('#formdata').val('');
}

function getGoalName(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
			return(selx.title);
		}
	}
	return "";
}

function getGoalKRA(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
		  var kname = show_kra_name(selx.kra);
			return(kname);
		}
	}
	return "";
}

function show_kra_name(kraid)    {
  var kras = JSON.parse($('#kradata').val());
  var ktitle = '**No Data**';
  if (kras != null)   {
    var klen = kras.length;
    for (var x=0; x< klen; x++)   {
      if (kras[x]._id == kraid)    {
    	  ktitle =  kras[x].kratitle;
	    }
    }
  }
  return ktitle;
}

function defMesgTable()    {
	$('table.targetlist').data('footable').reset();
	$('table.targetlist thead').append('<tr>');
	$('table.targetlist thead tr').append('<th>Target Name</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">KRA</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">Expected Performance</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">Weightage (%)</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet"> Final Rating</th>');
	$('table.targetlist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.targetlist thead tr').append('</tr>');
	$('table.targetlist').footable();
}

function showgoals()			{
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
	  var tobj = JSON.parse($('#rperiod-list').val());
	  var frat = tobj.frating;

		var glen = mesgObj.length;
		var rtable = $('table.targetlist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.ugoalid != null)
				newRow += getGoalName(obj.ugoalid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ugoalid != null)
				newRow += getGoalKRA(obj.ugoalid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.weight != null)
				newRow += obj.weight;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
      switch (frat)   {
        case '1':
			    if (obj.selfrating != null)
				    newRow += obj.selfrating;
			    else
				    newRow += '**No Data**';
          break;
        case '2':
			    if (obj.mgrrating != null)
				    newRow += obj.mgrrating;
			    else
				    newRow += '**No Data**';
          break;
        case '3':
			    if (obj.skiprating != null)
				    newRow += obj.skiprating;
			    else
				    newRow += '**No Data**';
          break;
      }
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function GetValidInputs()			{
	var temp = $('#rperiod-list').val();
	if (temp == '' || temp == null )	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a review period');
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var tobj = JSON.parse(temp);
	var s_rpid = tobj._id;
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rpid= s_rpid;
  return formdataObj;
}

function get_goals()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/465search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				//alert(JSON.stringify(resp));
				showgoals();
				//fillUserData();
				setSuccess($('#errtext1'),'Targets for selected review period listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_rperiod();
	def_rlevel();
  defMesgTable();
	$('#search_btn').click(function() 			{
		get_goals();
  	$('#rperiod-list').prop('disabled', true);
  	$('#clear_btn').prop('disabled', false);
		return false;
	});
	$('#clear_btn').click(function() 			{
  	$('#rperiod-list').prop('disabled', false);
  	$('#clear_btn').prop('disabled', true);
    clearForm();
		return false;
	});
});

