$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#desc').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

// ------------------------------------------------------------
function disableForm1()		{
	document.getElementById('role-list').disabled = true;
	document.getElementById('kra-list').disabled = true;
	document.getElementById('search_btn').disabled = true;
	document.getElementById('clear_btn').disabled = false;
}

function enableForm1()		{
	document.getElementById('role-list').disabled = false;
	document.getElementById('kra-list').disabled = false;
	document.getElementById('search_btn').disabled = false;
	document.getElementById('clear_btn').disabled = false;
}

function disableForm2()		{
	document.getElementById('title').disabled = true;
	document.getElementById('desc').disabled = true;

	document.getElementById('add_btn').disabled = true;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function enableForm2()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#desc').val('');
	clearError($('#errtext1'));

	document.getElementById('title').disabled = false;
	document.getElementById('desc').disabled = false;

	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function clearTable()		{
	var rtable = $('table.perfgoals').data('footable');

	$('table.perfgoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.perfgoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}
// ------------------------------------------------------------

function getRoleTitle(roleid)		{
	var xObj = JSON.parse($('#roledata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == roleid)
				return obj.roletitle;
		}
	}
	return "";
}

function def_role()    {
	var xObj = JSON.parse($('#roledata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.roletitle).appendTo('#role-list');
		}
	}
}
/*
function def_kra()    {
	var xObj = JSON.parse($('#kradata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.kratitle).appendTo('#kra-list');
		}
	}
}

function def_role()    {
	var rn = JSON.parse($('#subroledata').val());
	var rnames = [];
	for (var x=0; x < rn.length; x++)			{
		rnames.push(rn[x].role);
	}
	var unq = [];
	unq.push($('#ownrole').val());
	$.each(rnames, function(i, el)		{
	  if($.inArray(el, unq) === -1) unq.push(el);
	});
	var glen = unq.length;
	for(var i=0; i < glen; i++)   {
		var rt = getRoleTitle(unq[i]);
		$('<option>').val(unq[i]).text(rt).appendTo('#role-list');
	}
}
*/
function def_kra()    {
	$('#kra-list').empty();
	var crole = $('#role-list').val();
	var rd = JSON.parse($('#roledata').val());
	if (rd != null)			{
		var rlen = rd.length;
		if (rlen != 0)			{
			for(var x=0; x<rlen; x++)		{
				if (crole == rd[x]._id)	{
					var rkra = rd[x].kras;
					if (rkra != null)		{
						var xObj = JSON.parse($('#kradata').val());
						if (xObj != null)    {
							var glen = xObj.length;
							for(var i=0; i<glen; i++)   {
								var obj = xObj[i];
								if ( rkra.indexOf(obj._id) != -1)
									$('<option>').val(obj._id).text(obj.kratitle).appendTo('#kra-list');
							}
						}
					}
				}
			}
		}
	}
}

function defMesgTable()    {
	$('table.perfgoals').data('footable').reset();
	$('table.perfgoals thead').append('<tr>');
	$('table.perfgoals thead tr').append('<th>Title</th>');
	$('table.perfgoals thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.perfgoals thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.perfgoals thead tr').append('<th data-hide="phone,tablet">KRAs</th>');
	$('table.perfgoals thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.perfgoals thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.perfgoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.perfgoals thead tr').append('</tr>');
	$('table.perfgoals').footable();
}

function fillUserData()		{
	var sel4Obj = JSON.parse($('#roledata').val());
	var sel5Obj = JSON.parse($('#kradata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		if (glen != 0)			{
			var rtable = $('table.perfgoals').data('footable');
			var rcount = 0;
			for(var i=0; i<glen; i++)   {
				var obj = mesgObj[i];
				var newRow = '<tr>';
				newRow += '<td>';
				if (obj.title != null)
					newRow += obj.title;
				else
					newRow += '**No Data**';
				newRow += '</td>';
				newRow += '<td>';
				if (obj.desc != null)
					newRow += obj.desc;
				else
					newRow += '**No Data**';
				newRow += '</td>';

				newRow += '<td>';
				var dblen = sel4Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel4Obj[ctr];
					if (obj.role == selx._id)	{
						newRow += selx.roletitle;
						break;
					}
				}
				newRow += '</td>';

				newRow += '<td>';
				var dblen = sel5Obj.length;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel5Obj[ctr];
					if (obj.kra == selx._id)	{
						newRow += selx.kratitle;
						break;
					}
				}
				newRow += '</td>';

				newRow += '<td>';
				newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
				newRow += '</td><td>';
				newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
				newRow += '</td>';
				newRow += '<td id="rowIndex">';
				newRow += rcount+1;
				newRow += '</td></tr>';
				rtable.appendRow(newRow);
				rcount++;
			}
			rtable.redraw();
		}
	} else		{
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/262newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.perfgoals').data('footable');

			$('table.perfgoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.perfgoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.perfgoals').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.perfgoals').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#title').val(obj.title);
		$('#desc').val(obj.desc);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.title= obj.title;
			dataObj.desc= obj.desc;
			dataObj.role= obj.role;
			dataObj.kra= obj.kra;

			$.ajax({
				type: 'DELETE',
				url: '/262',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.perfgoals').data('footable');

						$('table.perfgoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.perfgoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'Goal successfully removed from repository');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_desc = $('#desc').val();
	if (s_desc == '')	{
		$('#desc').css('border-color', 'red');
		setError($('#errtext1'),'Please input Description');
		$('#desc').focus();
		return false;
	} else    {
		$('#desc').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_role = $('#role-list').val();
	if (s_role == null)	{
		$('#role-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Role');
		$('#role-list').focus();
		return false;
	} else    {
		$('#role-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_kra = $('#kra-list').val();
	if (s_kra == null)	{
		$('#kra-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select KRAs');
		$('#kra-list').focus();
		return false;
	} else    {
		$('#kra-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.title= s_title;
	formdataObj.desc= s_desc;
	formdataObj.role= s_role;
	formdataObj.kra= s_kra;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/262add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.perfgoals').data('footable');

				$('table.perfgoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.perfgoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Goal successfully added to repository');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/262upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.perfgoals').data('footable');

				$('table.perfgoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.perfgoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Goal successfully updated in repository');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}
function GetSearchInputs()		{
	var s_role = $('#role-list').val();
	if (s_role == null)	{
		$('#role-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a role');
		$('#role-list').focus();
		return false;
	} else    {
		$('#role-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_kra = $('#kra-list').val();
	if (s_kra == null)	{
		$('#kra-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a KRA');
		$('#kra-list').focus();
		return false;
	} else    {
		$('#kra-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.role = s_role;
	formdataObj.kra = s_kra;
	return formdataObj;
}

function get_goals()		{
	var formdataObj = GetSearchInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/262search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));
				//$('#kradata').val(JSON.stringify(resp.data1));
				$('#totpages').val(JSON.stringify(resp.totpages));

				fillUserData();
				SetPagination();

				disableForm1();
				enableForm2();

				$("#add_btn").prop('disabled',false);
				setSuccess($('#errtext1'),'Goals for selected role listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_role();
	def_kra();
	defMesgTable();
	$('#search_btn').click(function() 		{
		get_goals();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		disableForm2();
		enableForm1();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

