$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function weeks_between(date1, date2) {
	// The number of milliseconds in one week
	var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
	// Convert both dates to milliseconds
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();
	// Calculate the difference in milliseconds
	var difference_ms = Math.abs(date1_ms - date2_ms);
	// Convert back to weeks and return hole weeks
	return Math.floor(difference_ms / ONE_WEEK);
}

function leave_availed(ltype)			{
	//alert(ltype);
	var mesgObj = JSON.parse($('#lvdata').val());
	var ld = 0.0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			if (obj.lname == ltype && obj.status != 'rejected')	{
				ld += parseFloat(obj.ltot);
			}
		}
	}
	return ld;
}

function show_leave_details(ltype)    {
	var xObj = JSON.parse($('#codata').val());
	var hrdate = xObj.hrdate;
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var lper = ['year','quarter','month','week'];
	var hrmon = months.indexOf(hrdate)+1;
	var date = new Date();
	var cmon = date.getMonth()+1;
	var cyr = date.getFullYear();
	var firstDay = new Date(cyr, hrmon-1, 1);
	if (hrmon > cmon)
		cyr++;
	var xObj = JSON.parse($('#lnamedata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == ltype)		{
				$('#lcrt').text(obj.lcriteria);
				$('#laccp').text("Starting " + hrdate + "/" + cyr +", " + obj.ldays + " days of leaves for every completed "+ lper[obj.lper-1]);
				$('#lavbl').text("");
				var ld = 0.00;
				var la = 0.00;
				if (obj.lper == 1 )			{			// Annual
					ld = obj.ldays;
					$('#lavbl').text("Total allowed leave days = " + ld);
				}
				if (obj.lper == 2 )			{			// Quarterly
					if (hrmon > cmon)
						cmon += 8;
					else
						cmon -= 4;
					var qr = Math.floor(cmon / 3);
					ld = Math.floor(obj.ldays * qr);
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				if (obj.lper == 3 )			{			// Monthly
					if (hrmon > cmon)
						cmon += 8;
					else
						cmon -= 4;
					ld = obj.ldays * cmon;
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				if (obj.lper == 4 )			{			// Weekly
					if (hrmon > cmon)
						cmon += 8;
					else
						cmon -= 4;
					var wb = weeks_between(firstDay, date);
					ld = Math.floor(obj.ldays * wb);
					$('#lavbl').text("Leave accrued till today = " + ld);
				}
				la = leave_availed(ltype);
				$('#ltakn').text("Leave availed till today = " + parseFloat(la));
				$('#lv01').val(parseFloat(ld));
				$('#lv02').val(parseFloat(la));
				$('#lv03').val(0);
			}
		}
	}
}

function def_lname()    {
	var xObj = JSON.parse($('#lnamedata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.lname).appendTo('#lname-list');
		}
	}
}
function defMesgTable()    {
	$('table.empleaves').data('footable').reset();
	$('table.empleaves thead').append('<tr>');
	$('table.empleaves thead tr').append('<th>Employee</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Leave Type</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Full days</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Half days</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Total</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Approve</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Reject</th>');
	$('table.empleaves thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.empleaves thead tr').append('</tr>');
	$('table.empleaves').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#lnamedata').val());
	var sel2Obj = JSON.parse($('#empdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	var ld = 0.0;

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.empleaves').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			var dblen = sel2Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel2Obj[ctr];
				if (obj.userid == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.lname == selx._id)	{
					newRow += selx.lname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.hdays != null)
				newRow += obj.hdays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ltot != null)
				newRow += obj.ltot;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-approve" href="#"><span id = "a'+i+'" class="text-success glyphicon glyphicon-ok" title="Approve"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-reject" href="#"><span id = "r'+i+'" class="text-danger glyphicon glyphicon-remove" title="Reject"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/430newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.empleaves').data('footable');

			$('table.empleaves tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.empleaves tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.empleaves').footable().on('click', '.row-approve', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		leave_update(e, rowindex, "a");
	});
	$('table.empleaves').footable().on('click', '.row-reject', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		leave_update(e, rowindex, "r");
	});
});

function leave_update(evtObj, rowIndex, optype) { 
	if (optype === "a")		{
		var mesg = "Leave approval - reconfirm";
		alertify.set({ labels: {
			ok     : "Approve",
			cancel : "Don't Approve"
			} 
		});
	}		else		{
		var mesg = "Leave rejection - reconfirm";
		alertify.set({ labels: {
			ok     : "Reject",
			cancel : "Don't Reject"
			} 
		});
	}
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.lname= obj.lname;
			dataObj.ldays= obj.ldays;
			dataObj.hdays= obj.hdays;
			dataObj.luser = obj.userid;
			dataObj.optype = optype;

			$.ajax({
				type: 'POST',
				url: '/409upd',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));
						$('#lvdata').val(JSON.stringify(resp.lvdata));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.empleaves').data('footable');

						$('table.empleaves tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.empleaves tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						if (optype == "a")
							setSuccess($('#errtext1'),'Leave approved');
						else
							setSuccess($('#errtext1'),'Leave rejected');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}
function GetValidInputs(upd)		{
	var s_lname = $('#lname-list').val();
	if (s_lname == null)	{
		$('#lname-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Leave Type');
		$('#lname-list').focus();
		return false;
	} else    {
		$('#lname-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ldays = $('#ldays').multiDatesPicker('value');
	var s_hdays = $('#hdays').multiDatesPicker('value');
	if (s_ldays == '' && s_hdays == '')	{
		$('#ldays').css('border-color', 'red');
		$('#hdays').css('border-color', 'red');
		setError($('#errtext1'),'Please select  days');
		return false;
	} else    {
		$('#ldays').css('border-color', 'default');
		$('#hdays').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var arr = s_ldays.split(',');
	var ltot = 0.00;
	if (s_ldays != "")
		ltot = arr.length;

	arr = s_hdays.split(',');
	if (s_hdays != "")
		ltot += (arr.length*0.5);

	var ld = parseFloat($('#lv01').val());
	var la = parseFloat($('#lv02').val());

	if (upd)
		la -= parseFloat($('#lv03').val());			// If updating existing leave, remove previous total and add new tot
	la += parseFloat(ltot);

	if (ld < la)		{
		$('#ldays').css('border-color', 'red');
		$('#hdays').css('border-color', 'red');
		setError($('#errtext1'),'Insufficient leave balance');
		//$('#ldays').focus();
		return false;
	} else    {
		$('#ldays').css('border-color', 'default');
		$('#hdays').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.lname= s_lname;
	formdataObj.ldays= s_ldays;
	formdataObj.hdays= s_hdays;
	formdataObj.ltot = ltot;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs(false);
	if (!formdataObj)
		return false;
	formdataObj.lstatus = 'applied';

	$.ajax({
		type: 'POST',
		url: '/430add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));
				$('#lvdata').val(JSON.stringify(resp.lvdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.empleaves').data('footable');

				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'empleaves data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs(true);
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/430upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));
				$('#lvdata').val(JSON.stringify(resp.lvdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.empleaves').data('footable');

				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'empleaves data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_lname();
	defMesgTable();
	fillUserData();
	SetPagination();
});

