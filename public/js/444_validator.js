$(function () {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.policies').footable();
	var uid = $('#cuser').text();
});


function defMesgTable()    {
	$('table.policies').data('footable').reset();
	$('table.policies thead').append('<tr>');
	$('table.policies thead tr').append('<th>Policy ID</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">Policy Title</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">Filename</th>');
	$('table.policies thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.policies thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.policies thead tr').append('</tr>');
	$('table.policies').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.policies').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			newRow += obj.policyid;
			newRow += '</td>';
			newRow += '<td>';
			newRow += obj.policytitle;
			newRow += '</td>';
			newRow += '<td>';
			newRow += obj.docfile;
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span class="glyphicon glyphicon-eye-open" title="View Document"></span></a>';
			newRow += '</td>';
			newRow += '<td>';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}
$(function () {
  $('table.policies').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.policies').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
  });
});

function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#formdata").val());
    var docfile = dataObj[rowIndex].docfile;
		var xcoid = $('#ccoid').text();
		var xuserid = $('#cuserid').text();

    var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + '/policy/' + docfile;
    window.open(docsrc,'_blank');

    return false;
  }
}
function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/444newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.cvalues').data('footable');

			$('table.cvalues tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.cvalues tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
});

