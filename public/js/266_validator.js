$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.userleaves').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function def_emp()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defleavesTable()    {
	$('table.userleaves').data('footable').reset();
	$('table.userleaves thead').append('<tr>');
	$('table.userleaves thead tr').append('<th>Leave Type</th>');
	$('table.userleaves thead tr').append('<th data-hide="phone,tablet">Full days</th>');
	$('table.userleaves thead tr').append('<th data-hide="phone,tablet">Half days</th>');
	$('table.userleaves thead tr').append('<th data-hide="phone,tablet">Total</th>');
	$('table.userleaves thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.userleaves thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.userleaves thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.userleaves thead tr').append('</tr>');
	$('table.userleaves').footable();
}

function fillleavesData()		{
	var sel1Obj = JSON.parse($('#lnamedata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	var ld = 0.0;

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.userleaves').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.lname == selx._id)	{
					newRow += selx.lname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.hdays != null)
				newRow += obj.hdays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ltot != null)
				newRow += obj.ltot;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.lstatus != null)
				newRow += obj.lstatus;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(function () {
	$('table.userleaves').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.userid = obj.userid;
			dataObj.dbid= obj._id;
			dataObj.lname= obj.lname;
			dataObj.ldays= obj.ldays;
			dataObj.hdays= obj.hdays;

			$.ajax({
				type: 'DELETE',
				url: '/266',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
				    $('#formdata').val(JSON.stringify(resp.data));

				    // DELETE & RE-CREATE Table 
				    var rtable = $('table.userleaves').data('footable');

				    $('table.userleaves tbody tr').each(function() {
					    rtable.removeRow($(this));
				    });
				    $('table.userleaves tbody tr').each(function() {
					    rtable.removeRow($(this));
				    });
				    $('#editrow').val(-1);
				    fillleavesData();
						setSuccess($('#errtext1'),'Leave request deleted');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function get_list()		{
	var s_emp = $('#emp-list').val();
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp= s_emp;

	$.ajax({
		type: 'POST',
		url: '/266search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#lnamedata').val(JSON.stringify(resp.lnamedata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.userleaves').data('footable');

				$('table.userleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.userleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillleavesData();
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_emp();
	defleavesTable();
	$('#search_btn').click(function() 			{
		get_list();
		$('#emp-list').prop('disabled', true);
		return false;
	});
	$('#clear_btn').click(function() 			{
		// DELETE & RE-CREATE Table 
		var rtable = $('table.userleaves').data('footable');
		$('table.userleaves tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.userleaves tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('#emp-list').prop('disabled', false);
		return false;
	});
});

