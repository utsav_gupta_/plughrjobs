$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#email').val('');
	$('#pwd1').val('');
	$('#pwd2').val('');
	$('#cname').val('');
	$('#comp').val('');
	$('#addr1').val('');
	$('#addr2').val('');
	$('#city').val('');
	$('#pcode').val('');
	$('#gender-list').prop('selectedIndex', 0);
	$('#ccno').val('');
	$('#ccdate').val('');
	$('#ccname').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.consul').data('footable').reset();
	$('table.consul thead').append('<tr>');
	$('table.consul thead tr').append('<th>Email ID</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Consultant Name</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Company Name</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Address</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">City</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Pincode</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Gender</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Activation Code</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.consul thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.consul thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.consul thead tr').append('</tr>');
	$('table.consul').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.consul').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.emailid != null)
				newRow += obj.emailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.cname != null)
				newRow += obj.cname;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.compname != null)
				newRow += obj.compname;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.addr1 != null)
				newRow += obj.addr1+','+obj.addr2;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.city != null)
				newRow += obj.city;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.pincode != null)
				newRow += obj.pincode;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.gender)     {
				case '1':
					newRow += 'Female';
					break;
				case '2':
					newRow += 'Male';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.actvcode != null)
				newRow += obj.actvcode;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/107newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.consul').data('footable');

			$('table.consul tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.consul tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.consul').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.consul').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#email').val(obj.emailid);
		$('#pwd1').val('');
		$('#pwd2').val('');
		$('#cname').val(obj.cname);
		$('#comp').val(obj.compname);
		$('#addr1').val(obj.addr1);
		$('#addr2').val(obj.addr2);
		$('#city').val(obj.city);
		$('#pcode').val(obj.pincode);
		$('#gender-list').val(obj.gender);
		$('#ccno').val(obj.ccno);
		$('#ccdate').val(obj.ccdate);
		$('#ccname').val(obj.ccname);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	if (evtObj && evtObj.target)    {
		var fdata = JSON.parse($('#formdata').val());

		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex,10)+1;
		var obj = fdata[rno-1];
		dataObj.dbid= obj._id;
		dataObj.email= obj.emailid;
		dataObj.pwd= obj.password;
		dataObj.cname= obj.cname;
		dataObj.comp= obj.compname;
		dataObj.addr1= obj.addr1;
		dataObj.addr2= obj.addr2;
		dataObj.city= obj.city;
		dataObj.pcode= obj.pincode;
		dataObj.gender= obj.gender;
		dataObj.ccno= obj.ccno;
		dataObj.ccdate= obj.ccdate;
		dataObj.ccname= obj.ccname;

		$.ajax({
			type: 'DELETE',
			url: '/107',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#totpages').val(JSON.stringify(resp.totpages));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.consul').data('footable');

					$('table.consul tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.consul tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					SetPagination();
					clearForm();
					setSuccess($('#errtext1'),'consul data deleted');
				} else  {
					setError($('#errtext1'),resp.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
	}
}
function GetValidInputs()		{
	var s_email = $('#email').val();
	if (s_email == '')	{
		$('#email').css('border-color', 'red');
		setError($('#errtext1'),'Please input Email ID');
		$('#email').focus();
		return false;
	} else    {
		$('#email').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var lastAtPos = s_email.lastIndexOf('@');
	var lastDotPos = s_email.lastIndexOf('.');
	var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_email.indexOf('@@') == -1 && lastDotPos > 2 && (s_email.length - lastDotPos) > 2);
	if (!result)	{
		setError($('#errtext1'),'Please input valid email id');
		$('#email').focus();
		return false;
	} else    {
		$('#email').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pwd1 = $('#pwd1').val();
	if (s_pwd1 == '')	{
		$('#pwd1').css('border-color', 'red');
		setError($('#errtext1'),'Please input Password');
		$('#pwd1').focus();
		return false;
	} else    {
		$('#pwd1').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if (s_pwd1.length < 8)   {
		$('#pwd1').css('border-color', 'red');
		setError($('#errtext1'),'Password should be atleast 8 characters');
		$('#pwd1').focus();
		return false;
	} else    {
		$('#pwd1').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pwd2 = $('#pwd2').val();
	if (s_pwd2 == '')	{
		$('#pwd2').css('border-color', 'red');
		setError($('#errtext1'),'Please input Password');
		$('#pwd2').focus();
		return false;
	} else    {
		$('#pwd2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if (s_pwd2.length < 8)   {
		$('#pwd2').css('border-color', 'red');
		setError($('#errtext1'),'Password should be atleast 8 characters');
		$('#pwd2').focus();
		return false;
	} else    {
		$('#pwd2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if (s_pwd1 != s_pwd2)	{
		$('#pwd1').css('border-color', 'red');
		$('#pwd2').css('border-color', 'red');
		setError($('#errtext1'),'Passwords are not same');
		$('#pwd1').focus();
		return false;
	} else    {
		$('#pwd1').css('border-color', 'default');
		$('#pwd2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_cname = $('#cname').val();
	if (s_cname == '')	{
		$('#cname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Consultant Name');
		$('#cname').focus();
		return false;
	} else    {
		$('#cname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_comp = $('#comp').val();
	if (s_comp == '')	{
		$('#comp').css('border-color', 'red');
		setError($('#errtext1'),'Please input Company Name');
		$('#comp').focus();
		return false;
	} else    {
		$('#comp').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_addr1 = $('#addr1').val();
	if (s_addr1 == '')	{
		$('#addr1').css('border-color', 'red');
		setError($('#errtext1'),'Please input Address');
		$('#addr1').focus();
		return false;
	} else    {
		$('#addr1').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_addr2 = $('#addr2').val();
	if (s_addr2 == '')	{
		$('#addr2').css('border-color', 'red');
		setError($('#errtext1'),'Please input ');
		$('#addr2').focus();
		return false;
	} else    {
		$('#addr2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_city = $('#city').val();
	if (s_city == '')	{
		$('#city').css('border-color', 'red');
		setError($('#errtext1'),'Please input City');
		$('#city').focus();
		return false;
	} else    {
		$('#city').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pcode = $('#pcode').val();
	if (s_pcode == '')	{
		$('#pcode').css('border-color', 'red');
		setError($('#errtext1'),'Please input Pincode');
		$('#pcode').focus();
		return false;
	} else    {
		$('#pcode').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_gender = $('#gender-list').val();
	if (s_gender == null)	{
		$('#gender-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Gender');
		$('#gender-list').focus();
		return false;
	} else    {
		$('#gender-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_ccno = $('#ccno').val();
	if (s_ccno == '')	{
		$('#ccno').css('border-color', 'red');
		setError($('#errtext1'),'Please input Credit Card Number');
		$('#ccno').focus();
		return false;
	} else    {
		$('#ccno').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ccdate = $('#ccdate').val();
	if (s_ccdate == '')	{
		$('#ccdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input Valid upto');
		$('#ccdate').focus();
		return false;
	} else    {
		$('#ccdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ccname = $('#ccname').val();
	if (s_ccname == '')	{
		$('#ccname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Name in card');
		$('#ccname').focus();
		return false;
	} else    {
		$('#ccname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.email= s_email;
	formdataObj.pwd= s_pwd1;
	formdataObj.cname= s_cname;
	formdataObj.comp= s_comp;
	formdataObj.addr1= s_addr1;
	formdataObj.addr2= s_addr2;
	formdataObj.city= s_city;
	formdataObj.pcode= s_pcode;
	formdataObj.gender= s_gender;
	formdataObj.ccno= s_ccno;
	formdataObj.ccdate= s_ccdate;
	formdataObj.ccname= s_ccname;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/107add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.consul').data('footable');

				$('table.consul tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.consul tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'consul data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/107upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.consul').data('footable');

				$('table.consul tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.consul tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'consul data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

