$(function() {
	$(document).ajaxStart(function(){
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
	$('table.targetlist').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			if (xObj[i].status == '4')  {
			  var obj = new Object;
			  obj._id = xObj[i]._id;
			  obj.frating = xObj[i].frating;
  			$('<option>').val(JSON.stringify(obj)).text(xObj[i].rptitle).appendTo('#rperiod-list');
			}
		}
	}
}

function def_replist()    {
	var xObj = JSON.parse($('#repdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#rep-list');
		}
	}
}

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	$('#usr-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#usr-list');
		}
	}
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_rating');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
	$('#rperiod-list').prop('selectedIndex', 0);
	$('#usr-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	$('#formdata').val('');

	$('#rperiod-list').prop('disabled', false);
	$('#rep-list').prop('disabled', false);
	$('#usr-list').empty();
	$('#usr-list').prop('disabled', true);
	$('#search_btn').prop('disabled', true);

	var rtable = $('table.targetlist').data('footable');
	$('table.targetlist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.targetlist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('#formdata').val('');
}

function getGoalName(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
			return(selx.title);
		}
	}
	return "";
}

function getGoalKRA(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
		  var kname = show_kra_name(selx.kra);
			return(kname);
		}
	}
	return "";
}

function show_kra_name(kraid)    {
  var kras = JSON.parse($('#kradata').val());
  var ktitle = '**No Data**';
  if (kras != null)   {
    var klen = kras.length;
    for (var x=0; x< klen; x++)   {
      if (kras[x]._id == kraid)    {
    	  ktitle =  kras[x].kratitle;
	    }
    }
  }
  return ktitle;
}

function get_skips()		{
	var s_rperiod = $('#rperiod-list').val();
	var s_repobj = JSON.parse($('#rep-list').val());
	var s_usr = s_repobj.userid;
	var s_role = s_repobj.roleid;

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod= s_rperiod;
	formdataObj.usr= s_usr;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/467skips',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#usrdata').val(JSON.stringify(resp.data));
				def_usr();
				$('#rperiod-list').prop('disabled', true);
				$('#rep-list').prop('disabled', true);
				$('#usr-list').prop('disabled', false);
				$('#search_btn').prop('disabled', false);
				setSuccess($('#errtext1'),'Skip reportees selected');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function GetValidInputs()			{
	var temp1 = $('#rperiod-list').val();
	if (temp1 == '' || temp1 == null )	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a review period');
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var temp2 = $('#usr-list').val();
	if (temp2 == '' || temp2 == null )	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a team member');
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var tobj = JSON.parse(temp1);
	var s_rpid = tobj._id;

	var s_usrobj = JSON.parse(temp2);
	var s_usr = s_usrobj.userid;
	var s_role = s_usrobj.roleid;

	var s_usrobj = JSON.parse($('#usr-list').val());
	if (s_usrobj)   {
	  var s_usr = s_usrobj.userid;
	  var s_role = s_usrobj.roleid;

	  var formdataObj = new  Object();
	  formdataObj.coid = $('#ccoid').text();
	  formdataObj.userid = $('#cuserid').text();
	  formdataObj.rperiod = s_rpid;
	  formdataObj.usr = s_usr;
    return formdataObj;
  } else  {
    return null;
  }
}

function get_goals()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/467search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				showgoals();
				$('#rperiod-list').prop('disabled', true);
				$('#usr-list').prop('disabled', true);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function defMesgTable()    {
	$('table.targetlist').data('footable').reset();
	$('table.targetlist thead').append('<tr>');
	$('table.targetlist thead tr').append('<th>Target Name</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">KRA</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">Expected Performance</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet">Weightage (%)</th>');
	$('table.targetlist thead tr').append('<th data-hide="phone,tablet"> Final Rating</th>');
	$('table.targetlist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.targetlist thead tr').append('</tr>');
	$('table.targetlist').footable();
}

function showgoals()			{
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
	  var tobj = JSON.parse($('#rperiod-list').val());
	  var frat = tobj.frating;

		var glen = mesgObj.length;
		var rtable = $('table.targetlist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.ugoalid != null)
				newRow += getGoalName(obj.ugoalid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ugoalid != null)
				newRow += getGoalKRA(obj.ugoalid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.weight != null)
				newRow += obj.weight;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
      switch (frat)   {
        case '1':
			    if (obj.selfrating != null)
				    newRow += obj.selfrating;
			    else
				    newRow += '**No Data**';
          break;
        case '2':
			    if (obj.mgrrating != null)
				    newRow += obj.mgrrating;
			    else
				    newRow += '**No Data**';
          break;
        case '3':
			    if (obj.skiprating != null)
				    newRow += obj.skiprating;
			    else
				    newRow += '**No Data**';
          break;
      }
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(document).ready(function()	{
	def_rperiod();
	def_replist();
	def_rlevel();
  defMesgTable();
	$('#clear_btn').click(function() 			{
		clearForm();
		return false;
	});
	$('#search_btn').click(function() 			{
		get_goals();
		return false;
	});
	$('#skips_btn').click(function() 			{
		get_skips();
		return false;
	});
});

