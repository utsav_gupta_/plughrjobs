/*
var FormData = {formdata};
var MembData = membdata;
var TotPages = totpages;
var EditRow = -1;
*/

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#memb-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth : true
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#cname').val('');
	$('#purp').val('');
	$('#memb-list').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_memb()    {
	var xObj = JSON.parse($('#membdata').val());
	//var xObj = MembData;
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#memb-list');
		}
	}
}
function defMesgTable()    {
	$('table.committees').data('footable').reset();
	$('table.committees thead').append('<tr>');
	$('table.committees thead tr').append('<th>Committee Name</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Purpose</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Members</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.committees thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.committees thead tr').append('</tr>');
	$('table.committees').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#membdata').val());
	var mesgObj = JSON.parse($('#formdata').val());

	//var sel3Obj = MembData;
	//var mesgObj = FormData;

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.committees').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.comname != null)
				newRow += obj.comname;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.purpose != null)
				newRow += obj.purpose;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.cmembers.length;
			var dblen = sel3Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.cmembers[temp];
					if (inx == selx.userid)	{
						newRow += selx.username+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/314newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			//FormData = JSON.stringify(mesgObj);

			// DELETE & RE-CREATE Table 
			var rtable = $('table.committees').data('footable');

			$('table.committees tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.committees tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.committees').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.committees').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		//var dataObj = FormDatal
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#cname').val(obj.comname);
		$('#purp').val(obj.purpose);
		$('#memb-list').select2('val', obj.cmembers);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			//var fdata = FormDatal
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.cname= obj.comname;
			dataObj.purp= obj.purpose;
			dataObj.memb= obj.cmembers;

			$.ajax({
				type: 'DELETE',
				url: '/314',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						//FormData = JSON.stringify(resp.data);
						//TotPages = JSON.stringify(resp.totpages);

						// DELETE & RE-CREATE Table 
						var rtable = $('table.committees').data('footable');

						$('table.committees tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.committees tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'committees data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_cname = $('#cname').val();
	if (s_cname == '')	{
		$('#cname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Committee Name');
		$('#cname').focus();
		return false;
	} else    {
		$('#cname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_purp = $('#purp').val();
	if (s_purp == '')	{
		$('#purp').css('border-color', 'red');
		setError($('#errtext1'),'Please input Purpose');
		$('#purp').focus();
		return false;
	} else    {
		$('#purp').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_memb = $('#memb-list').val();
	if (s_memb == null)	{
		$('#memb-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Members');
		$('#memb-list').focus();
		return false;
	} else    {
		$('#memb-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.cname= s_cname;
	formdataObj.purp= s_purp;
	formdataObj.memb= s_memb;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/314add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.committees').data('footable');

				$('table.committees tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.committees tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'committees data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/314upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.committees').data('footable');

				$('table.committees tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.committees tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'committees data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_memb();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

