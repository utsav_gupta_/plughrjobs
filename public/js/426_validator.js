$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#bgrp-list').select2({
		placeholder: 'Select multiple Blood Groups',
		allowClear: true,
		dropdownAutoWidth : true
	});
	$('#pint-list').select2({
		placeholder: 'Select multiple personal interests',
		allowClear: true,
		dropdownAutoWidth : true
	});
	$('#skill-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
  $('table.users').footable();
});
function clearForm()		{
	$('#dbid').val('');
	$('#bgrp-list').select2('val', null);
	$('#pint-list').select2('val', null);
	$('#skill-list').select2('val', null);
	$('#addr').val('');
	$('#uni').val('');
	$('#degree').val('');
	$('#college').val('');
	$('#emp').val('');
	//clearError($('#errtext1'));
	document.getElementById('search_btn').disabled = false;
}
function def_skill()    {
	var xObj = JSON.parse($('#skilldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#skill-list');
		}
	}
}
function getLoc(locid)		{
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == locid)
				return obj.oname;
		}
	}
	return "";
}
function getRole(roleid)		{
	var xObj = JSON.parse($('#roledata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == roleid)
				return obj.roletitle;
		}
	}
	return "";
}
function getDept(depid)		{
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == depid)
				return obj.depname;
		}
	}
	return "";
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">View Profile</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.role != null)
				newRow += getRole(obj.role);
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.dept != null)
				newRow += getDept(obj.dept);
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.location != null)
				newRow += getLoc(obj.location);
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-user" title="View"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(function () {
	$('table.users').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.docs').data('footable');
    var row = $(this).parents('tr:first');
		clearError($('#errtext1'));
    viewDoc(e, row[0]["rowIndex"]-1);
	});
});
function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);
		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		var coid = $('#ccoid').text();
		var userid = obj.userid;
    window.open('/426user?coid='+coid + '&userid=' + userid);
    return false;
  }
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;
	mesgObject.sobj = $("#listquery").val();

	$.ajax ({
		type: 'GET',
		url: '/426newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.users').data('footable');

			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
function GetValidInputs()		{
	var s_bgrp = $('#bgrp-list').val();
	var s_pint = $('#pint-list').val();
	var s_addr = $('#addr').val();
	var s_uni = $('#uni').val();
	var s_degree = $('#degree').val();
	var s_college = $('#college').val();
	var s_emp = $('#emp').val();
	var s_skill = $('#skill-list').val();

  if (!s_bgrp && !s_pint && !s_addr && !s_uni && !s_degree && !s_college && !s_emp && !s_skill )   {
      setError($('#errtext1'),'Please input at least one criteria');
      return false;
  } else  {
      clearError($('#errtext1'));
  }
	var searchObj = new  Object();
	searchObj.coid = $('#ccoid').text();
	searchObj.userid = $('#cuserid').text();
	searchObj.bgrp = s_bgrp;
	searchObj.pint = s_pint;
	searchObj.addr = s_addr;
	searchObj.uni = s_uni;
	searchObj.degree = s_degree;
	searchObj.college = s_college;
	searchObj.emp = s_emp;
	searchObj.skill = s_skill;
	return searchObj;
}

function search_emp()		{
	var searchObj = GetValidInputs();
	if (!searchObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/426search',
		data: searchObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			//alert(JSON.stringify(resp));
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				//alert(JSON.stringify(resp.totpages));
				$('#totpages').val(JSON.stringify(resp.totpages));
				//alert(JSON.stringify(resp.sobj));
        $("#listquery").val(resp.sobj);

				$('#editrow').val(-1);
				//defMesgTable();

			  // DELETE & RE-CREATE Table 
			  var rtable = $('table.users').data('footable');

			  $('table.users tbody tr').each(function() {
				  rtable.removeRow($(this));
			  });
			  $('table.users tbody tr').each(function() {
				  rtable.removeRow($(this));
			  });

				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Search Complete');
			} else  {
			  // DELETE & RE-CREATE Table 
			  var rtable = $('table.users').data('footable');

			  $('table.users tbody tr').each(function() {
				  rtable.removeRow($(this));
			  });
			  $('table.users tbody tr').each(function() {
				  rtable.removeRow($(this));
			  });
				clearForm();
				$('#editrow').val(-1);
			  setError($('#errtext1'),"No users found for the defined criteria");
			}
			stopAjaxIcon();
		},
		error: function(err) {
		  setError($('#errtext1'),"No users found for the defined criteria");
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_skill();
	defMesgTable();
	//fillUserData();
	SetPagination();
	$('#search_btn').click(function() 			{
		search_emp();
		return false;
	});
});

