$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.salaries').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode == 46)
		return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#mon-list').prop('selectedIndex', 0);
	$('#year-list').prop('selectedIndex', 0);
	$('#usr').val('');
	clearError($('#errtext1'));
}

function def_stru()    {
	$('#stru-list').empty();
	var xObj = JSON.parse($('#strudata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.sstitle).appendTo('#stru-list');
		}
	}
}

function defMesgTable()    {
	$('table.salaries').data('footable').reset();
	$('table.salaries thead').append('<tr>');
	$('table.salaries thead tr').append('<th>Salary Component</th>');
	$('table.salaries thead tr').append('<th data-hide="phone,tablet">Earnings</th>');
	$('table.salaries thead tr').append('<th data-hide="phone,tablet">Deductions</th>');
	$('table.salaries thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.salaries thead tr').append('</tr>');
	$('table.salaries').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.salaries').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			switch (obj.month)     {
				case '1':
					newRow += 'Option 1';
					break;
				case '2':
					newRow += 'Option 2';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.year)     {
				case '1':
					newRow += 'Option 1';
					break;
				case '2':
					newRow += 'Option 2';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/246newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.salaries').data('footable');

			$('table.salaries tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.salaries tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.salaries').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.salaries').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});


function data_delete(evtObj, rowIndex) { 
	if (evtObj && evtObj.target)    {
		var fdata = JSON.parse($('#formdata').val());

		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex,10)+1;
		var obj = fdata[rno-1];
		dataObj.dbid= obj._id;
		dataObj.mon= obj.month;
		dataObj.year= obj.year;
		dataObj.usr= obj.userid;

		$.ajax({
			type: 'DELETE',
			url: '/246',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#totpages').val(JSON.stringify(resp.totpages));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.salaries').data('footable');

					$('table.salaries tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.salaries tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					SetPagination();
					clearForm();
					setSuccess($('#errtext1'),'salaries data deleted');
				} else  {
					setError($('#errtext1'),resp.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
	}
}
function GetValidInputs()		{
	var s_mon = $('#mon-list').val();
	if (s_mon == null)	{
		$('#mon-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Month');
		$('#mon-list').focus();
		return false;
	} else    {
		$('#mon-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_year = $("#year-list option:selected").text();
	if (s_year == null)	{
		$('#year-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Year');
		$('#year-list').focus();
		return false;
	} else    {
		$('#year-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_usr = $('#usr').val();
	if (s_usr == '')	{
		$('#usr').css('border-color', 'red');
		setError($('#errtext1'),'Please input User ID');
		$('#usr').focus();
		return false;
	} else    {
		$('#usr').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_stru = $('#stru-list').val();
	if (s_stru == null)	{
		$('#stru-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select Salary Structure');
		$('#year-list').focus();
		return false;
	} else    {
		$('#stru-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.mon= s_mon;
	formdataObj.year= s_year;
	formdataObj.usr= s_usr;
	formdataObj.stru= s_stru;
	return formdataObj;
}

function search_sal()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/246search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				if (resp.data.length > 0)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#totpages').val(JSON.stringify(resp.totpages));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.salaries').data('footable');

					$('table.salaries tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.salaries tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					SetPagination();
					clearForm();
					setSuccess($('#errtext1'),'Salary data listed below');
				} else	{
					setError($('#errtext1'),"Salary data not available");
				}
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/246upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.salaries').data('footable');

				$('table.salaries tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.salaries tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'salaries data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_stru();
	defMesgTable();
	$('#search_btn').click(function() 			{
		search_sal();
		return false;
	});
});

