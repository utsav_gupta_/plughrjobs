$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.userskills').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function def_emp()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defskillsTable()    {
	$('table.userskills').data('footable').reset();
	$('table.userskills thead').append('<tr>');
	$('table.userskills thead tr').append('<th>Ser. No </th>');
	$('table.userskills thead tr').append('<th>Skill</th>');
	$('table.userskills thead tr').append('<th>Rating</th>');
	$('table.userskills thead tr').append('</tr>');
	$('table.userskills').footable();
}

function fillskillsData()		{
	var uskldata = JSON.parse($('#formdata').val());
	if (uskldata[0])			{
		var roleskills = uskldata[0].skratings;

		// DELETE & RE-CREATE Table 
		var rtable = $('table.userskills').data('footable');
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});

		if (roleskills != null)		{
			var glen = roleskills.length;
			if (glen > 0)		{
				for(var i=0; i<glen; i++)   {
					var obj = roleskills[i];
					var newRow = '<tr>';

					newRow += '<td>';
					newRow += i+1;
					newRow += '</td>';

					newRow += '<td>';
					newRow += obj.skdesc;
					newRow += '</td>';

					newRow += '<td>';
					newRow += obj.ratlevel + " ("+ obj.ratdesc + ")";
					newRow += '</td>';

					newRow += '</tr>';
					rtable.appendRow(newRow);
				}
			}
			rtable.redraw();
		}
	}
}
function get_list()		{
	var s_emp = $('#emp-list').val();
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp= s_emp;

	$.ajax({
		type: 'POST',
		url: '/212search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#skilldata').val(JSON.stringify(resp.skilldata));
				$('#skrdata').val(JSON.stringify(resp.skrdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.userskills').data('footable');

				$('table.userskills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.userskills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillskillsData();
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_emp();
	defskillsTable();
	$('#search_btn').click(function() 			{
		get_list();
		$('#emp-list').prop('disabled', true);
		return false;
	});
	$('#clear_btn').click(function() 			{
		// DELETE & RE-CREATE Table 
		var rtable = $('table.userskills').data('footable');
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('#emp-list').prop('disabled', false);
		return false;
	});
});

