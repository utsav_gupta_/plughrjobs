$(function() {
	$('#jdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#edate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#rpid-list').select2({
		placeholder: 'Select required review periods',
		allowClear: true
	});
	$('#rvteam-list').select2({
		placeholder: 'Select required appraisers',
		allowClear: true
	});
	$('#loc-list').select2({
		placeholder: 'Select required locations',
		allowClear: true
	});
	$('#dept-list').select2({
		placeholder: 'Select required departments',
		allowClear: true
	});
	$('#role-list').select2({
		placeholder: 'Select required role types',
		allowClear: true
	});
	$('#etype-list').select2({
		placeholder: 'Select required employee types',
		allowClear: true
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function brat()  {
	var sts = $('#bvrt').prop('checked');
	if (sts)  {
	  $('#bvwt').prop('disabled', false);
	  $('#bvtxt').prop('disabled', false);
	} else  {
	  $('#bvwt').prop('disabled', true);
	  $('#bvtxt').prop('disabled', true);
  }
}

function vrat()  {
	var sts = $('#cvrt').prop('checked');
	if (sts)
	  $('#cvwt').prop('disabled', false);
	else
	  $('#cvwt').prop('disabled', true);
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#aprtitle').val('');
	$('#rvteam-list').select2('val', null);
	$('#loc-list').select2('val', null);
	$('#dept-list').select2('val', null);
	$('#role-list').select2('val', null);
	$('#etype-list').select2('val', null);
	$('#jdate').val('');
	$('#sdate').val('');
	$('#edate').val('');
	$('#rv_kra').prop('checked',true);
	$('#rv_rev').prop('checked',false);
	$('#rpwt').val('');
	$('#rpid-list').select2('val', null);
	$('#bvrt').removeAttr('checked');
	$('#bvwt').val('');
	$('#bvtxt').val('');
	$('#cvrt').removeAttr('checked');
	$('#cvwt').val('');
	$('#trng').removeAttr('checked');
	$('#recom').removeAttr('checked');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_rpid()    {
	var xObj = JSON.parse($('#rpiddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status == '4')
  			$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rpid-list');
		}
	}
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}

function def_loc()    {
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#loc-list');
		}
	}
}

function defMesgTable()    {
	$('table.apprsl').data('footable').reset();
	$('table.apprsl thead').append('<tr>');
	$('table.apprsl thead tr').append('<th>Appraisal Title</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Reviewes</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Review Periods</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Review %</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Behavioral %</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Behavioral skills</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Values %</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Trainings</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Recommendations</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.apprsl thead tr').append('<th data-hide="phone,tablet">Mark Complete</th>');
	$('table.apprsl thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.apprsl thead tr').append('</tr>');
	$('table.apprsl').footable();
}

function fillUserData()		{
	var sel1Obj = JSON.parse($('#rpiddata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.apprsl').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];

			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.aprtitle != null)
				newRow += obj.aprtitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.rvteam != null) {
			  var xlen = obj.rvteam.length;
  			for(var x=0; x < xlen; x++) {
			    switch (obj.rvteam[x]) {
			      case '1':
      				newRow += "Self, ";
      				break;
			      case '2':
      				newRow += "Supervisor, ";
      				break;
			      case '3':
      				newRow += "Skip Supervisor, ";
      				break;
			      case '4':
      				newRow += "HR, ";
      				break;
			      case '5':
      				newRow += "Peer, ";
      				break;
			      case '6':
      				newRow += "Team member, ";
      				break;
          }    				
        }    				
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var olen = obj.rperiod.length;
			var dblen = sel1Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.rperiod[temp];
					if (inx == selx._id)	{
						newRow += selx.rptitle+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.rpwt != null)
				newRow += obj.rpwt;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.bvwt != null)
				newRow += obj.bvwt;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.bvtxt != null)
				newRow += obj.bvtxt;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.cvwt != null)
				newRow += obj.cvwt;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.trng != null)
				newRow += obj.trng;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.recom != null)
				newRow += obj.recom;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.status != null)   {
		    switch (obj.status) {
		      case 1:
    				newRow += "Scheduled";
    				break;
		      case 2:
    				newRow += "Started";
    				break;
		      case 3:
    				newRow += "Ended";
    				break;
        }    				
			} else
				newRow += '**No Data**';
			newRow += '</td>';

		  newRow += '<td>';
			if (obj.status == 1 )
			  newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			else
			  newRow += '-';
		  newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
		  newRow += '</td><td>';
			if (obj.status == 2 )
			  newRow += '<a class="row-close" href="#"><span id = "c'+i+'" class="glyphicon glyphicon-ok" title="Mark Complete"></span></a>';
			else
			  newRow += '-';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/268newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.apprsl').data('footable');

			$('table.apprsl tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.apprsl tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.apprsl').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.apprsl').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
	$('table.apprsl').footable().on('click', '.row-close', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_close(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#aprtitle').val(obj.aprtitle);

    if (obj.rvoptn == '1') {
  		$('#rv_kra').prop('checked',true);
  	  $('#rv_rev').prop('checked',false);
  	  $('#rpid-list').select2('val', '');
	    $('#rpid-list').prop('disabled', true);
  	} else  {
  		$('#rv_kra').prop('checked',false);
  	  $('#rv_rev').prop('checked',true);
	    $('#rpid-list').prop('disabled', false);
  	  $('#rpid-list').select2('val', obj.rperiod);
	  }
		$('#rvteam-list').select2('val', obj.rvteam);

	  $('#loc-list').select2('val', obj.loc);
	  $('#dept-list').select2('val', obj.dept);
	  $('#role-list').select2('val', obj.role);
	  $('#etype-list').select2('val', obj.etype);
	  $('#jdate').val(obj.jdate);
	  $('#sdate').val(obj.sdate);
	  $('#edate').val(obj.edate);

		$('#rpwt').val(obj.rpwt);
		if (obj.bvrt == 'true')   {
			$('#bvrt').prop('checked', true);
		  $('#bvwt').val(obj.bvwt);
		  $('#bvtxt').val(obj.bvtxt.join("\n"));
	    $('#bvwt').prop('disabled', false);
	    $('#bvtxt').prop('disabled', false);
		} else  {
			$('#bvrt').prop('checked', false);
		  $('#bvwt').val('');
		  $('#bvtxt').val('');
	    $('#bvwt').prop('disabled', true);
	    $('#bvtxt').prop('disabled', true);
		}

		if (obj.cvrt == 'true') {
			$('#cvrt').prop('checked', true);
  		$('#cvwt').val(obj.cvwt);
  	  $('#cvwt').prop('disabled', false);
		} else  {
			$('#cvrt').prop('checked', false);
  		$('#cvwt').val('');
  	  $('#cvwt').prop('disabled', true);
		}

		if (obj.trng == 'true')
			$('#trng').prop('checked', true);
		else
			$('#trng').prop('checked', false);

		if (obj.recom == 'true')
			$('#recom').prop('checked', true);
		else
			$('#recom').prop('checked', false);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE? All appraisal related data will also be deleted and cannot be recovered";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
		  var fdata = JSON.parse($('#formdata').val());

		  var dataObj = new Object();
		  dataObj.coid = $('#ccoid').text();
		  dataObj.user = $('#cuser').text();
		  var rno = parseInt(rowIndex,10)+1;
		  var obj = fdata[rno-1];
		  dataObj.dbid = obj._id;

		  $.ajax({
			  type: 'DELETE',
			  url: '/268',
			  data: dataObj,
			  dataType: 'json',
			  beforeSend:  function()   {
				  startAjaxIcon();
			  },
			  success: function(resp) {
				  if (!resp.err)   {
					  $('#formdata').val(JSON.stringify(resp.data));
					  $('#totpages').val(JSON.stringify(resp.totpages));

					  // DELETE & RE-CREATE Table 
					  var rtable = $('table.apprsl').data('footable');

					  $('table.apprsl tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('table.apprsl tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('#editrow').val(-1);
					  //defMesgTable();
					  fillUserData();
					  SetPagination();
					  clearForm();
					  setSuccess($('#errtext1'),'apprsl data deleted');
				  } else  {
					  setError($('#errtext1'),o.text);
				  }
				  stopAjaxIcon();
			  },
			  error: function(err) {
				  setError($('#errtext1'),err.responseText);
				  stopAjaxIcon();
			  }
		  });
		  return false;
		}
	});
}

function data_close(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to mark this appraisal as COMPLETED?";
	alertify.set({ labels: {
		ok     : "Mark as Complete",
		cancel : "Not Yet"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
		  var fdata = JSON.parse($('#formdata').val());

		  var dataObj = new Object();
		  dataObj.coid = $('#ccoid').text();
		  dataObj.user = $('#cuser').text();
		  var rno = parseInt(rowIndex,10)+1;
		  var obj = fdata[rno-1];
		  dataObj.dbid = obj._id;

		  $.ajax({
			  type: 'POST',
			  url: '/268mark',
			  data: dataObj,
			  dataType: 'json',
			  beforeSend:  function()   {
				  startAjaxIcon();
			  },
			  success: function(resp) {
				  if (!resp.err)   {
					  $('#formdata').val(JSON.stringify(resp.data));
					  $('#totpages').val(JSON.stringify(resp.totpages));

					  // DELETE & RE-CREATE Table 
					  var rtable = $('table.apprsl').data('footable');

					  $('table.apprsl tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('table.apprsl tbody tr').each(function() {
						  rtable.removeRow($(this));
					  });
					  $('#editrow').val(-1);
					  //defMesgTable();
					  fillUserData();
					  SetPagination();
					  clearForm();
					  setSuccess($('#errtext1'),'apprsl data deleted');
				  } else  {
					  setError($('#errtext1'),o.text);
				  }
				  stopAjaxIcon();
			  },
			  error: function(err) {
				  setError($('#errtext1'),err.responseText);
				  stopAjaxIcon();
			  }
		  });
		  return false;
		}
	});
}

function GetValidInputs()		{
	var s_title = $('#aprtitle').val();
	if (s_title == null || s_title == '' )	{
		$('#aprtitle').css('border-color', 'red');
		setError($('#errtext1'),'Please provide a title for appraisal');
		$('#aprtitle').focus();
		return false;
	} else    {
		$('#aprtitle').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sdate = $('#sdate').val();
	if (s_sdate == null || s_jdate == '')	{
		$('#sdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select a date on which appraisal begins');
		$('#sdate').focus();
		return false;
	} else    {
		$('#sdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_edate = $('#edate').val();
	if (s_edate == null || s_jdate == '')	{
		$('#edate').css('border-color', 'red');
		setError($('#errtext1'),'Please select a date on which appraisal ends');
		$('#edate').focus();
		return false;
	} else    {
		$('#edate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_loc = $('#loc-list').val();
	if (s_loc == null)	{
		$('#loc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select applicable location(s)');
		$('#loc-list').focus();
		return false;
	} else    {
		$('#loc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_dept = $('#dept-list').val();
	if (s_dept == null)	{
		$('#dept-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select applicable department(s)');
		$('#dept-list').focus();
		return false;
	} else    {
		$('#dept-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_role = $('#role-list').val();
	if (s_role == null)	{
		$('#role-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select applicable role type(s)');
		$('#role-list').focus();
		return false;
	} else    {
		$('#role-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_etype = $('#etype-list').val();
	if (s_etype == null)	{
		$('#etype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select applicable employee type(s)');
		$('#etype-list').focus();
		return false;
	} else    {
		$('#etype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_jdate = $('#jdate').val();
	if (s_jdate == null || s_jdate == '')	{
		$('#jdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select cut-off joining date');
		$('#jdate').focus();
		return false;
	} else    {
		$('#jdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_rvteam = $('#rvteam-list').val();
	if (s_rvteam == null)	{
		$('#rvteam-list').css('border-color', 'red', '!important');
		setError($('#errtext1'),'Please select Appraiser(s)');
		$('#rvteam-list').focus();
		return false;
	} else    {
		$('#rvteam-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

  var s_rvoptn = $("input[name=rvoptn]:checked").val();
	if (s_rvoptn == null)	{
		$('#rvoptn').css('border-color', 'red');
		setError($('#errtext1'),'Please include either KRAs or past reviews in appraisal');
		$('#rvoptn').focus();
		return false;
	} else    {
		$('#rvoptn').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_rpwt = $('#rpwt').val();
  if (s_rpwt == '' || parseInt(s_rpwt) < 1 || parseInt(s_rpwt) > 100 )	{
		$('#rpwt').css('border-color', 'red');
		setError($('#errtext1'),'Please input proper Weightage (%)');
		$('#rpwt').focus();
		return false;
	} else    {
		$('#rpwt').css('border-color', 'default');
		clearError($('#errtext1'));
	}
  var s_rpid = $('#rpid-list').val();
	if (s_rvoptn == '2')	{
	  if (s_rpid == null)	{
		  $('#rpid-list').css('border-color', 'red');
		  setError($('#errtext1'),'Please select Review Period(s)');
		  $('#rpid-list').focus();
		  return false;
	  } else    {
		  $('#rpid-list').css('border-color', 'default');
		  clearError($('#errtext1'));
	  }
	}
	var s_bvrt = false;
	if ($('#bvrt').prop('checked'))
		var s_bvrt = true;

  if (s_bvrt == true)   {
	  var s_bvwt = $('#bvwt').val();
	  if (s_bvwt == '' || parseInt(s_bvwt) < 1 || parseInt(s_bvwt) > 100 )	{
		  $('#bvwt').css('border-color', 'red');
		  setError($('#errtext1'),'Please input proper Weightage (%)');
		  $('#bvwt').focus();
		  return false;
	  } else    {
		  $('#bvwt').css('border-color', 'default');
		  clearError($('#errtext1'));
	  }
	  var s_bvtxt = $('#bvtxt').val();
	  if (s_bvtxt == '')	{
		  $('#bvtxt').css('border-color', 'red');
		  setError($('#errtext1'),'Please input Behavioral skills');
		  $('#bvtxt').focus();
		  return false;
	  } else    {
		  $('#bvtxt').css('border-color', 'default');
		  clearError($('#errtext1'));
	  }
    var s_bvtxt = $("#bvtxt").val().split("\n");
  } else  {
	  var s_bvwt = '0';
	  var s_bvtxt = '';
  }
	var s_cvrt = false;
	if ($('#cvrt').prop('checked'))
		var s_cvrt = true;

  if (s_cvrt)   {
	  var s_cvwt = $('#cvwt').val();
	  if (s_cvwt == '' || parseInt(s_cvwt) < 1 || parseInt(s_cvwt) > 100 )	{
		  $('#cvwt').css('border-color', 'red');
		  setError($('#errtext1'),'Please input proper Weightage (%)');
		  $('#cvwt').focus();
		  return false;
	  } else    {
		  $('#cvwt').css('border-color', 'default');
		  clearError($('#errtext1'));
	  }
  } else  {
	  var s_cvwt = '0';
  }
	var s_trng = false;
	if ($('#trng').prop('checked'))
		var s_trng = true;

	var s_recom = false;
	if ($('#recom').prop('checked'))
		var s_recom = true;

  var totwt = parseInt(s_rpwt) + parseInt(s_bvwt) + parseInt(s_cvwt);
  //alert(totwt);
  if (totwt != 100 )	{
	  $('#rpwt').css('border-color', 'red');
    if (s_bvrt)
  	  $('#bvwt').css('border-color', 'red');
    if (s_cvrt)
  	  $('#cvwt').css('border-color', 'red');
	  setError($('#errtext1'),'Total Weightage should be 100%');
	  $('#rpwt').focus();
	  return false;
  } else    {
	  $('#rpwt').css('border-color', 'default');
	  $('#bvwt').css('border-color', 'default');
	  $('#cvwt').css('border-color', 'default');
	  clearError($('#errtext1'));
  }

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.aprtitle = s_title;
	formdataObj.rvteam = s_rvteam;
	formdataObj.loc = s_loc;
	formdataObj.dept = s_dept;
	formdataObj.role = s_role;
	formdataObj.etype = s_etype;
	formdataObj.jdate = s_jdate;
	formdataObj.sdate = s_sdate;
	formdataObj.edate = s_edate;
	formdataObj.rvoptn = s_rvoptn;
	formdataObj.rpwt = s_rpwt;
	formdataObj.rpid = s_rpid;
	formdataObj.bvrt = s_bvrt;
	formdataObj.bvwt = s_bvwt;
	formdataObj.bvtxt = s_bvtxt;
	formdataObj.cvrt = s_cvrt;
	formdataObj.cvwt = s_cvwt;
	formdataObj.trng = s_trng;
	formdataObj.recom = s_recom;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/268add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.apprsl').data('footable');

				$('table.apprsl tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.apprsl tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'apprsl data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/268upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.apprsl').data('footable');

				$('table.apprsl tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.apprsl tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'apprsl data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_rpid();
	def_loc();
	def_dept();
	defMesgTable();
	fillUserData();
	SetPagination();

	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
  $('input:radio[name=rvoptn]').change(function() {
    if (this.value == '1') {
    	$('#rpid-list').prop('disabled', true);
    }
    else if (this.value == '2') {
    	$('#rpid-list').prop('disabled', false);
    }
  });
});

