$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#psdate').datepicker({ dateFormat: 'dd-mm-yy'	});
	$('#prdate').datepicker({ dateFormat: 'dd-mm-yy'	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function fydates() {
  var txt = parseInt($('#fysdate').val());
  var totxt = txt + 11;
  var cd = new Date();
  var incr = 0;
  if (totxt > 12) {
    totxt -= 12;
    incr++;
  }
  var yr = cd.getFullYear()+incr;
  totxt = totxt + "-" + yr
  $('#fyedate').val(totxt);
}

function hrdates() {
  var txt = parseInt($('#hrdate').val());
  var totxt = txt + 11;
  var cd = new Date();
  var incr = 0;
  if (totxt > 12) {
    totxt -= 12;
    incr++;
  }
  var yr = cd.getFullYear()+incr;
  totxt = totxt + "-" + yr
  $('#hr2date').val(totxt);
}

function def_we(fld)   {
  var txt = parseInt($('#wwstart').val());
  txt--;
  //alert(txt);
  var wdays = ["Mondays", "Tuesdays", "Wednesdays", "Thursdays", "Fridays", "Saturdays", "Sundays"]
  var tmp1 = txt+5;
  var tmp2 = txt+6;
  if (tmp1 > 6)
    tmp1 = tmp1-7;
  if (tmp2 > 6)
    tmp2 = tmp2-7;
  //alert("Weekend days are "+wdays[tmp1]+" and "+ wdays[tmp2]);
  var wmesg = "";
  var wno = parseInt($('#weno').val());
  switch (wno)   {
    case 1:
        wmesg = "Weekend days are every " + wdays[tmp2];
        break;
    case 2:
        wmesg = "Weekend days are every half " + wdays[tmp1] + " and " + wdays[tmp2];
        break;
    case 3:
        wmesg = "Weekend days are every " + wdays[tmp1] + " and " + wdays[tmp2];
        break;
    case 4:
        wmesg = "Weekend days are first and third " + wdays[tmp1] + " of every month and all " + wdays[tmp2];
        break;
    case 5:
        wmesg = "Weekend days are second and fourth " + wdays[tmp1] + " of every month and all " + wdays[tmp2];
        break;
  }
	$('#wetext').text(wmesg);
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		//alert(JSON.stringify(formdataObj));
    if (formdataObj.fysdate && formdataObj.fysdate != "")   {
  		$('#fysdate').val(formdataObj.fysdate.substr(0,1));
  		$('#fyedate').val(formdataObj.fyedate);
  	}
		$('#wwstart').val(formdataObj.wwstart);
		$('#weno').val(formdataObj.weno);
    if (formdataObj.hrdate && formdataObj.hrdate != "")   {
  		$('#hrdate').val(formdataObj.hrdate.substr(0,1));
	  	$('#hr2date').val(formdataObj.hr2date);
	  }
		$('#maxl').val(formdataObj.maxl);
		$('#negl').val(formdataObj.negl);
		$('#preh').val(formdataObj.preh);
		$('#inth').val(formdataObj.inth);
		$('#such').val(formdataObj.such);
		$('#maxcf').val(formdataObj.maxcf);
		$('#maxtr').val(formdataObj.maxtr);

		$('#psdate').val(formdataObj.psdate);
		$('#prdate').val(formdataObj.prdate);
		if (formdataObj.rperiod && formdataObj.rperiod != "")
			$('#rperiod').val(formdataObj.rperiod);
		if (formdataObj.prwho && formdataObj.prwho != "")
			$('#prwho').val(formdataObj.prwho);
	}
}

function GetValidInputs()			{
  var cd = new Date();
	var s_fysdate = $('#fysdate').val();
	if (s_fysdate == '')	{
		$('#fysdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select start date for Financial year');
		return false;
	} else    {
		$('#fysdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
  s_fysdate = s_fysdate +"-"+ cd.getFullYear();
	var s_fyedate = $('#fyedate').val();

	var s_wwstart = $('#wwstart').val();
	if (s_wwstart == '')	{
		$('#wwstart').css('border-color', 'red');
		setError($('#errtext1'),'Please select start date for work week');
		return false;
	} else    {
		$('#wwstart').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_weno = $('#weno').val();
	if (s_weno == '')	{
		$('#weno').css('border-color', 'red');
		setError($('#errtext1'),'Please select number of weekend days');
		return false;
	} else    {
		$('#weno').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_hrdate = $('#hrdate').val();
	if (s_hrdate == '')	{
		$('#hrdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select start date for HR Calender');
		return false;
	} else    {
		$('#hrdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
  s_hrdate = s_hrdate +"-"+ cd.getFullYear();
  s_hr2date = $('#hr2date').val();

	var s_maxl = $('#maxl').val();
	if (s_maxl == '')	{
		$('#maxl').css('border-color', 'red');
		setError($('#errtext1'),'Please input maximun allowed consecutive leave days');
		return false;
	} else    {
		$('#maxl').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_negl = $('#negl').val();
	if (s_negl == '')	{
		$('#negl').css('border-color', 'red');
		setError($('#errtext1'),'Please input maximun allowed negative leaves');
		return false;
	} else    {
		$('#negl').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_preh = $('#preh').val();
	if (s_preh == '')	{
		$('#preh').css('border-color', 'red');
		setError($('#errtext1'),'Please input Preceeding holidays to be included in leaves');
		return false;
	} else    {
		$('#preh').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_inth = $('#inth').val();
	if (s_inth == '')	{
		$('#inth').css('border-color', 'red');
		setError($('#errtext1'),'Please input intervening holidays to be included in leaves');
		return false;
	} else    {
		$('#inth').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_such = $('#such').val();
	if (s_such == '')	{
		$('#such').css('border-color', 'red');
		setError($('#errtext1'),'Please input succeeding holidays to be included in leaves');
		return false;
	} else    {
		$('#such').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_maxcf = $('#maxcf').val();
	if (s_maxcf == '')	{
		$('#maxcf').css('border-color', 'red');
		setError($('#errtext1'),'Please input maximum  allowerd carry-forward leaves');
		return false;
	} else    {
		$('#maxcf').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_maxtr = $('#maxtr').val();
	if (s_maxtr == '')	{
		$('#maxtr').css('border-color', 'red');
		setError($('#errtext1'),'Please input maximum  allowerd transfer leaves');
		return false;
	} else    {
		$('#maxtr').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_psdate = $('#psdate').val();
	if (s_psdate == '')	{
		$('#psdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select start date for Current performance review');
		return false;
	} else    {
		$('#psdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_prdate = $('#prdate').val();
	if (s_prdate == '')	{
		$('#prdate').css('border-color', 'red');
		setError($('#errtext1'),'Please select end date for Current performance review');
		return false;
	} else    {
		$('#prdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_rperiod = $('#rperiod').val();
	if (!s_rperiod)	{
		$('#rperiod').css('border-color', 'red');
		setError($('#errtext1'),'Please select performance review frequency');
		return false;
	} else    {
		$('#rperiod').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_prwho = $('#prwho').val();
	if (!s_prwho)	{
		$('#prwho').css('border-color', 'red');
		setError($('#errtext1'),'Please select performance reviewer');
		return false;
	} else    {
		$('#prwho').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();

	formdataObj.fysdate = s_fysdate;
	formdataObj.fyedate = s_fyedate;
	formdataObj.wwstart = s_wwstart;
	formdataObj.weno = s_weno;
	formdataObj.hrdate = s_hrdate;
	formdataObj.hr2date = s_hr2date;
	formdataObj.maxl = s_maxl;
	formdataObj.negl = s_negl;
	formdataObj.preh = s_preh;
	formdataObj.inth = s_inth;
	formdataObj.such = s_such;
	formdataObj.maxcf = s_maxcf;
	formdataObj.maxtr = s_maxtr;
	formdataObj.prdate = s_prdate;
	formdataObj.psdate = s_psdate;
	formdataObj.rperiod = s_rperiod;
	formdataObj.prwho = s_prwho;
  return formdataObj;
}
function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/263upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				//$('#formdata').val(JSON.stringify(resp.data));
				//showformdata();
				setSuccess($('#errtext1'),'Settings updated. Any changes to review date(s) or frequency will take few hours reflect in the system.');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	showformdata();
  def_we();
  hrdates();
	$('#upd_btn').click(function() 			{
		data_upd();
	});
});

