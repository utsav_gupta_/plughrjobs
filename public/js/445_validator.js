/*
var FormData = {formdata};
var MembData = membdata;
var TotPages = totpages;
var EditRow = -1;
*/

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.committees').footable();
});
function defMesgTable()    {
	$('table.committees').data('footable').reset();
	$('table.committees thead').append('<tr>');
	$('table.committees thead tr').append('<th>Committee Name</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Purpose</th>');
	$('table.committees thead tr').append('<th data-hide="phone,tablet">Members</th>');
	$('table.committees thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.committees thead tr').append('</tr>');
	$('table.committees').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#membdata').val());
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.committees').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.comname != null)
				newRow += obj.comname;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.purpose != null)
				newRow += obj.purpose;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.cmembers.length;
			var dblen = sel3Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.cmembers[temp];
					if (inx == selx.userid)	{
						newRow += selx.username+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/445newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			//FormData = JSON.stringify(mesgObj);

			// DELETE & RE-CREATE Table 
			var rtable = $('table.committees').data('footable');

			$('table.committees tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.committees tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
});

