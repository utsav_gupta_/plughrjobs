$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.perfgoals').footable();
	var uid = $('#cuser').text();
	$('#modal_tdate').datepicker({ dateFormat: 'dd-mm-yy'});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status != '4')
  			$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function def_rlevel()    {
	var xObj = JSON.parse($('#rleveldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.rlevel).text(obj.rlevel).appendTo('#modal_rating');
		}
	}
}

function GetValidInputs()			{
	var s_rpid = $('#rperiod-list').val();
	if (s_rpid == '' || s_rpid == null )	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select a review period');
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rpid= s_rpid;
  return formdataObj;
}

function clearForm()		{
	$('#dbid').val('');
	$('#rperiod-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));

	var rtable = $('table.perfgoals').data('footable');
	$('table.perfgoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.perfgoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('#formdata').val('');
}

function getGoalName(gid)			{
	var goals = JSON.parse($('#goalsdata').val());
	var dblen = goals.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = goals[ctr];
		if (gid == selx._id)	{
			return(selx.title);
		}
	}
	return "";
}

function verify_ratings()     {
	var ugoals = JSON.parse($('#formdata').val());
	if (ugoals)		{
		var glen = ugoals.length;
		var submitted = 0;
		for (var ctr = 0;ctr < glen; ctr++)		{
			var obj = ugoals[ctr];
      if (!obj.selfrating)
        return false;
    }
  }
}

function showgoals()			{
	var rcount = 0;
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		$('#accordion1').empty();
		var glen = ugoals.length;
		var submitted = 0;

		var newDiv = "<div id='goalslist'>";
		for (var ctr = 0;ctr < glen; ctr++)		{
			var obj = ugoals[ctr];

			var or = "";
			if (!obj.selfrating)
				or = "Pending";
			else
				or = obj.selfrating;
			var mr = "";
			if (!obj.mgrrating)
				mr = "Pending";
			else
				mr = obj.mgrrating;
			var sr = "";
			if (!obj.skiprating)
				sr = "Pending";
			else
				sr = obj.skiprating;
			
			newDiv += "<div id='goal" + ctr + "'><h3><b><a href='#'>"+getGoalName(obj.ugoalid)+"</a></b></h3>";
			newDiv += "<div>";
			newDiv += "<p><span><b>Expected Performance </b></span>"+obj.meets+"</p>";
			newDiv += "<p><span><b>Weightage (%) </b></span>"+obj.weight;
			newDiv += "<span class='col-sm-offset-1'><b>Self-rating </b></span>"+or;
			newDiv += "<span class='col-sm-offset-1'><b>Manager rating </b></span>"+mr;
			newDiv += "<span class='col-sm-offset-1'><b>Skip Manager rating </b></span>"+sr+"</p>";

			// For Task #1
			newDiv += "<p>";
			newDiv += "<span><b>Task #1 </b></span>";
			newDiv += "is ";
			if (obj.task1desc)			{
				newDiv += "<b>\" </b><i>" + obj.task1desc + "</i><b> \"</b>. ";
				if (obj.task1date)
					newDiv += "You have planned to complete it by " + obj.task1date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task1status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #2
			newDiv += "<p>";
			newDiv += "<span><b>Task #2 </b></span>";
			newDiv += "is ";
			if (obj.task2desc)			{
				newDiv += "<b>\" </b><i>" + obj.task2desc + "</i><b> \"</b>. ";
				if (obj.task2date)
					newDiv += "You have planned to complete it by " + obj.task2date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task2status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #3
			newDiv += "<p>";
			newDiv += "<span><b>Task #3 </b></span>";
			newDiv += "is ";
			if (obj.task3desc)			{
				newDiv += "<b>\" </b><i>" + obj.task3desc + "</i><b> \"</b>. ";
				if (obj.task3date)
					newDiv += "You have planned to complete it by " + obj.task3date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task3status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #4
			newDiv += "<p>";
			newDiv += "<span><b>Task #4 </b></span>";
			newDiv += "is ";
			if (obj.task4desc)			{
				newDiv += "<b>\" </b><i>" + obj.task4desc + "</i><b> \"</b>. ";
				if (obj.task4date)
					newDiv += "You have planned to complete it by " + obj.task4date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task4status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			// For Task #5
			newDiv += "<p>";
			newDiv += "<span><b>Task #5 </b></span>";
			newDiv += "is ";
			if (obj.task5desc)			{
				newDiv += "<b>\" </b><i>" + obj.task5desc + "</i><b> \"</b>. ";
				if (obj.task5date)
					newDiv += "You have planned to complete it by " + obj.task5date + ". ";
				else
					newDiv += "You haven't defined any completion date yet. ";
				if (obj.task5status == 1)
					newDiv += "<span class = 'text-success'> Task is now completed. <span>";
				else
					newDiv += "<span class = 'text-danger'> Task is incomplete. <span>";
			} else
				newDiv += "not yet defined. ";
			newDiv += "<p>";

			if (obj.trgt_submited == '1')			{
			  if (!obj.self_submited || obj.self_submited == '0')			{
				  newDiv += "<button class='btn-xs btn-default col-sm-1' id = 'upd1_btn' data-btn ='" + ctr + "' type='button'>Task 1</button>";
				  newDiv += "<button class='btn-xs btn-default col-sm-1 col-sm-offset-1' id = 'upd2_btn' data-btn ='" + ctr + "' type='button'>Task 2</button>";
				  newDiv += "<button class='btn-xs btn-default col-sm-1 col-sm-offset-1' id = 'upd3_btn' data-btn ='" + ctr + "' type='button'>Task 3</button>";
				  newDiv += "<button class='btn-xs btn-default col-sm-1 col-sm-offset-1' id = 'upd4_btn' data-btn ='" + ctr + "' type='button'>Task 4</button>";
				  newDiv += "<button class='btn-xs btn-default col-sm-1 col-sm-offset-1' id = 'upd5_btn' data-btn ='" + ctr + "' type='button'>Task 5</button>";
				  newDiv += "<button class='btn-xs btn-default col-sm-1 col-sm-offset-1' id = 'upd6_btn' data-btn ='" + ctr + "' type='button'>Self Rating</button>";
			  } else	{
				  submitted = 1;
				  newDiv += "<span class = 'text-danger'><b>Review is already submitted for review & approval. </b></span>";
			  }
		  } else	{
			  submitted = 1;
			  newDiv += "<span class = 'text-danger'><b>Your supervisor is still finalising your targets! </b></span>";
		  }
			newDiv += "<br><hr>";
			newDiv += "</div></div>";
		}
		newDiv += "</div>";
		$('#accordion1').append(newDiv);

		if (glen > 0 && submitted == 0)
			$('#submit_btn').prop('disabled',false);
		else
			$('#submit_btn').prop('disabled',true);
	}
	$('#accordion1').css("visibility","visible");
	$('#accordion1').accordion("refresh");
}

function get_goals()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/432search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				//alert(JSON.stringify(resp));
				if (resp.incvfile != "")		{
					$('#view_btn').show();
				}	else	{
					$('#view_btn').hide();
				}
				showgoals();
				//fillUserData();
				setSuccess($('#errtext1'),'Targets for selected review period listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function showTask1(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_tid').val('1');
		$('#modal_dbid').val(obj._id);
		$('#modal_indx').val(indx);
		$('#modal_tdesc').val(obj.task1desc);
		$('#modal_tdate').val(obj.task1date);
		$('#modal_tsts').val(obj.task1status);

	  $('#GoalModal').modal();
  }
  return false;
}

function showTask2(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_tid').val('2');
		$('#modal_dbid').val(obj._id);
		$('#modal_indx').val(indx);
		$('#modal_tdesc').val(obj.task2desc);
		$('#modal_tdate').val(obj.task2date);
		$('#modal_tsts').val(obj.task2status);

	  $('#GoalModal').modal();
  }
  return false;
}

function showTask3(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_tid').val('3');
		$('#modal_dbid').val(obj._id);
		$('#modal_indx').val(indx);
		$('#modal_tdesc').val(obj.task3desc);
		$('#modal_tdate').val(obj.task3date);
		$('#modal_tsts').val(obj.task3status);

	  $('#GoalModal').modal();
  }
  return false;
}

function showTask4(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_tid').val('4');
		$('#modal_dbid').val(obj._id);
		$('#modal_indx').val(indx);
		$('#modal_tdesc').val(obj.task4desc);
		$('#modal_tdate').val(obj.task4date);
		$('#modal_tsts').val(obj.task4status);

	  $('#GoalModal').modal();
  }
  return false;
}

function showTask5(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_tid').val('5');
		$('#modal_dbid').val(obj._id);
		$('#modal_indx').val(indx);
		$('#modal_tdesc').val(obj.task5desc);
		$('#modal_tdate').val(obj.task5date);
		$('#modal_tsts').val(obj.task5status);

	  $('#GoalModal').modal();
  }
  return false;
}

function upd_task()				{
	var s_indx = $('#modal_indx').val();
	var s_dbid = $('#modal_dbid').val();
	var s_tid = $('#modal_tid').val();
	var s_tsts = $('#modal_tsts').val();

	var s_tdesc = $('#modal_tdesc').val();
	if (s_tdesc == '' || s_tdesc == null )	{
		$('#modal_tdesc').css('border-color', 'red');
		setError($('#errtext2'),'Please input Task description');
		return false;
	} else    {
		$('#modal_tdesc').css('border-color', 'default');
		clearError($('#errtext2'));
	}
	var s_tdate = $('#modal_tdate').val();
	if (s_tdate == '' || s_tdate == null )	{
		$('#modal_tdate').css('border-color', 'red');
		setError($('#errtext2'),'Please input expected completion date');
		return false;
	} else    {
		$('#modal_tdate').css('border-color', 'default');
		clearError($('#errtext2'));
	}

	var taskObj = new  Object();
	taskObj.coid = $('#ccoid').text();
	taskObj.userid = $('#cuserid').text();
	taskObj.dbid = s_dbid;
	taskObj.tid = s_tid;
	taskObj.tdesc = s_tdesc;
	taskObj.tdate = s_tdate;
	taskObj.tsts = s_tsts;

	//alert(JSON.stringify(taskObj));
	$.ajax({
		type: 'POST',
		url: '/432task',
		data: taskObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				switch (s_tid)		{
					case '1':
						ugoals[s_indx].task1desc = s_tdesc;
						ugoals[s_indx].task1date = s_tdate;
						ugoals[s_indx].task1status = s_tsts;
						break;
					case '2':
						ugoals[s_indx].task2desc = s_tdesc;
						ugoals[s_indx].task2date = s_tdate;
						ugoals[s_indx].task2status = s_tsts;
						break;
					case '3':
						ugoals[s_indx].task3desc = s_tdesc;
						ugoals[s_indx].task3date = s_tdate;
						ugoals[s_indx].task3status = s_tsts;
						break;
					case '4':
						ugoals[s_indx].task4desc = s_tdesc;
						ugoals[s_indx].task4date = s_tdate;
						ugoals[s_indx].task4status = s_tsts;
						break;
					case '5':
						ugoals[s_indx].task5desc = s_tdesc;
						ugoals[s_indx].task5date = s_tdate;
						ugoals[s_indx].task5status = s_tsts;
						break;
				}
				$('#formdata').val(JSON.stringify(ugoals));
				showgoals();

				setSuccess($('#errtext1'),'Self-rating updated');
				$('#GoalModal').modal('hide');
			} else  {
				setError($('#errtext2'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext2'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function showRating(indx)	{
	var ugoals = JSON.parse($('#formdata').val());

	if (ugoals)		{
		var glen = ugoals.length;
		var obj = ugoals[indx];

		$('#modal_indx1').val(indx);
		$('#modal_dbid').val(obj._id);
		$('#modal_rting').val(obj.selfrating);

	  $('#RatingModal').modal();
  }
  return false;
}

function upd_rating()				{
	var s_dbid = $('#modal_dbid').val();
	var s_rating = $('#modal_rating').val();
	var s_indx = $('#modal_indx1').val();

	var ratingObj = new  Object();
	ratingObj.coid = $('#ccoid').text();
	ratingObj.userid = $('#cuserid').text();
	ratingObj.dbid = s_dbid;
	ratingObj.rating = s_rating;

	$.ajax({
		type: 'POST',
		url: '/432self',
		data: ratingObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (resp.err == 0)   {
				var ugoals = JSON.parse($('#formdata').val());
				ugoals[s_indx].selfrating = s_rating;

				$('#formdata').val(JSON.stringify(ugoals));
				showgoals();

				setSuccess($('#errtext1'),'Self-rating updated');
				$('#RatingModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

function submit_progress()				{
	var mesg = "Once submitted, you won't be able to make changes. Pls confirm";
	alertify.set({ labels: {
		ok     : "Submit",
		cancel : "Don't submit"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var ratingObj = new  Object();
			ratingObj.coid = $('#ccoid').text();
			ratingObj.userid = $('#cuserid').text();
			ratingObj.rpid = $('#rperiod-list').val();

			//alert(JSON.stringify(ratingObj));

			$.ajax({
				type: 'POST',
				url: '/432submit',
				data: ratingObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (resp.err == 0)   {
						var ugoals = JSON.parse($('#formdata').val());
						for (var ctr=0; ctr < ugoals.length; ctr++)			{
							ugoals[ctr].self_submited = '1';
						}
						$('#formdata').val(JSON.stringify(ugoals));
						showgoals();

						setSuccess($('#errtext1'),'Performance progress submitted successfully');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.text);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function view_doc()		{
	var s_rperiod = $('#rperiod-list').val();

	var sData = new Object;
	sData.rperiod = s_rperiod;
	sData.user = $('#cuserid').text();

	$.ajax({
		type: 'get',
		url: '/434doc',
		data: sData,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			//alert(JSON.stringify(resp));
			if (!resp.err)   {
				var incvfile = resp.data.incvfile;
				var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + $('#ccoid').text() + "/" + resp.data.userid + "/" + incvfile;
				window.open(docsrc);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	$('#view_btn').hide();
	def_rperiod();
	def_rlevel();
	$("#accordion1").accordion({ 
		header: "h3",          
		autoheight: true,
		active: false,
		alwaysOpen: false,
		fillspace: false,
		collapsible: true,
    highstyle : "auto"
	});
	$('#search_btn').click(function() 			{
		get_goals();
  	$('#rperiod-list').prop('disabled', true);
  	$('#clear_btn').prop('disabled', false);
		return false;
	});
	$('#clear_btn').click(function() 			{
  	$('#rperiod-list').prop('disabled', false);
  	$('#clear_btn').prop('disabled', true);
		$('#accordion1').empty();
		$('#submit_btn').prop('disabled',true);
		return false;
	});
	$('#accordion1').on('click','button', function (evt) {
		var indx = $(this).data('btn');
		var txt = $(this).text();

		switch (txt)		{
			case 'Task 1':
				showTask1(indx);
				break;
			case 'Task 2':
				showTask2(indx);
				break;
			case 'Task 3':
				showTask3(indx);
				break;
			case 'Task 4':
				showTask4(indx);
				break;
			case 'Task 5':
				showTask5(indx);
				break;
			case 'Self Rating':
				showRating(indx);
				break;
		}
   	return false;
	});
	$('#taskupd_btn').click(function() 			{
		upd_task();
		return false;
	});
	$('#ratingupd_btn').click(function() 			{
		upd_rating();
		return false;
	});
	$('#submit_btn').click(function() 			{
    if (verify_ratings() == false)
      setError($('#errtext1'),"Please complete self-rating for all targets");
    else
  		submit_progress();
		return false;
	});
	$('#view_btn').click(function() 			{
		view_doc();
		return false;
	});
});

