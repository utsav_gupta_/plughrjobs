$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
		$(document).ajaxStart(function(){
				$("#ajaxaction").show();
		})
		$(document).ajaxStop(function(){
				$("#ajaxaction").hide();
		});
   $('#updpwd_btn').click(function()   {
      var opass = $("#currPassword").val();
      if (opass == "")	{
         $('#currPassword').css('border-color', 'red');
         setError($('#errtext1'),'Please input current password');
         return false;
      } else    {
         $('#currPassword').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (opass.length < 8)   {
         $('#currPassword').css('border-color', 'red');
         setError($('#errtext1'),'Password should be atleast 8 characters');
         return false;
      } else    {
         $('#currPassword1').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      var npass1 = $("#stuPassword1").val();
      if (npass1 == "")	{
         $('#stuPassword1').css('border-color', 'red');
         setError($('#errtext1'),'Please input new password');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass1.length < 8)   {
         $('#stuPassword1').css('border-color', 'red');
         setError($('#errtext1'),'Password should be atleast 8 characters');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      var npass2 = $("#stuPassword2").val();
      if (npass2 == "")	{
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Please input new password twice');
         return false;
      } else    {
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass2.length < 8)   {
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Password should be atleast 8 characters');
         return false;
      } else    {
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass1 != npass2)	{
         $('#stuPassword1').css('border-color', 'red');
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Passwords are not same');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }

      var joinObject = new Object();
      joinObject.cuser = $("#cuser").text();
      joinObject.opass = opass;
      joinObject.npass = npass1;
      $.ajax({
         type: "POST",
         url: '/503',
         data: joinObject,
         dataType: 'json',
         beforeSend:  function()   {
            startAjaxIcon();
         },
         success: function(robj) {
            if (robj.err == 0)  {
               setSuccess($('#errtext1'),robj.text);
               $("#currPassword").val("");
               $("#stuPassword1").val("");
               $("#stuPassword2").val("");
            } else
               setError($('#errtext1'),robj.text);
               stopAjaxIcon();
            },
         error: function(eobj) {
            setError($('#errtext1'),eobj.text);
            stopAjaxIcon();
         }
      });
      return false;
   });
});

