$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.preCompany').footable();
});

function defMesgTable()    {
	$('table.preCompany').data('footable').reset();
	$('table.preCompany thead').append('<tr>');
	$('table.preCompany thead tr').append('<th>User Name</th>');
	$('table.preCompany thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.preCompany thead tr').append('<th data-hide="phone,tablet">Mobile</th>');
	$('table.preCompany thead tr').append('<th data-hide="phone,tablet">Company</th>');
	$('table.preCompany thead tr').append('<th data-hide="phone,tablet">Verification ID</th>');
	$('table.preCompany thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.preCompany thead tr').append('</tr>');
	$('table.preCompany').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.preCompany').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.name != null)
				newRow += obj.name;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.user != null)
				newRow += obj.user;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mobile != null)
				newRow += obj.mobile;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.company != null)
				newRow += obj.company;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.verid != null)
				newRow += obj.verid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/105newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.preCompany').data('footable');

			$('table.preCompany tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.preCompany tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

