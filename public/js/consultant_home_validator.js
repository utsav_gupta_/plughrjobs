$(function () {
  $('table.received').footable();
  $('#send_btn').hide();
});

function SetPagination() {
  var totpages = $("#totmesgs").val();
  if (totpages == 0)
    totpages =1; 
  var options = {
    currentPage: 1,
    totalPages: totpages,
    size:'small',
    bootstrapMajorVersion:3,
    itemTexts: function (type, page, current) {
      switch (type) {
        case "first":
            return "First";
        case "prev":
            return "Previous";
        case "next":
            return "Next";
        case "last":
            return "Last";
        case "page":
            return page;
      }
    },
    onPageClicked: function(e,originalEvent,type,page)    {
      var currPage = $(e.currentTarget).bootstrapPaginator("getPages").current;
      if (currPage != page)   {
        GetNextPage(page);
      }
    }
  }
  $('#rmesgpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;
	mesgObject.sstr = $("#sstr").val();

	$.ajax ({
		type: "GET",
		url: '/inboxnewpage',
		data: mesgObject,
		dataType: 'json',
    beforeSend:  function()   {
      startAjaxIcon();
    },
		error: function(err) {
      setError($('#errtext1'),err.responseText);
      stopAjaxIcon();
		},
		success: function(mesgObj)	{
      $("#mesgdata").val(JSON.stringify(mesgObj));
      var rtable = $('table.received').data('footable');
      $('table.received tbody tr').each(function() {
        rtable.removeRow($(this));
      });
      $('table.received thead tr').each(function() {
        rtable.removeRow($(this));
      });
      defMesgTable();
      fillMesgData();
      stopAjaxIcon();
		}
	});
  return false;
}
function defMesgTable()    {
	$('table.received').data('footable').reset();
	$('table.received thead').append('<tr>');
	$('table.received thead tr').append('<th>From</th>');
	$('table.received thead tr').append('<th data-hide="phone">Email Id</th>');
	$('table.received thead tr').append('<th data-hide="phone">Subject</th>');
	$('table.received thead tr').append('<th data-hide="phone,tablet">When Received</th>');
	$('table.received thead tr').append('<th data-hide="phone">View</th>');
	$('table.received thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.received thead tr').append('</tr>');
  $('table.received').footable();
}
function fillMesgData()		{
	var mesgObj = JSON.parse($("#mesgdata").val());
  var glen = mesgObj.length;

  var rtable = $('table.received').data('footable');

  var rcount = 0;
  for(var i=0; i<glen; i++)   {
    var obj = mesgObj[i];
    if (!obj.readflag)
      var newRow = '<tr class="success">';
    else
      var newRow = '<tr>';
    newRow += '<td>';
    newRow += obj.fromname;
    newRow += '</td><td>';
    newRow += obj.from;
    newRow += '</td><td>';
    newRow += obj.subject;
    newRow += '</td><td>';
    newRow += obj.date;
    newRow += '</td><td>';
    newRow += '<a class="row-view" href="#"><span style="font-size:16px" class="glyphicon glyphicon-envelope" title="View Message"></span></a>';
    newRow += '</td><td id="rowIndex">';
    newRow += rcount+1;
    newRow += '</td></tr>';
    rtable.appendRow(newRow);
    rcount++;
  }
  rtable.redraw();
}
$(document).ready(function()	{
  defMesgTable();
  fillMesgData();
  SetPagination();
	$('#search_btn').click(function() {
		var searchstr = $("#searchstr").val();
		if (!searchstr)		{
			searchstr = "";
		}
		var searchObject = new Object();
		searchObject.user = $("#cuser").text();
		searchObject.searchstr = ".*"+searchstr;
		$.ajax ({
      type: "GET",
			url: '/srchstuinbox',
			data: searchObject,
			dataType: 'json',
      beforeSend:  function()   {
        startAjaxIcon();
      },
			success: function(o)	{
        if (!o.err)     {
          $("#mesgdata").val(JSON.stringify(o.data));
          $("#sstr").val(o.sstr);

          if (o.totpages > 0)
            $("#totmesgs").val(o.totpages);
          else
            $("#totmesgs").val(1);

          var rtable = $('table.received').data('footable');

          $('table.received tbody tr').each(function() {
            rtable.removeRow($(this));
          });

          $('table.received thead tr').each(function() {
            rtable.removeRow($(this));
          });
          defMesgTable();
          fillMesgData();
          SetPagination();
        } else    {
          setError($('#errtext1'),o.text);
        }
        stopAjaxIcon();
      },
			error: function(err) { 
        setError($('#errtext1'),err.text);
        stopAjaxIcon();
			}
    });
		return false;
  });
	$('#reply_btn').click(function() {
    var uniid = $("#uniid").val();
    var from = $("#fromid").val();
    var fromname = $("#from").val();
    var to = $("#toid").val();
    var toname = $("#to").val();
    var subject = $("#subject").val();
    var message = $("#mesgtext").val();
    var date = $("#mesgdate").val();
    ReplyMessage(uniid, from, fromname, to, toname, date, subject, message);
		return false;
	});
	$('#send_btn').click(function() {
    var uniid = $("#uniid").val();
    var from = $("#fromid").val();
    var fromname = $("#from").val();
    var to = $("#toid").val();
    var toname = $("#to").val();
    var subject = $("#subject").val();
    var message = $("#mesgtext").val();
    SaveMessage(uniid, from, fromname, to, toname, subject, message);
		return false;
	});
});

$(function () {
  $('table.received').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.received').data('footable');

    var row = $(this).parents('tr:first');
    $(this).parents('tr:first').removeClass('success');
    ShowViewModal(e, row[0]["rowIndex"]-1);
  });
});

function ShowViewModal(evtObj, rowIndex, rowData) {
  if (evtObj && evtObj.target) {
	  var mesgObj = JSON.parse($("#mesgdata").val());

    $('#uniid').val(mesgObj[rowIndex].uniid);
    $('#mesgdate').val(mesgObj[rowIndex].date);
    $('#fromid').val(mesgObj[rowIndex].from);
    $('#from').val(mesgObj[rowIndex].fromname);
    $('#toid').val(mesgObj[rowIndex].to);
    $('#to').val(mesgObj[rowIndex].toname);
    $('#subject').val(mesgObj[rowIndex].subject);
    $('#mesgtext').val(mesgObj[rowIndex].mesgtext);

    $('#from').attr("disabled", "disabled");
    $('#fromid').attr("disabled", "disabled");
    $('#to').attr("disabled", "disabled");
    $('#toid').attr("disabled", "disabled");
    $('#subject').attr("disabled", "disabled");
    $('#mesgtext').attr("disabled", "disabled");

    if (mesgObj[rowIndex].readflag == false)    {
	    var mesgObject = new Object();
	    mesgObject.user = $("#cuser").text();
	    mesgObject.uniid = mesgObj[rowIndex].uniid;
	    mesgObject.from = mesgObj[rowIndex].from;
	    mesgObject.to = mesgObj[rowIndex].to;
	    mesgObject.subject = mesgObj[rowIndex].subject;
	    mesgObject.mesgtext = mesgObj[rowIndex].mesgtext;
	    mesgObject.date = mesgObj[rowIndex].date;
	    mesgObject.type = "R";
	    $.ajax ({
		    type: "POST",
		    url: '/setreadtrue',
		    data: mesgObject,
		    dataType: 'json',
        beforeSend:  function()   {
          startAjaxIcon();
        },
	      error: function(err) {
          setError($('#errtext1'),'Unable to set read flag');
          $('#reply_btn').show();
          $('#send_btn').hide();
          $('#MesgModel').modal();
          stopAjaxIcon();
	      },
			  success : function(robj) { 
          if (robj.err == 0)    {
            $('#reply_btn').show();
            $('#send_btn').hide();
            $('#MesgModel').modal();
          } else  {
            $('#reply_btn').show();
            $('#send_btn').hide();
    				setError($('#errtext1'),robj.text);
          }
          stopAjaxIcon();
			  },
      });
    } else    {
      $('#reply_btn').show();
      $('#send_btn').hide();
      $('#MesgModel').modal();
      stopAjaxIcon();
    }
  }
}

function ReplyMessage(uniid, from, fromname, to, toname, date, subject, message)   {
  var sepr = "-------------------------------------------";
  var newln = "\r\n";
  var pcont = newln + newln + sepr + newln;
  pcont += "From: " + from + newln;
  pcont += "To: " + to + newln;
  pcont += "Date: " + date + newln;
  pcont += "Subject: " + subject + newln;
  pcont += newln + message + newln;

  $('#titletxt').text('Reply to Message');
  $('#from').val(toname);
  $('#fromid').val(to);
  $('#to').val(fromname);
  $('#toid').val(from);
  $('#subject').val("RE: " + subject);
  $('#mesgtext').val(pcont);

  $('#reply_btn').hide();
  $('#send_btn').show();
  $('#mesgtext').removeAttr("disabled");
}

function SaveMessage(uniid, from, fromname, to, toname, subject, mesgtext)    {
	var mesgObject = new Object();
	mesgObject.uniid = uniid;
	mesgObject.from = from;
	mesgObject.to = to;
	mesgObject.fromname = fromname;
	mesgObject.toname = toname;
	mesgObject.subject = subject;
	mesgObject.mesgtext = mesgtext;
	$.ajax ({
		type : "POST",
		url : '/messageunistaf',
		data : mesgObject,
		dataType: 'json',
    beforeSend:  function()   {
      startAjaxIcon();
    },
		error  : function(err) {
      setError($('#errtext1'),err.responseText);
      stopAjaxIcon();
		},
		success  : function()	{
      setSuccess($('#errtext1'),"Replied to sender");
      $('#MesgModel').modal('hide');
      stopAjaxIcon();
		}
	});
  return false;
}

