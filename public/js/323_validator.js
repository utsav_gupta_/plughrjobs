$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#validfrom').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#validto').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#appoffice-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true
	});
	$('#selholidays-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#htitle').val('');
	$('#validfrom').val('');
	$('#validto').val('');
	$('#appoffice-list').select2('val', null);
	$('#maxopholidays').val('');
	$('#selholidays-list').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_appoffice()    {
	var xObj = JSON.parse($('#appofficedata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#appoffice-list');
		}
	}
}
function def_selholidays()    {
	var xObj = JSON.parse($('#selholidaysdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.holidayname).appendTo('#selholidays-list');
		}
	}
}
function defMesgTable()    {
	$('table.holidaypolicy').data('footable').reset();
	$('table.holidaypolicy thead').append('<tr>');
	$('table.holidaypolicy thead tr').append('<th>Title</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Valid From</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Valid To</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Applicable to</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Max allowed optional holidays</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Select Holiday(s)</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.holidaypolicy thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.holidaypolicy thead tr').append('</tr>');
	$('table.holidaypolicy').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#appofficedata').val());
	var sel5Obj = JSON.parse($('#selholidaysdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.holidaypolicy').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.htitle != null)
				newRow += obj.htitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.validfrom != null)
				newRow += obj.validfrom;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.validto != null)
				newRow += obj.validto;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.appoffice.length;
			var dblen = sel3Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.appoffice[temp];
					if (inx == selx._id)	{
						newRow += selx.oname+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.maxopholidays != null)
				newRow += obj.maxopholidays;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.selholiday.length;
			var dblen = sel5Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel5Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.selholiday[temp];
					if (inx == selx._id)	{
						newRow += selx.holidayname+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/323newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.holidaypolicy').data('footable');

			$('table.holidaypolicy tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.holidaypolicy tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.holidaypolicy').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.holidaypolicy').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);

		$('#htitle').val(obj.htitle);
		$('#validfrom').val(obj.validfrom);
		$('#validto').val(obj.validto);
		$('#appoffice-list').select2('val', obj.appoffice);
		$('#maxopholidays').val(obj.maxopholidays);
		$('#selholidays-list').select2('val', obj.selholiday);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.htitle= obj.htitle;
			dataObj.validfrom= obj.validfrom;
			dataObj.validto= obj.validto;
			dataObj.appoffice= obj.appoffice;
			dataObj.maxopholidays= obj.maxopholidays;
			dataObj.selholidays= obj.selholiday;

			$.ajax({
				type: 'DELETE',
				url: '/323',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.holidaypolicy').data('footable');

						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'holidaypolicy data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_htitle = $('#htitle').val();
	if (s_htitle == '')	{
		$('#htitle').css('border-color', 'red');
		setError($('#errtext1'),'Please input a Title');
		$('#htitle').focus();
		return false;
	} else    {
		$('#htitle').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_validfrom = $('#validfrom').val();
	if (s_validfrom == '')	{
		$('#validfrom').css('border-color', 'red');
		setError($('#errtext1'),'Please input Valid From');
		$('#validfrom').focus();
		return false;
	} else    {
		$('#validfrom').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_validto = $('#validto').val();
	if (s_validto == '')	{
		$('#validto').css('border-color', 'red');
		setError($('#errtext1'),'Please input Valid To');
		$('#validto').focus();
		return false;
	} else    {
		$('#validto').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_appoffice = $('#appoffice-list').val();
	if (s_appoffice == null)	{
		$('#appoffice-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Applicable to');
		$('#appoffice-list').focus();
		return false;
	} else    {
		$('#appoffice-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_maxopholidays = $('#maxopholidays').val();
	if (s_maxopholidays == '')	{
		$('#maxopholidays').css('border-color', 'red');
		setError($('#errtext1'),'Please input Max allowed optional holidays');
		$('#maxopholidays').focus();
		return false;
	} else    {
		$('#maxopholidays').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_selholidays = $('#selholidays-list').val();
	if (s_selholidays == null)	{
		$('#selholidays-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Holiday(s)');
		$('#selholidays-list').focus();
		return false;
	} else    {
		$('#selholidays-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();

	formdataObj.htitle= s_htitle;
	formdataObj.validfrom= s_validfrom;
	formdataObj.validto= s_validto;
	formdataObj.appoffice= s_appoffice;
	formdataObj.maxopholidays= s_maxopholidays;
	formdataObj.selholidays= s_selholidays;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/323add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.holidaypolicy').data('footable');

				$('table.holidaypolicy tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.holidaypolicy tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'holidaypolicy data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/323upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.holidaypolicy').data('footable');

				$('table.holidaypolicy tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.holidaypolicy tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'holidaypolicy data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_appoffice();
	def_selholidays();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

