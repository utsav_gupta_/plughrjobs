function startAjaxIcon()   {
  $("#enteem_nav").removeClass("navbar-fixed-top");
  $("body").addClass("loading");
}
function stopAjaxIcon()   {
  $("#enteem_nav").addClass("navbar-fixed-top");
  $("body").removeClass("loading"); 
}

// Homepage
$('#homepage').click(function() {
  startAjaxIcon();
});
$('#whyenteem').click(function() {
  startAjaxIcon();
});
$('#pricing').click(function() {
  startAjaxIcon();
});
$('#howenteem').click(function() {
  startAjaxIcon();
});
$('#stuverify').click(function() {
  startAjaxIcon();
});
$('#widgetdemo').click(function() {
  startAjaxIcon();
});
$('#forgotpwd').click(function() {
  $('#loginModal').modal('hide');
  startAjaxIcon();
});
$('#logout_btn').click(function() {
  startAjaxIcon();
});

// Students
$('#101').click(function() {
  startAjaxIcon();
});
$('#studentsent').click(function() {
  startAjaxIcon();
});
$('#studentactv').click(function() {
  startAjaxIcon();
});
$('#stu-uni-list').click(function() {
  startAjaxIcon();
});
$('#stu-upd-profile').click(function() {
  startAjaxIcon();
});
$('#stu-show-docs').click(function() {
  startAjaxIcon();
});
$('#stu-upd-pwd').click(function() {
  startAjaxIcon();
});

// University Staff
$('#ustfinbox').click(function() {
  startAjaxIcon();
});
$('#ustfsent').click(function() {
  startAjaxIcon();
});
$('#ustf-eng-list').click(function() {
  startAjaxIcon();
});
$('#ustf-stu-list').click(function() {
  startAjaxIcon();
});
$('#ustf-stu-groups').click(function() {
  startAjaxIcon();
});
$('#ustf-upd-profile').click(function() {
  startAjaxIcon();
});
$('#ustf-upd-pwd').click(function() {
  startAjaxIcon();
});

// Uni admin
$('#uadminbox').click(function() {
  startAjaxIcon();
});
$('#uadmsent').click(function() {
  startAjaxIcon();
});
$('#uadm-abt-uni').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-course').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-links').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-faqs').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-events').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-users').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-eusers').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-agents').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-locations').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-widget').click(function() {
  startAjaxIcon();
});
$('#uadm-uni-wreport').click(function() {
  startAjaxIcon();
});
$('#uadm-upd-pwd').click(function() {
  startAjaxIcon();
});
$('#showunilic').click(function() {
  startAjaxIcon();
});

// Site admin
$('#sadminbox').click(function() {
  startAjaxIcon();
});
$('#sadmsent').click(function() {
  startAjaxIcon();
});
$('#sadm-new-list').click(function() {
  startAjaxIcon();
});
$('#sadm-uni-list').click(function() {
  startAjaxIcon();
});
$('#sadm-uni-license').click(function() {
  startAjaxIcon();
});

