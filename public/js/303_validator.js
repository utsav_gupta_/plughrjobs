$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selfile').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').text(fileName);
  });
});

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}

function showformdata()		{
  // Show current data
  var cuser = JSON.parse($("#formdata").val());
	if (cuser != null)    {
    $("#company").val(cuser.companyname);
    $("#industry").val(cuser.industry);
    $("#addr").val(cuser.addr);
    $("#city").val(cuser.city);
    $("#pincode").val(cuser.pincode);
    $("#linkedin").val(cuser.linkedin);
    $("#about").val(cuser.about);
    $("#values").val(cuser.values);
    var randm = (new Date()).toString();
    var imgsrc = "https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/workhalf/" + cuser.clogo  + "?" + new Date().getTime();
    $("#userpict").attr('src',imgsrc);
	}
}

function GetValidInputs()			{
  var scompany = $("#company").val();
  if (scompany == "")	{
     $('#company').css('border-color', 'red');
     setError($('#errtext1'),'Please input company name');
     return false;
  } else    {
     $('#company').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var sindustry = $("#industry").val();
  if (sindustry == "")	{
     $('#industry').css('border-color', 'red');
     setError($('#errtext1'),'Please select an industry');
     return false;
  } else    {
     $('#industry').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var saddr = $("#addr").val();
  if (saddr == "")	{
     $('#addr').css('border-color', 'red');
     setError($('#errtext1'),'Please input street address');
     return false;
  } else    {
     $('#addr').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var scity = $("#city").val();
  if (scity == "")	{
     $('#city').css('border-color', 'red');
     setError($('#errtext1'),'Please input city');
     return false;
  } else    {
     $('#city').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var spincode = $("#pincode").val();
  if (spincode == "")	{
     $('#pincode').css('border-color', 'red');
     setError($('#errtext1'),'Please input pincode');
     return false;
  } else    {
     $('#pincode').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  if (spincode.length != 6)   {
     $('#pincode').css('border-color', 'red');
     setError($('#errtext1'),'Please input valid pincode');
     return false;
  } else    {
     $('#pincode').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var slinkedin = $("#linkedin").val();
  if (slinkedin == "")	{
     $('#linkedin').css('border-color', 'red');
     setError($('#errtext1'),'Please input company linkedin page');
     return false;
  } else    {
     $('#linkedin').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var sabout = $("#about").val();
  if (sabout == "")	{
     $('#about').css('border-color', 'red');
     setError($('#errtext1'),'Please write briefly about your company');
     return false;
  } else    {
     $('#about').css('border-color', 'default');
     clearError($('#errtext1'));
  }
  var svalues = $("#values").val();

	var sfile = $("#selfile").val();
	var s_filename = '';
  if (sfile != "")	{
		s_filename = $('#filename').val();
		// Check filetype and filesize
		var filename = $("#selfile").val();
		var extensionAllowed=[".jpg",".jpeg",".gif",".png"];
		var i = filename.lastIndexOf('.');
		var file_extension= (i < 0) ? '' : filename.substr(i);
		var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

		if(rfound == -1)		{
		  $('#selfile').css('font-color', 'red');
		  setError($('#errtext1'),' Only Image file formats (jpg or jpeg or gif or png) are allowed');
			$("#selfile").text("select new image file");
		  return false;
		} else    {
		  $('#selfile').css('font-color', 'default');
		  clearError($('#errtext1'));
		}
		var fsize = document.getElementById('docfile').files[0].size;
		if ((fsize / 1024) > 2500)		{
		  $('#selfile').css('font-color', 'red');
		  setError($('#errtext1'),'Image file should be less than 2MB');
			$("#selfile").text("select new document");
		  return false;
		} else    {
		  $('#selfile').css('font-color', 'default');
		  clearError($('#errtext1'));
		}
	}

  var formdataObj = new Object();
	formdataObj.userid = $('#cuserid').text();
  formdataObj.company = scompany;
  formdataObj.industry = sindustry;
  formdataObj.addr = saddr;
  formdataObj.city = scity;
  formdataObj.pincode = spincode;
  formdataObj.linkedin = slinkedin;
  formdataObj.about = sabout;
  formdataObj.values = svalues;

  return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
  $('#submit_btn').click();
  return true;
}

$(document).ready(function()	{
    var options = {
      beforeSend: function()  {
        clearError($('#errtext1'));
        startAjaxIcon();
      },
		  success: function(resp) {
			  if (!resp.err)   {
				  $('#formdata').val(JSON.stringify(resp.data));
				  if (resp.logofile)	{
					  var randm = (new Date()).toString();
					  var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/workhalf/' + resp.logofile  + "?" + new Date().getTime();
					  $("#userpict").attr('src',imgsrc);
				  }
		      $('#selfile').text('click here to change logo');
				  setSuccess($('#errtext1'),'Company info updated');
			  } else  {
				  setError($('#errtext1'),resp.text);
			  }
			  stopAjaxIcon();
		  },
    	complete: function(response)    {
        stopAjaxIcon();
	    },
	    error: function(response)   {
        setError($('#errtext1'),response.text);
        stopAjaxIcon();
	    }
    };
    $("#adddocs-form").ajaxForm(options);

	showformdata();
	$('#updcpr_btn').click(function() 			{
		data_add();
	});
});

