function setError(el, txt)  {
  el.text(txt);
	el.css("color","#DC6F57");
	el.css("font-weight","bold");
	el.css("visibility","visible");
}
function clearError(el)  {
  el.css('border-color', 'default');
  el.text('');
	el.css("visibility","hidden");
}
function setSuccess(el, txt)  {
  el.text(txt);
	el.css("color","green");
	el.css("font-weight","bold");
	el.css("visibility","visible");
}

