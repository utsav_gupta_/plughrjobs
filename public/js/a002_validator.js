$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}
function clearForm()		{
	$('#dbid').val('');
	$('#uname').val('');
	$('#email').val('');
	$('#pass1').val('');
	$('#pass2').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>Admin Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/a002newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.users').data('footable');

			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.users').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.users').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#uname').val(obj.username);
		$('#email').val(obj.useremailid);
		$('#pass1').val('');
		$('#pass2').val('');

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.uname= obj.username;
			dataObj.email= obj.useremailid;
			dataObj.pass= obj.password;

			$.ajax({
				type: 'DELETE',
				url: '/a002',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.users').data('footable');

						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'users data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs(mode)		{
	var s_email = $('#email').val();
	if (s_email == '')	{
		$('#email').css('border-color', 'red');
		setError($('#errtext1'),'Please input User email id');
		$('#email').focus();
		return false;
	} else    {
		$('#email').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var lastAtPos = s_email.lastIndexOf('@');
	var lastDotPos = s_email.lastIndexOf('.');
	var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_email.indexOf('@@') == -1 && lastDotPos > 2 && (s_email.length - lastDotPos) > 2);
	if (!result)	{
		setError($('#errtext1'),'Please input valid email id');
		$('#email').focus();
		return false;
	} else    {
		$('#email').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_uname = $('#uname').val();
	if (s_uname == '')	{
		$('#uname').css('border-color', 'red');
		setError($('#errtext1'),'Please input User Name');
		$('#uname').focus();
		return false;
	} else    {
		$('#uname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pass1 = $('#pass1').val();
	if (mode == 'add')		{
		if (s_pass1 == '')	{
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	if (s_pass1 != '')	{
		if (s_pass1.length < 8)   {
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		var s_pass2 = $('#pass2').val();
		if (s_pass2 == '')	{
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass2.length < 8)   {
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass1 != s_pass2)	{
			$('#pass1').css('border-color', 'red');
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Passwords are not same');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}

	var formdataObj = new  Object();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.uname= s_uname;
	formdataObj.email= s_email;
	formdataObj.pass= s_pass1;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs('add');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/a002add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);

				fillUserData();
				SetPagination();
				clearForm();				
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs('edit');
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/a002upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'users data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;

		def_role();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
      beforeSend:  function()   {
        $("body").addClass("loading");
      },
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

