/**
 * Common stuff used on all FooTable demos
 */

$(function () {
  $("body").removeClass("loading"); 
});

$(document).ready(function()	{
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/logout',
      beforeSend:  function()   {
        $("body").addClass("loading");
      },
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

