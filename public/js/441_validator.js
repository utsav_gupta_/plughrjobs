$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#edate').datepicker({ dateFormat: 'dd-mm-yy'});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}
function clearForm()		{
	$('#dbid').val('');
	$('#obj').val('');
	$('#meets').val('');
	$('#sdate').val('');
	$('#edate').val('');
	clearError($('#errtext1'));

	$('#add_btn').prop('disabled',false);
	$('#upd_btn').prop('disabled',true);
	$('#cancel_btn').prop('disabled',true);
}

function disableForm1()		{
	$('#usr-list').prop('disabled',true);
	$('#search_btn').prop('disabled',true);
	$('#clear_btn').prop('disabled',false);
}
function enableForm1()		{
	$('#usr-list').prop('disabled',false);
	$('#search_btn').prop('disabled',false);
	$('#clear_btn').prop('disabled',false);
}
function disableForm2()		{
	$('#obj').prop('disabled',true);
	$('#meets').prop('disabled',true);
	$('#sdate').prop('disabled',true);
	$('#edate').prop('disabled',true);

	$('#add_btn').prop('disabled',true);
	$('#upd_btn').prop('disabled',true);
	$('#cancel_btn').prop('disabled',true);
}
function enableForm2()		{
	$('#obj').prop('disabled',false);
	$('#meets').prop('disabled',false);
	$('#sdate').prop('disabled',false);
	$('#edate').prop('disabled',false);

	$('#add_btn').prop('disabled',false);
	$('#upd_btn').prop('disabled',true);
	$('#cancel_btn').prop('disabled',true);
}

function clearTable()		{
	var rtable = $('table.usergoals').data('footable');

	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#usr-list');
		}
	}
}

function def_sgoal()    {
	var xObj = JSON.parse($('#sgoaldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.ugoalid).text(obj.title).appendTo('#sgoal-list');
		}
	}
}

function def_ugoalid()    {
	var xObj = JSON.parse($('#ugoaliddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#ugoalid-list');
		}
	}
}

function defMesgTable()    {
	$('table.usergoals').data('footable').reset();
	$('table.usergoals thead').append('<tr>');
	$('table.usergoals thead tr').append('<th>Objective</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Expected Performance</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">End Date</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Member Comments</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Manager Comments</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Comment</th>');
	$('table.usergoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.usergoals thead tr').append('</tr>');
	$('table.usergoals').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	var sel2Obj = JSON.parse($('#usrdata').val());

	if (mesgObj.length > 0)    {
		if (mesgObj[0].piptask)
			var glen = mesgObj[0].piptask.length;
		else
			var glen = 0;

		var rtable = $('table.usergoals').data('footable');
		var rcount = 0;

		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[0].piptask[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.obj != null)
				newRow += obj.obj;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.sdate != null)
				newRow += obj.sdate;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.edate != null)
				newRow += obj.edate;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.scomm != null)
				newRow += obj.scomm;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mcomm != null)
				newRow += obj.mcomm;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-comment" href="#"><span id = "c'+i+'" class="glyphicon glyphicon-comment" title="Comment"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/440newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.usergoals').data('footable');

			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.usergoals').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.usergoals').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
	$('table.usergoals').footable().on('click', '.row-comment', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		showModal(rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var mesgObj = JSON.parse($('#formdata').val());
		if (mesgObj.length != 0)    {
			var glen = mesgObj[0].piptask.length;

			var rno = parseInt(rowIndex);
			var obj = mesgObj[0].piptask[rno];
			$('#editrow').val(rno-1);

			$('#dbid').val(mesgObj[0]._id);
			$('#obj').val(obj.obj);
			$('#meets').val(obj.meets);
			$('#sdate').val(obj.sdate);
			$('#edate').val(obj.edate);

			document.getElementById('add_btn').disabled = true;
			document.getElementById('upd_btn').disabled = false;
			document.getElementById('cancel_btn').disabled = false;
		}
	}
}

function showModal(indx)	{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		var glen = mesgObj[0].piptask.length;

		var rno = parseInt(indx);
		var obj = mesgObj[0].piptask[rno];
		$('#modal_indx1').val(rno);
		$('#modal_dbid1').val(mesgObj[0]._id);
		$('#mcomm').val(obj.mcomm);
	  $('#CommentModal').modal();
  }
  return false;
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.mgr = $('#cuser').text();

			var rno = parseInt(rowIndex);
			var obj = fdata[0].piptask[rno];

			dataObj.dbid = fdata[0]._id;
			dataObj.usr = fdata[0].userid;
			dataObj.obj = obj.obj;
			dataObj.meets = obj.meets;
			dataObj.sdate = obj.sdate;
			dataObj.edate = obj.edate;

			$.ajax({
				type: 'DELETE',
				url: '/440',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.usergoals').data('footable');

						$('table.usergoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.usergoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'PIP data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_usrobj = JSON.parse($('#usr-list').val());
	var s_usr = s_usrobj.userid;
	var s_role = s_usrobj.roleid;
	var s_mgr = $('#cuserid').text();

	var s_obj = $('#obj').val();
	if (s_obj == null)	{
		$('#obj').css('border-color', 'red');
		setError($('#errtext1'),'Please input an Objective');
		$('#obj').focus();
		return false;
	} else    {
		$('#obj').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_meets = $('#meets').val();
	if (s_meets == null)	{
		$('#meets').css('border-color', 'red');
		setError($('#errtext1'),'Please input criteria for achieving this objective');
		$('#meets').focus();
		return false;
	} else    {
		$('#meets').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_sdate = $('#sdate').val();
	if (s_sdate == null)	{
		$('#sdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input start date');
		$('#sdate').focus();
		return false;
	} else    {
		$('#sdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_edate = $('#edate').val();
	if (s_edate == null)	{
		$('#edate').css('border-color', 'red');
		setError($('#errtext1'),'Please input completion date');
		$('#sdate').focus();
		return false;
	} else    {
		$('#edate').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.mgr = s_mgr;
	formdataObj.dbid= $('#dbid').val();
	formdataObj.usr = s_usr
	formdataObj.obj = s_obj;
	formdataObj.meets= s_meets;
	formdataObj.sdate= s_sdate;
	formdataObj.edate= s_edate;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/440add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'PIP data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	formdataObj.arrIndex = $('#editrow').val();

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length == 0)    
		return false;
	else		{
		var obj = mesgObj[0].piptask[$('#editrow').val()];
		formdataObj.old_obj = obj.obj;
		formdataObj.old_meets= obj.meets;
		formdataObj.old_sdate= obj.sdate;
		formdataObj.old_edate= obj.edate;
	}

	$.ajax({
		type: 'POST',
		url: '/440upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'PIP data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function GetSearchInputs()		{
	var s_usrobj = JSON.parse($('#usr-list').val());
	var s_usr = s_usrobj.userid.toString();
	var s_role = s_usrobj.roleid;
	if (s_usr == null)	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select team member');
		$('#usr-list').focus();
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.usr = s_usr;
	formdataObj.roleid = s_role;
	return formdataObj;
}

function get_goals()		{
	var formdataObj = GetSearchInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/240search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));
				if (resp.data0.length > 0)    {
					fillUserData();
					SetPagination();
					disableForm1();
					enableForm2();
					setSuccess($('#errtext1'),'PIP data for selected team member listed below');
				} else	{
					setError($('#errtext1'),'PIP not defined for selected team member');
				}
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}


function upd_comment()				{
	var s_dbid = $('#modal_dbid1').val();
	var s_indx = $('#modal_indx1').val();
	var s_comment = $('#mcomm').val();

	var commObj = new  Object();
	commObj.coid = $('#ccoid').text();
	commObj.dbid = s_dbid;
	commObj.mcomm = s_comment;

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length == 0)    
		return false;
	else		{
		var obj = mesgObj[0].piptask[s_indx-1];
		commObj.usr = mesgObj[0].userid;
		commObj.old_obj = obj.obj;
		commObj.old_meets= obj.meets;
		commObj.old_sdate= obj.sdate;
		commObj.old_edate= obj.edate;
	}

	//alert(JSON.stringify(commObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/240mgr',
		data: commObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Manager comment updated');
				$('#CommentModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

$(document).ready(function()	{
	def_usr();
	defMesgTable();

	$('#search_btn').click(function() 		{
		get_goals();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		disableForm2();
		enableForm1();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#commentupd_btn').click(function() 			{
		upd_comment();
		return false;
	});
});

