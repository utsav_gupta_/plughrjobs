$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}
$(document).ready(function()	{
	$('#signin_btn').click(function() {
    var suserid = $("#loginUserId").val();
    if (suserid == "")   {
      $('#loginUserId').css('border-color', 'red');
      setError($('#errtext3'),'Please input valid user id');
      return false;
    } else    {
      $('#loginUserId').css('border-color', 'default');
      clearError($('#errtext3'));
    }

		var spwd = $("#loginPassword").val();
    if (!spwd)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Please input password');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }
    if (spwd.length < 8)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var loginObject = new Object();
		loginObject.userid = suserid;
		loginObject.pwd = spwd;
		//alert(JSON.stringify(loginObject));

		$.ajax({
		  url: '/adminlogin',
		  data: loginObject,
		  dataType: 'json',
      beforeSend:  function()   {
        startAjaxIcon();
      },
      success : function(robj) { 
        //alert(JSON.stringify(robj));
        if (robj.err == 0)    {
	        window.location.href = robj.redirect;
        } else    {
		      setError($('#errtext3'),robj.text);
          stopAjaxIcon();
        }
			},
			error: function(eobj) {
				setError($('#errtext3'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});

