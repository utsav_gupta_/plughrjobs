$(function () {
  $("body").removeClass("loading"); 
});
$(document).ready(function()	{
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
      beforeSend:  function()   {
        $("body").addClass("loading");
      },
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

