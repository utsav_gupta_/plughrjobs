$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.clist').footable();
});

function defclistTable()    {
	$('table.clist').data('footable').reset();
	$('table.clist thead').append('<tr>');
	$('table.clist thead tr').append('<th>Client Name</th>');
	$('table.clist thead tr').append('<th data-hide="phone,tablet">Client ID</th>');
	$('table.clist thead tr').append('<th data-hide="phone,tablet">Creation date</th>');
	$('table.clist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.clist thead tr').append('</tr>');
	$('table.clist').footable();
}
function fillclistData()		{
	var clients = JSON.parse($('#formdata').val());
	var clen = clients.length;
	var rtable = $('table.clist').data('footable');
	$('table.clist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.clist tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	var rcount = 0;
	for(var i=0; i<clen; i++)   {
		var newRow = '<tr>';

		newRow += '<td>';
		newRow += clients[i].companyname;
		newRow += '</td>';

		newRow += '<td>';
		newRow += clients[i].companyid;
		newRow += '</td>';

		var ipos = clients[i].creationdate.indexOf('T');
		newRow += '<td>';
		newRow += clients[i].creationdate.substr(0,ipos);
		newRow += '</td>';

		newRow += '<td id="rowIndex">';
		newRow += rcount+1;
		newRow += '</td></tr>';
		rtable.appendRow(newRow);
		rcount++;
	}
	rtable.redraw();
}

$(document).ready(function()	{
	defclistTable();
	fillclistData();
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
});

