$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#sdt').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#edt').datepicker({ dateFormat: 'dd-mm-yy'});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#usr-list').prop('selectedIndex', 0);
	$('#rsn').val('');
	$('#sdt').val('');
	$('#edt').val('');
	$('#sts-list').prop('selectedIndex', 0);
	$('#cmts').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#usr-list');
		}
	}
}
function defMesgTable()    {
	$('table.userpips').data('footable').reset();
	$('table.userpips thead').append('<tr>');
	$('table.userpips thead tr').append('<th>Person</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Reason for PIP</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">End Date</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Comments</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.userpips thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.userpips thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.userpips thead tr').append('</tr>');
	$('table.userpips').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#usrdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.userpips').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.userid == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.reason != null)
				newRow += obj.reason;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.sdate != null)
				newRow += obj.sdate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.edate != null)
				newRow += obj.edate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'Open';
					break;
				case '2':
					newRow += 'Closed';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.comments != null)
				newRow += obj.comments;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/340newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.userpips').data('footable');

			$('table.userpips tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.userpips tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.userpips').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.userpips').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#usr-list').val(obj.userid);
		$('#rsn').val(obj.reason);
		$('#sdt').val(obj.sdate);
		$('#edt').val(obj.edate);
		$('#sts-list').val(obj.status);
		$('#cmts').val(obj.comments);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.usr= obj.userid;
			dataObj.rsn= obj.reason;
			dataObj.sdt= obj.sdate;
			dataObj.edt= obj.edate;
			dataObj.sts= obj.status;
			dataObj.cmts= obj.comments;

			$.ajax({
				type: 'DELETE',
				url: '/340',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.userpips').data('footable');

						$('table.userpips tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.userpips tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'userpips data deleted');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_usr = $('#usr-list').val();
	if (s_usr == null)	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Person');
		$('#usr-list').focus();
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_rsn = $('#rsn').val();
	if (s_rsn == '')	{
		$('#rsn').css('border-color', 'red');
		setError($('#errtext1'),'Please input Reason for PIP');
		$('#rsn').focus();
		return false;
	} else    {
		$('#rsn').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sdt = $('#sdt').val();
	if (s_sdt == '')	{
		$('#sdt').css('border-color', 'red');
		setError($('#errtext1'),'Please input Start Date');
		$('#sdt').focus();
		return false;
	} else    {
		$('#sdt').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_edt = $('#edt').val();
	if (s_edt == '')	{
		$('#edt').css('border-color', 'red');
		setError($('#errtext1'),'Please input End Date');
		$('#edt').focus();
		return false;
	} else    {
		$('#edt').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sts = $('#sts-list').val();
	if (s_sts == null)	{
		$('#sts-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Status');
		$('#sts-list').focus();
		return false;
	} else    {
		$('#sts-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_cmts = $('#cmts').val();
	if (s_cmts == '')	{
		$('#cmts').css('border-color', 'red');
		setError($('#errtext1'),'Please input HR Comments');
		$('#cmts').focus();
		return false;
	} else    {
		$('#cmts').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.usr= s_usr;
	formdataObj.rsn= s_rsn;
	formdataObj.sdt= s_sdt;
	formdataObj.edt= s_edt;
	formdataObj.sts= s_sts;
	formdataObj.cmts= s_cmts;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/340add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.userpips').data('footable');

				$('table.userpips tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.userpips tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'userpips data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/340upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.userpips').data('footable');

				$('table.userpips tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.userpips tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'userpips data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_usr();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

