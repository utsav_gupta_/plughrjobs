/*
	// Status values
	1 = "Submitted"
	2 = "Accepted"
	3 = "Rejected"
	4 = "Withdrawn"
	5 = "closed"
*/

$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.eacts').footable();
	$('table.exits').footable();
});

function getEact(eactid)		{
	var xObj = JSON.parse($('#eactdata').val());
	var rval = '';
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var fnd = eactid.indexOf(obj._id);
			if (fnd != -1)
				rval += obj.title + ', ';
		}
	}
	return rval;
}


function getUser(usrid)		{
	var xObj = JSON.parse($('#repldata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == usrid)
				return obj.username;
		}
	}
	return "";
}

function defMesgTable()    {
	$('table.exits').data('footable').reset();
	$('table.exits thead').append('<tr>');
	$('table.exits thead tr').append('<th>Employee</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Reason</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Last working day</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Take-over person</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Exit Activites</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Exit Actions</th>');
	$('table.exits thead tr').append('<th data-hide="phone,tablet">Finalise</th>');
	$('table.exits thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.exits thead tr').append('</tr>');
	$('table.exits').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#repldata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.exits').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.userid != null)
				newRow += getUser(obj.userid);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.reason)     {
				case '1':
					newRow += 'Better Career Prospects';
					break;
				case '2':
					newRow += 'Better Role';
					break;
				case '3':
					newRow += 'Better Salary';
					break;
				case '4':
					newRow += 'Work-Life Balance';
					break;
				case '5':
					newRow += 'Dream Job Offer';
					break;
				case '6':
					newRow += 'Family circumstances';
					break;
				case '7':
					newRow += 'Personal Reasons';
					break;
				case '8':
					newRow += 'Getting Married';
					break;
				case '9':
					newRow += 'Higher Education';
					break;
				case '10':
					newRow += 'Health Reasons';
					break;
				case '11':
					newRow += 'Full-time position';
					break;
				case '12':
					newRow += 'Maternity related';
					break;
				case '13':
					newRow += 'Not returning from Maternity leave';
					break;
				case '14':
					newRow += 'Other Reasons';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.ldate != null)
				newRow += obj.ldate;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel3Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				if (obj.repluserid == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.eact != null)
				newRow += getEact(obj.eact);
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'Submitted';
					break;
				case '2':
					newRow += 'Approved';
					break;
				case '3':
					newRow += 'Rejected';
					break;
				case '4':
					newRow += 'Withdrawn';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span id = "v'+i+'" class="glyphicon glyphicon-eye-open" title="View"></span></a>';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/442newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.exits').data('footable');

			$('table.exits tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.exits tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.exits').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.exits').footable().on('click', '.row-view', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		viewdetails(e, rowindex);
	});
});

function viewdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		fillEactsData();
	  $('#EActModal').modal();
	}
}

function defEactsTable()    {
	$('table.eacts').data('footable').reset();
	$('table.eacts thead').append('<tr>');
	$('table.eacts thead tr').append('<th>Title</th>');
	$('table.eacts thead tr').append('<th>Description</th>');
	$('table.eacts thead tr').append('<th>Approval</th>');
	$('table.eacts thead tr').append('</tr>');
	$('table.eacts').footable();
}

function fillEactsData()		{
	var mesgObj = JSON.parse($('#formdata').val());

	// DELETE & RE-CREATE Table 
	var rtable = $('table.eacts').data('footable');
	$('table.eacts tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.eacts tbody tr').each(function() {
		rtable.removeRow($(this));
	});

	var glen = mesgObj.length;
	if (glen > 0)		{
		var obj = mesgObj[0];
		var ptxt1 = getUser(obj.userid)
		var ptxt2 = "Last working day is "+ obj.ldate
		$('#ptext1').text(ptxt1);
		$('#ptext2').text(ptxt2);

		var elen = obj.eact.length;
		for(var i=0; i<elen; i++)   {
			var eID = obj.eact[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (eID)	{
				newRow += getEactData(eID);
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			for(var x=0;x<40;x++)
				newRow += '&nbsp;';
			newRow += '</td>';

			newRow += '</tr>';
			rtable.appendRow(newRow);
		}
	}
	rtable.redraw();
}

function getEactData(eactid)		{
	var xObj = JSON.parse($('#eactdata').val());
	var rval = '';
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == eactid)
				rval += obj.title + '</td><td>' + obj.desc;
		}
	}
	return rval;
}

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var mesg = "Do you really want to Finalise?";
		alertify.set({ labels: {
			ok     : "Proceed",
			cancel : "Not Now"
			} 
		});
		alertify.set({ buttonFocus: "cancel" });
		alertify.set({ buttonReverse: true });
		alertify.confirm(mesg, function (e) {
			if (e) {
				var dataObj = JSON.parse($('#formdata').val());
				var rno = parseInt(rowIndex);
				var obj = dataObj[rno];

				var formdataObj = new  Object();
				formdataObj.coid = $('#ccoid').text();
				formdataObj.userid = $('#cuserid').text();
				formdataObj.dbid = obj._id;
				formdataObj.ruser = obj.userid;

				alert(JSON.stringify(formdataObj));
				//return false;

				$.ajax({
					type: 'POST',
					url: '/241upd',
					data: formdataObj,
					dataType: 'json',
					beforeSend:  function()   {
						startAjaxIcon();
					},
					success: function(resp) {
						if (!resp.err)   {
							$('#formdata').val(JSON.stringify(resp.data));
							$('#totpages').val(JSON.stringify(resp.totpages));

							// DELETE & RE-CREATE Table 
							var rtable = $('table.exits').data('footable');

							$('table.exits tbody tr').each(function() {
								rtable.removeRow($(this));
							});
							$('table.exits tbody tr').each(function() {
								rtable.removeRow($(this));
							});
							$('#editrow').val(-1);
							//defMesgTable();
							fillUserData();
							SetPagination();
							setSuccess($('#errtext1'),'Exit Finalised');
						} else  {
							setError($('#errtext1'),o.text);
						}
						stopAjaxIcon();
					},
					error: function(err) {
						setError($('#errtext1'),err.responseText);
						stopAjaxIcon();
					}
				});
				return false;
			}
		});
	}
}

function data_upd()		{
	var dataObj = JSON.parse($('#formdata').val());
	var rno = parseInt(rowIndex,10)+1;
	var obj = dataObj[rno-1];

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid = obj._id;
	formdataObj.ruser = obj.userid;

	alert(JSON.stringify(formdataObj));
	return false;

	$.ajax({
		type: 'POST',
		url: '/241upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.exits').data('footable');

				$('table.exits tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.exits tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'exits data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	defEactsTable();
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#print_btn').click(function() 			{
		window.print();
		return false;
	});
});

