$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#dob').datepicker({ dateFormat: 'yy-mm-dd'});
	$('#jdate').datepicker({ dateFormat: 'yy-mm-dd'});
	$('#probdate').datepicker({ dateFormat: 'yy-mm-dd'});
	$('table.skills').footable();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}
function clearForm()		{
	$('#dbid').val('');
	$('#uname').val('');
	$('#email').val('');
	$('#pass1').val('');
	$('#pass2').val('');
	$('#dob').val('');
	$('#gender-list').prop('selectedIndex', 0);
	$('#mgr-list').prop('selectedIndex', 0);
	$('#dep-list').prop('selectedIndex', 0);
	$('#role-list').prop('selectedIndex', 0);
	$('#loc-list').prop('selectedIndex', 0);
	$('#desg').val('');
	$('#jdate').val('');
	$('#probdate').val('');
	$('#sal').val('');
	$('#etype-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_emp()    {
	$('#mgr-list').empty();
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#mgr-list');
		}
	}
}

function def_dep()    {
	var xObj = JSON.parse($('#depdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dep-list');
		}
	}
}

function def_role(roleid)    {
	var xObj = JSON.parse($('#roledata').val());
	var seldep = $('#dep-list').val();

	$('#role-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == seldep)		{
				var optObj = new Object;
				optObj.roleid = obj._id;
				optObj.depid = obj.depid;
				optObj.rtype = obj.roletype;
				optObj.skills = obj.mustskills;
				if (roleid && roleid == optObj.roleid)	{
					$('<option selected>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
				} else	{
					$('<option>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
				}
			}
		}
	}
	fillskillsData();
}

function def_loc()    {
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#loc-list');
		}
	}
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Designation</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Reporting to</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Salary (CTC)</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Employment Type</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}
function fillUserData()		{
	var sel4Obj = JSON.parse($('#depdata').val());
	var sel5Obj = JSON.parse($('#roledata').val());
	var sel6Obj = JSON.parse($('#locdata').val());
	var sel16Obj = JSON.parse($('#empdata').val());

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel4Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel4Obj[ctr];
				if (obj.dept == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel5Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel5Obj[ctr];
				if (obj.role == selx._id)	{
					newRow += selx.roletitle;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel6Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel6Obj[ctr];
				if (obj.location == selx._id)	{
					newRow += selx.oname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.desg != null)
				newRow += obj.desg;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel16Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel16Obj[ctr];
				if (obj.mgr == selx.userid)	{
					newRow += selx.username;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.doj != null)  {
        var t = obj.doj.indexOf('T00:');
				newRow += obj.doj.substring(0,t);
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.salary != null)
				newRow += obj.salary;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.emptype)     {
				case '1':
					newRow += 'Employee';
					break;
				case '2':
					newRow += 'Trainee';
					break;
				case '3':
					newRow += 'Intern';
					break;
				case '4':
					newRow += 'Contractor';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'On Probation';
					break;
				case '2':
					newRow += 'Confirmed';
					break;
				case '3':
					newRow += 'Resigned';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/220newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.users').data('footable');

			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.users tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.users').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.users').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#uname').val(obj.username);
		$('#email').val(obj.useremailid);
    var t = obj.dob.indexOf('T00:');
		$('#dob').val(obj.dob.substring(0,t));
		$('#gender-list').val(obj.gender);
		$('#pass1').val('');
		$('#pass2').val('');
		$('#mgr-list').val(obj.mgr);
		$('#dep-list').val(obj.dept);
		def_role(obj.role);
		//$('#role-list').val(obj.role);
		$('#loc-list').val(obj.location);
		$('#desg').val(obj.desg);
    var t = obj.doj.indexOf('T00:');
		$('#jdate').val(obj.doj.substring(0,t));
    var t = obj.probdate.indexOf('T00:');
		$('#probdate').val(obj.probdate.substring(0,t));
		$('#sal').val(obj.salary);
		$('#etype-list').val(obj.emptype);

		document.getElementById('mgr-list').disabled = true;
		document.getElementById('dep-list').disabled = true;
		document.getElementById('role-list').disabled = true;
		document.getElementById('loc-list').disabled = true;
		document.getElementById('probdate').disabled = true;
		document.getElementById('etype-list').disabled = true;

		document.getElementById('skills_btn').disabled = true;
		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Please reconfirm action";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.userid = obj.userid;
			dataObj.uname= obj.username;
			dataObj.email= obj.useremailid;
			dataObj.pass= obj.password;
			dataObj.mgr= obj.mgr;
			dataObj.dep= obj.dept;
			dataObj.role= obj.role;
			dataObj.loc= obj.location;
			dataObj.desg= obj.desg;
			dataObj.jdate= obj.doj;
			dataObj.probdate= obj.probdate;
			dataObj.sal= obj.salary;
			dataObj.etype= obj.emptype;

			$.ajax({
				type: 'DELETE',
				url: '/220',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.users').data('footable');

						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.users tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'users data deleted');
						get_users();
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs(mode)		{
	var s_uname = $('#uname').val();
	if (s_uname == '')	{
		$('#uname').css('border-color', 'red');
		setError($('#errtext1'),'Please input User Name');
		$('#uname').focus();
		return false;
	} else    {
		$('#uname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_email = $('#email').val();
	if (s_email != '')	{
		var lastAtPos = s_email.lastIndexOf('@');
		var lastDotPos = s_email.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_email.indexOf('@@') == -1 && lastDotPos > 2 && (s_email.length - lastDotPos) > 2);
		if (!result)	{
			setError($('#errtext1'),'Please input valid email id');
			$('#email').focus();
			return false;
		} else    {
			$('#email').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	var s_pass1 = $('#pass1').val();
	if (mode == 'add')		{
		if (s_pass1 == '')	{
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	if (s_pass1 != '')	{
		if (s_pass1.length < 8)   {
			$('#pass1').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		var s_pass2 = $('#pass2').val();
		if (s_pass2 == '')	{
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Please input Password');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass2.length < 8)   {
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Password should be atleast 8 characters');
			$('#pass2').focus();
			return false;
		} else    {
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
		if (s_pass1 != s_pass2)	{
			$('#pass1').css('border-color', 'red');
			$('#pass2').css('border-color', 'red');
			setError($('#errtext1'),'Passwords are not same');
			$('#pass1').focus();
			return false;
		} else    {
			$('#pass1').css('border-color', 'default');
			$('#pass2').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}

	var s_dob = $('#dob').val();
	if (s_dob == '')	{
		$('#dob').css('border-color', 'red');
		setError($('#errtext1'),'Please input Date Of Birth');
		return false;
	} else    {
		$('#dob').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_gender = $('#gender-list').val();
	if (s_gender == '')	{
		$('#gender-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Gender');
		return false;
	} else    {
		$('#gender-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_dep = $('#dep-list').val();
	if (s_dep == null)	{
		$('#dep-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Department');
		$('#dep-list').focus();
		return false;
	} else    {
		$('#dep-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_role = $('#role-list').val();
	var s_rtype;

	if (s_role != null)	{
		var tmp = JSON.parse(s_role); 
		s_role = tmp.roleid;
		s_rtype = tmp.rtype;
	}
	if (s_role == null)	{
		$('#role-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Role');
		$('#role-list').focus();
		return false;
	} else    {
		$('#role-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_loc = $('#loc-list').val();
	if (s_loc == null)	{
		$('#loc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Location');
		$('#loc-list').focus();
		return false;
	} else    {
		$('#loc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_desg = $('#desg').val();
	if (s_desg == '')	{
		$('#desg').css('border-color', 'red');
		setError($('#errtext1'),'Please input Designation');
		$('#desg').focus();
		return false;
	} else    {
		$('#desg').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_jdate = $('#jdate').val();
	if (s_jdate == '')	{
		$('#jdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input Date Joined');
		$('#jdate').focus();
		return false;
	} else    {
		$('#jdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_probdate = $('#probdate').val();
	if (s_probdate == '')	{
		$('#probdate').css('border-color', 'red');
		setError($('#errtext1'),'Please input probation end Date');
		$('#probdate').focus();
		return false;
	} else    {
		$('#probdate').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_sal = $('#sal').val();
	if (s_sal == '')	{
		$('#sal').css('border-color', 'red');
		setError($('#errtext1'),'Please input Salary (CTC)');
		$('#sal').focus();
		return false;
	} else    {
		$('#sal').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_etype = $('#etype-list').val();
	if (s_etype == null)	{
		$('#etype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Employment Type');
		$('#etype-list').focus();
		return false;
	} else    {
		$('#etype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_mgr = $('#mgr-list').val();
	if (s_etype == null)	{
		$('#mgr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a reporting manager');
		$('#mgr-list').focus();
		return false;
	} else    {
		$('#mgr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.uname= s_uname;
	formdataObj.email= s_email;
	formdataObj.pass= s_pass1;
	formdataObj.gender= s_gender;
	formdataObj.dob = s_dob;
	formdataObj.mgr= s_mgr;
	formdataObj.dep= s_dep;
	formdataObj.role= s_role;
	formdataObj.rtype= s_rtype;
	formdataObj.loc= s_loc;
	formdataObj.desg= s_desg;
	formdataObj.jdate= s_jdate;
	formdataObj.probdate= s_probdate;
	formdataObj.sal= s_sal;
	formdataObj.etype= s_etype;

	// status ==> 1 = On Probation 2 = Confirmed 3 = On notice period
	if (s_jdate == s_probdate)
		formdataObj.empsts = 2;
	else
		formdataObj.empsts = 1;

	var data1 = JSON.parse($('#onbdata').val());
	var onblen = data1.length;
	var onbObj = [];
	for (var i=0; i< onblen; i++)			{
		onbObj.push({title : data1[i].title, desc : data1[i].desc, status : 0});
	}
	formdataObj.onbdacts = onbObj;
	
	formdataObj.skratings = JSON.parse($('#currskills').val());

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs('add');
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/220add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);

				fillUserData();
				SetPagination();
				setSuccess($('#errtext1'),'New user added');
				get_users();
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'), err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function get_users()		{
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();

	$.ajax({
		type: 'GET',
		url: '/220users',
		data: formdataObj,
		dataType: 'json',
		success: function(resp) {
			if (resp)   {
				$('#empdata').val(JSON.stringify(resp.empdata));
				$('#totusers').val(resp.totusers);
				$('#totlic').val(resp.totlic);
				$('#licv').val(resp.licv);

				def_emp();
				clearForm();
				lic_val();
				//setSuccess($('#errtext1'),'users data added');
			}
		},
		error: function(err) {
			setError($('#errtext1'), err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs('edit');
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/220upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.users').data('footable');

				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.users tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				document.getElementById('mgr-list').disabled = false;
				document.getElementById('dep-list').disabled = false;
				document.getElementById('role-list').disabled = false;
				document.getElementById('loc-list').disabled = false;
				document.getElementById('probdate').disabled = false;
				document.getElementById('etype-list').disabled = false;
				setSuccess($('#errtext1'),'users data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function getSkillsOfRole(s_role)		{
	var xObj = JSON.parse($('#roledata').val());

	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == s_role)		{
				return obj.mustskills;
			}
		}
	}
	return null;
}

function getskilldesc(skid)		{
	var xObj = JSON.parse($('#skilldata').val());

	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == skid)		{
				return obj.skillname;
			}
		}
	}
	return "** No Data **";
}

function defskillsTable()    {
	$('table.skills').data('footable').reset();
	$('table.skills thead').append('<tr>');
	$('table.skills thead tr').append('<th>Ser #</th>');
	$('table.skills thead tr').append('<th>Skill</th>');
	$('table.skills thead tr').append('<th>Rating</th>');
	$('table.skills thead tr').append('</tr>');
	$('table.skills').footable();
}

function fillskillsData()		{
	var skdata = JSON.parse($('#skilldata').val());
	var s_role = $('#role-list').val();
	if (s_role != null)	{
		var tmp = JSON.parse(s_role); 
		s_role = tmp.roleid;
	}
	var roleskills = getSkillsOfRole(s_role);

	var rtgdata = JSON.parse($('#skrdata').val());
	if (rtgdata != null)
		var rtlen = rtgdata.length;
	else
		var rtlen = 0;
	var cskills = [];

	// DELETE & RE-CREATE Table 
	var rtable = $('table.skills').data('footable');
	$('table.skills tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.skills tbody tr').each(function() {
		rtable.removeRow($(this));
	});

	if (roleskills != null)		{
		var glen = roleskills.length;
		if (glen > 0)		{
			for(var i=0; i<glen; i++)   {
				var obj = roleskills[i];
				var newRow = '<tr>';

				newRow += '<td>';
				newRow += i+1;
				newRow += '</td>';

				newRow += '<td class = "col-lg-2">';
				var skdesc = getskilldesc(obj);
				newRow += skdesc;
				newRow += '</td>';

				newRow += '<td>';
				newRow += '<select class = "col-lg-10" id = "rating'+i+'">';
				for(x=0;x<rtlen;x++)		{
					if (x ==0)
						newRow += '<option value="'+rtgdata[x].sklevel+'" selected="">'+rtgdata[x].sklevel + " ("+ rtgdata[x].skdesc+")"+'</option>';
					else
						newRow += '<option value="'+rtgdata[x].sklevel+'">'+rtgdata[x].sklevel + " ("+ rtgdata[x].skdesc+")"+'</option>';
				}
				newRow += '</select>';
				newRow += '</td>';

				cskills.push({skid:obj, skdesc: skdesc, ratlevel:rtgdata[0].sklevel, ratdesc:rtgdata[0].skdesc});
				newRow += '</tr>';
				rtable.appendRow(newRow);
			}
		}
		rtable.redraw();
	}
	$('#currskills').val(JSON.stringify(cskills));
}

function saveSkills()			{
	var cskills = JSON.parse($('#currskills').val());
	if (cskills)		{
		var clen = cskills.length;
		for (var i=0;i<clen;i++)		{
			cskills[i].ratlevel = $("#rating"+i).val();
			cskills[i].ratdesc = $("#rating"+i+" :selected").text();
		}
		$('#currskills').val(JSON.stringify(cskills));
	}
}

function lic_val()		{
	var licstatus = $('#licv').val();
	var tusers = $('#totusers').val();
	var tlic = $('#totlic').val();
	if (licstatus == 'true')		{
		$('#add_btn').prop('disabled', false);
		if (tusers > tlic)
  		setError($('#errtext1'),"Total users (" + tusers +") exceeds or equals total licenses ("+tlic+")");
	} else	{
		$('#add_btn').prop('disabled', true);
		setError($('#errtext1'),"Licenses expired - You cannot add more users");
	}
}

$(document).ready(function()	{
	lic_val();
	def_dep();
	def_emp();
	def_role();
	def_loc();

	defMesgTable();
	fillUserData();
	SetPagination();

	defskillsTable();
	fillskillsData();

	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();

		document.getElementById('mgr-list').disabled = false;
		document.getElementById('dep-list').disabled = false;
		document.getElementById('role-list').disabled = false;
		document.getElementById('loc-list').disabled = false;
		document.getElementById('probdate').disabled = false;
		document.getElementById('etype-list').disabled = false;

		document.getElementById('skills_btn').disabled = false;
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;

		def_role();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
	$('#skills_btn').click(function() 			{
	  $('#skillsModal').modal();
		return false;
	});
	$('#save_btn').click(function() 			{
		saveSkills();
		$('#skillsModal').modal('hide');
		return false;
	});
});

