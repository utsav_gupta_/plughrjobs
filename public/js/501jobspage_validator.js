/* -----------------------------------------------
usertype 1 = site admin
usertype 2 = Site users
usertype 3 = Company admins
usertype 4 = Company users
usertype 5 = Candidates
------------------------------------------------- */

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
	var dataObj = JSON.parse($('#formdata').val());
	var obj = dataObj[0];
  var arrwdays = ['Full time', 'Few days a week', 'Work from home']
  var curncy = ["","USA Dollar","IND Rupees","UK Pounds","EUR","Canadian Dollar","Singapore Dollar","Australian Dollar","UAE Dirhams","Malaysia Ringgit","Thailand Baht"];

 	$('#title').text(obj.title);
 	if (obj.company)    {
    $('#empl').text(obj.company.companyname + " ,  " + obj.company.city);
    $('#aboute').text(obj.company.about);
 	}
 	
  $('#wdays').text(arrwdays[obj.workdays-1]);
  if (obj.eqtymin != '' && parseFloat(obj.eqtymin) != 0 && obj.eqtymax != '' && parseFloat(obj.eqtymax) != 0 )
    var sstr = " Annual " + obj.comp + " ( " + curncy[obj.curr] + " )" + " ,  Equity " + obj.eqtymin + "% to " + obj.eqtymax +"%";
  else
    var sstr = " Annual " + obj.comp + " ( " + curncy[obj.curr] + " )";
  $('#salry').text(sstr);

  $('#aboutj').text(obj.jdesc);

	$('#apply_btn').click(function() {
  	$('#errtext1').text('To apply for this job, please email to ' + obj.emailid);
	});
});

