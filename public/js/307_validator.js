$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(function () {
	$('table.skills').footable();
	var uid = $('#cuser').text();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#sdesc').val('');
	$('#srating-list').prop('selectedIndex', 0);
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.skills').data('footable').reset();
	$('table.skills thead').append('<tr>');
	$('table.skills thead tr').append('<th>Skill</th>');
	$('table.skills thead tr').append('<th data-hide="phone,tablet">Self Rating</th>');
	$('table.skills thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.skills thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.skills thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.skills thead tr').append('</tr>');
	$('table.skills').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.skills').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			newRow += obj.skilldesc;
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.selfrating)     {
			  case '1':
    			newRow += "Highly Skilled";
    			break;
			  case '2':
    			newRow += "Somewhat skilled";
    			break;
			  case '3':
    			newRow += "Knows basics";
    			break;
			  case '4':
    			newRow += "Novice";
    			break;
			}
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

$(function () {
	$('table.skills').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.skills').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

    //alert(obj.selfrating);
		$('#sdesc').val(obj.skilldesc);
	  $('#srating-list').prop('selectedIndex', (obj.selfrating-1));
		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	if (evtObj && evtObj.target)    {
		var fdata = JSON.parse($('#formdata').val());

		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex,10)+1;
		var obj = fdata[rno-1];
		dataObj.sdesc= obj.skilldesc;
		dataObj.srating= obj.selfrating;

		$.ajax({
			type: 'DELETE',
			url: '/307',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(o) {
				if (!o.err)   {
					$('#formdata').val(JSON.stringify(o));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.skills').data('footable');

					$('table.skills tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.skills tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					clearForm();
					setSuccess($('#errtext1'),'skills data deleted');
				} else  {
					setError($('#errtext1'),o.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
	}
}
function GetValidInputs()			{
	var s_sdesc = $('#sdesc').val();
	if (s_sdesc == '')	{
		$('#sdesc').css('border-color', 'red');
		setError($('#errtext1'),'Please input Skill');
		return false;
	} else    {
		$('#sdesc').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_srating = $('#srating-list').val();
	if (s_srating == '')	{
		$('#srating').css('border-color', 'red');
		setError($('#errtext1'),'Please input Self Rating');
		return false;
	} else    {
		$('#srating').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.sdesc= s_sdesc;
	formdataObj.srating= s_srating;
  return formdataObj;
}
function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/307add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(o) {
			if (!o.err)   {
				$('#formdata').val(JSON.stringify(o));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.skills').data('footable');

				$('table.skills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.skills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				clearForm();
				setSuccess($('#errtext1'),'skills data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/307upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(o) {
			if (!o.err)   {
				$('#formdata').val(JSON.stringify(o));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.skills').data('footable');

				$('table.skills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.skills tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				clearForm();
				setSuccess($('#errtext1'),'skills data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

