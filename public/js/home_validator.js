/* -----------------------------------------------
usertype 1 = site admin
usertype 2 = Site users
usertype 3 = Company admins
usertype 4 = Company users
usertype 5 = Candidates
------------------------------------------------- */

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}

function getQueryStringValue (key) {  
  return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
}  

function showResults(sData)     {
  $("#results").empty();
  var wdays = ['Full time', 'Few days a week', 'Work from home']
  var newDiv = "";
  var curncy = ["","USA Dollar","IND Rupees","UK Pounds","EUR","Canadian Dollar","Singapore Dollar","Australian Dollar","UAE Dirhams","Malaysia Ringgit","Thailand Baht"];
  var dlen = sData.length;
  for(i=0;i<dlen;i++)     {
    
    newDiv += "<div class='well' id='well'"+i+">";
    if (sData[i].company) {
      newDiv += "<h3>" + sData[i].title + "&nbsp;,&nbsp;&nbsp;" + sData[i].company.companyname + "&nbsp;,&nbsp;&nbsp;" + sData[i].jobcity + "</h3>";
    } else  {
      newDiv += "<h3>" + sData[i].title + "&nbsp;,&nbsp;&nbsp;" + 'n/a' + "&nbsp;,&nbsp;&nbsp;" + 'n/a' + "</h3>";
    }
    newDiv += "<p>" + wdays[sData[i].workdays-1] + "</p>";
    if (sData[i].eqtymin != '' && parseFloat(sData[i].eqtymin) != 0 && sData[i].eqtymax != '' && parseFloat(sData[i].eqtymax) != 0 )
      var sstr = " Annual " + sData[i].comp + " ( " + curncy[sData[i].curr] + " )" + " ,  Equity " + sData[i].eqtymin + "% to " + sData[i].eqtymax +"%";
    else
      var sstr = " Annual " + sData[i].comp + " ( " + curncy[sData[i].curr] + " )";
    $('#salry').text(sstr);

    newDiv += "<p>" + sstr;

    newDiv += "<a href='/jobpage?jobid="+ sData[i]._id +"' target='_blank'>" + " more... " +  "</a></p>";
    newDiv += "</div>";
  }
  $("#results").append(newDiv);
}

$(document).ready(function()	{
  if ($("#formdata").val())     {
    showResults(JSON.parse($("#formdata").val()));
    setSuccess($('#errtext1'),'');
  }

	$('#home1-signin').click(function() {
		$("#loginUserId").val('');
    $('#loginUserId').css('border-color', 'default');
		$("#loginPassword").val('');
    $('#loginPassword').css('border-color', 'default');
    clearError($('#errtext3'));
	  $('#loginModal').modal();
		return false;
	});
	// Would write the value of the QueryString-variable called name to the console  
	$("#loginCoId").val(getQueryStringValue("coid"));
	$("#loginUserId").val(getQueryStringValue("userid"));

	$('#jsearch_btn').click(function() {
    window.location = "/jsearch";
	});
	$('#searchjobs_btn').click(function() {
    var s_stext = $("#stext").val();
	  var s_jobcntry = $('#jobcntry').val();
	  var s_jobcity = $('#jobcity').val();
    if (s_stext == '' && s_jobcntry == '' && s_jobcity == '' )	{
		   $('#label1').css('color', 'red');
       $('#label6').css('color', 'red');
 	     $('#label6').css('color', 'red');
       setError($('#errtext1'),'Please input at least one criteria to search');
       return false;
    } else    {
 		   $('#label1').css('color', 'inherit');
 		   $('#label6').css('color', 'inherit');
       $('#label6').css('color', 'inherit');
       clearError($('#errtext1'));
    }
    var searchObject = new Object();
    searchObject.stext = s_stext;
    searchObject.jobcntry = s_jobcntry;
    searchObject.jobcity = s_jobcity;

    $.ajax({
       type: "POST",
       url: '/jsearch',
       data: searchObject,
       dataType: 'json',
       beforeSend:  function()   {
          startAjaxIcon();
       },
       success: function(robj) {
          if (robj.err1 == false)  {
            if (robj.data.length > 0)   {
              showResults(robj.data);
              setSuccess($('#errtext1'),'Search complete');
            } else  {
              $("#results").empty();
              setError($('#errtext1'),"No matching jobs found");
            }
            stopAjaxIcon();
          } else  {
            $("#results").empty();
            setError($('#errtext1'),"No matching jobs found");
            stopAjaxIcon();
          }
       },
       error: function(eobj) {
          setError($('#errtext1'),eobj.text);
          stopAjaxIcon();
       }
    });
    $('#clearsearch_btn').prop('disabled', false);
    return false;
	});
  $('#clearsearch_btn').click(function() {
    window.location = "/jsearch";
  });

	$('#candidate_btn').click(function() {
    window.location = "/csignup";
	});
	$('#employer_btn').click(function() {
    window.location = "/esignup";
	});
	$('#newpwd_btn').click(function() {
    var npass1 = $("#stuPassword1").val();
    if (npass1 == "")	{
       $('#stuPassword1').css('border-color', 'red');
       setError($('#errtext1'),'Please input new password');
       return false;
    } else    {
       $('#stuPassword1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (npass1.length < 8)   {
       $('#stuPassword1').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#stuPassword1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    var npass2 = $("#stuPassword2").val();
    if (npass2 == "")	{
       $('#stuPassword2').css('border-color', 'red');
       setError($('#errtext1'),'Please input new password twice');
       return false;
    } else    {
       $('#stuPassword2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (npass2.length < 8)   {
       $('#stuPassword2').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#stuPassword2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (npass1 != npass2)	{
       $('#stuPassword1').css('border-color', 'red');
       $('#stuPassword2').css('border-color', 'red');
       setError($('#errtext1'),'Passwords are not same');
       return false;
    } else    {
       $('#stuPassword1').css('border-color', 'default');
       $('#stuPassword2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    var joinObject = new Object();
    joinObject.coid = $("#loginCoId").val();
    joinObject.userid = $("#loginUserId").val();
    joinObject.npass = npass1;

    $.ajax({
       type: "POST",
       url: '/updpwd',
       data: joinObject,
       dataType: 'json',
       beforeSend:  function()   {
          startAjaxIcon();
       },
       success: function(robj) {
          if (robj.err1 == false)  {
             window.location.assign('showlogin');
             stopAjaxIcon();
          } else
             setError($('#errtext1'),robj.text);
             stopAjaxIcon();
          },
       error: function(eobj) {
          setError($('#errtext1'),eobj.text);
          stopAjaxIcon();
       }
    });
    return false;
	});
	$('#userJoin_btn').click(function() {
		var susername = $("#username").val();
    if (!susername)   {
       $('#username').css('border-color', 'red');
       setError($('#errtext1'),"Pls input your name");
       return false;
    } else    {
       $('#username').css('border-color', 'default');
       clearError($('#errtext1'));
    }
	  var semail = $("#useremail").val();
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
    if (semail == "")   {
       $('#useremail').css('border-color', 'red');
       setError($('#errtext1'),'Please input email id');
       return false;
    } else    {
       $('#useremail').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
       $('#userEmail').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid email id');
       return false;
    } else    {
       $('#userEmail').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var susermobile = $("#usermobile").val();
    if (!susermobile)   {
       $('#usermobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input your mobile number');
       return false;
    } else    {
       $('#usermobile').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (susermobile.length < 10)   {
       $('#usermobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid mobile number');
       return false;
    } else    {
       $('#usermobile').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var spwd1 = $("#pw1").val();
    if (!spwd1)   {
       $('#pw1').css('border-color', 'red');
       setError($('#errtext1'),"Pls input a password");
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd1.length < 8)   {
       $('#pw1').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var spwd2 = $("#pw2").val();
    if (!spwd2)   {
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),"Pls recofirm the password");
       return false;
    } else    {
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd2.length < 8)   {
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd1 != spwd2)   {
       $('#pw1').css('border-color', 'red');
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),"Both password should be same");
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var sstuagree = $("#useragree").prop('checked');
    if (sstuagree == false)   {
       $('#useragree').css('border-color', 'red');
       setError($('#errtext1'),'You have to agree to our policies to Join');
       return false;
    } else    {
       $('#useragree').css('border-color', 'default');
       clearError($('#errtext1s'));
    }

		var joinObject = new Object();
		joinObject.username = susername;
		joinObject.email = semail;
		joinObject.usermobile = susermobile;
    joinObject.password = spwd1;
	  joinObject.usertype = '5';

    //alert(JSON.stringify(joinObject));
    //return false;

		$.ajax({
			type: "POST",
			url: '/userjoin',
			data: joinObject,
			dataType: 'json',
        beforeSend:  function()   {
          startAjaxIcon();
        },
        success : function(robj) { 
          if (robj.err == 0)
            //setSuccess($('#errtext1'),robj.text);
            window.location.assign('signupok');
          else
				    setError($('#errtext1'),robj.text);
            stopAjaxIcon();
			  },
			  error: function(eobj) {
				  setError($('#errtext1'),eobj.text);
            stopAjaxIcon();
	      }
		});
		return false;
	});
	$('#emplJoin_btn').click(function() {
		var susername = $("#username").val();
    if (!susername)   {
       $('#username').css('border-color', 'red');
       setError($('#errtext1'),"Pls input your name");
       return false;
    } else    {
       $('#username').css('border-color', 'default');
       clearError($('#errtext1'));
    }
	  var semail = $("#useremail").val();
		var lastAtPos = semail.lastIndexOf('@');
		var lastDotPos = semail.lastIndexOf('.');
    if (semail == "")   {
       $('#useremail').css('border-color', 'red');
       setError($('#errtext1'),'Please input email id');
       return false;
    } else    {
       $('#useremail').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && semail.indexOf('@@') == -1 && lastDotPos > 2 && (semail.length - lastDotPos) > 2);
		if (!result)	{
       $('#userEmail').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid email id');
       return false;
    } else    {
       $('#userEmail').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var susermobile = $("#usermobile").val();
    if (!susermobile)   {
       $('#usermobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input your mobile number');
       return false;
    } else    {
       $('#usermobile').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (susermobile.length < 10)   {
       $('#usermobile').css('border-color', 'red');
       setError($('#errtext1'),'Please input valid mobile number');
       return false;
    } else    {
       $('#usermobile').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var spwd1 = $("#pw1").val();
    if (!spwd1)   {
       $('#pw1').css('border-color', 'red');
       setError($('#errtext1'),"Pls input a password");
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd1.length < 8)   {
       $('#pw1').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var spwd2 = $("#pw2").val();
    if (!spwd2)   {
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),"Pls recofirm the password");
       return false;
    } else    {
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd2.length < 8)   {
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
    if (spwd1 != spwd2)   {
       $('#pw1').css('border-color', 'red');
       $('#pw2').css('border-color', 'red');
       setError($('#errtext1'),"Both password should be same");
       return false;
    } else    {
       $('#pw1').css('border-color', 'default');
       $('#pw2').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var sindusty = $("#industy").val();
		var scompany = $("#company").val();
    if (!scompany)   {
       $('#company').css('border-color', 'red');
       setError($('#errtext1'),'Please input company name');
       return false;
    } else    {
       $('#company').css('border-color', 'default');
       clearError($('#errtext1'));
    }
		var scoagree = $("#coagree").prop('checked');
    if (scoagree == false)   {
       $('#coagree').css('border-color', 'red');
       setError($('#errtext1'),'Please confirm your authority');
       return false;
    } else    {
       $('#coagree').css('border-color', 'default');
       clearError($('#errtext1s'));
    }
		var sstuagree = $("#useragree").prop('checked');
    if (sstuagree == false)   {
       $('#useragree').css('border-color', 'red');
       setError($('#errtext1'),'You have to agree to our policies to Join');
       return false;
    } else    {
       $('#useragree').css('border-color', 'default');
       clearError($('#errtext1s'));
    }

		var joinObject = new Object();
		joinObject.username = susername;
		joinObject.email = semail;
		joinObject.usermobile = susermobile;
    joinObject.password = spwd1;
		joinObject.company = scompany;
	  joinObject.usertype = '3';    // Employer admin

    //alert(JSON.stringify(joinObject));
    //return false;

		$.ajax({
			type: "POST",
			url: '/companyjoin',
			data: joinObject,
			dataType: 'json',
         beforeSend:  function()   {
            startAjaxIcon();
         },
			success : function(robj) { 
            if (robj.err == 0)
  				   //setSuccess($('#errtext1'),robj.text);
             window.location.assign('signupok');
            else
  				   setError($('#errtext1'),robj.text);
            stopAjaxIcon();
			},
			error: function(eobj) {
				setError($('#errtext1'),eobj.text);
            stopAjaxIcon();
			}
		});
		return false;
	});
	
	$('#signin_btn').click(function() {
    var suserid = $("#loginUserId").val();
    if (suserid == "")   {
      $('#loginUserId').css('border-color', 'red');
      setError($('#errtext3'),'Please input email id');
      return false;
    } else    {
      $('#loginUserId').css('border-color', 'default');
      clearError($('#errtext3'));
    }
		var lastAtPos = suserid.lastIndexOf('@');
		var lastDotPos = suserid.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && suserid.indexOf('@@') == -1 && lastDotPos > 2 && (suserid.length - lastDotPos) > 2);
		if (!result)	{
       $('#loginUserId').css('border-color', 'red');
       setError($('#errtext3'),'Please input valid email id');
       return false;
    } else    {
       $('#loginUserId').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var spwd = $("#loginPassword").val();
    if (!spwd)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Please input password');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }
    if (spwd.length < 8)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var loginObject = new Object();
		loginObject.userid = suserid;
		loginObject.pwd = spwd;

		//alert(JSON.stringify(loginObject));

		$.ajax({
		  url: '/login',
		  data: loginObject,
		  dataType: 'json',
      beforeSend:  function()   {
        startAjaxIcon();
      },
      success : function(robj) { 
        //alert(JSON.stringify(robj));
        if (robj.err == false)    {
          //alert('1');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 0)    {
          //alert('2');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 99)    {
          //alert('3');
	        window.location.href = robj.redirect+"?coid=" + robj.coid+"&userid="+ robj.userid;
        } else    {
          //alert(JSON.stringify(robj));
		      setError($('#errtext3'),robj.text);
          stopAjaxIcon();
        }
          //alert('5');
			},
			error: function(eobj) {
				setError($('#errtext3'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});




