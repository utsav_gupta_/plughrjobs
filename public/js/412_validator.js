$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.userskills').footable();
	$('table.skills').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function def_emp()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var eObj = new Object;
			eObj.userid = obj.userid;
			eObj.role = obj.role;
			$('<option>').val(JSON.stringify(eObj)).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defskillsTable()    {
	$('table.userskills').data('footable').reset();
	$('table.userskills thead').append('<tr>');
	$('table.userskills thead tr').append('<th>Ser. No </th>');
	$('table.userskills thead tr').append('<th>Skill</th>');
	$('table.userskills thead tr').append('<th>Rating</th>');
	$('table.userskills thead tr').append('</tr>');
	$('table.userskills').footable();
}

function fillskillsData()		{
	var uskldata = JSON.parse($('#formdata').val());
	if (uskldata[0])
		var roleskills = uskldata[0].skratings;
	else		{
		setError($('#errtext1'),'No skills defined for user');
	}
	// DELETE & RE-CREATE Table 
	var rtable = $('table.userskills').data('footable');
	$('table.userskills tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.userskills tbody tr').each(function() {
		rtable.removeRow($(this));
	});

	if (roleskills != null)		{
		var glen = roleskills.length;
		if (glen > 0)		{
			for(var i=0; i<glen; i++)   {
				var obj = roleskills[i];
				var newRow = '<tr>';

				newRow += '<td>';
				newRow += i+1;
				newRow += '</td>';

				newRow += '<td>';
				newRow += obj.skdesc;
				newRow += '</td>';

				newRow += '<td>';
				newRow += obj.ratlevel + " ("+ obj.ratdesc + ")";
				newRow += '</td>';

				newRow += '</tr>';
				rtable.appendRow(newRow);
			}
		}
		rtable.redraw();
	}
}
function get_list()		{
	var eObj = JSON.parse($('#emp-list').val());
	var s_emp = eObj.userid;
	var s_role = eObj.role;

	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp = s_emp;
	formdataObj.role = s_role;

	$.ajax({
		type: 'POST',
		url: '/412search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#skilldata').val(JSON.stringify(resp.skilldata));
				$('#skrdata').val(JSON.stringify(resp.skrdata));
				$('#roleskills').val(JSON.stringify(resp.roleskills));

				$('#editrow').val(-1);
				fillskillsData();
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function getSkillsOfRole(s_role)		{
	var xObj = JSON.parse($('#roledata').val());

	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == s_role)		{
				return obj.mustskills;
			}
		}
	}
	return null;
}

function getskilldesc(skid)		{
	var xObj = JSON.parse($('#skilldata').val());

	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == skid)		{
				return obj.skillname;
			}
		}
	}
	return "** No Data **";
}
function defModalTable()    {
	$('table.skills').data('footable').reset();
	$('table.skills thead').append('<tr>');
	$('table.skills thead tr').append('<th>Ser #</th>');
	$('table.skills thead tr').append('<th>Skill</th>');
	$('table.skills thead tr').append('<th>Rating</th>');
	$('table.skills thead tr').append('</tr>');
	$('table.skills').footable();
}

function fillModalData()		{
	var rtgdata = JSON.parse($('#skrdata').val());
	if (rtgdata != null)
		var rtlen = rtgdata.length;
	else
		var rtlen = 0;
	var uskldata = JSON.parse($('#formdata').val());
	if (uskldata[0])			{
		var roleskills = uskldata[0].skratings;

		// DELETE & RE-CREATE Table 
		var rtable = $('table.skills').data('footable');
		$('table.skills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.skills tbody tr').each(function() {
			rtable.removeRow($(this));
		});

		if (roleskills != null)		{
			var glen = roleskills.length;
			if (glen > 0)		{
				for(var i=0; i<glen; i++)   {
					var obj = roleskills[i];
					var newRow = '<tr>';

					newRow += '<td>';
					newRow += i+1;
					newRow += '</td>';

					newRow += '<td>';
					newRow += obj.skdesc;
					newRow += '</td>';

					newRow += '<td>';
					newRow += '<select id = "rating'+i+'">';
					for(x=0;x<rtlen;x++)		{
						if (rtgdata[x].sklevel == obj.ratlevel)
								newRow += '<option value="'+rtgdata[x].sklevel+'" selected="">'+rtgdata[x].sklevel + " ("+ rtgdata[x].skdesc+")"+'</option>';
							else
								newRow += '<option value="'+rtgdata[x].sklevel+'">'+rtgdata[x].sklevel + " ("+ rtgdata[x].skdesc+")"+'</option>';
					}
					newRow += '</select>';
					newRow += '</td>';

					//newRow += '<td>';
					//newRow += obj.ratdesc;
					//newRow += '</td>';

					newRow += '</tr>';
					rtable.appendRow(newRow);
				}
			}
			rtable.redraw();
		}
	}
}

function saveSkills()			{
	var uskldata = JSON.parse($('#formdata').val());
	if (uskldata[0])			{
		var cskills = uskldata[0].skratings;
		if (cskills)		{
			var clen = cskills.length;
			for (var i=0;i<clen;i++)		{
				cskills[i].ratlevel = $("#rating"+i).val();
				cskills[i].ratdesc = $("#rating"+i+" :selected").text();
			}
			var formdataObj = new  Object();
			formdataObj.coid = $('#ccoid').text();
			formdataObj.userid = $('#cuserid').text();
			formdataObj.emp = $('#emp-list').val();
			formdataObj.dbid = uskldata[0]._id;
			formdataObj.dep = uskldata[0].dept;
			formdataObj.role = uskldata[0].role;
			formdataObj.rtype = parseInt(uskldata[0].rtype);
			formdataObj.mgr = uskldata[0].mgr;
			formdataObj.skratings = cskills;

			//alert(JSON.stringify(formdataObj));
			//return false;

			$.ajax({
				type: 'POST',
				url: '/412add',
				data: formdataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						fillskillsData();
						fillModalData();
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'), err.responseText);
					stopAjaxIcon();
				}
			});
		}
	}
}

$(document).ready(function()	{
	def_emp();
	defskillsTable();
	defModalTable();
	$('#search_btn').click(function() 			{
		get_list();
		$('#emp-list').prop('disabled', true);
		$('#upd_btn').prop('disabled', false);
		return false;
	});
	$('#clear_btn').click(function() 			{
		// DELETE & RE-CREATE Table 
		var rtable = $('table.userskills').data('footable');
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.userskills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('#emp-list').prop('disabled', false);
		$('#upd_btn').prop('disabled', true);
		return false;
	});
	$('#upd_btn').click(function() 			{
		fillModalData();
	  $('#skillsModal').modal();
		return false;
	});
	$('#save_btn').click(function() 			{
		saveSkills();
		$('#skillsModal').modal('hide');
		return false;
	});
});

