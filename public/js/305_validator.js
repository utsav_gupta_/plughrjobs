$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.jobs').footable();
 	/* $('#kra').select2({
    placeholder: "Select all applicable KRA",
		allowClear: true,
		dropdownAutoWidth : true,
		tags: true,
    tokenSeparators: [","],
    createTag: function(newTag) {
        return {
            id: newTag.term,
            text: newTag.term + ' (new)'
        };
    }
 	}); 

 	$('#skills').select2({
    placeholder: "Select all applicable skills",
		allowClear: true,
		dropdownAutoWidth : true,
		tags: true,
    tokenSeparators: [","],
    createTag: function(newTag) {
        return {
            id: newTag.term,
            text: newTag.term + ' (new)'
        };
    }
 	}); */
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode == 46)
    return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function redef()    {
  def_kra();
  def_skills();
}

function selwdays()    {
	var currsel = $('select[name=workdays]').val();
  if (currsel == '1')   {
    $("select[name='pdays']").removeAttr("disabled");
  } else  {
    $("select[name='pdays']").attr("disabled", true);
  }
}

function selwhours()    {
	var currsel = $('select[name=workhours]').val();
  if (currsel == '1')   {
    $("#sthour").removeAttr("disabled");
  } else  {
    $("#sthour").attr("disabled", true);
  }
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept');
		}
	}
}

function def_kra()    {
	var currdept = $('#dept').val();
	$('#kra').empty();
	var xObj = JSON.parse($('#kradata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == currdept)
  			$('<option>').val(obj._id).text(obj.kratitle).appendTo('#kra');
		}
	}
}

function def_skills()    {
	var currdept = $('#dept').val();
	$('#skills').empty();
	var xObj = JSON.parse($('#skillsdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == currdept)
  			$('<option>').val(obj._id).text(obj.skillname).appendTo('#skills');
		}
	}
}

function clearForm()		{
	$('#dbid').val('');
 	$('#title').val('');
  // $('#dept').prop('selectedIndex',0);
  // redef();
  // $('#kra').trigger("change"); 
  // $('#skills').trigger("change");
  $('#skills').val(''); 
  $('#jdesc').val('');
  $('#comp').val('');
  $('#eqtymin').val('');
  $('#eqtymax').val('');

  $('#jobcntry').val('');
  $('#jobcity').val('');
 	$('#locale').val('');
 	$('#emailid').val('');

 	$('#exp').prop('selectedIndex',0);
  $('#curr').prop('selectedIndex',0);

  $("select[name=workdays]").prop('selectedIndex',0);
  $('#kra').val(null).trigger("change"); 
  $('#skills').val(null).trigger("change");

	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.jobs').data('footable').reset();
	$('table.jobs thead').append('<tr>');
	$('table.jobs thead tr').append('<th>Job Title</th>');
	// $('table.jobs thead tr').append('<th data-hide="phone,tablet">Department</th>');
	$('table.jobs thead tr').append('<th data-hide="phone,tablet">Work Preference</th>');
	$('table.jobs thead tr').append('<th data-hide="phone,tablet">Country</th>');
	$('table.jobs thead tr').append('<th data-hide="phone,tablet">City</th>');
	$('table.jobs thead tr').append('<th data-hide="phone,tablet">Edit/Repost</th>');
	$('table.jobs thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.jobs thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.jobs thead tr').append('</tr>');
	$('table.jobs').footable();
}
function fillUserData()		{
	// var deptlist = JSON.parse($('#deptdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.jobs').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			/* newRow += '<td>';
			var dblen = deptlist.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = deptlist[ctr];
				if (obj.dept == selx._id)	{
					newRow += selx.depname;
					break;
				}
			}
			newRow += '</td>'; */

			newRow += '<td>';
			if (obj.workdays != null)   {
			  var tstr = '';
			  if (obj.workdays == '1')
			    tstr = 'Full time';
			  if (obj.workdays == '2')
			    tstr = 'Few days a week';
			  if (obj.workdays == '3')
			    tstr = 'Work from home';
		    newRow += tstr;
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.jobcntry != null)   {
				newRow += obj.jobcntry;
			} else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.jobcity != null)
				newRow += obj.jobcity;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit/Repost"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/305newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.jobs').data('footable');

			$('table.jobs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.jobs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(function () {
	$('table.jobs').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
		document.getElementById('postjob').style.display = "inline";
	});
	$('table.jobs').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
   	$('#title').val(obj.title);
    $('#skills').val(obj.skills); 
    $('#jdesc').val(obj.jdesc);
    $('#exp').val(obj.expr);
    $('#comp').val(obj.comp);
    $('#eqtymin').val(obj.eqtymin);
    $('#eqtymax').val(obj.eqtymax);

    $('#jobcntry').val(obj.jobcntry);
    $('#jobcity').val(obj.jobcity);

   	$('#locale').val(obj.locale);
   	$('#emailid').val(obj.emailid);

    $('#industy').val(obj.industy);
    $('#curr').val(obj.curr);
    //$("input[name=workdays][value=" + obj.workdays + "]").prop('checked', true);
    $('select[name=workdays]').val(obj.workdays);
    $('#kra').val(obj.kra).trigger("change"); 
    $('#skills').val(obj.skills).trigger("change"); 
    
		document.getElementById('add_btn').disabled = false;
		$('#add_btn').html('Repost as new job');
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid = obj._id;
			dataObj.title = obj.title;
			dataObj.dept = obj.dept;
			dataObj.kra = obj.kra;
			dataObj.jdesc = obj.jdesc;
			dataObj.jobcntry = obj.jobcntry;
			dataObj.jobcity = obj.jobcity;
			dataObj.locale = obj.locale;
			dataObj.emailid = obj.emailid;
			dataObj.workdays = obj.workdays;
			dataObj.expr = obj.exp;
			dataObj.skills = obj.skills;
			dataObj.comp = obj.comp;
			dataObj.eqtymin = obj.eqtymin;
			dataObj.eqtymax = obj.eqtymax;

			$.ajax({
				type: 'DELETE',
				url: '/305',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.jobs').data('footable');

						$('table.jobs tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.jobs tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'jobs data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs(mode)		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#label1').css('color', 'red');
		setError($('#errtext1'),'Please input Job title');
		$('#title').focus();
		return false;
	} else    {
		$('#label1').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	/* var s_dept = $('#dept').val();
	if (s_dept == null)	{
		$('#label2').css('color', 'red');
		setError($('#errtext1'),'Please select a department');
		$('#dept').focus();
		return false;
	} else    {
		$('#label2').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_kra = $("#kra").select2("val");
	if (s_kra == null)	{
		$('#label3').css('color', 'red');
		setError($('#errtext1'),'Please select applicable KRAs');
		$('#kra').focus();
		return false;
	} else    {
		$('#label3').css('color', 'inherit');
		clearError($('#errtext1'));
	} */
	var s_jdesc = $('#jdesc').val();
	if (s_jdesc == '')	{
		$('#label4').css('color', 'red');
		setError($('#errtext1'),'Please input Job details');
		$('#jdesc').focus();
		return false;
	} else    {
		$('#label4').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_jobcntry = $('#jobcntry').val();
	if (s_jobcntry == null)	{
		$('#label6').css('color', 'red');
		setError($('#errtext1'),'Please select a country');
		$('#jobcntry').focus();
		return false;
	} else    {
		$('#label6').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_jobcity = $('#jobcity').val();
	if (s_jobcity == null)	{
		$('#label6').css('color', 'red');
		setError($('#errtext1'),'Please select a city');
		$('#jobcity').focus();
		return false;
	} else    {
		$('#label6').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_locale = $('#locale').val();
	if (s_locale == '')	{
		$('#label14').css('color', 'red');
		setError($('#errtext1'),'Please input the locality within the city');
		$('#locale').focus();
		return false;
	} else    {
		$('#label14').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_emailid = $('#emailid').val();
	var lastAtPos = s_emailid.lastIndexOf('@');
	var lastDotPos = s_emailid.lastIndexOf('.');
	if (s_emailid == '')	{
		$('#label15').css('color', 'red');
		setError($('#errtext1'),'Please input emailid to apply');
		$('#emailid').focus();
		return false;
	} else    {
		$('#label15').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_emailid.indexOf('@@') == -1 && lastDotPos > 2 && (s_emailid.length - lastDotPos) > 2);
	if (!result)	{
		$('#label15').css('color', 'red');
    setError($('#errtext1'),'Please input valid email id');
    return false;
  } else    {
		$('#label15').css('color', 'inherit');
    clearError($('#errtext1'));
  }
	var s_workdays = $("#wdays").val();
	if (!s_workdays || s_workdays == null)	{
		$('#label8').css('color', 'red');
		setError($('#errtext1'),'Please select work preference');
		$('#wdays').focus();
		return false;
	} else    {
		$('#label8').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_exp = $('#exp').val();
	if (s_exp == null)	{
		$('#label11').css('color', 'red');
		setError($('#errtext1'),'Please select required experience in years');
		$('#exp').focus();
		return false;
	} else    {
		$('#label11').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_skills = $("#skills").val();
	if (s_skills == null)	{
		$('#label12').css('color', 'red');
		setError($('#errtext1'),'Please enter applicable skills');
		$('#skills').focus();
		return false;
	} else    {
		$('#label12').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_comp = $('#comp').val();
	if (s_comp == '')	{
		$('#label13').css('color', 'red');
		setError($('#errtext1'),'Please input salary offered for this job');
		$('#comp').focus();
		return false;
	} else    {
		$('#label13').css('color', 'inherit');
		clearError($('#errtext1'));
	}
  var s_curr = $('#curr').val();
  if (s_curr == null)	{
	  $('#label13').css('color', 'red');
	  setError($('#errtext1'),'Please select compensation currency');
	  $('#curr').focus();
	  return false;
  } else    {
	  $('#label13').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_eqtymin = $('#eqtymin').val();
	var s_eqtymax = $('#eqtymax').val();

	var formdataObj = new  Object();
	formdataObj.title = s_title;
	formdataObj.jdesc = s_jdesc;
	formdataObj.jobcntry = s_jobcntry;
	formdataObj.jobcity = s_jobcity;
	formdataObj.locale = s_locale;
	formdataObj.emailid = s_emailid;
	formdataObj.workdays = s_workdays;
	formdataObj.expr = s_exp;
	formdataObj.skills = s_skills;
	formdataObj.comp = s_comp;
	formdataObj.curr = s_curr;
	formdataObj.eqtymin = s_eqtymin;
	formdataObj.eqtymax = s_eqtymax;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs('add');
	if (!formdataObj)
		return false;

  //console.log(formdataObj);
  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/305add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.jobs').data('footable');

				$('table.jobs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.jobs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);

				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),resp.text);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs('edit');
	if (!formdataObj)
		return false;
	formdataObj.dbid= $('#dbid').val();

  //console.log(formdataObj);
  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/305upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.jobs').data('footable');

				$('table.jobs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.jobs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),resp.text);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}


$(document).ready(function()	{
  // def_dept();
  // def_kra();
  // def_skills();
	defMesgTable();
	fillUserData();
	SetPagination();

	$('#postjob_btn').click(function() {
		document.getElementById('postjob').style.display = "inline";
		return false;
	});

	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

