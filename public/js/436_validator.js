$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.users').footable();
});

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Designation</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Employment Type</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}
function fillUserData()		{
	var sel5Obj = JSON.parse($('#roledata').val());
	var sel6Obj = JSON.parse($('#locdata').val());

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel5Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel5Obj[ctr];
				if (obj.role == selx._id)	{
					newRow += selx.roletitle;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel6Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel6Obj[ctr];
				if (obj.location == selx._id)	{
					newRow += selx.oname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.desg != null)
				newRow += obj.desg;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.doj != null)
				newRow += obj.doj;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.emptype)     {
				case '1':
					newRow += 'Full-time';
					break;
				case '2':
					newRow += 'Part-time';
					break;
				case '3':
					newRow += 'Freelancing';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'On Probation';
					break;
				case '2':
					newRow += 'Confirmed';
					break;
				case '3':
					newRow += 'Resigned';
					break;
			}
			newRow += '</td>';


			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/436newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.cvalues').data('footable');

			$('table.cvalues tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.cvalues tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
});


