var tablelist = [];
var totpos = 0;   // Total possible optional leaves
var totused = 0;	// Totally availed optional leaves

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(function () {
	document.getElementById('mxopt').disabled = true;
	document.getElementById('used').disabled = true;
	$('table.holidaypolicy').footable();
	var uid = $('#cuser').text();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function def_hid()    {
	var xObj = JSON.parse($('#hiddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.holidayid).text(obj.holidayname + " (" +  obj.holidaydate + ") ").appendTo('#hid-list');
		}
	}
}

function def_oname()    {
	var xObj = JSON.parse($('#onamedata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.oname).text(obj.oname).appendTo('#oname-list');
		}
	}
}
function defMesgTable()    {
	$('table.holidaypolicy').data('footable').reset();
	$('table.holidaypolicy thead').append('<tr>');
	$('table.holidaypolicy thead tr').append('<th>Holiday</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Date</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Type</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.holidaypolicy thead tr').append('<th data-hide="phone,tablet">Action</th>');
	$('table.holidaypolicy thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.holidaypolicy thead tr').append('</tr>');
	$('table.holidaypolicy').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#formdata').val());
	var sel2Obj = JSON.parse($('#hiddata').val());
	var sel5Obj = JSON.parse($('#onamedata').val());
	var rtable = $('table.holidaypolicy').data('footable');
	var rcount = 0;

	totpos = 0;   // Total possible optional leaves
	totused = 0;	// Totally availed optional leaves

	var alist = [];
	if (sel1Obj)		{
		var alen = sel1Obj.length;
		for(var x=0;x<alen;x++)		{
			alist.push(sel1Obj[x].holidayid);
		}
	}
	//alert(alist);

	var obj = JSON.parse($('#hpdata').val());
	var olen = obj.length;
	var found = false;
	for (var octr = 0;octr < olen; octr++)		{
		var hlist = obj[octr];

		var today = new Date();
    var edate = new Date(hlist.validto);
    var sdate = new Date(hlist.validfrom);
    if (today <= edate && today >= sdate)       {
    		found = true;
    		break;
    }
  }
	if (found)		{
		var sdatepos = hlist.validfrom.indexOf(' 00');		// Don't remove the space before the 00
		var edatepos = hlist.validto.indexOf(' 00');

		$('#pgtitle').text("Holiday list for period  " + hlist.validfrom.substr(0,sdatepos) + "  to  " + hlist.validto.substr(0,edatepos));
		totpos = hlist.maxopholidays;
		$('#mxopt').val(totpos);
		var pid = hlist._id;			// Holiday Policy ID

		var olen = hlist.selholiday.length;
		var rcount = 0;
		
		for (var temp = 0;temp < olen; temp++)		{
			var newRow = '<tr>';
			var selx = hlist.selholiday[temp];
			var dblen = sel2Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel2Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = hlist.selholiday[temp];
					if (inx == selx._id)	{

						var tmpobj = new Object;
						tmpobj.pid = pid;
						tmpobj.hid = selx._id
						tmpobj.hdate = selx.holidaydate;
						tablelist.push(tmpobj);

						newRow += '<td>';
						newRow += selx.holidayname;
						newRow += '</td>';
						newRow += '<td text-center>';
						newRow += selx.holidaydate;
						newRow += '</td>';
						newRow += '<td>';
						switch (selx.holidaytype)     {
							case '1':
								newRow += 'Mandatory';
								break;
							case '2':
								newRow += 'Optional';
								break;
						}
						newRow += '</td>';

						var hdate = selx.holidaydate;
						hdate = selx.holidaydate.substring(3,5) + "/"+selx.holidaydate.substring(0,2)+"/"+selx.holidaydate.substring(6,10)
 				 		hdate = new Date(hdate);
						if (alist.indexOf(selx._id) != -1)		{
							totused++;
							newRow += '<td>';
  						if (today < hdate)       {
								newRow += 'Planned';
							} else	{
								newRow += 'Availed';
							}
							newRow += '</td>';

							newRow += '<td>';
							if (selx.holidaytype == '2')
	  						if (today < hdate)
									newRow += '<a class="row-cancel" href="#"><span id = "e'+temp+'" class="btn btn-danger btn-sm" title="Edit">Cancel</span></a>';
	  						else
									newRow += '-';
							else
								newRow += '';
							newRow += '</td>';
						} else		{
							newRow += '<td>';
							newRow += '-';
							newRow += '</td>';

							newRow += '<td>';
							if (selx.holidaytype == '2')
								newRow += '<a class="row-avail" href="#"><span id = "e'+temp+'" class="btn btn-success btn-sm" title="Edit">Avail</span></a>';
							else
								newRow += '';
							newRow += '</td>';
						}
						newRow += '<td id="rowIndex">';
						newRow += rcount+1;
						newRow += '</td></tr>';
					}
				}
			}
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
	$("#used").val(totused);
}

$(function () {
	$('table.holidaypolicy').footable().on('click', '.row-avail', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		if (totpos <= totused)
			setError($('#errtext1'),'You have availed max allowed optional holidays');
		else
			availleave(e, rowindex);
	});
	$('table.holidaypolicy').footable().on('click', '.row-cancel', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		cancelleave(e, rowindex);
	});
});

function availleave(evtObj, rowIndex)	{
	var rno = parseInt(rowIndex,10);
	var today = new Date();
  var hdate = new Date(tablelist[rno].hdate);
  console.log(today);
  console.log(hdate);
  if (today > hdate)
		var mesg = "Holiday is in the past. Once availed, it can't be cancelled. Are you sure?";
	else
		var mesg = "Please confirm";

	alertify.set({ labels: {
		ok     : "Avail",
		cancel : "Don't Avail"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var obj = JSON.parse($('#hpdata').val());
			$('#editrow').val(rowIndex);
			//alert(rno);

			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuserid').text();
			dataObj.policyid = tablelist[rno].pid.toString();
			dataObj.holidayid = tablelist[rno].hid.toString();


			//alert(JSON.stringify(dataObj));
			//return false;

			$.ajax({
				type: 'POST',
				url: '/422add',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(o) {
					if (!o.err)   {
						$('#formdata').val(JSON.stringify(o));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.holidaypolicy').data('footable');

						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						setSuccess($('#errtext1'),'Optional holiday availed');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function cancelleave(evtObj, rowIndex)	{
	var mesg = "Please confirm";
	alertify.set({ labels: {
		ok     : "Cancel",
		cancel : "Don't cancel"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var obj = JSON.parse($('#hpdata').val());
			$('#editrow').val(rowIndex);
			var rno = parseInt(rowIndex,10);
			//alert(rno);

			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuserid').text();
			dataObj.policyid = tablelist[rno].pid.toString();
			dataObj.holidayid = tablelist[rno].hid.toString();

			//alert(JSON.stringify(dataObj));
			//return false;

			$.ajax({
				type: 'POST',
				url: '/422del',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(o) {
					if (!o.err)   {
						$('#formdata').val(JSON.stringify(o));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.holidaypolicy').data('footable');

						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.holidaypolicy tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						setSuccess($('#errtext1'),'Availed holiday is now cancelled');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
});

