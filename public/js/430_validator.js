$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#ldays').pickmeup({format  : 'd-m-Y', mode	: 'multiple', separator:' , ', default_date	: false, date : ''});
	$('#hdays').pickmeup({format  : 'd-m-Y', mode	: 'multiple', separator:' , ', default_date	: false, date : ''});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');

  $('#ldays').pickmeup('clear');
  $('#hdays').pickmeup('clear');
  $('#ldays').val('');
  $('#hdays').val('');

  $('#lname-list').prop('selectedIndex', 0);
	show_leave_details($('#lname-list').val());
	clearError($('#errtext1'));
	$('#lname-list').css('border-color', 'default');
	$('#ldays').css('border-color', 'default');
	$('#hdays').css('border-color', 'default');
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function weeks_between(date1, date2) {
	// The number of milliseconds in one week
	var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
	// Convert both dates to milliseconds
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();
	// Calculate the difference in milliseconds
	var difference_ms = Math.abs(date1_ms - date2_ms);
	// Convert back to weeks and return hole weeks
	return Math.floor(difference_ms / ONE_WEEK);
}

function getPrevBal(ltype)			{
	var ld = 0.0;
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee
	if (lvbalObj != null)    {
		var glen = lvbalObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = lvbalObj[i];
			if (obj.ltype == ltype)	{
				ld += parseFloat(obj.pbal);
			}
		}
	}
	return ld;
}

function prev_availed(ltype)			{
	var ld = 0.0;
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee
	if (lvbalObj != null)    {
		var glen = lvbalObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = lvbalObj[i];
			if (obj.ltype == ltype)	{
				ld += parseFloat(obj.totav);
			}
		}
	}
	return ld;
}

function leave_availed(ltype, hryear)			{
	var mesgObj = JSON.parse($('#lvdata').val());
	var ld = 0.0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
	    if (obj.lname == ltype && obj.status != 'rejected')	{
        if (obj.ldays) {
          var ldtot = obj.ldays.length;
          for (var x=0;x < ldtot;x++)     {
            var lday = obj.ldays[x];
            var lyear = parseFloat(lday.substr(6,4));
            if (lyear == hryear)
              ld++;
		      }
		    }
        //alert(obj.hdays);
        if (obj.hdays) {
          var ldtot = obj.hdays.length;
          for (var x=0;x < ldtot;x++)     {
            lday = obj.hdays[x];
            var lyear = parseFloat(lday.substr(6,4));
            if (lyear == hryear)
              ld = ld + 0.5;
		      }
		    }
	    }
		}
	}
	return ld;
}

function show_leave_details(ltype)    {
	var coObj = JSON.parse($('#codata').val());           // Current leave availed data
	var lvbalObj = JSON.parse($('#emplvdata').val());			// Opening leave balances for the employee

  // Leave year starting date
	var codate = new Date(coObj.hrdate.substring(2,6), coObj.hrdate.substring(0,1)-1, '1');

  // Joining date
	var tmpdate = $('#doj').val();
	var tmpdate = tmpdate.split("-");
	var jodate = new Date(tmpdate[2], tmpdate[1] - 1, tmpdate[0]);

  //console.log('Joinig date = ' + jodate);
  //console.log('Leave accrual start date = ' + codate);

	if (jodate > codate)	{
	  //console.log("Joining date is greater than leave calendar date");
		var hrmon = parseInt(tmpdate[1]);
		var hrdate = tmpdate[1] + "-" + tmpdate[2];
		//console.log("Calculating based on joining date");
	}	else		{
	  //console.log("Joining date is lesser than leave calendar date");
		var hrmon = parseInt(coObj.hrdate.substring(0,1));
		var hrdate = coObj.hrdate;
		//console.log("Calculating based on Hr year date");
	}

	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var lper = ['year','quarter','month','week'];

  // Get Current Month & Year
	var date = new Date();
	var cmon = date.getMonth()+1;
	var cyr = date.getFullYear();
	var firstDay = new Date(cyr, hrmon-1, 1);
  // Not needed - if HR year starts Apr and currently we are in Jan - then cyr would anyway be hryear + 1
	//if (hrmon > cmon)
		//cyr++;

	var leaveObj = JSON.parse($('#lnamedata').val());
	var ut = $('#utype').val();
	var pr = $('#prob').val();

	if (leaveObj != null)    {
		var glen = leaveObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = leaveObj[i];
			if (obj._id == ltype)		{     // Used for showing leave balances when user selects from drop-down list
				if (obj.etype != null)		{   // If leave obj doesn't have any applicable employee types
					var ef = obj.etype.indexOf(ut);
					if (ef == -1)		{
						setError($('#errtext1'),"You are not allowed to avail this leave");
						document.getElementById('add_btn').disabled = true;
					} else	{
						if (obj.puser == 'false')		{
							if (pr == '1')	{
								setError($('#errtext1'),"You are not allowed to avail this leave, while on probation");
								document.getElementById('add_btn').disabled = true;
							} else	{
								clearError($('#errtext1'));
								document.getElementById('add_btn').disabled = false;
							}
						} else	{
							clearError($('#errtext1'));
							document.getElementById('add_btn').disabled = false;
						}
					}
				}
        var lcrt1 = '';
        var lcrt2 = '';
        var lcrt3 = '';
        var lcrt4 = '';
        var lcrt5 = '';
        var lcrt6 = '';
        if (obj.maxl && obj.maxl != '')   {
          lcrt1 = "You can avail only "+obj.maxl+" days of consequtive leaves. ";
        }
        if (obj.negl && obj.negl != '')   {
          lcrt2 = "You can avail only "+obj.negl+" days of leaves in excess of what is normally accrued (negative leaves). ";
        }
        /*
        if (obj.preh && obj.preh != '')   {
          lcrt4 = " the preceeding day(s),";
        }
        if (obj.inth && obj.inth != '')   {
          lcrt5 = " the intervening day(s),";
        }
        if (obj.preh && obj.preh != '')   {
          lcrt6 = " the succeeding day(s),";
        }
        */
        //lcrt3 = "This leave includes "+ lcrt4 + lcrt5 + lcrt6 + " if they are weekends or holidays. ";

				$('#lcrt').text(obj.lcriteria+". "+ lcrt1+lcrt2);
				$('#laccp').text("Starting " + hrdate +", " + obj.ldays + " days of leaves for every "+ lper[obj.lper-1]);
        
        // Opening leave balance
				var op_bal = 0.00;
				//alert(ltype);
				op_bal = getPrevBal(ltype);
				//alert("prev bal = " + op_bal);

        // Current year accumulated leaves
				var cl_bal = 0.00;
				if (obj.lper == 1 )			{			// Annual
					cl_bal = cl_bal + parseFloat(obj.ldays);
				}
				if (obj.lper == 2 )			{			// Quarterly
          var smon = hrmon;
          var emon = hrmon+2;
          var qr = 0.0;
          //cmon = 6;
          //alert("cmon = "+cmon);
          for(var x = 0; x<4;x++)   {   // A year can have max of 4 quarters only
            if (cmon == smon)   {
    					qr = qr + 0;
    					break;
    				}
            if (cmon < emon)    {
    					qr = qr + 0;
    					break;
            }
            if (cmon == emon || cmon > emon)    {
    					qr = qr + parseFloat(obj.ldays);
              smon = smon + 3;
              emon = emon + 3;
            }
            //alert("qr = "+qr);
          }
          cl_bal = cl_bal + qr;
				}
				if (obj.lper == 3 )			{			// Monthly
          var diffmon = 0.0;
          if (hrmon == cmon || cmon == 12)
            diffmon = 1;
          if (hrmon > cmon)
            cmon = cmon + 12;
          diffmon = diffmon +  cmon - hrmon;
					cl_bal = cl_bal + (obj.ldays * diffmon);
				}
				if (obj.lper == 4 )			{			// Weekly
					//alert(firstDay);
					var wb = weeks_between(firstDay, date)+1;
					//alert(wb);
					cl_bal = cl_bal + (obj.ldays * wb);
				}
				//alert("current leave accrued = " + cl_bal);

        // Opening leave availed
				var pa = 0.00;
				pa = prev_availed(obj._id);
				//alert("prev availed = " + pa);

        // current year leave availed
				var la = 0.00;
				la = leave_availed(obj._id, cyr);
				//alert("leave availed = " + la);

        // Current leave balance
				var cb = 0.00;
				cb = op_bal + cl_bal - pa - la;

				$('#lavbl').text("Current Leave Balance = " + cb);

				//$('#paccrd').text("Prev leave balance = " + op_bal);
				//$('#ptakn').text("Prev availed leaves = " + pa);

				//$('#laccrd').text("Leave accrued till today = " + cl_bal);
				//$('#ltakn').text("Leave availed till today = " + la);

				$('#tol_lv_bal').val(parseFloat(cb));
				$('#tol_lv_used').val(parseFloat(la));
				$('#edit_lv').val(0);
			}
		}
	}
}

function def_lname()    {
	var leaveObj = JSON.parse($('#lnamedata').val());
	if (leaveObj != null)    {
		var glen = leaveObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = leaveObj[i];
			$('<option>').val(obj._id).text(obj.lname).appendTo('#lname-list');
		}
	}
}
function defMesgTable()    {
	$('table.empleaves').data('footable').reset();
	$('table.empleaves thead').append('<tr>');
	$('table.empleaves thead tr').append('<th>Leave Type</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Full days</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Half days</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Total</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.empleaves thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.empleaves thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.empleaves thead tr').append('</tr>');
	$('table.empleaves').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#lnamedata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	var ld = 0.0;

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.empleaves').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.lname == selx._id)	{
					newRow += selx.lname;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.hdays != null)
				newRow += obj.hdays;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ltot != null)
				newRow += obj.ltot;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.lstatus != null)
				newRow += obj.lstatus;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.lstatus == 'applied')		{
				newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
				newRow += '</td><td>';
				newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			} else	{
				newRow += '-';
				newRow += '</td><td>';
				newRow += '-';
			}
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/430newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.empleaves').data('footable');

			$('table.empleaves tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.empleaves tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.empleaves').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.empleaves').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#lname-list').val(obj.lname);
		show_leave_details($('#lname-list').val());

    if (obj.ldays)  {
  		$('#ldays').val(obj.ldays);
      var llen = obj.ldays.length;
      var nDates = [];
      for (x=0;x < llen; x++)     {
          var queryDate = obj.ldays[x];
          var dateParts = queryDate.match(/(\d+)/g);
          var realDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);      // months are 0-based!
          nDates.push(realDate);
      }
      $('#ldays').pickmeup({ dateFormat: 'dd-mm-yy' }); // format to show
      $('#ldays').pickmeup('set_date', nDates);
      $('#ldays').pickmeup('update');
    }
    if (obj.hdays)  {
  		$('#hdays').val(obj.hdays);
      var llen = obj.hdays.length;
      var nDates = [];
      for (x=0;x < llen; x++)     {
          var queryDate = obj.hdays[x];
          var dateParts = queryDate.match(/(\d+)/g);
          var realDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);      // months are 0-based!
          nDates.push(realDate);
      }
      $('#hdays').pickmeup({ dateFormat: 'dd-mm-yy' }); // format to show
      $('#hdays').pickmeup('set_date', nDates);
      $('#hdays').pickmeup('update');
    }
		$('#edit_lv').val(parseFloat(obj.ltot));

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.lname= obj.lname;
			dataObj.ldays= obj.ldays;
			dataObj.hdays= obj.hdays;

			$.ajax({
				type: 'DELETE',
				url: '/430',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));
						$('#lvdata').val(JSON.stringify(resp.lvdata));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.empleaves').data('footable');

						$('table.empleaves tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.empleaves tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'Leave request deleted');
						show_leave_details($('#lname-list').val());
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}
function GetValidInputs(upd)		{
	var s_lname = $('#lname-list').val();
	if (s_lname == null)	{
		$('#lname-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Leave Type');
		$('#lname-list').focus();
		return false;
	} else    {
		$('#lname-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_ldays = $('#ldays').pickmeup('get_date', true);
	var s_hdays = $('#hdays').pickmeup('get_date', true);
  var llen = s_ldays.length;
  var hlen = s_hdays.length;
  
	if (llen == 0 && hlen == 0)	{
		$('#ldays').css('border-color', 'red');
		$('#hdays').css('border-color', 'red');
		setError($('#errtext1'),'Please select leave days');
		return false;
	} else    {
		$('#ldays').css('border-color', 'default');
		$('#hdays').css('border-color', 'default');
		clearError($('#errtext1'));
	}

  for (var i=0; i < hlen; i++)      {
    if (s_ldays.indexOf(s_hdays[i]) != -1)      {
		  $('#ldays').css('border-color', 'red');
		  $('#hdays').css('border-color', 'red');
		  setError($('#errtext1'),'Please ensure leave days are not repeating');
		  return false;
    }
  }

	ltot = s_ldays.length;
	ltot += (s_hdays.length*0.5);

	var ld = parseFloat($('#tol_lv_bal').val());
	var la = parseFloat($('#tol_lv_used').val());

	var leaveObj = JSON.parse($('#lnamedata').val());
	if (leaveObj != null)    {
		var glen = leaveObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = leaveObj[i];
			if (obj._id == s_lname)   {
			  if (obj.maxl > 0) {
			    if (ltot > obj.maxl)  {
		        $('#ldays').css('border-color', 'red');
		        $('#hdays').css('border-color', 'red');
		        setError($('#errtext1'),'Exceeds maximum allowed consequent leave of '+obj.maxl+' days');
		        return false;
			    } else  {
		        $('#ldays').css('border-color', 'default');
		        $('#hdays').css('border-color', 'default');
		        clearError($('#errtext1'));
			    }
        }
        var ldiff = ld - ltot;
        if (upd)
          ldiff = ldiff + parseFloat($('#edit_lv').val());
        var ldiff = parseFloat(ldiff);

        if (obj.negl && obj.negl != ''&& obj.negl != '0')     {
          //alert("bal = "+ld);
          //alert("ldiff = "+ldiff);
          if (ldiff < 0)
            ldiff = ldiff * -1;
          //alert("ldiff = "+ldiff);
          var negl = obj.negl;
          var negl = parseFloat(negl);
          //alert("negl = "+negl);

          if (ld < 0) {
            var ldp = ld * -1;
            //alert("new bal = "+ldp);
            if (ld < 0 && ldp >= negl) {
	            $('#ldays').css('border-color', 'red');
	            $('#hdays').css('border-color', 'red');
	            setError($('#errtext1'),'Exceeds maximum allowed negative leave of '+obj.negl+' days');
	            return false;
		        } else  {
	            $('#ldays').css('border-color', 'default');
	            $('#hdays').css('border-color', 'default');
	            clearError($('#errtext1'));
		        }
	        }
          //alert(negl > ldiff);

          if (ldiff > negl)   {
	          $('#ldays').css('border-color', 'red');
	          $('#hdays').css('border-color', 'red');
	          setError($('#errtext1'),'Exceeds maximum allowed negative leave of '+obj.negl+' days');
	          return false;
		      } else  {
	          $('#ldays').css('border-color', 'default');
	          $('#hdays').css('border-color', 'default');
	          clearError($('#errtext1'));
		      }
        } else  {
          if (ldiff < 0)		{
            $('#ldays').css('border-color', 'red');
            $('#hdays').css('border-color', 'red');
            setError($('#errtext1'),'Insufficient leave balance');
            return false;
          } else    {
            $('#ldays').css('border-color', 'default');
            $('#hdays').css('border-color', 'default');
            clearError($('#errtext1'));
          }
        }
      }
		}
	}
  //return false;

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.lname= s_lname;
	formdataObj.ldays= s_ldays;
	formdataObj.hdays= s_hdays;
	formdataObj.ltot = ltot;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs(false);
	if (!formdataObj)
		return false;
	formdataObj.lstatus = 'applied';

	$.ajax({
		type: 'POST',
		url: '/430add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));
				$('#lvdata').val(JSON.stringify(resp.lvdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.empleaves').data('footable');

				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Leave request sent to supervisor for approval');
    		show_leave_details($('#lname-list').val());
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs(true);
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/430upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));
				$('#lvdata').val(JSON.stringify(resp.lvdata));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.empleaves').data('footable');

				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.empleaves tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Leave request updated');
    		show_leave_details($('#lname-list').val());
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_lname();
	defMesgTable();
	fillUserData();
	SetPagination();
	clearForm();
	show_leave_details($('#lname-list').val());

	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		$('#edit_lv').val(0);
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

