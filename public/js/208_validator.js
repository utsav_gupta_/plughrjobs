/*
cntry-list
ctype
regaddr
cinno
tanno
tanci
panno
pf
esic
*/

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selfile').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').text(fileName);
  });
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		$('#cname').val(formdataObj.companyname);
		$('#cwebsite').val(formdataObj.cwebsite);
		$('#abtus').val(formdataObj.aboutus);
		$('#ctype-list').val(formdataObj.ctype);
		//$('#cntry-list').val(formdataObj.country);
		$('#regaddr').val(formdataObj.regaddr);

    var randm = (new Date()).toString();
    var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + $('#ccoid').text() + "/" + formdataObj.clogo  + "?" + new Date().getTime();
    $("#userpict").attr('src',imgsrc);
	}
}

function GetValidInputs()			{
	var s_cname = $('#cname').val();
	if (s_cname == '')	{
		$('#cname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Company Name');
		return false;
	} else    {
		$('#cname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_cwebsite = $('#cwebsite').val();
	if (s_cwebsite == '')	{
		$('#cwebsite').css('border-color', 'red');
		setError($('#errtext1'),'Please input Corporate Website');
		return false;
	} else    {
		$('#cwebsite').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_abtus = $('#abtus').val();
	if (s_abtus == '')	{
		$('#abtus').css('border-color', 'red');
		setError($('#errtext1'),'Please write something about company');
		return false;
	} else    {
		$('#abtus').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var sfile = $("#selfile").val();
	var s_filename = '';
  if (sfile != "")	{
		s_filename = $('#filename').val();
		// Check filetype and filesize
		var filename = $("#selfile").val();
		var extensionAllowed=[".jpg",".jpeg",".gif",".png"];
		var i = filename.lastIndexOf('.');
		var file_extension= (i < 0) ? '' : filename.substr(i);
		var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

		if(rfound == -1)		{
		  $('#selfile').css('font-color', 'red');
		  setError($('#errtext1'),' Only Image file formats (jpg or jpeg or gif or png) are allowed');
			$("#selfile").text("select new image file");
		  return false;
		} else    {
		  $('#selfile').css('font-color', 'default');
		  clearError($('#errtext1'));
		}
		var fsize = document.getElementById('docfile').files[0].size;
		if ((fsize / 1024) > 2500)		{
		  $('#selfile').css('font-color', 'red');
		  setError($('#errtext1'),'Image file should be less than 2MB');
			$("#selfile").text("select new document");
		  return false;
		} else    {
		  $('#selfile').css('font-color', 'default');
		  clearError($('#errtext1'));
		}
	}

	//var s_country = $('#cntry-list').val();
	var s_country = '';
	var s_ctype = $('#ctype-list').val();
	var s_regaddr = $('#regaddr').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.country = s_country;
	formdataObj.cname = s_cname;
	formdataObj.cwebsite = s_cwebsite;
	formdataObj.abtus = s_abtus;
	formdataObj.clogo = s_filename;
	formdataObj.ctype = s_ctype;
	formdataObj.regaddr = s_regaddr;

  return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
  $('#submit_btn').click();
  return true;

}

$(document).ready(function()	{
  var options = {
    beforeSend: function()  {
      clearError($('#errtext1'));
      startAjaxIcon();
    },
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				if (resp.logofile)	{
					var randm = (new Date()).toString();
					var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + $('#ccoid').text() + "/" + resp.logofile  + "?" + new Date().getTime();
					$("#userpict").attr('src',imgsrc);
				}
		    $('#selfile').text('click here to change logo');
				setSuccess($('#errtext1'),'Company info updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
  	complete: function(response)    {
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#errtext1'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#adddocs-form").ajaxForm(options);

	showformdata();
	$('#upd_btn').click(function() 			{
		data_add();
	});
});

