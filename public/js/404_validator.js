$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#wt').val('');
	$('#meets').val('');
	//$('#ugoalid-list').empty();
	clearError($('#errtext1'));

	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function disableForm1()		{
	document.getElementById('rperiod-list').disabled = true;
	document.getElementById('search_btn').disabled = true;
	document.getElementById('clear_btn').disabled = false;
}

function enableForm1()		{
	document.getElementById('rperiod-list').disabled = false;
	document.getElementById('search_btn').disabled = false;
	document.getElementById('clear_btn').disabled = false;
}

function disableForm2()		{
	document.getElementById('ugoalid-list').disabled = true;
	document.getElementById('wt').disabled = true;
	document.getElementById('meets').disabled = true;

	document.getElementById('add_btn').disabled = true;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function enableForm2()		{
	$('#dbid').val('');
	$('#ugoalid-list').prop('selectedIndex', 0);
	$('#wt').val('');
	$('#meets').val('');
	clearError($('#errtext1'));

	document.getElementById('ugoalid-list').disabled = false;
	document.getElementById('wt').disabled = false;
	document.getElementById('meets').disabled = false;

	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function clearTable()		{
	var rtable = $('table.usergoals').data('footable');

	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function validatewt(curr_wt, prev_wt)		{
  //alert(curr_wt);
  //alert(prev_wt);
	var mesgObj = JSON.parse($('#formdata').val());
	var totwt = 0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			totwt += parseInt(mesgObj[i].weight);
		}
		totwt += curr_wt;
		totwt -= prev_wt;
		//alert(totwt);
		if (totwt > 100)
			return true;
	}
	return false;
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status == 2)
				$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function def_ugoalid()    {
	var xObj = JSON.parse($('#ugoaliddata').val());
	$('#ugoalid-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#ugoalid-list');
		}
	}
}

function getGoalTitle(goalid)		{
	var sel4Obj = JSON.parse($('#ugoaliddata').val());
	var rval = "Desc not found";
	if (sel4Obj)	{
		var dblen = sel4Obj.length;
		var set = false;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel4Obj[ctr];
			if (goalid == selx._id)	{
				rval = selx.title;
				break;
			}
		}
	}
	return rval;
}

function defMesgTable()    {
	$('table.usergoals').data('footable').reset();
	$('table.usergoals thead').append('<tr>');
	$('table.usergoals thead tr').append('<th>Review period</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Self target</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Weightage (%)</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Criteria</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.usergoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.usergoals thead tr').append('</tr>');
	$('table.usergoals').footable();
}

function fillUserData()		{
	var sel1Obj = JSON.parse($('#rperioddata').val());
	var sel4Obj = JSON.parse($('#ugoaliddata').val());
	var mesgObj = JSON.parse($('#formdata').val());

	// DELETE & RE-CREATE Table 
	var rtable = $('table.usergoals').data('footable');

	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.usergoals').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.rperiod == selx._id)	{
					newRow += selx.rptitle;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			if (sel4Obj)	{
				var dblen = sel4Obj.length;
				var set = false;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel4Obj[ctr];
					if (obj.ugoalid == selx._id)	{
						newRow += selx.title;
						newRow += '</td>';
						newRow += '<td>';
						newRow += selx.desc;
						set = true;
						break;
					}
				}
			}
			if (!set)		{
				newRow += '**No Data**';
				newRow += '</td>';
				newRow += '<td>';
				newRow += '**No Data**';
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.weight != null)
				newRow += obj.weight;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			if (obj.submitted == '0')			{
				newRow += '<td>';
				newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
				newRow += '</td><td>';
				newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
				newRow += '</td>';
			} else	{
				newRow += '<td>';
				newRow += ' ';
				newRow += '</td><td>';
				newRow += ' ';
				newRow += '</td>';
			}
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			  case 'first':
				  return 'First';
			  case 'prev':
				  return 'Previous';
			  case 'next':
				  return 'Next';
			  case 'last':
				  return 'Last';
			  case 'page':
				  return page;
		  }
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}

function GetNextPage(newpage)    {
	get_goals(newpage);
	return false;
}

$(function () {
	$('table.usergoals').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.usergoals').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);

		$('#ugoalid-list').val(obj.ugoalid);
		$('#wt').val(obj.weight);
		$('#prev_wt').val(obj.weight);
		$('#meets').val(obj.meets);
		$('#editrow').val(obj.weight);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.rperiod= obj.rperiod;
			dataObj.usr= $('#cuser').text();
			dataObj.ugoalid= obj.ugoalid;
			dataObj.wt= obj.weight;

			$.ajax({
				type: 'DELETE',
				url: '/404',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.usergoals').data('footable');

						$('table.usergoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.usergoals tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						//SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'Self target deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_rperiod = $('#rperiod-list').val();
	var s_ugoalid = $('#ugoalid-list').val();
	if (s_ugoalid == null)	{
		$('#ugoalid-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select target');
		$('#ugoalid-list').focus();
		return false;
	} else    {
		$('#ugoalid-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	s_ugoaltitle = getGoalTitle(s_ugoalid);
	var s_wt = $('#wt').val();
	if (s_wt == '')	{
		$('#wt').css('border-color', 'red');
		setError($('#errtext1'),'Please input Weightage (%)');
		$('#wt').focus();
		return false;
	} else    {
		$('#wt').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_meets = $('#meets').val();
	if (s_meets == null)	{
		$('#meets').css('border-color', 'red');
		setError($('#errtext1'),'Please input criteria for achieving this target');
		$('#meets').focus();
		return false;
	} else    {
		$('#meets').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.rperiod= s_rperiod;
	formdataObj.usr= $('#cuserid').text();
	formdataObj.ugoalid= s_ugoalid;
	formdataObj.ugoaltitle= s_ugoaltitle;
	formdataObj.wt= s_wt;
	formdataObj.meets= s_meets;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	if (validatewt(parseInt(formdataObj.wt),0))		{
		$('#wt').css('border-color', 'red');
		setError($('#errtext1'),'Cumulative Weightage should not exceed 100%');
		$('#wt').focus();
		return false;
	} else    {
		$('#wt').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	$.ajax({
		type: 'POST',
		url: '/404add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				//SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Self target added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	var p_wt = $('#prev_wt').val();
	if (validatewt(parseInt(formdataObj.wt),parseInt(p_wt)))		{
		$('#wt').css('border-color', 'red');
		setError($('#errtext1'),'Cumulative Weightage should not exceed 100%');
		$('#wt').focus();
		return false;
	} else    {
		$('#wt').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	$.ajax({
		type: 'POST',
		url: '/404upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				//SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'Self target updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function GetSearchInputs()		{
	var s_rperiod = $('#rperiod-list').val();
	if (s_rperiod == null)	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select Review period');
		$('#rperiod-list').focus();
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod = s_rperiod;
	formdataObj.usr = $('#cuserid').text();
	return formdataObj;
}

function get_goals(pageno)		{
	var formdataObj = GetSearchInputs();
	if (!formdataObj)
		return false;
	formdataObj.pageno = pageno;

	$.ajax({
		type: 'POST',
		url: '/404search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));
				$('#ugoaliddata').val(JSON.stringify(resp.data2));
				$('#totpages').val(JSON.stringify(resp.totpages));

				def_ugoalid();
				fillUserData();
				if (pageno == 0)
  				//SetPagination();

				disableForm1();
				enableForm2();

				$("#add_btn").prop('disabled',false);
				setSuccess($('#errtext1'),'Targets for selected review period listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_rperiod();
	defMesgTable();

	$('#search_btn').click(function() 		{
		get_goals(0);
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearForm();
		clearTable();
		disableForm2();
		enableForm1();
		$('#ugoalid-list').empty();
		return false;
	});
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

