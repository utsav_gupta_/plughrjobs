$(function() {
	$(document).ajaxStart(function(){
	  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
	  $("#ajaxaction").hide();
	});
	$('table.kra').footable();
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#role-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('searchs_btn').disabled = false;
}

function def_roles(roleid)    {
	var xObj = JSON.parse($('#roledata').val());
	var seldep = $('#dept-list').val();

	$('#role-list').empty();
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.depid == seldep)		{
				var optObj = new Object;
				optObj.roleid = obj._id;
				optObj.depid = obj.depid;
				optObj.rtype = obj.roletype;
				if (roleid && roleid == optObj.roleid)		{
					$('<option selected>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
				} else	{
					$('<option>').val(JSON.stringify(optObj)).text(obj.roletitle).appendTo('#role-list');
				}
			}
		}
	}
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept-list');
		}
	}
}


function defMesgTable()    {
	$('table.kra').data('footable').reset();
	$('table.kra thead').append('<tr>');
	$('table.kra thead tr').append('<th>Department</th>');
	$('table.kra thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.kra thead tr').append('<th data-hide="phone,tablet">KRA Title</th>');
	$('table.kra thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.kra thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.kra thead tr').append('</tr>');
	$('table.kra').footable();
}

function clearTable()			{
	// DELETE & RE-CREATE Table 
	var rtable = $('table.kra').data('footable');

	$('table.kra tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.kra tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function fillUserData(roleid, deptid)		{
	var sel1Obj = JSON.parse($('#deptdata').val());
	var sel2Obj = JSON.parse($('#roledata').val());
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		clearTable();
		var rtable = $('table.kra').data('footable');

		var dblen = sel1Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel1Obj[ctr];
			if (deptid == selx._id)	{
				var depname = selx.depname;
				break;
			}
		}

		var dblen = sel2Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel2Obj[ctr];
			if (roleid == selx._id)	{
				var roledesc = selx.roletitle;
				break;
			}
		}

		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			newRow += depname;
			newRow += '</td>';

			newRow += '<td>';
			newRow += roledesc;
			newRow += '</td>';

			newRow += '<td>';
			if (obj.kratitle != null)
				newRow += obj.kratitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.kradesc != null)
				newRow += obj.kradesc;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/252newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.roles').data('footable');

			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.roles tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_dept();
	def_roles();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_roles();
		return false;
	});
});

function GetValidInputs()			{
	var s_role = $('#role-list').val();
	var s_rtype;

	if (s_role != null)	{
		var tmp = JSON.parse(s_role); 
		s_role = tmp.roleid;
		s_rtype = tmp.rtype;
	}
	var s_dept = $('#dept-list').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.role= s_role;
	formdataObj.dept= s_dept;

  return formdataObj;
}

function get_roles()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/252search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.data.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#deptdata').val(JSON.stringify(resp.dept));
				fillUserData(formdataObj.role, formdataObj.dept);
				setSuccess($('#errtext1'),'KRAs for selected roles listed below');
			} else  {
				clearTable();
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}


