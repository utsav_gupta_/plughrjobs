$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.kra').footable();
	$('table.skills').footable();
});
function showformdata()		{
	var sel02Obj = JSON.parse($('#skilldata').val());
	var sel03Obj = JSON.parse($('#empdata').val());
	var sel4Obj = JSON.parse($('#depdata').val());
	var sel5Obj = JSON.parse($('#roledata').val());
	var sel6Obj = JSON.parse($('#locdata').val());

	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		$('#uid').val(formdataObj.userid);
		$('#uname').val(formdataObj.username);

		var dblen = sel4Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel4Obj[ctr];
			if (formdataObj.dept == selx._id)	{
				$('#dep').val(selx.depname);
				break;
			}
		}
		var dblen = sel5Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel5Obj[ctr];
			if (formdataObj.role == selx._id)		{
				$('#role').val(selx.roletitle);
				break;
			}
		}
		var dblen = sel6Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel6Obj[ctr];
			if (formdataObj.location == selx._id)		{
				$('#loc').val(selx.oname);
				break;
			}
		}

		var dblen = sel03Obj.length;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel03Obj[ctr];
			if (parseInt(formdataObj.mgr) == selx.userid)	{
				$('#mgr').val(selx.username);
				break;
			}
		}

		$('#desg').val(formdataObj.desg);

		switch (formdataObj.emptype)     {
			case '1':
				$('#etype').val('Full-time Employee');
				break;
			case '2':
				$('#etype').val('Part-time Employee');
				break;
			case '3':
				$('#etype').val('Freelancing');
				break;
			case '4':
				$('#etype').val('Full-time Consultant');
				break;
			case '5':
				$('#etype').val('Part-time Consultant');
				break;
		}
		switch (formdataObj.status)     {
			case '1':
				$('#sts').val('On Probation');
				break;
			case '2':
				$('#sts').val('Confirmed');
				break;
			case '3':
				$('#sts').val('On Notice Period');
				break;
		}
		$('#sdate').val(formdataObj.doj);
		$('#probdate').val(formdataObj.probdate);
	}
}

function defKraTable()    {
	$('table.kra').data('footable').reset();
	$('table.kra thead').append('<tr>');
	$('table.kra thead tr').append('<th>KRA Title</th>');
	$('table.kra thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.kra thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.kra thead tr').append('</tr>');
	$('table.kra').footable();
}

function fillKraData()		{
	var mesgObj = JSON.parse($('#kradata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.kra').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.kratitle != null)
				newRow += obj.kratitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.kradesc != null)
				newRow += obj.kradesc;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function defskillsTable()    {
	$('table.skills').data('footable').reset();
	$('table.skills thead').append('<tr>');
	$('table.skills thead tr').append('<th>Skill</th>');
	$('table.skills thead tr').append('<th>Rating</th>');
	$('table.skills thead tr').append('</tr>');
	$('table.skills').footable();
}

function fillskillsData()		{
	var cskills = JSON.parse($('#uskldata').val());
	if (cskills[0])			{
		var roleskills = cskills[0].skratings;

		// DELETE & RE-CREATE Table 
		var rtable = $('table.skills').data('footable');
		$('table.skills tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.skills tbody tr').each(function() {
			rtable.removeRow($(this));
		});

		if (roleskills != null)		{
			var glen = roleskills.length;
			if (glen > 0)		{
				for(var i=0; i<glen; i++)   {
					var obj = roleskills[i];
					var newRow = '<tr>';

					newRow += '<td>';
					newRow += obj.skdesc;
					newRow += '</td>';

					newRow += '<td>';
					//newRow += obj.ratlevel + " ("+ obj.ratdesc + ")";
					newRow += obj.ratdesc;
					newRow += '</td>';

					newRow += '</tr>';
					rtable.appendRow(newRow);
				}
			}
			rtable.redraw();
		}
	}
}

function getskilldesc(skid)		{
	var xObj = JSON.parse($('#skilldata').val());

	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == skid)		{
				return obj.skillname;
			}
		}
	}
	return "** No Data **";
}

$(document).ready(function()	{
	showformdata();
	defKraTable();
	fillKraData();
	defskillsTable();
	fillskillsData();
});

