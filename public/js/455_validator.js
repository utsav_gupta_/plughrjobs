$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.deptindcq').footable();
});

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#usr-list');
		}
	}
}
function def_indc()    {
	var xObj = JSON.parse($('#indcdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#indc-list');
		}
	}
}
function defMesgTable()    {
	$('table.deptindcq').data('footable').reset();
	$('table.deptindcq thead').append('<tr>');
	$('table.deptindcq thead tr').append('<th>Question</th>');
	$('table.deptindcq thead tr').append('<th data-hide="phone,tablet">Correct Answer</th>');
	$('table.deptindcq thead tr').append('<th data-hide="phone,tablet">Employee\'s Answer</th>');
	$('table.deptindcq thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.deptindcq thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.deptindcq thead tr').append('</tr>');
	$('table.deptindcq').footable();
}

function fillAnsData(indc)		{
	var qsObj = JSON.parse($('#formdata').val());
	qsObj = qsObj[0];

	if (!qsObj[indc])	{
		setError($('#errtext1'),'No results were found');
		return false;
	}
	setSuccess($('#errtext1'),'Results were found');

	qsObj = qsObj[indc];

	if (qsObj.length != 0)    {
		var rtable = $('table.deptindcq').data('footable');
		var rcount = 0;

		for(var i=0; i < qsObj.length; i++)				{
			var newRow = '<tr>';

			newRow += '<td>';
			if (qsObj[i].qtext != null)
				newRow += qsObj[i].qtext;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (qsObj[i].cans != null)
				newRow += qsObj[i].cans;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (qsObj[i].uans != null)
				newRow += qsObj[i].uans;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (qsObj[i].status != null)
				if (qsObj[i].status == 'Not Correct')
					newRow += "<span class='status-red'>Not Correct</span>";
				else
					newRow += "<span class='status-green'>Correct</span>";
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += i+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function GetValidInputs()		{
	var s_usr = $('#usr-list').val();
	if (s_usr == null)	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select an employee');
		$('#usr-list').focus();
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_indc = $('#indc-list').val();
	if (s_indc == null)	{
		$('#indc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please Select Induction');
		$('#indc-list').focus();
		return false;
	} else    {
		$('#indc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.usr= s_usr;
	formdataObj.indc= s_indc;
	return formdataObj;
}

function get_results()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/455search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.deptindcq').data('footable');
				$('table.deptindcq tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.deptindcq tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				fillAnsData(formdataObj.indc);
				//setSuccess($('#errtext1'),'Results are listed below');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_usr();
	def_indc();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_results();
		return false;
	});
});

