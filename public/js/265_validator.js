$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('table.wizard').footable();
});

function defWizardTable()    {
	$('table.wizard').data('footable').reset();
	$('table.wizard thead').append('<tr>');
	$('table.wizard thead tr').append('<th>Message</th>');
	$('table.wizard thead tr').append('<th data-hide="phone,tablet">Message Type</th>');
	$('table.wizard thead tr').append('<th data-hide="phone,tablet">Mark as Read</th>');
	$('table.wizard thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.wizard thead tr').append('</tr>');
	$('table.wizard').footable();
}
function fillWizardData()		{
	var rtable = $('table.wizard').data('footable');
	$('table.wizard tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.wizard tbody tr').each(function() {
		rtable.removeRow($(this));
	});

	var notifs = JSON.parse($('#formdata').val());
  if (notifs != null)     {
	  var rcount = 0;
	  var mlen = notifs.length;
	  for(var i=0; i<mlen; i++)   {
      var mObj = notifs[i];
		  var newRow = '<tr>';

		  newRow += '<td>';
		  newRow += mObj.mesg;
		  newRow += '</td>';

		  if (mObj.mtype == '1')  {
			  newRow += '<td class="text-danger">';
		    newRow += 'Action required';
		  } else  {
			  newRow += '<td class="text-primary">';
		    newRow += 'For information';
			}
		  newRow += '</td>';

		  newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Mark Read"></span></a>';
		  newRow += '</td>';

		  newRow += '<td id="rowIndex">';
		  newRow += rcount+1;
		  newRow += '</td></tr>';
		  rtable.appendRow(newRow);
		  rcount++;
	  }
  	rtable.redraw();
  }
}
$(function () {
	$('table.wizard').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var fdata = JSON.parse($('#formdata').val());
		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex);
		var obj = fdata[rno];
		dataObj.dbid = obj._id;
		dataObj.mailid = obj.mailid;
		dataObj.mesg = obj.mesg;

		$.ajax({
			type: 'POST',
			url: '/265',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
				  //alert(JSON.stringify(resp));
					$('#formdata').val(JSON.stringify(resp.data));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.wizard').data('footable');
					$('table.wizard tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.wizard tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					$('#notiftot').text(resp.tot);
					
					fillWizardData();
				} else  {
					setError($('#errtext1'),o.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
	}
}

$(document).ready(function()	{
	defWizardTable();
	fillWizardData();
});

