$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.users').footable();
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function def_loclist()    {
	var xObj = JSON.parse($('#locdata').val());
	if (xObj != null)    {
		$('<option selected>').val('0').text("All Locations").appendTo('#loc-list');
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.oname).appendTo('#loc-list');
		}
	}
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th class="text-center">Department</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Total Count</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Total Cost</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Average Cost</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Average Age</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Total Women</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Total Men</th>');
	$('table.users thead tr').append('<th class="text-center" data-hide="phone,tablet">Womenshare</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}

function getDeptName(currdept)		{
	var sel1Obj = JSON.parse($('#deptdata').val());
	var dname = "";
	var dblen = sel1Obj.length;
	for (var ctr = 0;ctr < dblen; ctr++)		{
		var selx = sel1Obj[ctr];
		if (currdept == selx._id)	{
			dname = selx.depname;
			break;
		}
	}
	return dname;
}

function calcAge(dob)			{
	var cdate = new Date();
	var cmon = parseInt(cdate.getMonth());
	var cyr = parseInt(cdate.getFullYear());

  //alert(dob);
  //alert(typeof(dob));

	var dmon = parseInt(dob.substr(5,2));
	var dyr = parseInt(dob.substr(0,4));
	//var dmon = dob.getMonth() + 1;
	//var dyr = dob.getFullYear();

  //alert(dmon);
  //alert(dyr);
  
	var age = (cyr - dyr)*12;
	age = age + cmon - dmon;
	age = (age / 12);
	age = Math.floor(age);

	return age;
}

function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	var rtable = $('table.users').data('footable');
	if (mesgObj != null)    {
		var rcount = 0;
		var totcount = 0, totcost = 0, totage = 0, women = 0, men = 0, avgcost = 0, avgage = 0;
		var gtcount = 0, gtcost = 0, gtage = 0, gtwomen = 0, gtmen = 0, gavcost = 0, gavage = 0;

		var glen = mesgObj.length;

		var obj = mesgObj[0];
		var currdept = obj.dept;
		var cdeptname = getDeptName(currdept);

		while (1)		{
			totcount++;
			totcost += parseInt(obj.salary);

			if (obj.dob && obj.dob != "")
				var age = calcAge(obj.dob);
			else
				var age = 0;
			totage += age;

			if (obj.gender == "1")
				men++;
			else if (obj.gender == "2")
				women++;
			rcount++;
			if (rcount < glen)		{
				obj = mesgObj[rcount];
				if (obj.dept != currdept)			{
					avgcost = totcost / totcount;
					avgcost = avgcost.toFixed(0);
					avgage = totage / totcount;
					avgage = avgage.toFixed(2);

					if (men == 0)	{
						w2m = women;
					} else if (women == 0)	{
						w2m = 0;
					} else if (men != 0 && women != 0)		{
						w2m = women / totcount;
					}
					w2m = w2m.toFixed(2);

					newRow = showData(cdeptname, totcount, totcost, avgcost, avgage, women, men, w2m);
					rtable.appendRow(newRow);
					gtcount += totcount;
					gtcost += totcost;
					gtage += totage;
					gtwomen += women;
					gtmen += men;

					totcount = 0, totcost = 0, totage = 0, women = 0, men = 0, avgcost = 0, avgage = 0;
					currdept = obj.dept;
					cdeptname = getDeptName(currdept);
				}
			} else	{
				avgcost = totcost / totcount;
				avgcost = avgcost.toFixed(0);
				avgage = totage / totcount;
				avgage = avgage.toFixed(2);

				if (men == 0)	{
					w2m = women;
				} else if (women == 0)	{
					w2m = 0;
				} else if (men != 0 && women != 0)		{
					w2m = women / totcount;
				}
				w2m = w2m.toFixed(2);

				gtcount += totcount;
				gtcost += totcost;
				gtage += totage;
				gtwomen += women;
				gtmen += men;

				newRow = showData(cdeptname, totcount, totcost, avgcost, avgage, women, men, w2m);
				rtable.appendRow(newRow);
				break;
			}
		}

		gavcost = gtcost / gtcount;
		gavcost = gavcost.toFixed(0);
		gavage = gtage / gtcount;
		gavage = gavage.toFixed(2);

		if (gtmen == 0)	{
			w2m = women;
		} else if (gtwomen == 0)	{
			w2m = 0;
		} else if (gtmen != 0 && gtwomen != 0)		{
			w2m = gtwomen / gtcount;
		}
		w2m = w2m.toFixed(2);

		newRow = showData("All Departments", gtcount, gtcost, gavcost, gavage, gtwomen, gtmen, w2m);
		rtable.appendRow(newRow);
		rtable.redraw();
	}
}

function showData(cdeptname, totcount, totcost, avgcost, avgage, women, men, w2m)			{
	var newRow = '<tr>';
	newRow += '<td>';
	newRow += cdeptname;
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += totcount;
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += numberWithCommas(totcost);
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += numberWithCommas(avgcost);
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += avgage;
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += women;
	newRow += '</td>';

	newRow += '<td class="text-right">';
	newRow += men;
	newRow += '</td>';

	var tmp = parseFloat(w2m)*100;
	w2m = tmp.toString();
	newRow += '<td class="text-right">';
	newRow += w2m+"%";
	newRow += '</td>';

	newRow += '</tr>';
	return newRow;
}

function GetValidInputs()		{
	var s_loc = $('#loc-list').val();
	if (s_loc == null)	{
		$('#loc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an office location');
		$('#loc-list').focus();
		return false;
	} else    {
		$('#loc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_etype = $('#etype-list').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.loc = s_loc;
	formdataObj.etype = s_etype;
	return formdataObj;
}

function get_team()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/225search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				if (resp.data.length > 0)   {
					$('#formdata').val(JSON.stringify(resp.data));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					fillUserData();
					setSuccess($('#errtext1'),'Manpower Data listed below');
				} else	{
					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					setError($('#errtext1'),"No employees available in location");
				}
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_loclist();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_team();
		return false;
	});
});


