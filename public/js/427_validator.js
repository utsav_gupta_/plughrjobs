$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('table.users').footable();
});

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status == 2)
				$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function getUser(usrid)		{
	var xObj = JSON.parse($('#userdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.userid == usrid)
				return obj.username;
		}
	}
	return "";
}

function defMesgTable()    {
	$('table.users').data('footable').reset();
	$('table.users thead').append('<tr>');
	$('table.users thead tr').append('<th>User Name</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">User ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Email ID</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Role</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Location</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Designation</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Employment Type</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.users thead tr').append('<th data-hide="phone,tablet">Profile</th>');
	$('table.users thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.users thead tr').append('</tr>');
	$('table.users').footable();
}

function getGoalsStatus(userid)			{
	var sel3Obj = JSON.parse($('#goalsdata').val());
	var warn = false;
	if (sel3Obj)			{
		var glen = sel3Obj.length;
		if (glen > 0)		{
			for(var i=0; i<glen; i++)		{
				if (sel3Obj[i].userid == userid)
					return true;
			}
		}
	}
	return warn;
}

function fillUserData()		{
	var sel1Obj = JSON.parse($('#roledata').val());
	var sel2Obj = JSON.parse($('#locdata').val());

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.users').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.username != null)
				newRow += obj.username;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.userid != null)
				newRow += obj.userid;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.useremailid != null)
				newRow += obj.useremailid;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.role == selx._id)	{
					newRow += selx.roletitle;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var dblen = sel2Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel2Obj[ctr];
				if (obj.location == selx._id)	{
					newRow += selx.oname;
					break;
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.desg != null)
				newRow += obj.desg;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.doj != null)
				newRow += obj.doj;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.emptype)     {
				case '1':
					newRow += 'Full-time';
					break;
				case '2':
					newRow += 'Part-time';
					break;
				case '3':
					newRow += 'Freelancing';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
				case '1':
					newRow += 'On Probation';
					break;
				case '2':
					newRow += 'Confirmed';
					break;
				case '3':
					newRow += 'Resigned';
					break;
			}
			newRow += "<td>";
			var warn = getGoalsStatus(obj.userid);
			if (!warn)
				newRow += "<span class='status-red'>Target undefined</span>";
			else
				newRow += "<span class='status-green'>Target(s) defined</span>";
			newRow += '</td>';

			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function GetValidInputs()		{
	var s_rpid = $('#rperiod-list').val();
	if (s_rpid == null)	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a department');
		$('#rperiod-list').focus();
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rpid = s_rpid;
	return formdataObj;
}

function get_team()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/427search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				if (resp.data.length > 0)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#userdata').val(JSON.stringify(resp.data1));
					$('#roledata').val(JSON.stringify(resp.data2));
					$('#locdata').val(JSON.stringify(resp.data3));
					$('#goalsdata').val(JSON.stringify(resp.data4));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					fillUserData();
					setSuccess($('#errtext1'),'User\'s goals status listed below');
				} else	{
					// DELETE & RE-CREATE Table 
					var rtable = $('table.users').data('footable');
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.users tbody tr').each(function() {
						rtable.removeRow($(this));
					});

					setError($('#errtext1'),"No users defined for department");
				}
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_rperiod();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_team();
		return false;
	});
});

