$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#updphoto').click(function(){
      $('#pictfile').click();
  });
  $('#pictfile').change(function () {
      var fileName = $(this).val();
      $('#updphoto').text(fileName);
  });
});
$(document).ready(function()	{
	$(function fillUserProfile()		{
		var loggeruserObj = JSON.parse($("#formdata").val());

    var randm = (new Date()).toString();
    //var imgsrc = ('/userpictul?filename=' + loggeruserObj.coid + "-" + loggeruserObj.userid + '&rnd=' + randm);
    var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + loggeruserObj.coid + "/" + loggeruserObj.userid + "/" + loggeruserObj.coid + "-" + loggeruserObj.userid  + "?" + new Date().getTime();

    //var imgsrc = '/userpictul?stuid='+loggeruserObj.user;
    $("#userpict").attr('src',imgsrc);
	});
  var options = {
    beforeSend: function()  {
      clearError($('#message'));
      startAjaxIcon();
    },
    success: function(response)     {
      if (response.err == 0)  
        setSuccess($('#message'),response.text);
      else
        setError($('#message'),response.text);

      //$('#addphoto-form').clearForm();
  		//$("#emailid").val($("#cuser").text());
      //stopAjaxIcon();
    },
  	complete: function(response)    {
      var randm = (new Date()).toString();
	    //var imgsrc = ('/userpictul?filename=' +  $("#ccoid").text()  + "-" + $("#cuserid").text() + '&rnd=' + randm);
	    var imgsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + $("#ccoid").text() + "/" + $("#cuserid").text() + "/" + $("#ccoid").text() + "-" + $("#cuserid").text() + "?" + new Date().getTime();

      $("#userpict").attr('src',imgsrc);
      $("#updphoto").text('select new photo');
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#message'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#addphoto-form").ajaxForm(options);

	$('#updphoto_btn').click(function() {
		var sfile = $("#updphoto").text();
	  if (sfile == "select new photo")	{
      $('#updphoto').css('font-color', 'red');
      setError($('#message'),'Please select a file to upload');
      return false;
    } else    {
      $('#updphoto').css('font-color', 'default');
      clearError($('#message'));
    }
		// Check filetype and filesize
		var filename = $("#updphoto").text();
    var extensionAllowed=[".jpg",".jpeg",".gif",".png"];
    var i = filename.lastIndexOf('.');
	  var file_extension= (i < 0) ? '' : filename.substr(i);
		var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

		if(rfound == -1)		{
      $('#updphoto').css('font-color', 'red');
      setError($('#message'),'Photo filetype should be .jpg or .jpeg or .gif or .png format');
			$("#updphoto").text("select new photo");
      return false;
    } else    {
      $('#updphoto').css('font-color', 'default');
      clearError($('#message'));
		}
		var fsize = document.getElementById('pictfile').files[0].size;

		if ((fsize / 1024) > 2500)		{
      $('#updphoto').css('font-color', 'red');
      setError($('#message'),'Photo file should be less than 2MB');
			$("#updphoto").text("select new photo");
      return false;
    } else    {
      $('#updphoto').css('font-color', 'default');
      clearError($('#message'));
		}
    return true;
	});
});

