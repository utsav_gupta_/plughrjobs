$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('table.onbdlist').footable();
});

function def_emp()    {
	$('#emp-list').empty();
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj.userid).text(obj.username).appendTo('#emp-list');
		}
	}
}

function defMesgTable()    {
	$('table.onbdlist').data('footable').reset();
	$('table.onbdlist thead').append('<tr>');
	$('table.onbdlist thead tr').append('<th>Title</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Status</th>');
	$('table.onbdlist thead tr').append('<th data-hide="phone,tablet">Mark as completed</th>');
	$('table.onbdlist thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.onbdlist thead tr').append('</tr>');
	$('table.onbdlist').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		if (mesgObj.onbdacts)
			var glen = mesgObj.onbdacts.length;
		else
			var glen = 0;
		var rtable = $('table.onbdlist').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj.onbdacts[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.desc != null)
				newRow += obj.desc;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			switch (obj.status)     {
			  case '0':
    			newRow += "Incomplete";
    			break;
			  case '1':
    			newRow += "Completed";
    			break;
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.status == '0')
				newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Mark as Completed"></span></a>';
			else
				newRow += '-';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}


$(function () {
  $('table.onbdlist').footable().on('click', '.row-edit', function(e) {
    e.preventDefault();
    var rtable = $('table.onbdlist').data('footable');
    var row = $(this).parents('tr:first');
    updstatus(e, row[0]["rowIndex"]-1);
  });
});

function updstatus(evtObj, rowIndex, rowData) {
	var mesg = "Please reconfirm that Onboarding action is complete";
	alertify.set({ labels: {
		ok     : "I confirm",
		cancel : "Don't update"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var dataObj = JSON.parse($("#formdata").val());
			var docsObject = new Object();
			var s_emp = $('#emp-list').val();

			var rno = parseInt(rowIndex);
			onbdObj = dataObj.onbdacts;

			docsObject.coid = $('#ccoid').text();
			docsObject.userid = s_emp;
			docsObject.dbid = dataObj._id;
			docsObject.title = onbdObj[rno].title;
			docsObject.desc = onbdObj[rno].desc;

			$.ajax({
				type: "POST",
				url: '/258upd',
				data: docsObject,
				dataType: 'json',
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val("");
						$('#formdata').val(JSON.stringify(resp.data));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.onbdlist').data('footable');
						$('table.onbdlist tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.onbdlist tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);

						fillUserData();
						setSuccess($('#errtext1'),'Onboarding status updated');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
		      setError($('#errtext1'),err.text);
				}
			});
		  return false;
		}
	});
}

function get_list()		{
	var s_emp = $('#emp-list').val();
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp = s_emp;

	$.ajax({
		type: 'POST',
		url: '/258search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#onbdacts').val(JSON.stringify(resp.onbdacts));

				if ((resp.data.onbdacts == null) || (resp.data.onbdacts.length == 0))			{
					if (resp.onbdacts && resp.onbdacts.length > 0)		{
						var xlen = resp.onbdacts.length;
						var actsObj = [];
						for (var x=0; x<xlen; x++)			{
							actsObj.push({title:resp.onbdacts[x].title, desc:resp.onbdacts[x].desc, status:'0'});
						}
						var ondbObj = new Object;
						ondbObj.coid = formdataObj.coid;
						ondbObj.userid = formdataObj.emp;
						ondbObj.onbdacts = actsObj;
						$.ajax({
							type: "POST",
							url: '/258add',
							data: ondbObj,
							dataType: 'json',
							beforeSend:  function()   {
								startAjaxIcon();
							},
							success: function(resp) {
								$('#formdata').val(JSON.stringify(resp));
								// DELETE & RE-CREATE Table 
								var rtable = $('table.onbdlist').data('footable');

								$('table.onbdlist tbody tr').each(function() {
									rtable.removeRow($(this));
								});
								$('table.onbdlist tbody tr').each(function() {
									rtable.removeRow($(this));
								});
								$('#editrow').val(-1);
								fillUserData();
							},
						});
					}
				} else		{
					// DELETE & RE-CREATE Table 
					var rtable = $('table.onbdlist').data('footable');

					$('table.onbdlist tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.onbdlist tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					fillUserData();
				}
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_emp();
	defMesgTable();
	$('#search_btn').click(function() 			{
		get_list();
		$('#emp-list').prop('disabled', true);
		$('#search_btn').prop('disabled', true);
		return false;
	});
	$('#clear_btn').click(function() 			{
		// DELETE & RE-CREATE Table 
		var rtable = $('table.onbdlist').data('footable');
		$('table.onbdlist tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('table.onbdlist tbody tr').each(function() {
			rtable.removeRow($(this));
		});
		$('#emp-list').prop('disabled', false);
		$('#search_btn').prop('disabled', false);
		return false;
	});
	$('#add_btn').click(function() 			{
		//data_add();
		return false;
	});
});

