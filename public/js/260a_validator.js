$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

$(document).ready(function()	{
	var blogsObj = JSON.parse($('#formdata').val());
	if (blogsObj != null)    {
		var bpos = blogsObj[0].date.indexOf('T');
		var bdate = blogsObj[0].date.substr(0,bpos);
		$("#blogtitle").text(blogsObj[0].title);
		$("#blogauthor").text(blogsObj[0].author);
		$("#blogdate").text(bdate);
		$("#blogstext").text(blogsObj[0].stext);
		$("#blogftext").text(blogsObj[0].ftext);
	}
});

