$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode == 46)
		return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#isb').removeAttr('checked');
	$('#desc').val('');
	$('#type-list').prop('selectedIndex', 0);
	$('#calc-list').prop('selectedIndex', 0);
	$('#cval').val('');
	$('#ymax').val('');
	$('#catg-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.salcomp').data('footable').reset();
	$('table.salcomp thead').append('<tr>');
	$('table.salcomp thead tr').append('<th>Title</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Basic Salary</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Component Type</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Computation Type</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Computation Value</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Max limit per year</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Category</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.salcomp thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.salcomp thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.salcomp thead tr').append('</tr>');
	$('table.salcomp').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.salcomp').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.title != null)
				newRow += obj.title;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.desc != null)
				newRow += obj.desc;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.isbasic != null)
				newRow += obj.isbasic;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.sctype)     {
				case '1':
					newRow += 'Earnings';
					break;
				case '2':
					newRow += 'Deductions';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.calctype)     {
				case '1':
					newRow += '% of CTC';
					break;
				case '2':
					newRow += '% of Basic';
					break;
				case '3':
					newRow += 'Fixed Value';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.calcval != null)
				newRow += obj.calcval;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.ymax != null)
				newRow += obj.ymax;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.catg)     {
				case '1':
					newRow += 'Fixed Component';
					break;
				case '2':
					newRow += 'Variable Component';
					break;
			}
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/245newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.salcomp').data('footable');

			$('table.salcomp tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.salcomp tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.salcomp').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.salcomp').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#title').val(obj.title);
		if (obj.isbasic == 'true')
			$('#isb').prop('checked', true);
		$('#desc').val(obj.desc);
		$('#type-list').val(obj.sctype);
		$('#calc-list').val(obj.calctype);
		$('#cval').val(obj.calcval);
		$('#ymax').val(obj.ymax);
		$('#catg-list').val(obj.catg);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	if (evtObj && evtObj.target)    {
		var fdata = JSON.parse($('#formdata').val());

		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex,10)+1;
		var obj = fdata[rno-1];
		dataObj.dbid= obj._id;
		dataObj.title= obj.title;
		dataObj.isb= obj.isbasic;
		dataObj.desc= obj.desc;
		dataObj.type= obj.sctype;
		dataObj.calc= obj.calctype;
		dataObj.cval= obj.calcval;
		dataObj.ymax= obj.ymax;
		dataObj.catg= obj.catg;

		$.ajax({
			type: 'DELETE',
			url: '/245',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#totpages').val(JSON.stringify(resp.totpages));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.salcomp').data('footable');

					$('table.salcomp tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.salcomp tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					SetPagination();
					clearForm();
					setSuccess($('#errtext1'),'salcomp data deleted');
				} else  {
					setError($('#errtext1'),resp.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
	}
}
function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_isb = false;
	if ($('#isb').prop('checked'))
		var s_isb = true;

	var s_desc = $('#desc').val();
	if (s_desc == '')	{
		$('#desc').css('border-color', 'red');
		setError($('#errtext1'),'Please input Description');
		$('#desc').focus();
		return false;
	} else    {
		$('#desc').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_type = $('#type-list').val();
	if (s_type == null)	{
		$('#type-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Component Type');
		$('#type-list').focus();
		return false;
	} else    {
		$('#type-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_calc = $('#calc-list').val();
	if (s_calc == null)	{
		$('#calc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Computation Type');
		$('#calc-list').focus();
		return false;
	} else    {
		$('#calc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if (s_calc != '3')		{
		var s_cval = $('#cval').val();
		if (s_cval == '')	{
			$('#cval').css('border-color', 'red');
			setError($('#errtext1'),'Please input Computation Value');
			$('#cval').focus();
			return false;
		} else    {
			$('#cval').css('border-color', 'default');
			clearError($('#errtext1'));
		}
	}
	var s_ymax = $('#ymax').val();
	var s_catg = $('#catg-list').val();
	if (s_catg == null)	{
		$('#catg-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Category');
		$('#catg-list').focus();
		return false;
	} else    {
		$('#catg-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.title= s_title;
	formdataObj.isb= s_isb;
	formdataObj.desc= s_desc;
	formdataObj.type= s_type;
	formdataObj.calc= s_calc;
	formdataObj.cval= s_cval;
	formdataObj.ymax= s_ymax;
	formdataObj.catg= s_catg;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/245add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.salcomp').data('footable');

				$('table.salcomp tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.salcomp tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'salcomp data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/245upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.salcomp').data('footable');

				$('table.salcomp tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.salcomp tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'salcomp data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

