$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
	$(document).ajaxStart(function(){
			$("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
			$("#ajaxaction").hide();
	});
	$('#logout_btn').click(function() {
		$.ajax({
			url: '/adminlogout',
      beforeSend:  function()   {
        $("body").addClass("loading");
      },
			success: function(robj) { 
				window.location.href = robj.redirect;
			}
		});
		return false;
	});
	$('#updpwd_btn').click(function()   {
	    var s_coid = $('#comid').val();
	    if (s_coid == '')	{
		    $('#comid').css('border-color', 'red');
		    setError($('#errtext1'),'Please input Company Id');
		    $('#comid').focus();
		    return false;
	    } else    {
		    $('#comid').css('border-color', 'default');
		    clearError($('#errtext1'));
	    }
	    var s_userid = $('#userid').val();
	    if (s_userid == '')	{
		    $('#userid').css('border-color', 'red');
		    setError($('#errtext1'),'Please input User Id');
		    $('#userid').focus();
		    return false;
	    } else    {
		    $('#userid').css('border-color', 'default');
		    clearError($('#errtext1'));
	    }
      var npass1 = $("#stuPassword1").val();
      if (npass1 == "")	{
         $('#stuPassword1').css('border-color', 'red');
         setError($('#errtext1'),'Please input new password');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass1.length < 8)   {
         $('#stuPassword1').css('border-color', 'red');
         setError($('#errtext1'),'Password should be atleast 8 characters');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      var npass2 = $("#stuPassword2").val();
      if (npass2 == "")	{
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Please input new password twice');
         return false;
      } else    {
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass2.length < 8)   {
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Password should be atleast 8 characters');
         return false;
      } else    {
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }
      if (npass1 != npass2)	{
         $('#stuPassword1').css('border-color', 'red');
         $('#stuPassword2').css('border-color', 'red');
         setError($('#errtext1'),'Passwords are not same');
         return false;
      } else    {
         $('#stuPassword1').css('border-color', 'default');
         $('#stuPassword2').css('border-color', 'default');
         clearError($('#errtext1'));
      }

      var joinObject = new Object();
      joinObject.coid = s_coid;
      joinObject.userid = s_userid;
      joinObject.npass = npass1;
      $.ajax({
         type: "POST",
         url: '/108',
         data: joinObject,
         dataType: 'json',
         beforeSend:  function()   {
            startAjaxIcon();
         },
         success: function(robj) {
            if (robj.err == 0)  {
               setSuccess($('#errtext1'),robj.text);
               $("#comid").val("");
               $("#userid").val("");
               $("#stuPassword1").val("");
               $("#stuPassword2").val("");
            } else
               setError($('#errtext1'),robj.text);
               stopAjaxIcon();
            },
         error: function(eobj) {
            setError($('#errtext1'),eobj.text);
            stopAjaxIcon();
         }
      });
      return false;
   });
});

