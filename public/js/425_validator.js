$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selectfile_btn').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').val(fileName);
  });
  $('table.userdocs').footable();
});

function clearForm()		{
	$('#dbid').val('');
	$('#catg-list').prop('selectedIndex', 1);
	$('#desc').val('');
	$('#selfile').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
}

function defMesgTable()    {
	$('table.userdocs').data('footable').reset();
	$('table.userdocs thead').append('<tr>');
	$('table.userdocs thead tr').append('<th>Description</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">Category</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">Filename</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">View</th>');
	$('table.userdocs thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.userdocs thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.userdocs thead tr').append('</tr>');
	$('table.userdocs').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.userdocs').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.docdesc != null)
				newRow += obj.docdesc;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.catg)     {
				case '1':
					newRow += 'Educational';
					break;
				case '2':
					newRow += 'Work Experiences';
					break;
				case '3':
					newRow += 'Trainings & Certifications';
					break;
				case '4':
					newRow += 'Personal Documents';
					break;
				case '5':
					newRow += 'Investment Declarations';
					break;
				case '6':
					newRow += 'Misc Documents';
					break;
				case '11':
					newRow += 'Official Documents';
					break;
				case '12':
					newRow += 'Special Documents';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			if (obj.docfile != null)
				newRow += obj.docfile;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-view" href="#"><span class="glyphicon glyphicon-eye-open" title="View Document"></span></a>';

			if (parseInt(obj.catg) < 10)		{
				newRow += '</td><td>';
				newRow += '<a class="row-delete" href="#"><span class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			} else	{
				newRow += '</td><td>';
				newRow += '-';
			}
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/425newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.userdocs').data('footable');

			$('table.userdocs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.userdocs tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

$(function () {
  $('table.userdocs').footable().on('click', '.row-view', function(e) {
    e.preventDefault();
    var rtable = $('table.userdocs').data('footable');
    var row = $(this).parents('tr:first');
    viewDoc(e, row[0]["rowIndex"]-1);
  });
  $('table.userdocs').footable().on('click', '.row-delete', function(e) {
    e.preventDefault();
    var rtable = $('table.userdocs').data('footable');
    var row = $(this).parents('tr:first');
    delDoc(e, row[0]["rowIndex"]-1);
  });
});

function viewDoc(evtObj, rowIndex)	{
  if (evtObj && evtObj.target)    {
  	var dataObj = JSON.parse($("#formdata").val());
    var docfile = dataObj[rowIndex].docfile;
		var xcoid = $('#ccoid').text();
		var xuserid = $('#cuserid').text();

    var docsrc = 'https://s3-ap-southeast-1.amazonaws.com/plughr-uploads/' + xcoid + "/" + xuserid + "/" + docfile;
    window.open(docsrc);

    //window.open('/uploads/'+ xcoid + "/" + xuserid + "/" + docfile);
    return false;
  }
}
function delDoc(evtObj, rowIndex, rowData) {
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var dataObj = JSON.parse($("#formdata").val());
			var docsObject = new Object();
			docsObject.coid = $('#ccoid').text();
			docsObject.userid = $('#cuserid').text();
			docsObject.catg = dataObj[rowIndex].catg;
			docsObject.desc = dataObj[rowIndex].docdesc;
			docsObject.filename = dataObj[rowIndex].docfile;
			docsObject.dbid= dataObj[rowIndex]._id;

			$.ajax({
				type: "DELETE",
				url: '/userdocsul',
				data: docsObject,
				dataType: 'json',
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.userdocs').data('footable');

						$('table.userdocs tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.userdocs tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'Document deleted');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
		      setError($('#errtext1'),err.text);
				}
			});
		  return false;
		}
	});
}

function GetValidInputs()		{
	var s_catg = $('#catg-list').val();
	if (s_catg == null)	{
		$('#catg-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Category');
		$('#catg-list').focus();
		return false;
	} else    {
		$('#catg-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_desc = $('#desc').val();
	if (s_desc == '')	{
		$('#desc').css('border-color', 'red');
		setError($('#errtext1'),'Please input Description');
		$('#desc').focus();
		return false;
	} else    {
		$('#desc').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_filename = $('#filename').val();
	if (s_filename == '')	{
		$('#filename').css('border-color', 'red');
		setError($('#errtext1'),'Please input Select File');
		$('#filename').focus();
		return false;
	} else    {
		$('#filename').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var sfile = $("#selfile").val();
  if (sfile == "")	{
    $('#selfile').css('border-color', 'red');
    setError($('#errtext1'),'Please select a file to upload');
    return false;
  } else    {
    $('#selfile').css('border-color', 'default');
    clearError($('#errtext1'));
  }

	// Check filetype and filesize
	var filename = $("#selfile").val();
  var extensionAllowed=[".jpg",".jpeg",".tiff",".png",".doc",".docx",".pdf"];
  var i = filename.lastIndexOf('.');
  var file_extension= (i < 0) ? '' : filename.substr(i);
	var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

	if(rfound == -1)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),' Only MS Word, PDF documents and image formats (JPG or PNG or TIFF) are allowed');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}
	var fsize = document.getElementById('docfile').files[0].size;
	if ((fsize / 1024) > 5500)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),'Document file should be less than 5MB');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.catg= s_catg;
	formdataObj.desc= s_desc;
	formdataObj.filename= s_filename;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
  $('#submit_btn').click();
  return true;
}

$(document).ready(function()	{
  var options = {
    beforeSend: function()  {
      clearError($('#errtext1'));
      startAjaxIcon();
    },
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.userdocs').data('footable');

				$('table.userdocs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.userdocs tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'New Document uploaded');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
  	complete: function(response)    {
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#errtext1'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#adddocs-form").ajaxForm(options);

	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
});

