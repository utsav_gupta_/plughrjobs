google.setOnLoadCallback(drawChart);
function drawChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Manager');
  data.addColumn('string', 'ToolTip');

  data.addRows([
    ['CEO / President', '', 'Company Owner'],
    ['COO', 'CEO / President', 'Chief Operating Officer'],
    ['CFO', 'CEO / President', 'Chief Financial Officer'],
    ['CTO', 'CEO / President', 'Chief Technology Officer'],
    ['CMO', 'CEO / President', 'Chief Marketing Officer'],
    ['CPO', 'CEO / President', 'Chief People Officer']
  ]);

  var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
  chart.draw(data, {allowHtml:true});
}

