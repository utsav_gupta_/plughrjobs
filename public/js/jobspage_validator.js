/* -----------------------------------------------
usertype 1 = site admin
usertype 2 = Site users
usertype 3 = Company admins
usertype 4 = Company users
usertype 5 = Candidates
------------------------------------------------- */

$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});
$(document).ready(function()	{
	var dataObj = JSON.parse($('#formdata').val());
	var obj = dataObj[0];
  var arrwdays = ['Full time', 'Few days a week', 'Work from home']
  var curncy = ["","USA Dollar","IND Rupees","UK Pounds","EUR","Canadian Dollar","Singapore Dollar","Australian Dollar","UAE Dirhams","Malaysia Ringgit","Thailand Baht"];

 	$('#title').text(obj.title);
 	if (obj.company)    {
    $('#empl').text(obj.company.companyname + " ,  " + obj.company.city);
    $('#aboute').text(obj.company.about);
 	}

  $('#wdays').text(arrwdays[obj.workdays-1]);
  if (obj.eqtymin != '' && parseFloat(obj.eqtymin) != 0 && obj.eqtymax != '' && parseFloat(obj.eqtymax) != 0 )
    var sstr = " Annual " + obj.comp + " ( " + curncy[obj.curr] + " )" + " ,  Equity " + obj.eqtymin + "% to " + obj.eqtymax +"%";
  else
    var sstr = " Annual " + obj.comp + " ( " + curncy[obj.curr] + " )";
  $('#salry').text(sstr);

  $('#aboutj').text(obj.jdesc);

  $('#reqpro').text(obj.skills);

	$('#apply_btn').click(function() {
  	$("#loginUserId").val('');
    $('#loginUserId').css('border-color', 'default');
    $("#loginPassword").val('');
    $('#loginPassword').css('border-color', 'default');
    clearError($('#errtext3'));
    $('#loginModal').modal();  
    $('#errtext1').text('Please login to apply for this job');
	});

  $('#share_btn').click(function() {
    var url = "https://www.facebook.com/sharer/sharer.php?u=https://www.workhalf.com/jobpage?jobid=" + obj._id;
    document.getElementById('share_btn').setAttribute("href", url);
  });

	$('#home1-signin').click(function() {
		$("#loginUserId").val('');
    $('#loginUserId').css('border-color', 'default');
		$("#loginPassword").val('');
    $('#loginPassword').css('border-color', 'default');
    clearError($('#errtext3'));
	  $('#loginModal').modal();
		return false;
	});
	$('#signin_btn').click(function() {
    var suserid = $("#loginUserId").val();
    if (suserid == "")   {
      $('#loginUserId').css('border-color', 'red');
      setError($('#errtext3'),'Please input email id');
      return false;
    } else    {
      $('#loginUserId').css('border-color', 'default');
      clearError($('#errtext3'));
    }
		var lastAtPos = suserid.lastIndexOf('@');
		var lastDotPos = suserid.lastIndexOf('.');
		var result = (lastAtPos < lastDotPos && lastAtPos > 0 && suserid.indexOf('@@') == -1 && lastDotPos > 2 && (suserid.length - lastDotPos) > 2);
		if (!result)	{
       $('#loginUserId').css('border-color', 'red');
       setError($('#errtext3'),'Please input valid email id');
       return false;
    } else    {
       $('#loginUserId').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var spwd = $("#loginPassword").val();
    if (!spwd)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Please input password');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }
    if (spwd.length < 8)   {
       $('#loginPassword').css('border-color', 'red');
       setError($('#errtext3'),'Password should be atleast 8 characters');
       return false;
    } else    {
       $('#loginPassword').css('border-color', 'default');
       clearError($('#errtext3'));
    }

		var loginObject = new Object();
		loginObject.userid = suserid;
		loginObject.pwd = spwd;

		//alert(JSON.stringify(loginObject));

		$.ajax({
		  url: '/login',
		  data: loginObject,
		  dataType: 'json',
      beforeSend:  function()   {
        startAjaxIcon();
      },
      success : function(robj) { 
        //alert(JSON.stringify(robj));
        if (robj.err == false)    {
          //alert('1');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 0)    {
          //alert('2');
	        window.location.href = robj.redirect;
	      } else if (robj.err == 99)    {
          //alert('3');
	        window.location.href = robj.redirect+"?coid=" + robj.coid+"&userid="+ robj.userid;
        } else    {
          //alert(JSON.stringify(robj));
		      setError($('#errtext3'),robj.text);
          stopAjaxIcon();
        }
          //alert('5');
			},
			error: function(eobj) {
				setError($('#errtext3'),eobj.text);
        stopAjaxIcon();
			}
		});
		return false;
	});
});

