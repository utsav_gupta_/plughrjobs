$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
});

function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#oname').val('');
	$('#aline1').val('');
	$('#aline2').val('');
	$('#city').val('');
	$('#pincode').val('');
	$('#phone1').val('');
	$('#phone2').val('');
	$('#faxno').val('');
	$('#ptno').val('');
	$('#se').val('');
	$('#cperson').val('');
	$('#chq-list').prop('selectedIndex', 0);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.locations').data('footable').reset();
	$('table.locations thead').append('<tr>');
	$('table.locations thead tr').append('<th>Office Name</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Address</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet"></th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">City</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Pincode</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Phone #1</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Phone #2</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Fax No</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Contact Person</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Corp HQ</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.locations thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.locations thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.locations thead tr').append('</tr>');
	$('table.locations').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.locations').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.oname != null)
				newRow += obj.oname;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.aline1 != null)
				newRow += obj.aline1;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.aline2 != null)
				newRow += obj.aline2;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.city != null)
				newRow += obj.city;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.pincode != null)
				newRow += obj.pincode;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.phone1 != null)
				newRow += obj.phone1;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.phone2 != null)
				newRow += obj.phone2;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.faxno != null)
				newRow += obj.faxno;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.cperson != null)
				newRow += obj.cperson;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			switch (obj.chq)     {
				case '1':
					newRow += 'Yes';
					break;
				case '2':
					newRow += 'No';
					break;
			}
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/209newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.locations').data('footable');

			$('table.locations tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.locations tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.locations').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.locations').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#oname').val(obj.oname);
		$('#aline1').val(obj.aline1);
		$('#aline2').val(obj.aline2);
		$('#city').val(obj.city);
		$('#pincode').val(obj.pincode);
		$('#phone1').val(obj.phone1);
		$('#phone2').val(obj.phone2);
		$('#faxno').val(obj.faxno);
		$('#ptno').val(obj.ptno);
		$('#se').val(obj.se);
		$('#cperson').val(obj.cperson);
		$('#chq-list').prop('selectedIndex', (obj.chq-1));

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
		var fdata = JSON.parse($('#formdata').val());
		var dataObj = new Object();
		dataObj.coid = $('#ccoid').text();
		dataObj.user = $('#cuser').text();
		var rno = parseInt(rowIndex);
		var obj = fdata[rno];
		dataObj.dbid= obj._id;
		dataObj.oname= obj.oname;
		dataObj.aline1= obj.aline1;
		dataObj.aline2= obj.aline2;
		dataObj.city= obj.city;
		dataObj.pincode= obj.pincode;
		dataObj.phone1= obj.phone1;
		dataObj.phone2= obj.phone2;
		dataObj.faxno= obj.faxno;
		dataObj.cperson= obj.cperson;
		dataObj.chq= obj.chq;
		dataObj.ptno= obj.ptno;
		dataObj.se= obj.se;

		$.ajax({
			type: 'DELETE',
			url: '/209',
			data: dataObj,
			dataType: 'json',
			beforeSend:  function()   {
				startAjaxIcon();
			},
			success: function(resp) {
				if (!resp.err)   {
					$('#formdata').val(JSON.stringify(resp.data));
					$('#totpages').val(JSON.stringify(resp.totpages));

					// DELETE & RE-CREATE Table 
					var rtable = $('table.locations').data('footable');

					$('table.locations tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('table.locations tbody tr').each(function() {
						rtable.removeRow($(this));
					});
					$('#editrow').val(-1);
					//defMesgTable();
					fillUserData();
					SetPagination();
					clearForm();
					setSuccess($('#errtext1'),'locations data deleted');
				} else  {
					setError($('#errtext1'),o.text);
				}
				stopAjaxIcon();
			},
			error: function(err) {
				setError($('#errtext1'),err.responseText);
				stopAjaxIcon();
			}
		});
		return false;
		}
	});
}
function GetValidInputs()		{
	var s_oname = $('#oname').val();
	if (s_oname == '')	{
		$('#oname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Office Name');
		$('#oname').focus();
		return false;
	} else    {
		$('#oname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_aline1 = $('#aline1').val();
	if (s_aline1 == '')	{
		$('#aline1').css('border-color', 'red');
		setError($('#errtext1'),'Please input Address');
		$('#aline1').focus();
		return false;
	} else    {
		$('#aline1').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_aline2 = $('#aline2').val();
/*
	if (s_aline2 == '')	{
		$('#aline2').css('border-color', 'red');
		setError($('#errtext1'),'Please input ');
		$('#aline2').focus();
		return false;
	} else    {
		$('#aline2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
*/
	var s_city = $('#city').val();
	if (s_city == '')	{
		$('#city').css('border-color', 'red');
		setError($('#errtext1'),'Please input City');
		$('#city').focus();
		return false;
	} else    {
		$('#city').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_pincode = $('#pincode').val();
	if (s_pincode == '')	{
		$('#pincode').css('border-color', 'red');
		setError($('#errtext1'),'Please input Pincode');
		$('#pincode').focus();
		return false;
	} else    {
		$('#pincode').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_phone1 = $('#phone1').val();
/*
	if (s_phone1 == '')	{
		$('#phone1').css('border-color', 'red');
		setError($('#errtext1'),'Please input Phone #1');
		$('#phone1').focus();
		return false;
	} else    {
		$('#phone1').css('border-color', 'default');
		clearError($('#errtext1'));
	}
*/
	var s_phone2 = $('#phone2').val();
/*
	if (s_phone2 == '')	{
		$('#phone2').css('border-color', 'red');
		setError($('#errtext1'),'Please input Phone #2');
		$('#phone2').focus();
		return false;
	} else    {
		$('#phone2').css('border-color', 'default');
		clearError($('#errtext1'));
	}
*/
	var s_faxno = $('#faxno').val();
/*
	if (s_faxno == '')	{
		$('#faxno').css('border-color', 'red');
		setError($('#errtext1'),'Please input Fax No');
		$('#faxno').focus();
		return false;
	} else    {
		$('#faxno').css('border-color', 'default');
		clearError($('#errtext1'));
	}
*/
	var s_cperson = $('#cperson').val();
/*
	if (s_cperson == '')	{
		$('#cperson').css('border-color', 'red');
		setError($('#errtext1'),'Please input Contact Person');
		$('#cperson').focus();
		return false;
	} else    {
		$('#cperson').css('border-color', 'default');
		clearError($('#errtext1'));
	}
*/
	var s_chq = $('#chq-list').val();
	if (s_chq == null)	{
		$('#chq-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Corp HQ');
		$('#chq-list').focus();
		return false;
	} else    {
		$('#chq-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	var s_ptno = $('#ptno').val();
	var s_se = $('#se').val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.oname= s_oname;
	formdataObj.aline1= s_aline1;
	formdataObj.aline2= s_aline2;
	formdataObj.city= s_city;
	formdataObj.pincode= s_pincode;
	formdataObj.phone1= s_phone1;
	formdataObj.phone2= s_phone2;
	formdataObj.faxno= s_faxno;
	formdataObj.ptno= s_ptno;
	formdataObj.se= s_se;
	formdataObj.cperson= s_cperson;
	formdataObj.chq= s_chq;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/209add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.locations').data('footable');

				$('table.locations tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.locations tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'locations data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/209upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.locations').data('footable');

				$('table.locations tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.locations tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'locations data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

