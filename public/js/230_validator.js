/*
lname
lper-list
ldays
lctr
etype-list
probuser
maxl
negl
alimit
maxtr
preh
inth
such
*/

$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#etype-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth : true,
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode == 46) 			// for decimal .
		return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#lname').val('');
	$('#lper-list').prop('selectedIndex', 0);
	$('#ldays').val('');
	$('#lctr').val('');
	$('#etype-list').select2('val', null);
	$('#probuser').prop( "checked", false );
	$('#preh').prop( "checked", false );
	$('#inth').prop( "checked", false );
	$('#such').prop( "checked", false );
	$('#maxl').val('');
	$('#negl').val('');
	$('#alimit').val('');
	$('#maxtr').val('');
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function defMesgTable()    {
	$('table.leavetypes').data('footable').reset();
	$('table.leavetypes thead').append('<tr>');
	$('table.leavetypes thead tr').append('<th>Name</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">User types</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">On probation</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Accrual period</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Accrual days</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Consecutive</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Negative</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Carry over</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Transfers</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.leavetypes thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.leavetypes thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.leavetypes thead tr').append('</tr>');
	$('table.leavetypes').footable();
}

function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.leavetypes').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.lname != null)
				newRow += obj.lname;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.puser == null)
				newRow += '-';
			else		{
				var elen = obj.etype.length;
				if (elen != 0)		{
					for (var x=0;x<elen; x++)		{
						var et = obj.etype[x];
						switch (et)     {
							case '1':
								newRow += 'Employee,';
								break;
							case '2':
								newRow += 'Trainee,';
								break;
							case '3':
								newRow += 'Intern,';
								break;
							case '4':
								newRow += 'Contractor,';
								break;
						}
					}
				} else	{
					newRow += '-';
				}
			}

			newRow += '<td>';
			if (obj.puser != null)		{
				if (obj.puser == "true")
					var ptext = "can avail";
				else
					var ptext = "can't avail";
				newRow += ptext;
			} else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.lper != null)
				switch (obj.lper)			{
					case '1':
						newRow += "Fixed days per year";
						break;
					case '2':
						newRow += "Quarterly";
						break;
					case '3':
						newRow += "Monthly";
						break;
					case '4':
						newRow += "Weekly";
						break;
				}
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.ldays != null)
				newRow += obj.ldays;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.maxl != '')
				newRow += obj.maxl;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.negl != '')
				newRow += obj.negl;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.alimit != '')
				newRow += obj.alimit;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.maxtr != '')
				newRow += obj.maxtr;
			else
				newRow += '-';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}

function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/230newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.leavetypes').data('footable');

			$('table.leavetypes tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.leavetypes tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.leavetypes').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.leavetypes').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex);
		var obj = dataObj[rno];

		$('#dbid').val(obj._id);
		$('#lname').val(obj.lname);
		$('#lper-list').val(obj.lper);
		$('#ldays').val(obj.ldays);
		$('#lctr').val(obj.lcriteria);
		$('#etype-list').select2('val', obj.etype);
		if (obj.puser == 'true')
			$('#probuser').prop( "checked", true );
		else
			$('#probuser').prop( "checked", false );
		if (obj.preh == 'true')
			$('#preh').prop( "checked", true );
		else
			$('#preh').prop( "checked", false );
		if (obj.inth == 'true')
			$('#inth').prop( "checked", true );
		else
			$('#inth').prop( "checked", false );
		if (obj.such == 'true')
			$('#such').prop( "checked", true );
		else
			$('#such').prop( "checked", false );
		$('#maxl').val(obj.maxl);
		$('#negl').val(obj.negl);
		$('#alimit').val(obj.alimit);
		$('#maxtr').val(obj.maxtr);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	var mesg = "Do you REALLY want to DELETE?";
	alertify.set({ labels: {
		ok     : "Delete",
		cancel : "Don't Delete"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var fdata = JSON.parse($('#formdata').val());
			var dataObj = new Object();
			dataObj.coid = $('#ccoid').text();
			dataObj.user = $('#cuser').text();
			var rno = parseInt(rowIndex);
			var obj = fdata[rno];
			dataObj.dbid= obj._id;
			dataObj.lname= obj.lname;
			dataObj.lctr= obj.lcriteria;
			dataObj.lper= obj.lper;
			dataObj.ldays= obj.ldays;

			$.ajax({
				type: 'DELETE',
				url: '/230',
				data: dataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						$('#formdata').val(JSON.stringify(resp.data));
						$('#totpages').val(JSON.stringify(resp.totpages));

						// DELETE & RE-CREATE Table 
						var rtable = $('table.leavetypes').data('footable');

						$('table.leavetypes tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('table.leavetypes tbody tr').each(function() {
							rtable.removeRow($(this));
						});
						$('#editrow').val(-1);
						//defMesgTable();
						fillUserData();
						SetPagination();
						clearForm();
						setSuccess($('#errtext1'),'leavetypes data deleted');
					} else  {
						setError($('#errtext1'),o.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

function GetValidInputs()		{
	var s_lname = $('#lname').val();
	if (s_lname == '')	{
		$('#lname').css('border-color', 'red');
		setError($('#errtext1'),'Please input Name');
		$('#lname').focus();
		return false;
	} else    {
		$('#lname').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_etype = $('#etype-list').val();
	if (s_etype == null)	{
		$('#etype-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select atleast one employee type');
		$('#etype-list').focus();
		return false;
	} else    {
		$('#etype-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ldays = $('#ldays').val();
	if (s_ldays == '')	{
		$('#ldays').css('border-color', 'red');
		setError($('#errtext1'),'Please input Permitted Days');
		$('#ldays').focus();
		return false;
	} else    {
		$('#ldays').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_lctr = $('#lctr').val();
	if (s_lctr == '')	{
		$('#lctr').css('border-color', 'red');
		setError($('#errtext1'),'Please input Availing Criteria');
		$('#lctr').focus();
		return false;
	} else    {
		$('#lctr').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_lper = $('#lper-list').val();
	var s_puser = $("#probuser").prop('checked');
	var s_preh = $("#preh").prop('checked');
	var s_inth = $("#inth").prop('checked');
	var s_such = $("#such").prop('checked');
	var s_maxl = $("#maxl").val();
	var s_negl = $("#negl").val();
	var s_alimit = $("#alimit").val();
	var s_maxtr = $("#maxtr").val();

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();

	formdataObj.lname= s_lname;
	formdataObj.lper= s_lper;
	formdataObj.ldays= s_ldays;
	formdataObj.lctr= s_lctr;
	formdataObj.etype = s_etype;
	formdataObj.puser = s_puser;
	formdataObj.maxl = s_maxl;
	formdataObj.negl = s_negl;
	formdataObj.alimit = s_alimit;
	formdataObj.maxtr = s_maxtr;
	formdataObj.preh = s_preh;
	formdataObj.inth = s_inth;
	formdataObj.such = s_such;

	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/230add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.leavetypes').data('footable');

				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'leavetypes data added');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	//alert(JSON.stringify(formdataObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/230upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.leavetypes').data('footable');

				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.leavetypes tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'leavetypes data updated');
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

