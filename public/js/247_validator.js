$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
	$('#inc-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
	$('#ded-list').select2({
		placeholder: 'Select all applicable options',
		allowClear: true,
		dropdownAutoWidth: true
	});
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function clearForm()		{
	$('#dbid').val('');
	$('#title').val('');
	$('#desc').val('');
	$('#inc-list').select2('val', null);
	$('#ded-list').select2('val', null);
	clearError($('#errtext1'));
	document.getElementById('add_btn').disabled = false;
	document.getElementById('upd_btn').disabled = true;
	document.getElementById('cancel_btn').disabled = true;
}

function def_sc()    {
	var xObj = JSON.parse($('#scdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.sctype == '1')
				$('<option>').val(obj._id).text(obj.title).appendTo('#inc-list');
			else
				$('<option>').val(obj._id).text(obj.title).appendTo('#ded-list');
		}
	}
}
function defMesgTable()    {
	$('table.salstru').data('footable').reset();
	$('table.salstru thead').append('<tr>');
	$('table.salstru thead tr').append('<th>Title</th>');
	$('table.salstru thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.salstru thead tr').append('<th data-hide="phone,tablet">Earnings</th>');
	$('table.salstru thead tr').append('<th data-hide="phone,tablet">Deductions</th>');
	$('table.salstru thead tr').append('<th data-hide="phone,tablet">Edit</th>');
	$('table.salstru thead tr').append('<th data-hide="phone,tablet">Delete</th>');
	$('table.salstru thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.salstru thead tr').append('</tr>');
	$('table.salstru').footable();
}
function fillUserData()		{
	var sel3Obj = JSON.parse($('#scdata').val());
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.salstru').data('footable');
		var rcount = 0;
		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';
			newRow += '<td>';
			if (obj.sstitle != null)
				newRow += obj.sstitle;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			if (obj.ssdesc != null)
				newRow += obj.ssdesc;
			else
				newRow += '**No Data**';
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.ssearnings.length;
			var dblen = sel3Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.ssearnings[temp];
					if (inx == selx._id)	{
						newRow += selx.title+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			var olen = obj.ssdeductions.length;
			var dblen = sel3Obj.length;
			for (var ctr = 0; ctr < dblen; ctr++)		{
				var selx = sel3Obj[ctr];
				for (var temp = 0;temp < olen; temp++)		{
					var inx = obj.ssdeductions[temp];
					if (inx == selx._id)	{
						newRow += selx.title+ ', ';
						break;
					}
				}
			}
			newRow += '</td>';
			newRow += '<td>';
			newRow += '<a class="row-edit" href="#"><span id = "e'+i+'" class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
			newRow += '</td><td>';
			newRow += '<a class="row-delete" href="#"><span id = "d'+i+'" class="glyphicon glyphicon-trash" title="Delete"></span></a>';
			newRow += '</td>';
			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/247newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.salstru').data('footable');

			$('table.salstru tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.salstru tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.salstru').footable().on('click', '.row-edit', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		editdetails(e, rowindex);
	});
	$('table.salstru').footable().on('click', '.row-delete', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row[1];
		clearError($('#errtext1'));
		data_delete(e, rowindex);
	});
});

function editdetails(evtObj, rowIndex)	{
	if (evtObj && evtObj.target)    {
		var dataObj = JSON.parse($('#formdata').val());
		$('#editrow').val(rowIndex);

		var rno = parseInt(rowIndex,10)+1;
		var obj = dataObj[rno-1];

		$('#dbid').val(obj._id);
		$('#title').val(obj.sstitle);
		$('#desc').val(obj.ssdesc);
		$('#inc-list').select2('val', obj.ssearnings);
		$('#ded-list').select2('val', obj.ssdeductions);

		document.getElementById('add_btn').disabled = true;
		document.getElementById('upd_btn').disabled = false;
		document.getElementById('cancel_btn').disabled = false;
	}
}

function data_delete(evtObj, rowIndex) { 
	if (evtObj && evtObj.target)    {
		var mesg = "Do you REALLY want to DELETE?";
		alertify.set({ labels: {
			ok     : "Delete",
			cancel : "Don't Delete"
			} 
		});
		alertify.set({ buttonFocus: "cancel" });
		alertify.set({ buttonReverse: true });
		alertify.confirm(mesg, function (e) {
			if (e) {
				var fdata = JSON.parse($('#formdata').val());

				var dataObj = new Object();
				dataObj.coid = $('#ccoid').text();
				dataObj.user = $('#cuser').text();
				var rno = parseInt(rowIndex,10)+1;
				var obj = fdata[rno-1];
				dataObj.dbid= obj._id;
				dataObj.title= obj.sstitle;
				dataObj.desc= obj.ssdesc;
				dataObj.inc= obj.ssearnings;
				dataObj.ded= obj.ssdeductions;

				$.ajax({
					type: 'DELETE',
					url: '/247',
					data: dataObj,
					dataType: 'json',
					beforeSend:  function()   {
						startAjaxIcon();
					},
					success: function(resp) {
						if (!resp.err)   {
							$('#formdata').val(JSON.stringify(resp.data));
							$('#totpages').val(JSON.stringify(resp.totpages));

							// DELETE & RE-CREATE Table 
							var rtable = $('table.salstru').data('footable');

							$('table.salstru tbody tr').each(function() {
								rtable.removeRow($(this));
							});
							$('table.salstru tbody tr').each(function() {
								rtable.removeRow($(this));
							});
							$('#editrow').val(-1);
							//defMesgTable();
							fillUserData();
							SetPagination();
							clearForm();
							setSuccess($('#errtext1'),'salstru data deleted');
						} else  {
							setError($('#errtext1'),resp.text);
						}
						stopAjaxIcon();
					},
					error: function(err) {
						setError($('#errtext1'),err.responseText);
						stopAjaxIcon();
					}
				});
				return false;
			}
		});
	}
}

function GetValidInputs()		{
	var s_title = $('#title').val();
	if (s_title == '')	{
		$('#title').css('border-color', 'red');
		setError($('#errtext1'),'Please input Title');
		$('#title').focus();
		return false;
	} else    {
		$('#title').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_desc = $('#desc').val();
	if (s_desc == '')	{
		$('#desc').css('border-color', 'red');
		setError($('#errtext1'),'Please input Description');
		$('#desc').focus();
		return false;
	} else    {
		$('#desc').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_inc = $('#inc-list').val();
	if (s_inc == null)	{
		$('#inc-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Add Earnings Components');
		$('#inc-list').focus();
		return false;
	} else    {
		$('#inc-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_ded = $('#ded-list').val();
	if (s_ded == null)	{
		$('#ded-list').css('border-color', 'red');
		setError($('#errtext1'),'Please input Add Deductions components');
		$('#ded-list').focus();
		return false;
	} else    {
		$('#ded-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.dbid= $('#dbid').val();
	formdataObj.title= s_title;
	formdataObj.desc= s_desc;
	formdataObj.inc= s_inc;
	formdataObj.ded= s_ded;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/247add',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.salstru').data('footable');

				$('table.salstru tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.salstru tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'salstru data added');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

function data_upd()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

	$.ajax({
		type: 'POST',
		url: '/247upd',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.salstru').data('footable');

				$('table.salstru tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.salstru tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				clearForm();
				setSuccess($('#errtext1'),'salstru data updated');
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
	def_sc();
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#add_btn').click(function() 			{
		data_add();
		return false;
	});
	$('#cancel_btn').click(function()    {
		clearForm();
		document.getElementById('add_btn').disabled = false;
		document.getElementById('upd_btn').disabled = true;
		document.getElementById('cancel_btn').disabled = true;
		return false;
	});
	$('#upd_btn').click(function() 			{
		data_upd();
		return false;
	});
});

