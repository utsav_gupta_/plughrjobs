$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
  $('#selectfile_btn').click(function(){
    $('#docfile').click();
  });
  $('#docfile').change(function () {
    var fileName = $(this).val();
    $('#selfile').val(fileName);
  });
});

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
		var ucoid = $('#ccoid').text();
		var fname = formdataObj.indcfname;

		if (!fname)			{
	    var imgsrc = ('');
	    $("#cfile").text("No Induction presentation uploaded so far");
	    $("#cfile").addClass('h1');
	    $("#cfile").addClass('text-danger');
		} else	{
		  var i = fname.lastIndexOf('.');
			var file_extension = (i < 0) ? '' : fname.substr(i);
			var file_name = (i < 0) ? '' : fname.substr(0,i);

			var src_path = "https://sg1b-powerpoint.officeapps.live.com/p/PowerPointFrame.aspx?PowerPointView=SlideShowView&ui=en-US&rs=en-US&WOPISrc=http%3A%2F%2Fsg1b-15-view-wopi%2Ewopi%2Elive%2Enet%3A808%2Foh%2Fwopi%2Ffiles%2F%40%2FwFileId%3FwFileId%3Dhttps%253A%252F%252Fs3%252Dap%252Dsoutheast%252D1%252Eamazonaws%252Ecom%253A443%252Fplughr-uploads%252F" + ucoid + "%252F" + file_name + file_extension + "&&access_token_ttl=0&wdSlideId=256&wdModeSwitchTime=1428168330942";
	    var imgsrc = (src_path);
		}
    $("#indcdoc").attr('src',imgsrc);
    //$("#cfile").text(fname);
    $("#currfile").val(fname);
		$('#selfile').val('');
	}
}

function GetValidInputs()		{
	var s_filename = $('#selfile').val();

	if (s_filename == '')	{
		$('#selfile').css('border-color', 'red');
		setError($('#errtext1'),'Please input File');
		$('#selectfile_btn').focus();
		return false;
	} else    {
		$('#selfile').css('border-color', 'default');
		clearError($('#errtext1'));
	}

	// Check filetype and filesize
	var filename = $("#selfile").val();
  var extensionAllowed=[".ppt",".pptx"];
  var i = filename.lastIndexOf('.');
  var file_extension= (i < 0) ? '' : filename.substr(i);
	var rfound = jQuery.inArray(file_extension.toLowerCase(), extensionAllowed);

	if (rfound == -1)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),' Only Powerpoint documents (.ppt or .pptx) are allowed');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}
	var fsize = document.getElementById('docfile').files[0].size;
	if ((fsize / 1024) > 10500)		{
    $('#selfile').css('font-color', 'red');
    setError($('#errtext1'),'Document file should be less than 10MB');
		$("#selfile").text("select new document");
    return false;
  } else    {
    $('#selfile').css('font-color', 'default');
    clearError($('#errtext1'));
	}

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.filename= s_filename;
	formdataObj.currfile = s_filename;
	return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;
	return true;
}

$(document).ready(function()	{
	showformdata();
  var options = {
    beforeSend: function()  {
      clearError($('#errtext1'));
      startAjaxIcon();
    },
    success: function(response)     {
    	//alert(JSON.stringify(response));
      if (response.err == 0)  {
        setSuccess($('#errtext1'),response.text);
				var formdataObj = JSON.parse($('#formdata').val());
				formdataObj.indcfname = response.indcfname;
				$('#formdata').val(JSON.stringify(formdataObj));
				showformdata();
      } else
        setError($('#errtext1'),response.text);
      stopAjaxIcon();
    },
  	complete: function(response)    {
      stopAjaxIcon();
	  },
	  error: function(response)   {
      setError($('#errtext1'),response.text);
      stopAjaxIcon();
	  }
  };
  $("#adddocs-form").ajaxForm(options);

	$('#add_btn').click(function() 			{
		var add = data_add();
		if (!add)
			return false;
		return true;
	});
});

