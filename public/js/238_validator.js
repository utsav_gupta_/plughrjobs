$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.userexits').footable();
});

function def_emp()    {
	var xObj = JSON.parse($('#empdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var emp = new Object;
			emp.empid = obj.userid;
			emp.mgr = obj.mgr;
			$('<option>').val(JSON.stringify(emp)).text(obj.username).appendTo('#emp-list');
		}
	}
}

function init_exit()		{
	var s_emp = JSON.parse($('#emp-list').val());
	//alert(JSON.stringify(s_emp));
	if (s_emp == null)	{
		$('#emp-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select an employee');
		$('#emp-list').focus();
		return false;
	} else    {
		$('#emp-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.emp = s_emp.empid;
	formdataObj.mgr = s_emp.mgr;
	formdataObj.sts	= 0;  // Inititated

	var mesg = "Please reconfirm action";
	alertify.set({ labels: {
		ok     : "Inititate",
		cancel : "Don't inititate"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			//alert(JSON.stringify(formdataObj));
			//return false;

			$.ajax({
				type: 'POST',
				url: '/238init',
				data: formdataObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (!resp.err)   {
						setSuccess($('#errtext1'),"Exit initiated for selected user");
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.responseText);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

$(document).ready(function()	{
	def_emp();
	$('#init_btn').click(function() 			{
		init_exit();
		return false;
	});
});

