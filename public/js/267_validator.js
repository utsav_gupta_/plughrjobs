$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
  $('table.usergoals').footable();
});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function disableForm1()		{
	document.getElementById('rperiod-list').disabled = true;
	document.getElementById('usr-list').disabled = true;

	document.getElementById('search_btn').disabled = true;
	document.getElementById('clear_btn').disabled = false;
	document.getElementById('goal_btn').disabled = false;
	document.getElementById('self_btn').disabled = false;
	document.getElementById('supr_btn').disabled = false;
	document.getElementById('skip_btn').disabled = false;
}
function enableForm1()		{
	document.getElementById('rperiod-list').disabled = false;
	document.getElementById('usr-list').disabled = false;

	document.getElementById('search_btn').disabled = false;
	document.getElementById('clear_btn').disabled = false;
	document.getElementById('goal_btn').disabled = true;
	document.getElementById('self_btn').disabled = true;
	document.getElementById('supr_btn').disabled = true;
	document.getElementById('skip_btn').disabled = true;
}

function clearTable()		{
	var rtable = $('table.usergoals').data('footable');

	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
	$('table.usergoals tbody tr').each(function() {
		rtable.removeRow($(this));
	});
}

function checkfrozen(rperiod)    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj._id == rperiod && obj.frozen == 'true')	{
        document.getElementById('goal_btn').disabled = true;
        document.getElementById('self_btn').disabled = true;
        document.getElementById('supr_btn').disabled = true;
        document.getElementById('skip_btn').disabled = true;
				setError($('#errtext1'),'Changes to review has been disabled');
			}
		}
	}
}

function def_rperiod()    {
	var xObj = JSON.parse($('#rperioddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (obj.status < 4 )
				$('<option>').val(obj._id).text(obj.rptitle).appendTo('#rperiod-list');
		}
	}
}

function verify_targets()     {
	var ugoals = JSON.parse($('#formdata').val());
	if (ugoals)		{
		var glen = ugoals.length;
		if (glen > 0)   {
		  var tot = 0;
  		for(var i=0; i<glen; i++)   {
  			var obj = ugoals[i];
  		  tot = tot + parseInt(obj.weight);
		  }
		  if (tot < 100)
		    return 2;
		  else
		    return 0;
		} else
		  return 1;
  } else  {
    return 1;
  }
}

function def_usr()    {
	var xObj = JSON.parse($('#usrdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			var optObj = new Object;
			optObj.userid = obj.userid;
			optObj.roleid = obj.role;
			optObj.mgrid = obj.mgr;
			$('<option>').val(JSON.stringify(optObj)).text(obj.username).appendTo('#usr-list');
		}
	}
}

function def_sgoal(rperiod)    {
	$('#sgoal-list').empty();
	var xObj = JSON.parse($('#sgoaldata').val());
	if (xObj != null)    {
		$('<option selected>').val('').text("None").appendTo('#sgoal-list');
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			if (rperiod == obj.rperiod)
				$('<option>').val(obj.ugoalid).text(obj.ugoaltitle).appendTo('#sgoal-list');
		}
	}
}

function def_ugoalid()    {
	$('#ugoalid-list').empty();
	var xObj = JSON.parse($('#ugoaliddata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.title).appendTo('#ugoalid-list');
		}
	}
}

function getGoalTitle(goalid)		{
	var sel4Obj = JSON.parse($('#ugoaliddata').val());
	var rval = "Desc not found";
	if (sel4Obj)	{
		var dblen = sel4Obj.length;
		var set = false;
		for (var ctr = 0;ctr < dblen; ctr++)		{
			var selx = sel4Obj[ctr];
			if (goalid == selx._id)	{
				rval = selx.title;
				break;
			}
		}
	}
	return rval;
}

function validatewt(curr_wt, prev_wt)		{
	var mesgObj = JSON.parse($('#formdata').val());
	var totwt = 0;
	if (mesgObj != null)    {
		var glen = mesgObj.length;
		for(var i=0; i<glen; i++)   {
			totwt += parseInt(mesgObj[i].weight);
		}
		totwt += curr_wt;
		totwt -= prev_wt;
		if (totwt > 100)
			return true;
	}
	return false;
}

function defMesgTable()    {
	$('table.usergoals').data('footable').reset();
	$('table.usergoals thead').append('<tr>');
	$('table.usergoals thead tr').append('<th>Review period</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">User Name</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Self-target</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">User target</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Description</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Weightage (%)</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Criteria</th>');
	$('table.usergoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.usergoals thead tr').append('</tr>');
	$('table.usergoals').footable();
}
function fillUserData()		{
	var sel1Obj = JSON.parse($('#rperioddata').val());
	var sel2Obj = JSON.parse($('#usrdata').val());
	var sel3Obj = JSON.parse($('#sgoaldata').val());
	var sel4Obj = JSON.parse($('#ugoaliddata').val());
	var mesgObj = JSON.parse($('#formdata').val());

	if (mesgObj != null)    {
		var glen = mesgObj.length;
		var rtable = $('table.usergoals').data('footable');
		var rcount = 0;
		if (glen < 1)   {
		}

		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[i];
			var newRow = '<tr>';

			newRow += '<td>';
			var dblen = sel1Obj.length;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel1Obj[ctr];
				if (obj.rperiod == selx._id)	{
					newRow += selx.rptitle;
					break;
				}
			}
			newRow += '</td>';

			newRow += '<td>';
			var dblen = sel2Obj.length;
			var set = false;
			for (var ctr = 0;ctr < dblen; ctr++)		{
				var selx = sel2Obj[ctr];
				if (obj.userid == selx.userid)	{
					newRow += selx.username;
					set = true;
					break;
				}
			}
			if (!set)
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (sel3Obj)	{
				var dblen = sel3Obj.length;
				var set = false;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel3Obj[ctr];
					if (obj.mgoalid == selx.ugoalid)	{
						newRow += selx.ugoaltitle;
						set = true;
						break;
					}
				}
			}
			if (!set)
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (sel4Obj)	{
				var dblen = sel4Obj.length;
				var set = false;
				for (var ctr = 0;ctr < dblen; ctr++)		{
					var selx = sel4Obj[ctr];
					if (obj.ugoalid == selx._id)	{
						newRow += selx.title;
						newRow += '</td>';
						newRow += '<td>';
						newRow += selx.desc;
						set = true;
						break;
					}
				}
			}
			if (!set)		{
				newRow += '**No Data**';
				newRow += '</td>';
				newRow += '<td>';
				newRow += '**No Data**';
			}
			newRow += '</td>';

			newRow += '<td>';
			if (obj.weight != null)
				newRow += obj.weight;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
	}
}

function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/267newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.usergoals').data('footable');

			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}

function GetSearchInputs()		{
	var s_rperiod = $('#rperiod-list').val();
	if (s_rperiod == null)	{
		$('#rperiod-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a Review period');
		$('#rperiod-list').focus();
		return false;
	} else    {
		$('#rperiod-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	if ($('#usr-list').val() == null)	{
		$('#usr-list').css('border-color', 'red');
		setError($('#errtext1'),'Please select a user');
		$('#usr-list').focus();
		return false;
	} else    {
		$('#usr-list').css('border-color', 'default');
		clearError($('#errtext1'));
	}
	var s_usrobj = JSON.parse($('#usr-list').val());
	var s_usr = s_usrobj.userid;
	var s_role = s_usrobj.roleid;
	var s_mgr = s_usrobj.mgrid;

	var formdataObj = new  Object();
	formdataObj.coid = $('#ccoid').text();
	formdataObj.userid = $('#cuserid').text();
	formdataObj.rperiod = s_rperiod;
	formdataObj.usr = s_usr;
	formdataObj.roleid = s_role;
	formdataObj.mgrid = s_mgr;
	return formdataObj;
}

function get_goals()		{
	var formdataObj = GetSearchInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/267search',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data0));
				$('#sgoaldata').val(JSON.stringify(resp.data1));
				$('#ugoaliddata').val(JSON.stringify(resp.data2));

				def_ugoalid();
				fillUserData();
				SetPagination();

				var s_rperiod = $('#rperiod-list').val();
				def_sgoal(s_rperiod);
				disableForm1();

        if (resp.data0.length > 0)
  				setSuccess($('#errtext1'),'Targets for selected team member & review period listed below');
				else  {
  				setError($('#errtext1'),'No Targets defined for selected team member & review period');
	        document.getElementById('goal_btn').disabled = true;
	        document.getElementById('self_btn').disabled = true;
	        document.getElementById('supr_btn').disabled = true;
	        document.getElementById('skip_btn').disabled = true;
				}
				checkfrozen(s_rperiod);
			} else  {
				setError($('#errtext1'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.text);
			stopAjaxIcon();
		}
	});
	return false;
}

function reset_progress(reset_type)				{
	var mesg = "Confirm again, do you want to reset?";
	alertify.set({ labels: {
		ok     : "Reset",
		cancel : "Don't reset"
		} 
	});
	alertify.set({ buttonFocus: "cancel" });
	alertify.set({ buttonReverse: true });
	alertify.confirm(mesg, function (e) {
		if (e) {
			var ratingObj = new  Object();
			ratingObj.coid = $('#ccoid').text();
	    var s_usrobj = JSON.parse($('#usr-list').val());
	    var s_usr = s_usrobj.userid;
			ratingObj.userid = s_usr;
			ratingObj.rpid = $('#rperiod-list').val();
			ratingObj.reset_type = reset_type;

			//alert(JSON.stringify(ratingObj));
      //return false;

			$.ajax({
				type: 'POST',
				url: '/267reset',
				data: ratingObj,
				dataType: 'json',
				beforeSend:  function()   {
					startAjaxIcon();
				},
				success: function(resp) {
					if (resp.err == 0)   {
		        clearTable();
		        enableForm1();
		        if (reset_type == '1')
  						setSuccess($('#errtext1'),'Target commit is now reset ');
		        if (reset_type == '2')
  						setSuccess($('#errtext1'),'Self commit is now reset ');
		        if (reset_type == '3')
  						setSuccess($('#errtext1'),'Supervisor commit is now reset ');
		        if (reset_type == '4')
  						setSuccess($('#errtext1'),'Skip supervisor commit is now reset ');
					} else  {
						setError($('#errtext1'),resp.text);
					}
					stopAjaxIcon();
				},
				error: function(err) {
					setError($('#errtext1'),err.text);
					stopAjaxIcon();
				}
			});
			return false;
		}
	});
}

$(document).ready(function()	{
	def_rperiod();
	def_usr();
	defMesgTable();

	$('#search_btn').click(function() 		{
		get_goals();
		return false;
	});
	$('#clear_btn').click(function() 			{
		clearTable();
		clearError($('#errtext1'));
		enableForm1();
		return false;
	});
	$('#goal_btn').click(function() 		{
		reset_progress(1);
		return false;
	});
	$('#self_btn').click(function() 		{
		reset_progress(2);
		return false;
	});
	$('#supr_btn').click(function() 		{
		reset_progress(3);
		return false;
	});
	$('#skip_btn').click(function() 		{
		reset_progress(4);
		return false;
	});
});

