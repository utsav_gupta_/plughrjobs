$(function() {
	$(document).ajaxStart(function(){
		  $("#ajaxaction").show();
	})
	$(document).ajaxStop(function(){
		  $("#ajaxaction").hide();
	});
	$('#dob').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#e1sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#e2sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#p1sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#p1edate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#p2sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#p2edate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed1sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed1edate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed2sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed2edate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed3sdate').datepicker({ dateFormat: 'dd-mm-yy'});
	$('#ed3edate').datepicker({ dateFormat: 'dd-mm-yy'});

	$('#industry').select2({
		placeholder: 'Select all applicable industries',
		allowClear: true,
		dropdownAutoWidth : true,
	});

	/*$('#dept').select2({
		placeholder: 'Select all applicable functional areas',
		allowClear: true,
		dropdownAutoWidth : true,
	}); */

	$('#skills').select2({
		placeholder: 'Select applicable skills',
		allowClear: true,
		dropdownAutoWidth : true,
		tags: true,
		createTag: function(newTag) {
        return {
            id: newTag.term,
            text: newTag.term + ' (new)'
        };
    }
	});

	$('#indresp').select2({
		placeholder: 'Select suitable responsibilites',
		allowClear: true,
		dropdownAutoWidth : true,
	});

});
function isNumberKey(evt)  {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function capitaliseFirstLetter(fld)  {
	var txt = fld.value;
	fld.value = txt.charAt(0).toUpperCase() + txt.slice(1);
}

function redef()    {
  def_skills();
}

function def_dept()    {
	var xObj = JSON.parse($('#deptdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.depname).appendTo('#dept');
			$('<option>').val(obj._id).text(obj.depname).appendTo('#e1dept');
			$('<option>').val(obj._id).text(obj.depname).appendTo('#e2dept');
			$('<option>').val(obj._id).text(obj.depname).appendTo('#p1dept');
			$('<option>').val(obj._id).text(obj.depname).appendTo('#p2dept');
		}
	}
}

function def_skills()    {
	//var currdept = $('#dept').val();
	$('#skills').empty();
	var xObj = JSON.parse($('#skillsdata').val());
	if (xObj != null)    {
		var glen = xObj.length;
		for(var i=0; i<glen; i++)   {
			var obj = xObj[i];
			$('<option>').val(obj._id).text(obj.skillname).appendTo('#skills');
		}
	}
}

function showformdata()		{
	var formdataObj = JSON.parse($('#formdata').val());

	if (formdataObj != null)    {
    //if (formdataObj.dob)  {
      //var t = formdataObj.dob.indexOf('T00:');
		  //$('#dob').val(formdataObj.dob.substring(0,t));
	  //}
    //console.log(formdataObj.uname);
		$('#dbid').val(formdataObj._id);
		$('#uname').val(formdataObj.uname);
		$('#emailid').val(formdataObj.emailid);
		$('#mobno').val(formdataObj.mobno);
		$('#gender').val(formdataObj.gender).prop('selected', true);
		$('#cal1').val(formdataObj.cal1);
		$('#cal2').val(formdataObj.cal2);
		$('#ccity').val(formdataObj.ccity);
		$('#cpin').val(formdataObj.cpin);
		$('#linkedin').val(formdataObj.linkedin);
		$('#fb').val(formdataObj.fb);
		$('#twit').val(formdataObj.twit);
		$('#dob').val(formdataObj.dob.substring(8, 10) + '-' + formdataObj.dob.substring(5, 7) + '-' + formdataObj.dob.substring(0, 4));

		$('#industry').val(formdataObj.industry).trigger('change');
		$('#skills').val(formdataObj.skills).trigger('change');
		$('#about').val(formdataObj.about);
		$('#sigach').val(formdataObj.sigach);
		$('#indresp').val(formdataObj.indresp).trigger('change');
		$("#adays").val(formdataObj.adays);
		//$('#adays').val(formdataObj.adays);
		/*if(formdataObj.adays)
			for(i=0;i<formdataObj.adays.length;i++) {
				var aday = formdataObj.adays[i];
				$('#astime'+aday.value).val(aday.astime);
				$('#aetime'+aday.value).val(aday.aetime);
			} */
		/*
		$('#astime1').val(formdataObj.astime1);
		$('#aetime1').val(formdataObj.aetime1);
		$('#astime2').val(formdataObj.astime2);
		$('#aetime2').val(formdataObj.aetime2);
		$('#astime3').val(formdataObj.astime3);
		$('#aetime3').val(formdataObj.aetime3);
		$('#astime4').val(formdataObj.astime4);
		$('#aetime4').val(formdataObj.aetime4);
		$('#astime5').val(formdataObj.astime5);
		$('#aetime5').val(formdataObj.aetime5);
		$('#astime6').val(formdataObj.astime6);
		$('#aetime6').val(formdataObj.aetime6);
		$('#astime7').val(formdataObj.astime7);
		$('#aetime7').val(formdataObj.aetime7);
		*/

		$('#e1org').val(formdataObj.e1org);
		//$('#e1dept').val(formdataObj.e1dept);
		$('#e1role').val(formdataObj.e1role);
		if(formdataObj.e1sdate != '')
			$('#e1sdate').val(formdataObj.e1sdate.substring(8, 10) + '-' + formdataObj.e1sdate.substring(5, 7) + '-' + formdataObj.e1sdate.substring(0, 4));
		else
			$('#e1sdate').val('');
		//$('#e1days').val(formdataObj.e1days);
		//$('#e1stime').val(formdataObj.e1stime);
		//$('#e1etime').val(formdataObj.e1etime);
		$('#e2org').val(formdataObj.e2org);
		//$('#e2dept').val(formdataObj.e2dept);
		$('#e2role').val(formdataObj.e2role);
		if(formdataObj.e2sdate != '')
			$('#e2sdate').val(formdataObj.e2sdate.substring(8, 10) + '-' + formdataObj.e2sdate.substring(5, 7) + '-' + formdataObj.e2sdate.substring(0, 4));
		else
			$('#e2sdate').val('');
		//$('#e2days').val(formdataObj.e2days);
		//$('#e2stime').val(formdataObj.e2stime);
		//$('#e2etime').val(formdataObj.e2etime);
		$('#p1org').val(formdataObj.p1org);
		//$('#p1dept').val(formdataObj.p1dept);
		$('#p1role').val(formdataObj.p1role);
		if(formdataObj.p1sdate != '')
			$('#p1sdate').val(formdataObj.p1sdate.substring(8, 10) + '-' + formdataObj.p1sdate.substring(5, 7) + '-' + formdataObj.p1sdate.substring(0, 4));
		else
			$('#p1sdate').val('');
		if(formdataObj.p1edate != '')	
			$('#p1edate').val(formdataObj.p1edate.substring(8, 10) + '-' + formdataObj.p1edate.substring(5, 7) + '-' + formdataObj.p1edate.substring(0, 4));
		else
			$('#p1edate').val('');
		$('#p2org').val(formdataObj.p2org);
		//$('#p2dept').val(formdataObj.p2dept);
		$('#p2role').val(formdataObj.p2role);
		if(formdataObj.p2sdate != '')	
			$('#p2sdate').val(formdataObj.p2sdate.substring(8, 10) + '-' + formdataObj.p2sdate.substring(5, 7) + '-' + formdataObj.p2sdate.substring(0, 4));
		else
			$('#p2sdate').val('');
		if(formdataObj.p2edate != '')	
			$('#p2edate').val(formdataObj.p2edate.substring(8, 10) + '-' + formdataObj.p2edate.substring(5, 7) + '-' + formdataObj.p2edate.substring(0, 4));
		else
			$('#p2edate').val('');
		$('#ed1name').val(formdataObj.ed1name);
		$('#ed1colg').val(formdataObj.ed1colg);
		$('#ed1sdate').val(formdataObj.ed1sdate.substring(8, 10) + '-' + formdataObj.ed1sdate.substring(5, 7) + '-' + formdataObj.ed1sdate.substring(0, 4));
		$('#ed1edate').val(formdataObj.ed1edate.substring(8, 10) + '-' + formdataObj.ed1edate.substring(5, 7) + '-' + formdataObj.ed1edate.substring(0, 4));
		$('#ed2name').val(formdataObj.ed2name);
		$('#ed2colg').val(formdataObj.ed2colg);
		$('#ed2sdate').val(formdataObj.ed2sdate.substring(8, 10) + '-' + formdataObj.ed2sdate.substring(5, 7) + '-' + formdataObj.ed2sdate.substring(0, 4));
		$('#ed2edate').val(formdataObj.ed2edate.substring(8, 10) + '-' + formdataObj.ed2edate.substring(5, 7) + '-' + formdataObj.ed2edate.substring(0, 4));
		$('#ed3name').val(formdataObj.ed3name);
		$('#ed3colg').val(formdataObj.ed3colg);
		$('#ed3sdate').val(formdataObj.ed3sdate.substring(8, 10) + '-' + formdataObj.ed3sdate.substring(5, 7) + '-' + formdataObj.ed3sdate.substring(0, 4));
		$('#ed3edate').val(formdataObj.ed3edate.substring(8, 10) + '-' + formdataObj.ed3edate.substring(5, 7) + '-' + formdataObj.ed3edate.substring(0, 4));

	}
}
function GetValidInputs()			{
	var s_uname = $('#uname').val();
	if (s_uname == '')	{
		$('#label01').css('color', 'red');
		setError($('#errtext1'),'Please input Full Name');
		$('#uname').focus();
		return false;
	} else    {
		$('#label01').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_emailid = $('#emailid').val();
	if (s_emailid != '')	{
	  var lastAtPos = s_emailid.lastIndexOf('@');
	  var lastDotPos = s_emailid.lastIndexOf('.');
	  var result = (lastAtPos < lastDotPos && lastAtPos > 0 && s_emailid.indexOf('@@') == -1 && lastDotPos > 2 && (s_emailid.length - lastDotPos) > 2);
	  if (!result)	{
		  $('#label02').css('color', 'red');
		  setError($('#errtext1'),'Please input valid email id');
  		$('#emailid').focus();
		  return false;
	  } else    {
		  $('#label02').css('color', 'inherit');
		  clearError($('#errtext1'));
	  }
	} else  {
	  $('#label02').css('color', 'red');
	  setError($('#errtext1'),'Please input email id');
		$('#emailid').focus();
	  return false;
	}
	var s_mobno = $('#mobno').val();
	if (s_mobno == '')	{
		$('#label03').css('color', 'red');
		setError($('#errtext1'),'Please input Mobile Number');
		$('#mobno').focus();
		return false;
	} else    {
		$('#label03').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_gender = $('#gender-list').val();
	var s_cal1 = $('#cal1').val();
	if (s_cal1 == '')	{
		$('#label05').css('color', 'red');
		setError($('#errtext1'),'Please input current address');
		$('#cal1').focus();
		return false;
	} else    {
		$('#label05').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_cal2 = $('#cal2').val();
	if (s_cal2 == '')	{
		$('#label06').css('color', 'red');
		setError($('#errtext1'),'Please input locality');
		$('#cal2').focus();
		return false;
	} else    {
		$('#label06').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ccity = $('#ccity').val();
	if (s_ccity == '')	{
		$('#label07').css('color', 'red');
		setError($('#errtext1'),'Please input locality');
		$('#ccity').focus();
		return false;
	} else    {
		$('#label07').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_cpin = $('#cpin').val();
	if (s_cpin == '')	{
		$('#label08').css('color', 'red');
		setError($('#errtext1'),'Please input pincode');
		$('#cpin').focus();
		return false;
	} else    {
		$('#label08').css('color', 'inherit');
		clearError($('#errtext1'));
	}
  var s_linkedin = $('#linkedin').val();
  var s_fb = $('#fb').val();
  var s_twit = $('#twit').val();
	var s_dob = $('#dob').val();
	if (s_dob == '')	{
		$('#label10').css('color', 'red');
		setError($('#errtext1'),'Please input date of birth');
		$('#dob').focus();
		return false;
	} else    {
		$('#label10').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_adays = $("#adays").val();
  if (s_adays == null)	{
	  $('#label11').css('color', 'red');
		setError($('#errtext1'),'Please select availability');
		$('#adays').focus();
		return false;
	} else    {
		$('#label11').css('color', 'inherit');
		clearError($('#errtext1'));
	}
  
  var s_indresp = $('#indresp').val();
	if (s_indresp == null)	{
		$('#label23').css('color', 'red');
		setError($('#errtext1'),'Please select a responsibility');
		$('#indresp').focus();
		return false;
	} else    {
		$('#label23').css('color', 'inherit');
		clearError($('#errtext1'));
	}

	var s_industry = $('#industry').val();
	if (s_industry == null)	{
		$('#label12').css('color', 'red');
		setError($('#errtext1'),'Please select an industry');
		$('#industry').focus();
		return false;
	} else    {
		$('#label12').css('color', 'inherit');
		clearError($('#errtext1'));
	}

  var s_skills = $('#skills').val();
	if (s_skills == null)	{
		$('#label13').css('color', 'red');
		setError($('#errtext1'),'Please enter a skill');
		$('#skills').focus();
		return false;
	} else    {
		$('#label13').css('color', 'inherit');
		clearError($('#errtext1'));
	}

	var s_about = $('#about').val();
	if (s_about == '')	{
		$('#label14').css('color', 'red');
		setError($('#errtext1'),'Please input profile summary');
		$('#about').focus();
		return false;
	} else    {
		$('#label14').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_e1org = $('#e1org').val();
	if (s_e1org == '')	{
		$('#label16').css('color', 'orange');
		setError($('#errtext1'),'Please input organization name');
		//$('#e1org').focus();
	} else    {
		$('#label16').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_sigach = $('#sigach').val();
	if (s_sigach == '')	{
		$('#label17').css('color', 'red');
		setError($('#errtext1'),'Please enter your significant achievements');
		$('#sigach').focus();
		return false;
	} else    {
		$('#label17').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_e1role = $('#e1role').val();
	if (s_e1role == '')	{
		$('#label18').css('color', 'orange');
		setError($('#errtext1'),'Please input role');
		//$('#e1role').focus();
	} else    {
		$('#label18').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_e1sdate = $('#e1sdate').val();
	if (s_e1sdate == '')	{
		$('#label19').css('color', 'orange');
		setError($('#errtext1'),'Please input start date');
		//$('#e1sdate').focus();
	} else    {
		$('#label19').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	/*var s_e1days = $("input[name='e1days']").serializeArray()
  if (s_e1days.length == 0)	{
	  $('#label20').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement level');
	  $('#e1days').focus();
  } else    {
	  $('#label20').css('color', 'inherit');
	  clearError($('#errtext1'));
  } */
	/*var s_e1stime = $('#e1stime').val();
  if (s_e1stime.length == 0)	{
	  $('#label21').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement start time');
	  $('#e1stime').focus();
  } else    {
	  $('#label21').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_e1etime = $('#e1etime').val();
  if (s_e1etime.length == 0)	{
	  $('#label21').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement end time');
	  $('#e1etime').focus();
  } else    {
	  $('#label21').css('color', 'inherit');
	  clearError($('#errtext1'));
  } */
	var s_e2org = $('#e2org').val();
	if (s_e2org == '')	{
		$('#label22').css('color', 'orange');
		setError($('#errtext1'),'Please input organization name');
		//$('#e2org').focus();
	} else    {
		$('#label22').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	//var s_e2dept = $('#e2dept').val();
	var s_e2role = $('#e2role').val();
	if (s_e2role == '')	{
		$('#label24').css('color', 'orange');
		setError($('#errtext1'),'Please input role');
		//$('#e2role').focus();
	} else    {
		$('#label24').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_e2sdate = $('#e2sdate').val();
	if (s_e2sdate == '')	{
		$('#label25').css('color', 'orange');
		setError($('#errtext1'),'Please input start date');
		//$('#e2sdate').focus();
	} else    {
		$('#label25').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	/*var s_e2days = $("input[name='e2days']").serializeArray()
	if (s_e2days.length == 0)	{
	  $('#label26').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement level');
	  $('#e2days').focus();
  } else    {
	  $('#label26').css('color', 'inherit');
	  clearError($('#errtext1'));
  } */
	/*var s_e2stime = $('#e2stime').val();
	if (s_e2stime.length == 0)	{
	  $('#label27').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement start time');
	  $('#e2stime').focus();
  } else    {
	  $('#label27').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_e2etime = $('#e2etime').val();
	if (s_e2etime.length == 0)	{
	  $('#label27').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement end time');
	  $('#e2etime').focus();
  } else    {
	  $('#label27').css('color', 'inherit');
	  clearError($('#errtext1'));
  } */

	var s_p1org = $('#p1org').val();
	if (s_p1org == '')	{
		$('#label29').css('color', 'orange');
		setError($('#errtext1'),'Please input organization name');
		//$('#e2org').focus();
	} else    {
		$('#label29').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	//var s_p1dept = $('#p1dept').val();
	var s_p1role = $('#p1role').val();
	if (s_p1role == '')	{
		$('#label31').css('color', 'orange');
		setError($('#errtext1'),'Please input role');
		//$('#e2role').focus();
	} else    {
		$('#label31').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_p1sdate = $('#p1sdate').val();
	if (s_p1sdate.length == 0)	{
	  $('#label32').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement start date');
	  //$('#e2stime').focus();
  } else    {
	  $('#label32').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_p1edate = $('#p1edate').val();
	if (s_p1edate.length == 0)	{
	  $('#label32').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement end date');
	  //$('#e2stime').focus();
  } else    {
	  $('#label32').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_p2org = $('#p2org').val();
	if (s_p2org == '')	{
		$('#label33').css('color', 'orange');
		setError($('#errtext1'),'Please input organization name');
		//$('#e2org').focus();
	} else    {
		$('#label33').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	//var s_p2dept = $('#p2dept').val();
	var s_p2role = $('#p2role').val();
	if (s_p2role == '')	{
		$('#label35').css('color', 'orange');
		setError($('#errtext1'),'Please input role');
		//$('#e2role').focus();
	} else    {
		$('#label35').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_p2sdate = $('#p2sdate').val();
	if (s_p2sdate.length == 0)	{
	  $('#label36').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement start date');
	  //$('#e2stime').focus();
  } else    {
	  $('#label36').css('color', 'inherit');
	  clearError($('#errtext1'));
  }
	var s_p2edate = $('#p2edate').val();
	if (s_p2edate.length == 0)	{
	  $('#label36').css('color', 'orange');
	  setError($('#errtext1'),'Please input engagement end date');
	  //$('#e2stime').focus();
  } else    {
	  $('#label36').css('color', 'inherit');
	  clearError($('#errtext1'));
  }

	var s_ed1name = $('#ed1name').val();
	if(s_ed1name == '')	{
		$('#label38').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the qualification');
		$('#ed1name').focus();
		return false;
	} else    {
		$('#label38').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed1colg = $('#ed1colg').val();
	if(s_ed1colg == '')	{
		$('#label39').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the college');
		$('#ed1colg').focus();
		return false;
	} else    {
		$('#label39').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed1sdate = $('#ed1sdate').val();
	if (s_ed1sdate == '')	{
		$('#label40').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed1sdate').focus();
		return false;
	} else    {
		$('#label40').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed1edate = $('#ed1edate').val();
	if (s_ed1edate == '')	{
		$('#label40').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed1edate').focus();
		return false;
	} else    {
		$('#label40').css('color', 'inherit');
		clearError($('#errtext1'));
	}

	var s_ed2name = $('#ed2name').val();
	if(s_ed2name == '')	{
		$('#label41').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the qualification');
		$('#ed2name').focus();
		return false;
	} else    {
		$('#label41').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed2colg = $('#ed2colg').val();
	if(s_ed2colg == '')	{
		$('#label42').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the college');
		$('#ed2colg').focus();
		return false;
	} else    {
		$('#label42').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed2sdate = $('#ed2sdate').val();
	if (s_ed2sdate == '')	{
		$('#label43').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed2sdate').focus();
		return false;
	} else    {
		$('#label43').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed2edate = $('#ed2edate').val();
	if (s_ed2edate == '')	{
		$('#label43').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed2edate').focus();
		return false;
	} else    {
		$('#label43').css('color', 'inherit');
		clearError($('#errtext1'));
	}

	var s_ed3name = $('#ed3name').val();
	if(s_ed3name == '')	{
		$('#label44').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the qualification');
		$('#ed3name').focus();
		return false;
	} else    {
		$('#label44').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed3colg = $('#ed3colg').val();
	if(s_ed3colg == '')	{
		$('#label45').css('color', 'red');
		setError($('#errtext1'),'Please input Name of the college');
		$('#ed3colg').focus();
		return false;
	} else    {
		$('#label45').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed3sdate = $('#ed3sdate').val();
	if (s_ed3sdate == '')	{
		$('#label46').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed3sdate').focus();
		return false;
	} else    {
		$('#label46').css('color', 'inherit');
		clearError($('#errtext1'));
	}
	var s_ed3edate = $('#ed3edate').val();
	if (s_ed3edate == '')	{
		$('#label46').css('color', 'red');
		setError($('#errtext1'),'Please input duration');
		$('#ed3edate').focus();
		return false;
	} else    {
		$('#label46').css('color', 'inherit');
		clearError($('#errtext1'));
	}

	var s_dbid = $('#dbid').val();

	var formdataObj = new  Object();
	formdataObj.dbid = s_dbid;
	formdataObj.uname = s_uname;
	formdataObj.emailid = s_emailid;
	formdataObj.mobno = s_mobno;
	formdataObj.gender = s_gender;
	formdataObj.cal1 = s_cal1;
	formdataObj.cal2 = s_cal2;
	formdataObj.ccity = s_ccity;
	formdataObj.cpin = s_cpin;
	formdataObj.linkedin = s_linkedin;
	formdataObj.fb = s_fb;
	formdataObj.twit = s_twit;
	formdataObj.dob = s_dob;
	//formdataObj.dept = s_dept;
	formdataObj.industry = s_industry;
	formdataObj.skills = s_skills;
	formdataObj.about = s_about;
	formdataObj.sigach = s_sigach;
	formdataObj.indresp = s_indresp;
	formdataObj.adays = s_adays;
	/*
	formdataObj.astime1 = s_astime1;
	formdataObj.aetime1 = s_aetime1;
	formdataObj.astime2 = s_astime2;
	formdataObj.aetime2 = s_aetime2;
	formdataObj.astime3 = s_astime3;
	formdataObj.aetime3 = s_aetime3;
	formdataObj.astime4 = s_astime4;
	formdataObj.aetime4 = s_aetime4;
	formdataObj.astime5 = s_astime5;
	formdataObj.aetime5 = s_aetime5;
	formdataObj.astime6 = s_astime6;
	formdataObj.aetime6 = s_aetime6;
	formdataObj.astime7 = s_astime7;
	formdataObj.aetime7 = s_aetime7;
	*/

	formdataObj.e1org = s_e1org;
	//formdataObj.e1dept = s_e1dept;
	formdataObj.e1role = s_e1role;
	formdataObj.e1sdate = s_e1sdate;
	//formdataObj.e1days = s_e1days;
	//formdataObj.e1stime = s_e1stime;
	//formdataObj.e1etime = s_e1etime;
	formdataObj.e2org = s_e2org;
	//formdataObj.e2dept = s_e2dept;
	formdataObj.e2role = s_e2role;
	formdataObj.e2sdate = s_e2sdate;
	//formdataObj.e2days = s_e2days;
	//formdataObj.e2stime = s_e2stime;
	//formdataObj.e2etime = s_e2etime;
	formdataObj.p1org = s_p1org;
	//formdataObj.p1dept = s_p1dept;
	formdataObj.p1role = s_p1role;
	formdataObj.p1sdate = s_p1sdate;
	formdataObj.p1edate = s_p1edate;
	formdataObj.p2org = s_p2org;
	//formdataObj.p2dept = s_p2dept;
	formdataObj.p2role = s_p2role;
	formdataObj.p2sdate = s_p2sdate;
	formdataObj.p2edate = s_p2edate;
	formdataObj.ed1name = s_ed1name;
	formdataObj.ed1colg = s_ed1colg;
	formdataObj.ed1sdate = s_ed1sdate;
	formdataObj.ed1edate = s_ed1edate;
	formdataObj.ed2name = s_ed2name;
	formdataObj.ed2colg = s_ed2colg;
	formdataObj.ed2sdate = s_ed2sdate;
	formdataObj.ed2edate = s_ed2edate;
	formdataObj.ed3name = s_ed3name;
	formdataObj.ed3colg = s_ed3colg;
	formdataObj.ed3sdate = s_ed3sdate;
	formdataObj.ed3edate = s_ed3edate;
  return formdataObj;
}

function data_add()		{
	var formdataObj = GetValidInputs();
	if (!formdataObj)
		return false;

  //alert(JSON.stringify(formdataObj));
  //return false;

	$.ajax({
		type: 'POST',
		url: '/502',
		data: formdataObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(o) { 
			if (!o.err)     {
				setSuccess($('#errtext1'),'data added');
				$('#formdata').val(JSON.stringify(o));
			} else  {
				setError($('#errtext1'),o.text);
			}
			stopAjaxIcon();
		},
		error: function(err) { 
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		}
	});
	return false;
}

$(document).ready(function()	{
  //def_dept();
  //def_skills();
	showformdata();

	$('#upd_btn').click(function() 			{
		data_add();
	});
});

