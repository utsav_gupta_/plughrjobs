$(function() {
	$(document).ajaxStart(function()		{
		$('#ajaxaction').show();
	})
	$(document).ajaxStop(function()	{
		$('#ajaxaction').hide();
	});
});

function defMesgTable()    {
	$('table.usergoals').data('footable').reset();
	$('table.usergoals thead').append('<tr>');
	$('table.usergoals thead tr').append('<th>Objective</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Expected Performance</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Start Date</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">End Date</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Your Comments</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Manager Comments</th>');
	$('table.usergoals thead tr').append('<th data-hide="phone,tablet">Comment</th>');
	$('table.usergoals thead tr').append('<th data-ignore="true", data-hide="all">SerNo</th>');
	$('table.usergoals thead tr').append('</tr>');
	$('table.usergoals').footable();
}
function fillUserData()		{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length > 0)    {
		if (mesgObj[0].piptask)
			var glen = mesgObj[0].piptask.length;
		else
			var glen = 0;

		var rtable = $('table.usergoals').data('footable');
		var rcount = 0;

		for(var i=0; i<glen; i++)   {
			var obj = mesgObj[0].piptask[i];
			var newRow = '<tr>';

			newRow += '<td>';
			if (obj.obj != null)
				newRow += obj.obj;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.meets != null)
				newRow += obj.meets;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.sdate != null)
				newRow += obj.sdate;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.edate != null)
				newRow += obj.edate;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.scomm != null)
				newRow += obj.scomm;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			if (obj.mcomm != null)
				newRow += obj.mcomm;
			else
				newRow += '**No Data**';
			newRow += '</td>';

			newRow += '<td>';
			newRow += '<a class="row-comment" href="#"><span id = "c'+i+'" class="glyphicon glyphicon-comment" title="Comment"></span></a>';
			newRow += '</td>';

			newRow += '<td id="rowIndex">';
			newRow += rcount+1;
			newRow += '</td></tr>';
			rtable.appendRow(newRow);
			rcount++;
		}
		rtable.redraw();
		setSuccess($('#errtext1'),'PIP objectives listed below');
	} else	{
		setError($('#errtext1'),'PIP not defined for you yet!! Are you expecting one?');
	}
}


function SetPagination() {
	var totpages = $('#totpages').val();
	if (totpages == 0)
		totpages =1;
	var options = {
		currentPage: 1,
		totalPages: totpages,
		size:'small',
		bootstrapMajorVersion:3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case 'first':
				return 'First';
			case 'prev':
				return 'Previous';
			case 'next':
				return 'Next';
			case 'last':
				return 'Last';
			case 'page':
				return page;
			}
		},
		onPageClicked: function(e,originalEvent,type,page)    {
			var currPage = $(e.currentTarget).bootstrapPaginator('getPages').current;
			if (currPage != page)   {
				GetNextPage(page);
			}
		}
	}
	$('#listpages').bootstrapPaginator(options);
}
function GetNextPage(newpage)    {
	var mesgObject = new Object();
	mesgObject.newpage = newpage;

	$.ajax ({
		type: 'GET',
		url: '/440newpage',
		data: mesgObject,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext1'),err.responseText);
			stopAjaxIcon();
		},
		success: function(mesgObj)	{
			$('#formdata').val(JSON.stringify(mesgObj));
			// DELETE & RE-CREATE Table 
			var rtable = $('table.usergoals').data('footable');

			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			$('table.usergoals tbody tr').each(function() {
				rtable.removeRow($(this));
			});
			fillUserData();
			stopAjaxIcon();
		}
	});
	return false;
}
$(function () {
	$('table.usergoals').footable().on('click', '.row-comment', function(e) {
		e.preventDefault();
		var row = $(this).find('span').attr('id');
		var rowindex = row.substring(1);
		clearError($('#errtext1'));
		showModal(rowindex);
	});
});

function showModal(indx)	{
	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length != 0)    {
		var glen = mesgObj[0].piptask.length;

		var rno = parseInt(indx);
		var obj = mesgObj[0].piptask[rno];
		$('#modal_indx1').val(rno);
		$('#modal_dbid1').val(mesgObj[0]._id);
		$('#mcomm').val(obj.scomm);
	  $('#CommentModal').modal();
  }
  return false;
}

function upd_comment()				{
	var s_dbid = $('#modal_dbid1').val();
	var s_indx = $('#modal_indx1').val();
	var s_comment = $('#mcomm').val();

	var commObj = new  Object();
	commObj.coid = $('#ccoid').text();
	commObj.dbid = s_dbid;
	commObj.scomm = s_comment;

	var mesgObj = JSON.parse($('#formdata').val());
	if (mesgObj.length == 0)    
		return false;
	else		{
		var obj = mesgObj[0].piptask[s_indx-1];
		commObj.usr = mesgObj[0].userid;
		commObj.old_obj = obj.obj;
		commObj.old_meets= obj.meets;
		commObj.old_sdate= obj.sdate;
		commObj.old_edate= obj.edate;
	}

	//alert(JSON.stringify(commObj));
	//return false;

	$.ajax({
		type: 'POST',
		url: '/440self',
		data: commObj,
		dataType: 'json',
		beforeSend:  function()   {
			startAjaxIcon();
		},
		success: function(resp) {
			if (!resp.err)   {
				$('#formdata').val(JSON.stringify(resp.data));
				$('#totpages').val(JSON.stringify(resp.totpages));

				// DELETE & RE-CREATE Table 
				var rtable = $('table.usergoals').data('footable');

				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('table.usergoals tbody tr').each(function() {
					rtable.removeRow($(this));
				});
				$('#editrow').val(-1);
				//defMesgTable();
				fillUserData();
				SetPagination();
				setSuccess($('#errtext1'),'Manager comment updated');
				$('#CommentModal').modal('hide');
			} else  {
				setError($('#errtext3'),resp.text);
			}
			stopAjaxIcon();
		},
		error: function(err) {
			setError($('#errtext3'),err.text);
			stopAjaxIcon();
		}
	});

	return false;
}

$(document).ready(function()	{
	defMesgTable();
	fillUserData();
	SetPagination();
	$('#commentupd_btn').click(function() 			{
		upd_comment();
		return false;
	});
});

